#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------

# try to get a hint from pkg-config
find_package(PkgConfig)
if (PKG_CONFIG_FOUND)
  pkg_check_modules(PKG_UDUNITS2 QUIET udunits2)
endif()

# locate the library and headers
find_library(UDUNITS2_LIBRARY NAMES "udunits2" PATHS ${PKG_UDUNITS2_LIBRARY_DIRS})
find_path(UDUNITS2_INCLUDE_DIR NAMES "udunits2.h" PATHS ${PKG_UDUNITS2_INCLUDE_DIRS} PATH_SUFFIXES udunits2)
set(UDUNITS2_VERSION ${PKG_UDUNITS2_VERSION})

# standard processing
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  UDUNITS2
  REQUIRED_VARS
  UDUNITS2_LIBRARY
  UDUNITS2_INCLUDE_DIR
  VERSION_VAR UDUNITS2_VERSION
)

if (UDUNITS2_FOUND)
  set(UDUNITS2_LIBRARIES ${UDUNITS2_LIBRARY})
  set(UDUNITS2_INCLUDE_DIRS ${UDUNITS2_INCLUDE_DIR})
  set(UDUNITS2_DEFINITIONS ${PKG_UDUNITS2_CFLAGS_OTHER})
endif()

# make an imported library
if (UDUNITS2_FOUND AND NOT TARGET UDUNITS2::UDUNITS2)
  add_library(UDUNITS2::UDUNITS2 UNKNOWN IMPORTED)
  set_target_properties(UDUNITS2::UDUNITS2 PROPERTIES
    IMPORTED_LOCATION "${UDUNITS2_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PKG_UDUNITS2_CFLAGS_OHTER}"
    INTERFACE_INCLUDE_DIRECTORIES "${UDUNITS2_INCLUDE_DIR}"
  )
endif()

mark_as_advanced(
  UDUNITS2_INCLUDE_DIR
  UDUNITS2_LIBRARY
)
