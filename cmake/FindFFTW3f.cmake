#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------

# try to get a hint from pkg-config
find_package(PkgConfig)
if (PKG_CONFIG_FOUND)
  pkg_check_modules(PKG_FFTW3f QUIET fftw3f)
endif()

# locate the library and headers
find_library(FFTW3f_LIBRARY NAMES "fftw3f" PATHS ${PKG_FFTW3f_LIBRARY_DIRS})
find_library(FFTW3f_threads_LIBRARY NAMES "fftw3f_threads" PATHS ${PKG_FFTW3f_LIBRARY_DIRS})
find_path(FFTW3f_INCLUDE_DIR NAMES "fftw3.h" PATHS ${PKG_FFTW3f_INCLUDE_DIRS})
set(FFTW3f_VERSION ${PKG_FFTW3f_VERSION})

# standard processing
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  FFTW3f
  REQUIRED_VARS
    FFTW3f_LIBRARY
    FFTW3f_threads_LIBRARY
    FFTW3f_INCLUDE_DIR
  VERSION_VAR FFTW3f_VERSION
)

if (FFTW3f_FOUND)
  set(FFTW3f_LIBRARIES ${FFTW3f_LIBRARY})
  set(FFTW3f_INCLUDE_DIRS ${FFTW3f_INCLUDE_DIR})
  set(FFTW3f_DEFINITIONS ${PKG_FFTW3f_CFLAGS_OTHER})
endif()

# make an imported library
if (FFTW3f_FOUND AND NOT TARGET FFTW3f::FFTW3f)
  add_library(FFTW3f::FFTW3f UNKNOWN IMPORTED)
  set_target_properties(FFTW3f::FFTW3f PROPERTIES
    IMPORTED_LOCATION "${FFTW3f_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PKG_FFTW3f_CFLAGS_OHTER}"
    INTERFACE_INCLUDE_DIRECTORIES "${FFTW3f_INCLUDE_DIR}"
  )
  add_library(FFTW3f::FFTW3f_threads UNKNOWN IMPORTED)
  set_target_properties(FFTW3f::FFTW3f_threads PROPERTIES
    IMPORTED_LOCATION "${FFTW3f_threads_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PKG_FFTW3f_CFLAGS_OHTER}"
    INTERFACE_INCLUDE_DIRECTORIES "${FFTW3f_INCLUDE_DIR}"
  )
endif()

mark_as_advanced(
  FFTW3f_INCLUDE_DIR
  FFTW3f_LIBRARY
  FFTW3f_threads_LIBRARY
)
