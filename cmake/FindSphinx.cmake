#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------

# Find the Sphinx documentation generator
#
# Usage:
#   find_package(Sphinx [REQUIRED] [QUIET])
#
# If Sphinx is found the following variables are set:
# - SPHINX_FOUND
# - SPHINX_EXECUTABLE

find_program(SPHINX_EXECUTABLE
  NAMES sphinx-build sphinx-build-3 sphinx-build-2
  DOC "Sphinx documentation generator"
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Sphinx DEFAULT_MSG SPHINX_EXECUTABLE)
