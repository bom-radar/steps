/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array2.h"
#include "filters.h"
#include "fourier_transform.h"
#include "util.h"
#include "vec2.h"

namespace steps
{
  /// Set of 2D arrays that represent the decomposed scales of a spatial field
  class cascade
  {
  public:
    /// Constructor
    cascade(int levels, vec2i shape);

    /// Number of levels in the cascase
    auto size() const -> int { return levels_.size(); }

    /// Access a cascade level
    auto& operator[](int lvl) const { return levels_[lvl]; }

    /// Access a cascade level
    auto& operator[](int lvl) { return levels_[lvl]; }

    /// Dump cascade to the diagnostics file
    auto dump_diagnostics(char const* name, std::initializer_list<int> coords = {}) const -> void;

  private:
    vector<array2f> levels_;
  };

  /// Decompose a cascade from a spatial input, optionally capturing the spectrum magnitude
  auto decompose(
        fourier_transform const& fft
      , filter_bank const& filters
      , array2f const& field
      , float padding
      , int border_width
      , cascade& cascade
      , filter* psd
      ) -> void;

  /// Decompose a cascade from a spectral input
  auto decompose(
        fourier_transform const& fft
      , filter_bank const& filters
      , fourier_transform::array2c const& spectrum
      , cascade& cascade
      ) -> void;

  /// Recompose a cascade into a single spatial image
  auto recompose(
        cascade const& cascade
      , array2f& field
      ) -> void;

  /// Recompose a specified number levels from the top of a cascade into a single spatial image
  auto recompose_partial(
        cascade const& cascade
      , int levels
      , array2f& field
      ) -> void;

  /// Recompose a cascade while also unnormalizing the values in each level
  auto recompose_unnormalize(
        cascade const& cascade
      , array1f const& means
      , array1f const& stddevs
      , array2f& field
      ) -> void;
}
