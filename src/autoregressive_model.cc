/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "autoregressive_model.h"
#include "format.h"
#include "trace.h"
#include "unit_test.h"
#include <cmath>

using namespace steps;

/* Note for future self:
 *
 * The phi(0) parameter is the variance of the random variable, calculated assuming the process variance is 1, and the
 * phi(1) and phi(2) as calculated based on the autocorrelations.
 *
 * We take the equations for the variance of the AR(1) or AR(2) process and assume the answer is 1.  We then simplify
 * the equation to solve for the variance of the noise.
 *
 * Thus, the phi(0) is the value we need to scale a N(0,1) random variable by to ensure it has the correct variance to
 * ensure the AR(n) model maintains our process variance of 1.
 */

ar_model::ar_model()
  : order_{0}
  , c_{0.0}
  , phi0_{1.0}
  , phi1_{0.0}
  , phi2_{0.0}
{ }

auto ar_model::setup_ar0(double mean, double stddev) -> void
{
  phi2_ = 0.0;
  phi1_ = 0.0;
  phi0_ = stddev;
  c_ = mean;
  order_ = 0;
}

auto ar_model::setup_ar1(double mean, double stddev, double ac) -> void
{
  phi2_ = 0.0;
  phi1_ = ac;
  phi0_ = std::sqrt(stddev * stddev * (1.0 - ac * ac));
  c_ = mean * (1.0 - phi1_);
  order_ = 1;
}

/* Use the Yule Walker equations for solve for phi 0, 1, 2 where phi 0 is the term for the N(0,1) noise update.
 * Assumes that the time series has mean of 0 and variance of 1.
 * Bras & Rodgriguez-Iturbe, 1984, Random functions and hydrology. Addison-Wesley Publishing Company. page 29 & 33 */
auto ar_model::setup_ar2(double mean, double stddev, double ac1, double ac2) -> void
{
  // do not permit oscillating time series
  if (ac1 < 0.01)
    ac1 = 0.01;

  double ac11 = ac1 * ac1;

  // ensure that the correlation lies in the proper solution space
  // this just checks that ac2 will lead to a stationary AR(2) model
  if (ac2 < (2.0 * ac11) - 1.0)
  {
    // if we are non-stationary we just raise ac1 to the power of 1.5 (Alan's suggestion)
    ac2 = std::pow(ac1, 1.5);

    // we could also attempt to move ac2 the minimal amount to ensure a good solution - as below:
    //ac2 = (2.0 * ac11) - 1.0;
  }

  // calculate the values of phi
  phi2_ = (ac2 - ac11) / (1.0 - ac11);
  phi1_ = ac1 * (1.0 - ac2) / (1.0 - ac11);
  phi0_ = sqrt((stddev * stddev * (1.0 + phi2_) * (1.0 - phi1_ - phi2_) * (1.0 + phi1_ - phi2_)) / (1.0 - phi2_));
  c_ = mean * (1.0 - phi1_ - phi2_);

  // sanity check that the derived process is stationary
  // https://mcs.utm.utoronto.ca/~nosedal/sta457/ar1-and-ar2.pdf (slide 29)
  if (phi2_ <= -1.0 || phi2_ >= 1.0 || phi2_ + phi1_ >= 1.0 || phi2_ - phi1_ >= 1.0)
    throw std::runtime_error{format("Non-stationary AR(2) phi parameters ({}, {}, {})", phi0_, phi1_, phi2_)};

  order_ = 2;
}

auto ar_model::autocovariance(int lag) -> double
{
  if (order_ == 2)
  {
    auto varz = ((1.0 - phi2_) * phi0_ * phi0_) / ((1 + phi2_) * (1 - phi1_ - phi2_) * (1 + phi1_ - phi2_));
    if (lag > 1)
    {
#if 0
      // analytic implementation (fastest)
      // TODO
#elif 1
      // iterative implementation (slow)
      auto acneg2 = varz;
      auto acneg1 = (phi1_ * varz) / (1.0 - phi2_);
      auto acneg0 = phi1_ * acneg1 + phi2_ * acneg2;
      for (int i = 2; i < lag; ++i)
      {
        acneg2 = acneg1;
        acneg1 = acneg0;
        acneg0 = phi1_ * acneg1 + phi2_ * acneg2;
      }
      return acneg0;
#else
      // recursive implementation (very slow)
      return phi1_ * autocovariance(lag - 1) + phi2_ * autocovariance(lag - 2);
#endif
    }
    else if (lag == 1)
      return (phi1_ * varz) / (1.0 - phi2_);
    else
      return varz;
  }
  else if (order_ == 1)
    return std::pow(phi1_, lag) * ((phi0_ * phi0_) / (1.0 - phi1_ * phi1_));
  else
    return 0.0;
}

auto ar_model::autocorrelation(int lag) -> double
{
  if (order_ == 2)
  {
    if (lag > 2)
    {
#if 0
      // analytic implementation (fastest)
      // TODO
#elif 1
      // iterative implementation (slow)
      auto acneg2 = phi1_ / (1.0 - phi2_);
      auto acneg1 = (phi1_ * phi1_) / (1.0 - phi2_) + phi2_;
      auto acneg0 = phi1_ * acneg1 + phi2_ * acneg2;
      for (int i = 3; i < lag; ++i)
      {
        acneg2 = acneg1;
        acneg1 = acneg0;
        acneg0 = phi1_ * acneg1 + phi2_ * acneg2;
      }
      return acneg0;
#else
      // recursive implementation (very slow)
      return phi1_ * autocorrelation(lag - 1) + phi2_ * autocorrelation(lag - 2);
#endif
    }
    else if (lag == 2)
      return (phi1_ * phi1_) / (1.0 - phi2_) + phi2_;
    else if (lag == 1)
      return phi1_ / (1.0 - phi2_);
    else
      return 1.0;
  }
  else if (order_ == 1)
    return std::pow(phi1_, lag);
  else
    return 0.0;
}

auto ar_model::forecast_ar0(span<float> lag0, span<float const> noise) -> void
{
  for (auto i = 0; i < lag0.size(); ++i)
    lag0[i] = c_ + phi0_ * noise[i];
}

auto ar_model::forecast_ar1(span<float> lag0, span<float const> noise, span<float const> lag1) -> void
{
  for (auto i = 0; i < lag0.size(); ++i)
    lag0[i] = c_ + phi0_ * noise[i] + phi1_ * lag1[i];
}

auto ar_model::forecast_ar2(span<float> lag0, span<float const> noise, span<float const> lag1, span<float const> lag2) -> void
{
  for (auto i = 0; i < lag0.size(); ++i)
    lag0[i] = c_ + phi0_ * noise[i] + phi1_ * lag1[i] + phi2_ * lag2[i];
}

// LCOV_EXCL_START
TEST_CASE("ar_model")
{
  auto arn = ar_model{};

  // check default state
  CHECK(arn.order() == 0);
  CHECK(arn.c() == approx(0.0));
  CHECK(arn.phi0() == approx(1.0));
  CHECK(arn.phi1() == approx(0.0));
  CHECK(arn.phi2() == approx(0.0));

  // ar0
  arn.setup_ar0(10.0, 5.0);
  CHECK(arn.order() == 0);
  CHECK(arn.c() == approx(10.0));
  CHECK(arn.phi0() == approx(5.0));
  CHECK(arn.phi1() == approx(0.0));
  CHECK(arn.phi2() == approx(0.0));

  // ar1
  arn.setup_ar1(100.0, 10.0, 0.9);
  CHECK(arn.order() == 1);
  CHECK(arn.c() == approx(10.0));
  CHECK(arn.phi0() == approx(4.35889));
  CHECK(arn.phi1() == approx(0.9));
  CHECK(arn.phi2() == approx(0.0));

  // ar2 (degenerate to ar1)
  arn.setup_ar2(100.0, 10.0, 0.9, 0.81);
  CHECK(arn.order() == 2);
  CHECK(arn.c() == approx(10.0));
  CHECK(arn.phi0() == approx(4.35889));
  CHECK(arn.phi1() == approx(0.9));
  CHECK(arn.phi2() == approx(0.0));

  // ar2
  arn.setup_ar2(100.0, 10.0, 0.9, 0.85);
  CHECK(arn.order() == 2);
  CHECK(arn.c() == approx(7.89473));
  CHECK(arn.phi0() == approx(4.26120));
  CHECK(arn.phi1() == approx(0.71052));
  CHECK(arn.phi2() == approx(0.21052));

  // forecast as ar0, ar1 and ar2
  float lag0, noise = 1.0f, lag1 = 5.0f, lag2 = 3.0f;
  arn.forecast_ar0(span{&lag0, 1}, span{&noise, 1});
  CHECK(lag0 == approx(7.89473 + 4.26120 * 1.0));
  arn.forecast_ar1(span{&lag0, 1}, span{&noise, 1}, span{&lag1, 1});
  CHECK(lag0 == approx(7.89473 + 4.26120 * 1.0 + 0.71052 * 5.0));
  arn.forecast_ar2(span{&lag0, 1}, span{&noise, 1}, span{&lag1, 1}, span{&lag2, 1});
  CHECK(lag0 == approx(7.89473 + 4.26120 * 1.0 + 0.71052 * 5.0 + 0.21052 * 3.0));
}
// LCOV_EXCL_STOP
