var layout =
{
  width: 1360,
  height: 900,
  title:
  {
    text: '<b>STEPS Verification</b>',
    font: { size: 24 },
  },
  //paper_bgcolor: '#ccc',
  margin:
  {
    l: 180,
    r: 20,
    t: 40,
    b: 40,
  },
  grid:
  {
    rows: 3,
    cols: 3,
    subplots: [['xy','x2y2', 'x5y5'], ['x3y3', 'x4y4', 'x6y6'], ['', 'x7y7', 'x8y8']],
  },

  // reliability curve
  xaxis:
  {
    title: "Forecast probability",
    domain: [0.0, 0.30],
    range: [0, 1],
    constrain: 'domain',
    nticks: 6,
    hoverformat: '.2f',
  },
  yaxis:
  {
    title: "Observed relative frequency",
    domain: [0.49, 1.0],
    range: [0, 1],
    constrain: 'domain',
    nticks: 6,
    hoverformat: '.2f',
    scaleanchor: 'x',
  },
  // roc curve
  xaxis2:
  {
    title: "False alarm rate",
    domain: [0.35, 0.65],
    range: [0, 1],
    constrain: 'domain',
    nticks: 6,
    hoverformat: '.2f',
  },
  yaxis2:
  {
    title: "Probability of detection",
    domain: [0.49, 1.0],
    constrain: 'domain',
    range: [0, 1],
    nticks: 6,
    hoverformat: '.2f',
    scaleanchor: 'x2',
  },
  // spread reliability
  xaxis5:
  {
    title: "RMSE of Ensemble Mean (mm/hr)",
    domain: [0.70, 1.0],
    constrain: 'domain',
    range: [0.0, spread_max],
    hoverformat: '.2f',
  },
  yaxis5:
  {
    title: "Average Ensemble Spread (mm/hr)",
    domain: [0.49, 1.0],
    constrain: 'domain',
    range: [0.0, spread_max],
    hoverformat: '.2f',
    scaleanchor: 'x5',
  },
  // reliability histogram (sharpness)
  xaxis3:
  {
    title: "Forecast probability",
    domain: [0.0, 0.30],
    constrain: 'domain',
    nticks: 6,
    hoverformat: '.2f',
  },
  yaxis3:
  {
    title: "Forecasts",
    domain: [0.31, 0.41],
    //type: 'log',
    rangemode: 'tozero',
  },
  // CRPS
  xaxis4:
  {
    title: "Forecast lead time (min)",
    domain: [0.35, 0.65],
  },
  yaxis4:
  {
    title: "CRPS",
    domain: [0.31, 0.41],
  },
  // rank histogram
  xaxis6:
  {
    //title: "Forecast lead time (min)",
    domain: [0.70, 1.0],
  },
  yaxis6:
  {
    title: "Relative frequency",
    domain: [0.31, 0.41],
  },
  // RMSE
  xaxis7:
  {
    title: "Forecast lead time (min)",
    domain: [0.35, 0.65],
  },
  yaxis7:
  {
    title: "RMSE",
    domain: [0.07, 0.17],
  },
  // correlation
  xaxis8:
  {
    title: "Forecast lead time (min)",
    domain: [0.70, 1.0],
  },
  yaxis8:
  {
    title: "Avg. Correl.",
    domain: [0.07, 0.17],
    range: [0, 1],
  },

  legend:
  {
    orientation: 'v',
    xanchor: 'left',
    yanchor: 'top',
    x: -0.145,
    y: 0.79,
    bgcolor: '#fff0',
  },
  annotations:
  [
    {
      text: '<b>Reliability Curve</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.15,
      y: 1.00,
      showarrow: false,
    },
    {
      text: '<b>Receiver Operating Characteristic</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.5,
      y: 1.00,
      showarrow: false,
    },
    {
      text: '<b>Spread Reliability</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.85,
      y: 1.00,
      showarrow: false,
    },
    {
      text: '<b>Forecast Probability Histogram</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.15,
      y: 0.43,
      showarrow: false,
    },
    {
      text: '<b>Continuous Ranked Probability Score</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.5,
      y: 0.43,
      showarrow: false,
    },
    {
      text: '<b>Rank Histogram</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.85,
      y: 0.43,
      showarrow: false,
    },
    {
      text: '<b>Root Mean Square Error</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.5,
      y: 0.19,
      showarrow: false,
    },
    {
      text: '<b>Average Correaltion Coefficient</b>',
      font: { size: 16 },
      xref: 'paper',
      yref: 'paper',
      xanchor: 'center',
      x: 0.85,
      y: 0.19,
      showarrow: false,
    },
  ],
  shapes:
  [
    // reliability curve
    {
      type: 'line',
      xref: 'x',
      yref: 'y',
      x0: 0,
      x1: 1,
      y0: 0,
      y1: 1,
      line: { color: 'grey', dash: 'dash' },
    },
    {
      type: 'path',
      xref: 'x',
      yref: 'y',
      path: '',
      fillcolor: 'grey',
      opacity: 0.3,
      layer: 'below',
      line: { color: 'grey' },
      visible: false,
    },
    {
      type: 'line',
      xref: 'x',
      yref: 'y',
      x0: 0,
      x1: 1,
      y0: 0,
      y1: 1,
      line: { color: 'grey', dash: 'dot' },
      visible: false,
    },
    // roc curve
    {
      type: 'line',
      xref: 'x2',
      yref: 'y2',
      x0: 0,
      x1: 1,
      y0: 0,
      y1: 1,
      line: { color: 'grey', dash: 'dash' },
    },
    // spread reliability
    {
      type: 'line',
      xref: 'x5',
      yref: 'y5',
      x0: 0,
      x1: spread_max,
      y0: 0,
      y1: spread_max,
      line: { color: 'grey', dash: 'dash' },
    },
  ],
};

var config_all =
{
  displaylogo: false,
  displayModeBar: false,
};

function set_options(element_id, values)
{
  var sel = document.getElementById(element_id);
  for (var i = 0; i < values.length; ++i)
  {
    var opt = document.createElement('option');
    opt.text = values[i];
    opt.value = i;
    sel.appendChild(opt);
  }
}
function current_compare()
{
  return document.getElementById('sel_cmp').selectedIndex;
}
function update_compare()
{
  var cmp = current_compare();
  document.getElementById('sel_nm_x').style.display = cmp == 0 ? 'none' : '';
  document.getElementById('sel_lt_x').style.display = cmp == 1 ? 'none' : '';
  document.getElementById('sel_th_x').style.display = cmp == 2 ? 'none' : '';
  update_filters();
}
function pick_traces_lt_th(list)
{
  var colors = Plotly.d3.scale.category10();
  var cmp = current_compare();
  var ret = [];
  for (var inm = 0; inm < model_names.length; ++inm)
  {
    if (cmp != 0 && document.getElementById('sel_nm').selectedIndex != inm)
      continue;
    for (var ilt = 0; ilt < lead_times.length; ++ilt)
    {
      if (cmp != 1 && document.getElementById('sel_lt').selectedIndex != ilt)
        continue;
      for (var ith = 0; ith < thresholds.length; ++ith)
      {
        if (cmp != 2 && document.getElementById('sel_th').selectedIndex != ith)
          continue;
        var trace = list[inm][ilt][ith];
        if (cmp == 0)
        {
          trace.name = model_names[inm];
          trace.legendgroup = inm;
          trace.line.color = colors(inm);
          trace.marker.color = colors(inm);
        }
        else if (cmp == 1)
        {
          trace.name = lead_times[ilt] + 'sec';
          trace.legendgroup = ilt;
          trace.line.color = colors(ilt);
          trace.marker.color = colors(ilt);
        }
        else if (cmp == 2)
        {
          trace.name = thresholds[ith] + 'mm/hr';
          trace.legendgroup = ith;
          trace.line.color = colors(ith);
          trace.marker.color = colors(ith);
        }
        ret.push(trace);
      }
    }
  }
  return ret;
}
function pick_traces_lt(list)
{
  var colors = Plotly.d3.scale.category10();
  var cmp = current_compare();
  var ret = [];
  for (var inm = 0; inm < model_names.length; ++inm)
  {
    if (cmp != 0 && document.getElementById('sel_nm').selectedIndex != inm)
      continue;
    for (var ilt = 0; ilt < lead_times.length; ++ilt)
    {
      if (cmp != 1 && document.getElementById('sel_lt').selectedIndex != ilt)
        continue;
      var trace = list[inm][ilt];
      if (cmp == 0)
      {
        trace.name = model_names[inm];
        trace.legendgroup = inm;
        trace.line.color = colors(inm);
        trace.marker.color = colors(inm);
      }
      else if (cmp == 1)
      {
        trace.name = lead_times[ilt] + 'sec';
        trace.legendgroup = ilt;
        trace.line.color = colors(ilt);
        trace.marker.color = colors(ilt);
      }
      else if (cmp == 2)
      {
        trace.name = "";
        trace.legendgroup = "";
        trace.line.color = colors(thresholds.length);
        trace.marker.color = colors(thresholds.length);
      }
      ret.push(trace);
    }
  }
  return ret;
}
function pick_traces(list)
{
  var colors = Plotly.d3.scale.category10();
  var cmp = current_compare();
  var ret = [];
  for (var inm = 0; inm < model_names.length; ++inm)
  {
    if (cmp != 0 && document.getElementById('sel_nm').selectedIndex != inm)
      continue;
    var trace = list[inm];
    if (cmp == 0)
    {
      trace.name = model_names[inm];
      trace.legendgroup = inm;
      trace.line.color = colors(inm);
      trace.marker.color = colors(inm);
    }
    else if (cmp == 1)
    {
      trace.name = "";
      trace.legendgroup = "";
      trace.line.color = colors(lead_times.length);
      trace.marker.color = colors(lead_times.length);
    }
    else if (cmp == 2)
    {
      trace.name = "";
      trace.legendgroup = "";
      trace.line.color = colors(thresholds.length);
      trace.marker.color = colors(thresholds.length);
    }
    ret.push(trace);
  }
  return ret;
}
function update_filters()
{
  var cmp = current_compare();
  var inm = document.getElementById('sel_nm').selectedIndex;
  var ilt = document.getElementById('sel_lt').selectedIndex;
  var ith = document.getElementById('sel_th').selectedIndex;

  // setup plot title
  if (cmp == 0)
    layout.title.text = "Verification of lead time T+" + lead_times[ilt] + ", threshold " + thresholds[ith] + " mm/hr";
  else if (cmp == 1)
    layout.title.text = "Verification of " + model_names[inm] + ", threshold " + thresholds[ith] + " mm/hr";
  else if (cmp == 2)
    layout.title.text = "Verification of " + model_names[inm] + ", lead time T+" + lead_times[ilt];

  // determine skill shading (if compaing models - other modes don't share them)
  if (cmp == 0)
  {
    var cl = rel_climatologies[ilt][ith];
    var y0 = cl / 2;
    var y1 = ((1 - cl) / 2) + cl;
    path = 'M0,0';
    path += ' L0,' + y0;
    path += ' L' + cl + ',' + cl;
    path += ' L' + cl + ',1';
    path += ' L1,1';
    path += ' L1,' + y1;
    path += ' L' + cl + ',' + cl;
    path += ' L' + cl + ',0';
    path += ' L0,0';
    layout.shapes[1].path = path;
    layout.shapes[1].visible = true;

    layout.shapes[2].y0 = cl;
    layout.shapes[2].y1 = cl;
    layout.shapes[2].visible = true;
  }
  else
  {
    layout.shapes[1].visible = false;
    layout.shapes[2].visible = false;
  }
  var traces = pick_traces_lt_th(rel_traces);
  traces = traces.concat(pick_traces_lt_th(rel_hist_traces));
  traces = traces.concat(pick_traces_lt_th(roc_traces));
  traces = traces.concat(pick_traces_lt(spread_traces));
  traces = traces.concat(pick_traces(crps_traces));
  traces = traces.concat(pick_traces_lt(rank_traces));
  traces = traces.concat(pick_traces(rmse_traces));
  traces = traces.concat(pick_traces(rmse_spread_traces));
  traces = traces.concat(pick_traces(correl_traces));
  Plotly.react('plot', traces, layout, config_all);
}

set_options('sel_nm', model_names);
set_options('sel_lt', lead_times);
set_options('sel_th', thresholds);

// if there is only one model, default to comparing thresholds
document.getElementById('sel_cmp').selectedIndex = model_names.length == 1 ? 2 : 0;
update_compare();
update_filters();
