/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "optical_flow.h"
#include "advection.h"
#include "core.h"
#include "trace.h"
#include "unit_test.h"

using namespace steps;

profile optical_flow::prof_optical_flow{"optical_flow"};

auto optical_flow::instantiate(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config) -> unique_ptr<optical_flow>
{
  auto const& type = config["type"].string();
  if (type == "brox_mt")
    return std::make_unique<optical_flow_brox_mt>(grid_shape, grid_resolution, log_zero, config);
  if (type == "brox")
    return std::make_unique<optical_flow_brox>(grid_shape, grid_resolution, log_zero, config);
  if (type == "clg")
    return std::make_unique<optical_flow_clg>(grid_shape, grid_resolution, log_zero, config);
  if (type == "rf3")
    return std::make_unique<optical_flow_rf3>(grid_shape, grid_resolution, log_zero, config);
  if (type == "test")
    return std::make_unique<optical_flow_test>(grid_shape, grid_resolution, log_zero, config);
  throw std::runtime_error{format("Invalid optical flow algorithm type {}", type)};
}

optical_flow_test::optical_flow_test(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config)
  : value_{config.optional("x", 0.0f), config.optional("y", 0.0f)}
{ }

auto optical_flow_test::determine_flow(
      array2f const& from
    , array2f const& to
    , bool use_initial_flow
    , array2f2& flow
    ) const -> void
{
  auto ps = profile::scope{prof_optical_flow};
  flow.fill(value_);
}

// LCOV_EXCL_START
static auto ut_optical_flow(char const* name, char const* conf)
{
  auto resolution = 1.0f;
  auto log_zero = 0.0f;

  auto lag1 = ut_load_reference_grid(1, ut_fast_mode());
  auto shape = lag1.shape();

  auto lag0 = array2f{shape};
  auto rflow = array2f2{shape};
  auto dflow = array2f2{shape};
  auto errmag = array2f{shape};
  auto errang = array2f{shape};

  // generate a reference flow field that we will later try to diagnose from lagged images
  char const* subcase = "";
  SUBCASE("")
  {
    subcase = "translate";
    for (int y = 0; y < shape.y; ++y)
      for (int x = 0; x < shape.x; ++x)
        rflow[y][x] = vec2f{10.0f, -5.0f};
  }
  SUBCASE("")
  {
    subcase = "rotate-10";
    auto angle = degrees_to_radians(10.0);
    auto sintheta = sin(angle);
    auto costheta = cos(angle);
    for (int y = 0; y < shape.y; ++y)
    {
      for (int x = 0; x < shape.x; ++x)
      {
        // convert to a center origin
        auto x0 = x - shape.x * 0.5f;
        auto y0 = y - shape.y * 0.5f;

        // rotate the vector
        auto xr = x0 * costheta - y0 * sintheta;
        auto yr = x0 * sintheta + y0 * costheta;

        // translate origin back to NW corner
        auto xd = xr + shape.x * 0.5f;
        auto yd = yr + shape.y * 0.5f;

        // calculate the vector pointing from x,y to xd,yd
        rflow[y][x].x = xd - x;
        rflow[y][x].y = yd - y;
      }
    }
  }
  CAPTURE(subcase);

  // advect the lag1 image by the reference flow field
  advect(rflow, 0.0f, lag1, lag0);

  // instantiate and run optical flow
  auto optflow = optical_flow::instantiate(shape, resolution, log_zero, configuration::read_string(conf));
  optflow->determine_flow(lag0, lag1, false, dflow);

  // calculate error for each diagnosed flow vector as magnitude and angle error components
  for (auto i = 0; i < dflow.size(); ++i)
  {
    errmag.data()[i] = dflow.data()[i].magnitude() - rflow.data()[i].magnitude();
    errang.data()[i] = dflow.data()[i].angle() - rflow.data()[i].angle();

    // normalize angle error to -180..180
    errang.data()[i] = radians_to_degrees(errang.data()[i]);
    while (errang.data()[i] < -180.0f)
      errang.data()[i] += 360.0f;
    while (errang.data()[i] > 180.0f)
      errang.data()[i] -= 360.0f;
  }

  // ignore a border at the edge of the image for now
  // these are always a little problematic (partially due to radar range limits / second trip rings etc)
  auto border_condition = [&](auto i)
  {
    auto border = 40;
    auto y = i / shape.x;
    auto x = i - (y * shape.x);
    return x >= border && x < shape.x - border && y >= border && y < shape.y - border;
  };
  auto angle_condition = [&](auto i)
  {
    return border_condition(i) && std::abs(dflow.data()[i].magnitude()) > 2.0f;
  };

  // check error distribution for magnitude component
  auto errmagstats = errmag.conditional_mean_stddev(border_condition);
  CHECK(std::abs(errmagstats.first) < 2.5);
  CHECK(errmagstats.second < 5.0);

  // check error distribution for angle component
  auto errangstats = errang.conditional_mean_stddev(angle_condition);
  CHECK(std::abs(errangstats.first) < 2.5);
  CHECK(errangstats.second < 5.0);
}

TEST_CASE("optical_flow_brox")
{
  ut_optical_flow("brox", "type brox");
}
TEST_CASE("optical_flow_brox_lp")
{
  ut_optical_flow("brox", "type brox low_pass 8.0");
}
TEST_CASE("optical_flow_clg" * doctest::may_fail(true) * doctest::skip())
{
  // clg fails the unit test due to being a local method
  // we may remove this as a selectable algorithm in the future
  ut_optical_flow("clg", "type clg");
}
TEST_CASE("optical_flow_rf3" * doctest::may_fail(true))
{
  ut_optical_flow("rf3", "type rf3");
}
// LCOV_EXCL_STOP
