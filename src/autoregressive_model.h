/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "util.h"

namespace steps
{
  /// AR(n) auto-regressive model
  class ar_model
  {
  public:
    /// Constructor
    ar_model();

    /// Setup AR(0) model
    auto setup_ar0(double mean, double stddev) -> void;

    /// Setup AR(1) model using lag-1 auto-correlation coefficient
    auto setup_ar1(double mean, double stddev, double ac) -> void;

    /// Setup AR(2) model using lag-2 and lag-1 auto-correlation coefficients
    auto setup_ar2(double mean, double stddev, double ac1, double ac2) -> void;

    /// Return the current order of the model (the 'n' in AR(n))
    auto order() const { return order_; }

    /// Get the constant offset
    auto c() const { return c_; }

    /// Get noise coefficient
    auto phi0() const { return phi0_; }

    /// Get the lag1 coefficient
    auto phi1() const { return phi1_; }

    /// Get the lag2 coefficient
    auto phi2() const { return phi2_; }

    /// Determine the autocovariance at a given number of time steps (lags) apart
    auto autocovariance(int lag) -> double;

    /// Determine the autocorrelation at a given number of time steps (lags) apart
    auto autocorrelation(int lag) -> double;

    /// Forecast the next state in an AR(0)
    auto forecast_ar0(span<float> lag0, span<float const> noise) -> void;

    /// Forecast the next state in an AR(1) system
    auto forecast_ar1(span<float> lag0, span<float const> noise, span<float const> lag1) -> void;

    /// Forecast the next state in an AR(2) system
    auto forecast_ar2(span<float> lag0, span<float const> noise, span<float const> lag1, span<float const> lag2) -> void;

  private:
    int     order_;   // Order of the model (0, 1 or 2)
    double  c_;       // Constant offset
    double  phi0_;    // Coefficient to scale N(0,1) noise to ensure correct process variance
    double  phi1_;    // Coefficient for the first lag input in AR(1) and AR(2)
    double  phi2_;    // Coefficient for the second lag input in AR(2)
  };

  inline auto ar1_forecast(float mean, float stddev, float autocor, float noise, float lag1) -> float
  {
    auto phi1 = autocor;
    auto phi0 = std::sqrt(stddev * stddev * (1.0f - autocor * autocor));
    auto c = mean * (1.0f - phi1);
    return c + phi0 * noise + phi1 * lag1;
  }

  inline auto ar1_autocovariance(float stddev, float autocor, int lag) -> float
  {
    auto phi1 = autocor;
    auto phi0 = std::sqrt(stddev * stddev * (1.0f - autocor * autocor));
    return std::pow(phi1, lag) * ((phi0 * phi0) / (1.0f - phi1 * phi1));
  }

  inline auto ar1_autocorrelation(float autocor, int lag) -> float
  {
    auto phi1 = autocor;
    return std::pow(phi1, lag);
  }
}
