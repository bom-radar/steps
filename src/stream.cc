/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "stream.h"
#include "advection.h"
#include "core.h"
#include "options.h"
#include "profile.h"
#include "statistics.h"
#include "thread_pool.h"
#include "trace.h"
#include "unit_test.h"

using namespace steps;

static profile prof_cache_wait{"stream::cache ready wait"};

static auto const null_skill_config = "lead_times [] alpha 0"_config;

static auto parse_members_config(configuration const& config, string_view name)
{
  if (auto c = config.find(name))
    return config_array_to_container<array1i>(*c);

  auto ret = array1i{1};
  ret[0] = 0;
  return ret;
}

static auto parse_per_level_array(configuration const& config, string_view name, int levels, float def)
{
  if (auto c = config.find(name))
  {
    auto ret = config_array_to_container<array1f>(*c);
    if (ret.size() != levels)
      throw std::runtime_error{format("Incorrect number of levels in {}", name)};
    return ret;
  }

  auto ret = array1f{levels};
  ret.fill(def);
  return ret;
}

stream::stream(core& c, string name, configuration const& config, bool observation)
  : core_{&c}
  , name_{std::move(name)}
  , reference_time_{config.optional("reference_time", time_point{})}
  , nwp_members_{parse_members_config(config, "members")}
  , input_{input::instantiate(config["input"], c.user_parameters())}
  , bf_reference_time_{config.optional("backfill_reference_time", time_point{})}
  , bf_nwp_members_{parse_members_config(config, "backfill_members")}
  , bf_input_{[&]{ auto b = config.find("backfill"); return b ? input::instantiate(*b, c.user_parameters()) : nullptr; }()}
  , persistent_cache_{persistent_cache::instantiate(
            name_
          , reference_time_
          , bf_reference_time_
          , format(config.optional("cache", string{}), "param"_a = c.user_parameters())
          , config.optional("cache_compression", 0)
          , config.optional("cache_precision_reduction", 0))}
  , stddev_scale_{parse_per_level_array(config, "stddev_scale", c.levels(), 1.0f)}
  , flow_scale_{config["flow_scale"]}
  , psd_smooth_{config["smooth_psd"]}
  , ac_smooth_{config["smooth_autocorrelation"]}
  , skills_halflife_{observation ? 0s : std::chrono::seconds{int(config["skills"]["halflife"])}}
  , skills_spatial_{1, observation ? null_skill_config : config["skills"]}
  , skills_spectral_{c.levels(), observation ? null_skill_config : config["skills"]}
  , skills_motion_{1, observation ? null_skill_config : config["skills"]}
{ }

stream::stream(stream&& rhs)
  : core_{rhs.core_}
  , name_(std::move(rhs.name_))
  , reference_time_{rhs.reference_time_}
  , nwp_members_{std::move(rhs.nwp_members_)}
  , input_{std::move(rhs.input_)}
  , bf_reference_time_{rhs.bf_reference_time_}
  , bf_nwp_members_{std::move(rhs.bf_nwp_members_)}
  , bf_input_{std::move(rhs.bf_input_)}
  , persistent_cache_{std::move(rhs.persistent_cache_)}
  , stddev_scale_{std::move(rhs.stddev_scale_)}
  , flow_scale_{rhs.flow_scale_}
  , psd_smooth_{rhs.psd_smooth_}
  , ac_smooth_{rhs.ac_smooth_}
  , skills_halflife_{rhs.skills_halflife_}
  , skill_updates_{std::move(rhs.skill_updates_)}
  , skills_spatial_{std::move(rhs.skills_spatial_)}
  , skills_spectral_{std::move(rhs.skills_spectral_)}
  , skills_motion_{std::move(rhs.skills_motion_)}
  , cache_{std::move(rhs.cache_)}
{ }

auto stream::load_skills(configuration const& data) -> void
try
{
  auto lock = std::lock_guard<std::mutex>{mut_skills_};
  skill_updates_.clear();
  if (auto c = data.find("spatial"))
    skills_spatial_.load(*c);
  if (auto c = data.find("spectral"))
    skills_spectral_.load(*c);
  if (auto c = data.find("motion"))
    skills_motion_.load(*c);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to load {} skills", name_)});
}

auto stream::store_skills(configuration& data) const -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_skills_};
  skills_spatial_.store(data["spatial"]);
  skills_spectral_.store(data["spectral"]);
  skills_motion_.store(data["motion"]);
}

auto stream::update_skills(int member, time_point time, float spatial, span<float const> spectral, float motion) -> void
{
  // determine the cache key for the desired member/time
  // we keep a record of keys used to update the climatology to prevent duplicate entries
  auto key = cache_key{time, nwp_members_[member % nwp_members_.size()], bf_nwp_members_[member % bf_nwp_members_.size()]};

  // synchronize access to skill climatologies
  auto lock = std::lock_guard<std::mutex>{mut_skills_};
  {
    // ignore this data point if it has been seen before during this run of STEPS
    // this does NOT prevent people running the same forecast twice and updating the climatology
    // again.  that is an external problem for the user to manage.
    if (std::find(skill_updates_.begin(), skill_updates_.end(), key) != skill_updates_.end())
      return;
    skill_updates_.push_back(std::move(key));

    // udpate the climatology for the input lead time
    auto lead_time = time - reference_time_;
    skills_spatial_.update(lead_time, span{&spatial, 1});
    skills_spectral_.update(lead_time, spectral);
    skills_motion_.update(lead_time, span{&motion, 1});
  }

  // trace the updated skills
  // we do this here because it means we only trace new skill values
  if (trace_active(trace_level::log))
  {
    trace(trace_level::log, "{} skills diagnosed for model lead time {}", name_, time - reference_time_);
    trace(trace_level::log, "{} diagnosed spatial skill {}", name_, motion);
    string str;
    for (auto lvl = 0; lvl < spectral.size(); ++lvl)
      str.append(format(" {:6.3f}", spectral[lvl]));
    trace(trace_level::log, "{} diagnosed spectral skills{}", name_, str);
    trace(trace_level::log, "{} diagnosed motion skill {}", name_, motion);
  }
}

auto stream::lookup_skills(int member, time_point time, float& spatial, span<float> spectral, float& motion) const -> void
try
{
  // retrieve the climatology for the input lead time
  auto lock = std::lock_guard<std::mutex>{mut_skills_};
  auto lead_time = time - reference_time_;
  skills_spatial_.lookup(lead_time, span{&spatial, 1});
  skills_spectral_.lookup(lead_time, spectral);
  skills_motion_.lookup(lead_time, span{&motion, 1});
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to determine {} skills for lead time {}", name_, time - reference_time_)});
}

auto stream::notify_active_time_range(time_point from, time_point till) -> void
{
  /* We keep an extra time step at the lower side of the time range. this ensures that the state is available as the
   * 'previous' state when diagnosing the next time step.  Without this we end up writing the state to the persistent
   * cache, deleting it here, then immediately needing to read it back in the next determine_state(). */
  from -= core_->time_step();

  auto lock = std::lock_guard<std::mutex>{mut_cache_};
  if (from <= till)
  {
    // normal case - erase above and below range
    cache_.erase(cache_.begin(), cache_.lower_bound(from));
    cache_.erase(cache_.upper_bound(till), cache_.end());
  }
  else
  {
    // wrap around end to start - erase middle section
    cache_.erase(cache_.upper_bound(till), cache_.lower_bound(from));
  }
}

auto stream::determine_state(int member, time_point time) -> shared_ptr<stream_state const>
try
{
  // generate the unique cache key corresponding to the desired state
  auto key = cache_key{time, nwp_members_[member % nwp_members_.size()], bf_nwp_members_[member % bf_nwp_members_.size()]};

  // find or create an entry in the memory cache
  auto lock = std::unique_lock<std::mutex>{mut_cache_};
  while (true)
  {
    auto res = cache_.try_emplace(key);
    if (res.first->second.first)
    {
      // state is in cache and initialized - return it
      return res.first->second.second;
    }
    else if (!res.second)
    {
      // state is in cache but being initialized by another thread - wait for it
      auto ps = profile::scope{prof_cache_wait};
      cv_ready_.wait(lock);
    }
    else
    {
      // state is not in cache - generate it
      // unlock cache during generation to allow access to other states
      // don't use res after this point since the entry may be deleted by notify_active_time_range() while unlocked
      auto state = std::make_shared<stream_state>(core_->levels(), core_->grid_shape(), core_->fft().shape_c());
      lock.unlock();
      try
      {
        // try to load it from persistent (disk) cache first
        if (!persistent_cache_->read(key, *state))
        {
          // not in persistent cache so run the diagnosis ourselves
          if (diagnose_state_full(member, time, *state))
            // success - write the diagnosed state back to the persistent cache
            persistent_cache_->write(key, *state);
          else
            // failure - reset to ensure we store a nullptr in the cache
            state.reset();
        }
      }
      catch (...)
      {
        lock.lock();
        cache_[key] = state_store::mapped_type{true, nullptr};
        cv_ready_.notify_all();
        throw;
      }
      lock.lock();
      cache_[key] = state_store::mapped_type{true, state};
      cv_ready_.notify_all();
      return state;
    }
  }
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{
      format("Failed to determine state {}/{} for stream {}", time, member, name_)});
}

auto stream::read_and_backfill(int member, time_point time, array2f& field) -> bool
{
  if (!input_->read(reference_time_, nwp_members_[member % nwp_members_.size()], time, field))
    return false;
  if (bf_input_)
  {
    auto buf = array2f{core_->grid_shape()};
    if (!bf_input_->read(bf_reference_time_, bf_nwp_members_[member % bf_nwp_members_.size()], time, buf))
      throw std::runtime_error{format("Backfill for {} not available", time)}; // TODO better error text
    for (auto i = 0; i < field.size(); ++i)
      if (std::isnan(field.data()[i]))
        field.data()[i] = std::isnan(buf.data()[i]) ? 0.0f : buf.data()[i];
  }
  else
    field.replace_nan(0.0f);
  return true;
}

auto stream::check_memory_cache(cache_key const& key) -> shared_ptr<stream_state const>
{
  auto lock = std::lock_guard<std::mutex>{mut_cache_};
  auto iter = cache_.find(key);
  return iter != cache_.end() && iter->second.first ? iter->second.second : nullptr;
}

/* This function is a cut down version of diagnose_state_full() which only fill the stream_state members
 * that are needed to fulfill the roll of a 'previous' state in a call to that function.
 *
 * This is called whenever we need to call diagnose_state_full() but there is no current previous state.
 * Currently the only time this can happen is at the first time step of a member.
 *
 * The members which are filled are:
 * - field        needed for tracking
 * - flow         needed for autocorrelation of flow fields
 * - var_flow     needed for autocorrelation of flow fields
 * - cascade      neeed for autocorrelation of cascade
 * - psd          needed for autocorrelation of psd
 * - var_psd      needed for autocorrelation of psd
 */
auto stream::diagnose_state_cutdown(int member, time_point time, stream_state& state) -> bool
{
  auto lag1_field = array2f{core_->grid_shape()};

  // read and transform the lag1 input field
  if (!read_and_backfill(member, time - core_->time_step(), lag1_field))
    return false;
  core_->transformer().forward(lag1_field);

  // read and transform the lag0 input field
  if (!read_and_backfill(member, time, state.field))
    return false;
  core_->transformer().forward(state.field);

  // decompose to cascade
  decompose(
        core_->fft()
      , core_->bandpass_filters()
      , state.field
      , core_->log_zero()
      , core_->anti_ring_border()
      , state.cascade
      , &state.psd);

  // perform optical flow tracking
  // we always track 'backwards' to ensure vectors are oriented with regard to the later image
  core_->tracker().determine_flow(state.field, lag1_field, false, state.flow);

  // determine variance of flow field
  state.var_flow = variance(state.flow);

  // determine variance of psd at each level
  thread_pool_distribute_loop(state.cascade.size(), [&](int lvl)
  {
    state.var_psd[lvl] = variance_weighted(core_->bandpass_filters()[lvl], state.psd);
  });

  // flag this state as a cold start so we don't use uninitialzed members in time smoothing later on
  state.cold_start = true;

  // warn about cold starts where time smoothing is enabled
  if (psd_smooth_ > 0.0f)
    trace(trace_level::warning, "Cold starting PSD for {}", name_);
  if (ac_smooth_ > 0.0f)
    trace(trace_level::warning, "Cold starting autocorrelations for {}", name_);

  return true;
}

auto stream::diagnose_state_full(int member, time_point time, stream_state& state) -> bool
{
  // read the input field
  if (!read_and_backfill(member, time, state.field))
    return false;

  // linear to log space conversion
  core_->transformer().forward(state.field);

  // decompose to cascade
  decompose(
        core_->fft()
      , core_->bandpass_filters()
      , state.field
      , core_->log_zero()
      , core_->anti_ring_border()
      , state.cascade
      , &state.psd);

  // grab the previous state from our in memory cache, or disk, or by generating a cold start state
  auto prev_key = cache_key{time - core_->time_step(), nwp_members_[member % nwp_members_.size()], bf_nwp_members_[member % bf_nwp_members_.size()]};
  auto pstate = check_memory_cache(prev_key);
  if (!pstate)
  {
    auto ptemp = std::make_shared<stream_state>(core_->levels(), core_->grid_shape(), core_->fft().shape_c());
    if (   !persistent_cache_->read(prev_key, *ptemp)
        && !diagnose_state_cutdown(member, prev_key.time, *ptemp))
      throw std::runtime_error{"Failed to acquire previous state"};
    pstate = std::move(ptemp);
  }

  // time smooth the PSD
  if (psd_smooth_ > 0.0f)
  {
    for (int i = 0; i < state.psd.size(); ++i)
      state.psd.data()[i] = pstate->psd.data()[i] * psd_smooth_ + state.psd.data()[i] * (1.0f - psd_smooth_);
  }

  // optical flow tracking
  /* We track from lag0 'backwards' to lag1 to ensure that our vectors are based at lag0 locations.  We
   * must then negate the vectors to make them point forward to future locations. */
  state.flow.copy(pstate->flow);
  core_->tracker().determine_flow(state.field, pstate->field, true, state.flow);

  // determine standard deviation and lag 1 autocorrelation of flow field
  state.var_flow = variance(state.flow);
  state.ac_flow = covariance_to_correlation(covariance(state.flow, pstate->flow), state.var_flow, pstate->var_flow).magnitude();

  // determine standard deviation and lag 1 autocorrelation of psd
  thread_pool_distribute_loop(state.cascade.size(), [&](int lvl)
  {
    state.var_psd[lvl] = variance_weighted(core_->bandpass_filters()[lvl], state.psd);
    state.ac_psd[lvl] = covariance_to_correlation(
          covariance_weighted(core_->bandpass_filters()[lvl], state.psd, pstate->psd)
        , state.var_psd[lvl]
        , pstate->var_psd[lvl]);
  });

  // advect the lag 1 field to match our target time and decompose
  auto buf_cascade = cascade{core_->levels(), core_->grid_shape()};
#if 1 // advect lag1 field, then decompose
  auto buf_field = array2f{core_->grid_shape()};
  advect(state.flow, core_->log_zero(), pstate->field, buf_field);
  decompose(
        core_->fft()
      , core_->bandpass_filters()
      , buf_field
      , core_->log_zero()
      , core_->anti_ring_border()
      , buf_cascade
      , nullptr
      );
#elif 1 // decompose lag1 field, then advect
  for (auto lvl = 0; lvl < state.cascade.size(); ++lvl)
    advect(state.flow, 0.0f, pstate->cascade[lvl], buf_cascade[lvl]);
#else // calculate correlation directy against decomposed lag1 (no advect)
  for (auto lvl = 0; lvl < state.cascade.size(); ++lvl)
    buf_cascade[lvl].copy(pstate->cascade[lvl]);
#endif

  // determine the process parameters for cascade levels
  /* TODO - if there isn't enough rain to get reliable autocorrelations to use as the 'global' value in non-raining areas
   * then we should fallback to the default autocorrelations from 'core'.  this will need to be passed in to local_statistics() */
  thread_pool_distribute_loop(state.cascade.size(), [&](int lvl)
  {
    auto scalar = local_statistics(buf_cascade[lvl], state.cascade[lvl], core_->parameter_radius(), state.cascade_stats[lvl]);
    local_statistics_post(stddev_scale_[lvl], scalar, state.cascade_stats[lvl]);
    trace(trace_level::verbose, "{} level {} global {:.4f}", name_, lvl, scalar);
  });

  // determine the standard deviation and lag 1 autocorrelation of cascade
  // TODO - if there isn't enough rain use the defaults from core
  if (core_->spatial_skill_levels() > 1)
  {
    auto partial_lag1 = array2f{core_->grid_shape()};
    recompose_partial(buf_cascade, core_->spatial_skill_levels(), partial_lag1);

    auto partial_lag0 = array2f{core_->grid_shape()};
    recompose_partial(state.cascade, core_->spatial_skill_levels(), partial_lag0);

    state.var_cascade = variance(partial_lag0);
    state.ac_cascade = correlation(partial_lag1, partial_lag0);
  }
  else
  {
    state.var_cascade = variance(state.cascade[0]);
    state.ac_cascade = correlation(buf_cascade[0], state.cascade[0]);
  }

  // time smooth the autocorrelations
  if (ac_smooth_ > 0.0f && !pstate->cold_start)
  {
    state.ac_flow = lerp(state.ac_flow, pstate->ac_flow, ac_smooth_);
    for (auto lvl = 0; lvl < state.cascade.size(); ++lvl)
      state.ac_psd[lvl] = lerp(state.ac_psd[lvl], pstate->ac_psd[lvl], ac_smooth_);
    state.ac_cascade = lerp(state.ac_cascade, pstate->ac_cascade, ac_smooth_);
  }

  // apply the systematic bias correction to the flow velocities
  // we calculate autocorrelations before this to ensure they are not penalized by user choice of correction which
  // is aimed at fixing a bias in the flow over the entire forecast not just the firs time step
  if (flow_scale_ != 1.0f)
    state.flow.multiply(flow_scale_);

  // perform any analysis required by the distribution correction mechanism
  state.distcor_analysis = core_->distribution_correction().analyse(state.field);

  return true;
}

// LCOV_EXCL_START
TEST_CASE("parse_members_config")
{
  auto def = parse_members_config(configuration::read_string("foo bar"), "members");
  CHECK(def.size() == 1);
  CHECK(def[0] == 0);

  auto set = parse_members_config(configuration::read_string("members [1 2]"), "members");
  CHECK(set.size() == 2);
  CHECK(set[0] == 1);
  CHECK(set[1] == 2);
}
// LCOV_EXCL_STOP
