/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "configuration.h"
#include "format.h"
#include "unit_test.h"
#include <fstream>
#include <sstream>
#include <system_error>

using namespace steps;

constexpr configuration::top_level_tag configuration::top_level;

configuration const configuration::null;

/* Note: it is critical to return std::istream::int_type here! the reason is that the eof() character is not
 * representable by a char (of course).  We previously returned char here, and it worked for years on all of
 * our x86 platforms.  This was because char on x86 is defined as signed, so eof(-1) was converted to char it
 * remained as a -1, and then later comparisons to eof were valid.  On aarch64 (arm) systems however this
 * failed badly because char is unsigned.  The eof (-1) was converted to 255, which then failed on subsequent
 * conversions to eof again.  Of course, both systems would have been failed if someone tried to read data
 * containing an ascii 255! */
static inline auto skip_whitespace_and_peek(std::istream& in) -> std::istream::int_type
{
  while (true)
  {
    auto next = in.peek();

    if (std::isspace(next))
    {
      in.ignore();
      continue;
    }

    if (next == '#')
    {
      do
      {
        in.ignore();
        next = in.peek();
      } while (next != '\n' && next != std::istream::traits_type::eof());
      in.ignore();
      continue;
    }

    return next;
  }
}

// note: a string is _exclusively_ terminated by a '"' character.  that is, not also by a null.
// this allows the user to safely store binary data in strings if they really want to
static auto parse_string(std::istream& in) -> string
{
  string ret;
  auto c = in.peek();
  if (c == '"')
  {
    in.ignore();
    c = in.get();
    while (c != '"')
    {
      if (c == std::istream::traits_type::eof())
        throw configuration::parse_error{"unterminated string", in};

      if (c != '\\')
        ret.push_back(c);
      else
      {
        switch (c = in.peek())
        {
        case '"': ret.push_back('\"'); break;
        case '\\': ret.push_back('\\'); break;
        case '/': ret.push_back('/'); break;
        case 't': ret.push_back('\t'); break;
        case 'n': ret.push_back('\n'); break;
        case 'v': ret.push_back('\v'); break;
        case 'f': ret.push_back('\f'); break;
        case 'r': ret.push_back('\r'); break;
        default:
          throw configuration::parse_error{"unknown escape sequence", in};
        }
        in.ignore();
      }
      c = in.get();
    }
  }
  else
  {
    while (   !std::isspace(c)
           && c != '#'
           && c != '[' && c != ']'
           && c != '{' && c != '}'
           && c != std::istream::traits_type::eof())
    {
      ret.push_back(c);
      in.ignore();
      c = in.peek();
    }
  }
  return ret;
}

static auto write_string(std::ostream& out, string const& str) -> void
{
  // decide whether to wrap the string in quotes.  we will do so if it contains:
  // - any non-printable characters (whitespace, control or non-ascii)
  // - any of our semantically relevant characters (brackets, comment etc)
  size_t pos = 0;
  while (pos != str.size())
  {
    auto c = str[pos];
    if (!std::isgraph(c) || c == '[' || c == ']' || c == '{' || c == '}' || c == '#')
      break;
    ++pos;
  }
  if (pos != str.size())
  {
    out.put('"');
    size_t from = pos = 0;
    while ((pos = str.find_first_of("\\\"", pos)) != string::npos)
    {
      out.write(str.data() + from, pos - from);
      out.put('\\');
      from = pos++;
    }
    out.write(str.data() + from, str.size() - from);
    out.put('"');
  }
  else if (str.empty())
    out.write("\"\"", 2);
  else
    out.write(str.data(), str.size());
}

configuration::parse_error::parse_error(char const* description, std::istream& in)
  : line_{0}
  , column_{0}
{
  // the clear and check of pos is needed to cope with end of file (when we want a line/column count)
  // and missing files / broken streams etc (when we can't get a line/column count)
  in.clear();
  std::streamoff pos = in.tellg();
  if (pos != -1)
  {
    in.seekg(0, std::ios_base::beg);
    for (decltype(pos) i = 0; i < pos; ++i)
    {
      char c = in.get();
      if (c == std::istream::traits_type::eof())
        break;
      if (c == '\n')
      {
        ++line_;
        column_ = 0;
      }
      else
        ++column_;
    }
    ++line_;
    ++column_;
  }

  what_ = format("{} at line {} column {}", description, line_, column_);
}

auto configuration::parse_error::what() const noexcept -> char const*
{
  return what_.c_str();
}

configuration::type_mismatch::type_mismatch(node_type expected, node_type found)
  : std::runtime_error{format("configuration node type mismatch. expected {} found {}", expected, found)}
  , expected_{expected}
  , found_{found}
{ }

auto configuration::read_file(std::filesystem::path const& path, bool optional) -> configuration
try
{
  std::ifstream file{path};
  if (file.fail())
  {
    if (optional && !std::filesystem::exists(path))
      return {};
    throw std::system_error{errno, std::system_category()};
  }
  return configuration{file};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("failed to read configuration file {}", path.string())});
}

auto configuration::write_file(std::filesystem::path const& path, configuration const& config) -> void
try
{
  std::ofstream file;
  file.exceptions(std::ofstream::failbit | std::ofstream::badbit);
  file.open(path, std::ofstream::trunc);
  config.write(file);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to write configuration file {}", path.string())});
}

auto configuration::read_string(std::string const& str) -> configuration
try
{
  return configuration{std::istringstream{str}};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{"failed to read configuration from string"});
}

configuration::configuration(std::istream& in)
{
  type_ = node_type::object;
  new (&object_) object_type();
  try
  {
    while (skip_whitespace_and_peek(in) != std::istream::traits_type::eof())
    {
      auto key(parse_string(in));
      if (skip_whitespace_and_peek(in) == '}')
        throw parse_error{"key/value pair missing value", in};

      object_.emplace_back(std::move(key), configuration{in, top_level});
    }
  }
  catch (...)
  {
    object_.~vector();
    throw;
  }
}

configuration::configuration(std::istream& in, top_level_tag)
{
  auto next = skip_whitespace_and_peek(in);
  if (next == '{')
  {
    type_ = node_type::object;
    new (&object_) object_type();
    try
    {
      in.ignore();
      while (skip_whitespace_and_peek(in) != '}')
      {
        auto key(parse_string(in));
        if (skip_whitespace_and_peek(in) == '}')
          throw parse_error{"key/value pair missing value", in};
        object_.emplace_back(std::move(key), configuration{in, top_level});
      }
      in.ignore();
    }
    catch (...)
    {
      object_.~vector();
      throw;
    }
  }
  else if (next == '[')
  {
    type_ = node_type::array;
    new (&array_) array_type();
    try
    {
      in.ignore();
      while (skip_whitespace_and_peek(in) != ']')
        array_.emplace_back(in, top_level);
      in.ignore();
    }
    catch (...)
    {
      array_.~vector();
      throw;
    }
  }
  else if (next != std::istream::traits_type::eof())
  {
    type_ = node_type::string;
    new (&string_) string_type(parse_string(in));
  }
  else
    throw parse_error{"unexpected end of file", in};
}

configuration::configuration(configuration const& rhs)
  : type_{rhs.type_}
{
  switch (type_)
  {
  case node_type::null:
    break;
  case node_type::string:
    new (&string_) string_type(rhs.string_);
    break;
  case node_type::array:
    new (&array_) array_type(rhs.array_);
    break;
  case node_type::object:
    new (&object_) object_type(rhs.object_);
    break;
  }
}

configuration::configuration(configuration&& rhs) noexcept
  : type_{rhs.type_}
{
  switch (type_)
  {
  case node_type::null:
    break;
  case node_type::string:
    new (&string_) string_type(std::move(rhs.string_));
    break;
  case node_type::array:
    new (&array_) array_type(std::move(rhs.array_));
    break;
  case node_type::object:
    new (&object_) object_type(std::move(rhs.object_));
    break;
  }
}

configuration::~configuration()
{
  clear();
}

auto configuration::operator=(configuration const& rhs) -> configuration&
{
  clear();
  switch (rhs.type_)
  {
  case node_type::null:
    break;
  case node_type::string:
    new (&string_) string_type(rhs.string_);
    break;
  case node_type::array:
    new (&array_) array_type(rhs.array_);
    break;
  case node_type::object:
    new (&object_) object_type(rhs.object_);
    break;
  }
  type_ = rhs.type_;
  return *this;
}

auto configuration::operator=(configuration&& rhs) noexcept -> configuration&
{
  clear();
  switch (rhs.type_)
  {
  case node_type::null:
    break;
  case node_type::string:
    new (&string_) string_type(std::move(rhs.string_));
    break;
  case node_type::array:
    new (&array_) array_type(std::move(rhs.array_));
    break;
  case node_type::object:
    new (&object_) object_type(std::move(rhs.object_));
    break;
  }
  type_ = rhs.type_;
  return *this;
}

static inline auto put_indent(std::ostream& out, int indent) -> void
{
  for (int i = 0; i < indent; ++i)
    out.put(' ');
}

auto configuration::write(std::ostream& out, int indent) const -> void
{
  if (type_ != node_type::object)
    throw type_mismatch{node_type::object, type_};

  if (object_.empty())
    return;

  for (auto& sub : object_)
  {
    if (sub.second.type_ != node_type::null)
    {
      put_indent(out, indent);
      write_string(out, sub.first);
      out.put(' ');
      sub.second.write(out, top_level, indent);
      out.put('\n');
    }
  }
}

auto configuration::write(std::ostream& out, top_level_tag, int indent) const -> void
{
  switch (type_)
  {
  case node_type::null:
    break;
  case node_type::string:
    write_string(out, string_);
    break;
  case node_type::array:
    if (array_.empty())
      out.write("[]", 2);
    else
    {
      out.write("[ ", 2);
      for (auto& sub : array_)
      {
        sub.write(out, top_level, indent);
        out.put(' ');
      }
      out.put(']');
    }
    break;
  case node_type::object:
    if (object_.empty())
      out.write("{}", 2);
    else
    {
      out.write("{\n", 2);
      for (auto& sub : object_)
      {
        if (sub.second.type_ != node_type::null)
        {
          put_indent(out, indent + 1);
          write_string(out, sub.first);
          out.put(' ');
          sub.second.write(out, top_level, indent + 1);
          out.put('\n');
        }
      }
      put_indent(out, indent);
      out.put('}');
    }
    break;
  }
}

auto configuration::clear() -> void
{
  switch (type_)
  {
  case node_type::null:
    break;
  case node_type::string:
    string_.~basic_string();
    break;
  case node_type::array:
    array_.~vector();
    break;
  case node_type::object:
    object_.~vector();
    break;
  }
  type_ = node_type::null;
}

auto configuration::string() const -> string_type const&
{
  if (type_ == node_type::string)
    return string_;
  throw type_mismatch{node_type::string, type_};
}

auto configuration::array() const -> array_type const&
{
  if (type_ == node_type::array)
    return array_;
  throw type_mismatch{node_type::array, type_};
}

auto configuration::object() const -> object_type const&
{
  if (type_ == node_type::object)
    return object_;
  throw type_mismatch{node_type::object, type_};
}

auto configuration::size() const -> int
{
  if (type_ == node_type::array)
    return array_.size();
  if (type_ == node_type::object)
    return object_.size();
  throw type_mismatch{node_type::array, type_};
}

auto configuration::operator[](size_t i) const -> configuration const&
{
  if (type_ == node_type::array)
    return array_[i];
  if (type_ == node_type::object)
    return object_[i].second;
  throw type_mismatch{node_type::array, type_};
}

auto configuration::at(size_t i) const -> configuration const&
{
  if (type_ == node_type::array)
  {
    if (i < array_.size())
      return array_[i];
    throw std::out_of_range{format("configuration index {} out of range", i)};
  }
  if (type_ == node_type::object)
  {
    if (i < object_.size())
      return object_[i].second;
    throw std::out_of_range{format("configuration index {} out of range", i)};
  }
  throw type_mismatch{node_type::array, type_};
}

auto configuration::operator[](string_view key) const -> configuration const&
{
  if (type_ == node_type::object)
  {
    for (auto& val : object_)
      if (val.first == key)
        return val.second;
    throw std::out_of_range{format("configuration key {} not found", key)};
  }
  throw type_mismatch{node_type::object, type_};
}

auto configuration::operator[](string_view key) -> configuration&
{
  if (type_ == node_type::object)
  {
    for (auto& val : object_)
      if (val.first == key)
        return val.second;

    // if the value is not found, insert a new null value and return it
    object_.emplace_back(key, configuration{});
    return object_.back().second;
  }
  // if the object in uninitialized make us an empty object, insert a new null value and return it
  if (type_ == node_type::null)
  {
    new (&object_) object_type();
    type_ = node_type::object;

    object_.emplace_back(key, configuration{});
    return object_.back().second;
  }
  throw type_mismatch{node_type::object, type_};
}

auto configuration::find(string_view key) const -> configuration const*
{
  if (type_ == node_type::object)
  {
    for (auto& val : object_)
      if (val.first == key)
        return &val.second;
    return nullptr;
  }
  if (type_ == node_type::null)
    return nullptr;
  throw type_mismatch{node_type::object, type_};
}

// LCOV_EXCL_START
#include <sstream>
TEST_CASE("configuration")
{
  std::istringstream ss;
  auto iss = [&](char const* str) -> std::istringstream&
  {
    ss.str(str);
    ss.clear();
    return ss;
  };

  SUBCASE("parse_error")
  {
    iss("foo\nbar");
    ss.ignore(6);
    configuration::parse_error err{"test", ss};
    CHECK(err.line() == 2);
    CHECK(err.column() == 3);
    CHECK(err.what() == "test at line 2 column 3");
  }

  SUBCASE("parse_string")
  {
    CHECK(parse_string(iss("foo")) == "foo");
    CHECK(parse_string(iss("\"foo\"")) == "foo");
    CHECK(parse_string(iss("\"foo bar\"")) == "foo bar");
    CHECK(parse_string(iss("\"\"")) == "");
    CHECK(parse_string(iss("foo#comment")) == "foo");
    CHECK(parse_string(iss("foo[")) == "foo");
    CHECK(parse_string(iss("foo]")) == "foo");
    CHECK(parse_string(iss("foo{")) == "foo");
    CHECK(parse_string(iss("foo}")) == "foo");

    CHECK(parse_string(iss("\"\\\"\"")) == "\"");
    CHECK(parse_string(iss("\"\\\\\"")) == "\\");
    CHECK(parse_string(iss("\"\\/\"")) == "/");
    CHECK(parse_string(iss("\"\\t\"")) == "\t");
    CHECK(parse_string(iss("\"\\n\"")) == "\n");
    CHECK(parse_string(iss("\"\\v\"")) == "\v");
    CHECK(parse_string(iss("\"\\f\"")) == "\f");
    CHECK(parse_string(iss("\"\\r\"")) == "\r");

    CHECK_THROWS(parse_string(iss("\"foo")));         // unterminated string
    CHECK_THROWS(parse_string(iss("\"\\w\"")));       // invalid escape sequence
  }
  SUBCASE("write_string")
  {
    auto write_str = [&](string const& str) -> string
    {
      std::ostringstream oss;
      write_string(oss, str);
      return oss.str();
    };

    CHECK(write_str("") == "\"\"");
    CHECK(write_str("foo") == "foo");
    CHECK(write_str(" foo") == "\" foo\"");
    CHECK(write_str("foo\t\n\"[]{}#\v\f\rbar") == "\"foo\t\n\\\"[]{}#\v\f\rbar\"");
  }
  SUBCASE("main")
  {
    CHECK_THROWS(configuration::read_string("}"));        // key value pair missing value (1)
    CHECK_THROWS(configuration::read_string("a{b}}"));    // key value pair missing value (2)
    CHECK_THROWS(configuration::read_string("a[b{c}}]")); // key value pair missing value (2)
    CHECK_THROWS(configuration::read_string("a{"));       // unexpected end of file

    constexpr char const* test_str =
    R"(a b
    # a comment
    c
    {
      d 123 # another comment
      e [ hello world ]
      f
      [
        {
          blah foo
          happy fun
        }
        {
          blah foo
          happy fun
        }
        { } # empty object
        [ ] # empty array
      ]
      " white space " "as a key"
    })";

    auto conf = configuration::read_string(test_str);

    CHECK(conf["c"].type() == configuration::node_type::object);
    CHECK(conf["c"][1][1].string() == "world");
    CHECK(conf["c"].object()[0].first == "d");
    CHECK(std::stoi(conf["c"]["d"].string()) == 123);
    CHECK(conf["c"]["e"].type() == configuration::node_type::array);
    CHECK(conf["c"]["e"][0].string() == "hello");
    CHECK(conf["c"]["e"].array()[0].string() == "hello");
    CHECK(conf["c"]["e"][1].string() == "world");
    CHECK(conf["c"][" white space "].string() == "as a key");

    // write()
    std::ostringstream oss;
    conf.write(oss);
    CHECK(oss.str() == R"(a b
c {
 d 123
 e [ hello world ]
 f [ {
  blah foo
  happy fun
 } {
  blah foo
  happy fun
 } {} [] ]
 " white space " "as a key"
}
)");

    // clear()

    // string()
    CHECK(conf["a"].string() == "b");
    CHECK_THROWS_AS(conf["c"].string(), configuration::type_mismatch);

    // array()
    CHECK(conf["c"]["e"].array().size() == 2);
    CHECK_THROWS_AS(conf["c"].array(), configuration::type_mismatch);

    // object()
    CHECK(conf["c"].object().size() == 4);
    CHECK_THROWS_AS(conf["a"].object(), configuration::type_mismatch);

    // size()
    REQUIRE(conf["c"]["e"].size() == 2);
    REQUIRE(conf["c"].size() == 4);
    CHECK_THROWS_AS(conf["a"].size(), configuration::type_mismatch);

    // operator[index]
    CHECK(conf["c"]["e"][0].string() == "hello");
    CHECK(conf["c"]["e"][1].string() == "world");
    CHECK(conf[0].string() == "b");
    CHECK(conf["c"][3].string() == "as a key");
    CHECK_THROWS_AS(conf["a"][0], configuration::type_mismatch);

    // at()
    CHECK(conf["c"].at(1).at(1).string() == "world");
    CHECK_THROWS_AS(conf["c"].at(10), std::out_of_range);
    CHECK_THROWS_AS(conf["c"].at(1).at(10), std::out_of_range);
    CHECK_THROWS_AS(conf["a"].at(0), configuration::type_mismatch);

    // operator[string] const
    auto const& cconf = conf;
    CHECK(cconf["a"].string() == "b");
    CHECK(cconf["c"][" white space "].string() == "as a key");
    CHECK_THROWS_AS(cconf["bad"], std::out_of_range);
    CHECK_THROWS_AS(cconf["a"]["bad"], configuration::type_mismatch);

    // operator[string]
    CHECK(conf["new"].type() == configuration::node_type::null);
    CHECK_NOTHROW(conf["new"]["blah"]);
    CHECK(conf["new"].type() == configuration::node_type::object);
    CHECK_THROWS_AS(conf["a"]["bad"], configuration::type_mismatch);

    // find()
    CHECK(conf.find("c") != nullptr);
    CHECK(conf.find("bad") == nullptr);
    CHECK(conf["new"]["blah"].find("bad") == nullptr);
    CHECK_THROWS_AS(conf["a"].find("bad"), configuration::type_mismatch);

    // optional()
    CHECK(conf.optional("bad", 5) == 5);
    CHECK(conf["c"].optional("d", 77) == 123);
    CHECK(conf["c"].optional("blah", configuration{}).type() == configuration::node_type::null);
    CHECK(conf["c"].optional("e", configuration{}).type() == configuration::node_type::array);
    CHECK(conf.optional("a", "foo") == "b");
    CHECK(conf.optional("bad", "foo") == "foo");

    // copy constructor
    configuration copy{conf};
    CHECK(copy["a"].string() == "b");
    CHECK(copy["c"]["e"].size() == 2);
    CHECK(copy["c"].type() == configuration::node_type::object);
    CHECK(std::stoi(copy["c"]["d"].string()) == 123);
    CHECK(copy["c"]["e"].type() == configuration::node_type::array);
    CHECK(copy["c"]["e"].size() == 2);
    CHECK(copy["c"]["e"][0].string() == "hello");
    CHECK(copy["c"]["e"][1].string() == "world");
    CHECK(copy["c"][" white space "].string() == "as a key");

    // operator= const
    CHECK((copy = conf["a"]).string() == "b");
    CHECK((copy = conf["c"]["e"]).array().size() == 2);
    CHECK((copy = conf["c"]["f"][0])["blah"].string() == "foo");
    CHECK((copy = configuration{}).type() == configuration::node_type::null);
  }

  SUBCASE("binary")
  {
    // check that storing of binary data in the strings works (however ill advised it may be)
    string buf1;
    for (int i = 0; i < 1024; ++i)
      buf1.push_back((unsigned char) (i % 256));
    configuration conf;
    conf["foo"] = buf1;

    std::ostringstream oss;
    conf.write(oss);
    configuration conf2{std::istringstream{oss.str()}};

    string buf2 = conf2["foo"].string();
    CHECK(buf1.size() == buf2.size());
    for (int i = 0; i < 1024; ++i)
      CHECK(buf2[i] == buf1[i]);
  }

  SUBCASE("read_file")
  {
    auto path = std::filesystem::temp_directory_path();
    path /= "steps.tmp";

    REQUIRE(!std::filesystem::exists(path));
    CHECK_THROWS(configuration::read_file(path, false));
    CHECK_NOTHROW(configuration::read_file(path, true));

    configuration conf;
    conf["a"] = "b";
    std::ofstream ofs{path};
    CHECK_NOTHROW(conf.write(ofs));
    ofs.flush();

    conf["a"] = "c";
    CHECK(conf["a"].string() == "c");

    CHECK_NOTHROW(conf = configuration::read_file(path, false));
    CHECK(conf["a"].string() == "b");

    std::filesystem::remove(path);
  }
}
// LCOV_EXCL_STOP
