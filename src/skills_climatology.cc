/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "skills_climatology.h"
#include "format.h"
#include "trace.h"
#include "unit_test.h"

using namespace steps;

skills_climatology::skills_climatology(int levels, configuration const& config)
  : lead_times_{config_array_to_container<array1<duration>>(config["lead_times"])}
  , alpha_{config["alpha"]}
  , skills_{vec2i{levels, lead_times_.size()}}
{
  skills_.fill(std::numeric_limits<float>::quiet_NaN());
}

auto skills_climatology::load(configuration const& data) -> void
{
  for (auto lead = 0; lead < lead_times_.size(); ++lead)
  {
    auto& data_lead = data[format("{:g}", lead_times_[lead])].array();
    if (int(data_lead.size()) != levels())
      throw std::runtime_error{"Level count mismatch"};
    for (auto lvl = 0; lvl < levels(); ++lvl)
      skills_[lead][lvl] = float(data_lead[lvl]);
  }
}

auto skills_climatology::store(configuration& data) const -> void
{
  // write the updated file
  // TODO - building a configuration object is quite inefficient at the moment
  for (auto lead = 0; lead < lead_times_.size(); ++lead)
  {
    auto ltstr = format("{:g}", lead_times_[lead]);
    auto& data_lead = (data[ltstr] = configuration::array_type(levels()));
    for (auto lvl = 0; lvl < levels(); ++lvl)
      data_lead[lvl] = configuration{format("{}", skills_[lead][lvl])};
  }
}

auto skills_climatology::update(duration lead_time, span<float const> skills) -> void
{
  if (skills.size() != levels())
    throw std::runtime_error{"Array size mismatch"};

  // short circuit static climatology (EMA alpha is 0.0)
  if (alpha_ <= 0.0f)
    return;

  // find the lead time, ignore this update if we don't track this lead time
  auto lead = int(std::lower_bound(lead_times_.begin(), lead_times_.end(), lead_time) - lead_times_.begin());
  if (lead == lead_times_.size() || lead_times_[lead] != lead_time)
    return;

  // is it a cold start?
  if (std::isnan(skills_[lead][0]))
  {
    trace(trace_level::warning, "Cold start skills climatology for lead time {}", lead_time);
    for (auto lvl = 0; lvl < levels(); ++lvl)
      skills_[lead][lvl] = skills[lvl];
  }
  else
  {
    for (auto lvl = 0; lvl < levels(); ++lvl)
      skills_[lead][lvl] = lerp(skills_[lead][lvl], skills[lvl], alpha_);
  }
}

auto skills_climatology::lookup(duration lead_time, span<float> skills) const -> void
{
  if (skills.size() != levels())
    throw std::runtime_error{"Array size mismatch"};

  // find the (possibly uninitialized) climatology lead time equal or later than the desired one
  auto lead = int(std::lower_bound(lead_times_.begin(), lead_times_.end(), lead_time) - lead_times_.begin());

  // find initialized climatology before desired lead time
  auto il = lead - 1;
  while (il >= 0 && std::isnan(skills_[il][0]))
    --il;

  // find initialized climatology at or after the desired lead time
  auto ir = lead;
  while (ir < lead_times_.size() && std::isnan(skills_[ir][0]))
    ++ir;

  // there were no initialized values either above or below, clamp to the valid value
  if (ir == lead_times_.size())
    ir = il;
  if (il < 0)
    il = ir;

  // interpolate to desired lead time
  if (il < 0)
  {
    // il still invalid so we have no initialized entries
    for (auto lvl = 0; lvl < levels(); ++lvl)
      skills[lvl] = std::numeric_limits<float>::quiet_NaN();
  }
  else if (il == ir)
  {
    // we had one entry, or target time is before first or after last
    for (auto lvl = 0; lvl < levels(); ++lvl)
      skills[lvl] = skills_[il][lvl];
  }
  else
  {
    // interpolate between the two entries
    auto frac = (lead_time - lead_times_[il]).count() / float((lead_times_[ir] - lead_times_[il]).count());
    for (auto lvl = 0; lvl < levels(); ++lvl)
      skills[lvl] = lerp(skills_[il][lvl], skills_[ir][lvl], frac);
  }
}

// LCOV_EXCL_START
TEST_CASE("skills_climatology")
{
  auto conf = "alpha 0.1 lead_times [ 600 1800 3600 ]"_config;
  auto data = R"(
     600 [ 0.99 0.91 0.7 0.3 0.05 ]
    1800 [ 0.97 0.85 0.7 0.3 0.05 ]
    3600 [ 0.95 0.91 0.7 0.3 0.05 ]
  )"_config;

  auto clim = skills_climatology{5, conf};

  array1f skills{5};
  clim.lookup(900s, skills);
  CHECK(std::isnan(skills[0]));
  CHECK(std::isnan(skills[1]));

  for (int i = 0; i < 5; ++i)
    skills[i] = i * 0.2;
  clim.update(1800s, skills);

  clim.lookup(0s, skills);
  CHECK(skills[0] == approx(0.0));
  CHECK(skills[1] == approx(0.2));

  // load some explicit skills
  clim.load(data);

  // store skills back out and check they match
  configuration data_out;
  clim.store(data_out);
  for (auto i = 0; i < data.size(); ++i)
  {
    CHECK(data.object()[i].first == data_out.object()[i].first);
    for (auto j = 0; j < data.object()[i].second.size(); ++j)
      CHECK(data.object()[i].second.array()[j].string() == data_out.object()[i].second.array()[j].string());
  }

  // perform an update of the 1800
  for (int i = 0; i < 5; ++i)
    skills[i] = i * 0.2;
  clim.update(1800s, skills);

  // check the update
  clim.lookup(1800s, skills);
  CHECK(skills[0] == approx(0.873));
  CHECK(skills[3] == approx(0.33));

  // check short lead time clamp
  clim.lookup(0s, skills);
  CHECK(skills[0] == approx(0.99));

  // check interpolated value
  clim.lookup(960s, skills);
  CHECK(skills[0] == approx(0.9549));

  // check long lead time clamp
  clim.lookup(5400s, skills);
  CHECK(skills[0] == approx(0.95));
}
// LCOV_EXCL_STOP
