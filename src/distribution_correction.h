/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include "configuration.h"
#include "transform.h"
#include <any>

/* TODO:
 * - try passing a whole 'stream_state' object to the initialize() and correct_field() functions instead of just
 *   the analysis objects.  this would allow simple distcors to just do their analysis inside the initialize()
 *   function instead of forcing the allocation and I/O of writing values into the stream_state object via the
 *   analyse() function.  also would allow access to other stuff like the per stream flow field.
 */

namespace steps
{
  /// Rain rate distribution correction manager interface
  /** This interface provides a generic way to implement algorithms that adjust the rainfall distribution after each
   *  time step of the forecast process.  This is achieved by passing a modifiably reference to the recomposed rain
   *  field to the class immediately before it is sent to outputs.  Any changes that the distribution correction makes
   *  to the field will be incorporated into the next cycle of the forecast.
   *
   *  The distribution correction method may need to keep some state of its own which should be maintained throughout
   *  the forecast.  This is achieved by allowing the method to return an opaque state object from the initialize()
   *  function that is called before the start of each member.  The state object will then be stored in the forecast
   *  engine and passed back each time a correction is to be made by the correct_field() function.
   *
   *  In addition to internal state, the distribution correction method may depend on the state of the input
   *  observation and model streams.  To facilitate this, the analyse() function is called each time a stream state
   *  is diagnosed.  This function returns another opaque object which holds the results of any analysis that the
   *  distribution correction method wishes to store.  The analysis state of both the observation and model stream
   *  states at model reference time is passed into the initialize() function, while the analysis for all model
   *  input streams is passed into the correct_field() function each time step.  This allows a distribution correction
   *  method to use model inputs to adjust the target rainfall distribution.  Any stream state analysis that
   *  is returned by the analyse() function is written to the stream cache (if enabled).  This allows expensive
   *  analysis steps for model inputs to be performed as part of preprocessing and reused efficiently during
   *  multiple forecasts.
   *
   *  In many cases the type of the state that is produced for each stream by the analyse() function is also suitable
   *  for use as the overall tracked state.  In this case the "analysis" type may simply be reused (i.e. returned by
   *  the initialize() function) to avoid the creation of another struct.
   *
   *  The distribution correction is passed data which is in log space, not in mm/hr.  The reason for this is that it
   *  desireable to correct the distribution before inverse transforming, since this transformation usually involves
   *  truncation of the values at 0 to prevent output of negative rain rates.
   *
   *  Implementing distribution classes are provided with the minimum rain rate, and a reference to the chosen log space
   *  transformation function.  This allows them to perform conversions between log and linear spaces as needed according
   *  to their correction model. */
  class distribution_correction
  {
  public:
    struct analysis
    {
      /// Forward declaration for analysis type id
      enum class type_id : int;

      /// Virtual destructor
      virtual ~analysis() = default;

      /// Provides a unique ID to identify the concrete analysis type
      /** This ID is output to stream cache files, and then is used when reading the cache back in to know what the
       *  correct derived analysis class to create is.  It is critical that the ID returned by each implementing type
       *  is unique, and matches the value found in the static 'read_analysis()' function.  In an ideal world we would
       *  use some RTTI such as std::type_info::hash_code() for this purpose, however the standard makes no guarantee
       *  about the stability of std::type_info - even between subsequent invocations of the same binary!  The value
       *  of 'none' is reserved to mean no analysis and will cause read_analysis() to return nullptr. */
      virtual auto type() const -> type_id = 0;

      /// Write the data of the analysis to an output stream
      virtual auto write(std::ostream& os) const -> void = 0;
    };

  public:
    /// Factory function to create a distribution correction manager
    static auto instantiate(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config
        ) -> unique_ptr<distribution_correction>;

    /// Write analysis state to an output stream
    static auto write_analysis(unique_ptr<analysis> const& a, std::ostream& os) -> void;

    /// Read analysis state from an input stream
    static auto read_analysis(std::istream& is) -> unique_ptr<analysis>;

  public:
    /// Virtual destructor
    virtual ~distribution_correction() = default;

    /// Perform initial analysis of a stream field
    virtual auto analyse(array2f const& field) const -> unique_ptr<analysis>;

    /// Perform the initial analysis of the data to match
    /** The std::any object returned by this function must be passed to all subsequent calls of correct_field(). */
    virtual auto initialize(analysis const* obs, span<analysis const*> mod) const -> std::any;

    /// Correct the passed field
    /** The state object passed _must_ be the one which was returned by initialize() */
    virtual auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void = 0;
  };

  /// Null distribution correction
  class distcor_none : public distribution_correction
  {
  public:
    distcor_none(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config);
    auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void override;
  };

  /// Simple maximum rain rate clamp distribution correction
  class distcor_clamp : public distribution_correction
  {
  public:
    distcor_clamp(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config);
    auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void override;

  private:
    float max_rate_;
    float xform_max_rate_;
  };

  /// Probability matching based distribution correction
  class distcor_probability_matching : public distribution_correction
  {
  public:
    distcor_probability_matching(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config);
    auto analyse(array2f const& field) const -> unique_ptr<analysis> override;
    auto initialize(analysis const* obs, span<analysis const*> mod) const -> std::any override;
    auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void override;

  private:
    struct dc_analysis : analysis
    {
      array1f sorted;

      dc_analysis(int size) : sorted{size} { }
      dc_analysis(std::istream& in);
      auto type() const -> type_id override;
      auto write(std::ostream& os) const -> void override;
    };
    friend auto distribution_correction::read_analysis(std::istream&) -> unique_ptr<analysis>;
  };

  /// Shift and scale based distribution correction
  class distcor_shift_scale : public distribution_correction
  {
  public:
    distcor_shift_scale(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config);
    auto analyse(array2f const& field) const -> unique_ptr<analysis> override;
    auto initialize(analysis const* obs, span<analysis const*> mod) const -> std::any override;
    auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void override;

  private:
    struct dc_analysis : analysis
    {
      float wet_area_ratio;
      float mean_rainfall;

      dc_analysis() = default;
      dc_analysis(std::istream& in);
      auto type() const -> type_id override;
      auto write(std::ostream& os) const -> void override;
    };

  private:
    transform const*  xform_;
    float             xform_min_rain_;
    float             xform_zero_;

    friend auto distribution_correction::read_analysis(std::istream&) -> unique_ptr<analysis>;
  };

  /// Probability matching based distribution with out of domain advection compensation
  class distcor_probability_matching_adv : public distribution_correction
  {
  public:
    distcor_probability_matching_adv(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config);
    auto analyse(array2f const& field) const -> unique_ptr<analysis> override;
    auto initialize(analysis const* obs, span<analysis const*> mod) const -> std::any override;
    auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void override;

  private:
    struct dc_analysis : analysis
    {
      array2f field;

      dc_analysis(array2f field) : field{std::move(field)} { }
      dc_analysis(std::istream& in);
      auto type() const -> type_id override;
      auto write(std::ostream& os) const -> void override;
    };

    struct dc_state
    {
      array2i index;    // advected indexes into target distribution
      array1f target;   // sorted target distribution we are matching to
      dc_state(vec2i shape) : index{shape}, target{index.size()} { }
    };

  private:
    auto reinitialize(array2f const& field, dc_state& dcs) const -> void;

  private:
    float restart_ratio_;

    friend auto distribution_correction::read_analysis(std::istream&) -> unique_ptr<analysis>;
  };

  class distcor_local_war : public distribution_correction
  {
  public:
    distcor_local_war(
          duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , float min_rain_rate
        , transform const& xform
        , configuration const& config);
    auto analyse(array2f const& field) const -> unique_ptr<analysis> override;
    auto initialize(analysis const* obs, span<analysis const*> mod) const -> std::any override;
    auto correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void override;

  private:
    struct dc_analysis : analysis
    {
      array2f war;

      dc_analysis(vec2i shape) : war{shape} { }
      dc_analysis(std::istream& is);
      auto type() const -> type_id override;
      auto write(std::ostream& os) const -> void override;
    };

    struct dc_state
    {
      array2f war;
    };

    struct lerp_lookup_entry
    {
      int   d0;     // x or y index for low side of linear interpolation
      float frac;   // fraction for linear interpolation
    };

    using lerp_lookup = array1<lerp_lookup_entry>;

  private:
    int   box_shape_;       // analysis box minimum side length in grid cells
    float blend_factor_;    // amount of model WAR analysis to blend in each time step
    float xform_min_rain_;
    float xform_zero_;
    vec2i analysis_shape_;

    lerp_lookup lerp_y_;
    lerp_lookup lerp_x_;

    friend auto distribution_correction::read_analysis(std::istream&) -> unique_ptr<analysis>;
  };
}
