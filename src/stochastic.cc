/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "stochastic.h"
#include "core.h"
#include "options.h"
#include "profile.h"
#include "unit_test.h"
#include "trace.h"

using namespace steps;

static profile prof_stochastic_spectrum{"stochastic::generate_spectrum"};
static profile prof_stochastic_spatial{"stochastic::generate_spatial"};

static std::mutex         mut_random_device_;
static std::random_device random_device_;

auto steps::init_random_engine() -> random_engine
{
  auto data = std::array<random_engine::result_type, random_engine::state_size>{};
  {
    auto lock = std::lock_guard<std::mutex>{mut_random_device_};
    std::generate(data.begin(), data.end(), std::ref(random_device_));
  }
  auto seeds = std::seed_seq(data.begin(), data.end());
  return random_engine{seeds};
}

auto steps::seed_random_engine(int seed) -> random_engine
{
  return random_engine(seed);
}

auto steps::generate_noise_spectrum(fourier_transform const& fft, random_engine& rng) -> fourier_transform::array2c
{
  auto ps = profile::scope(prof_stochastic_spectrum);

  // initialize the noise spectrum
  auto spectrum = fourier_transform::array2c{fft.shape_c()};

  if constexpr (opt_spatial_noise)
  {
    // this method generates noise in the spatial domain then transforms it to the spectral domain
    auto dist_N_0_1 = std::normal_distribution<float>{0.0f, 1.0f};
    auto spatial = fourier_transform::array2r{fft.shape_r()};
    auto sqrtxy = sqrt(spatial.shape().x * spatial.shape().y);
    for (auto i = 0; i < spatial.size(); ++i)
      spatial.data()[i] = dist_N_0_1(rng) * sqrtxy;
    fft.execute_forward(spatial, spectrum);
  }
  else
  {
    // this method generates noise directly in the spectral domain
    /* A N(0,1) distribution of a complex random variable is equivalent to N(0,0.5) + iN(0,0.5).  This is the reason that we
     * use the N(0,0.5) distribution for each of the components.  Since the first column of the FFT is not duplicated for the
     * negative frequencies, we actually need to double these values, in which case we have 2 * N(0,1), which is N(0,2) in the
     * complex space, but expaned out to real and imaginary components is N(0,1) + iN(0,0.5). */
    auto dist_N_0_1 = std::normal_distribution<float>{0.0f, 1.0f};
    auto dist_N_0_05 = std::normal_distribution<float>{0.0f, std::sqrt(0.5f)};
    for (auto y = 0; y < spectrum.shape().y; ++y)
    {
      spectrum[y][0].real(dist_N_0_1(rng));
      spectrum[y][0].imag(dist_N_0_1(rng));
      for (auto x = 1; x < spectrum.shape().x; ++x)
      {
        spectrum[y][x].real(dist_N_0_05(rng));
        spectrum[y][x].imag(dist_N_0_05(rng));
      }
    }
  }

  // remove the DC component
  if constexpr (opt_remove_dc_from_stochastic_psd)
  {
    spectrum[0][0].real(0.0f);
    spectrum[0][0].imag(0.0f);
  }

  return spectrum;
}

auto steps::generate_n01_spatial_noise(
      fourier_transform const& fft
    , filter const& psd
    , fourier_transform::array2c const& noise_spectrum
    , array2f& noise_spatial
    ) -> void
{
  auto ps = profile::scope(prof_stochastic_spatial);

  // TODO - allow the user to pass these in to save allocations
  auto band_c = fourier_transform::array2c{fft.shape_c()};
  auto band_r = fourier_transform::array2r{fft.shape_r()};

  /* derive the natural variance of the noise by integrating the PSD at the same time as applying the PSD to the noise
   * spectrum.  we use this variance to normalize the output levels back to N(0,1).  every column of the PSD except the
   * first has a mirrored version that is omitted - for this reason we must double the 'sum' contribution from all columns
   * except the first when determining the normalization factor. */
  double sum = 0.0;
  for (auto y = 0; y < psd.shape().y; ++y)
  {
    band_c[y][0] = noise_spectrum[y][0] * psd[y][0];
    sum += psd[y][0] * psd[y][0];
    for (auto x = 1; x < psd.shape().x; ++x)
    {
      band_c[y][x] = noise_spectrum[y][x] * psd[y][x];
      sum += 2 * psd[y][x] * psd[y][x];
    }
  }
  float stddev = std::sqrt(sum);

  // transform from complex to real
  fft.execute_inverse(band_c, band_r);

  // transfer top right corner of spatial domain to cascade and normalize level to N(0,1)
  for (int y = 0; y < noise_spatial.shape().y; ++y)
    for (int x = 0; x < noise_spatial.shape().x; ++x)
      noise_spatial[y][x] = band_r[y][x] / stddev;
}

#if 0
stochastic::stochastic(fourier_transform const& fft, filter_bank const& filters, int seed, bool spatial)
  : fft_{&fft}
  , filters_{&filters}
  , spatial_{spatial}
  , random_engine_{seed_mt19937_engine(seed)}
  , dist_N_0_1_{0.0f, 1.0f}
  , dist_N_0_05_{0.0f, std::sqrt(0.5f)}
{ }

auto stochastic::generate_noise(fourier_transform::array2c const& noise, filter const& psd_l, filter const& psd_h, int crossover_level, cascade& cascade) -> void
{
  auto band_c = fourier_transform::array2c{fft_->shape_c()};
  auto band_r = fourier_transform::array2r{fft_->shape_r()};

  // apply each bandpass and inverse FFT to generate the levels
  for (int lvl = 0; lvl < cascade.size(); ++lvl)
  {
    auto& level = cascade[lvl];
    auto& bandpass = (*filters_)[lvl];
    auto& psd = lvl < crossover_level ? psd_l : psd_h;

    /* derive the natural variance of the noise by integrating the PSD at the same time as applying the PSD/bandpass
     * to the noise spectrum.  we use this variance to normalize the output levels back to N(0,1).  every column of
     * the PSD except the first has a mirrored version that is omitted - for this reason we must double the 'sum'
     * contribution from all columns except the first when determining the normalization factor. */
    double sum = 0.0;
    for (auto y = 0; y < bandpass.shape().y; ++y)
    {
      auto val = bandpass[y][0] * psd[y][0];
      band_c[y][0] = noise[y][0] * val;
      sum += val * val;
      for (auto x = 1; x < bandpass.shape().x; ++x)
      {
        val = bandpass[y][x] * psd[y][x];
        band_c[y][x] = noise[y][x] * val;
        sum += 2 * val * val;
      }
    }
    float stddev = std::sqrt(sum);

    // transform from complex to real
    fft_->execute_inverse(band_c, band_r);

    // transfer top right corner of spatial domain to cascade and normalize level to N(0,1)
    for (int y = 0; y < level.shape().y; ++y)
      for (int x = 0; x < level.shape().x; ++x)
        level[y][x] = band_r[y][x] / stddev;
  }
}

/* The way to verify this is to take a long time series of stochastic fields which have not been normalized and calculate
 * their standard deviations (their mean should be naturally zero).  This should be done with hundreds of instantiations since
 * the low frequency levels of the cascade take a long time to stabilize.  The standard deviations derived in this way should
 * match very closely the ones which have been calculated in this loop.
 *
 * We did this manually and checked that the stddevs derived from our PSD are extremely good matches for those diagnosed from a
 * long time series (100s) of unnormalized noise cascades.
 *
 * This was verified to work with both the 'spatial noise' method where generate N(0,1) in space and decompose it, and the
 * 'spectral noise' method where we generate N(0,1) directly in the spectrum.  The spatial method naturally results in the
 * same shape of stddevs with level, but scaled - the multiplication by sqrt(x*y) seems to fix this, ensuring that you can
 * switch between methods and stil get the same result.
 *
 * Note that it is important to account for the fact that all columns other than the first one in the spectrum have a mirrored
 * value which is omitted from the array.  We scale the random vector magnitudes by (1/sqrt(2)) to account for this.  Experimental
 * work shows that doing this ensures the stddev distributions then scale in exactly the same way as the spatial noise method
 * with the benefit of saving a fourier transform.
 *
 * Our unit test ensures the whole system is working in harmony by running a long time series of noise and checking that every
 * level has a mean of 0 and standard deviation of 1. */

// LCOV_EXCL_START
auto ut_stochastic(bool spatial)
{
  struct online_stats
  {
    long count = 0;
    double mean = 0;
    double m2 = 0;
  };

  auto shape = vec2i{256, 256};
  auto levels = 5;

  auto fft = fourier_transform{shape};
  auto filters = filter_bank{fft, 1.0f, levels, 1.0f, {}};

  // setup a parametric PSD that scales nicely with frequency
  auto psd = make_parametric_psd(fft, 0.5, 32.0, 1.8, 2.4);

  // setup our stochastic generator
  auto stoch = stochastic{fft, filters, 25, spatial};

  // generate many realizations of the stochastic cascade and keep running statistics
  auto iters = ut_fast_mode() ? 5 : 300;
  auto noise = cascade{levels, shape};
  auto stats = vector<online_stats>(levels);
  for (auto iter = 0; iter < iters; ++iter)
  {
    stoch.generate_noise(psd, psd, 0, noise);

    for (int lvl = 0; lvl < levels; ++lvl)
    {
      auto& s = stats[lvl];
      for (int i = 0; i < noise[lvl].size(); ++i)
      {
        s.count++;
        auto d1 = noise[lvl].data()[i] - s.mean;
        s.mean += d1 / s.count;
        auto d2 = noise[lvl].data()[i] - s.mean;
        s.m2 += d1 * d2;
      }
    }
  }

  // check that each level has a mean of 0 and standard deviation of 1
  for (int lvl = 0; lvl < levels; ++lvl)
  {
    auto mean = stats[lvl].mean;
    auto stddev = std::sqrt(stats[lvl].m2 / stats[lvl].count);

    CHECK(std::abs(mean) < 0.001);
    CHECK(std::abs(stddev - 1.0) < 0.05);
  }
}
TEST_CASE("stochastic")
{
  SUBCASE("spectral") { ut_stochastic(false); }
  SUBCASE("spatial")  { ut_stochastic(true); }
}
// LCOV_EXCL_STOP
#endif
