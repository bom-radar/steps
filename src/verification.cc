/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "verification.h"
#include "cf.h"
#include "io_netcdf.h"
#include "thread_pool.h"
#include "trace.h"
#if STEPS_WITH_AWS
#include "aws.h"
#endif

using namespace steps;

static constexpr auto nodata = -999.0f;

template <typename T>
auto parse_config_array(configuration::array_type const& config)
{
  array1<T> ret(config.size());
  for (auto i = 0; i < ret.size(); ++i)
    ret[i] = parse<T>(config[i].string());
  return ret;
}

static verification_io::factory verification_io_factory_ = [](
      verification const& c
    , configuration const& config
    , metadata const& user_parameters
    ) -> unique_ptr<verification_io>
{
  auto& type = config["type"].string();
  try
  {
    if (type == "netcdf")
    {
      switch (determine_uri_scheme(config["path"].string()))
      {
      case uri_scheme::none:
      case uri_scheme::file:
        // copes with plain path or file schema uri
        return std::make_unique<verification_io_netcdf_local>(c, config, user_parameters);
      case uri_scheme::s3:
#if STEPS_WITH_AWS
        return std::make_unique<aws::verification_io_netcdf_s3>(c, config, user_parameters);
#else
        throw std::runtime_error{"AWS support disabled"};
#endif
      default:
        throw std::runtime_error{"Unsupported URI schema"};
      }
    }
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to configure verification output type {}", type)});
  }
  throw std::runtime_error{format("Unknown verification output type {}", type)};
};

auto verification_io::set_factory(factory function) -> void
{
  verification_io_factory_ = function;
}

auto verification_io::instantiate(
      verification const& c
    , configuration const& config
    , metadata const& user_parameters
    ) -> unique_ptr<verification_io>
{
  return verification_io_factory_(c, config, user_parameters);
}

verification::threshold_scores::threshold_scores(int roc_points, int reliability_bins)
  : roc{roc_points}
  , reliability{reliability_bins}
{ }

verification::lead_time_scores::lead_time_scores(
      int members
    , vec2i grid_shape
    , int spread_bins
    , float max_error
    , int num_thresholds
    , int roc_points
    , int reliability_bins)
  : sample_count{0}
  , rank{members}
  , crps{grid_shape}
  , spread{spread_bins, max_error}
{
  thresholds.reserve(num_thresholds);
  for (auto i = 0; i < num_thresholds; ++i)
    thresholds.emplace_back(std::make_unique<threshold_scores>(roc_points, reliability_bins));
}

verification::verification(configuration const& config)
  : user_parameters_{parse_metadata(config.optional("user_parameters", configuration{}), metadata{})}
  , model_name_{config["model_name"]}
  , minimum_rain_rate_{config["minimum_rain_rate"]}
  , border_width_{config["border_width"]}
  , grid_shape_{int(config["grid_cols"]), int(config["grid_rows"])}
  , members_{[&]()
    {
      auto ret = array1i{int(config["members"])};
      auto offset = int(config["member_offset"]);
      for (auto i = 0; i < ret.size(); ++i)
        ret[i] = offset + i;
      return ret;
    }()}
  , lead_times_{parse_config_array<duration>(config["lead_times"].array())}
  , thresholds_{parse_config_array<float>(config["thresholds"].array())}
  , spread_bins_{config["spread_reliability_bins"]}
  , max_error_{config["spread_reliability_max_error"]}
  , roc_points_{members_.size()}
  , reliability_bins_{members_.size() + 1}
  , input_obs_{input::instantiate(config["observations"], user_parameters_)}
  , input_ens_{input::instantiate(config["ensembles"], user_parameters_)}
  , output_{verification_io::instantiate(*this, config["output"], user_parameters_)}
  , scores_{construct_scores()}
{ }

auto verification::verify_period(time_point from, time_point till, duration period) -> void
{
  // queue all of the observation times we want to verify
  for (auto time = from; time < till; time += period)
    queue_observations_.emplace_back(time);

  threads_idle_ = 0;
  fatal_error_ = nullptr;

  // distribute our verification tasks over the thread pool
  /* TODO this still works on the original concept where we have one thread per verification 'engine'.
   * since we now have the thread_pool capability it would have been simpler to just enqueue all the
   * work needed and let the thread pool churn through it. */
  auto thread_count = thread_pool_size() + 1;
  thread_pool_distribute_loop(thread_count, [&](int i) { thread_verify(thread_count); });

  // if an exception occurred in one of our processing threads rethrow it to the user
  if (fatal_error_)
    std::rethrow_exception(fatal_error_);

  // write our new state to the output file
  output_->write(from, till, scores_);
}

auto verification::aggregate_results(vector<string> const& paths) -> void
{
  // buffers for time and scores of an individual input
  time_point from, till;
  auto scores_in = construct_scores();

  // read and aggregate each score
  auto total_from = time_point::max();
  auto total_till = time_point::min();
  for (auto& path : paths)
  {
    if (!output_->read(path, from, till, scores_in))
    {
      trace(trace_level::warning, "Verification record {} not found, ignored for aggregation", path);
      continue;
    }

    for (auto ilead = 0; ilead < lead_times_.size(); ++ilead)
    {
      scores_[ilead]->sample_count += scores_in[ilead]->sample_count;
      scores_[ilead]->rmse.aggregate(scores_in[ilead]->rmse);
      scores_[ilead]->correlation.aggregate(scores_in[ilead]->correlation);
      scores_[ilead]->rank.aggregate(scores_in[ilead]->rank);
      scores_[ilead]->crps.aggregate(scores_in[ilead]->crps);
      scores_[ilead]->spread.aggregate(scores_in[ilead]->spread);
      for (auto ith = 0; ith < thresholds_.size(); ++ith)
      {
        scores_[ilead]->thresholds[ith]->roc.aggregate(scores_in[ilead]->thresholds[ith]->roc);
        scores_[ilead]->thresholds[ith]->reliability.aggregate(scores_in[ilead]->thresholds[ith]->reliability);
      }
    }
    total_from = std::min(from, total_from);
    total_till = std::max(till, total_till);
  }

  // output our final file
  output_->write(total_from, total_till, scores_);
}

auto verification::thread_verify(int thread_count) -> void
try
{
  // holders for a single request just to let us use splice() to save on list node allocations
  list<job_observation> cur_observation;
  list<job_lead_time>   cur_lead_time;
  list<job_threshold>   cur_threshold;
  list<job_score>       cur_score;

  auto lock = std::unique_lock<std::mutex>{mut_queues_};
  while (!fatal_error_)
  {
    if (!queue_scores_.empty())
    {
      cur_score.splice(cur_score.end(), queue_scores_, queue_scores_.begin());
      lock.unlock();
      process_job(cur_score.front());
      lock.lock();
      cur_score.clear();
    }
    else if (!queue_thresholds_.empty())
    {
      cur_threshold.splice(cur_threshold.end(), queue_thresholds_, queue_thresholds_.begin());
      lock.unlock();
      auto jobs = process_job(cur_threshold.front());
      lock.lock();
      cur_threshold.clear();
      if (!jobs.empty())
      {
        queue_scores_.splice(queue_scores_.end(), jobs);
        cv_queues_.notify_all();
      }
    }
    else if (!queue_lead_times_.empty())
    {
      cur_lead_time.splice(cur_lead_time.end(), queue_lead_times_, queue_lead_times_.begin());
      lock.unlock();
      auto jobs = process_job(cur_lead_time.front());
      lock.lock();
      if (!jobs.empty())
      {
        queue_thresholds_.splice(queue_thresholds_.end(), jobs);
        cv_queues_.notify_all();
      }
      cur_lead_time.clear();
    }
    else if (!queue_observations_.empty())
    {
      cur_observation.splice(cur_observation.end(), queue_observations_, queue_observations_.begin());
      lock.unlock();
      auto jobs = process_job(cur_observation.front());
      lock.lock();
      cur_observation.clear();
      if (!jobs.empty())
      {
        queue_lead_times_.splice(queue_lead_times_.end(), jobs);
        cv_queues_.notify_all();
      }
    }
    else
    {
      // nothing currently avaialble in the queue, mark our thread as idle
      ++threads_idle_;

      // if all threads are idle then we can all terminate and wrap it up
      if (threads_idle_ >= thread_count)
      {
        cv_queues_.notify_all();
        break;
      }

      // there's potentially more work to do so just idle until something comes along
      cv_queues_.wait(lock);

      // we are awake again, revoke our idle status and go look for more work
      --threads_idle_;
    }
  }
}
catch (...)
{
  auto lock = std::lock_guard<std::mutex>{mut_queues_};
  if (!fatal_error_)
    fatal_error_ = std::current_exception();
  threads_idle_ = thread_count;
  cv_queues_.notify_all();
}

auto verification::process_job(job_observation const& req) -> list<job_lead_time>
{
  auto ret = list<job_lead_time>{};
  auto obs = std::make_shared<array2f>(grid_shape_);

  trace(trace_level::log, "Commencing verification for time {}", req.time);

  // load the observation data
  try
  {
    if (!input_obs_->read(req.time, 0, req.time, *obs))
    {
      trace(trace_level::warning, "No observation available for time {}", req.time);
      return ret;
    }
  }
  catch (std::exception& err)
  {
    trace(trace_level::error, "Failed to read observation for time {}: {}", req.time, err);
    return ret;
  }

  // exclude border area (if desired)
  mask_border(*obs);

  // mask the observations (if desired)
  // TODO

  // build list of lead times to process against this observation
  for (auto ilead = 0; ilead < lead_times_.size(); ++ilead)
    ret.emplace_back(req.time, obs, ilead);

  return ret;
}

auto verification::process_job(job_lead_time const& req) -> list<job_threshold>
{
  auto ret = list<job_threshold>{};
  auto ref_time = req.time - lead_times_[req.ilead];

  // load the member data
  auto ens = std::make_shared<member_store>();
  ens->reserve(members_.size());
  for (auto imember = 0; imember < members_.size(); ++imember)
    ens->emplace_back(grid_shape_);
  try
  {
    if (!input_ens_->read(ref_time, members_, req.time, *ens))
    {
      trace(trace_level::warning, "No forecast available for time {} with lead time {}", req.time, lead_times_[req.ilead]);
      return ret;
    }
  }
  catch (std::exception& err)
  {
    trace(trace_level::error, "Failed to read forecast for time {} with lead time {}: {}", req.time, lead_times_[req.ilead], err);
    return ret;
  }

  // remove observations where the observation and every member is exactly 0
  auto cond_obs = std::make_shared<array2f>(grid_shape_);
  condition_active_cells(*req.obs, *ens, *cond_obs);

  // build list of scores to process agaisnt this lead time
  // scores 0..4 are our deterministic scores, scores 5..(thresholds+5) are our 'pre threshold' groups of scores
  for (auto iscore = 0; iscore < 5 + thresholds_.size(); ++iscore)
    ret.emplace_back(req.ilead, ens, cond_obs, iscore);

  return ret;
}

auto verification::process_job(job_threshold const& req) -> list<job_score>
{
  auto ret = list<job_score>{};

  switch (req.iscore)
  {
  case 0:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->mut_rmse};
      scores_[req.ilead]->rmse.update(*req.ens, *req.cond_obs);
      scores_[req.ilead]->sample_count++;
    }
    break;
  case 1:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->mut_correlation};
      scores_[req.ilead]->correlation.update(*req.ens, *req.cond_obs);
    }
    break;
  case 2:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->mut_rank};
      scores_[req.ilead]->rank.update(*req.ens, *req.cond_obs);
    }
    break;
  case 3:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->mut_crps};
      scores_[req.ilead]->crps.update(*req.ens, *req.cond_obs);
    }
    break;
  case 4:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->mut_spread};
      scores_[req.ilead]->spread.update(*req.ens, *req.cond_obs);
    }
    break;
  default:
    {
      auto ithreshold = req.iscore - 5;

      auto events = std::make_shared<array2<event_occurrence>>(grid_shape_);

      // generate the PoPs for this threshold
      auto pops = std::make_shared<ensemble_probability>(grid_shape_);
      for (auto imember = 0; imember < members_.size(); ++imember)
      {
        generate_occurrence_field_high_detect((*req.ens)[imember], thresholds_[ithreshold], *events);
        pops->add_member(*events);
      }
      pops->finalize();

      // generate the event occurrence flags for the observation
      generate_occurrence_field_high_detect(*req.cond_obs, thresholds_[ithreshold], *events);

      // build list of scores to process against this lead time / threshold combination
      for (auto iscore = 0; iscore < 2; ++iscore)
        ret.emplace_back(req.ilead, ithreshold, events, pops, iscore);
    }
  }

  return ret;
}

auto verification::process_job(job_score const& req) -> void
{
  switch (req.iscore)
  {
  case 0:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->thresholds[req.ithreshold]->mut_roc};
      scores_[req.ilead]->thresholds[req.ithreshold]->roc.update(*req.events, req.pops->probabilities());
    }
    break;
  case 1:
    {
      auto lock = std::lock_guard<std::mutex>{scores_[req.ilead]->thresholds[req.ithreshold]->mut_reliability};
      scores_[req.ilead]->thresholds[req.ithreshold]->reliability.update(*req.events, req.pops->probabilities());
    }
    break;
  default:
    throw std::runtime_error{"Invalid score index"};
  }
}

auto verification::construct_scores() const -> score_store
{
  score_store ret;
  ret.reserve(lead_times_.size());
  for (auto ilt = 0; ilt < lead_times_.size(); ++ilt)
    ret.push_back(std::make_unique<lead_time_scores>(
            members_.size()
          , grid_shape_
          , spread_bins_
          , max_error_
          , thresholds_.size()
          , roc_points_
          , reliability_bins_));
  return ret;
}

auto verification::mask_border(array2f& obs) const -> void
{
  if (border_width_ == 0)
    return;

  for (auto y = 0; y < border_width_; ++y)
    for (auto x = 0; x < grid_shape_.x; ++x)
      obs[y][x] = std::numeric_limits<float>::quiet_NaN();

  for (auto y = border_width_; y < grid_shape_.y - border_width_; ++y)
  {
    for (auto x = 0; x < border_width_; ++x)
      obs[y][x] = std::numeric_limits<float>::quiet_NaN();
    for (auto x = grid_shape_.x - border_width_; x < grid_shape_.x; ++x)
      obs[y][x] = std::numeric_limits<float>::quiet_NaN();
  }

  for (auto y = grid_shape_.y - border_width_; y < grid_shape_.y; ++y)
    for (auto x = 0; x < grid_shape_.x; ++x)
      obs[y][x] = std::numeric_limits<float>::quiet_NaN();
}

// TODO - use a very negative number for nodata instead of NaN
//      - this will allow simplification to a simple '<' test
auto verification::condition_active_cells(array2f const& obs, member_store const& ens, array2f& cond_obs) const -> void
{
  for (auto i = 0; i < obs.size(); ++i)
  {
    cond_obs.data()[i] = obs.data()[i];

    // observation is nan
    if (std::isnan(obs.data()[i]))
      continue;

    // check if ensemble is nan at any member
    // this can happen due to reprojection/masking of the ensemble, or just bad model output
    size_t j = 0;
    for (; j < ens.size(); ++j)
      if (std::isnan(ens[j].data()[i]))
        break;
    if (j < ens.size())
    {
      cond_obs.data()[i] = std::numeric_limits<float>::quiet_NaN();
      continue;
    }

    // if we have activity in the observation, include it
    if (obs.data()[i] > minimum_rain_rate_)
      continue;

    // if we have activity in any ensemble member, include it
    for (j = 0; j < ens.size(); ++j)
      if (ens[j].data()[i] > minimum_rain_rate_)
        break;
    if (j == ens.size())
      cond_obs.data()[i] = std::numeric_limits<float>::quiet_NaN();
  }
}

verification_io_netcdf::verification_io_netcdf(verification const& c, configuration const& config)
  : c_{&c}
  , metadata_{parse_metadata(config.optional("metadata", configuration{}), c.user_parameters())}
  , projection_{nc::get_grid_reference(metadata_)}
{ }

auto verification_io_netcdf::write_impl(
      time_point from
    , time_point till
    , verification::score_store const& scores
    , nc::file& fstats
    ) -> void
{
  // netcdf mutex must be locked by caller

  fstats.att_set("Conventions", "CF-1.7");
  fstats.att_set("source", format("STEPS {} ({})", steps_version(), steps_source_ref()));
  fstats.att_set("history", format("{:%a %b %d %H:%M:%S %Y}: {}", clock::now(), get_command_line_string()));
  write_metadata_as_attributes(metadata_, fstats);

  // create our dimensions
  auto [dy, dx, grid_mapping_name] = nc::create_spatial_dimensions(fstats, c_->grid_shape(), projection_);
  auto& dtim = fstats.create_dimension("lead_time", c_->lead_times().size());
  auto& vtim = fstats.create_variable("lead_time", nc::data_type::i32, {&dtim});
  vtim.att_set("units", "seconds");
  vtim.att_set("long_name", "Forecast lead time");
  auto& dthr = fstats.create_dimension("threshold", c_->thresholds().size());
  auto& vthr = fstats.create_variable("threshold", nc::data_type::f32, {&dthr});
  vthr.att_set("long_name", "Observation threshold");
  vthr.att_set("units", "kg-2 h-1");
  auto& drnk = fstats.create_dimension("rank", c_->members().size() + 1);
  auto& droc = fstats.create_dimension("roc_probability", c_->roc_points());
  auto& vroc = fstats.create_variable("roc_probability", nc::data_type::f32, {&droc});
  vroc.att_set("long_name", "Probability threshold");
  vroc.att_set("units", "1");
  auto& drel = fstats.create_dimension("reliability_probability", c_->reliability_bins());
  auto& vrel = fstats.create_variable("reliability_probability", nc::data_type::f32, {&drel});
  vrel.att_set("long_name", "Forecast probability");
  vrel.att_set("units", "1");
  vrel.att_set("comment", "value represents middle of probability bin");
  auto& dspr = fstats.create_dimension("spread_rmse", c_->spread_bins());
  auto& vspr = fstats.create_variable("spread_rmse", nc::data_type::f32, {&dspr});
  vspr.att_set("long_name", "RMSE of Ensemble Mean");
  vspr.att_set("units", "kg-2 h-1");
  vspr.att_set("maximum_error", c_->max_error());

  // create our variables
  auto& vmodel = fstats.create_variable("model", nc::data_type::string);
  vmodel.att_set("long_name", "Name of model being verified");

  auto& vfrom = fstats.create_variable("from", nc::data_type::i64);
  vfrom.att_set("long_name", "Start of observation period used for verification");
  vfrom.att_set("units", "seconds since 1970-01-01 00:00:00 UTC");

  auto& vtill = fstats.create_variable("till", nc::data_type::i64);
  vtill.att_set("long_name", "End of observation period used for verification");
  vtill.att_set("units", "seconds since 1970-01-01 00:00:00 UTC");

  auto& vsample_count = fstats.create_variable("sample_count", nc::data_type::i64, {&dtim});
  vsample_count.att_set("long_name", "Total number of forecast/observation pairs used in verification");
  vsample_count.att_set("units", "1");

  auto& vmin_obs = fstats.create_variable("minimum_value", nc::data_type::f32);
  vmin_obs.att_set("long_name", "Minimum observation/forecast value needed to use pair in verification");
  vmin_obs.att_set("units", "kg m-2 h-1");

  auto& vborder = fstats.create_variable("border_width", nc::data_type::f32);
  vborder.att_set("long_name", "Width of border at domain edge excluded from verification (in grid cells)");

  auto& vrmse_ms_error = fstats.create_variable("rmse_ms_error", nc::data_type::f64, {&dtim});
  vrmse_ms_error.att_set("long_name", "Mean square error");
  auto& vrmse_ms_spread = fstats.create_variable("rmse_ms_spread", nc::data_type::f64, {&dtim});
  vrmse_ms_spread.att_set("long_name", "Mean square spread");
  auto& vrmse_count = fstats.create_variable("rmse_count", nc::data_type::i64, {&dtim});
  vrmse_count.att_set("long_name", "Number of samples in RMSE calculation");
  auto& vrmse_rmse = fstats.create_variable("rmse_rmse_ens_mean", nc::data_type::f32, {&dtim});
  vrmse_rmse.att_set("long_name", "RMSE of Ensemble Mean");
  vrmse_rmse.att_set("units", "kg m-2 h-1");
  auto& vrmse_rms_spread = fstats.create_variable("rmse_rms_spread", nc::data_type::f32, {&dtim});
  vrmse_rms_spread.att_set("long_name", "RMS of Ensemble Spread");
  vrmse_rms_spread.att_set("units", "kg m-2 h-1");

  auto& vcorrel_mean = fstats.create_variable("correl_mean", nc::data_type::f64, {&dtim});
  vcorrel_mean.att_set("long_name", "Average correlation coefficient");
  auto& vcorrel_count = fstats.create_variable("correl_count", nc::data_type::i64, {&dtim});
  vcorrel_count.att_set("long_name", "Number of samples in mean correlation calculation");

  auto& vrank_counts = fstats.create_variable("rank_counts", nc::data_type::i64, {&dtim, &drnk});
  vrank_counts.att_set("long_name", "Bin counts");
  auto& vrank_freq = fstats.create_variable("rank_frequency", nc::data_type::f32, {&dtim, &drnk});
  vrank_freq.att_set("long_name", "Relative frequency");

  auto& vcrps_samples = fstats.create_variable("crps_samples", nc::data_type::i32, {&dtim, dy, dx});
  vcrps_samples.att_set("grid_mapping", grid_mapping_name);
  vcrps_samples.att_set("long_name", "Samples observed at grid point");
  auto& vcrps_spatial = fstats.create_variable("crps_spatial", nc::data_type::f32, {&dtim, dy, dx});
  vcrps_spatial.att_set("grid_mapping", grid_mapping_name);
  vcrps_spatial.att_set("long_name", "CRPS at grid point");
  vcrps_spatial.att_set("_FillValue", nodata);
  auto& vcrps_domain  = fstats.create_variable("crps_domain", nc::data_type::f32, {&dtim});
  vcrps_domain.att_set("long_name", "CRPS over domain");
  vcrps_domain.att_set("_FillValue", nodata);

  auto& vspr_samples = fstats.create_variable("spread_samples", nc::data_type::i64, {&dtim, &dspr});
  vspr_samples.att_set("long_name", "Samples in spread bin");
  auto& vspr_sqr_spread = fstats.create_variable("spread_sqr_spread", nc::data_type::f64, {&dtim, &dspr});
  vspr_sqr_spread.att_set("long_name", "Square spread for RMSE bin");
  auto& vspr_spread = fstats.create_variable("spread_spread", nc::data_type::f32, {&dtim, &dspr});
  vspr_spread.att_set("long_name", "Ensemble Spread");
  vspr_spread.att_set("units", "kg-2 h-1");
  vspr_spread.att_set("_FillValue", nodata);

  auto& vroc_occurrences = fstats.create_variable("roc_occurrences", nc::data_type::i64, {&dtim, &dthr});
  vroc_occurrences.att_set("long_name", "Observations above observation threshold");
  auto& vroc_non_occurrences = fstats.create_variable("roc_non_occurrences", nc::data_type::i64, {&dtim, &dthr});
  vroc_non_occurrences.att_set("long_name", "Observations below observation threshold");
  auto& vroc_false_alarms = fstats.create_variable("roc_false_alarms", nc::data_type::i64, {&dtim, &dthr, &droc});
  vroc_false_alarms.att_set("long_name", "False alarms at probability threshold");
  auto& vroc_hits = fstats.create_variable("roc_hits", nc::data_type::i64, {&dtim, &dthr, &droc});
  vroc_hits.att_set("long_name", "Hits at probability threshold");
  auto& vroc_pod = fstats.create_variable("roc_pod", nc::data_type::f32, {&dtim, &dthr, &droc});
  vroc_pod.att_set("long_name", "Probability of detection");
  vroc_pod.att_set("units", "1");
  vroc_pod.att_set("_FillValue", nodata);
  auto& vroc_far = fstats.create_variable("roc_far", nc::data_type::f32, {&dtim, &dthr, &droc});
  vroc_far.att_set("long_name", "False alarm rate");
  vroc_far.att_set("units", "1");
  vroc_far.att_set("_FillValue", nodata);
  auto& vroc_area = fstats.create_variable("roc_area", nc::data_type::f32, {&dtim, &dthr});
  vroc_area.att_set("long_name", "Area under ROC curve");
  vroc_area.att_set("units", "1");
  vroc_area.att_set("_FillValue", nodata);

  auto& vrel_forecasts = fstats.create_variable("reliability_forecasts", nc::data_type::i64, {&dtim, &dthr, &drel});
  vrel_forecasts.att_set("long_name", "Forecasts in probability bin");
  auto& vrel_occurrences = fstats.create_variable("reliability_occurrences", nc::data_type::i64, {&dtim, &dthr, &drel});
  vrel_occurrences.att_set("long_name", "Observations above threshold given forecast");
  auto& vrel_observed = fstats.create_variable("reliability_observed_frequency", nc::data_type::f32, {&dtim, &dthr, &drel});
  vrel_observed.att_set("long_name", "Observed relative frequency");
  vrel_observed.att_set("units", "1");
  vrel_observed.att_set("_FillValue", nodata);
  auto& vrel_climatology = fstats.create_variable("reliability_climatology", nc::data_type::f32, {&dtim, &dthr});
  vrel_climatology.att_set("long_name", "Observed climatology");
  vrel_climatology.att_set("_FillValue", nodata);

  // buffers for i/o
  float fval;
  long lval;
  double dval;
  vector<long> l1, l2;
  vector<float> f1, f2;
  vector<double> d1;

  // write the static data
  vmodel.write(span{&c_->model_name(), 1});
  fval = c_->minimum_rain_rate();
  vmin_obs.write(span{&fval, 1});
  lval = c_->border_width();
  vborder.write(span{&lval, 1});
  {
    array1<long> buf{c_->lead_times().size()};
    for (auto i = 0; i < buf.size(); ++i)
      buf[i] = std::chrono::duration_cast<std::chrono::seconds>(c_->lead_times()[i]).count();
    vtim.write(buf);
  }
  vthr.write(c_->thresholds());
  {
    array1<float> buf{c_->roc_points()};
    for (auto i = 0; i < buf.size(); ++i)
      buf[i] = scores[0]->thresholds[0]->roc.probability_threshold(i);
    vroc.write(buf);
  }
  {
    array1<float> buf{c_->reliability_bins()};
    for (auto i = 0; i < buf.size(); ++i)
      buf[i] = scores[0]->thresholds[0]->reliability.forecast_probability(i);
    vrel.write(buf);
  }
  {
    array1<float> buf{c_->spread_bins()};
    for (auto i = 0; i < buf.size(); ++i)
      buf[i] = scores[0]->spread.rms_error(i);
    vspr.write(buf);
  }

  // write the dynamic data
  {
    auto val = std::chrono::system_clock::to_time_t(from);
    vfrom.write(span{&val, 1});
  }

  {
    auto val = std::chrono::system_clock::to_time_t(till);
    vtill.write(span{&val, 1});
  }

  for (auto ilead = 0; ilead < c_->lead_times().size(); ++ilead)
  {
    auto& lead_score = *scores[ilead];

    // write sample count
    lval = lead_score.sample_count;
    vsample_count.write(span{&lval, 1}, {ilead});

    // write RMSE data
    lval = lead_score.rmse.count();
    vrmse_count.write(span{&lval, 1}, {ilead});
    dval = lead_score.rmse.mean_square_error();
    vrmse_ms_error.write(span{&dval, 1}, {ilead});
    dval = lead_score.rmse.mean_square_spread();
    vrmse_ms_spread.write(span{&dval, 1}, {ilead});
    fval = lead_score.rmse.rmse_ensemble_mean();
    vrmse_rmse.write(span{&fval, 1}, {ilead});
    fval = lead_score.rmse.rms_ensemble_spread();
    vrmse_rms_spread.write(span{&fval, 1}, {ilead});

    // write average correlation coefficient data
    lval = lead_score.correlation.count();
    vcorrel_count.write(span{&lval, 1}, {ilead});
    dval = lead_score.correlation.mean_correlation();
    vcorrel_mean.write(span{&dval, 1}, {ilead});

    // write rank histogram data
    vrank_counts.write(lead_score.rank.counts(), {ilead});
    d1.resize(lead_score.rank.bin_count());
    lval = 0l;
    for (auto i = 0; i < lead_score.rank.bin_count(); ++i)
    {
      d1[i] = lead_score.rank.counts()[i];
      lval += lead_score.rank.counts()[i];
    }
    for (auto i = 0; i < lead_score.rank.bin_count(); ++i)
      d1[i] /= lval;
    vrank_freq.write(d1, {ilead});

    // write crps data
    vcrps_samples.write(lead_score.crps.samples(), {ilead});
    vcrps_spatial.write(lead_score.crps.spatial_crps(), {ilead});
    fval = lead_score.crps.domain_crps();
    vcrps_domain.write(span{&fval, 1}, {ilead});

    // write spread reliability data
    l1.resize(lead_score.spread.bin_count());
    d1.resize(lead_score.spread.bin_count());
    f1.resize(lead_score.spread.bin_count());
    for (auto i = 0; i < lead_score.spread.bin_count(); ++i)
    {
      l1[i] = lead_score.spread.samples(i);
      d1[i] = lead_score.spread.square_spread(i);
      f1[i] = lead_score.spread.rms_spread(i);
      if (std::isnan(f1[i]))
        f1[i] = nodata;
    }
    vspr_samples.write(l1, {ilead});
    vspr_sqr_spread.write(d1, {ilead});
    vspr_spread.write(f1, {ilead});

    // write roc data
    l1.resize(lead_score.thresholds[0]->roc.point_count());
    l2.resize(lead_score.thresholds[0]->roc.point_count());
    f1.resize(lead_score.thresholds[0]->roc.point_count());
    f2.resize(lead_score.thresholds[0]->roc.point_count());
    for (auto i = 0; i < int(lead_score.thresholds.size()); ++i)
    {
      auto& m = lead_score.thresholds[i]->roc;

      lval = m.occurrences();
      vroc_occurrences.write(span{&lval, 1}, {ilead, i});
      lval = m.non_occurrences();
      vroc_non_occurrences.write(span{&lval, 1}, {ilead, i});
      fval = m.area();
      if (std::isnan(fval))
        fval = nodata;
      vroc_area.write(span{&fval, 1}, {ilead, i});

      for (auto j = 0; j < m.point_count(); ++j)
      {
        l1[j] = m.false_alarms(j);
        l2[j] = m.hits(j);
        f1[j] = m.probability_of_detection(j);
        if (std::isnan(f1[j]))
          f1[j] = nodata;
        f2[j] = m.false_alarm_rate(j);
        if (std::isnan(f2[j]))
          f2[j] = nodata;
      }
      vroc_false_alarms.write(l1, {ilead, i});
      vroc_hits.write(l2, {ilead, i});
      vroc_pod.write(f1, {ilead, i});
      vroc_far.write(f2, {ilead, i});
    }

    // write reliability data
    l1.resize(lead_score.thresholds[0]->reliability.bin_count());
    l2.resize(lead_score.thresholds[0]->reliability.bin_count());
    f1.resize(lead_score.thresholds[0]->reliability.bin_count());
    for (auto i = 0; i < c_->thresholds().size(); ++i)
    {
      auto& m = lead_score.thresholds[i]->reliability;

      fval = m.climatology_probability();
      if (std::isnan(fval))
        fval = nodata;
      vrel_climatology.write(span{&fval, 1}, {ilead, i});

      for (auto j = 0; j < m.bin_count(); ++j)
      {
        l1[j] = m.forecasts(j);
        l2[j] = m.occurrences(j);
        f1[j] = m.observed_frequency(j);
        if (std::isnan(f1[j]))
          f1[j] = nodata;
      }
      vrel_forecasts.write(l1, {ilead, i});
      vrel_occurrences.write(l2, {ilead, i});
      vrel_observed.write(f1, {ilead, i});
    }
  }
}

auto verification_io_netcdf::read_impl(
      nc::file const& fstats
    , time_point& from
    , time_point& till
    , verification::score_store& scores
    ) -> void
{
  time_t tval;
  long lval;
  double dval;
  vector<long> l1, l2;
  vector<double> d1;

  fstats.lookup_variable("from").read(span{&tval, 1});
  from = std::chrono::system_clock::from_time_t(tval);

  fstats.lookup_variable("till").read(span{&tval, 1});
  till = std::chrono::system_clock::from_time_t(tval);

  // TODO - robust checking that input setup matches configuration

  for (auto ilead = 0; ilead < c_->lead_times().size(); ++ilead)
  {
    auto& lead_scores = *scores[ilead];
    fstats.lookup_variable("sample_count").read(span{&lead_scores.sample_count, 1}, {ilead});

    fstats.lookup_variable("rmse_count").read(span{&lval, 1}, {ilead});
    lead_scores.rmse.set_count(lval);
    fstats.lookup_variable("rmse_ms_error").read(span{&dval, 1}, {ilead});
    lead_scores.rmse.set_mean_square_error(dval);
    fstats.lookup_variable("rmse_ms_spread").read(span{&dval, 1}, {ilead});
    lead_scores.rmse.set_mean_square_spread(dval);

    fstats.lookup_variable("correl_count").read(span{&lval, 1}, {ilead});
    lead_scores.correlation.set_count(lval);
    fstats.lookup_variable("correl_mean").read(span{&dval, 1}, {ilead});
    lead_scores.correlation.set_mean_correlation(dval);

    fstats.lookup_variable("rank_counts").read(lead_scores.rank.counts(), {ilead});

    fstats.lookup_variable("crps_samples").read(lead_scores.crps.samples(), {ilead});
    fstats.lookup_variable("crps_spatial").read(lead_scores.crps.spatial_crps(), {ilead});

    l1.resize(lead_scores.spread.bin_count());
    d1.resize(lead_scores.spread.bin_count());
    fstats.lookup_variable("spread_samples").read(l1, {ilead});
    fstats.lookup_variable("spread_sqr_spread").read(d1, {ilead});
    for (auto ibin = 0; ibin < lead_scores.spread.bin_count(); ++ibin)
    {
      lead_scores.spread.set_samples(ibin, l1[ibin]);
      lead_scores.spread.set_square_spread(ibin, d1[ibin]);
    }

    for (auto ithresh = 0; ithresh < c_->thresholds().size(); ++ithresh)
    {
      auto& roc = lead_scores.thresholds[ithresh]->roc;
      fstats.lookup_variable("roc_occurrences").read(span{&lval, 1}, {ilead, ithresh});
      roc.set_occurrences(lval);
      fstats.lookup_variable("roc_non_occurrences").read(span{&lval, 1}, {ilead, ithresh});
      roc.set_non_occurrences(lval);
      l1.resize(roc.point_count());
      l2.resize(roc.point_count());
      fstats.lookup_variable("roc_false_alarms").read(l1, {ilead, ithresh});
      fstats.lookup_variable("roc_hits").read(l2, {ilead, ithresh});
      for (auto ipoint = 0; ipoint < roc.point_count(); ++ipoint)
      {
        roc.set_false_alarms(ipoint, l1[ipoint]);
        roc.set_hits(ipoint, l2[ipoint]);
      }
    }

    for (auto ithresh = 0; ithresh < c_->thresholds().size(); ++ithresh)
    {
      auto& reliability = lead_scores.thresholds[ithresh]->reliability;
      l1.resize(reliability.bin_count());
      l2.resize(reliability.bin_count());
      fstats.lookup_variable("reliability_forecasts").read(l1, {ilead, ithresh});
      fstats.lookup_variable("reliability_occurrences").read(l2, {ilead, ithresh});
      for (auto ibin = 0; ibin < reliability.bin_count(); ++ibin)
      {
        reliability.set_forecasts(ibin, l1[ibin]);
        reliability.set_occurrences(ibin, l2[ibin]);
      }
    }
  }
}

verification_io_netcdf_local::verification_io_netcdf_local(
      verification const& c
    , configuration const& config
    , metadata const& user_parameters)
  : verification_io_netcdf{c, config}
  , user_parameters_{&user_parameters}
  , path_format_{strip_file_schema(config["path"].string())}
{ }

auto verification_io_netcdf_local::write(
      time_point from
    , time_point till
    , verification::score_store const& scores
    ) -> void
{
  auto path = determine_path(from, till);
  try
  {
    auto lock = nc::lock_mutex();
    auto file = nc::file{path, nc::io_mode::create};
    write_impl(from, till, scores, file);
  }
  catch (...)
  {
    /* We try to remove the output file if this is not a successful outcome.  If we fail to remove the file just trace
     * it as a non-fatal error - we just give a best-effort guarantee for cleanup during forecast failure. */
    if (auto ec = std::error_code{}; std::filesystem::exists(path, ec) && !std::filesystem::remove(path, ec) && ec)
      trace(trace_level::error, "Failed to remove partial output file {}: {}", path, ec.message());
    throw;
  }
}

auto verification_io_netcdf_local::read(
      string_view path
    , time_point& from
    , time_point& till
    , verification::score_store& scores
    ) -> bool
try
{
  if (!std::filesystem::exists(path))
    return false;
  read_impl(nc::file{string(path), nc::io_mode::read_only}, from, till, scores);
  return true;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to read verification file {}", path)});
}

auto verification_io_netcdf_local::determine_path(time_point from, time_point till) const -> string
try
{
  return format(path_format_, "param"_a = *user_parameters_, "from"_a = from, "till"_a = till);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Path format '{}' is invalid", path_format_)});
}
