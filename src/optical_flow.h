/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array2.h"
#include "configuration.h"
#include "filters.h"
#include "fourier_transform.h"
#include "profile.h"

namespace steps
{
  class core;

  /// Interface for optical flow algorithms
  class optical_flow
  {
  public:
    static auto instantiate(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config) -> unique_ptr<optical_flow>;

  public:
    /// Virtual destructor
    virtual ~optical_flow() = default;

    /// Perform the optical flow analysis
    /** The output flow vectors are at the 'from' locations and point to the 'to' locations. */
    virtual auto determine_flow(
          array2f const& from     ///< Field state at origin of flow vectors
        , array2f const& to       ///< Field state at target of flow vectors
        , bool use_initial_flow   ///< Whether flow has been pre-initialized with a flow
        , array2f2& flow          ///< Diagnosed vectors that will advect 'from' to 'to'
        ) const -> void = 0;

  protected:
    static profile prof_optical_flow;
  };

  /// Robust optical flow estimation (BROX) algorithm (multi-threaded)
  class optical_flow_brox_mt : public optical_flow
  {
  public:
    optical_flow_brox_mt(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config);

    auto determine_flow(
          array2f const& from
        , array2f const& to
        , bool use_initial_flow
        , array2f2& flow
        ) const -> void override;

  private:
    struct prefilter
    {
      fourier_transform fft;
      filter            low_pass;

      prefilter(vec2i fft_shape, int fft_threads, float grid_resolution, float rolloff, float crossover);
    };

  private:
    float               log_zero_;
    optional<prefilter> prefilter_;
    float               alpha_;
    float               gamma_;
    int                 scales_;
    float               zfactor_;
    float               tol_;
    int                 initer_;
    int                 outiter_;
  };


  /// Robust optical flow estimation (BROX) algorithm
  class optical_flow_brox : public optical_flow
  {
  public:
    optical_flow_brox(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config);

    auto determine_flow(
          array2f const& from
        , array2f const& to
        , bool use_initial_flow
        , array2f2& flow
        ) const -> void override;

  private:
    struct prefilter
    {
      fourier_transform fft;
      filter            low_pass;

      prefilter(vec2i fft_shape, int fft_threads, float grid_resolution, float rolloff, float crossover);
    };

  private:
    float               log_zero_;
    optional<prefilter> prefilter_;
    float               alpha_;
    float               gamma_;
    int                 scales_;
    float               zfactor_;
    float               tol_;
    int                 initer_;
    int                 outiter_;
  };

  /// Combined local-global (CLG) optical flow algorithm
  class optical_flow_clg : public optical_flow
  {
  public:
    optical_flow_clg(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config);

    auto determine_flow(
          array2f const& from
        , array2f const& to
        , bool use_initial_flow
        , array2f2& flow
        ) const -> void override;

  private:
    double  alpha_;
    double  rho_;
    double  sigma_;
    int     iterations_;

    double  w_factor_;
    int     scales_;
    double  scale_factor_;
    bool    coupled_mode_;
  };

  /// Rainfields 3.1 optical flow algorithm
  class optical_flow_rf3 : public optical_flow
  {
  public:
    optical_flow_rf3(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config);

    auto determine_flow(
          array2f const& from
        , array2f const& to
        , bool use_initial_flow
        , array2f2& flow
        ) const -> void override;

  private:
    // flag to indicate a missing vector that we want to interpolate over
    static constexpr float bad_vel = -9999.0f;
    static constexpr float bad_vel_cmp = -9900.0f;

    // per level constants
    struct level_info
    {
      float scale;
      float sigma;
      int   ksize;
      vec2i dims;
    };
    using vec5d = double[5];

  private:
    auto track_fields(
          array2f const& lag1
        , array2f const& lag0
        , array2f2& velocity
        , bool use_initial_flow
        ) const -> void;
    auto poly_exp_setup() -> void;
    auto poly_exp(array2f const& src, array2<vec5d>& dst) const -> void;
    auto update_matrices(
          array2<vec5d> const& r0
        , array2<vec5d> const& r1
        , array2f2 const& flow
        , array2<vec5d>& mat
        , int y0
        , int y1
        ) const -> void;
    auto blur_iteration(
          array2<vec5d> const& r0
        , array2<vec5d> const& r1
        , array2f2& flow
        , array2<vec5d>& mat
        , bool update_mats
        ) const -> void;

    auto interpolate_gaps(array2f2& velocity) const -> void;

  private:
    // flow model input parameters
    vec2i   dims_;
    float   scale_;
    int     win_size_;
    int     iterations_;
    int     poly_n_;
    double  poly_sigma_;

    // other parameters
    float   background_;
    float   threshold_;
    float   gain_;
    bool    fill_gaps_;
    int     spacing_;
    float   min_frac_bins_for_avg_;
    double  idw_low_res_pwr_;
    double  idw_high_res_pwr_;

    // precalculated terms
    vector<level_info> info_;    // used in main loop

    // constant junk used in poly_exp
    vector<float> kbuf_;
    double        ig11_;
    double        ig03_;
    double        ig33_;
    double        ig55_;
  };

  /// Test pattern generator for optical flow
  class optical_flow_test : public optical_flow
  {
  public:
    optical_flow_test(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config);
    auto determine_flow(
          array2f const& from
        , array2f const& to
        , bool use_initial_flow
        , array2f2& flow
        ) const -> void override;

  private:
    vec2f   value_;
  };
}
