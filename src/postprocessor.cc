/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2023 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "postprocessor.h"
#include "profile.h"
#include "thread_pool.h"

using namespace steps;

static profile prof_postprocess_initialize{"postprocess::initialize"};
static profile prof_postprocess_spread_wait{"postprocess::spread_wait"};
static profile prof_postprocess_input{"postprocess::input"};
static profile prof_postprocess_generate{"postprocess::generate"};
static profile prof_postprocess_finalize{"postprocess::finalize"};

postprocessor::postprocessor(configuration const& config)
  : user_parameters_{parse_metadata(config.optional("user_parameters", configuration{}), metadata{})}
  , members_{config["members"]}
  , member_offset_{config["member_offset"]}
  , forecasts_{config["forecasts"]}
  , time_step_{config["time_step"]}
  , grid_shape_{int(config["grid_cols"]), int(config["grid_rows"])}
  , grid_resolution_{config["grid_resolution"]}
  , input_{input::instantiate(config["input"], user_parameters_)}
  , products_{
      members_
    , member_offset_
    , forecasts_
    , time_step_
    , grid_shape_
    , grid_resolution_
    , config["product"]
    , user_parameters_}
  , max_engine_spread_{config.optional("max_engine_spread", -1)}
{ }

auto postprocessor::generate_products(time_point reference_time) -> void
{
  try
  {
    {
      auto ps = profile::scope{prof_postprocess_initialize};
      products_.initialize(reference_time);
    }

    auto counts = vector<std::atomic_int>(forecasts_);
    for (auto& c : counts)
      c = members_;

    auto abort = std::atomic_bool{false};
    thread_pool_distribute_loop(members_, [&](int imem)
    {
      try
      {
        auto member = imem + member_offset_;
        auto field = array2f{grid_shape_};

        // notify driver code that we've finished the initialization phase
        /* we didn't actually need any initialization so this is just to ensure the driver can always rely on receiving
         * a notification with forecast set to zero. */
        if (cb_progress_)
          cb_progress_(member, 0);

        for (auto forecast = 0; forecast < forecasts_; ++forecast)
        {
          auto lead_time = (forecast + 1) * time_step_;
          auto time = reference_time + lead_time;

          // enforce the maximum spread to limit memory consumption and contribute to any queued tasks while we wait
          /* it is critical to check the abort condition here because once a member iteration terminates via exception we
           * will no longer be fully decrementing the counts array, which means we would stall here forever waiting for
           * the failed member iteration to "catch up". */
          while (max_engine_spread_ > -1 && forecast > max_engine_spread_ && counts[forecast - max_engine_spread_ - 1] > 0 && !abort)
          {
            if (!thread_pool_process_task())
            {
              // only count time we are truly blocked and not contributing to other tasks as wasted "wait" time
              auto ps = profile::scope{prof_postprocess_spread_wait};
              std::this_thread::yield();
            }
          }

          // if another member iteration has failed, then we might as well stop processing now too
          if (abort)
            break;

          {
            auto ps = profile::scope{prof_postprocess_input};
            if (!input_->read(reference_time, member, time, field))
              throw std::runtime_error{format("Failed to load forecast for member {} time {}", member, time)};
          }

          {
            auto ps = profile::scope{prof_postprocess_generate};
            products_.write(member - member_offset_, forecast, field, nullptr);
          }

          --counts[forecast];

          // notify driver code that we've made some forward progress
          // forecast '0' is reference time in callback spec so we need to add one here to get the right index
          if (cb_progress_)
            cb_progress_(member, forecast + 1);
        }
      }
      catch (...)
      {
        // notify the other member processing loop iterations that they should shortcircuit
        abort = true;
        throw;
      }
    });

    {
      auto ps = profile::scope{prof_postprocess_finalize};
      products_.finalize();
    }
  }
  catch (...)
  {
    products_.abort();
    throw;
  }
}
