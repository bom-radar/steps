/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once
#include "format.h"
#include <type_traits>

namespace steps
{
  /// 2D vector
  template <typename T>
  struct vec2
  {
    T x, y;

    constexpr vec2() = default;
    constexpr vec2(T x, T y) : x{std::move(x)}, y{std::move(y)} { }

    template <typename U>
    constexpr explicit vec2(vec2<U> const& v) : x{static_cast<T>(v.x)}, y{static_cast<T>(v.y)} { }

    template <typename U = T, typename = std::enable_if_t<!std::is_floating_point_v<U>>>
    constexpr auto operator==(vec2 const& rhs) const
    {
      return x == rhs.x && y == rhs.y;
    }

    template <typename U = T, typename = std::enable_if_t<!std::is_floating_point_v<U>>>
    constexpr auto operator!=(vec2 const& rhs) const
    {
      return x != rhs.x || y != rhs.y;
    }

    constexpr auto operator+(vec2 const& rhs) const
    {
      return vec2{x + rhs.x, y + rhs.y};
    }

    constexpr auto operator-(vec2 const& rhs) const
    {
      return vec2{x - rhs.x, y - rhs.y};
    }

    constexpr auto operator*(T val) const
    {
      return vec2{x * val, y * val};
    }

    template <typename U>
    constexpr auto operator/(U val) const
    {
      return vec2<decltype(x / val)>{x / val, y / val};
    }

    constexpr auto& operator+=(vec2 const& val)
    {
      x += val.x;
      y += val.y;
      return *this;
    }

    constexpr auto& operator-=(vec2 const& val)
    {
      x -= val.x;
      y -= val.y;
      return *this;
    }

    constexpr auto& operator*=(T val)
    {
      x *= val;
      y *= val;
      return *this;
    }

    constexpr auto& operator/=(T val)
    {
      x /= val;
      y /= val;
      return *this;
    }

    auto magnitude() const
    {
      return std::sqrt(x * x + y * y);
    }

    auto angle() const
    {
      return -(std::atan2(y, x) - pi<T> / 2);
    }

    friend constexpr auto operator*(T lhs, vec2 const& rhs)
    {
      return vec2{lhs * rhs.x, lhs * rhs.y};
    }
  };

  using vec2i = vec2<int>;
  using vec2f = vec2<float>;
  using vec2d = vec2<double>;

  template <typename T>
  auto isnan(vec2<T> val)
  {
    return std::isnan(val.x) || std::isnan(val.y);
  }

  template <typename T>
  auto vec2_angle_magnitude(T angle, T magnitude)
  {
    return vec2<T>(magnitude * sin(angle), magnitude * cos(angle));
  }

  template <typename T>
  struct allow_simple_serialization<vec2<T>>
  {
    static constexpr bool value = allow_simple_serialization<T>::value;
  };

  template <typename T>
  struct parser<vec2<T>>
  {
    auto parse(string_view str) -> vec2<T>
    {
      try
      {
        auto endx = str.find_first_of(" \t\r\n");
        if (endx == string_view::npos)
          throw std::runtime_error{"unable to delimit x and y values"};
        auto begy = str.find_first_not_of(" \t\r\n", endx);
        if (begy == string_view::npos)
          throw std::runtime_error{"unable to delimit x and y values"};
        return {::steps::parse<T>(str.substr(0, endx)), ::steps::parse<T>(str.substr(begy))};
      }
      catch (...)
      {
        std::throw_with_nested(std::runtime_error{format("failed to parse {} from '{}'", "vec2<T>", str)});
      }
    }
  };
}

namespace fmt
{
  // By inheriting from underlying type's formatter we don't have to implement the parse function ourselves
  template <typename T>
  struct formatter<steps::vec2<T>> : formatter<T>
  {
    template <typename FormatContext>
    auto format(steps::vec2<T> const& v, FormatContext& ctx)
    {
      formatter<T>::format(v.x, ctx);
      format_to(ctx.out(), " ");
      formatter<T>::format(v.y, ctx);
      return ctx.out();
    }
  };
}
