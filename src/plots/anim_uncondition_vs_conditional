#!/usr/bin/env python3
#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
import argparse
import os
import numpy as np
import netCDF4 as netcdf
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import dateutil.parser
import datetime

# parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('input_adv', help='steps-adv mode debug file (debug.nc)')
parser.add_argument('input_nwp', help='steps-nwp mode debug file (debug.nc)')
parser.add_argument('output', help='Path of output animation (output.mp4)')
args = parser.parse_args()

# open our diagnostic files
nc_adv = netcdf.Dataset(args.input_adv)
nc_nwp = netcdf.Dataset(args.input_nwp)

# select the variables to plot
var_inp = nc_nwp.variables['mod-fld']
var_adv = nc_adv.variables['lag0-f']
var_nwp = nc_nwp.variables['lag0-f']

def setup_axes(slot, data, title):
    ax[0][slot].set_aspect('equal')
    ax[0][slot].grid(alpha=0.5, linestyle='--')
    ax[0][slot].set_title(title)
    im[slot] = ax[0][slot].imshow(data, cmap='jet', vmin=0.0, vmax=6.0)

def animate(i):
    im[0].set_data(var_inp[i][0])
    im[1].set_data(var_adv[i])
    im[2].set_data(var_nwp[i])

# setup our axes
fig, ax = plt.subplots(1, 3, figsize=(12,4.2), constrained_layout=True, squeeze=False)
im = [None] * 3
setup_axes(0, var_inp[0][0], "NWP Input")
setup_axes(1, var_adv[0], "Advection Only")
setup_axes(2, var_nwp[0], "NWP Blended")
anim = animation.FuncAnimation(fig, animate, frames=len(var_adv), interval=20, repeat=True, repeat_delay=500)
anim.save(args.output, fps=3, extra_args=['-vcodec', 'libx264'])
