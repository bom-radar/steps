/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "profile.h"
#include "unit_test.h"
#include <mutex>
#include <vector>

using namespace steps;

#if !opt_disable_profile
// this avoids the static initialization order fiasco so that we can safely create static profile objects
static auto& profile_static()
{
  static struct
  {
    std::mutex mut;
    std::vector<profile*> profiles;
  } data;
  return data;
}

profile::profile(string name)
  : name_(std::move(name))
  , count_{0}
  , total_{0}
{
  auto lock = std::lock_guard<std::mutex>{profile_static().mut};
  profile_static().profiles.push_back(this);
}

profile::~profile()
{
  auto lock = std::lock_guard<std::mutex>{profile_static().mut};
  auto& profiles = profile_static().profiles;

  profiles.erase(std::find(profiles.begin(), profiles.end(), this));
}

auto profile::generate_report(bool reset) -> string
{
  // collect data from all the profiles
  std::vector<std::tuple<string, long, long>> data;
  {
    auto lock = std::lock_guard<std::mutex>{profile_static().mut};
    auto& profiles = profile_static().profiles;
    data.reserve(profiles.size());
    for (auto& p : profiles)
    {
      data.emplace_back(p->name_, p->count_, p->total_);
      if (reset)
        p->reset();
    }
  }

  // sort by total time (descending)
  std::sort(data.begin(), data.end(), [](auto const& lhs, auto const& rhs) { return std::get<2>(lhs) > std::get<2>(rhs); });

  // determine maximum profile name length
  string::size_type max_len = 4;
  for (auto& d : data)
    max_len = std::max(max_len, std::get<0>(d).size());

  // output the report
  string report;
  report.append(format("{:{}} {:>8} {:>8}\n", "Name", max_len, "Count", "Time"));
  report.append(format("{:-<{}}\n", "", max_len + 18));
  for (auto& d : data)
  {
    if (std::get<1>(d) > 0)
    {
      auto seconds = std::get<2>(d) / 1000000.0;
      report.append(format("{:{}} {:8} {:8.2f}\n", std::get<0>(d), max_len, std::get<1>(d), seconds));
    }
  }

  return report;
}

// LCOV_EXCL_START
#include <regex>
#include <thread>
TEST_CASE("profile")
{
  // this ensures that any other profile objects (probably static) are ignored for the purpose of our test
  auto old_profile_list = profile_static().profiles;
  profile_static().profiles.clear();

  try
  {
    profile p1{"foo"};
    profile p2{"bar"};
    {
      auto ps1 = profile::scope{p1};
      std::this_thread::sleep_for(std::chrono::milliseconds{10});
      auto ps2 = profile::scope{p2};
      std::this_thread::sleep_for(std::chrono::milliseconds{30});
      {
        auto ps2b = profile::scope{p2};
        std::this_thread::sleep_for(std::chrono::milliseconds{30});
      }
    }
    CHECK(p1.count() == 1);
    CHECK(p2.count() == 2);

    auto report = profile::generate_report(false);
    CHECK(std::regex_match(report, std::regex(R"(Name +Count +Time\n-+\nbar +\d+ +\d+\.\d+\nfoo +\d+ +\d+\.\d+\n)")));

    CHECK(p1.count() == 1);
    CHECK(p2.count() == 2);

    report = profile::generate_report(true);
    CHECK(std::regex_match(report, std::regex(R"(Name +Count +Time\n-+\nbar +\d+ +\d+\.\d+\nfoo +\d+ +\d+\.\d+\n)")));

    CHECK(p1.count() == 0);
    CHECK(p1.total() == 0);
    CHECK(p2.count() == 0);
    CHECK(p2.total() == 0);
  }
  catch (...)
  {
    profile_static().profiles = old_profile_list;
    throw;
  }

  profile_static().profiles = old_profile_list;
}
// LCOV_EXCL_STOP
#endif
