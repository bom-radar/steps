/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "diagnostics.h"
#include "core.h"
#include "trace.h"
#include "unit_test.h"
#include <algorithm>
#include <map>

using namespace steps;

static optional<nc::file> file_;

auto steps::diagnostics::initialize(string_view path) -> void
try
{
  auto lock = nc::lock_mutex();

  // check if we are already initialized
  if (file_)
    throw std::runtime_error{"File already open"};

  // open the file
  file_.emplace(string(path), nc::io_mode::create);

  // store some standard metadata
  // - i.e. wall time

  trace(trace_level::notice, "Initialized diagnostic file '{}'", path);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to initialize diagnostic file '{}'", path)});
}

auto steps::diagnostics::shutdown() -> void
{
  auto lock = nc::lock_mutex();
  file_.reset();
}

auto steps::diagnostics::enabled() -> bool
{
  return file_.has_value();
}

auto steps::diagnostics::create_dimension(char const* name, int size) -> void
try
{
  if (!enabled())
    return;

  auto lock = nc::lock_mutex();
  file_->create_dimension(name, size);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to create dimension '{}'", name)});
}

auto steps::diagnostics::create_variable(
      char const* name
    , nc::data_type type
    , std::initializer_list<char const*> dims
    , char const* long_name
    , char const* units
    ) -> void
try
{
  if (!enabled())
    return;

  auto lock = nc::lock_mutex();

  // lookup the dimensions
  vector<nc::dimension*> dim_ptrs;
  for (auto dim_name : dims)
    dim_ptrs.push_back(&file_->lookup_dimension(dim_name));

  // define the variable
  auto& var = file_->create_variable(name, type, dim_ptrs);
  if (long_name)
    var.att_set("long_name", long_name);
  if (units)
    var.att_set("units", units);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to create variable '{}'", name)});
}

auto steps::diagnostics::write(char const* name, data_span data, span<int const> coords) -> void
try
{
  if (!enabled())
    return;
  auto lock = nc::lock_mutex();
  std::visit([&](auto& s) { file_->lookup_variable(name).write(s, coords); }, data);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to write variable '{}'", name)});
}

auto steps::diagnostics::flush() -> void
{
  if (!enabled())
    return;

  auto lock = nc::lock_mutex();
  file_->flush();
}

// LCOV_EXCL_START
TEST_CASE("diagnostics")
{

}
// LCOV_EXCL_STOP
