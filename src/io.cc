/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "io.h"
#include "io_netcdf.h"
#include "cf.h"
#include "mosaic.h"
#include "trace.h"
#include "unit_test.h"
#if STEPS_WITH_AWS
#include "aws.h"
#endif

#include <fstream>

using namespace steps;

auto steps::determine_uri_scheme(string_view uri) -> uri_scheme
{
  auto pos = uri.find(":/");
  if (pos == string::npos || uri[pos] == '/' || uri.compare(pos, 3, "://") != 0)
    return uri_scheme::none;
  if (uri.compare(0, pos, "s3"sv) == 0 || uri.compare(0, pos, "s3crt"sv) == 0)
    return uri_scheme::s3;
  if (uri.compare(0, pos, "file"sv) == 0) // check last since it's not likely to be used in reality
    return uri_scheme::file;
  throw std::runtime_error{format("Unsupported URI scheme '{}'", uri.substr(0, pos))};
}

auto steps::load_configuration(string_view uri, bool optional) -> configuration
try
{
  switch (determine_uri_scheme(uri))
  {
  case uri_scheme::none:
    return configuration::read_file(uri, optional);
  case uri_scheme::file:
    return configuration::read_file(strip_file_schema(uri), optional);
  case uri_scheme::s3:
#if STEPS_WITH_AWS
    return aws::load_configuration_s3(uri, optional);
#else
    throw std::runtime_error{"AWS support disabled"};
#endif
  }
  throw std::logic_error{"Unreachable"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to load configuration from '{}'", uri)});
}

auto steps::save_configuration(string_view uri, configuration const& data) -> void
try
{
  switch (determine_uri_scheme(uri))
  {
  case uri_scheme::none:
    configuration::write_file(uri, data);
    break;
  case uri_scheme::file:
    configuration::write_file(strip_file_schema(uri), data);
    break;
  case uri_scheme::s3:
#if STEPS_WITH_AWS
    return aws::save_configuration_s3(uri, data);
#else
    throw std::runtime_error{"AWS support disabled"};
#endif
  }
  throw std::logic_error{"Unreachable"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to save configuration to '{}'", uri)});
}

static auto read_file(string_view path, bool optional) -> array1<char>
{
  auto fspath = std::filesystem::path{path};
  auto file = std::ifstream{fspath, std::ios::binary | std::ios::ate};
  if (file.fail())
  {
    if (optional && !std::filesystem::exists(fspath))
      return {};
    throw std::system_error{errno, std::system_category()};
  }
  auto size = std::streamoff{file.tellg()};
  if (size >= std::numeric_limits<array1<char>::size_type>::max())
    throw std::runtime_error{"Binary blob is too large for array1<char> container"};
  auto data = array1<char>{static_cast<array1<char>::size_type>(size)};
  if (!file.seekg(0))
    throw std::system_error{errno, std::system_category()};
  if (!file.read(data.data(), data.size()))
    throw std::system_error{errno, std::system_category()};
  return data;
}

auto steps::load_binary_blob(string_view uri, bool optional) -> array1<char>
try
{
  switch (determine_uri_scheme(uri))
  {
  case uri_scheme::none:
    return read_file(uri, optional);
  case uri_scheme::file:
    return read_file(strip_file_schema(uri), optional);
  case uri_scheme::s3:
#if STEPS_WITH_AWS
    return aws::load_binary_blob_s3(uri, optional);
#else
    throw std::runtime_error{"AWS support disabled"};
#endif
  }
  throw std::logic_error{"Unreachable"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to read binary blob from '{}'", uri)});
}

output_stream_local::output_stream_local(string path)
  : path_{std::move(path)}
{
  stream_.exceptions(std::ofstream::failbit | std::ofstream::badbit);
  stream_.open(path_, std::ofstream::binary | std::ofstream::trunc);
}

output_stream_local::~output_stream_local()
{
  abort();
}

auto output_stream_local::write(span<char const> data) -> void
{
  stream_.write(data.data(), data.size());
}

auto output_stream_local::commit() -> void
{
  stream_.close();
  path_.clear();
}

auto output_stream_local::abort() -> void
{
  if (stream_.is_open())
  {
    stream_.close();
    if (auto ec = std::error_code{}; !path_.empty() && !std::filesystem::remove(path_, ec) && ec)
      trace(trace_level::error, "Failed to remove partial output file {}: {}", path_, ec.message());
  }
  path_.clear();
}

auto steps::save_binary_stream(string_view uri) -> unique_ptr<output_stream>
try
{
  switch (determine_uri_scheme(uri))
  {
  case uri_scheme::none:
    return std::make_unique<output_stream_local>(string(uri));
  case uri_scheme::file:
    return std::make_unique<output_stream_local>(string(strip_file_schema(uri)));
  case uri_scheme::s3:
#if STEPS_WITH_AWS
    return std::make_unique<aws::output_stream_s3>(string(uri));
#else
    throw std::runtime_error{"AWS support disabled"};
#endif
  }
  throw std::logic_error{"Unreachable"};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to open binary stream to '{}'", uri)});
}

static input::factory input_factory_ = [](
      configuration const& config
    , metadata const& user_parameters
    ) -> unique_ptr<input>
{
  auto const& type = config["type"].string();
  try
  {
    if (type == "netcdf"sv)
    {
      switch (determine_uri_scheme(config["path"].string()))
      {
      case uri_scheme::none:
      case uri_scheme::file:
        // copes with plain path or file schema uri
        return std::make_unique<input_netcdf_local>(config, user_parameters);
      case uri_scheme::s3:
#if STEPS_WITH_AWS
        return std::make_unique<aws::input_netcdf_s3>(config, user_parameters);
#else
        throw std::runtime_error{"AWS support disabled"};
#endif
      default:
        throw std::runtime_error{"Unsupported URI schema"};
      }
    }
    throw std::runtime_error{format("Invalid input manager type {}", type)};
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to configure input type {}", type)});
  }
};

auto input::set_factory(factory function) -> void
{
  input_factory_ = function;
}

auto input::instantiate(configuration const& config, metadata const& user_parameters) -> unique_ptr<input>
{
  return input_factory_(config, user_parameters);
}

static output::factory output_factory_ = [](configuration const& config, metadata const& user_parameters) -> unique_ptr<output>
{
  auto& type = config["output"].string();
  try
  {
    if (type == "netcdf"sv)
    {
      switch (determine_uri_scheme(config["path"].string()))
      {
      case uri_scheme::none:
      case uri_scheme::file:
        // copes with plain path or file schema uri
        return std::make_unique<output_netcdf_local>(config, user_parameters);
      case uri_scheme::s3:
#if STEPS_WITH_AWS
        return std::make_unique<aws::output_netcdf_s3>(config, user_parameters);
#else
        throw std::runtime_error{"AWS support disabled"};
#endif
      default:
        throw std::runtime_error{"Unsupported URI schema"};
      }
    }
    if (type == "mosaic-tile"sv)
    {
      switch (determine_uri_scheme(config["path"].string()))
      {
      case uri_scheme::none:
      case uri_scheme::file:
        // copes with plain path or file schema uri
        return std::make_unique<output_mosaic_tile_local>(config, user_parameters);
      case uri_scheme::s3:
#if STEPS_WITH_AWS
        return std::make_unique<aws::output_mosaic_tile_s3>(config, user_parameters);
#else
        throw std::runtime_error{"AWS support disabled"};
#endif
      default:
        throw std::runtime_error{"Unsupported URI schema"};
      }
    }
    if (type == "none"sv)
      return nullptr;
    throw std::runtime_error{format("Unknown output manager type {}", type)};
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to configure output type {}", type)});
  }
};

auto output::set_factory(factory function) -> void
{
  output_factory_ = function;
}

auto output::instantiate(configuration const& config, metadata const& user_parameters) -> unique_ptr<output>
{
  return output_factory_(config, std::move(user_parameters));
}

// LCOV_EXCL_START
TEST_CASE("determine_uri_scheme")
{
  CHECK(determine_uri_scheme("my_file") == uri_scheme::none);
  CHECK(determine_uri_scheme("my/relative/path") == uri_scheme::none);
  CHECK(determine_uri_scheme("/my/absolute/path") == uri_scheme::none);
  CHECK(determine_uri_scheme("my/co:on/path") == uri_scheme::none);  // colon's allowed in relative URIs but not in first segment
  CHECK(determine_uri_scheme("/my/co:on/path") == uri_scheme::none);

  CHECK(determine_uri_scheme("s3://my_file") == uri_scheme::s3);
  CHECK(determine_uri_scheme("s3://my/relative/path") == uri_scheme::s3);
  CHECK(determine_uri_scheme("s3crt://my_file") == uri_scheme::s3);
  CHECK(determine_uri_scheme("s3crt://my/relative/path") == uri_scheme::s3);

  // technically the two slash versions here are invalid file scheme uris but we allow them and treat as realative paths
  CHECK(determine_uri_scheme("file://my_file") == uri_scheme::file);
  CHECK(determine_uri_scheme("file://my/relative/path") == uri_scheme::file);
  CHECK(determine_uri_scheme("file:///my/absolute/path") == uri_scheme::file);

  CHECK_THROWS(determine_uri_scheme("bad://bad.bad"));
}
TEST_CASE("input")
{
  CHECK_THROWS(input::instantiate("type bad"_config, metadata{}));
}
// LCOV_EXCL_STOP
