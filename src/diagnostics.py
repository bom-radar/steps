#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
import sys
import datetime
import math
import matplotlib.animation as animation
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np

# constants
figsize=(8, 6)
figsize_sq=(6, 6)

# open the diagnostics file and setup all global data
def open_diagnostics(path):
    global cdf
    global nforecasts, nmodels, nstreams, nlevels, ny, nx, nsy, nsx
    global times, leads, models, streams, wavelengths

    # open the file
    cdf = nc.Dataset(sys.argv[1])

    # get dimension sizes
    nforecasts = cdf.dimensions["forecast"].size
    nmodels = cdf.dimensions["model"].size
    nstreams = cdf.dimensions["stream"].size
    nlevels = cdf.dimensions["level"].size
    ny = cdf.dimensions["y"].size
    nx = cdf.dimensions["x"].size
    nsy = cdf.dimensions["sy"].size
    nsx = cdf.dimensions["sx"].size

    # read coordinates
    times = read_times("time")
    times = times.astype(datetime.datetime)
    leads = cdf.variables["lead_time"][:] / 60
    models = cdf.variables["model"][:]
    streams = cdf.variables["stream"][:]
    wavelengths = cdf.variables["wavelength"][:]

# read a 1D array of times
def read_times(variable):
  var = cdf.variables[variable]
  try:
    calendar = var.calendar
  except AttributeError:
    calendar = u"gregorian"
  if len(var) == 0:
    return np.array([], dtype='datetime64[us]')
  return np.array(nc.num2date(var[:], units=var.units, calendar=calendar), dtype='datetime64[us]')

def plot_field(itime):
    plt.clf()
    plt.figure(figsize=(streams * 6, 6))
    for istream in range(streams):
        plt.subplot(1, streams, istream + 1)
        plt.title(stream_names[istream])
        plt.imshow(cdf.variables["field"][itime, istream], cmap='jet', vmin=0.0, vmax=7.5)
    plt.suptitle("Stream fields in native dataspace, forecast {}, {}".format(itime, times[itime]))
    plt.savefig("images/field_fc{}.png".format(itime))

def plot_flow(itime):
    plt.clf()
    plt.figure(figsize=(streams * 6, 6))
    for istream in range(streams):
        plt.subplot(1, streams, istream + 1)
        flow = cdf.variables["flow"][itime, istream]
        if False:
            # convert our U,V flow vectors into direction and magnitude
            mag = np.sqrt(flow[:,:,0]**2 + flow[:,:,1]**2)
            ang = np.arctan2(flow[:,:,1], flow[:,:,0])

            # convert polar coordinates to HSV, then to RGB
            hsv = np.zeros((512,512,3))
            hsv[..., 0] = np.where(ang < 0, ang / (np.pi * 2) + 1, ang / (np.pi * 2))
            hsv[..., 1] = mag / np.amax(mag) * 3
            hsv[..., 2] = 1 #mag / np.amax(mag) # TODO - normalize magnitude into 0..1 in a nice way
            rgb = colors.hsv_to_rgb(hsv)

            y = 200
            x = 100
            print("flow = {} {}", flow[y,x,0], flow[y,x,1])
            print(" mag = {} {}", mag[y,x], ang[y,x])
            print(" hsv = {} {} {}", hsv[y,x,0], hsv[y,x,1], hsv[y,x,2])
            print(" rgb = {} {} {}", rgb[y,x,0], rgb[y,x,1], rgb[y,x,2])

            plt.imshow(rgb)

        elif False:
            x = np.linspace(0, nx, nx)
            y = np.linspace(0, ny, ny)
            plt.imshow(cdf.variables["field"][itime, istream])
            plt.streamplot(x, y, flow[:,:,0], flow[:,:,1], density=2)

        elif True:
            gap = 20
            x = np.linspace(0, nx, nx)
            y = np.linspace(0, ny, ny)
            plt.imshow(cdf.variables["field"][itime, istream], cmap='ocean_r')
            plt.quiver(x[::gap], y[::gap], -flow[::gap,::gap,0], flow[::gap,::gap,1])
        plt.title(stream_names[istream])
    plt.suptitle("Flow fields, forecast {}, {}".format(itime, times[itime]))
    plt.savefig("images/flow_fc{}.png".format(itime))

def plot_xform(itime):
    plt.clf()
    plt.figure(figsize=(streams * 6, 6))
    for istream in range(streams):
        plt.subplot(1, streams, istream + 1)
        plt.title(stream_names[istream])
        plt.imshow(cdf.variables["xform"][itime, istream], cmap='jet', vmin=0.0, vmax=7.5)
    plt.suptitle("Stream fields in log dataspace, forecast {}, {}".format(itime, times[itime]))
    plt.savefig("images/xform_fc{}.png".format(itime))

# plot a histogram comparing input stream distributions without output in the native data space (i.e. mm/hr)
def plot_field_stream_histogram(itime):
    plt.clf()
    plt.figure(figsize=figsize)
    plt.suptitle("Histogram of stream fields in native dataspace")
    plt.title("Forecast {}, {}".format(itime, times[itime]))
    plt.xlabel("Value")
    plt.ylabel("Number of samples")
    plt.xscale('log')
    for istream in range(streams):
        plt.hist(np.ravel(cdf.variables["field"][itime, istream]), bins=80, histtype='step', label=stream_names[istream], log=True)
    plt.legend()
    plt.savefig("images/hist_field_fc{}.png".format(itime))

# plot a histogram comparing input stream distributions without output in the native data space (i.e. mm/hr)
def plot_xform_stream_histogram(itime):
    plt.clf()
    plt.figure(figsize=figsize)
    plt.suptitle("Histogram of streams in log dataspace")
    plt.title("Forecast {}, {}".format(itime, times[itime]))
    plt.xlabel("Value")
    plt.ylabel("Number of samples")
    for istream in range(streams):
        plt.hist(np.ravel(cdf.variables["xform"][itime, istream]), bins=80, histtype='step', label=stream_names[istream], log=True)
    plt.legend()
    plt.savefig("images/hist_xform_fc{}.png".format(itime))

# plot a histogram showing the distribution of input stream values at a particular cascade level
def plot_cascade_level_histogram(itime, ilevel):
    plt.clf()
    plt.figure(figsize=figsize)
    plt.suptitle("Histogram of cascade level {}".format(ilevel))
    plt.title("Forecast {}, {}".format(itime, times[itime]))
    plt.xlabel("Value")
    plt.ylabel("Number of samples")
    flat = np.concatenate((cdf.variables["cascade"][:, ilevel, :].flatten(), cdf.variables["noise"][:, ilevel].flatten()))
    bin_range = max(abs(np.amin(flat)), abs(np.amax(flat)))
    # do we actually need to run the histogram of 'flat' here to get the bins???
    bins = np.histogram(flat, bins=100, range=(-bin_range, bin_range))[1]
    for istream in range(streams):
        plt.hist(np.ravel(cdf.variables["cascade"][itime, ilevel, istream]), bins=bins, histtype='step', label=stream_names[istream])
    plt.hist(np.ravel(cdf.variables["noise"][itime, ilevel]), bins=bins, histtype='step', label="noise")
    plt.legend()
    plt.savefig("images/hist_lvl{}_fc{}.png".format(ilevel, itime))

# plot a histogram showing the distribution of input stream values at a particular cascade level
def plot_cascade_level_histogram_2(itime):
    plt.clf()
    plt.figure(figsize=(8 * 2, 6 * nlevels / 2))
    for ilevel in range(nlevels):
        plt.subplot(nlevels / 2, 2, ilevel + 1)
        plt.title("level {}".format(ilevel))
        plt.xlabel("Value")
        plt.ylabel("Number of samples")
        flat = np.concatenate((cdf.variables["cascade"][:, ilevel, :].flatten(), cdf.variables["noise"][:, ilevel].flatten()))
        bin_range = max(abs(np.amin(flat)), abs(np.amax(flat)))
        # do we actually need to run the histogram of 'flat' here to get the bins???
        bins = np.histogram(flat, bins=100, range=(-bin_range, bin_range))[1]
        for istream in range(streams):
            plt.hist(np.ravel(cdf.variables["cascade"][itime, ilevel, istream]), bins=bins, histtype='step', label=stream_names[istream])
        plt.hist(np.ravel(cdf.variables["noise"][itime, ilevel]), bins=bins, histtype='step', label="noise")
        plt.legend()
    plt.suptitle("Cascade histograms, forecast {}, {}".format(itime, times[itime]))
    plt.savefig("images/hist_cascade_fc{}.png".format(itime))

# plot histogram showing the distribution of values
def plot_cascade_stream_histogram(itime, istream):
    plt.clf()
    plt.figure(figsize=figsize)
    plt.suptitle("Histogram of cascade for stream {}".format(stream_names[istream]))
    plt.title("Forecast {}, {}".format(itime, times[itime]))
    plt.xlabel("Value")
    plt.ylabel("Number of samples")
    # should we do a min/max like above???
    bins = np.histogram(cdf.variables["cascade"][:, :, istream], bins=100)[1]
    for ilevel in range(nlevels):
        plt.hist(np.ravel(cdf.variables["cascade"][itime, ilevel, istream]), bins=bins, histtype='step', label="{}".format(ilevel), log=True)
    plt.legend()
    plt.savefig("images/hist_{}_fc{}.png".format(stream_names[istream], itime))

# plot autocorrelations verses time for a particular stream
def plot_autocorrelations(istream):
    plt.clf()
    plt.figure(figsize=figsize)
    plt.title("Autocorrelation for stream {}".format(stream_names[istream]))
    plt.xlabel("Forecast")
    plt.xlim(0, times - 1)
    plt.ylabel("Autocorrelation")
    plt.ylim(0, 1)
    cmap = plt.get_cmap('jet_r')
    for ilevel in range(nlevels):
        color = cmap(float(ilevel) / nlevels)
        plt.plot(range(times), cdf.variables["autocor"][:, ilevel, istream, 0], label="Level {} Lag 1".format(ilevel), c=color)
        plt.plot(range(times), cdf.variables["autocor"][:, ilevel, istream, 1], label="Level {} Lag 2".format(ilevel), c=color, linestyle='dashed')
    plt.legend()
    plt.savefig("images/autocor_{}.png".format(stream_names[istream]))

def plot_skills(var):
    height = 0.25 + 2
    plt.clf()
    fig, ax = plt.subplots(1, 1, figsize=(8, height), sharex=True)
    for istream in range(nstreams):
        ax.plot(leads, cdf.variables[var][:, istream], label=streams[istream])
    ax.set_ylim(0, 1)
    ax.legend(ncol=nstreams, loc='lower center', bbox_to_anchor=(0.5, 1.0))
    ax.set_xlim(leads[1], leads[nforecasts - 1])
    ax.set_xlabel("Lead time (min)")
    fig.suptitle(cdf.variables[var].long_name, size=18, y=(1 - 0.1 / height))
    fig.tight_layout(rect=(0, 0, 1, 1 - 0.25 / height))
    plt.savefig("images/{}.png".format(var))

def plot_level_skills(var):
    # extra 0.25in in height for the suptitle
    # we use rect in tight_layout to remove that same 0.25in to prevent collision between plots and title
    height = 0.25 + 2 * nlevels
    plt.clf()
    fig, ax = plt.subplots(nlevels, 1, figsize=(8, height), squeeze=False, sharex=True)
    for ilevel in range(nlevels):
        for istream in range(nstreams):
            ax[ilevel][0].plot(leads, cdf.variables[var][:, ilevel, istream], label=streams[istream])
        ax[ilevel][0].set_ylim(0, 1)
        ax[ilevel][0].set_title("Level {}".format(ilevel), loc='left')
        ax[ilevel][0].set_title("~{:0.0f} km".format(wavelengths[ilevel]), loc='right')
        if (ilevel == 0):
            ax[ilevel][0].legend(ncol=nstreams, loc='lower center', bbox_to_anchor=(0.5, 1.0))
        if (ilevel == nlevels - 1):
            ax[ilevel][0].set_xlim(leads[1], leads[nforecasts - 1])
            ax[ilevel][0].set_xlabel("Lead time (min)")
    fig.suptitle(cdf.variables[var].long_name, size=18, y=(1 - 0.1 / height))
    fig.tight_layout(rect=(0, 0, 1, 1 - 0.25 / height))
    plt.savefig("images/{}.png".format(var))

def plot_weights(var):
    height = 0.25 + 2
    plt.clf()
    fig, ax = plt.subplots(1, 1, figsize=(8, height), sharex=True)
    for istream in range(nstreams):
        ax.plot(leads, cdf.variables[var][:, istream], label=streams[istream])
    ax.plot(leads, 1 - np.sum(cdf.variables[var][:], -1), label='residual')
    ax.legend(ncol=nstreams + 1, loc='lower center', bbox_to_anchor=(0.5, 1.0))
    ax.set_xlim(leads[1], leads[nforecasts - 1])
    ax.set_xlabel("Lead time (min)")
    fig.suptitle(cdf.variables[var].long_name, size=18, y=(1 - 0.1 / height))
    fig.tight_layout(rect=(0, 0, 1, 1 - 0.25 / height))
    plt.savefig("images/{}.png".format(var))

def plot_level_weights(var):
    # extra 0.25in in height for the suptitle
    # we use rect in tight_layout to remove that same 0.25in to prevent collision between plots and title
    height = 0.25 + 2 * nlevels
    plt.clf()
    fig, ax = plt.subplots(nlevels, 1, figsize=(8, height), squeeze=False, sharex=True)
    for ilevel in range(nlevels):
        for istream in range(nstreams):
            ax[ilevel][0].plot(leads, cdf.variables[var][:, ilevel, istream], label=streams[istream])
        ax[ilevel][0].plot(leads, 1 - np.sum(cdf.variables[var][:, ilevel], -1) , label='residual')
        ax[ilevel][0].set_title("Level {}".format(ilevel), loc='left')
        ax[ilevel][0].set_title("~{:0.0f} km".format(wavelengths[ilevel]), loc='right')
        if (ilevel == 0):
            ax[ilevel][0].legend(ncol=nstreams + 1, loc='lower center', bbox_to_anchor=(0.5, 1.0))
        if (ilevel == nlevels - 1):
            ax[ilevel][0].set_xlim(leads[1], leads[nforecasts - 1])
            ax[ilevel][0].set_xlabel("Lead time (min)")
    fig.suptitle(cdf.variables[var].long_name, size=18, y=(1 - 0.1 / height))
    fig.tight_layout(rect=(0, 0, 1, 1 - 0.25 / height))
    plt.savefig("images/{}.png".format(var))

def plot_field_animation(var):
    fig = plt.figure(figsize=(6,6))
    ax = fig.gca()
    fig.suptitle(cdf.variables[var].long_name, size=18)
    ts = ax.set_title("{}".format(times[0]), loc='left')
    ld = ax.set_title("T+{}".format(leads[0]), loc='right')
    im = ax.imshow(cdf.variables[var][1], cmap='viridis', vmin=0.0, vmax=10)
    ax.grid(alpha=0.5, linestyle='--')
    fig.tight_layout(rect=(0, 0, 1, 0.95))
    def animate(i):
        i = i + 1
        ts.set_text("{:%Y-%m-%d %H:%M:%S} UTC".format(times[i]))
        ld.set_text("T+{:0.0f}".format(leads[i]))
        im.set_data(cdf.variables[var][i])
        return [ts, ld, im]
    anim = animation.FuncAnimation(fig, animate, frames=nforecasts - 1, interval=20, repeat=True, repeat_delay=500)
    anim.save('images/{}.mp4'.format(var), fps=3, extra_args=['-vcodec', 'libx264'])

def plot_blend_animation():
    fig, ax = plt.subplots(1, nmodels + 1, figsize=((nmodels + 1) * 6, 6), squeeze=False)
    fig.suptitle("Model Inputs vs STEPS", size=18)
    im = [None] * (nmodels + 1)
    for imodel in range(nmodels):
        ax[0][imodel].grid(alpha=0.5, linestyle='--')
        ax[0][imodel].set_title(models[imodel])
        im[imodel] = ax[0][imodel].imshow(cdf.variables["mod-fld-0"][0, imodel], cmap='viridis', vmin=0.0, vmax=10)
    ax[0][nmodels].grid(alpha=0.5, linestyle='--')
    ax[0][nmodels].set_title("STEPS")
    ts = ax[0][nmodels].set_title("{}".format(times[0]), loc='left')
    ld = ax[0][nmodels].set_title("T+{}".format(leads[0]), loc='right')
    im[nmodels] = ax[0][nmodels].imshow(cdf.variables["output"][1], cmap='viridis', vmin=0.0, vmax=10)
    fig.tight_layout(rect=(0.02, 0.02, 0.98, 0.93))
    def animate(i):
        i = i + 1
        ts.set_text("{:%Y-%m-%d %H:%M:%S} UTC".format(times[i]))
        ld.set_text("T+{:0.0f}".format(leads[i]))
        for imodel in range(nmodels):
            im[imodel].set_data(cdf.variables["mod-fld-0"][i, imodel])
        im[nmodels].set_data(cdf.variables["output"][i])
        return [ts, ld]
    anim = animation.FuncAnimation(fig, animate, frames=nforecasts - 1, interval=20, repeat=True, repeat_delay=500)
    anim.save('images/blend.mp4', fps=3, extra_args=['-vcodec', 'libx264'])

# entry point
if __name__ == '__main__':

    if (len(sys.argv) != 2):
        print("Usage: python diagnostics.py (input.nc)")
        exit(0)

    open_diagnostics(sys.argv[1])

    plot_blend_animation()
    plot_field_animation("output")

    plot_skills("skills_f")
    plot_level_skills("skills_p")
    plot_level_skills("skills_c")

    plot_weights("weight_f")
    plot_level_weights("weight_p")
    plot_level_weights("weight_c")

#    for istream in range(streams - 1):
#        plot_skill_stream(istream)
#
#    for istream in range(streams):
#        plot_autocorrelations(istream)
#
#    for itime in range(times):
#        plot_field(itime)
#        plot_flow(itime)
#        plot_xform(itime)
#        plot_field_stream_histogram(itime)
#        plot_xform_stream_histogram(itime)
#        plot_cascade_level_histogram_2(itime)
#        for istream in range(streams):
#            plot_cascade_stream_histogram(itime, istream)
#        #for ilevel in range(nlevels):
#        #    plot_cascade_level_histogram(itime, ilevel)
