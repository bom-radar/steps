/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "persistent_cache.h"
#include "io.h"
#include "unit_test.h"
#if STEPS_WITH_AWS
#include "aws.h"
#endif

#include <fstream>

using namespace steps;

static persistent_cache::factory persistent_cache_factory_ = [](
      string_view name
    , time_point reference_time
    , time_point bf_reference_time
    , string_view path
    , int compression
    , int precision_reduction
    ) -> unique_ptr<persistent_cache>
{
  try
  {
    if (path.empty())
      return std::make_unique<persistent_cache_none>(
          name, reference_time, bf_reference_time, path, compression, precision_reduction);
    switch (determine_uri_scheme(path))
    {
    case uri_scheme::none:
      return std::make_unique<persistent_cache_local>(
          name, reference_time, bf_reference_time, path, compression, precision_reduction);
    case uri_scheme::file:
      return std::make_unique<persistent_cache_local>(
          name, reference_time, bf_reference_time, strip_file_schema(path), compression, precision_reduction);
    case uri_scheme::s3:
#if STEPS_WITH_AWS
      return std::make_unique<aws::persistent_cache_s3>(
          name, reference_time, bf_reference_time, path, compression, precision_reduction);
#else
      throw std::runtime_error{"AWS support disabled"};
#endif
    default:
      throw std::runtime_error{"Unsupported URI schema"};
    }
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to configure persistent cache for path {}", path)});
  }
};

auto persistent_cache::set_factory(factory function) -> void
{
  persistent_cache_factory_ = function;
}

auto persistent_cache::instantiate(
      string_view stream_name
    , time_point reference_time
    , time_point bf_reference_time
    , string_view path
    , int compression
    , int precision_reduction
    ) -> unique_ptr<persistent_cache>
{
  return persistent_cache_factory_(stream_name, reference_time, bf_reference_time, path, compression, precision_reduction);
}

persistent_cache_none::persistent_cache_none(
      string_view stream_name
    , time_point reference_time
    , time_point bf_reference_time
    , string_view path
    , int compression
    , int precision_reduction)
{ }

auto persistent_cache_none::write(cache_key const& key, stream_state const& state) -> void
{ }

auto persistent_cache_none::read(cache_key const& key, stream_state& state) -> bool
{
  return false;
}

persistent_cache_local::persistent_cache_local(
      string_view stream_name
    , time_point reference_time
    , time_point bf_reference_time
    , string_view path
    , int compression
    , int precision_reduction)
  : base_dir_{path}
  , compression_{compression}
  , precision_reduction_{precision_reduction}
{
  base_dir_.append("/"sv).append(stream_name);
  if (reference_time != time_point{})
    base_dir_.append(format(".{:%Y%m%dT%H%M%S}", reference_time));
  if (bf_reference_time != time_point{})
    base_dir_.append(format(".{:%Y%m%dT%H%M%S}", bf_reference_time));
  std::filesystem::create_directories(base_dir_);
}

auto persistent_cache_local::write(cache_key const& key, stream_state const& state) -> void
{
  auto path = format("{}/{:%Y%m%dT%H%M%S}.{}.{}", base_dir_, key.time, key.nwp_member, key.bf_nwp_member);
  try
  {
    std::ofstream file;
    file.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    file.open(path, std::ofstream::binary | std::ofstream::trunc);
    state.write(file, compression_, precision_reduction_);
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to write cache file {}", path)});
  }
}

auto persistent_cache_local::read(cache_key const& key, stream_state& state) -> bool
{
  auto path = format("{}/{:%Y%m%dT%H%M%S}.{}.{}", base_dir_, key.time, key.nwp_member, key.bf_nwp_member);
  try
  {
    if (auto file = std::ifstream{path, std::ifstream::binary})
    {
      file.exceptions(std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
      state.read(file);
      return true;
    }
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to read cache file {}", path)});
  }

  return false;
}

// LCOV_EXCL_START
TEST_CASE("cache_key")
{
  auto a = cache_key{parse<time_point>("2021-01-01T00:00:00Z"), 0, 0};
  auto b = cache_key{parse<time_point>("2021-01-01T00:00:00Z"), 1, 0};
  auto c = cache_key{parse<time_point>("2022-01-01T00:00:00Z"), 0, 0};
  auto d = cache_key{parse<time_point>("2022-01-01T00:00:00Z"), 1, 0};
  auto e = cache_key{parse<time_point>("2022-01-01T00:00:00Z"), 1, 1};

  // operator==
  CHECK(a == a);
  CHECK(!(a == b));
  CHECK(!(a == c));
  CHECK(!(a == d));

  // operator<(key)
  CHECK(a < b);
  CHECK(a < c);
  CHECK(a < d);
  CHECK(b < c);
  CHECK(d < e);

  // operator<(time)
  CHECK(a < c.time);
  CHECK(a.time < c);
}
TEST_CASE("persistent_cache_none")
{
  auto state = stream_state{1, vec2i{1,2}, vec2i{3,4}};
  auto cache = persistent_cache::instantiate("name", time_point{}, time_point{}, "", 0, 0);
  auto key = cache_key{time_point{}, 0, 0};
  CHECK_NOTHROW(cache->write(key, state));
  CHECK(cache->read(key, state) == false);
}
TEST_CASE("persistent_cache_local")
{
  auto path = std::filesystem::temp_directory_path() / "steps.tmp";
  REQUIRE(!std::filesystem::exists(path));

  auto ts1 = parse<time_point>("2021-01-01T00:00:00Z");
  auto ts2 = parse<time_point>("2021-01-02T00:00:00Z");
  auto ts3 = parse<time_point>("2023-01-01T00:00:00Z");
  auto state = stream_state{1, vec2i{1,2}, vec2i{3,4}};

  auto cache = persistent_cache::instantiate("foo", ts1, time_point{}, path.native(), 0, 0);

  CHECK(cache->read(cache_key{ts2, 1, 0}, state) == false);

  state.field.fill(111.0f);
  CHECK_NOTHROW(cache->write(cache_key{ts2, 1, 0}, state));

  state.field.fill(222.0f);
  CHECK_NOTHROW(cache->write(cache_key{ts3, 1, 0}, state));

  CHECK(cache->read(cache_key{ts2, 1, 0}, state) == true);
  CHECK(state.field[0][0] == approx(111.0f));

  std::filesystem::remove_all(path);
}
// LCOV_EXCL_STOP
