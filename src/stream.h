/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "configuration.h"
#include "io.h"
#include "persistent_cache.h"
#include "skills_climatology.h"
#include "stream_state.h"
#include <condition_variable>
#include <map>
#include <mutex>
#include <tuple>

namespace steps
{
  class stream
  {
  public:
    stream(core& c, string name, configuration const& config, bool observation);

    /// Move constructor
    /** The only reason the stream is move constructible is to allow storage in a vector at startup of the core.
     *  In reality it will never be called because we reserve space in this vector before emplacing the streams.
     *
     *  It is critical that a stream never be moved once the model has started runnning in earnset since the mutex
     *  and condition variable cannot be moved and are instead recreated.  If you somehow moved a stream while
     *  another thread was waiting on it bad things would happen. */
    stream(stream&& rhs);

    /// Get the name of the stream
    auto& name() const { return name_; }

    /// Get the halflife for diagnosed skills
    auto skills_halflife() const { return skills_halflife_; }

    /// Load skill climatologies
    auto load_skills(configuration const& data) -> void;

    /// Store skill climatologies
    auto store_skills(configuration& data) const -> void;

    /// Add a diagnosed skill set into the climatology
    /** Thread safe */
    auto update_skills(
          int member
        , time_point time
        , float spatial
        , span<float const> spectral
        , float motion
        ) -> void;

    /// Estimate skills based on the climatology
    /** Thread safe */
    auto lookup_skills(
          int member
        , time_point time
        , float& spatial
        , span<float> spectral
        , float& motion
        ) const -> void;

    /// Notify the stream of the range of times currently being forecast
    /** This function is provided as a way for streams to minimize the memory consumed by shared resources such as
     *  cascades from streams which are constant over all members (e.g. NWP).  Resource optimization based on this
     *  range is not compulsory.
     *
     *  The 'from' and 'till' parameters represent the inclusive bounds of times which one or more engines are
     *  currently forecasting.  It is possible for from to be greater than till indicating that the range of times
     *  extends from 'till' to the last forecast and from the first forecast to 'from'.  This is due to the fact
     *  that an engine may complete a member and start work on the next, while other engines are still completing
     *  the previous member.
     *
     *  Called during model run from engine threads.  Thread safe. */
    auto notify_active_time_range(time_point from, time_point till) -> void;

    /// Get the stream state for a given member and time
    /** Thread safe */
    auto determine_state(int member, time_point time) -> shared_ptr<stream_state const>;

    /// Read the raw input field and apply backfill if configured
    /** This is mostly used internally, but is public to allow the core to 'preview' the observation data so that
     *  it can decide whether to run the full forecast or output a null product set. */
    [[nodiscard]] auto read_and_backfill(int member, time_point time, array2f& field) -> bool;

  private:
    /* The bool in the pair is a flag to indicate whether the stream state has been initialized.  If a entry in
     * the cache exists and this flag is set to false, it indicates another thread is already initializing the
     * entry.  Threads which find a cache entry in this state will block until the flag is set to true. */
    using state_store = std::map<cache_key, pair<bool, shared_ptr<stream_state const>>, std::less<>>;
    using skill_update_record = vector<cache_key>;
    using persistent_cache_ptr = unique_ptr<persistent_cache>;

  private:
    auto check_memory_cache(cache_key const& key) -> shared_ptr<stream_state const>;

    [[nodiscard]] auto diagnose_state_cutdown(int member, time_point time, stream_state& state) -> bool;
    [[nodiscard]] auto diagnose_state_full(int member, time_point time, stream_state& state) -> bool;

  private:
    core*                   core_;              // parent model core

    string                  name_;              // name of stream
    time_point              reference_time_;    // reference time for ensemble NWP based inputs
    array1i                 nwp_members_;       // ensemble NWP input member numbers to use for each STEPS member
    unique_ptr<input>       input_;             // main input manager
    time_point              bf_reference_time_; // reference time for backfill input (optional)
    array1i                 bf_nwp_members_;    // NWP input members for backfill input (optional)
    unique_ptr<input>       bf_input_;          // input manager used to fill in missing data (optional)
    persistent_cache_ptr    persistent_cache_;  // persistent cache manager
    array1f                 stddev_scale_;      // scale factor applied to local stddev of each cascade level
    float                   flow_scale_;        // systematic bias correction factor for diagnosed flow field
    float                   psd_smooth_;        // time smoothing factor for PSD
    float                   ac_smooth_;         // time smoothing factor for autocorrelation diagnosis
    duration                skills_halflife_;   // time after which diagnosed skills should be faded half way to climatology

    mutable std::mutex      mut_skills_;        // mutex protecting the skills climatology
    skill_update_record     skill_updates_;     // record of previous skill updates used to ignore duplicates
    skills_climatology      skills_spatial_;    // climatology of model skills for representing spatial structure of cascade levels
    skills_climatology      skills_spectral_;   // climatology of model skills for representing spectrum of cascade levels
    skills_climatology      skills_motion_;     // climatology of model skill for representing flow field

    std::mutex              mut_cache_;         // mutex protecting the in memory cache
    state_store             cache_;             // in-memory cache of stream states
    std::condition_variable cv_ready_;          // condition variable used to notify engines that a state is now ready
  };
}
