/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "compression.h"
#include "unit_test.h"
#include <zlib.h>
#include "trace.h"

using namespace steps;

static_assert(sizeof(Bytef) == 1, "zlib Bytef is not 1 byte");

auto steps::zlib_parse_level(string_view str) -> int
try
{
  return str == "none" ? -1 : parse<int>(str);
}
catch (...)
{
  throw std::runtime_error{format("Invalid compression level '{}', must be 'none' or 0..9.", str)};
}

static auto zlib_error_desc(int error)
{
  switch (error)
  {
  case Z_BUF_ERROR:
    return "destination buffer too small";
  case Z_MEM_ERROR:
    return "out of memory";
  case Z_DATA_ERROR:
    return "data corruption detected";
  case Z_STREAM_ERROR:
    return "invalid compression level";
  default:
    return "unknown error";
  }
}

/* IMPORTANT: We use this function to manually pre-compress chunks of data that we will write to HDF5 variables as
 * part of the NetCDF outputs.  We do this to work around a lack of thread efficiency in the NetCDF API by performing
 * compression in our own threads to prevent it being serialized internally by NetCDF and HDF5 APIs.  As a result, the
 * serial nature of those APIs does far less damage since compression time was their main bottleneck.
 *
 * This means that the implementation of this function MUST match the way that HDF5 compresses its chunks internally.
 * If you want to change this function in a way that the output is no longer consistent with the HDF5 approach, you'll
 * need to maintain two versions - one for use with the NetCDF compression workaround, and one for general use. */
auto steps::zlib_compress(span<char const> data, int level, int offset) -> pair<array1<char>, int>
{
  auto csize = compressBound(data.size_bytes());
  auto cbuf = array1<char>{int(csize) + offset};
  auto res = compress2(
        reinterpret_cast<Bytef*>(cbuf.data() + offset)
      , &csize
      , reinterpret_cast<Bytef const*>(data.data())
      , data.size_bytes()
      , level);
  if (res != Z_OK)
    throw std::runtime_error{format("Compression failed: {}", zlib_error_desc(res))};
  return {std::move(cbuf), csize + offset};
}

auto steps::zlib_decompress(span<char const> chunk, span<char> data) -> int
{
  auto ucsize = uLongf(data.size_bytes());
  auto res = uncompress(
        reinterpret_cast<Bytef*>(data.data())
      , &ucsize
      , reinterpret_cast<Bytef const*>(chunk.data())
      , chunk.size_bytes());
  if (res != Z_OK)
    throw std::runtime_error{format("Decompression failed: {}", zlib_error_desc(res))};
  return ucsize;
}

// LCOV_EXCL_START
TEST_CASE("compression")
{
  CHECK_THROWS(zlib_parse_level("bad"s));
  CHECK(zlib_parse_level("none") == -1);
  CHECK(zlib_parse_level("5") == 5);

  // we don't really care what these descriptions are, as long as they are not 'unknown'
  CHECK(zlib_error_desc(Z_BUF_ERROR) != "unknown error");
  CHECK(zlib_error_desc(Z_MEM_ERROR) != "unknown error");
  CHECK(zlib_error_desc(Z_DATA_ERROR) != "unknown error");
  CHECK(zlib_error_desc(Z_STREAM_ERROR) != "unknown error");
  CHECK(zlib_error_desc(Z_OK) == "unknown error");

  auto data = array1<char>{128};
  for (auto i = 0; i < data.size(); ++i)
    data[i] = i % 10;
  auto cdata = zlib_compress(data, 5);
  auto ucdata = array1<char>{128};
  CHECK(zlib_decompress(span{cdata.first.data(), cdata.second}, ucdata) == 128);
  CHECK_ALL(data, ucdata, lhs == rhs);
}
// LCOV_EXCL_STOP
