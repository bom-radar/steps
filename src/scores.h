/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include <random>

namespace steps
{
  /// Flags to indicate occurrence of a forecast event
  enum class event_occurrence
  {
      no
    , yes
    , unknown
  };

  /// Detect events indicated by observation below a threshold
  void generate_occurrence_field_low_detect(
        span<float const> observations
      , float threshold
      , span<event_occurrence> occurrence);

  /// Detect events indicated by observation above a threshold
  void generate_occurrence_field_high_detect(
        span<float const> observations
      , float threshold
      , span<event_occurrence> occurrence);

  /// Detect events indicated by observation within a band
  void generate_occurrence_field_band_detect(
        span<float const> observations
      , float threshold_low
      , float threshold_high
      , span<event_occurrence> occurrence);

  /// Detect events indicated by observation below a spatially varying threshold
  void generate_occurrence_field_low_detect(
        span<float const> observations
      , span<float const> threshold
      , span<event_occurrence> occurrence);

  /// Detect events indicated by observation above a spatially varying threshold
  void generate_occurrence_field_high_detect(
        span<float const> observations
      , span<float const> threshold
      , span<event_occurrence> occurrence);

  /// Detect events indicated by observation within a spatially varying band
  void generate_occurrence_field_band_detect(
        span<float const> observations
      , span<float const> threshold_low
      , span<float const> threshold_high
      , span<event_occurrence> occurrence);

  /// Generate a probability forecast from an ensemble
  class ensemble_probability
  {
  public:
    ensemble_probability(vec2i extents, bool initialize = true);

    auto reset() -> void;
    auto add_member(array2<event_occurrence> const& member) -> void;
    auto finalize() -> void;

    auto& probabilities() const { return probabilities_; }

  private:
    int     members_;
    array2f probabilities_;
  };

  /// Rank histogram ensemble evaluation model
  class rank_histogram
  {
  public:
    rank_histogram(int members, bool initialize = true);

    auto reset() -> void;
    auto aggregate(rank_histogram const& other) -> void;
    auto update(span<array2f const> ensemble, array2f const& truth) -> void;

    auto bin_count() const  { return counts_.size(); }

    auto& counts()          { return counts_; }
    auto& counts() const    { return counts_; }

  private:
    array1l       counts_;
    std::mt19937  rnd_;
  };

  /// Continuous Ranked Probability Score (CRPS) ensemble evaluation model
  class crps
  {
  public:
    crps(vec2i extents, bool initialize = true);

    auto reset() -> void;
    auto aggregate(crps const& other) -> void;
    auto update(span<array2f const> ensemble, array2f const& truth) -> void;

    auto shape() { return samples_.shape(); }

    auto samples() -> array2i&             { return samples_; }
    auto samples() const -> array2i const& { return samples_; }

    auto spatial_crps() -> array2f&                { return crps_; }
    auto spatial_crps() const -> array2f const&    { return crps_; }

    auto domain_crps() const -> float;

  private:
    array2i samples_;
    array2f crps_;
  };

  /// Spread-reliability ensemble forecast evaluation model
  class spread_reliability
  {
  public:
    spread_reliability(int bins, float max_error);

    auto reset() -> void;
    auto aggregate(spread_reliability const& other) -> void;
    auto update(span<array2f const> ensemble, array2f const& truth) -> void;

    auto bin_count() const                      { return bins_.size(); }
    auto rms_error_threshold(int bin) const     { return bins_[bin].threshold; }

    auto samples(int bin) const                 { return bins_[bin].samples; }
    auto set_samples(int bin, long val)         { bins_[bin].samples = val; }

    auto square_spread(int bin) const           { return bins_[bin].sqr_spread; }
    auto set_square_spread(int bin, double val) { bins_[bin].sqr_spread = val; }

    auto rms_error(int bin) const               { return bins_[bin].mid_point; }
    auto rms_spread(int bin) const              { return std::sqrt(bins_[bin].sqr_spread / bins_[bin].samples); }

  private:
    struct bin
    {
      float   threshold;
      float   mid_point;
      long    samples;
      double  sqr_spread;
    };

  private:
    array1<bin> bins_;
  };

  /// Reliability curve for ensemble forecast evalutation
  class reliability_curve
  {
  public:
    reliability_curve(int bins);

    auto reset() -> void;
    auto aggregate(reliability_curve const& other) -> void;
    auto update(span<event_occurrence const> observations, span<float const> probabilities) -> void;

    auto bin_count() const                            { return bins_.size(); }

    /// Central probability of bin
    auto forecast_probability(int bin) const          { return bins_[bin].probability; }

    auto forecasts(int bin) const                     { return bins_[bin].forecasts; }
    auto set_forecasts(int bin, long val)             { bins_[bin].forecasts = val; }
    auto occurrences(int bin) const                   { return bins_[bin].occurrences; }
    auto set_occurrences(int bin, long val)           { bins_[bin].occurrences = val; }

    auto observed_frequency(int bin) const -> float
    {
      return double(bins_[bin].occurrences) / bins_[bin].forecasts;
    }

    auto climatology_probability() const -> float;

  private:
    struct bin
    {
      float threshold;    // upper probability limit for this band
      float probability;  // middle of probability band (ie: x coordinate)
      long  forecasts;    // number of forecasts encountered in this probability band
      long  occurrences;  // number of times event occurred at this probability
    };

  private:
    array1<bin> bins_;
  };

  /// Relative Operating Characteristic (ROC) ensemble evaluation model
  class roc_curve
  {
  public:
    roc_curve(int points);

    auto reset() -> void;
    auto aggregate(roc_curve const& other) -> void;
    auto update(span<event_occurrence const> observations, span<float const> probabilities) -> void;

    auto point_count() const                        { return points_.size(); }

    auto non_occurrences() const                    { return non_occurrences_; }
    auto set_non_occurrences(int val)               { non_occurrences_ = val; }
    auto occurrences() const                        { return occurrences_; }
    auto set_occurrences(int val)                   { occurrences_ = val; }

    auto probability_threshold(int point) const     { return points_[point].threshold; }
    auto false_alarms(int point) const              { return points_[point].false_alarms; }
    auto set_false_alarms(int point, long val)      { points_[point].false_alarms = val; }
    auto hits(int point) const                      { return points_[point].hits; }
    auto set_hits(int point, long val)              { points_[point].hits = val; }

    auto probability_of_detection(int point) const  { return double(points_[point].hits) / occurrences_; }

    auto false_alarm_rate(int point) const          { return double(points_[point].false_alarms) / non_occurrences_; }

    auto area() const -> float;

  private:
    struct point
    {
      float threshold;
      long  false_alarms;
      long  hits;
    };

  private:
    long          non_occurrences_;
    long          occurrences_;
    array1<point> points_;
  };

  /// RMSE of ensemble mean vs RMS spread
  class rmse
  {
  public:
    rmse();

    auto reset() -> void;
    auto aggregate(rmse const& other) -> void;
    auto update(span<array2f const> ensemble, array2f const& truth) -> void;

    auto count() const                              { return count_; }
    auto set_count(long val)                        { count_ = val; }

    auto mean_square_error() const                  { return ms_error_; }
    auto set_mean_square_error(double val)          { ms_error_ = val; }

    auto mean_square_spread() const                 { return ms_spread_; }
    auto set_mean_square_spread(double val)         { ms_spread_ = val; }

    auto rmse_ensemble_mean() const                 { return std::sqrt(ms_error_); }
    auto rms_ensemble_spread() const                { return std::sqrt(ms_spread_); }

  private:
    long    count_;
    double  ms_error_;
    double  ms_spread_;
  };

  /// Average correlation coefficient
  class average_correlation
  {
  public:
    average_correlation();

    auto reset() -> void;
    auto aggregate(average_correlation const& other) -> void;
    auto update(span<array2f const> ensemble, array2f const& truth) -> void;

    auto count() const                              { return count_; }
    auto set_count(long val)                        { count_ = val; }

    auto mean_correlation() const                   { return mean_; }
    auto set_mean_correlation(double val)           { mean_ = val; }

  private:
    long    count_;
    double  mean_;
  };
}
