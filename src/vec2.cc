/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "vec2.h"
#include "unit_test.h"

using namespace steps;

// LCOV_EXCL_START
TEST_CASE("vec2 angles and magnitudes")
{
  // vec2f
  auto checkf = [&](float x, float y, float angle, float mag)
  {
    auto a = vec2f{x, y};
    CHECK(a.magnitude() == approx(mag));
    CHECK(a.angle() == approx(degrees_to_radians(angle)));

    auto b = vec2_angle_magnitude<float>(degrees_to_radians(angle), mag);
    CHECK(b.x == approx(x));
    CHECK(b.y == approx(y));
  };
  checkf(0, 10, 0, 10);
  checkf(10, 10, 45, std::sqrt(10 * 10 * 2));
  checkf(10, 0, 90, 10);
  checkf(10, -10, 135, std::sqrt(10 * 10 * 2));
  checkf(0, -10, 180, 10);
  checkf(-10, -10, 225, std::sqrt(10 * 10 * 2));
  checkf(-10, 0, -90, 10);
  checkf(-10, 10, -45, std::sqrt(10 * 10 * 2));

  // vec2i
  auto checki = [&](int x, int y, float angle, float mag)
  {
    auto a = vec2i{x, y};
    CHECK(a.magnitude() == approx(mag));
    CHECK(a.angle() == approx(degrees_to_radians(angle)));
  };
  checki(0, 10, 0, 10);
  checki(10, 10, 45, std::sqrt(10 * 10 * 2));
  checki(10, 0, 90, 10);
  checki(10, -10, 135, std::sqrt(10 * 10 * 2));
  checki(0, -10, 180, 10);
  checki(-10, -10, 225, std::sqrt(10 * 10 * 2));
  checki(-10, 0, -90, 10);
  checki(-10, 10, -45, std::sqrt(10 * 10 * 2));
}
TEST_CASE("vec2 parse")
{
  CHECK(parse<vec2i>("10 \t\r\n \t\r\n-33") == vec2i{10, -33});
  CHECK_THROWS(parse<vec2i>("10-33"));
  CHECK_THROWS(parse<vec2i>("10 "));
}
TEST_CASE("vec2 format")
{
  CHECK(format("{}", vec2i{-3, 7}) == "-3 7");
  CHECK(format("{:x}", vec2i{10, 16}) == "a 10");
}
// LCOV_EXCL_STOP
