/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array2.h"
#include "options.h"
#include "vec2.h"
#include <complex>
#include <filesystem>

namespace steps
{
  class fourier_transform
  {
  public:
#if opt_fftw_single_precision
    using real = float;
#else
    using real = double;
#endif
    using array2r = array2<real>;
    using array2c = array2<std::complex<real>>;

  public:
    /// Create a fourier transformer for a given spatial shape
    /** The underlying FFT library will be allowed to launch and use up to the given number of threads.  These threads
     *  currently in addition to the standard thread pool so be careful.  For example, if you have 8 worker threads
     *  that try to run the fourier transform at the same time, then each of those threads will launch an extra N-1
     *  threads.  It is probably easy to generate too many threads.
     *
     *  When we update to FFTW 3.3.9 or later then we will have access to the fftw_threads_set_callback() function
     *  that will allow us to distribute FFT work onto our main thread pool.  This will eliminate the risk of too many
     *  threads being created at once.  Until then, it is suggested to keep this thread count quite low.
     *
     *  A value of 1 effectively disables multi-threading in the FFT library so that the entire calculation is performed
     *  on the calling thread. */
    fourier_transform(vec2i shape, int threads);

    /// Get the shape of the real array
    auto shape_r() const { return shape_r_; }

    /// Get the shape of the complex array
    auto shape_c() const { return shape_c_; }

    /// Transform from real (spatial) into complex (spectral)
    auto execute_forward(array2r const& src, array2c& dst) const -> void;

    /// Transform from complex (spectral) into real (spatial)
    auto execute_inverse(array2c& src, array2r& dst) const -> void;

  public:
    /// Set the maximum number of threads for the underlying FFT library
    static auto set_threads(int threads) -> void;

    /// Load FFTW wisdom from a file
    static auto load_wisdom(std::filesystem::path const& path) -> void;

    /// Save current FFTW wisdom to a file
    static auto save_wisdom(std::filesystem::path const& path) -> void;

    /// Set whether to perform expensive measurements when planning the FFT
    static auto set_plan_exhaustive(bool enabled) -> void;

  private:
    using plan_handle = std::unique_ptr<void, void(*)(void*)>;

  private:
    vec2i       shape_r_;
    vec2i       shape_c_;
    plan_handle plan_r2c_;
    plan_handle plan_c2r_;
  };
}
