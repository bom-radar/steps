/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2020 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "configuration.h"
#include "io.h"
#include "products.h"
#include <atomic>
#include <fstream>
#include <mutex>

namespace steps
{
  /// Output that writes a mosaic tile file
  class output_mosaic_tile : public output
  {
  public:
    output_mosaic_tile(configuration const& config);
    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(array2f const& data, int d0 = -1, int d1 = -1) -> void override;
    auto write(array2<int8_t> const& data, int d0 = -1, int d1 = -1) -> void override;
    auto write_all(array2f const& data) -> void override;
    auto write_all(array2<int8_t> const& data) -> void override;

  protected:
    virtual auto output_chunk(span<char const> chunk) -> void = 0;

  private:
    int   compression_;

    int   grid_count_;
    int   grid_stride_;
  };

  class output_mosaic_tile_local : public output_mosaic_tile
  {
  public:
    output_mosaic_tile_local(configuration const& config, metadata const& user_parameters);
    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto finalize() -> void override;
    auto abort() -> void override;

  protected:
    auto output_chunk(span<char const> chunk) -> void override;

  private:
    metadata const* user_parameters_;
    string          path_format_;

    std::mutex      mut_stream_;
    string          path_final_;
    std::ofstream   stream_;
  };

  struct mosaic_input
  {
    mosaic_input(configuration const& config);

    string  domain;
    vec2i   shape;
    vec2i   offset;
  };

  using mosaic_input_list = vector<vector<mosaic_input>>;

  enum class mosaic_method
  {
      minimum
    , maximum
  };
  STEPS_ENUM_TRAITS(mosaic_method, minimum, maximum);

  class mosaic_product : public product
  {
  public:
    mosaic_product(configuration const& config, metadata const& user_parameters);

    auto series_type() const -> product_series_type override { return series_type_; }
    auto series_labels() const -> array1f const* override { return labels_ ? &labels_.value() : nullptr; }
    auto forecast_times() const -> array1<duration> const* override { return times_ ? &times_.value() : nullptr; }
    auto forecast_lengths() const -> array1<duration> const* override { return lengths_ ? &lengths_.value() : nullptr; }
    auto time_step() const -> duration override { return time_step_; }
    auto accumulation_length() const -> duration override { return accumulation_length_; }
    auto grid_shape() const -> vec2i override { return grid_shape_; }
    auto grid_resolution() const -> float override { return grid_resolution_; }
    auto units() const -> product_units override { return units_; }

    auto generate(mosaic_input_list const& inputs, time_point reference_time) -> void;

  private:
    // this buffer holds an entire input product (all d0, d1)
    struct buffer_input
    {
      std::mutex            mut;              // mutex to protect initialization (after which it is shared read only)
      bool                  inited = false;   // has this buffer object been initialized?
      mosaic_input const*   input = nullptr;  // pointer to input that the buffer comes from (needed for offsets etc)
      array1<char>          data;             // raw compressed tile data (entire product)
      array1i               chunks;           // offsets to chunk header for each grid in the file
    };

    // this buffer holds a single output grid (single d0, d1)
    struct buffer_output
    {
      std::mutex            mut;              // mutex to protect acquisition of this buffer
      bool                  inited = false;   // has this buffer object been initialized?
      array2f               dataf;            // float product data (single grid)
      array2<int8_t>        datai;            // int8_t product data
      int                   count = 0;        // number of inputs that have been processed so far
    };

  private:
    auto generate_one(mosaic_input_list const& inputs, time_point reference_time, int job_index) -> void;
    auto acquire_input_data(mosaic_input_list const& inputs, time_point reference_time, int iinput) -> void;

  private:
    // product attributes needed by the output class that we will be spoofing
    metadata const*             user_parameters_;
    product_series_type         series_type_;
    optional<array1f>           labels_;
    optional<array1<duration>>  times_;
    optional<array1<duration>>  lengths_;
    duration                    time_step_;
    duration                    accumulation_length_;
    vec2i                       grid_shape_;
    float                       grid_resolution_;
    product_units               units_;

    // our actual stuff
    string                      input_path_;
    mosaic_method               method_;          // whether to combine using minimum or maximum values
    int                         grid_count_;      // total number of grids in a single product instance
    bool                        int8_data_;       // true means grid cells are int8_t (otherwise float)
    int                         cell_size_;       // size of either int8_t or float
    int                         grid_size_bytes_; // size of a single output grid in bytes

    // live stuff
    unique_ptr<buffer_input[]>  input_buffers_;   // each holds an entire input product (all dims)
    unique_ptr<buffer_output[]> output_buffers_;  // each holds a single mosaic grid (just x/y dims)
  };

  class mosaic_generator
  {
  public:
    mosaic_generator(configuration const& config);

    auto generate_mosaics(time_point reference_time) -> void;

  private:
    using product_list = vector<unique_ptr<mosaic_product>>;

  private:
    metadata          user_parameters_;
    mosaic_input_list inputs_;
    product_list      products_;
  };
}
