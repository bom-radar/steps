/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "nc.h"
#include "util.h"
#include "vec2.h"

namespace steps::cf
{
  /// Default functor used to check whether a value is 'nodata' and should be packed using the fill_value
  template <typename T>
  struct default_is_nodata
  {
    auto operator()(T val) const { return std::isnan(val); }
  };

  class packer
  {
  public:
    packer()
      : size_{0}
    { }

#define STEPS_CF_PACKER_FUNCS(T, ND) \
    void read(nc::variable const& var, span<T> data, T nodata = ND, span<int const> start = {}, span<int const> count = {}) \
    { \
      read_impl(var, data, nodata, start, count); \
    } \
    void read(nc::variable const& var, span<T> data, T nodata, std::initializer_list<int> start, std::initializer_list<int> count = {}) \
    { \
      read_impl(var, data, nodata, {start.begin(), start.end()}, {count.begin(), count.end()}); \
    } \
    template <class IsNoData = default_is_nodata<T>> \
    void write(nc::variable& var, span<T const> data, span<int const> start = {}, span<int const> count = {}) \
    { \
      write_impl(var, data, start, count); \
    } \
    template <class IsNoData = default_is_nodata<T>> \
    void write(nc::variable& var, span<T const> data, std::initializer_list<int> start, std::initializer_list<int> count = {}) \
    { \
      write_impl(var, data, {start.begin(), start.end()}, {count.begin(), count.end()}); \
    }

    STEPS_CF_PACKER_FUNCS(float, std::numeric_limits<float>::quiet_NaN())
    STEPS_CF_PACKER_FUNCS(double, std::numeric_limits<double>::quiet_NaN())
    STEPS_CF_PACKER_FUNCS(int, 0)
    STEPS_CF_PACKER_FUNCS(short, 0)
    STEPS_CF_PACKER_FUNCS(long, 0)
    STEPS_CF_PACKER_FUNCS(long long, 0)

#undef STEPS_CF_PACKER_FUNCS

  private:
    template <typename T>
    void read_impl(nc::variable const& var, span<T> data, T nodata, span<int const> start, span<int const> count)
    {
      auto scale = var.att_get_as<T>("scale_factor", 1);
      auto offset = var.att_get_as<T>("add_offset", 0);

      auto impl = [&](auto dummy)
      {
        using ptype = decltype(dummy);
        auto fill = var.att_get_as<optional<ptype>>("_FillValue");

        // ensure buffer is large enough
        auto bufsize = sizeof(ptype) * data.size();
        if (size_ < bufsize)
        {
          buffer_ = std::make_unique<char[]>(bufsize);
          size_ = bufsize;
        }
        auto buf = reinterpret_cast<ptype*>(buffer_.get());

        // read the data into our raw buffer
        var.read(span{buf, data.size()}, start, count);

        // what combination of scale, offset and fill do we have?
        if (fill)
        {
          for (decltype(data.size()) i = 0; i < data.size(); ++i)
            data[i] = buf[i] == *fill ? nodata : buf[i] * scale + offset;
        }
        else
        {
          for (decltype(data.size()) i = 0; i < data.size(); ++i)
            data[i] = buf[i] * scale + offset;
        }
      };

      switch (var.type())
      {
      case nc::data_type::i8:  impl(int8_t(0)); break;
      case nc::data_type::u8:  impl(uint8_t(0)); break;
      case nc::data_type::i16: impl(int16_t(0)); break;
      case nc::data_type::u16: impl(uint16_t(0)); break;
      case nc::data_type::i32: impl(int32_t(0)); break;
      case nc::data_type::u32: impl(uint32_t(0)); break;
      case nc::data_type::i64: impl(int64_t(0)); break;
      case nc::data_type::u64: impl(uint64_t(0)); break;
      case nc::data_type::f32: impl(float(0)); break;
      case nc::data_type::f64: impl(double(0)); break;
      case nc::data_type::string:
        throw std::runtime_error{format("Packing used with string variable {}", var.name())};
      }
    }

    template <typename T, class IsNoData = default_is_nodata<T>>
    void write_impl(nc::variable& var, span<T const> data, span<int const> start, span<int const> count)
    {
      auto is_nodata = IsNoData{};
      auto scale = var.att_get_as<T>("scale_factor", 1);
      auto offset = var.att_get_as<T>("add_offset", 0);

      auto impl = [&](auto dummy)
      {
        using ptype = decltype(dummy);
        auto fill = var.att_get_as<optional<ptype>>("_FillValue");

        // ensure buffer is large enough
        auto bufsize = sizeof(ptype) * data.size();
        if (size_ < bufsize)
        {
          buffer_ = std::make_unique<char[]>(bufsize);
          size_ = bufsize;
        }
        auto buf = reinterpret_cast<ptype*>(buffer_.get());

        // pack based on scale, offset and fill
        // if packing into integral types we must round to nearest to ensure maximum accuracy on unpack
        if (fill)
        {
          if (std::is_integral<ptype>::value)
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = is_nodata(data[i]) ? *fill : std::lround((data[i] - offset) / scale);
          else
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = is_nodata(data[i]) ? *fill : (data[i] - offset) / scale;
        }
        else
        {
          if (std::is_integral<ptype>::value)
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = std::lround((data[i] - offset) / scale);
          else
            for (decltype(data.size()) i = 0; i < data.size(); ++i)
              buf[i] = (data[i] - offset) / scale;
        }

        // write the data into our raw buffer
        var.write(span{buf, data.size()}, start, count);
      };

      switch (var.type())
      {
      case nc::data_type::i8:  impl(int8_t(0)); break;
      case nc::data_type::u8:  impl(uint8_t(0)); break;
      case nc::data_type::i16: impl(int16_t(0)); break;
      case nc::data_type::u16: impl(uint16_t(0)); break;
      case nc::data_type::i32: impl(int32_t(0)); break;
      case nc::data_type::u32: impl(uint32_t(0)); break;
      case nc::data_type::i64: impl(int64_t(0)); break;
      case nc::data_type::u64: impl(uint64_t(0)); break;
      case nc::data_type::f32: impl(float(0)); break;
      case nc::data_type::f64: impl(double(0)); break;
      case nc::data_type::string:
        throw std::runtime_error{format("Packing used with string variable {}", var.name())};
      }
    }

  private:
    unique_ptr<char[]>  buffer_;
    size_t              size_;
  };

  /// Time units
  /* Note: if you change this enum you must also update the time_unit_factors array */
  enum class time_unit
  {
      seconds
    , minutes
    , hours
    , days
  };

  /// Convert a quantity in a time based unit into a timestamp compatible duration
  auto duration_from_time_units(double val, time_unit units) -> duration;

  /// Convert a timestamp compatible duration into a quantity in a time based unit
  auto time_units_from_duration(duration val, time_unit units) -> double;

  /// Parse the CF style timestamp string
  auto parse_time_units(string const& str) -> pair<time_unit, time_point>;

  /// Read a scalar time from a variable
  auto read_time(nc::variable const& var, span<int const> start = {}) -> time_point;

  /// Read a set of times from a variable
  auto read_times(
        nc::variable const& var
      , span<time_point> data
      , span<int const> start = {}
      , span<int const> count = {}
      ) -> void;

  /// Find the coordinate variable associated with a dimension (if any)
  /** This function will ensure that the coordinate variable has the correct rank and size. */
  auto find_coordinates(nc::dimension const& dim) -> nc::variable const*;
  inline auto find_coordinates(nc::dimension& dim) -> nc::variable*
  {
    return const_cast<nc::variable*>(find_coordinates(const_cast<nc::dimension const&>(dim)));
  }

  /// Find the coordinate variable associated with a dimension and throw on failure
  auto lookup_coordinates(nc::dimension const& dim) -> nc::variable const&;
  inline auto lookup_coordinates(nc::dimension& dim) -> nc::variable&
  {
    return const_cast<nc::variable&>(lookup_coordinates(const_cast<nc::dimension const&>(dim)));
  }

  /// Copy a dimension and any associated coordinate variable
  auto copy_dimension_and_coordinates(nc::dimension const& dim, nc::group& target) -> nc::dimension&;
}
