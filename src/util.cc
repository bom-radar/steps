/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "util.h"
#include "unit_test.h"
#include <mutex>

using namespace steps;

static std::mutex mut_cmd_line_;
static string cmd_line_string_;

auto steps::set_command_line_string(int argc, char const* const* argv) -> void
{
  auto lock = std::lock_guard<std::mutex>(mut_cmd_line_);
  cmd_line_string_.clear();
  for (auto i = 0; i < argc; ++i)
  {
    for (auto c = argv[i]; *c != '\0'; ++c)
    {
      if (std::isspace(*c) || *c == '\\')
        cmd_line_string_.push_back('\\');
      cmd_line_string_.push_back(*c);
    }
    cmd_line_string_.push_back(' ');
  }
  if (!cmd_line_string_.empty())
    cmd_line_string_.pop_back();
}

auto steps::get_command_line_string() -> string
{
  auto lock = std::lock_guard<std::mutex>(mut_cmd_line_);
  return cmd_line_string_;
}

auto steps::replace_all(string_view subject, string_view search, string_view replace) -> string
{
  string ret;
  auto pos = 0;
  auto end = subject.find(search);
  while (end != string::npos)
  {
    ret.append(subject, pos, end - pos);
    ret.append(replace);
    pos = end + search.size();
    end = subject.find(search, pos);
  }
  ret.append(subject, pos);
  return ret;
}

auto steps::calculate_blend_factor_for_halflife(duration time_step, duration blend_halflife) -> float
{
  if (blend_halflife == duration::max())
    return 0.0f;
  if (blend_halflife <= duration::zero())
    return 1.0f;

  // convert the half-life into the number of time steps
  auto n = double(blend_halflife.count()) / double(time_step.count());

  // determine the maximum blend contribution per time step
  return 1.0 - std::pow(0.5, 1.0 / n);
}

// LCOV_EXCL_START
TEST_CASE("unique_handle")
{
  static std::vector<int> list;
  struct deleter
  {
    auto operator()(int val) -> void
    {
      if (val != 0)
        list.push_back(val);
    }
  };
  using handle = unique_handle<int, deleter, 0>;

  CHECK(handle::nullhnd == 0);

  handle h0;
  CHECK(static_cast<bool>(h0) == false);
  CHECK(h0 == 0);
  CHECK(h0.get() == 0);
  h0.reset();
  CHECK(list.empty());

  handle h1{1};
  CHECK(static_cast<bool>(h1) == true);
  CHECK(h1 == 1);
  CHECK(h1.get() == 1);
  h1.reset();
  CHECK(list.at(0) == 1);

  handle h2{2};
  handle h2b{std::move(h2)};
  CHECK(h2.get() == 0);
  CHECK(h2b.get() == 2);
  h2.reset();
  CHECK(list.size() == 1);
  h2b.reset();
  CHECK(list.at(1) == 2);

  handle h3{3};
  h3.reset(4);
  CHECK(list.at(2) == 3);
  CHECK(h3.release() == 4);
  CHECK(h3.get() == 0);
}

TEST_CASE("set_command_line_string")
{
  char const* argv[] = { "foo", "b ar", "h\\i" };
  set_command_line_string(3, argv);
  CHECK(get_command_line_string() == "foo b\\ ar h\\\\i");
}

TEST_CASE("replace_all")
{
  string val;

  val = "hihi";
  val = replace_all(val, "hi", "bye");
  CHECK(val == "byebye");

  val = "ahihia";
  val = replace_all(val, "hi", "bye");
  CHECK(val == "abyebyea");

  val = "hhii";
  val = replace_all(val, "hi", "");
  CHECK(val == "hi");

  val = "hiyeshi";
  val = replace_all(val, "hi", "nohino");
  CHECK(val == "nohinoyesnohino");
}
// LCOV_EXCL_STOP
