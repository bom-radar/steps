/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "local_statistics.h"

#include "profile.h"
#include "statistics.h"
#include "unit_test.h"

using namespace steps;

static profile prof_local_statistics{"local_statistics"};

/// Statistics accumulated during iteration
/** We do *not* guarantee sum-of-squares values sqr1 and sqr0 are non-negative.  They may become negative as they are
 *  incrementally-calculated floating-point values. */
struct accum_stats
{
  double sum1;
  double sqr1;
  double sum0;
  double sqr0;
  double prod;

  /// Accumulate (and calculate) interim statistics.
  /* this should probably take double arguments however we then somehow differ from local_statistics_impl() */
  auto accum_calc(float lag1, float lag0) -> void
  {
    sum1 += lag1;
    sum0 += lag0;
    sqr1 += lag1 * lag1;
    sqr0 += lag0 * lag0;
    prod += lag1 * lag0;
  }

  /// Subtract (and calculate) interim statistics.
  /* this should probably take double arguments however we then somehow differ from local_statistics_impl() */
  auto subtract_calc(float lag1, float lag0) -> void
  {
    sum1 -= lag1;
    sum0 -= lag0;
    prod -= lag1 * lag0;
    // incremental sqrN can become negative here
    sqr1 -= lag1 * lag1;
    sqr0 -= lag0 * lag0;
  }

  /// Accumulate pre-calculated interim statistics.
  auto accum_precalc(accum_stats const& rhs) -> void
  {
    sum1 += rhs.sum1;
    sum0 += rhs.sum0;
    sqr1 += rhs.sqr1;
    sqr0 += rhs.sqr0;
    prod += rhs.prod;
  }

  /// Subtract pre-calculated interim statistics.
  auto subtract_precalc(accum_stats const& rhs) -> void
  {
    sum1 -= rhs.sum1;
    sum0 -= rhs.sum0;
    prod -= rhs.prod;
    // incremental sqrN can become negative here
    sqr1 -= rhs.sqr1;
    sqr0 -= rhs.sqr0;
  }

  /// Calculate final statistics from those accumulated during iteration
  /** We guarantee that stddev is non-negative and autocorrelation is in [0.0, 1.0].
   *  Since we test our floating-point inputs and outputs as little as possible, we must be careful:
   *  - squares sqrN may be negative due to incremental calculation.
   *  - variances varN may be negative due to negative sqrN or near-zero subtraction.
   *  - autocorrelation calculation may be outside expected range. */
  auto final_stats(int count) const -> cell_stats
  {
    auto mean0 = sum0 / count;
    auto var0 = (sqr0 / count) - (mean0 * mean0);

    // test order matters here for 0.0 / 0.0
    if (var0 <= 0.0)
      return { float(mean0), 0.0f, 1.0f }; // mean, stddev, autocor

    auto stddev0 = sqrt(var0);
    auto mean1 = sum1 / count;
    auto var1 = (sqr1 / count) - (mean1 * mean1);
    if (var1 <= 0.0)
      return { float(mean0), float(stddev0), 1.0f }; // mean, stddev, autocor

    auto varC = (prod / count) - (mean1 * mean0);
    if (varC <= 0.0)
      return { float(mean0), float(stddev0), 0.0f }; // mean, stddev, autocor

    // min() to ensure autocor is in [0.0, 1.0]
    auto autocor = std::min(1.0, varC / std::sqrt(var1 * var0));
    return { float(mean0), float(stddev0), float(autocor) };
  }
};

auto steps::local_statistics(array2f const& lag1, array2f const& lag0, int radius, local_stats& out) -> cell_stats
{
  auto ps = profile::scope{prof_local_statistics};

  radius = std::clamp(radius, 0, std::min(lag1.shape().x, lag1.shape().y) - 1);

  auto buf = array2<accum_stats>{lag1.shape()};
  auto scalar_accum = accum_stats{};

  auto count = (radius * 2 + 1) * (radius * 2 + 1);

  // collect the sums in the X direction
  for (auto y = 0; y < lag1.shape().y; ++y)
  {
    // initialize the kernel (as if processing x = -1)
    auto cell_accum = accum_stats{};
    for (auto xi = -1 - radius; xi <= -1 + radius; ++xi)
    {
      auto xr
        = xi < 0 ? -xi - 1
        : xi >= lag1.shape().x ? 2 * lag1.shape().x - xi - 1
        : xi;
      cell_accum.accum_calc(lag1[y][xr], lag0[y][xr]);
    }

    // simple implementation - we can make faster by adding / subtracting the pixel at either side of the window
    for (auto x = 0; x < lag1.shape().x; ++x)
    {
      // subtract oldest value from kernel
      {
        auto xi = x - radius - 1;
        auto xr
          = xi < 0 ? -xi - 1
          : xi >= lag1.shape().x ? 2 * lag1.shape().x - xi - 1
          : xi;
        cell_accum.subtract_calc(lag1[y][xr], lag0[y][xr]);
      }

      // add new value to kernel
      {
        auto xi = x + radius;
        auto xr
          = xi < 0 ? -xi - 1
          : xi >= lag1.shape().x ? 2 * lag1.shape().x - xi - 1
          : xi;
        cell_accum.accum_calc(lag1[y][xr], lag0[y][xr]);
      }

      // save value
      buf[y][x] = cell_accum;

      // also collect the scalar total field mean/stddev/autocor while we are here
      scalar_accum.accum_calc(lag1[y][x], lag0[y][x]);
    }
  }

  // now sum the X sums in the Y direction and divide by counts to detemine the local means
  for (auto x = 0; x < lag1.shape().x; ++x)
  {
    // initialize the kernel (as if y = -1)
    auto cell_accum = accum_stats{};
    for (auto yi = -1 - radius; yi <= -1 + radius; ++yi)
    {
      auto yr
        = yi < 0 ? -yi - 1
        : yi >= lag1.shape().y ? 2 * lag1.shape().y - yi - 1
        : yi;
      cell_accum.accum_precalc(buf[yr][x]);
    }

    // simple implementation - we can make faster by adding / subtracting the pixel at either side of the window
    for (auto y = 0; y < lag1.shape().y; ++y)
    {
      // subtract oldest value from kernel
      {
        auto yi = y - radius - 1;
        auto yr
          = yi < 0 ? -yi - 1
          : yi >= lag1.shape().y ? 2 * lag1.shape().y - yi - 1
          : yi;
        cell_accum.subtract_precalc(buf[yr][x]);
      }

      // add new value to kernel
      {
        auto yi = y + radius;
        auto yr
          = yi < 0 ? -yi - 1
          : yi >= lag1.shape().y ? 2 * lag1.shape().y - yi - 1
          : yi;
        cell_accum.accum_precalc(buf[yr][x]);
      }

      // calculate means, variances and covariance for this grid cell
      out[y][x] = cell_accum.final_stats(count);
    }
  }

  // scalar results
  return scalar_accum.final_stats(lag1.size());
}

/* In areas with no rain the local stddev will be extremely low, particularly in high frequency levels of the
 * cascade.  This is a problem, because it means we are trying to measure the autocorrelation of an (almost)
 * uniform field.  As a result the autocorrelation will be extremely low, often actually zero.  To work around
 * this we detect areas with very low standard deviation, and in those areas we use a default autocorrelation
 * instead.  To reduce hard boundaries we interpolate between the local and default value as the stddev drops. */
auto steps::local_statistics_post(float stddev_scale, cell_stats scalar_stats, local_stats& out) -> void
{
  scalar_stats.stddev *= stddev_scale;
  auto var0 = scalar_stats.stddev * scalar_stats.stddev;
  for (auto y = 0; y < out.shape().y; ++y)
  {
    for (auto x = 0; x < out.shape().x; ++x)
    {
      auto stddev = out[y][x].stddev * stddev_scale;
      auto var = stddev * stddev;

      // scale the stddev according to user setting
      out[y][x].stddev = stddev;

      // HACK - is the variance as a fraction of scalar variance the best interpolation factor here???
      auto frac = std::clamp(var / var0, 0.0f, 1.0f);
      out[y][x].autocor = lerp(scalar_stats.autocor, out[y][x].autocor, frac);
    }
  }
}

// LCOV_EXCL_START
static auto local_statistics_reference_impl(array2f const& lag1, array2f const& lag0, int radius, local_stats& out) -> void
{
  radius = std::clamp(radius, 0, std::min(lag1.shape().x, lag1.shape().y) - 1);

  // naive approach - loops through entire kernel at each output pixel
  auto count = (radius * 2 + 1) * (radius * 2 + 1);
  for (auto y = 0; y < lag1.shape().y; ++y)
  {
    for (auto x = 0; x < lag1.shape().x; ++x)
    {
      double mean1 = 0.0, mean0 = 0.0;
      for (auto yi = y - radius; yi < y + radius + 1; ++yi)
      {
        // reflect over edge kernel back into array
        auto yr = yi < 0 ? -yi - 1 : yi >= lag1.shape().y ? 2 * lag1.shape().y - yi - 1 : yi;
        for (auto xi = x - radius; xi < x + radius + 1; ++xi)
        {
          // reflect over edge kernel back into array
          auto xr = xi < 0 ? -xi - 1 : xi >= lag1.shape().x ? 2 * lag1.shape().x - xi - 1 : xi;

          mean1 += lag1[yr][xr];
          mean0 += lag0[yr][xr];
        }
      }
      mean1 /= count;
      mean0 /= count;

      double var1 = 0.0, var0 = 0.0, varC = 0.0;
      for (auto yi = y - radius; yi < y + radius + 1; ++yi)
      {
        // reflect over edge kernel back into array
        auto yr = yi < 0 ? -yi - 1 : yi >= lag1.shape().y ? 2 * lag1.shape().y - yi - 1 : yi;
        for (auto xi = x - radius; xi < x + radius + 1; ++xi)
        {
          // reflect over edge kernel back into array
          auto xr = xi < 0 ? -xi - 1 : xi >= lag1.shape().x ? 2 * lag1.shape().x - xi - 1 : xi;

          var1 += (lag1[yr][xr] - mean1) * (lag1[yr][xr] - mean1);
          var0 += (lag0[yr][xr] - mean0) * (lag0[yr][xr] - mean0);
          varC += (lag1[yr][xr] - mean1) * (lag0[yr][xr] - mean0);
        }
      }
      var1 /= count;
      var0 /= count;
      varC /= count;

      // we must handle zero (no rain) and negative (thanks floating-point) varN
      out[y][x].mean = mean0;
      out[y][x].stddev = (var0 <= 0.0) ? 0.0 : std::sqrt(var0);
      out[y][x].autocor = [=]() -> auto
      {
        // test order matters here for 0.0 / 0.0
        if (var1 <= 0.0 || var0 <= 0.0)
          return 1.0;
        if (varC <= 0.0)
          return 0.0;
        return std::min(1.0, varC / std::sqrt(var1 * var0));
      }();
    }
  }
}

static auto compare_cell_stats(cell_stats const& lhs, cell_stats const& rhs) -> bool
{
  return lhs.mean == approx(rhs.mean) && lhs.stddev == approx(rhs.stddev) && lhs.autocor == approx(rhs.autocor);
}

TEST_CASE("local_statistics")
{
  auto radius = ut_fast_mode() ? 8 : 50;

  // load our standard lag1 and lag0 unit test reference images
  auto lag1 = ut_load_reference_grid(1, ut_fast_mode());
  auto lag0 = ut_load_reference_grid(0, ut_fast_mode());
  REQUIRE(lag1.shape() == lag0.shape());

  // generate the reference result
  auto res_ref = local_stats{lag1.shape()};
  REQUIRE_NOTHROW(local_statistics_reference_impl(lag1, lag0, radius, res_ref));

  // generate the test result
  auto res_tst = local_stats{lag1.shape()};
  REQUIRE_NOTHROW(local_statistics(lag1, lag0, radius, res_tst));

  // check that the test answer is close to the reference answer
  CHECK_ALL(res_tst, res_ref, compare_cell_stats(lhs, rhs));
}
// LCOV_EXCL_STOP
