/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

// awful macro hacks made necessary by inflexibility of the variadic macros capability
#define STEPS_NUM_ARGS_IMPL(x100, x99, x98, x97, x96, x95, x94, x93, x92, x91, x90, x89, x88, x87, x86, x85, x84, x83, x82, x81, x80, x79, x78, x77, x76, x75, x74, x73, x72, x71, x70, x69, x68, x67, x66, x65, x64, x63, x62, x61, x60, x59, x58, x57, x56, x55, x54, x53, x52, x51, x50, x49, x48, x47, x46, x45, x44, x43, x42, x41, x40, x39, x38, x37, x36, x35, x34, x33, x32, x31, x30, x29, x28, x27, x26, x25, x24, x23, x22, x21, x20, x19, x18, x17, x16, x15, x14, x13, x12, x11, x10, x9, x8, x7, x6, x5, x4, x3, x2, x1, n, ...)   n
#define STEPS_NUM_ARGS(...)         STEPS_NUM_ARGS_IMPL(__VA_ARGS__, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#define STEPS_EXPAND(x)             x
#define STEPS_FIRSTARG(x, ...)      (x)
#define STEPS_RESTARGS(x, ...)      (__VA_ARGS__)
#define STEPS_FOREACH(macro, list)  STEPS_FOREACH_(STEPS_NUM_ARGS list, macro, list)
#define STEPS_FOREACH_(n, m, list)  STEPS_FOREACH__(n, m, list)
#define STEPS_FOREACH__(n, m, list) STEPS_FOREACH_##n(m, list)
#define STEPS_FOREACH_1(m, list)    m list
#define STEPS_FOREACH_2(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_1(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_3(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_2(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_4(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_3(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_5(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_4(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_6(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_5(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_7(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_6(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_8(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_7(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_9(m, list)    STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_8(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_10(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_9(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_11(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_10(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_12(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_11(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_13(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_12(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_14(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_13(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_15(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_14(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_16(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_15(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_17(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_16(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_18(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_17(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_19(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_18(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_20(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_19(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_21(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_20(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_22(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_21(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_23(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_22(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_24(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_23(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_25(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_24(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_26(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_25(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_27(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_26(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_28(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_27(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_29(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_28(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_30(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_29(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_31(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_30(m, STEPS_RESTARGS list)
#define STEPS_FOREACH_32(m, list)   STEPS_EXPAND(m STEPS_FIRSTARG list), STEPS_FOREACH_31(m, STEPS_RESTARGS list)

#define STEPS_STRINGIFY(x) #x
#define STEPS_STRINGIFY_ARGS(...)   STEPS_FOREACH(STEPS_STRINGIFY, (__VA_ARGS__))

#define STEPS_PREPEND_TYPE(x) (value_type) type::x
#define STEPS_PREPEND_TYPE_ARGS(...) STEPS_FOREACH(STEPS_PREPEND_TYPE, (__VA_ARGS__))
