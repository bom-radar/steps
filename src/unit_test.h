/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#ifndef STEPS_UNIT_TEST
  #define DOCTEST_CONFIG_DISABLE
#endif

// we want doctest to assume CHECK("foo" == "bar") is comparing strings rather than pointers
#define DOCTEST_CONFIG_TREAT_CHAR_STAR_AS_STRING

#include "array2.h"
#include "traits.h"
#include "format.h"
#include "configuration.h"
#include "ext/doctest/doctest.h"
#include <filesystem>

// custom unit test assertion macros
#define CHECK_ALL(lhs_, rhs_, ...)   ut_check_all(__FILE__, __LINE__, false, lhs_, rhs_, [](auto lhs, auto rhs){ return __VA_ARGS__; })
#define REQUIRE_ALL(lhs_, rhs_, ...) ut_check_all(__FILE__, __LINE__, true, lhs_, rhs_, [](auto lhs, auto rhs){ return __VA_ARGS__; })

namespace steps
{
  /// Test for approximately floating point equality
  using approx = doctest::Approx;

  /// Run all unit tests
  auto run_unit_tests(configuration const& config) -> int;

  // forward declarations needed for special cases in our ut_check_all template
  template <typename T> class array2;
  template <typename T> class vec2;

  /// Get the path to the unit test reference data directory
  auto ut_reference_data_dir() -> std::filesystem::path const&;

  /// Get whether the unit tests are being run in fast mode
  auto ut_fast_mode() -> bool;

  /// Load one of our standard unit test reference grids
  auto ut_load_reference_grid(int lag, bool mini) -> array2f;

  // LCOV_EXCL_START
  /// Test that all elements of two containers for equality
  template <typename T, typename Test>
  auto ut_check_all(char const* file, int line, bool fatal, T&& lhs, T&& rhs, Test test)
  {
    auto il = std::begin(lhs), ir = std::begin(rhs);
    auto ile = std::end(lhs), ire = std::end(rhs);
    while (il != ile && ir != ire)
    {
      if (!test(*il, *ir))
      {
        auto i = (il - std::begin(lhs));

        string values;
        if constexpr (allow_simple_serialization<typename std::decay<T>::type::value_type>::value)
          values = format("{} != {}", *il, *ir);
        else
          values = "??? != ???";

        string msg;
        if constexpr (is_instance_v<typename std::decay<T>::type, array2>)
          msg = format("Mismatch at index {} row {} col {}: {}", i, (i / lhs.shape().x), (i % lhs.shape().x), values);
        else
          msg = format("Mismatch at index {}: {}", i, values);

        if (fatal)
          ADD_FAIL_AT(file, line, msg);
        else
          ADD_FAIL_CHECK_AT(file, line, msg);

        return;
      }
      ++il;
      ++ir;
    }
    if (il != ile || ir != ire)
    {
      if (fatal)
        ADD_FAIL_AT(file, line, "Size mismatch");
      else
        ADD_FAIL_CHECK_AT(file, line, "Size mismatch");
    }
  }
  // LCOV_EXCL_STOP
}
