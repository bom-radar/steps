/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include "configuration.h"

namespace steps
{
  /// Model skills climatology tracker
  /** The climatological skill is maintained for each cascade level at a nominated set of lead times using an
   *  exponentially weighted moving average (EMA).  The alpha term of the EMA is configured by the user and can
   *  be used to influence how quickly the climatology responds to changes in skill.
   *
   *  If no climatology data is available the system will cold start the skills using the values first passed
   *  to update().  In the case that no climatology data was available, and lookup() is called without any cold
   *  start having being performed via update(), an exception will be thrown.
   */
  class skills_climatology
  {
  public:
    skills_climatology(int levels, configuration const& config);

    /// Get the number of levels in the climatology
    auto levels() const { return skills_.shape().x; }

    /// Get the lead times tracked by the climatology
    auto& lead_times() const { return lead_times_; }

    /// Load the climatology data from a configuration object
    auto load(configuration const& data) -> void;

    /// Store the climatology data to a configuration object
    auto store(configuration& data) const -> void;

    /// Update the climatology with new diagnosed skills
    auto update(duration lead_time, span<float const> skills) -> void;

    /// Fetch skills for a given lead time from the climatology
    auto lookup(duration lead_time, span<float> skills) const -> void;

  private:
    array1<duration>      lead_times_;
    float                 alpha_;
    array2f               skills_;
  };
}
