/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "products.h"
#include "compression.h"
#include "io_netcdf.h"
#include "profile.h"
#include "cf.h"
#include "thread_pool.h"
#include "trace.h"

using namespace steps;
using namespace steps::products;

static profile prof_prod_median{"product::median_filter"};

product::product(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : metadata_{parse_metadata(config.optional("metadata", configuration{}), user_parameters, parent_metadata)}
  , output_{output::instantiate(config, user_parameters)}
  , parent_{nullptr}
{ }

auto product::initialize(product const& parent, time_point reference_time) -> void
{
  parent_ = &parent;
  if (output_)
    output_->initialize(*this, reference_time);
}

auto product::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  if (output_)
    output_->write(data, member, forecast);
}

auto product::write(int member, int forecast, array2<int8_t> const& data, array2f2 const* flow) -> void
{
  if (output_)
    output_->write(data, member, forecast);
}

auto product::write_all(array2f const& data) -> void
{
  if (output_)
    output_->write_all(data);
}

auto product::write_all(array2<int8_t> const& data) -> void
{
  if (output_)
    output_->write_all(data);
}

auto product::finalize() -> void
{
  if (output_)
    output_->finalize();
  parent_ = nullptr;
}

auto product::abort() -> void
{
  if (output_)
    output_->abort();
  parent_ = nullptr;
}

auto product::series_type() const -> product_series_type
{
  return parent_ ? parent_->series_type() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::series_labels() const -> array1f const*
{
  return parent_ ? parent_->series_labels() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::forecast_times() const -> array1<duration> const*
{
  return parent_ ? parent_->forecast_times() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::forecast_lengths() const -> array1<duration> const*
{
  return parent_ ? parent_->forecast_lengths() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::time_step() const -> duration
{
  return parent_ ? parent_->time_step() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::accumulation_length() const -> duration
{
  return parent_ ? parent_->accumulation_length() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::grid_shape() const -> vec2i
{
  return parent_ ? parent_->grid_shape() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::grid_resolution() const -> float
{
  return parent_ ? parent_->grid_resolution() : throw std::logic_error{"Product parent used before initialization"};
}

auto product::units() const -> product_units
{
  return parent_ ? parent_->units() : throw std::logic_error{"Product parent used before initialization"};
}

ensemble::ensemble(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : product{config, user_parameters, parent_metadata}
{
  auto metaptr = &output_metadata();
  if (auto config_children = config.find("children"))
  {
    auto& config_children_obj = config_children->object();
    children_.reserve(config_children_obj.size());
    for (auto& [type, config_child] : config_children_obj)
    {
      try
      {
        if (type == "accumulation_scaled")
          children_.push_back(std::make_unique<accumulation_scaled>(config_child, user_parameters, metaptr));
        else if (type == "accumulation_sliding")
          children_.push_back(std::make_unique<accumulation_sliding>(config_child, user_parameters, metaptr));
        else if (type == "accumulation_sequential")
          children_.push_back(std::make_unique<accumulation_sequential>(config_child, user_parameters, metaptr));
        else if (type == "ensemble_mean")
          children_.push_back(std::make_unique<ensemble_mean>(config_child, user_parameters, metaptr));
        else if (type == "filter_members")
          children_.push_back(std::make_unique<filter_members>(config_child, user_parameters, metaptr));
        else if (type == "filter_time")
          children_.push_back(std::make_unique<filter_time>(config_child, user_parameters, metaptr));
        else if (type == "time_lagged")
          children_.push_back(std::make_unique<time_lagged>(config_child, user_parameters, metaptr));
        else if (type == "median_filter")
          children_.push_back(std::make_unique<median_filter>(config_child, user_parameters, metaptr));
        else if (type == "probabilities")
          children_.push_back(std::make_unique<probabilities_scalar>(config_child, user_parameters, metaptr));
        else if (type == "probabilities_spatial")
          children_.push_back(std::make_unique<probabilities_spatial>(config_child, user_parameters, metaptr));
        else if (type == "percentiles")
          children_.push_back(std::make_unique<percentiles>(config_child, user_parameters, metaptr));
        else
          throw std::runtime_error{format("Invalid output manager type {}", type)};
      }
      catch (...)
      {
        std::throw_with_nested(std::runtime_error{format("Failed to configure output type {}", type)});
      }
    }
  }
}

auto ensemble::initialize(product const& parent, time_point reference_time) -> void
{
  product::initialize(parent, reference_time);
  for (auto& c : children_)
    c->initialize(*this, reference_time);
}

auto ensemble::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  product::write(member, forecast, data, flow);
  for (auto& c : children_)
    c->write(member, forecast, data, flow);
}

auto ensemble::write_all(array2f const& data) -> void
{
  product::write_all(data);
  for (auto& c : children_)
    c->write_all(data);
}

auto ensemble::finalize() -> void
{
  for (auto& c : children_)
    c->finalize();
  product::finalize();
}

auto ensemble::abort() -> void
{
  for (auto& c : children_)
    c->abort();
  product::abort();
}

rain_rate::rain_rate(
      int members
    , int member_offset
    , int forecasts
    , duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , configuration const& config
    , metadata const& user_parameters)
  : ensemble{config, user_parameters, nullptr}
  , members_{members}
  , times_{forecasts}
  , time_step_{time_step}
  , grid_shape_{grid_shape}
  , grid_resolution_{grid_resolution}
{
  for (auto i = 0; i < members_.size(); ++i)
    members_[i] = member_offset + i;
  for (auto i = 0; i < times_.size(); ++i)
    times_[i] = (i + 1) * time_step;
}

auto rain_rate::initialize(time_point reference_time) -> void
{
  /* CAREFUL - we are passing a reference to ourselves as our own parent!  this is just to avoid forcing us to
   * convert all the parent references to pointers, since almost every product is guaranteed to have a valid parent
   * it is nice to use references.  this is actually safe to do since our base class(es) actually do anything with
   * the parent.  but if you are messing around in the future take care that you don't setup an infinite loop by
   * calling into our parent (which is ourselves). */
  ensemble::initialize(*this, reference_time);
}

accumulation::accumulation(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : ensemble{config, user_parameters, parent_metadata}
  , accumulation_length_{config["accumulation_length"]}
{
  if (accumulation_length_ <= duration::zero())
    throw std::runtime_error{"Invalid accumulation length"};
}

accumulation_scaled::accumulation_scaled(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : accumulation{config, user_parameters, parent_metadata}
{ }

auto accumulation_scaled::initialize(product const& parent, time_point reference_time) -> void
{
  if (parent.units() != product_units::rate)
    throw std::runtime_error{"Product expects rain rate as input"};
  auto parent_times = parent.forecast_times();
  if (!parent_times)
    throw std::runtime_error{"Product expects a time series as input"};

  lengths_.resize(parent_times->size());
  lengths_.fill(accumulation_length_);

  accumulation::initialize(parent, reference_time);
}

auto accumulation_scaled::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  // short cut if length happens to be exactly one hour
  if (accumulation_length_ == 3600s)
  {
    accumulation::write(member, forecast, data, flow);
    return;
  }

  auto scale = std::chrono::duration_cast<std::chrono::seconds>(accumulation_length_).count() / 3600.0f;
  auto buffer = array2f{data.shape()};
  buffer.copy_scaled(data, scale);
  accumulation::write(member, forecast, buffer, flow);
}

accumulation_sliding::accumulation_sliding(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : accumulation{config, user_parameters, parent_metadata}
  , input_{[&]{ auto c = config.find("input"); return c ? input::instantiate(*c, user_parameters) : nullptr; }()}
  , compression_{zlib_parse_level(config.optional("cache_compression", "none"))}
{ }

auto accumulation_sliding::initialize(product const& parent, time_point reference_time) -> void
{
  if (parent.units() != product_units::accumulation || parent.accumulation_length() == duration::zero())
    throw std::runtime_error{"Product expects accumulations as input"};
  if (parent.accumulation_length() != parent.time_step())
    throw std::runtime_error{"Product expects input period to match accum length"};
  if (accumulation_length_.count() % parent.accumulation_length().count() != 0)
    throw std::runtime_error{"Product expects input length to divide into output length"};
  auto parent_times = parent.forecast_times();
  if (!parent_times)
    throw std::runtime_error{"Product expects a time series as input"};

  // determine our sequence length
  forecasts_in_ = parent_times->size();
  seq_length_ = accumulation_length_ / parent.accumulation_length();
  // if we are unable to look back and find QPE inputs from T<=0 then the first actual forecast that
  // we will be able to output is for the T+length forecast
  if (input_)
    times_ = *parent_times;
  else
  {
    times_.resize(forecasts_in_ - (seq_length_ - 1));
    for (auto i = 0; i < times_.size(); ++i)
      times_[i] = (*parent_times)[i + (seq_length_ - 1)];
    trace(
          trace_level::log
        , "No input provided for accum_aggregate to acquire T<0.  First output time step will be forecast {}."
        , seq_length_ - 1);
  }
  lengths_.resize(times_.size());
  lengths_.fill(accumulation_length_);
  buffers_.resize(parent.series_labels()->size());

  // ensure our buffer sequences for each member are allocated
  // note: does not actually allocate the array2f buffers - that is done on demand
  for (auto& bufs : buffers_)
    bufs.resize(seq_length_ + 1);

  // if we have access to historical data then preload historical qpe
  if (input_)
  {
    // we need to load previous images in ascending time order so that our last image coresponds to the
    // reference time itself.  note we skip the first image because that one would be immediately subtracted
    // from the accumulator anyway on first forecast.  we just leave that slot null since we check for null
    // before subtracting the image - this prevents us from subtracting it and saves us a file load and memory
    auto accum = std::make_shared<array_forc>();
    auto& raccum = accum->emplace<array2f>(parent.grid_shape());
    raccum.fill(0.0f);
    auto step = parent.time_step();
    auto first = reference_time + times_[0] - seq_length_ * step;
    for (auto idx = 1; idx < seq_length_; ++idx)
    {
      auto time = first + idx * step;

      // read the historical grid for this time step
      // since we are using the standard input interface the values will be scaled to rain rate
      auto data = std::make_shared<array_forc>();
      auto& rdata = data->emplace<array2f>(raccum.shape());
      if (!input_->read(time_point{}, 0, time, rdata))
      {
        trace(
              trace_level::warning
            , "Past accumulation data for time {} missing.  Early aggregated accumulations will be incomplete."
            , time);
        continue;
      }

      // scale the data back to our time step length
      // TODO it would be nice if we could change the input interface to allow us to request different units so we don't need this
      rdata.multiply(std::chrono::duration_cast<std::chrono::duration<float>>(step).count() / 3600.0f);

      // add it into our accumulator
      raccum.add(rdata);

      // compress the buffer if desired
      // after this block rdata may be a dangling reference since we change the variant's held type - don't use it!
      if (compression_ > 0)
      {
        auto zc = zlib_compress(span{reinterpret_cast<char const*>(rdata.data()), rdata.size_bytes()}, compression_, 0);
        auto& cdata = data->emplace<array1<char>>(zc.second);
        for (auto i = 0; i < cdata.size(); ++i)
          cdata[i] = zc.first[i];
      }

      // share it to the sequence of each member
      for (auto& buffers : buffers_)
      {
        buffers[idx].data = data;
        buffers[idx].shared = true;
      }
    }

    // share a copy of the initial accumulator to each member
    /* the members will replace this with their own uniquely owned buffer at time 0.  we never compress the accumulation
     * buffer held at buffers[seq_length_] since it is used every time step. */
    for (auto& buffers : buffers_)
    {
      buffers[seq_length_].data = accum;
      buffers[seq_length_].shared = true;
    }
  }

  accumulation::initialize(parent, reference_time);
}

auto accumulation_sliding::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  // we subtract the oldest and add the newest every time step, it uses one more image in the buffer chain
  auto& buffers = buffers_[member];
  auto idx = forecast % seq_length_;

  // setup our accumulation buffer when starting a new member
  /* we don't do this in initialize() because that would force us to initialize memory for all members at
   * once. depending on the number of engines used, we may only need a subset of members in memory simultaneously
   * so by allocating this buffer on first forecast for the member (and clearing on last forcast below) we minimize
   * the memory footprint of the buffers. the exception is the buffer pages for the historical (T<=0) times. these
   * are loaded in initialize() and shared using shared_ptr so that we don't need to re-read input files for each
   * member. */
  if (forecast == 0)
  {
    /* if we have historical data then a shared copy of the accumulator will be in our slot, so we want to copy
     * values from it into a non-shared buffer dedicated to this member.  there is no need to check if the shared
     * buffer is compressed since we never compress the initial accumulation buffer - see initialize() above. */
    auto tmp = std::make_shared<array_forc>();
    auto& rtmp = tmp->emplace<array2f>(data.shape());
    if (buffers[seq_length_].data)
      rtmp.copy(std::get<array2f>(*buffers[seq_length_].data));
    else
      rtmp.fill(0.0f);

    std::swap(tmp, buffers[seq_length_].data);
    buffers[seq_length_].shared = false;
  }

  /* note that the accum buffer at buffers[seq_length_] is never compressed since it is usd every time step
   * so there's no need to check for a compressed array or decompress it. */
  auto& accum = std::get<array2f>(*buffers[seq_length_].data);

  // subtract the oldest buffer from the accumulator (if available)
  if (buffers[idx].data)
  {
    if (std::holds_alternative<array1<char>>(*buffers[idx].data))
    {
      auto rdata = array2f(data.shape());
      zlib_decompress(std::get<array1<char>>(*buffers[idx].data), span{reinterpret_cast<char*>(rdata.data()), rdata.size_bytes()});
      accum.subtract(rdata);
    }
    else
      accum.subtract(std::get<array2f>(*buffers[idx].data));
  }

  // add our new image into the accumulator
  accum.add(data);

  // output the downstream product (suppress early forecasts if no historical data was available for initialization)
  // we don't pass on the flow field since it is no longer accurate (we've included extra history that wasn't tracked)
  if (auto fcout = input_ ? forecast : forecast - (seq_length_ - 1); fcout >= 0)
    accumulation::write(member, fcout, accum, nullptr);

  // cleanup at the end of a member and bail since we don't need to copy the new field to our buffers
  if (forecast == forecasts_in_)
  {
    buffers.clear();
    return;
  }

  // allocate a buffer for this slot if needed
  if (!buffers[idx].data || buffers[idx].shared)
  {
    buffers[idx].data = std::make_shared<array_forc>();
    buffers[idx].shared = false;
  }

  // copy the current image into the slot so that we can subtract it at a later time step
  if (compression_ > 0)
  {
    auto zc = zlib_compress(span{reinterpret_cast<char const*>(data.data()), data.size_bytes()}, compression_, 0);
    auto& cdata = buffers[idx].data->emplace<array1<char>>(zc.second);
    for (auto i = 0; i < cdata.size(); ++i)
      cdata[i] = zc.first[i];
  }
  else
  {
    buffers[idx].data->emplace<array2f>(data.shape()).copy(data);
  }
}

auto accumulation_sliding::write_all(array2f const& data) -> void
{
  /* if every grid cell is a NaN (implying a null_forecast_mode of missing), then we know that every output will also
   * be all NaNs.  in this case we can defer straight to the standard write_all behaviour.  otherwise we need to
   * actually run the product passing in copies of the grid because the historical data may contain significant values
   * that actually lead to different results for different time steps despite the same input. */
  if (!input_ || std::find_if(data.begin(), data.end(), [](auto v){ return !std::isnan(v); }) != data.end())
  {
    accumulation::write_all(data);
    return;
  }

  // simulate the forecast using as many threads as we can
  // TODO - this is still much much slower than a true null forecast... we should try to further optimise it
  thread_pool_distribute_loop(series_labels()->size(), [&](int member)
  {
    for (auto fc = 0; fc < times_.size(); ++fc)
      write(member, fc, data, nullptr);
  });
}

accumulation_sequential::accumulation_sequential(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : accumulation{config, user_parameters, parent_metadata}
  , clock_sync_{config.optional("clock_sync", false)}
  , clock_offset_{config.optional("clock_offset", duration{})}
  , input_{[&]{ auto c = config.find("input"); return c ? input::instantiate(*c, user_parameters) : nullptr; }()}
{ }

auto accumulation_sequential::initialize(product const& parent, time_point reference_time) -> void
{
  auto parent_length = parent.accumulation_length();
  auto parent_times = parent.forecast_times();

  if (parent.units() != product_units::accumulation || parent_length == duration::zero())
    throw std::runtime_error{"Product expects accumulations as input"};
  if (parent_length != parent.time_step())
    throw std::runtime_error{"Product expects input period to match accum length"};
  if (accumulation_length_.count() % parent_length.count() != 0)
    throw std::runtime_error{"Product expects input length to divide into output length"};
  if (!parent_times)
    throw std::runtime_error{"Product expects a time series as input"};

  /* we could avoid this requirement and just suppress our output if parent provides no time steps, but that is a bit
   * too esoteric and not worth the effort right now. */
  if (parent_times->size() == 0)
    throw std::runtime_error{"Product requires at least one input time step"};

  auto seq_length = accumulation_length_ / parent_length;
  auto offset = 0;

  /* if our parent product is already itself a clock synchronized output then the model core reference time
   * will not necessarily line up nicely with our accumulation windows.  we need to instead align against the
   * start of the input seqeuence from the parent.  so we subtract one accumulation length from the time of
   * the first parent forecast to create this pseduo-reference time that is used for clock alignment. */
  auto pseudo_reference_time = reference_time + parent_times->front() - parent_length;

  if (clock_sync_)
  {
    auto offset_epoch = (pseudo_reference_time - clock_offset_).time_since_epoch();
    if (offset_epoch % parent_length != duration::zero())
      throw std::runtime_error{"Product expects input times compatible with clock synchronization"};
    offset = (offset_epoch % accumulation_length_) / parent_length;
    offset %= seq_length;
  }

  auto offset_length = offset * parent_length;
  auto forecast_count = (parent_times->back() + offset_length) / accumulation_length_;
  if (offset != 0 && !input_)
    --forecast_count;

  forecast_map_.resize(parent_times->size());
  times_.resize(forecast_count);
  for (auto i = 0, o = 0; i < forecast_map_.size(); ++i)
  {
    if (!input_ && offset != 0 && i < seq_length - offset)
    {
      // first forecast would need historical data but we have no input - suppress this forecast
      // -2 is a code for "don't accumulate or output on this time step"
      forecast_map_[i] = -2;
    }
    else if ((i + 1 + offset) % seq_length == 0)
    {
      // this time step is the end of a window, map contains index of forecast to output
      forecast_map_[i] = o;
      times_[o] = (*parent_times)[i];
      ++o;
    }
    else
    {
      // intermediate time step
      // -1 is a code for "acumulate this time step, but don't output anything"
      forecast_map_[i] = -1;
    }
  }

  if (trace_active(trace_level::verbose))
  {
    trace(trace_level::verbose, "Accumulation plan: ref {}, sync {}, offset {}", reference_time, clock_sync_, clock_offset_);
    if (input_)
    {
      for (auto os = offset - 1; os >= 0; --os)
        trace(trace_level::verbose, "  {: >3} {} -> preload", -os - 1, pseudo_reference_time - os * parent_length);
    }
    for (auto i = 0; i < forecast_map_.size(); ++i)
    {
      auto t = reference_time + (*parent_times)[i];
      auto o = forecast_map_[i];
      if (o == -2)
        trace(trace_level::verbose, "  {: >3} {} -> skip", i, t);
      else if (o == -1)
        trace(trace_level::verbose, "  {: >3} {} -> accum", i, t);
      else
        trace(trace_level::verbose, "  {: >3} {} -> output {}, {}, {}", i, t, o, times_[o], reference_time + times_[o]);
    }
  }

  lengths_.resize(times_.size());
  lengths_.fill(accumulation_length_);
  buffers_.resize(parent.series_labels()->size());
  for (auto& buf : buffers_)
    buf.resize(parent.grid_shape());

  // if we have access to historical data then preload historical qpe
  if (input_)
  {
    auto data = array2f{parent.grid_shape()};
    auto accum = array2f{data.shape()};
    accum.fill(0.0f);
    for (auto os = offset - 1; os >= 0; --os)
    {
      auto time = pseudo_reference_time - os * parent_length;

      // read the historical grid for this time step
      // since we are using the standard input interface the values will be scaled to rain rate
      if (!input_->read(time_point{}, 0, time, data))
      {
        trace(
              trace_level::warning
            , "Past accumulation data for time {} missing.  Early aggregated accumulations will be incomplete."
            , time);
        continue;
      }

      // scale the data back to our time step length
      // TODO it would be nice if we could change the input interface to allow us to request different units so we don't need this
      data.multiply(std::chrono::duration_cast<std::chrono::duration<float>>(parent_length).count() / 3600.0f);

      // add it into our accumulator
      accum.add(data);
    }

    // initialize each member with our partial accumulation
    for (auto& buf : buffers_)
      buf.copy(accum);
  }
  else
  {
    for (auto& buf : buffers_)
      buf.fill(0.0f);
  }

  accumulation::initialize(parent, reference_time);
}

auto accumulation_sequential::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  // bail out if we are in the partial window before the first full window
  if (forecast_map_[forecast] == -2)
    return;

  // add the grid to the current accumulation for this member
  buffers_[member].add(data);

  // bail out if we are in an intermediate step and don't need to output yet
  if (forecast_map_[forecast] == -1)
    return;

  // output the now completed accumulation
  accumulation::write(member, forecast_map_[forecast], buffers_[member], nullptr);

  // reset the accumulation for the next member
  buffers_[member].fill(0.0f);
}

auto accumulation_sequential::write_all(array2f const& data) -> void
{
  /* if every grid cell is a NaN (implying a null_forecast_mode of missing), then we know that every output will also
   * be all NaNs.  in this case we can defer straight to the standard write_all behaviour.  otherwise we need to
   * actually run the product passing in copies of the grid because the historical data may contain significant values
   * that actually lead to different results for different time steps despite the same input. */
  if (!input_ || std::find_if(data.begin(), data.end(), [](auto v){ return !std::isnan(v); }) != data.end())
  {
    accumulation::write_all(data);
    return;
  }

  // simulate the forecast using as many threads as we can
  // TODO - this is still much much slower than a true null forecast... we should try to further optimise it
  thread_pool_distribute_loop(series_labels()->size(), [&](int member)
  {
    for (auto fc = 0; fc < times_.size(); ++fc)
      write(member, fc, data, nullptr);
  });
}

filter_members::filter_members(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : ensemble{config, user_parameters, parent_metadata}
  , members_all_{config_array_to_container<array1i>(config["members"])}
  , require_all_{config.optional("require_all", true)}
{ }

auto filter_members::initialize(product const& parent, time_point reference_time) -> void
{
  auto& members_in = *parent.series_labels();

  member_map_.resize(members_in.size());
  member_map_.fill(-1);

  // find the desired member in the parent member list
  auto members_out = vector<int>();
  members_out.reserve(members_in.size());
  for (auto imember : members_all_)
  {
    auto iin = 0;
    while (iin < members_in.size() && std::abs(members_in[iin] - imember) > 0.001)
      ++iin;
    if (iin == members_in.size())
    {
      if (require_all_)
        throw std::runtime_error{format("Member {} is not provided by parent of filter_members product", imember)};
      continue;
    }
    if (member_map_[iin] != -1)
      throw std::runtime_error{format("Member {} listed multiple times", iin)};
    member_map_[iin] = members_out.size();
    members_out.emplace_back(imember);
  }

  members_.resize(members_out.size());
  for (auto i = 0; i < int(members_out.size()); ++i)
    members_[i] = members_out[i];

  // suppress downstream stuff if our filter selected no members
  if (members_.size() == 0)
  {
    trace(trace_level::verbose, "No members selected by filter_members product, suppressing output and downstream products");
    return;
  }

  ensemble::initialize(parent, reference_time);
}

auto filter_members::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  auto member_out = member_map_[member];
  if (member_out < 0)
    return;
  ensemble::write(member_out, forecast, data, flow);
}

auto filter_members::write_all(array2f const& data) -> void
{
  if (members_.size() == 0)
    return;
  ensemble::write_all(data);
}

auto filter_members::finalize() -> void
{
  if (members_.size() == 0)
    return;
  ensemble::finalize();
}

auto filter_members::abort() -> void
{
  if (members_.size() == 0)
    return;
  ensemble::abort();
}

filter_time::filter_time(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : ensemble{config, user_parameters, parent_metadata}
  , min_lead_{config.optional("min_lead_time", duration::min())}
  , max_lead_{config.optional("max_lead_time", duration::max())}
{ }

auto filter_time::initialize(product const& parent, time_point reference_time) -> void
{
  auto& times_in = *parent.forecast_times();

  auto ifrom = std::lower_bound(times_in.begin(), times_in.end(), min_lead_);
  auto itill = std::upper_bound(times_in.begin(), times_in.end(), max_lead_);

  fc_from_ = ifrom - times_in.begin();
  fc_till_ = itill - times_in.begin();
  times_.resize(fc_till_ - fc_from_);
  for (auto i = 0; i < times_.size(); ++i)
    times_[i] = times_in[fc_from_ + i];

  if (auto plengths = parent.forecast_lengths())
  {
    lengths_.resize(fc_till_ - fc_from_);
    for (auto i = 0; i < lengths_.size(); ++i)
      lengths_[i] = (*plengths)[fc_from_ + i];
  }

  // suppress downstream processing if our filter slected no forecasts
  if (times_.size() == 0)
  {
    trace(trace_level::verbose, "No forecasts selected by filter_time product, suppressing output and downstream products");
    return;
  }

  ensemble::initialize(parent, reference_time);
}

auto filter_time::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  if (forecast < fc_from_ || forecast >= fc_till_)
    return;

  ensemble::write(member, forecast - fc_from_, data, flow);
}

auto filter_time::write_all(array2f const& data) -> void
{
  if (times_.size() == 0)
    return;
  ensemble::write_all(data);
}

auto filter_time::finalize() -> void
{
  if (times_.size() == 0)
    return;
  ensemble::finalize();
}

auto filter_time::abort() -> void
{
  if (times_.size() == 0)
    return;
  ensemble::abort();
}

static constexpr uint64_t time_lag_file_magic = 0xfaa230c8e5f03b18;
static constexpr uint32_t time_lag_file_version = 2;

struct time_lag_header
{
  uint64_t  magic;
  uint32_t  version;
  int       members;
  vec2i     grid_shape;
};

struct time_lag_chunk_header
{
  int       member;
  uint32_t  chunk_size;   // size of compressed data chunk
};

time_lagged::time_lagged(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : ensemble{config, user_parameters, parent_metadata}
  , lags_{config["lags"]}
  , lag_offset_user_{config.optional("lag_offset", duration::zero())}
  , lead_time_limit_{config.optional("lead_time_limit", duration::zero())}
  , cache_path_{format(config["cache"].string(), "param"_a = user_parameters)}
  , resolution_{config.optional("cache_resolution", std::numeric_limits<float>::quiet_NaN())}
  , compression_{config.optional("cache_compression", 1)}
{ }

auto time_lagged::initialize(product const& parent, time_point reference_time) -> void
{
  parent_times_ = parent.forecast_times();
  lag_offset_ = lag_offset_user_ == duration::zero() ? parent.time_step() : lag_offset_user_;

  if (lag_offset_ == duration::zero())
    throw std::runtime_error{"Time lagging requires regular time step"};

  /* time lagging multiplies the number of members by the number of lags.  lagged members are given the higer numbers
   * so that [0..N) == this forecast, [N,2N) == lag 1, [2N,3N] == lag 2 etc */
  members_per_lag_ = parent.series_labels()->size();
  members_.resize(members_per_lag_ * (lags_ + 1));
  for (auto ilag = 0; ilag <= lags_; ++ilag)
    for (auto imember = 0; imember < members_per_lag_; ++imember)
      members_[ilag * members_per_lag_ + imember] = (ilag * members_per_lag_) + (*parent.series_labels())[imember];

  /* the latest time step that we will be able to successfully time lag is one corresponding to the last possible lead time
   * of our parent, minus lags * lag step.  the main problem is that we don't know for sure what the last possible lead time
   * of our parent is.  in the most common case it will be equal to the lead time of the last forecast output by the parent,
   * so we default to that.  but in the case of a clock synchronized accumulation (for example) we might only output up to
   * some less than maximal lead time for this reference time, but the previous reference time we might have gone later.
   * e.g. clock synced 1-hour accums, and our last engine lead time is 00:10, meaning the last lead time of our parent would
   * be 00:00 that is our second to last engine lead time.  however the previous reference time would also have been able
   * to output 00:00 which would have been its last lead time.  since the parent product doesn't give us this 'maximum
   * possible lead time' (since it may not know itself), we are forced to allow the user to override it. */
  auto lead_time_limit = lead_time_limit_ == duration::zero() ? parent_times_->back() : lead_time_limit_;
  auto max_lead = lead_time_limit - (lag_offset_ * lags_);
  auto time_count = parent_times_->size();
  while (time_count > 0 && (*parent_times_)[time_count - 1] > max_lead)
    --time_count;

  times_.resize(time_count);
  for (auto i = 0; i < times_.size(); ++i)
    times_[i] = (*parent_times_)[i];
  if (times_.size() == 0)
    trace(trace_level::warning, "Insufficient forecasts to support {} time lags", lags_);

  if (auto plengths = parent.forecast_lengths())
  {
    lengths_.resize(times_.size());
    for (auto i = 0; i < lengths_.size(); ++i)
      lengths_[i] = (*plengths)[i];
  }

  reference_time_ = reference_time;
  lag_data_ = vector<lag_data>(times_.size() * lags_);
  streams_ = vector<lag_stream>(parent_times_->size());

  if (trace_active(trace_level::verbose))
  {
    trace(trace_level::verbose, "Time lag plan:");
    trace(trace_level::verbose, "   reference {}", reference_time);
    trace(trace_level::verbose, "        lags {}", lags_);
    trace(trace_level::verbose, "  lag offset {}", lag_offset_);
    trace(trace_level::verbose, "  lead limit {} ({})", lead_time_limit, reference_time + lead_time_limit);
    trace(trace_level::verbose, "    max lead {} ({})", max_lead, reference_time + max_lead);
    if (times_.size() != 0)
      trace(trace_level::verbose, "   last lead {} ({})", times_.back(), reference_time + times_.back());
  }

  ensemble::initialize(parent, reference_time);
}

/* note that we don't currently bother lagging the flow field.  this means that things will not work properly if you
 * intend to feed the time lagged ensemble into a downstream product that requires flow information.  there's nothing
 * stopping us from storing/restoring the flow field - but it seems like a large waste of time right now.  the only
 * product that (will) need a flow field is properly backward advected accumulations.  give the high cost of doing
 * these accumulations it is expected that it would almost certainly be better to time lag the accumulated ensemble
 * rather than time lagging the rain rates and then having to run backward accumulation on the same members again
 * that were done last cycle. */
auto time_lagged::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  // write the new member out to our lag cache for next time
  write_lag(member, forecast, data);

  // don't output or forward the last lag_ forecasts because they will not have the full set of lagged members
  if (forecast >= times_.size())
    return;

  // forward this member to our children and outputs (except the last lag_ forecasts which can't be time lagged)
  ensemble::write(member, forecast, data, flow);

  // read the equivalent member from each lag and forward it to our children and output
  for (auto ilag = 1; ilag <= lags_; ++ilag)
  {
    auto lag_data = read_lag(member, forecast, ilag);

    /* if we are missing the data for this lag just duplicate the current ensemble output.  this isn't ideal, but it
     * ensures we still get valid probabilistic products (albiet with silently reduced probabilistic resolution). */
    if (lag_data.size() == 0)
    {
      trace(trace_level::warning, "Failed to acquire time lagged input for lag {}", ilag);
      ensemble::write(member + ilag * members_per_lag_, forecast, data, nullptr);
      continue;
    }

    // forward the lagged forecast to our children and outputs as if we had generated it ourselves
    ensemble::write(member + ilag * members_per_lag_, forecast, lag_data, nullptr);
  }
}

auto time_lagged::format_lag_path(int forecast, int lag) const -> string
{
  /* we MUST use the parent times_ array here and NOT our own times_ array.  this is because our times_ array is
   * limited to only those forecasts which will be forwarded to outputs and downstream products.  but we need to
   * call this function for any forecast that we receive from the parent (since we will be caching it regardless of
   * whether it is immediately forwarded downstream. */
  return format(
        "{}/{:%Y%m%dT%H%M%S}.{:%Y%m%dT%H%M%S}.timelag"
      , cache_path_
      , reference_time_ - lag * lag_offset_
      , reference_time_ + (*parent_times_)[forecast]);
}

static auto pack_compress(span<float const> rdata, float resolution, int compression, int offset) -> pair<array1<char>, int>
{
  if (std::isnan(resolution))
  {
    return zlib_compress(span{reinterpret_cast<char const*>(rdata.data()), rdata.size_bytes()}, compression, offset);
  }
  else
  {
    auto idata = array1<int16_t>(rdata.size());
    for (auto i = 0; i < idata.size(); ++i)
      idata[i] = std::isnan(rdata[i])
        ? std::numeric_limits<int16_t>::lowest()
        : std::lround(rdata[i] / resolution);
    return zlib_compress(span{reinterpret_cast<char const*>(idata.data()), idata.size_bytes()}, compression, offset);
  }
}

static auto decompress_unpack(span<char const> cdata, float resolution, span<float> rdata) -> void
{
  if (std::isnan(resolution))
  {
    if (zlib_decompress(cdata, span{reinterpret_cast<char*>(rdata.data()), rdata.size_bytes()}) != rdata.size_bytes())
      throw std::runtime_error{"Lagged forecast decompressed to incorrect size"};
  }
  else
  {
    auto idata = array1<int16_t>(rdata.size());
    if (zlib_decompress(cdata, span{reinterpret_cast<char*>(idata.data()), idata.size_bytes()}) != idata.size_bytes())
      throw std::runtime_error{"Lagged forecast decompressed to incorrect size"};
    for (auto i = 0; i < idata.size(); ++i)
      rdata[i] = idata[i] == std::numeric_limits<int16_t>::lowest()
        ? std::numeric_limits<float>::quiet_NaN()
        : idata[i] * resolution;
  }
}

auto time_lagged::write_lag(int member, int forecast, array2f const& data) -> void
{
  auto& ls = streams_[forecast];

  // encode the chunk we need to output to the file
  auto cdata = pack_compress(data, resolution_, compression_, sizeof(time_lag_chunk_header));
  auto hdr = reinterpret_cast<time_lag_chunk_header*>(cdata.first.data());
  hdr->member = member;
  hdr->chunk_size = cdata.second - sizeof(time_lag_chunk_header);

  // ensure access to the stream itself is thread safe
  auto lock = std::lock_guard<std::mutex>{ls.mutex};

  // do we need to initialize the stream?
  if (!ls.stream)
  {
    ls.stream = save_binary_stream(format_lag_path(forecast, 0));

    auto hdr = time_lag_header{};
    hdr.magic = time_lag_file_magic;
    hdr.version = time_lag_file_version;
    hdr.members = members_per_lag_;
    hdr.grid_shape = data.shape();
    ls.stream->write(span{reinterpret_cast<char const*>(&hdr), sizeof(time_lag_header)});
  }

  // write our compressed chunk
  ls.stream->write(span{cdata.first.data(), cdata.second});

  // if we've written all members close the stream and free it up
  if (++ls.counter == members_per_lag_)
  {
    ls.stream->commit();
    ls.stream.reset();
  }
}

auto time_lagged::read_lag(int member, int forecast, int lag) -> array2f
try
{
  auto& ld = lag_data_[forecast * lags_ + (lag - 1)];

  // do we need to load in the raw lag data?
  {
    auto lock = std::lock_guard<std::mutex>{ld.mutex};
    if (!ld.raw)
    {
      /* make sure we init chunks before reading the blob.  this ensures that if an exception is thrown we have
       * already set the chunk sizes to 0 so that future iterations will know to skip it. */
      ld.raw = std::make_unique<lag_raw_data>();
      ld.raw->chunks.resize(members_per_lag_);
      ld.raw->chunks.fill({0, 0});
      ld.raw->data = load_binary_blob(format_lag_path(forecast, lag), true);

      if (ld.raw->data.size() != 0)
      {
        // sanity check the header
        if (ld.raw->data.size_bytes() < int(sizeof(time_lag_header)))
          throw std::runtime_error{"Truncated file header"};
        auto hdr = reinterpret_cast<time_lag_header const*>(ld.raw->data.data());
        if (hdr->magic != time_lag_file_magic)
          throw std::runtime_error{"Magic token mismatch"};
        if (hdr->version != time_lag_file_version)
          throw std::runtime_error{"File version mismatch"};
        if (hdr->members != members_per_lag_)
          throw std::runtime_error{"Member count mismatch"};
        if (hdr->grid_shape != grid_shape())
          throw std::runtime_error{"Grid shape mismatch"};

        // scan the file and determine the offset to the chunk for each member
        auto pos = int(sizeof(time_lag_header));
        while (pos < ld.raw->data.size())
        {
          if (pos + sizeof(time_lag_chunk_header) > size_t(ld.raw->data.size()))
            throw std::runtime_error{"Truncated chunk header"};
          auto chdr = reinterpret_cast<time_lag_chunk_header const*>(&ld.raw->data[pos]);
          if (chdr->member < 0 || chdr->member >= members_per_lag_)
            throw std::runtime_error{"Invalid member index"};
          if (ld.raw->chunks[chdr->member].offset != 0)
            throw std::runtime_error{"Duplicated member chunk"};
          if (pos + sizeof(time_lag_chunk_header) + chdr->chunk_size > size_t(ld.raw->data.size()))
            throw std::runtime_error{"Truncated chunk data"};

          ld.raw->chunks[chdr->member].offset = pos + sizeof(time_lag_chunk_header);
          ld.raw->chunks[chdr->member].size = chdr->chunk_size;
          pos += sizeof(time_lag_chunk_header) + chdr->chunk_size;
        }
        if (pos != ld.raw->data.size())
          throw std::runtime_error{"Unexpected file size (too long)"};

        // check that all chunks are accounted for
        for (auto& os : ld.raw->chunks)
          if (os.offset == 0)
            throw std::runtime_error{"Missing chunk"};
      }
    }
  }

  // bail if the data is unavailable
  if (ld.raw->chunks[member].size == 0)
    return {};

  // decompress the chunk
  /* it is safe to do this without holding the lock because we know that the raw data has been initialized by now,
   * and we also know that it can't be uninitialized until the counter == members_per_lag_ which it won't be until
   * we apply our own increment to it.  this allows different threads to decompress at the same time. */
  auto cdata = span<char const>{ld.raw->data.data() + ld.raw->chunks[member].offset, ld.raw->chunks[member].size};
  auto rdata = array2f{grid_shape()};
  decompress_unpack(cdata, resolution_, rdata);

  // if we've now read all members free up the memory for this forecast
  {
    auto lock = std::lock_guard<std::mutex>{ld.mutex};
    if (++ld.counter == members_per_lag_)
    {
      ld.raw.reset();
      ld.counter = 0;
    }
  }

  return rdata;
}
catch (std::exception const& err)
{
  trace(trace_level::error, "Exception while acquiring time lagged input: {}", err);
  return {};
}

median_filter::median_filter(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : ensemble{config, user_parameters, parent_metadata}
  , kernel_size_{config["kernel_size"]}
{
  if (kernel_size_ < 0 || kernel_size_ % 2 == 0)
    throw std::runtime_error{"Invalid kernel size specified, must be odd"};
}

auto median_filter::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  auto ps = profile::scope{prof_prod_median};

  auto buf = array2f{data.shape()};
  auto kernel = array2f{vec2i{kernel_size_, kernel_size_}};

  // we unroll the 3x3 kernel case, which yields a moderate (~15%) performance improvement
  if (kernel_size_ == 3)
  {
    // top row
    {
      auto constexpr y = 0;
      // left column
      {
        auto constexpr x = 0;
        kernel[0][0] = data[ y ][ x ]; kernel[0][1] = data[ y ][ x ]; kernel[0][2] = data[ y ][x+1];
        kernel[1][0] = data[ y ][ x ]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][x+1];
        kernel[2][0] = data[y+1][ x ]; kernel[2][1] = data[y+1][ x ]; kernel[2][2] = data[y+1][x+1];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[0][0] = kernel[1][1];
      }

      // main body
      for (auto x = 1; x < buf.shape().x - 1; ++x)
      {
        kernel[0][0] = data[ y ][x-1]; kernel[0][1] = data[ y ][ x ]; kernel[0][2] = data[ y ][x+1];
        kernel[1][0] = data[ y ][x-1]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][x+1];
        kernel[2][0] = data[y+1][x-1]; kernel[2][1] = data[y+1][ x ]; kernel[2][2] = data[y+1][x+1];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[0][x] = kernel[1][1];
      }

      // right column
      {
        auto const x = buf.shape().x - 1;
        kernel[0][0] = data[ y ][x-1]; kernel[0][1] = data[ y ][ x ]; kernel[0][2] = data[ y ][ x ];
        kernel[1][0] = data[ y ][x-1]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][ x ];
        kernel[2][0] = data[y+1][x-1]; kernel[2][1] = data[y+1][ x ]; kernel[2][2] = data[y+1][ x ];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[0][x] = kernel[1][1];
      }
    }

    // main body
    for (auto y = 1; y < buf.shape().y - 1; ++y)
    {
      // left column
      {
        auto constexpr x = 0;
        kernel[0][0] = data[y-1][ x ]; kernel[0][1] = data[y-1][ x ]; kernel[0][2] = data[y-1][x+1];
        kernel[1][0] = data[ y ][ x ]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][x+1];
        kernel[2][0] = data[y+1][ x ]; kernel[2][1] = data[y+1][ x ]; kernel[2][2] = data[y+1][x+1];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[y][0] = kernel[1][1];
      }

      // main body
      for (auto x = 1; x < buf.shape().x - 1; ++x)
      {
        kernel[0][0] = data[y-1][x-1]; kernel[0][1] = data[y-1][ x ]; kernel[0][2] = data[y-1][x+1];
        kernel[1][0] = data[ y ][x-1]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][x+1];
        kernel[2][0] = data[y+1][x-1]; kernel[2][1] = data[y+1][ x ]; kernel[2][2] = data[y+1][x+1];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[y][x] = kernel[1][1];
      }

      // right column
      {
        auto const x = buf.shape().x - 1;
        kernel[0][0] = data[y-1][x-1]; kernel[0][1] = data[y-1][ x ]; kernel[0][2] = data[y-1][ x ];
        kernel[1][0] = data[ y ][x-1]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][ x ];
        kernel[2][0] = data[y+1][x-1]; kernel[2][1] = data[y+1][ x ]; kernel[2][2] = data[y+1][ x ];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[y][x] = kernel[1][1];
      }
    }

    // bottom row
    {
      auto const y = buf.shape().y - 1;

      // left column
      {
        auto constexpr x = 0;
        kernel[0][0] = data[y-1][ x ]; kernel[0][1] = data[y-1][ x ]; kernel[0][2] = data[y-1][x+1];
        kernel[1][0] = data[ y ][ x ]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][x+1];
        kernel[2][0] = data[ y ][ x ]; kernel[2][1] = data[ y ][ x ]; kernel[2][2] = data[ y ][x+1];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[y][0] = kernel[1][1];
      }

      // main body
      for (auto x = 1; x < buf.shape().x - 1; ++x)
      {
        kernel[0][0] = data[y-1][x-1]; kernel[0][1] = data[y-1][ x ]; kernel[0][2] = data[y-1][x+1];
        kernel[1][0] = data[ y ][x-1]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][x+1];
        kernel[2][0] = data[ y ][x-1]; kernel[2][1] = data[ y ][ x ]; kernel[2][2] = data[ y ][x+1];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[y][x] = kernel[1][1];
      }

      // right column
      {
        auto x = buf.shape().x - 1;
        kernel[0][0] = data[y-1][x-1]; kernel[0][1] = data[y-1][ x ]; kernel[0][2] = data[y-1][ x ];
        kernel[1][0] = data[ y ][x-1]; kernel[1][1] = data[ y ][ x ]; kernel[1][2] = data[ y ][ x ];
        kernel[2][0] = data[ y ][x-1]; kernel[2][1] = data[ y ][ x ]; kernel[2][2] = data[ y ][ x ];
        std::nth_element(kernel.begin(), kernel.begin() + 4, kernel.end());
        buf[y][x] = kernel[1][1];
      }
    }
  }
  else
  {
    auto kside = kernel_size_ / 2;
    auto kmid = kernel.size() / 2;

    // top edge
    for (auto y = 0; y < kside; ++y)
    {
      // left edge
      for (auto x = 0; x < kside; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[std::max(y - kside + ky, 0)][std::max(x - kside + kx, 0)];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }

      // main body
      for (auto x = kside; x < buf.shape().x - kside; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[std::max(y - kside + ky, 0)][x - kside + kx];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }

      // right edge
      for (auto x = buf.shape().x - kside; x < buf.shape().x; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[std::max(y - kside + ky, 0)][std::min(x - kside + kx, buf.shape().x - 1)];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }
    }

    // main body
    for (auto y = kside; y < buf.shape().y - kside; ++y)
    {
      // left edge
      for (auto x = 0; x < kside; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[y - kside + ky][std::max(x - kside + kx, 0)];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }

      // main body
      for (auto x = kside; x < buf.shape().x - kside; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[y - kside + ky][x - kside + kx];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }

      // right edge
      for (auto x = buf.shape().x - kside; x < buf.shape().x; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[y - kside + ky][std::min(x - kside + kx, buf.shape().x - 1)];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }
    }

    // bottom edge
    for (auto y = buf.shape().y - kside; y < buf.shape().y; ++y)
    {
      // left edge
      for (auto x = 0; x < kside; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[std::min(y - kside + ky, buf.shape().y - 1)][std::max(x - kside + kx, 0)];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }

      // main body
      for (auto x = kside; x < buf.shape().x - kside; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[std::min(y - kside + ky, buf.shape().y - 1)][x - kside + kx];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }

      // right edge
      for (auto x = buf.shape().x - kside; x < buf.shape().x; ++x)
      {
        for (auto ky = 0; ky < kernel_size_; ++ky)
          for (auto kx = 0; kx < kernel_size_; ++kx)
            kernel[ky][kx] = data[std::min(y - kside + ky, buf.shape().y - 1)][std::min(x - kside + kx, buf.shape().x - 1)];
        std::nth_element(kernel.begin(), kernel.begin() + kmid, kernel.end());
        buf[y][x] = kernel[kside][kside];
      }
    }
  }

  ensemble::write(member, forecast, buf, flow);
}

ensemble_mean::ensemble_mean(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : product{config, user_parameters, parent_metadata}
{ }

auto ensemble_mean::initialize(product const& parent, time_point reference_time) -> void
{
  member_count_ = parent.series_labels()->size();
  slots_.clear();
  slots_ = vector<time_slot>(parent.forecast_times()->size());
  product::initialize(parent, reference_time);
}

auto ensemble_mean::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  auto& slot = slots_[forecast];
  auto lock = std::lock_guard<std::mutex>{slot.mutex};

  // initialize a new time?
  if (slot.member_count == 0)
  {
    slot.sum.resize(data.shape());
    slot.sum.copy(data);
  }
  else
    slot.sum.add(data);
  ++slot.member_count;

  // write output for a completed forecast
  if (slot.member_count == member_count_)
  {
    auto inv_count = 1.0f / slot.member_count;
    slot.sum.multiply(inv_count);
    product::write(forecast, -1, slot.sum, flow);

    // free up memory for this time now that we've finisehd with it
    slot.sum = array2f{};
  }
}

probabilities::probabilities(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : product{config, user_parameters, parent_metadata}
{ }

auto probabilities::initialize(product const& parent, time_point reference_time) -> void
{
  member_count_ = parent.series_labels()->size();
  slots_.clear();
  slots_ = vector<time_slot>(parent.forecast_times()->size());

  if (member_count_ > 126)
    throw std::runtime_error{"Product type probability expects fewer than 126 members input"};

  /* we pass our probabilities to the output class as integers which need to be scaled (divided) by the number
   * of members to determine the actual floating point probabilitiy.  this metadatum is the mechanism that is
   * used to pass the appropriate scale factor to the output class. */
  output_metadata()[".prob-scale_factor"] = 1.0f / member_count_;

  product::initialize(parent, reference_time);
}

probabilities_scalar::probabilities_scalar(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : probabilities{config, user_parameters, parent_metadata}
  , thresholds_{config_array_to_container<array1f>(config["thresholds"])}
{
  std::sort(thresholds_.begin(), thresholds_.end());
}

auto probabilities_scalar::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  auto& slot = slots_[forecast];

  auto lock = std::lock_guard<std::mutex>{slot.mutex};

  // initialize a new time?
  if (slot.member_count == 0)
  {
    slot.buffers.reserve(thresholds_.size());
    for (auto t = 0; t < thresholds_.size(); ++t)
    {
      slot.buffers.emplace_back(data.shape());
      slot.buffers.back().fill(0);
    }
  }

  // accumulate exceedence counts
  for (auto i = 0; i < data.size(); ++i)
  {
    for (auto t = 0; t < thresholds_.size(); ++t)
    {
      if (slot.buffers[t].data()[i] == -1)
        break;
      if (data.data()[i] < thresholds_[t])
        break;
      if (std::isnan(data.data()[i]))
        slot.buffers[t].data()[i] = -1;
      else
        ++slot.buffers[t].data()[i];
    }
  }
  ++slot.member_count;

  // write output for a completed forecast
  if (slot.member_count == member_count_)
  {
    for (auto t = 0; t < thresholds_.size(); ++t)
      probabilities::write(t, forecast, slot.buffers[t], flow);

    // free up memory for this time now that we've finisehd with it
    slot.buffers.clear();
  }
}

auto probabilities_scalar::write_all(array2f const& data) -> void
{
  /* in theory the data array passed in could contain any values, meaning that if a value is higher than one of our
   * thresholds we would need to output a PoP of 100%.  then depending on the thresholds we might have a value that is
   * higher than some but not higher than others.  this would mean we have identical output for each forecast of a
   * given threshold, but potentially different output for each threshold.  in turn, this prevents us from using
   * the write_all functionality of the base product class (and therefore the output), which is very pessimistic.
   *
   * in reality, we know the data array will contain only zeros or nans because write_all is only used to provide null
   * forecast capability.  so rather than ruin our optimisation, we just throw an exception if a value is greater than
   * zero since this is currently impossible. */
  auto idata = array2<int8_t>{data.shape()};
  for (auto i = 0; i < data.size(); ++i)
  {
    if (data.data()[i] > 0.0)
      throw std::logic_error{"Unexpected value in probabilities_scalar::write_all"};
    idata.data()[i] = std::isnan(data.data()[i]) ? -1 : 0;
  }
  probabilities::write_all(idata);
}

probabilities_spatial::probabilities_spatial(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : probabilities{config, user_parameters, parent_metadata}
{
  // load thresholds from file
  try
  {
    // this must occur outside the netcdf lock
    auto file = nc::get_reference_file(format(config["thresholds_path"].string(), "param"_a = user_parameters));

    auto lock = nc::lock_mutex();

    auto& var = file->lookup_variable(config["thresholds_variable"].string());
    if (var.dimensions().size() != 3)
      throw std::runtime_error{"Thresholds variable has unexpected rank"};
    if (var.dimensions()[0]->size() == 0)
      throw std::runtime_error{"Thresholds variable contains no thresholds"};

    auto packer = cf::packer{};

    // load the threshold coordinates (i.e. ARI or AEP thresholds)
    auto& var_th = file->lookup_variable(var.dimensions()[0]->name());
    thresholds_.resize(var.dimensions()[0]->size());
    packer.read(var_th, thresholds_);

    // store metadata for the output
    output_metadata()[".th-name"] = var_th.name();
    if (auto att = var_th.att_get_as<optional<string>>("standard_name"))
      output_metadata()[".th-standard_name"] = std::move(att).value();
    if (auto att = var_th.att_get_as<optional<string>>("units"))
      output_metadata()[".th-units"] = std::move(att).value();
    if (auto att = var_th.att_get_as<optional<string>>("long_name"))
      output_metadata()[".th-long_name"] = std::move(att).value();

    // load the actual spatially varying threshold values
    thresholds_spatial_.reserve(thresholds_.size());
    for (auto t = 0; t < var.dimensions()[0]->size(); ++t)
    {
      thresholds_spatial_.emplace_back(vec2i{var.dimensions()[2]->size(), var.dimensions()[1]->size()});
      packer.read(var, thresholds_spatial_.back(), std::numeric_limits<float>::quiet_NaN(), {t});
    }
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{"Failed to load spatially varying probability thresholds"});
  }
}

auto probabilities_spatial::initialize(product const& parent, time_point reference_time) -> void
{
  if (thresholds_spatial_[0].shape() != parent.grid_shape())
    throw std::runtime_error{"Spatially varying thresholds grid size mismatch"};

  probabilities::initialize(parent, reference_time);
}

auto probabilities_spatial::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  auto& slot = slots_[forecast];
  auto lock = std::lock_guard<std::mutex>{slot.mutex};

  // initialize a new time?
  if (slot.member_count == 0)
  {
    slot.buffers.reserve(thresholds_.size());
    for (auto t = 0; t < thresholds_.size(); ++t)
    {
      slot.buffers.emplace_back(data.shape());
      for (auto i = 0; i < data.size(); ++i)
        slot.buffers.back().data()[i] = std::isnan(thresholds_spatial_[t].data()[i]) ? -1 : 0;
    }
  }

  // accumulate exceedence counts
  for (auto i = 0; i < data.size(); ++i)
  {
    for (auto t = 0; t < thresholds_.size(); ++t)
    {
      if (slot.buffers[t].data()[i] == -1)
        break;
      if (data.data()[i] < thresholds_spatial_[t].data()[i])
        break;
      if (std::isnan(data.data()[i]))
        slot.buffers[t].data()[i] = -1;
      else
        ++slot.buffers[t].data()[i];
    }
  }
  ++slot.member_count;

  // write output for a completed forecast
  if (slot.member_count == member_count_)
  {
    for (auto t = 0; t < thresholds_.size(); ++t)
      probabilities::write(t, forecast, slot.buffers[t], flow);

    // free up memory for this time now that we've finisehd with it
    slot.buffers.clear();
  }
}

auto probabilities_spatial::write_all(array2f const& data) -> void
{
  /* in theory the data array passed in could contain any values, meaning that if a value is higher than one of our
   * thresholds we would need to output a PoP of 100%.  then depending on the thresholds we might have a value that is
   * higher than some but not higher than others.  this would mean we have identical output for each forecast of a
   * given threshold, but potentially different output for each threshold.  in turn, this prevents us from using
   * the write_all functionality of the base product class (and therefore the output), which is very pessimistic.
   *
   * in reality, we know the data array will contain only zeros or nans because write_all is only used to provide null
   * forecast capability.  so rather than ruin our optimisation, we just throw an exception if a value is greater than
   * zero since this is currently impossible. */
  auto idata = array2<int8_t>{data.shape()};
  for (auto i = 0; i < data.size(); ++i)
  {
    if (data.data()[i] > 0.0)
      throw std::logic_error{"Unexpected value in probabilities_spatial::write_all"};
    // this preserves the missing values from the spatial threshold grid as well
    // there's also an assumption that the nans are in the same cells for every threshold
    idata.data()[i] = std::isnan(data.data()[i]) || std::isnan(thresholds_spatial_[0].data()[i]) ? -1 : 0;
  }
  probabilities::write_all(idata);
}

percentiles::percentiles(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata)
  : product{config, user_parameters, parent_metadata}
  , percentiles_{config_array_to_container<array1f>(config["percentiles"])}
{
  for (auto p : percentiles_)
    if (p < 0.0f || p > 100.0f)
      throw std::runtime_error{format("Invalid percentile {} requested.  Must be between 0 and 1", p)};
}

auto percentiles::initialize(product const& parent, time_point reference_time) -> void
{
  member_count_ = parent.series_labels()->size();
  slots_.clear();
  slots_ = vector<time_slot>(parent.forecast_times()->size());

  product::initialize(parent, reference_time);
}

/* Calculating percentiles is unavoidably expensive.  Implementations I've tried include:
 * 1. Append members to a buffer during forecast.  At the end of last member use a nth_element call to retrieve
 *    the smallest percentiles requested, then subsequent nth_element calls that each retrieve the next desired
 *    percentiles but always shifting the begin point so that the range we need to partially sort gets smaller with
 *    each percentiles.  Also means for a single percentiles we don't need to do a full sort.  When tested with four
 *    percentiles this method was the slowest!
 * 2. Append members to a buffer during forecast.  At the end of last member use a std::sort call to sort the
 *    entire array for each grid cell.  Then loop through again and extract the values for each desired percentiles
 *    directly from the sorted array.  Slightly slower than nth_element method for one percentiles, but faster for
 *    multiple percentiles.
 * 3. Use an insertion sort to append members to the buffer during forecast.  This ensures that the buffer is
 *    already fully sorted by the time we have finished appending the last member.  Then just loop through and
 *    extract the values for each desired percentiles directly from the sorted array.  Significantly faster than
 *    the above approaches.  This is due to the fact that the insertion_sort is happenining on each member
 *    and is therefore effectively parallelized over many cores.  Leaving the sorting operation until the last
 *    member has arrived serializes it because the same thread (running the last member) will do all of the
 *    sorting operations for all time steps in serial.  */
auto percentiles::write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void
{
  auto& slot = slots_[forecast];
  auto lock = std::lock_guard<std::mutex>{slot.mutex};

  if (slot.member_count == 0)
  {
    // we only allocate our (very large) buffer when it is first needed
    slot.buffer.resize(data.shape().y * data.shape().x, member_count_);
    for (auto i = 0; i < slot.buffer.shape().y; ++i)
      slot.buffer[i][0] = data.data()[i];
  }
  else
  {
    // copy the contents of this member into our buffer keeping the values for each member sorted as we go
    for (auto i = 0; i < slot.buffer.shape().y; ++i)
    {
      if (std::isnan(slot.buffer[i][0]))
        continue;

      if (std::isnan(data.data()[i]))
      {
        /* we use a nan in the first member as a flag to indicate no data at this grid cell and then
         * check it below during final output.  this flag method profiled faster than filling all member
         * values with a nan when first detected eliminiting the check in the lower loop. */
        slot.buffer[i][0] = std::numeric_limits<float>::quiet_NaN();
        continue;
      }

      // find the correct position in the sorted array and insert our member there
      auto pos = std::upper_bound(&slot.buffer[i][0], &slot.buffer[i][slot.member_count], data.data()[i]);
      std::move_backward(pos, &slot.buffer[i][slot.member_count], &slot.buffer[i][slot.member_count + 1]);
      *pos = data.data()[i];
    }
  }

  // increment the observed member count
  ++slot.member_count;

  // if this is the last forecast then generate the percentiles and free our buffer
  if (slot.member_count == member_count_)
  {
    // extract each desired percentile and output it
    auto out = array2f{data.shape()};
    for (auto p = 0; p < percentiles_.size(); ++p)
    {
      // determine the rank that this percentile relates to
      // see https://en.wikipedia.org/wiki/Percentile interpolation method variant C=1
      auto rank = percentiles_[p] * 0.01f * (member_count_ - 1);
      auto irank = static_cast<int>(std::floor(rank));
      auto interp = rank - irank;

      // sanity check
      if (rank < 0)
        rank = 0, interp = 0.0f;
      else if (rank >= member_count_ - 1)
        rank = member_count_ - 1, interp = 0.0f;

      // extract each value and output the product
      if (std::fabs(interp) < 0.01)
      {
        // optimisation if we are using an exact rank
        for (auto i = 0; i < slot.buffer.shape().y; ++i)
        {
          if (std::isnan(slot.buffer[i][0]))
            out.data()[i] = std::numeric_limits<float>::quiet_NaN();
          else
            out.data()[i] = slot.buffer[i][irank];
        }
      }
      else
      {
        for (auto i = 0; i < slot.buffer.shape().y; ++i)
        {
          if (std::isnan(slot.buffer[i][0]))
            out.data()[i] = std::numeric_limits<float>::quiet_NaN();
          else
            out.data()[i] = slot.buffer[i][irank] + interp * (slot.buffer[i][irank+1] - slot.buffer[i][irank]);
        }
      }

      // output the field
      product::write(p, forecast, out, flow);
    }

    // free up memory for this time now that we've finisehd with it
    slot.buffer.resize(0, 0);
  }
}
