/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#define DOCTEST_CONFIG_IMPLEMENT
#include "unit_test.h"
#include "cf.h"
#include "nc.h"
#include <iostream>
#include <fstream>

using namespace steps;

static std::filesystem::path reference_data_dir_;
static bool fast_mode_;

// LCOV_EXCL_START
auto steps::run_unit_tests(configuration const& config) -> int
{
#if !STEPS_UNIT_TEST
  std::cout << "sorry - unit tests were excluded from the build\n";
  return 1;
#endif

  // read configuration
  reference_data_dir_ = config.optional("reference_dir", "ref");
  fast_mode_ = config.optional("fast_mode", false);
  auto out_path = string{config.optional("out_path", "")};
  auto out_format = string{config.optional("format", "console")};

  // setup the test context
  doctest::Context ctx;
  ctx.setOption("duration", true);
  ctx.setOption("no-path-filenames", 1);
  ctx.setOption("out", out_path.c_str());
  if (!out_path.empty())
    ctx.setOption("no-colors", 1);
  if (out_format == "console")
    ctx.setOption("reporters", "console");
  else if (out_format == "junit")
    ctx.setOption("reporters", "junit");
  else
    throw std::runtime_error{"Unsupported unit test format requested"};

  // run the tests
  auto ret = ctx.run();

  // in fast mode always return a successful test result code
  return fast_mode_ ? 0 : ret;
}

auto steps::ut_reference_data_dir() -> std::filesystem::path const&
{
  return reference_data_dir_;
}

auto steps::ut_fast_mode() -> bool
{
  return fast_mode_;
}

auto steps::ut_load_reference_grid(int lag, bool mini) -> array2f
try
{
  auto path = format("{}/ut-lag{}-{}.nc", reference_data_dir_.native(), lag, mini ? "mini" : "full");
  auto file = nc::file{path, nc::io_mode::read_only};

  auto& var = file.lookup_variable("rain_rate");
  if (var.dimensions().size() != 2)
    throw std::runtime_error{format("Unexpected variable rank {}, should be 2", var.dimensions().size())};

  auto ret = array2f{var.dimensions()[0]->size(), var.dimensions()[1]->size()};
  cf::packer{}.read(var, ret, 0.0f);

  return ret;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to load ut reference grid lag {} mini {}", lag, mini)});
}
// LCOV_EXCL_STOP
