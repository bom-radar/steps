/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include "configuration.h"
#include "metadata.h"
#include "util.h"

#include <fstream>

namespace steps
{
  /// Supported URI schemes for file paths
  enum uri_scheme
  {
      none      ///< No scheme present, used for relative URIs or absolute file paths
    , file      ///< Standard file scheme
    , s3        ///< AWS S3 bucket scheme
  };

  /// Classify the scheme from a URI
  auto determine_uri_scheme(string_view uri) -> uri_scheme;

  /// Strip 'file://' from uri if it is present
  inline auto strip_file_schema(string_view uri_or_path) -> string_view
  {
    return starts_with(uri_or_path, "file://"sv) ? uri_or_path.substr(7) : uri_or_path;
  }

  /// Load a configuration file from a URL
  /** If optional is true and the configuration file does not exist, then an empty configuration item will be returned
   *  instead of throwing an exception. */
  auto load_configuration(string_view url, bool optional) -> configuration;

  /// Save configuration to a URL
  auto save_configuration(string_view url, configuration const& data) -> void;

  /// Load a generic binary blob from a URL
  /** This function returns an array1, which uses an 'int' for both size and indexing.  This implies that your data
   *  blob must be less than 2GB (for a 32-bit int architecture).  If larger sizes than this are required, the return
   *  type should be changed to a vector<char>, or pair<unique_ptr<char[]>, size_t> (to avoid vector's zero
   *  initialization.  Currently an exception will be thrown if the blob size exceeds the storage capacity of an
   *  array1<char>. */
  auto load_binary_blob(string_view uri, bool optional) -> array1<char>;

  /// Wrapper for an output stream to a generic URL
  class output_stream
  {
  public:
    /// Destructor
    /** If neither commit() nor abort() has been called, then perform cleanup as if calling abort().  This implies
     *  that a user _must_ call commit() to retain any output they have written to the stream. */
    virtual ~output_stream() = default;

    /// Write data to the stream
    virtual auto write(span<char const> data) -> void = 0;

    /// Flush any cached output and close the stream
    virtual auto commit() -> void = 0;

    /// Close the stream and remove any output already flushed
    /** Removal of already flushed output is on a best effort basis.  Some implementations may not be able to
     *  guarantee perfect cleanup during an abort(). */
    virtual auto abort() -> void = 0;
  };

  /// Local file implementation of output_sream
  class output_stream_local : public output_stream
  {
  public:
    output_stream_local(string path);
    ~output_stream_local();
    auto write(span<char const> data) -> void override;
    auto commit() -> void override;
    auto abort() -> void override;

  private:
    string        path_;
    std::ofstream stream_;
  };

  /// Save a generic binary stream to a URL
  /** This function returns a pointer to an object which may be used to write data out to a generic URL.  The actual
   *  storage mechanism is determined by the URL schema and may be a local file, cloud storage etc.  The object provide
   *  access to a std::ostream interface which should be used to output data. */
  auto save_binary_stream(string_view uri) -> unique_ptr<output_stream>;

  /// Input manager interface
  class input
  {
  public:
    /// Factory function pointer type
    using factory = auto (*)(configuration const&, metadata const&) -> unique_ptr<input>;

    /// Set the input factory function
    static auto set_factory(factory function) -> void;

    /// Instanciate an input manager using the currently installed factory function
    static auto instantiate(configuration const& config, metadata const& user_parameters) -> unique_ptr<input>;

  public:
    /// Virtual destructor
    virtual ~input() = default;

    /// Read a grid
    /** This function should read the spatial input for the given time of a stream.  If no input is available for the
     *  time given then it should return false.  If input is available but an error occurs then the function should
     *  throw an exception.  Data returned must be in units of mm/hr.  Invalid grid cells (for example areas in a mosaic
     *  with no coverage, should be set to NaN.
     *
     *  Note that gcc has a bug where it is not correctly generating warnings for [[nodiscard]] on a virtual function
     *  call so this attribute is currently of no effect.
     *  */
    [[nodiscard]] virtual auto read(
          time_point reference_time   ///< Input reference time
        , int member                  ///< Input member number
        , time_point time             ///< Time to read
        , array2f& data               ///< Output field
        ) -> bool = 0;

    /// Read multiple grids representing different members
    /** This function is called by the verification functionality to read all members related to a single forecast
     *  time at once.  It exists separately from the main read function only as a way to allow inputs to optimize their
     *  read pattern based on the knowledge that all members will be read in a tight loop.  Functionally it should work
     *  as if calling the main read function in a loop passind a different member index each iteration.  If the first
     *  member does not exist the function should return false.  If any subsequent member cannot be found an exception
     *  should be thrown. */
    [[nodiscard]] virtual auto read(
          time_point reference_time   ///< Input reference time
        , span<int> members           ///< Input member numbers
        , time_point time             ///< Time to read
        , span<array2f> data          ///< Output fields
        ) -> bool = 0;
  };

  class product;

  /// Product writer manager interface
  /** A common output interface is used regardless of the parent product type.
   *
   *  The reason for a unified output interface despite having different product inputs is to prevent fan-out of the
   *  classes that implement this interface.  For example, using this interface we can implement a single derived
   *  class to write NetCDF products which share a large amount of code.  We could then derive from this NetCDF output
   *  class to allow for either local or remote storage - 3 derived classes in total (base netcdf output, derived
   *  netcdf output for local files, derived netcdf output for remote files).  Had we started with an independent
   *  output interface per product type, this would have required creating at least 9 derived classes.
   *
   *  Due to the unified interface, we cannot directly know whether data passed for writing relates to a specific
   *  member, or forecast time etc.  For this reason, we provide the optional 'd0' and 'd1' indexes to the write()
   *  function which default to -1.  It is up to the implementer to correctly interpret the meaning of these values
   *  in the context of the specific product type being written.  Known examples include:
   *  - product_ensemble uses d0 = member and d1 = forecast
   *  - product_ensemble_mean uses d0 = forecast and d1 is unused
   *  - product_probabilities uses d0 = threshold and d1 = forecast
   *
   *  Products mmay also call any of the overloads of write() to provide output in native types other than float.
   *  It is up to the implementation to correctly respond to the write() calls based on product type.
   */
  class output
  {
  public:
    /// Output factory function pointer type
    using factory = auto (*)(configuration const&, metadata const&) -> unique_ptr<output>;

    /// Set the output factory function
    static auto set_factory(factory function) -> void;

    /// Instanciate an output using the currently installed factory function
    static auto instantiate(configuration const& config, metadata const& user_parameters) -> unique_ptr<output>;

  public:
    /// Virtual destructor
    virtual ~output() = default;

    /// Initialize output for a new model run
    virtual auto initialize(product const& parent, time_point reference_time) -> void = 0;

    /// Write output for the given d0, d1 index
    virtual auto write(array2f const& data, int d0 = -1, int d1 = -1) -> void = 0;

    /// Write output for the given d0, d1 index
    virtual auto write(array2<int8_t> const& data, int d0 = -1, int d1 = -1) -> void = 0;

    /// Write the same output grid to all indexes (used for null forecasts)
    virtual auto write_all(array2f const& data) -> void = 0;

    /// Write the same output grid to all indexes (used for null forecasts)
    virtual auto write_all(array2<int8_t> const& data) -> void = 0;

    /// Finalize the current output after a successful run
    virtual auto finalize() -> void = 0;

    /// Abort the current output after a failed run
    virtual auto abort() -> void = 0;
  };
}
