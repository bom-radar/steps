/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "transform.h"
#include "trace.h"
#include "unit_test.h"
#include <cmath>

using namespace steps;

auto transform::instantiate(configuration const& config) -> unique_ptr<transform>
{
  auto const& type = config["type"].string();
  if (type == "inverse-hyperbolic-sine")
    return std::make_unique<transform_ihs>(config);
  if (type == "log-truncate")
    return std::make_unique<transform_log_truncate>(config);
  if (type == "log-bias")
    return std::make_unique<transform_log_bias>(config);
  throw std::runtime_error{format("Invalid transform manager type {}", type)};
}

transform_log_bias::transform_log_bias(configuration const& config)
  : scale_{config["scale"]}
  , base_{config["base"]}
  , bias_{config["bias"]}
  , fwd_factor_{scale_ / std::log(base_)}
  , inv_factor_{std::log(base_) / scale_}
{ }

auto transform_log_bias::forward(float value) const -> float
{
  return fwd_factor_ * std::log(value + bias_);
}

auto transform_log_bias::inverse(float value) const -> float
{
  return std::exp(value * inv_factor_) - bias_;
}

auto transform_log_bias::forward(span<float> data) const -> void
{
  for (int i = 0; i < data.size(); ++i)
    data[i] = fwd_factor_ * std::log(data[i] + bias_);
}

auto transform_log_bias::inverse(span<float> data) const -> void
{
  for (int i = 0; i < data.size(); ++i)
    data[i] = std::exp(data[i] * inv_factor_) - bias_;
}

transform_log_truncate::transform_log_truncate(configuration const& config)
  : threshold_{config["threshold"]}
  , log_threshold_{std::log(threshold_)}
{ }

auto transform_log_truncate::forward(float value) const -> float
{
  return value > threshold_ ? std::log(value) - log_threshold_ : 0.0f;
}

auto transform_log_truncate::inverse(float value) const -> float
{
  return value > 0.0f ? std::exp(value + log_threshold_) : 0.0f;
}

auto transform_log_truncate::forward(span<float> data) const -> void
{
  for (int i = 0; i < data.size(); ++i)
    data[i] = data[i] > threshold_ ? std::log(data[i]) - log_threshold_ : 0.0f;
}

auto transform_log_truncate::inverse(span<float> data) const -> void
{
  for (int i = 0; i < data.size(); ++i)
    data[i] = data[i] > 0.0f ? std::exp(data[i] + log_threshold_) : 0.0f;
}

transform_ihs::transform_ihs(configuration const& config)
  : prescale_{config["prescale"]}
{ }

auto transform_ihs::forward(float value) const -> float
{
  return asinh(value * prescale_);
}

auto transform_ihs::inverse(float value) const -> float
{
  return value > 0.0f ? sinh(value) / prescale_ : 0.0f;
}

auto transform_ihs::forward(span<float> data) const -> void
{
  for (int i = 0; i < data.size(); ++i)
    data[i] = asinh(data[i] * prescale_);
}

auto transform_ihs::inverse(span<float> data) const -> void
{
  for (int i = 0; i < data.size(); ++i)
    data[i] = data[i] > 0.0f ? sinh(data[i]) / prescale_ : 0.0f;
}

// LCOV_EXCL_START
TEST_CASE("transform")
{
  auto xform = unique_ptr<transform>{};
  auto ref = vector<float>(4);
  ref[0] = 0.0;
  ref[1] = 0.1;
  ref[2] = 1.0;
  ref[3] = 10.0;
  auto test = vector<float>(4);
  for (size_t i = 0; i < ref.size(); ++i)
    test[i] = ref[i];

  SUBCASE("global")
  {
    CHECK_THROWS(transform::instantiate("type bad"_config));
  }

  SUBCASE("log-bias")
  {
    xform = transform::instantiate("type log-bias scale 10 base 10 bias 0.1"_config);
    CHECK(dynamic_cast<transform_log_bias*>(xform.get()));

    xform->forward(test);
    for (size_t i = 0; i < ref.size(); ++i)
      CHECK(test[i] == approx(10.0 * std::log10(ref[i] + 0.1)));

    for (size_t i = 0; i < ref.size(); ++i)
    {
      CHECK(xform->forward(ref[i]) == approx(test[i]));
      CHECK(xform->inverse(test[i]) == approx(ref[i]));
    }

    xform->inverse(test);
    for (size_t i = 0; i < ref.size(); ++i)
      CHECK(test[i] == approx(ref[i]));
  }

  SUBCASE("log-truncate")
  {
    xform = transform::instantiate("type log-truncate threshold 0.1"_config);
    CHECK(dynamic_cast<transform_log_truncate*>(xform.get()));

    xform->forward(test);
    CHECK(test[0] == approx(0.0));
    CHECK(test[1] == approx(0.0));
    CHECK(test[2] == approx(std::log(1.0) - std::log(0.1)));
    CHECK(test[3] == approx(std::log(10.0) - std::log(0.1)));

    for (size_t i = 0; i < ref.size(); ++i)
      CHECK(xform->forward(ref[i]) == approx(test[i]));
    CHECK(xform->inverse(test[0]) == approx(0.0));
    CHECK(xform->inverse(test[1]) == approx(0.0));
    CHECK(xform->inverse(test[2]) == approx(1.0));
    CHECK(xform->inverse(test[3]) == approx(10.0));

    // note - log-truncate does not have a perfect inverse.  we expect all values <= threshold to map back to 0.0
    xform->inverse(test);
    CHECK(test[0] == approx(0.0));
    CHECK(test[1] == approx(0.0));
    CHECK(test[2] == approx(1.0));
    CHECK(test[3] == approx(10.0));
  }

  SUBCASE("inverse-hyperbolic-sine")
  {
    xform = transform::instantiate("type inverse-hyperbolic-sine prescale 1.0"_config);
    CHECK(dynamic_cast<transform_ihs*>(xform.get()));

    xform->forward(test);
    for (size_t i = 0; i < ref.size(); ++i)
      CHECK(test[i] == approx(std::log(ref[i] + std::sqrt(ref[i] * ref[i] + 1.0))));

    for (size_t i = 0; i < ref.size(); ++i)
    {
      CHECK(xform->forward(ref[i]) == approx(test[i]));
      CHECK(xform->inverse(test[i]) == approx(ref[i]));
    }

    xform->inverse(test);
    for (size_t i = 0; i < ref.size(); ++i)
      CHECK(test[i] == approx(ref[i]));
  }
}
// LCOV_EXCL_STOP
