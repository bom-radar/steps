/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "configuration.h"
#include "util.h"

namespace steps
{
  /// Dataspace transform manager interface
  /** Implementations of this class are used to transform input data from mm/hr into log space.  Since the log of 0 is
   *  undefined, the tranformation is non-trivial.  The differences between implementations of the transform center
   *  around how to deal with the nil, and very low rain rates. */
  class transform
  {
  public:
    static auto instantiate(configuration const& config) -> unique_ptr<transform>;

  public:
    /// Virtual destructor
    virtual ~transform() = default;

    /// Transform a value from linear mm/hr into log space
    virtual auto forward(float value) const -> float = 0;
    /// Transform a value from log space into linear mm/hr
    virtual auto inverse(float value) const -> float = 0;

    /// Transform data from linear mm/hr into log space
    virtual auto forward(span<float> data) const -> void = 0;
    /// Transform data from log space into linear mm/hr
    virtual auto inverse(span<float> data) const -> void = 0;
  };

  /// Transform where data is biased away from zero prior to log transform
  class transform_log_bias : public transform
  {
  public:
    transform_log_bias(configuration const& config);
    auto forward(float value) const -> float override;
    auto inverse(float value) const -> float override;
    auto forward(span<float> data) const -> void override;
    auto inverse(span<float> data) const -> void override;

  private:
    float scale_;         // scale applied to logarithm result
    float base_;          // base of logarithm
    float bias_;          // offset applied to input values (used to make input of 0.0 well defined)

    float fwd_factor_;    // scale / log(base)
    float inv_factor_;    // log(base) / scale
  };

  /// Transform where low rain rates are truncated prior to log transform
  class transform_log_truncate : public transform
  {
  public:
    transform_log_truncate(configuration const& config);
    auto forward(float value) const -> float override;
    auto inverse(float value) const -> float override;
    auto forward(span<float> data) const -> void override;
    auto inverse(span<float> data) const -> void override;

  private:
    float threshold_;     // rain rate at which to truncate (all rates below this are set to 0)
    float log_threshold_; // log of truncation rate
  };

  /// Transform where Inverse Hyperbolic Sine function is used to approximate the log transform
  /** A prescale value of 0.5 closely approximates ln(x) */
  class transform_ihs : public transform
  {
  public:
    transform_ihs(configuration const& config);
    auto forward(float value) const -> float override;
    auto inverse(float value) const -> float override;
    auto forward(span<float> data) const -> void override;
    auto inverse(span<float> data) const -> void override;

  private:
    float prescale_;      // scale mm/hr rates by this value prior to conversion
  };
}
