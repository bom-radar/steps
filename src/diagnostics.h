/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include "configuration.h"
#include "io_netcdf.h"

/// Engine diagnostics output functions
/** This functionality is NOT designed to be thread safe.  You must ONLY ever initialize it if the 'engines' count in the
 *  model core is 1. */
namespace steps::diagnostics
{
  /// Variant of spans for the various data types which can be output
  using data_span = variant<span<int const>, span<long const>, span<float const>, span<double const>, span<string const>>;

  /// Initialize the diagnostics output subsystem
  auto initialize(string_view path) -> void;

  /// Shutdown the diagnostics output subsystem
  auto shutdown() -> void;

  /// Return true if diagnostics output is enabled for this run
  auto enabled() -> bool;

  /// Define a new dimension
  auto create_dimension(char const* name, int size) -> void;

  /// Define a variable in the diagnostic file
  auto create_variable(
        char const* name
      , nc::data_type type
      , std::initializer_list<char const*> dims = {}
      , char const* long_name = nullptr
      , char const* units = nullptr
      ) -> void;

  /// Special coordinate flags
  constexpr auto all = -1;

  /// Output data to a variable in the diagnostic file
  auto write(char const* name, data_span data, span<int const> coords = {}) -> void;
  inline auto write(char const* name, data_span data, std::initializer_list<int> coords) -> void
  {
    diagnostics::write(name, data, {coords.begin(), coords.end()});
  }
  inline auto write(char const* name, span<vec2f const> data, span<int const> coords = {}) -> void
  {
    diagnostics::write(name, span<float const>{&data[0].x, data.size() * 2}, coords);
  }
  inline auto write(char const* name, span<vec2f const> data, std::initializer_list<int> coords) -> void
  {
    diagnostics::write(name, span<float const>{&data[0].x, data.size() * 2}, coords);
  }

  /// Flush current diagnostics output state to disk
  auto flush() -> void;
}
