/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "stream_state.h"

namespace steps
{
  struct cache_key
  {
    time_point  time;
    int         nwp_member;
    int         bf_nwp_member;
  };
  inline auto operator==(cache_key const& lhs, cache_key const& rhs)
  {
    return lhs.time == rhs.time && lhs.nwp_member == rhs.nwp_member && lhs.bf_nwp_member == rhs.bf_nwp_member;
  }
  inline auto operator<(cache_key const& lhs, cache_key const& rhs)
  {
    return
        lhs.time < rhs.time ? true
      : lhs.time > rhs.time ? false
      : lhs.nwp_member < rhs.nwp_member ? true
      : lhs.nwp_member > rhs.nwp_member ? false
      : lhs.bf_nwp_member < rhs.bf_nwp_member;
  }
  inline auto operator<(cache_key const& lhs, time_point rhs)
  {
    return lhs.time < rhs;
  }
  inline auto operator<(time_point lhs, cache_key const& rhs)
  {
    return lhs < rhs.time;
  }

  /// Interface used for storing and retrieving stream state objects from persistent storage
  class persistent_cache
  {
  public:
    /// Factory function pointer type
    using factory = auto (*)(string_view, time_point, time_point, string_view, int, int) -> unique_ptr<persistent_cache>;

    /// Set the factory function
    static auto set_factory(factory function) -> void;

    /// Instanciate an archive using the currently installed factory function
    static auto instantiate(
          string_view stream_name
        , time_point reference_time
        , time_point bf_reference_time
        , string_view path
        , int compression
        , int precision_reduction
        ) -> unique_ptr<persistent_cache>;

  public:
    /// Virtual desctructor
    virtual ~persistent_cache() = default;

    /// Write a stream state entry
    virtual auto write(cache_key const& key, stream_state const& state) -> void = 0;

    /// Read a stream state entry
    virtual auto read(cache_key const& key, stream_state& state) -> bool = 0;
  };

  /// Null stream state archive - all reads fail, all writes are ignored
  class persistent_cache_none : public persistent_cache
  {
  public:
    persistent_cache_none(
          string_view stream_name
        , time_point reference_time
        , time_point bf_reference_time
        , string_view path
        , int compression
        , int precision_reduction);

    auto write(cache_key const& key, stream_state const& state) -> void override;
    auto read(cache_key const& key, stream_state& state) -> bool override;
  };

  /// Default stream state archive which stores stream states as files on disk locally
  class persistent_cache_local : public persistent_cache
  {
  public:
    persistent_cache_local(
          string_view stream_name
        , time_point reference_time
        , time_point bf_reference_time
        , string_view path
        , int compression
        , int precision_reduction);

    auto write(cache_key const& key, stream_state const& state) -> void override;
    auto read(cache_key const& key, stream_state& state) -> bool override;

  private:
    string  base_dir_;
    int     compression_;
    int     precision_reduction_;
  };
}
