/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "ext/span/span.h"

#include <chrono>
#include <cmath>
#include <exception>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <stdexcept>
#include <variant>
#include <vector>

namespace steps
{
  using std::unique_ptr;
  using std::shared_ptr;
  using std::weak_ptr;

  using std::optional;
  using std::nullopt;

  // the overload function allows us to overload lambdas when using std::visit
  using std::variant;
  using std::visit;
  template<typename... T> struct overload : T... { using T::operator()...; };
  template<typename... T> overload(T...) -> overload<T...>;

  using std::list;
  using std::map;
  using std::vector;
  using std::pair;

  // replace this with std::span once we have C++20
  using tcb::span;

  // adopt std::string as our standard string type
  using std::string;
  using std::string_view;

  // allow use of ""s UDL
  using namespace std::string_literals;

  // allow use of ""sv UDL
  using namespace std::string_view_literals;

  // allow use of ""h/min/s/ms/us/ns
  using namespace std::chrono_literals;

  // adopt std::chrono::system_clock as the basis for our time management
  using clock = std::chrono::system_clock;
  using duration = clock::duration;
  using time_point = clock::time_point;

  /// Allocate aligned memory
  /** This function is guaranteed to produce memory which is nicely aligned to support SIMD instruction sets and other
   *  optimizations.  It is currently implemented using the aligned memory allocator from the FFTW library.  Unlike
   *  regular malloc() this function will throw an exception if the requested memory cannot be allocated. */
  auto aligned_malloc(size_t size) -> void*;

  /// Free aligned memory
  auto aligned_free(void* ptr) -> void;

  /// Deleter type for use with smart pointers and containers that simply calls aligned_free
  template <typename T>
  struct aligned_deleter
  {
    auto operator()(T* ptr) -> void
    {
      // since we don't call the destructor we _must_ ensure we are only used with trivial types
      static_assert(std::is_trivially_destructible_v<T>, "aligned deleter must be used with trivial types");

      aligned_free(ptr);
    }
  };

  /// Unique resource handle (ie: unique_ptr for non-pointer types)
  /** \note We use std::tuple to store the pointer and deleter here because std::tuple is free to employ empty base
   *        optimization so that an empty deleter class consumes no space in our unique_handle object.  The std::tuple
   *        class is not required to perform this optimization, but has been observed to under GCC. */
  template <typename H, typename D, H nullval = H{}>
  class unique_handle
  {
  public:
    /// Construct a null handle
    constexpr unique_handle() noexcept
      : data_(nullval, D{})
    { }

    /// Construct a handle specifying the underlying handle to manage
    explicit unique_handle(H handle, D deleter = {}) noexcept
      : data_(std::move(handle), std::move(deleter))
    { }

    /// Move constructor
    unique_handle(unique_handle&& rhs) noexcept
      : data_(rhs.data_)
    {
      std::get<0>(rhs.data_) = nullval;
    }

    /// Move assignment
    auto operator=(unique_handle&& rhs) noexcept -> unique_handle&
    {
      using std::swap;
      swap(data_, rhs.data_);
      return *this;
    }

    /// Destroy a resource
    ~unique_handle()
    {
      std::get<1>(data_)(std::get<0>(data_));
    }

    /// Return true if the handle is not the null handle
    operator bool() const noexcept
    {
      return std::get<0>(data_) != nullval;
    }

    /// Return the native handle
    operator H() const noexcept
    {
      return std::get<0>(data_);
    }

    /// Member access (valid if handle is a pointer type)
    auto operator->() const noexcept -> H
    {
      return std::get<0>(data_);
    }

    /// Manually release the resource
    auto release() noexcept -> H
    {
      auto ret = std::get<0>(data_);
      std::get<0>(data_) = nullval;
      return ret;
    }

    /// Reset the resource with a new value
    auto reset(H handle = nullval) noexcept -> void
    {
      std::get<1>(data_)(std::get<0>(data_));
      std::get<0>(data_) = handle;
    }

    /// Explicitly get the handle
    auto get() const noexcept -> H
    {
      return std::get<0>(data_);
    }

    /// Stream output of handle
    /** This function is needed to resolve ambiguity when the unit test framework attempts to stringify a handle. */
    friend auto& operator<<(std::ostream& os, unique_handle const& rhs)
    {
      return os << rhs.get();
    }

  public:
    /// The null handle value for this handle type
    static constexpr H nullhnd = nullval;

  private:
    std::tuple<H, D> data_;
  };

  /// Adapter that converts a pointer to a function into a type which can be invoked with operator()
  /** This adapter is particularly useful for defining predicates and deleters as used by various templates
   *  within the standard library.  For example, one could define a functor type which simply calls the standard
   *  C free() function and then use this type as the deleter in a unique_ptr.  Since the function pointer itself
   *  is part of the type of the deleter, the deleter will occupy no space or runtime overhead in the resultant
   *  unique_ptr type.
   */
  template <auto function>
  struct functor
  {
    template <typename... Args>
    auto operator()(Args&&... args)
    {
      return function(std::forward<Args>(args)...);
    }
  };

  /// Set command line that was used to launch application
  auto set_command_line_string(int argc, char const* const* argv) -> void;

  /// Get command line used to launch application as a single string (if known)
  auto get_command_line_string() -> string;

  /// Check if a string starts with a given prefix
  inline auto starts_with(string_view subject, string_view prefix) -> bool
  {
    return subject.rfind(prefix, 0) == 0;
  }

  /// Replace all occurrences of a substring in a string
  auto replace_all(string_view subject, string_view search, string_view replace) -> string;

  /// Linear interpolation
  inline constexpr auto lerp(float lhs, float rhs, float fraction)
  {
    return lhs + fraction * (rhs - lhs);
  }
  inline constexpr auto lerp(double lhs, double rhs, double fraction)
  {
    return lhs + fraction * (rhs - lhs);
  }

  /// Constant for pi
  template <typename T>
  constexpr double pi = M_PI;
  template <>
  inline constexpr float pi<float> = M_PI;

  /// Constant for conversion of degrees to radians
  template <typename T>
  constexpr T deg_to_rad = pi<T> / T(180.0);

  /// Constant for conversion of radians to degrees
  template <typename T>
  constexpr T rad_to_deg = T(180.0) / pi<T>;

  /// Degrees to radians
  template <typename T>
  auto degrees_to_radians(T val) { return val * deg_to_rad<T>; }

  /// Radians to degrees
  template <typename T>
  auto radians_to_degrees(T val) { return val * rad_to_deg<T>; }

  /// Convert a blending half-life and time step into a per time-step contribution
  /** The value calculated by this function represents the fraction of some quantity that should be "blended in"
   *  (using a linear interpolation) to some value each time step, to ensure that the blended in value accounts for
   *  50% of the total value after the specified duration.
   *
   *  For example, suppose we have some value X which we want to gradulally transition towards the value Y.  One way
   *  to do this is to 'bump' the X value at each time step some fixed fraction of the way towards Y.  This is done
   *  using linearly interpolation such that X(t) = (1 - frac) * X_t-1 + frac * Y.  If frac is 0.1 then the value of
   *  X(t) will move 10% closer to Y at each time step, but will never actually reach it.  This function calculates
   *  the value of 'frac' that should be used to ensure X will be 'half way' to Y after some desired duration (for
   *  a given time step).
   *
   *  Since the blend amount is included each time step it represents a geometric series.  If 'frac' is the blend
   *  amount per time step, then the total contribution after 'n' time steps is "t_n = frac + (1 - frac) * t_n-1".
   *  The analytical solution to this series is "t_n = 1 - (1 - frac)^n".  solving for 'frac' we get
   *  "frac = 1 - (1 - t_n)^(1/n)".
   *
   *  We then allow the user to configure the parameter based on the desired 'halflife' of the blend.  That is, we
   *  fix t_n at 0.5 (total blend so far), and the user sets n (number of time steps).  So we now have
   *  'frac = 1 - 0.5^(1/n)'.
   *
   *  Setting a blend_halflife of std::duration::zero will cause the blend factor to be 1.0 (i.e. simply adopt the new
   *  'Y' value immediately.  Setting a blend_halflife of std::duration::max will cause the blend factor to be 0.0
   *  (i.e. disable blending and keep the original X value).
   */
  auto calculate_blend_factor_for_halflife(duration time_step, duration blend_halflife) -> float;

  /// Type of progress callback function
  /** First integer parameter is the member number, and the second is the forecast index.  Note that forecast 0
   *  is the model reference time, and the first true forecast is forecast 1.  This callback function may be
   *  invoked from any engine thread and by multiple threads at once.  The user must therefore ensure that the
   *  supplied callback function is thread safe and reentrant. */
  using progress_callback_fn = std::function<void(int member, int forecast)>;
}
