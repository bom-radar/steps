/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "util.h"

namespace steps
{
  /// 1D array
  /** The contents of an array1 are allocated using aligned_malloc() to ensure that operations on the array contents
   *  have the best chance of being vectorizable by the optimizer. */
  template <typename T>
  class array1
  {
  public:
    static_assert(std::is_trivially_copyable_v<T>, "array1<T> must be used with trivial types");

    using value_type = T;
    using size_type = int;

  public:
    /// Default constructor
    array1() : size_{0} { }

    /// Sized constructor
    explicit array1(int size)
      : size_{size}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    { }

    /// Copy constructor
    array1(array1 const& rhs)
      : size_{rhs.size_}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = rhs.data_[i];
    }

    /// Move constructor
    array1(array1&& rhs) noexcept = default;

    /// Copy assignment
    auto operator=(array1 const& rhs) -> array1&
    {
      if (size_ != rhs.size_)
      {
        data_ = storage_ptr{static_cast<T*>(aligned_malloc(sizeof(T) * rhs.size_))};
        size_ = rhs.size_;
      }
      copy(rhs);
      return *this;
    }

    /// Move assignment
    auto operator=(array1&& rhs) noexcept -> array1& = default;

    /// Resize the array
    /** Note that original contents of the array is NOT preserved!  If you need to preserve the array contents then
     *  either use a vector, or make a copy and move it on top. */
    auto resize(int size)
    {
      if (size_ == size)
        return;
      data_.reset(static_cast<T*>(aligned_malloc(sizeof(T) * size)));
      size_ = size;
    }

    /// Get the number of elements in the array
    auto size() const { return size_; }

    /// Get size of data in bytes (useful for I/O)
    auto size_bytes() const { return size_ * int(sizeof(value_type)); }

    /// Get the raw data pointer
    auto data() { return data_.get(); }

    /// Get the raw data pointer
    auto data() const { return (T const*) data_.get(); }

    /// Get an element of the array
    auto& operator[](int i) { return data_[i]; }

    /// Get an element of the array
    auto& operator[](int i) const { return (T const&) data_[i]; }

    /// Get the first element of the array
    auto& front() { return data_[0]; }

    /// Get the first element of the array
    auto& front() const { return data_[0]; }

    /// Get the last element of the array
    auto& back() { return data_[size_ - 1]; }

    /// Get the back element of the array
    auto& back() const { return data_[size_ - 1]; }

    /// Get begin iterator
    auto begin() { return data_.get(); }

    /// Get begin iterator
    auto begin() const { return (T const*) data_.get(); }

    /// Get begin iterator
    auto cbegin() const { return (T const*) data_.get(); }

    /// Get end iterator
    auto end() { return data_.get() + size_; }

    /// Get end iterator
    auto end() const { return (T const*) data_.get() + size_; }

    /// Get end iterator
    auto cend() const { return (T const*) data_.get() + size_; }

    /// Fill every element of the array
    auto fill(T val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = val;
    }

    /// Copy the value of another array into this one
    auto copy(array1 const& val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = val.data_[i];
    }

    /// Replace a value in the array
    auto replace(T old_val, T new_val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] == old_val ? new_val : data_[i];
    }

    /// Multiply and add to every element of the array
    auto multiply_add(T scale, T offset)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] * scale + offset;
    }

    /// Replace matching values in the array, or multiply and add to non-matching values
    auto replace_or_multiply_add(T old_val, T new_val, T scale, T offset)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] == old_val ? new_val : data_[i] * scale + offset;
    }

  private:
    using storage_ptr = std::unique_ptr<T[], aligned_deleter<T>>;

  private:
    int         size_;
    storage_ptr data_;
  };

  using array1i = array1<int>;
  using array1l = array1<long>;
  using array1f = array1<float>;
  using array1d = array1<double>;
}
