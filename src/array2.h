/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "vec2.h"
#include "util.h"

namespace steps
{
  /// 2D array
  /** The contents of an array2 are allocated using aligned_malloc() to ensure that operations on the array contents
   *  have the best chance of being vectorizable by the optimizer.  This also ensures that array2d and array2<vec2d>
   *  are able to be used directly with the fourier transform library. */
  template <typename T>
  class array2
  {
  public:
    static_assert(std::is_trivially_copyable_v<T>, "array2<T> must be used with trivial types");

    using value_type = T;

  public:
    /// Default constructor
    array2() : shape_{0, 0}, size_{0} { }

    /// Sized constructor
    explicit array2(vec2i shape)
      : shape_{shape}
      , size_{shape.x * shape.y}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    { }

    /// Sized constructor
    array2(int rows, int cols)
      : array2{vec2i{cols, rows}}
    { }

    /// Copy constructor
    array2(array2 const& rhs)
      : shape_{rhs.shape_}
      , size_{rhs.size_}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = rhs.data_[i];
    }

    /// Move constructor
    array2(array2&& rhs) noexcept = default;

    /// Copy assignment
    auto operator=(array2 const& rhs) -> array2&
    {
      if (size_ != rhs.size_)
      {
        data_ = storage_ptr{static_cast<T*>(aligned_malloc(sizeof(T) * rhs.size_))};
        shape_ = rhs.shape_;
        size_ = rhs.size_;
      }
      copy(rhs);
      return *this;
    }

    /// Move assignment
    auto operator=(array2&& rhs) noexcept -> array2& = default;

    /// Resize the array
    /** Note that original contents of the array is NOT preserved!  If you need to preserve the array contents then
     *  either use a vector, or make a copy and move it on top. */
    auto resize(vec2i shape)
    {
      if (shape_ == shape)
        return;
      auto size = shape.x * shape.y;
      if (size_ != size)
        data_.reset(static_cast<T*>(aligned_malloc(sizeof(T) * size)));
      shape_ = shape;
      size_ = size;
    }

    auto resize(int rows, int cols)
    {
      resize(vec2i{cols, rows});
    }

    /// Get the number of elements in the array
    auto size() const { return size_; }

    /// Get size of data in bytes (useful for I/O)
    auto size_bytes() const { return size_ * int(sizeof(value_type)); }

    /// Get the number of rows and columns
    auto shape() const { return shape_; }

    /// Get the raw data pointer
    auto data() { return data_.get(); }

    /// Get the raw data pointer
    auto data() const { return (T const*) data_.get(); }

    /// Get a pointer to the start of a row
    auto operator[](int y) { return &data_[y * shape_.x]; }

    /// Get a pointer to the start of a row
    auto operator[](int y) const { return (T const*) &data_[y * shape_.x]; }

    /// Get begin iterator
    auto begin() { return data_.get(); }

    /// Get begin iterator
    auto begin() const { return (T const*) data_.get(); }

    /// Get begin iterator
    auto cbegin() const { return (T const*) data_.get(); }

    /// Get end iterator
    auto end() { return data_.get() + size_; }

    /// Get end iterator
    auto end() const { return (T const*) data_.get() + size_; }

    /// Get end iterator
    auto cend() const { return (T const*) data_.get() + size_; }

    /// Determine mean of the array
    auto mean() const
    {
      auto sum = 0.0;
      for (auto i = 0; i < size_; ++i)
        sum += data_[i];
      return sum / size_;
    }

    /// Determine mean and standard deviation of the array
    auto mean_stddev() const
    {
      auto mean = array2::mean();
      auto sum = 0.0;
      for (auto i = 0; i < size_; ++i)
        sum += (data_[i] - mean) * (data_[i] - mean);
      return std::make_pair(mean, std::sqrt(sum / size_));
    }

    /// Determine conditional mean of the array
    /** The value provided to the condition functor is the index of the point to condition.  The condition should return true if
     *  the point is to be included in calculations. */
    template <typename Condition>
    auto conditional_mean(Condition&& condition) const
    {
      auto sum = 0.0;
      auto count = 0;
      for (auto i = 0; i < size_; ++i)
      {
        if (condition(i))
        {
          sum += data_[i];
          ++count;
        }
      }
      return sum / count;
    }

    /// Determine conditional mean and standard deviation of the array
    template <typename Condition>
    auto conditional_mean_stddev(Condition&& condition) const
    {
      auto mean = array2::conditional_mean(condition);
      auto sum = 0.0;
      auto count = 0;
      for (auto i = 0; i < size_; ++i)
      {
        if (condition(i))
        {
          sum += (data_[i] - mean) * (data_[i] - mean);
          ++count;
        }
      }
      return std::make_pair(mean, std::sqrt(sum / count));
    }

    /// Determine the number of cells in the array that are greater or equal to a value
    auto count_greater_equal(T value)
    {
      auto count = 0;
      for (auto i = 0; i < size_; ++i)
        if (data_[i] >= value)
          ++count;
      return count;
    }

    /// Normalize data using a known mean and standard deviation
    auto normalize(double mean, double stddev)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = (data_[i] - mean) / stddev;
    }

    /// Reverse the effects of normalize to restore the passed mean and standard deviation
    auto unnormalize(double mean, double stddev)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = (data_[i] * stddev) + mean;
    }

    /// Reverse the effects of normalize to restore the passed mean and standard deviation
    auto unnormalize(double mean, double stddev, array2 const& input)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = (input.data()[i] * stddev) + mean;
    }

    /// Fill every element of the array
    auto fill(T val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = val;
    }

    /// Copy the value of another array into this one
    auto copy(array2 const& val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = val.data_[i];
    }

    /// Add a scalar to every value in the array
    auto add(T val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] += val;
    }

    /// Add the values of another array to this one
    auto add(array2 const& val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] += val.data_[i];
    }

    /// Set the values to another array scaled by a constant
    template <typename U>
    auto copy_scaled(array2 const& val, U scale)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = val.data_[i] * scale;
    }

    /// Add the values of another array scaled by a constant to this one
    template <typename U>
    auto add_scaled(array2 const& val, U scale)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] += val.data_[i] * scale;
    }

    /// Subtract the values of another array from this one
    auto subtract(array2 const& val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] -= val.data_[i];
    }

    /// Multiply the value of this array by another one
    auto multiply(array2 const& val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] *= val.data_[i];
    }

    /// Multiply the value of this array by a scalar
    template <typename U>
    auto multiply(U val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] *= val;
    }

    /// Divide the values of this array by a scalar
    auto divide(T val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] /= val;
    }

    /// Divide the values of this array by another one
    auto divide(array2 const& val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] /= val.data_[i];
    }

    /// Replace a value in the array
    auto replace(T old_val, T new_val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] == old_val ? new_val : data_[i];
    }

    /// Replace nans in the array
    auto replace_nan(T new_val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = std::isnan(data_[i]) ? new_val : data_[i];
    }

    /// Replace values less than a threshold
    auto threshold_min(T threshold, T new_val)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] < threshold ? new_val : data_[i];
    }

    /// Add and multiply to every element of the array
    auto add_multiply(T offset, T scale)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = (data_[i] + offset) * scale;
    }

    /// Multiply and add to every element of the array
    auto multiply_add(T scale, T offset)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] * scale + offset;
    }

    /// Replace matching values in the array, or multiply and add to non-matching values
    auto replace_or_multiply_add(T old_val, T new_val, T scale, T offset)
    {
      for (auto i = 0; i < size_; ++i)
        data_[i] = data_[i] == old_val ? new_val : data_[i] * scale + offset;
    }

    // ADVANCED FUNCTIONS

    /// Hack the size and shape of the array without causing memory reallocation
    /** This is used by optical_flow_ancilla to avoid constant reallocation of different sized buffers */
    auto hack_size(vec2i shape)
    {
      shape_ = shape;
      size_ = shape.x * shape.y;
    }

  private:
    using storage_ptr = std::unique_ptr<T[], aligned_deleter<T>>;

  private:
    vec2i       shape_;
    int         size_;
    storage_ptr data_;
  };

  using array2i = array2<int>;
  using array2l = array2<long>;
  using array2f = array2<float>;
  using array2d = array2<double>;
  using array2f2 = array2<vec2<float>>;
}
