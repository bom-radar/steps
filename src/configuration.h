/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "format.h"
#include "traits.h"
#include <filesystem>
#include <istream>
#include <stdexcept>
#include <string>
#include <vector>

namespace steps
{
  /// Configuration file parser
  class configuration
  {
  public:
    /// Configuration file node types
    enum class node_type
    {
        null
      , string
      , array
      , object
    };
    using string_type = std::string;
    using array_type = std::vector<configuration>;
    using object_type = std::vector<std::pair<string_type, configuration>>;

    /// Error thrown when file contains a syntax error
    class parse_error : public std::exception
    {
    public:
      parse_error(char const* description, std::istream& in);

      virtual auto what() const noexcept -> char const*;

      auto line() const     { return line_; }
      auto column() const   { return column_; }

    private:
      std::string what_;
      int     line_;
      int     column_;
    };

    /// Error thrown when user requests a type that conflicts with type in file
    class type_mismatch : public std::runtime_error
    {
    public:
      type_mismatch(node_type expected, node_type found);

      auto expected() const { return expected_; }
      auto found() const    { return found_; }

    private:
      node_type expected_;
      node_type found_;
    };

    /// Tag type used to indicate streaming should include top level tags
    struct top_level_tag {};

    /// Constant used in stream constructor and write function to request top level tags
    constexpr static top_level_tag top_level = top_level_tag{};

    /// Null configuration typically used as second argument to optional()
    static configuration const null;

  public:
    /// Read a configuration file from disk
    /** If the optional parameter determines the behaviour if no file exists at the supplied path.  If true
     *  then an empty configuration object is returned, if false an exception is thrown.  On all other file
     *  errors (permissions etc) an exception is thrown. */
    static auto read_file(std::filesystem::path const& path, bool optional) -> configuration;

    /// Write a configuration file to disk
    static auto write_file(std::filesystem::path const& path, configuration const& config) -> void;

    /// Read a configuration file from string
    static auto read_string(std::string const& str) -> configuration;

  public:
    /// Initialize a configuration object as a null
    configuration()
      : type_{node_type::null}
    { }

    /// Initialize a configuration object as a string from a scalar type
    template <typename T, typename = std::enable_if_t<allow_simple_serialization<T>::value>>
    explicit configuration(T val)
      : type_{node_type::string}
    {
      // TEMP - this should be fmt::to_string(val), but that fails for struct tm and our custom types.  see format.cc unit tests
      new (&string_) string_type{format("{}", val)};
    }

    /// Initialize a configuration object as a string
    explicit configuration(char const* val)
      : type_{node_type::string}
    {
      new (&string_) string_type{val};
    }

    /// Initialize a configuration object as a string
    explicit configuration(string_type val)
      : type_{node_type::string}
    {
      new (&string_) string_type{std::move(val)};
    }

    /// Initialize a configuration object as an array
    explicit configuration(array_type val)
      : type_{node_type::array}
    {
      new (&array_) array_type{std::move(val)};
    }

    /// Initialize a configuration object as an object
    explicit configuration(object_type val)
      : type_{node_type::object}
    {
      new (&object_) object_type{std::move(val)};
    }

    /// Parse a configuration object from a stream
    /** This assumes that the configuration is to be an object and that the stream will contain individual
     *  key/value pairs without any top-level braces.  The stream will be read until EOF.  If you
     *  wish to load only a single value from the stream and then stop processing use the version
     *  of this constructor which accepts the top_level tag. */
    explicit configuration(std::istream& in);
    explicit configuration(std::istream&& in) : configuration{in} { }

    explicit configuration(std::istream& in, top_level_tag);
    explicit configuration(std::istream&& in, top_level_tag) : configuration{in, top_level} { }

    configuration(configuration const& rhs);
    configuration(configuration&& rhs) noexcept;

    /// Destroy the configuration object
    ~configuration();

    auto operator=(configuration const& rhs) -> configuration&;
    auto operator=(configuration&& rhs) noexcept -> configuration&;
    template <typename T>
    auto operator=(T&& val) -> configuration&
    {
      return *this = configuration{std::forward<T>(val)};
    }

    auto write(std::ostream& out, int indent = 0) const -> void;
    auto write(std::ostream&& out, int indent = 0) const -> void          { write(out, indent); }

    auto write(std::ostream& out, top_level_tag, int indent = 0) const -> void;
    auto write(std::ostream&& out, top_level_tag, int indent = 0) const -> void  { write(out, top_level, indent); }

    auto clear() -> void;

    auto type() const -> node_type                                        { return type_; }

    auto string() const -> string_type const&;
    auto array() const -> array_type const&;
    auto object() const -> object_type const&;

    template <typename T, typename = std::enable_if_t<allow_simple_serialization<T>::value>>
    explicit operator T() const                                           { return parse<T>(string()); }
    explicit operator string_type const&() const                          { return string(); }
    explicit operator array_type const&() const                           { return array(); }
    explicit operator object_type const&() const                          { return object(); }

    auto size() const -> int;

    /// Indexed access
    auto operator[](int i) const -> configuration const&                  { return operator[](static_cast<size_t>(i)); }
    auto operator[](int i) -> configuration&                              { return const_cast<configuration&>(static_cast<configuration const*>(this)->operator[](i)); }
    auto operator[](size_t i) const -> configuration const&;
    auto operator[](size_t i) -> configuration&                           { return const_cast<configuration&>(static_cast<configuration const*>(this)->operator[](i)); }
    auto at(size_t i) const -> configuration const&;
    auto at(size_t i) -> configuration&                                   { return const_cast<configuration&>(static_cast<configuration const*>(this)->at(i)); }

    /// Associative access
    /** The non-const version will insert an empty configuration and return it if no match is found this allows
     *  std::map style access when building a configuration */
    auto operator[](string_view key) const -> configuration const&;
    auto operator[](string_view key) -> configuration&;

    /// Object searching
    auto find(string_view key) const -> configuration const*;
    auto find(string_view key) -> configuration*                          { return const_cast<configuration*>(static_cast<configuration const*>(this)->find(key)); }

    /// Object lookup (optional)
    template <typename T>
    auto optional(string_view key, T const& def) const -> T
    {
      if (auto p = find(key))
        return static_cast<T>(*p);
      return def;
    }
    auto optional(string_view key, configuration const& def) const -> configuration const&
    {
      if (auto p = find(key))
        return *p;
      return def;
    }
    auto optional(string_view key, char const* def) const -> char const*
    {
      if (auto p = find(key))
        return p->string().c_str();
      return def;
    }

  private:
    node_type type_;
    union
    {
      string_type string_;
      array_type  array_;
      object_type object_;
    };
  };

  STEPS_ENUM_TRAITS(configuration::node_type, null, string, array, object);

  /// Initialize a container from a configuration array node
  template <typename Container>
  auto config_array_to_container(configuration const& config)
  {
    auto& array = config.array();
    Container ret(array.size());
    for (configuration::array_type::size_type i = 0; i < array.size(); ++i)
      ret[i] = parse<typename Container::value_type>(array[i].string());
    return ret;
  }

  /// User defined literal to generate a configuration object from a string constant
  inline auto operator "" _config(char const* str, std::size_t len) -> configuration
  {
    return configuration::read_string(std::string(str, len));
  }
}
