/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "autocorrelations.h"
#include "format.h"
#include "trace.h"
#include "unit_test.h"

using namespace steps;

autocorrelations::autocorrelations(double scale1, double ac1, double scale2, double ac2)
{
  auto r1 = (1.0 / (1.0 - ac1)) - 1.0;
  auto r2 = (1.0 / (1.0 - ac2)) - 1.0;

  b_ = std::log(r2 / r1) / std::log(scale2 / scale1);
  a_ = r1 / std::pow(scale1, b_);
}

auto autocorrelations::determine_autocorrelation(double scale) const -> double
{
  return 1.0 - (1.0 / (a_ * std::pow(scale, b_) + 1.0));
}

// LCOV_EXCL_START
TEST_CASE("autocorrelations")
{
  auto ac1 = autocorrelations{181.0, 0.994, 18.0, 0.893};
  CHECK(ac1.determine_autocorrelation(181.0) == approx(0.994));
  CHECK(ac1.determine_autocorrelation(18.0) == approx(0.893));
  CHECK(ac1.determine_autocorrelation(10.0) == approx(0.795886));
  CHECK(ac1.determine_autocorrelation(100.0) == approx(0.987154));
  CHECK(ac1.determine_autocorrelation(400.0) == approx(0.997842));

  auto ac2 = autocorrelations{57.0, 0.97, 18.0, 0.862};
  CHECK(ac2.determine_autocorrelation(57.0) == approx(0.97));
  CHECK(ac2.determine_autocorrelation(18.0) == approx(0.862));
  CHECK(ac2.determine_autocorrelation(10.0) == approx(0.729804));
  CHECK(ac2.determine_autocorrelation(100.0) == approx(0.986318));
  CHECK(ac2.determine_autocorrelation(400.0) == approx(0.998083));
}
// LCOV_EXCL_STOP
