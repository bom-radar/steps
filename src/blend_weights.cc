/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "blend_weights.h"
#include "unit_test.h"
#include "trace.h"

using namespace steps;

static void ludcmp(array2d& a, int n, int* indx, double* d)
{
  static constexpr auto tiny_value = 1.0e-10;
  int imax = 0;
  double dum;
  double* vv = static_cast<double*>(alloca(sizeof(double) * n));

  *d = 1.0;

  for (auto i = 0; i < n; ++i)
  {
    auto big = 0.0;
    for (auto j = 0; j < n; ++j)
      big = std::max(big, std::fabs(a[i][j]));

    if (big == 0.0)
      return;
    vv[i] = 1.0 / big;
  }

  for (auto j = 0; j < n; ++j)
  {
    for (auto i = 0; i < j; ++i)
    {
      auto sum = a[i][j];
      for (auto k = 0; k < i; ++k)
        sum -= a[i][k] * a[k][j];
      a[i][j] = sum;
    }
    auto big = 0.0;
    for (auto i = j; i < n; ++i)
    {
      auto sum = a[i][j];
      for (auto k = 0; k < j; ++k)
        sum -= a[i][k] * a[k][j];
      a[i][j] = sum;
      if ((dum = vv[i] * std::fabs(sum)) >= big)
      {
        big = dum;
        imax = i;
      }
    }
    if (j != imax)
    {
      for (auto k = 0; k < n; ++k)
      {
        dum = a[imax][k];
        a[imax][k] = a[j][k];
        a[j][k] = dum;
      }
      *d = -(*d);
      vv[imax] = vv[j];
    }
    indx[j] = imax;
    if (a[j][j] == 0.0)
      a[j][j] = tiny_value;
    if (j != n - 1)
    {
      dum = 1.0 / a[j][j];
      for (auto i = j + 1; i < n; ++i)
        a[i][j] *= dum;
    }
  }
}

static void lubksb(array2d const& a, int n, int* indx, double* b)
{
  for (auto i = 0, ii = -1; i < n; ++i)
  {
    auto ip = indx[i];
    auto sum = b[ip];
    b[ip] = b[i];
    if (ii >= 0)
    {
      for (auto j = ii; j <= i-1; ++j)
        sum -= a[i][j] * b[j];
    }
    else if (sum)
      ii = i;
    b[i] = sum;
  }

  for (auto i = n - 1; i >= 0; --i)
  {
    auto sum = b[i];
    for (auto j = i + 1; j < n; ++j)
      sum -= a[i][j] * b[j];
    b[i] = sum / a[i][i];
  }
}

auto steps::calculate_blend_weights(array2d const& covars, array1d const& skills, blend_weight_normalization mode) -> array1f
{
  // sanity check
  if (covars.shape().x != skills.size() || covars.shape().y != skills.size())
    throw std::invalid_argument{"Array size mismatch"};

  auto weights = array1f{skills.size()};
  auto sum_weights = 0.0f;

  // calculate the basic weights assuming noise is included
  // optimized versions for 1 and 2 inputs (produces exact same results)
  if (weights.size() == 0)
  {
  }
  else if (weights.size() == 1)
  {
    weights[0] = skills[0] / covars[0][0];
    sum_weights = weights[0];
  }
  else if (weights.size() == 2)
  {
    auto det = covars[0][0] * covars[1][1] - covars[0][1] * covars[1][0];

    double inv[2][2];
    inv[0][0] = covars[1][1] / det;
    inv[0][1] = -covars[0][1] / det;
    inv[1][0] = -covars[1][0] / det;
    inv[1][1] = covars[0][0] / det;

    weights[0] = inv[0][0] * skills[0] + inv[0][1] * skills[1];
    weights[1] = inv[1][0] * skills[0] + inv[1][1] * skills[1];
    sum_weights = weights[0] + weights[1];
  }
  else
  {
    // matrix for the inverse
    auto cov = array2d{covars.shape()};
    auto inv = array2d{covars.shape()};
    // vector for the columns used in the inversion
    auto col = static_cast<double*>(alloca(sizeof(double) * weights.size()));
    // needed for LU decomposition
    auto indx = static_cast<int*>(alloca(sizeof(int) * weights.size()));
    // used in ludcmp
    double d;

    for (auto i = 0; i < covars.size(); ++i)
      cov.data()[i] = covars.data()[i];

    // calculate the LU decomposition
    ludcmp(cov, weights.size(), indx, &d);

    // now go through column by column and calculate the inverse
    for (auto i = 0; i < weights.size(); ++i)
    {
      for (auto j = 0; j < weights.size(); ++j)
        col[j] = 0.0;
      col[i] = 1.0;
      lubksb(cov, weights.size(), indx, col);
      for (auto j = 0; j < weights.size(); ++j)
        inv[i][j] = col[j];
    }

    // calculate the weights
    for (auto i = 0; i < weights.size(); ++i)
    {
      auto w = 0.0;
      for (auto j = 0; j < weights.size(); ++j)
        w += inv[i][j] * skills[j];
      weights[i] = w;
      sum_weights += weights[i];
    }
  }

  // normalize according to the user preference
  switch (mode)
  {
  case blend_weight_normalization::none:
    break;
  case blend_weight_normalization::standard:
    if (sum_weights > 0.0f)
    {
      for (auto i = 0; i < weights.size(); ++i)
        weights[i] /= sum_weights;
    }
    else
    {
      for (auto i = 0; i < weights.size(); ++i)
        weights[i] = 1.0 / weights.size();
    }
    break;
  case blend_weight_normalization::only_first:
    weights[0] = 1.0 - (sum_weights - weights[0]);
    break;
  case blend_weight_normalization::only_last:
    weights[weights.size() - 1] = 1.0 - (sum_weights - weights[weights.size() - 1]);
    break;
  case blend_weight_normalization::except_first:
    if (sum_weights - weights[0] > 0.0f)
    {
      for (auto i = 1; i < weights.size(); ++i)
        weights[i] *= (1.0f - weights[0]) / (sum_weights - weights[0]);
    }
    else
    {
      for (auto i = 1; i < weights.size(); ++i)
        weights[i] = (1.0f - weights[0]) / (weights.size() - 1);
    }
    break;
  case blend_weight_normalization::except_last:
    if (sum_weights - weights[weights.size() - 1] > 0.0f)
    {
      for (auto i = 0; i < weights.size() - 1; ++i)
        weights[i] *= (1.0f - weights[weights.size() - 1]) / (sum_weights - weights[weights.size() - 1]);
    }
    else
    {
      for (auto i = 0; i < weights.size() - 1; ++i)
        weights[i] = (1.0f - weights[weights.size() - 1]) / (weights.size() - 1);
    }
    break;
  }

  return weights;
}

auto steps::constrain_covariances(array2d& covars, array1d const& skills, bool verbose) -> void
{
  if (skills.size() != 2)
    throw std::runtime_error{"UNIMPLEMENTED - CONSTRAIN MULTIPLE INPUTS"};

  auto c1 = covars[1][1] * skills[0] / skills[1];
  auto c2 = covars[0][0] * skills[1] / skills[0];
  auto constraint = std::min(c1, c2);
  if (covars[0][1] > constraint)
  {
    if (verbose)
      trace(trace_level::warning, "constraining excessive covar {} to {}", covars[0][1], constraint);
    covars[0][1] = constraint;
    covars[1][0] = constraint;
  }
  if (covars[0][1] < 0.0f)
  {
    if (verbose)
      trace(trace_level::warning, "constraining negative covar {} to 0.0", covars[0][1]);
    covars[0][1] = 0.0f;
    covars[1][0] = 0.0f;
  }
}

// LCOV_EXCL_START
TEST_CASE("blend_weights")
{
#if 0 // TODO
  array1d skills{2};

  skills[0] = 1.0;
  skills[1] = 0.1;

  auto weights = blend_weights{2};
  weights.update(skills);
#endif
}
// LCOV_EXCL_STOP
