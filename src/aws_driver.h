/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2020 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "aws.h"

#include <aws/batch/BatchClient.h>
#include <aws/monitoring/CloudWatchClient.h>
#include <aws/sns/SNSClient.h>
#include <aws/sqs/SQSClient.h>

namespace steps::aws
{
  /// Base class for drivers that process STEPS on AWS requests
  class driver
  {
  public:
    driver(configuration const& config);

  protected:
    auto handle_request(string const& group, Aws::Utils::Json::JsonValue& request) -> void;

    auto& cw()                      { return cw_; }
    auto& metric_namespace() const  { return metric_namespace_; }

    auto cur_members() const        { return cur_members_; }

  private:
    using CloudWatchClient = Aws::CloudWatch::CloudWatchClient;
    using SNSClient = Aws::SNS::SNSClient;
    using steady_time = std::chrono::steady_clock::time_point;

  private:
    virtual auto on_progress(int member, int forecast) -> void;

    auto notify_result(string const& group, Aws::Utils::Json::JsonValue& request, bool success) -> void;
    auto publish_metrics(char const* metric, bool success, steady_time time_start, steady_time time_end) -> void;

    auto run_forecast(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto run_postprocess(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto run_preprocess(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto run_mosaic(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto run_verify(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto run_verify_aggregate(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto run_custom_action(string const& group, Aws::Utils::Json::JsonValue& request) -> void;

  private:
    CloudWatchClient  cw_;                  // CloudWatch client
    SNSClient         sns_;                 // SNS client

    string            metric_namespace_;    // namespace for our custom cloudwatch metrics
    string            sns_arn_;             // ARN of our forecast complete SNS topic
    vector<string>    custom_actions_;      // user defined actions

    int               cur_members_;         // member count of current request (needed by driver_sqs)
  };

  /// Deletion policies for job requests received via SQS
  enum class sqs_delete_policy
  {
      immediate     // Delete SQS message as soon as received
    , after_init    // Delete SQS message after initialization phase before first forecast
    , complete      // Delete SQS message after request is processed
  };

  /// Main driver to service SQS queue of STEPS forecast requests
  /** This is the driver used to implement the steps-aws service that is typically run as part of an EC2 autoscaling
   *  group.  It continually monitors the main SQS queue, and processes requests one at a time as they arrive.  It
   *  periodically also checks the secondary SQS queue that is used for long running or low priority 'batch' requests.
   *  These it dequeues and transforms into job submissions for the AWS Batch environment. */
  class driver_sqs : public driver
  {
  public:
    driver_sqs(configuration const& config);

    auto run() -> void;

  private:
    using SQSClient = Aws::SQS::SQSClient;
    using BatchClient = Aws::Batch::BatchClient;

  private:
    auto on_progress(int member, int forecast) -> void override;

    auto publish_queue_latency(Aws::SQS::Model::Message const& msg) -> void;
    auto submit_batch_job(string const& group, Aws::Utils::Json::JsonValue& request) -> void;
    auto delete_current_request() -> void;

  private:
    SQSClient         sqs_;                 // SQS client
    BatchClient       batch_;               // Batch client

    string            sqs_url_;             // URL of the sqs queue to process requests from
    duration          timeout_initial_;     // initial visibility timeout for messages
    duration          timeout_extension_;   // visibility timeout extension applied when reaching safety margin
    duration          timeout_margin_;      // extend timeout when it gets this close to current time
    string            batch_job_defn_arn_;  // ARN of our batch job definition
    string            batch_queue_h_arn_;   // ARN of high priority batch job queue
    string            batch_queue_m_arn_;   // ARN of medium priority batch job queue
    string            batch_queue_l_arn_;   // ARN of low priority batch job queue

    std::mutex        mut_cur_request_;     // Mutex to protect below items (used by progress callback)
    string            cur_receipt_handle_;  // Receipt handle for current request
    time_point        cur_timeout_bump_;    // Time after which we should next extend the visibility timeout
    sqs_delete_policy cur_delete_policy_;   // Delete policy for current request
    int               cur_init_count_;      // Progress towards complete initialization of current request
  };

  /// Driver that processes a single request for use in AWS Batch jobs
  class driver_batch : public driver
  {
  public:
    driver_batch(configuration const& config);

    auto run(configuration const& config) -> void;
  };
}

namespace steps
{
  STEPS_ENUM_TRAITS(aws::sqs_delete_policy, immediate, after_init, complete);
}
