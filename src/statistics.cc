/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "statistics.h"
#include "array1.h"
#include "unit_test.h"
#include "trace.h"
#include <random>

using namespace steps;

auto steps::intersection(span<mask_val const> l, span<mask_val const> r, span<mask_val> out) -> void
{
  for (auto i = 0; i < out.size(); ++i)
    out[i] = l[i] && r[i];
}

auto steps::variance(span<float const> a) -> double
{
  auto mean = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    mean += a[i];
  mean /= a.size();

  auto var = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    var += (a[i] - mean) * (a[i] - mean);
  var /= a.size();

  return var;
}

// TODO - check same result from this as covariance(a, a)
auto steps::variance(span<vec2f const> a) -> double
{
  auto mean = vec2d{0.0, 0.0};
  for (auto i = 0; i < a.size(); ++i)
    mean += vec2d{a[i]};
  mean /= a.size();

  auto var = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    auto aa = vec2d{a[i]} - mean;
    var += aa.x * aa.x + aa.y * aa.y;
  }
  var /= a.size();

  return var;
}

auto steps::variance_ignore_nans(span<float const> a) -> double
{
  // mean of a
  auto count = 0;
  auto mean = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (!std::isnan(a[i]))
    {
      mean += a[i];
      ++count;
    }
  }

  // if there is no valid data we can't calculate a variance
  if (count == 0)
    return 0.0;

  mean /= count;

  // determine the variance
  auto var = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    if (!std::isnan(a[i]))
      var += (a[i] - mean) * (a[i] - mean);
  var /= count;

  return var;
}

auto steps::variance_masked(span<mask_val const> mask, span<float const> a) -> double
{
  // sanity checks
  if (a.size() != mask.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of a
  auto count = 0;
  auto mean = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      mean += a[i];
      ++count;
    }
  }

  // if there is no valid data we can't calculate a variance
  if (count == 0)
    return 0.0;

  mean /= count;

  // determine the variance
  auto var = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    if (mask[i])
      var += (a[i] - mean) * (a[i] - mean);
  var /= count;

  return var;
}

// TODO - check against covariance_masked(mask, a, a)
auto steps::variance_masked(span<mask_val const> mask, span<vec2f const> a) -> double
{
  // sanity checks
  if (a.size() != mask.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of a
  auto count = 0;
  auto mean = vec2d{0.0, 0.0};
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      mean += vec2d{a[i]};
      ++count;
    }
  }

  // if there is no valid data we can't calculate a variance
  if (count == 0)
    return 0.0;

  mean /= count;

  auto var = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      auto aa = vec2d{a[i]} - mean;
      var += aa.x * aa.x + aa.y * aa.y;
    }
  }
  var /= count;

  return var;
}

auto steps::variance_weighted(span<float const> weight, span<float const> a) -> double
{
  // sanity checks
  if (a.size() != weight.size())
    throw std::invalid_argument{"Array size mismatch"};

  // weighted mean of a and b
  auto mean = 0.0, sum_weight = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    mean += weight[i] * a[i];
    sum_weight += weight[i];
  }
  mean /= sum_weight;

  // determine the variance
  auto var = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    var += weight[i] * (a[i] - mean) * (a[i] - mean);
  var /= sum_weight;

  return var;
}

/// Determine covariance between two arrays
auto steps::covariance(span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of a and b
  auto mean_a = 0.0, mean_b = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    mean_a += a[i];
    mean_b += b[i];
  }
  mean_a /= a.size();
  mean_b /= a.size();

  // determine the covariance
  auto covar = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    covar += (a[i] - mean_a) * (b[i] - mean_b);
  covar /= a.size();

  return covar;
}

auto steps::covariance(span<vec2f const> a, span<vec2f const> b) -> vec2d
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  auto mean_a = vec2d{0.0, 0.0}, mean_b = vec2d{0.0, 0.0};
  for (auto i = 0; i < a.size(); ++i)
  {
    mean_a += vec2d{a[i]};
    mean_b += vec2d{b[i]};
  }
  mean_a /= a.size();
  mean_b /= a.size();

  auto covar = vec2d{0.0, 0.0};
  for (auto i = 0; i < a.size(); ++i)
  {
    auto aa = vec2d{a[i]} - mean_a;
    auto bb = vec2d{b[i]} - mean_b;
    // aa * conj(bb)
    covar += vec2d{aa.x * bb.x - aa.y * -bb.y, aa.x * -bb.y + aa.y * bb.x};
  }
  covar /= a.size();

  return covar;
}

auto steps::covariance_ignore_nans(span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of a and b
  auto count = 0;
  auto mean_a = 0.0, mean_b = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (!std::isnan(a[i]) && !std::isnan(b[i]))
    {
      mean_a += a[i];
      mean_b += b[i];
      ++count;
    }
  }

  // if there is no valid data we can't calculate a covariance
  if (count == 0)
    return 0.0;

  mean_a /= count;
  mean_b /= count;

  // determine the covariance
  auto covar = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    if (!std::isnan(a[i]) && !std::isnan(b[i]))
      covar += (a[i] - mean_a) * (b[i] - mean_b);
  covar /= count;

  return covar;
}

auto steps::covariance_masked(span<mask_val const> mask, span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of a and b
  auto count = 0;
  auto mean_a = 0.0, mean_b = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      mean_a += a[i];
      mean_b += b[i];
      ++count;
    }
  }

  // if there is no valid data we can't calculate a covariance
  if (count == 0)
    return 0.0;

  mean_a /= count;
  mean_b /= count;

  // determine the covariance
  auto covar = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    if (mask[i])
      covar += (a[i] - mean_a) * (b[i] - mean_b);
  covar /= count;

  return covar;
}

auto steps::covariance_masked(span<mask_val const> mask, span<vec2f const> a, span<vec2f const> b) -> vec2d
{
  // sanity checks
  if (a.size() != b.size() || a.size() != mask.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of a and b
  auto count = 0;
  auto mean_a = vec2d{0.0, 0.0}, mean_b = vec2d{0.0, 0.0};
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      mean_a += vec2d{a[i]};
      mean_b += vec2d{b[i]};
      ++count;
    }
  }

  // if there is no valid data we can't calculate a covariance
  if (count == 0)
    return vec2d{0.0, 0.0};

  mean_a /= count;
  mean_b /= count;

  auto covar = vec2d{0.0, 0.0};
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      auto aa = vec2d{a[i]} - mean_a;
      auto bb = vec2d{b[i]} - mean_b;
      // aa * conj(bb)
      covar += vec2d{aa.x * bb.x - aa.y * -bb.y, aa.x * -bb.y + aa.y * bb.x};
    }
  }
  covar /= count;

  return covar;
}

/// Determine weighted covariance between two arrays
auto steps::covariance_weighted(span<float const> weight, span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != weight.size() || b.size() != weight.size())
    throw std::invalid_argument{"Array size mismatch"};

  // weighted mean of a and b
  auto mean_a = 0.0, mean_b = 0.0, sum_weight = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    mean_a += weight[i] * a[i];
    mean_b += weight[i] * b[i];
    sum_weight += weight[i];
  }
  mean_a /= sum_weight;
  mean_b /= sum_weight;

  // determine the covariance
  auto covar = 0.0;
  for (auto i = 0; i < a.size(); ++i)
    covar += weight[i] * (a[i] - mean_a) * (b[i] - mean_b);
  covar /= sum_weight;

  return covar;
}

auto steps::correlation(span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  // TODO - which version performs better (especially with SIMD)?
#if 0
  // mean of the windowed area in a and b
  auto mean_a = 0.0, mean_b = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    mean_a += a[i];
    mean_b += b[i];
  }

  mean_a /= a.size();
  mean_b /= a.size();

  // determine the variance of a and b, and the cross product of a and b
  auto var_a = 0.0, var_b = 0.0, cross = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    var_a += (a[i] - mean_a) * (a[i] - mean_a);
    var_b += (b[i] - mean_b) * (b[i] - mean_b);
    cross += (a[i] - mean_a) * (b[i] - mean_b);
  }
  var_a /= a.size();
  var_b /= a.size();
  cross /= a.size();
#else
  auto var_a = covariance(a,a);
  auto var_b = covariance(b,b);
  auto cross = covariance(a,b);
#endif

  return (var_a * var_b) > 0.0 ? cross / std::sqrt(var_a * var_b) : 0.0;
}

auto steps::correlation(span<vec2f const> a, span<vec2f const> b) -> vec2d
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  auto var_a = covariance(a, a).x;
  auto var_b = covariance(b, b).x;
  auto cross = covariance(a, b);

  return (var_a * var_b) > 0.0 ? cross / std::sqrt(var_a * var_b) : vec2d{0.0, 0.0};
}

auto steps::correlation_masked(span<mask_val const> mask, span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != b.size() || a.size() != mask.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of the windowed area in a and b
  auto count = 0;
  auto mean_a = 0.0, mean_b = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      mean_a += a[i];
      mean_b += b[i];
      ++count;
    }
  }

  // if there is no valid data we can't calculate a correlation
  if (count == 0)
    return 0.0;

  mean_a /= count;
  mean_b /= count;

  // determine the variance of a and b, and the cross product of a and b
  auto var_a = 0.0, var_b = 0.0, cross = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (mask[i])
    {
      var_a += (a[i] - mean_a) * (a[i] - mean_a);
      var_b += (b[i] - mean_b) * (b[i] - mean_b);
      cross += (a[i] - mean_a) * (b[i] - mean_b);
    }
  }
  var_a /= count;
  var_b /= count;
  cross /= count;

  return (var_a * var_b) > 0.0 ? cross / std::sqrt(var_a * var_b) : 0.0;
}

auto steps::correlation_masked(span<mask_val const> mask, span<vec2f const> a, span<vec2f const> b) -> vec2d
{
  // sanity checks
  if (a.size() != b.size() || a.size() != mask.size())
    throw std::invalid_argument{"Array size mismatch"};

  auto var_a = covariance_masked(mask, a, a).x;
  auto var_b = covariance_masked(mask, b, b).x;
  auto cross = covariance_masked(mask, a, b);

  return (var_a * var_b) > 0.0 ? cross / std::sqrt(var_a * var_b) : vec2d{0.0, 0.0};
}

auto steps::correlation_ignore_nans(span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != b.size())
    throw std::invalid_argument{"Array size mismatch"};

  // mean of the windowed area in a and b
  auto count = 0;
  auto mean_a = 0.0, mean_b = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (!std::isnan(a[i]) && !std::isnan(b[i]))
    {
      mean_a += a[i];
      mean_b += b[i];
      ++count;
    }
  }

  // if there is no valid data we can't calculate a correlation
  if (count == 0)
    return 0.0;

  mean_a /= count;
  mean_b /= count;

  // determine the variance of a and b, and the cross product of a and b
  auto var_a = 0.0, var_b = 0.0, cross = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    if (!std::isnan(a[i]) && !std::isnan(b[i]))
    {
      var_a += (a[i] - mean_a) * (a[i] - mean_a);
      var_b += (b[i] - mean_b) * (b[i] - mean_b);
      cross += (a[i] - mean_a) * (b[i] - mean_b);
    }
  }
  var_a /= count;
  var_b /= count;
  cross /= count;

  return (var_a * var_b) > 0.0 ? cross / std::sqrt(var_a * var_b) : 0.0;
}

/// Determine correlation between two arrays
auto steps::correlation_weighted(span<float const> weight, span<float const> a, span<float const> b) -> double
{
  // sanity checks
  if (a.size() != weight.size() || b.size() != weight.size())
    throw std::invalid_argument{"Array size mismatch"};

  // TODO - which version performs better (especially with SIMD)?
#if 0
  // weighted mean of a and b
  auto mean_a = 0.0, mean_b = 0.0, sum_weight = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    mean_a += weight[i] * a[i];
    mean_b += weight[i] * b[i];
    sum_weight += weight[i];
  }

  mean_a /= sum_weight;
  mean_b /= sum_weight;

  // determine the variance of a and b, and the cross product of a and b
  auto var_a = 0.0, var_b = 0.0, cross = 0.0;
  for (auto i = 0; i < a.size(); ++i)
  {
    var_a += weight[i] * (a[i] - mean_a) * (a[i] - mean_a);
    var_b += weight[i] * (b[i] - mean_b) * (b[i] - mean_b);
    cross += weight[i] * (a[i] - mean_a) * (b[i] - mean_b);
  }
  var_a /= sum_weight;
  var_b /= sum_weight;
  cross /= sum_weight;
#else
  auto var_a = covariance_weighted(weight, a, a);
  auto var_b = covariance_weighted(weight, b, b);
  auto cross = covariance_weighted(weight, a, b);
#endif

  return (var_a * var_b) > 0.0 ? cross / std::sqrt(var_a * var_b) : 0.0;
}

auto steps::covariance_matrix(span<span<float const> const> inputs) -> array2d
{
  auto matrix = array2d(inputs.size(), inputs.size());
  auto means = static_cast<double*>(alloca(sizeof(double) * inputs.size()));

  // calculate the mean for each input
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
      sum += a[i];
    means[y] = sum / a.size();
  }

  // calculate the diagonal (variances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto a_mean = means[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
      sum += (a[i] - a_mean) * (a[i] - a_mean);
    matrix[y][y] = sum / a.size();
  }

  // calculate the off-diagonals (covariances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    for (auto x = y + 1; x < matrix.shape().x; ++x)
    {
      auto a = inputs[y], b = inputs[x];
      auto a_mean = means[y], b_mean = means[x];
      auto sum = 0.0;
      for (auto i = 0; i < a.size(); ++i)
        sum += (a[i] - a_mean) * (b[i] - b_mean);
      matrix[y][x] = sum / a.size();
      matrix[x][y] = matrix[y][x];
    }
  }

  return matrix;
}

/* technically the covariances are themselves 2d vectors, but we just need their magnitudes so we return them directly
 * as a simple 2d array rather than as a 2d array of 2d vectors. */
auto steps::covariance_matrix(span<span<vec2f const> const> inputs) -> array2d
{
  auto matrix = array2d(inputs.size(), inputs.size());
  auto means = static_cast<vec2d*>(alloca(sizeof(vec2d) * inputs.size()));

  // calculate the mean for each input
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto sum = vec2d{0.0, 0.0};
    for (auto i = 0; i < a.size(); ++i)
      sum += vec2d{a[i]};
    means[y] = sum / a.size();
  }

  // calculate the diagonal (variances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto a_mean = means[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
    {
      auto aa = vec2d{a[i]} - a_mean;
      sum += aa.x * aa.x + aa.y * aa.y;
    }
    matrix[y][y] = sum / a.size();
  }

  // calculate the off-diagonals (covariances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    for (auto x = y + 1; x < matrix.shape().x; ++x)
    {
      auto a = inputs[y], b = inputs[x];
      auto a_mean = means[y], b_mean = means[x];
      auto sum = vec2d{0.0, 0.0};
      for (auto i = 0; i < a.size(); ++i)
      {
        auto aa = vec2d{a[i]} - a_mean;
        auto bb = vec2d{b[i]} - b_mean;
        // aa * conj(bb)
        sum += vec2d{aa.x * bb.x - aa.y * -bb.y, aa.x * -bb.y + aa.y * bb.x};
      }
      matrix[y][x] = (sum / a.size()).magnitude();
      matrix[x][y] = matrix[y][x];
    }
  }

  return matrix;
}

auto steps::covariance_matrix_masked(span<mask_val const> mask, span<span<float const> const> inputs) -> array2d
{
  auto matrix = array2d(inputs.size(), inputs.size());
  auto means = static_cast<double*>(alloca(sizeof(double) * inputs.size()));

  // calculate the mean for each input
  auto count = 0;
  {
    auto a = inputs[0];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
    {
      if (mask[i])
      {
        sum += a[i];
        ++count;
      }
    }
    means[0] = sum / count;
  }
  for (auto y = 1; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
      if (mask[i])
        sum += a[i];
    means[y] = sum / count;
  }

  if (count == 0)
    throw std::runtime_error{"Failed to calculate covariance matrix"};

  // calculate the diagonal (variances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto a_mean = means[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
      if (mask[i])
        sum += (a[i] - a_mean) * (a[i] - a_mean);
    matrix[y][y] = sum / count;
  }

  // calculate the off-diagonals (covariances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    for (auto x = y + 1; x < matrix.shape().x; ++x)
    {
      auto a = inputs[y], b = inputs[x];
      auto a_mean = means[y], b_mean = means[x];
      auto sum = 0.0;
      for (auto i = 0; i < a.size(); ++i)
        if (mask[i])
          sum += (a[i] - a_mean) * (b[i] - b_mean);
      matrix[y][x] = sum / count;
      matrix[x][y] = matrix[y][x];
    }
  }

  return matrix;
}

/* technically the covariances are themselves 2d vectors, but we just need their magnitudes so we return them directly
 * as a simple 2d array rather than as a 2d array of 2d vectors. */
auto steps::covariance_matrix_masked(span<mask_val const> mask, span<span<vec2f const> const> inputs) -> array2d
{
  auto matrix = array2d(inputs.size(), inputs.size());
  auto means = static_cast<vec2d*>(alloca(sizeof(vec2d) * inputs.size()));

  // calculate the mean for each input
  auto count = 0;
  {
    auto a = inputs[0];
    auto sum = vec2d{0.0, 0.0};
    for (auto i = 0; i < a.size(); ++i)
    {
      if (mask[i])
      {
        sum += vec2d{a[i]};
        ++count;
      }
    }
    means[0] = sum / count;
  }
  for (auto y = 1; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto sum = vec2d{0.0, 0.0};
    for (auto i = 0; i < a.size(); ++i)
      if (mask[i])
        sum += vec2d{a[i]};
    means[y] = sum / count;
  }

  if (count == 0)
    throw std::runtime_error{"Failed to calculate covariance matrix"};

  // calculate the diagonal (variances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto a_mean = means[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
    {
      if (mask[i])
      {
        auto aa = vec2d{a[i]} - a_mean;
        sum += aa.x * aa.x + aa.y * aa.y;
      }
    }
    matrix[y][y] = sum / count;
  }

  // calculate the off-diagonals (covariances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    for (auto x = y + 1; x < matrix.shape().x; ++x)
    {
      auto a = inputs[y], b = inputs[x];
      auto a_mean = means[y], b_mean = means[x];
      auto sum = vec2d{0.0, 0.0};
      for (auto i = 0; i < a.size(); ++i)
      {
        if (mask[i])
        {
          auto aa = vec2d{a[i]} - a_mean;
          auto bb = vec2d{b[i]} - b_mean;
          // aa * conj(bb)
          sum += vec2d{aa.x * bb.x - aa.y * -bb.y, aa.x * -bb.y + aa.y * bb.x};
        }
      }
      matrix[y][x] = (sum / count).magnitude();
      matrix[x][y] = matrix[y][x];
    }
  }

  return matrix;
}

auto steps::covariance_matrix_weighted(span<float const> weight, span<span<float const> const> inputs) -> array2d
{
  auto matrix = array2d(inputs.size(), inputs.size());
  auto means = static_cast<double*>(alloca(sizeof(double) * inputs.size()));

  // calculate the mean for each input
  auto sum_weight = 0.0;
  {
    auto a = inputs[0];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
    {
      sum += weight[i] * a[i];
      sum_weight += weight[i];
    }
    means[0] = sum / sum_weight;
  }
  for (auto y = 1; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
      sum += weight[i] * a[i];
    means[y] = sum / sum_weight;
  }

  if (sum_weight <= 0.0)
    throw std::runtime_error{"Failed to calculate covariance matrix"};

  // calculate the diagonal (variances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    auto a = inputs[y];
    auto a_mean = means[y];
    auto sum = 0.0;
    for (auto i = 0; i < a.size(); ++i)
      sum += weight[i] * (a[i] - a_mean) * (a[i] - a_mean);
    matrix[y][y] = sum / sum_weight;
  }

  // calculate the off-diagonals (covariances)
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    for (auto x = y + 1; x < matrix.shape().x; ++x)
    {
      auto a = inputs[y], b = inputs[x];
      auto a_mean = means[y], b_mean = means[x];
      auto sum = 0.0;
      for (auto i = 0; i < a.size(); ++i)
        sum += weight[i] * (a[i] - a_mean) * (b[i] - b_mean);
      matrix[y][x] = sum / sum_weight;
      matrix[x][y] = matrix[y][x];
    }
  }

  return matrix;
}

auto steps::assert_covariance_matrix(array2d const& matrix) -> void
{
  // ensure var(i) * var(j) >= cov(i,j)^2
  for (auto y = 0; y < matrix.shape().y; ++y)
    for (auto x = y + 1; x < matrix.shape().x; ++x)
      if (matrix[y][y] * matrix[x][x] < matrix[y][x] * matrix[y][x])
        throw std::runtime_error{"Invalid covariance matrix detected"};
}

auto steps::covariance_matrix_to_correlation_matrix(array2d& matrix) -> void
{
  for (auto y = 0; y < matrix.shape().y; ++y)
  {
    for (auto x = y + 1; x < matrix.shape().x; ++x)
    {
      matrix[y][x] = matrix[y][x] / std::sqrt(matrix[y][y] * matrix[x][x]);
      matrix[x][y] = matrix[y][x];
    }
    matrix[y][y] = 1.0;
  }
}

auto steps::local_standard_deviation(array2f const& data, float mean, int radius, array2f& out) -> void
{
#if 0
  if (data.shape() != out.shape())
    throw std::invalid_argument{"Array size mismatch"};

  auto buf = array2f{data.shape()};

  /* in this approach I reflect over the edge pixels back into the grid to preseve a perfect sample count */

  // clamp the radius to sensible range (also prevents our mirroring trick from overlflowing)
  radius = std::clamp(radius, 0, std::min(data.shape().x, data.shape().y));

  // constants
  auto count = double((radius * 2 + 1) * (radius * 2 + 1));
  auto mean_sqr = mean * mean;

  // collect the sum of squares in the X direction
  for (auto y = 0; y < data.shape().y; ++y)
  {
    // simple implementation - we can make faster by adding / subtracting the pixel at either side of the window
    for (auto x = 0; x < data.shape().x; ++x)
    {
      auto sum = 0.0;
      for (auto xi = x - radius; xi <= x + radius; ++xi)
      {
        auto xr
          = xi < 0 ? -xi - 1
          : xi >= data.shape().x ? 2 * data.shape().x - xi - 1
          : xi;
        sum += data[y][xr] * data[y][xr];
      }
      buf[y][x] = sum;
    }
  }

  // now sum the X squared sums in the the y direction
  for (auto x = 0; x < data.shape().x; ++x)
  {
    // simple implementation - we can make faster by adding / subtracting the pixel at either side of the window
    for (auto y = 0; y < data.shape().y; ++y)
    {
      auto sum = 0.0;
      for (auto yi = y - radius; yi <= y + radius; ++yi)
      {
        auto yr
          = yi < 0 ? -yi - 1
          : yi >= data.shape().y ? 2 * data.shape().y - yi - 1
          : yi;
        sum += buf[yr][x];
      }
      auto var = sum / count - mean_sqr;
      out[y][x] = var < 0.0 ? 0.0 : std::sqrt(var);
    }
  }
#else
  if (data.shape() != out.shape())
    throw std::invalid_argument{"Array size mismatch"};

  auto buf = array2f{data.shape()};

  /* in this approach I reflect over the edge pixels back into the grid to preseve a perfect sample count */

  // clamp the radius to sensible range (also prevents our mirroring trick from overlflowing)
  radius = std::clamp(radius, 0, std::min(data.shape().x, data.shape().y) - 1);

  // constants
  auto count = double((radius * 2 + 1) * (radius * 2 + 1));
  auto mean_sqr = mean * mean;

  // collect the sum of squares in the X direction
  for (auto y = 0; y < data.shape().y; ++y)
  {
    // initialize the kernel (as if processing x = -1)
    auto sum = 0.0;
    for (auto xi = -1 - radius; xi <= -1 + radius; ++xi)
    {
      auto xr
        = xi < 0 ? -xi - 1
        : xi >= data.shape().x ? 2 * data.shape().x - xi - 1
        : xi;
      sum += data[y][xr] * data[y][xr];
    }

    // simple implementation - we can make faster by adding / subtracting the pixel at either side of the window
    for (auto x = 0; x < data.shape().x; ++x)
    {
      // subtract oldest value from kernel
      {
        auto xi = x - radius - 1;
        auto xr
          = xi < 0 ? -xi - 1
          : xi >= data.shape().x ? 2 * data.shape().x - xi - 1
          : xi;
        sum -= data[y][xr] * data[y][xr];
      }

      // add new value to kernel
      {
        auto xi = x + radius;
        auto xr
          = xi < 0 ? -xi - 1
          : xi >= data.shape().x ? 2 * data.shape().x - xi - 1
          : xi;
        sum += data[y][xr] * data[y][xr];
      }

      // save value
      buf[y][x] = sum;
    }
  }

  // now sum the X squared sums in the the y direction
  for (auto x = 0; x < data.shape().x; ++x)
  {
    // initialize the kernel (as if y = -1)
    auto sum = 0.0;
    for (auto yi = -1 - radius; yi <= -1 + radius; ++yi)
    {
      auto yr
        = yi < 0 ? -yi - 1
        : yi >= data.shape().y ? 2 * data.shape().y - yi - 1
        : yi;
      sum += buf[yr][x];
    }

    // simple implementation - we can make faster by adding / subtracting the pixel at either side of the window
    for (auto y = 0; y < data.shape().y; ++y)
    {
      // subtract oldest value from kernel
      {
        auto yi = y - radius - 1;
        auto yr
          = yi < 0 ? -yi - 1
          : yi >= data.shape().y ? 2 * data.shape().y - yi - 1
          : yi;
        sum -= buf[yr][x];
      }

      // add new value to kernel
      {
        auto yi = y + radius;
        auto yr
          = yi < 0 ? -yi - 1
          : yi >= data.shape().y ? 2 * data.shape().y - yi - 1
          : yi;
        sum += buf[yr][x];
      }

      // output value
      auto var = sum / count - mean_sqr;
      out[y][x] = var < 0.0 ? 0.0 : std::sqrt(var);
    }
  }
#endif
}

// LCOV_EXCL_START
TEST_CASE("intersection")
{
  auto l = array1<mask_val>{4};
  auto r = array1<mask_val>{4};
  auto o = array1<mask_val>{4};

  l[0] = 0; r[0] = 0;
  l[1] = 0; r[1] = 1;
  l[2] = 1; r[2] = 0;
  l[3] = 1; r[3] = 1;

  intersection(l, r, o);

  CHECK(o[0] == 0);
  CHECK(o[1] == 0);
  CHECK(o[2] == 0);
  CHECK(o[3] == 1);
}
TEST_CASE("variance<float>")
{
  auto a = std::array<float, 10>{0.996f, 0.912f, 0.158f, 0.622f, 0.563f, 0.778f, 0.123f, 0.017f, 0.356f, 0.375f};
  CHECK(variance(a) == approx(0.103964));

  auto m = std::array<mask_val, 10>{0, 0, 1, 1, 1, 0, 1, 1, 1, 1};
  CHECK(variance_masked(m, a) == approx(0.044477));

  auto w = std::array<float, 10>{0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 0.5f, 1.0f, 1.0f, 1.0f, 1.0f};
  CHECK(variance_weighted(w, a) == approx(0.086779));

  a[0] = a[1] = a[5] = std::numeric_limits<float>::quiet_NaN();
  CHECK(variance_ignore_nans(a) == approx(0.044477));
}
TEST_CASE("covariance<vec2f>")
{
  auto a = array2<vec2f>{vec2i{4, 4}};
  auto b = array2<vec2f>{vec2i{4, 4}};

  auto rnd = std::mt19937{250383};
  auto dist = std::normal_distribution<float>{0.0f, std::sqrt(0.5f)};

  for (auto i = 0; i < a.size(); ++i)
    a.data()[i] = vec2f{dist(rnd), dist(rnd)};

  // magnitude adjustment
  for (auto i = 0; i < a.size(); ++i)
    b.data()[i] = 2 * a.data()[i];
  auto covar = covariance(a, b);
  auto corrl = correlation(a, b);
  trace(
        trace_level::log
      , "x2  cov {} (mag {} angle {}) corrl {} (mag {} angle {})"
      , covar
      , covar.magnitude()
      , radians_to_degrees(covar.angle())
      , corrl
      , corrl.magnitude()
      , radians_to_degrees(corrl.angle())
      );

  // rotation by 30 degrees
  auto dista = std::normal_distribution<float>{0.0f, 5.0f};
  for (auto i = 0; i < a.size(); ++i)
  {
    auto ang = degrees_to_radians(dista(rnd));
    b.data()[i] = vec2f(a.data()[i].x * cos(ang) - a.data()[i].y * sin(ang), a.data()[i].x * sin(ang) + a.data()[i].y * cos(ang));
  }
  covar = covariance(a, b);
  corrl = correlation(a, b);
  trace(
        trace_level::log
      , "r30  cov {} (mag {} angle {}) corrl {} (mag {} angle {})"
      , covar
      , covar.magnitude()
      , radians_to_degrees(covar.angle())
      , corrl
      , corrl.magnitude()
      , radians_to_degrees(corrl.angle())
      );
}
// LCOV_EXCL_STOP
