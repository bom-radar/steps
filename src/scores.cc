/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "scores.h"
#include "statistics.h"
#include "trace.h"
#include "unit_test.h"

using namespace steps;

void steps::generate_occurrence_field_low_detect(
      span<float const> observations
    , float threshold
    , span<event_occurrence> occurrence)
{
  if (observations.size() != occurrence.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (std::isnan(observations[i]))
      occurrence[i] = event_occurrence::unknown;
    else if (observations[i] < threshold)
      occurrence[i] = event_occurrence::yes;
    else
      occurrence[i] = event_occurrence::no;
  }
}

void steps::generate_occurrence_field_high_detect(
      span<float const> observations
    , float threshold
    , span<event_occurrence> occurrence)
{
  if (observations.size() != occurrence.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (std::isnan(observations[i]))
      occurrence[i] = event_occurrence::unknown;
    else if (observations[i] > threshold)
      occurrence[i] = event_occurrence::yes;
    else
      occurrence[i] = event_occurrence::no;
  }
}

void steps::generate_occurrence_field_band_detect(
      span<float const> observations
    , float threshold_low
    , float threshold_high
    , span<event_occurrence> occurrence)
{
  if (observations.size() != occurrence.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (std::isnan(observations[i]))
      occurrence[i] = event_occurrence::unknown;
    else if (   observations[i] >= threshold_low
             && observations[i] < threshold_high)
      occurrence[i] = event_occurrence::yes;
    else
      occurrence[i] = event_occurrence::no;
  }
}

void steps::generate_occurrence_field_low_detect(
      span<float const> observations
    , span<float const> threshold
    , span<event_occurrence> occurrence)
{
  if (   observations.size() != occurrence.size()
      || threshold.size() != occurrence.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (std::isnan(observations[i]))
      occurrence[i] = event_occurrence::unknown;
    else if (observations[i] < threshold[i])
      occurrence[i] = event_occurrence::yes;
    else
      occurrence[i] = event_occurrence::no;
  }
}

void steps::generate_occurrence_field_high_detect(
      span<float const> observations
    , span<float const> threshold
    , span<event_occurrence> occurrence)
{
  if (   observations.size() != occurrence.size()
      || threshold.size() != occurrence.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (std::isnan(observations[i]))
      occurrence[i] = event_occurrence::unknown;
    else if (observations[i] > threshold[i])
      occurrence[i] = event_occurrence::yes;
    else
      occurrence[i] = event_occurrence::no;
  }
}

void steps::generate_occurrence_field_band_detect(
      span<float const> observations
    , span<float const> threshold_low
    , span<float const> threshold_high
    , span<event_occurrence> occurrence)
{
  if (   observations.size() != occurrence.size()
      || threshold_low.size() != occurrence.size()
      || threshold_high.size() != occurrence.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (std::isnan(observations[i]))
      occurrence[i] = event_occurrence::unknown;
    else if (   observations[i] >= threshold_low[i]
             && observations[i] < threshold_high[i])
      occurrence[i] = event_occurrence::yes;
    else
      occurrence[i] = event_occurrence::no;
  }
}

ensemble_probability::ensemble_probability(vec2i extents, bool initialize)
  : members_(0)
  , probabilities_{extents}
{
  if (initialize)
    reset();
}

auto ensemble_probability::reset() -> void
{
  members_ = 0;
  probabilities_.fill(0.0f);
}

auto ensemble_probability::add_member(const array2<event_occurrence>& member) -> void
{
  if (member.size() != probabilities_.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < probabilities_.size(); ++i)
    if (member.data()[i] == event_occurrence::yes)
      probabilities_.data()[i] += 1.0f;
  ++members_;
}

auto ensemble_probability::finalize() -> void
{
  for (auto i = 0; i < probabilities_.size(); ++i)
    if (probabilities_.data()[i] > 0.0f)
      probabilities_.data()[i] /= members_;
}

rank_histogram::rank_histogram(int members, bool initialize)
  : counts_{members + 1}
  // we don't need an amazing source of randomness hence use of time for seed
  , rnd_{std::mt19937::result_type(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()))}
{
  if (initialize)
    reset();
}

auto rank_histogram::reset() -> void
{
  counts_.fill(0);
}

auto rank_histogram::aggregate(rank_histogram const& other) -> void
{
  if (other.counts_.size() != counts_.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < counts_.size(); ++i)
    counts_[i] += other.counts_[i];
}

auto rank_histogram::update(span<array2f const> ensemble, array2f const& truth) -> void
{
  if (ensemble.size() != counts_.size() - 1)
    throw std::runtime_error{"Unexpected ensemble size"};

  std::uniform_int_distribution<int> uniform;

  array1f fcst(ensemble.size());
  for (auto i = 0; i < truth.size(); ++i)
  {
    auto obs = truth.data()[i];

    // skip missing observations
    if (std::isnan(obs))
      continue;

    // get the forecast value from each member
    for (auto j = 0; j < ensemble.size(); ++j)
      fcst[j] = ensemble[j].data()[i];

    // sort by forecast value (ascending)
    std::sort(fcst.begin(), fcst.end());

    // find the first member that is larger or equal to us
    auto bin = 0;
    while (bin < ensemble.size() && fcst[bin] < obs)
      ++bin;

    /* if our observation is exactly equal to one or more members the situation becomes a little complicated.
     * we should randomly assign the value between the the tied bins.  in the case of an exact match to a
     * single member (N = num tied == 0) this represents exactly falling on the bin boundary - so randomly pick
     * either bin N or N+1.  if multiple members are exactly equal, as happens when forecasting 0, then
     * pick between 0 and N + 1 and add this to the start bin rank */

    // count the number of forecasts that exactly match the observation
    auto matches = 0;
    while (bin + matches < ensemble.size() && fcst[bin + matches] == obs)
      ++matches;

    // if we exactly match one or more forecasts then randomly assign from the potentially valid bins
    if (matches > 0)
      bin += uniform(rnd_, std::uniform_int_distribution<int>::param_type{0, matches});

    // update the appropriate count bin
    ++counts_[bin];
  }
}

crps::crps(vec2i extents, bool initialize)
  : samples_{extents}
  , crps_{extents}
{
  if (initialize)
    reset();
}

auto crps::reset() -> void
{
  samples_.fill(0);
  crps_.fill(0.0f);
}

auto crps::aggregate(crps const& other) -> void
{
  if (other.samples_.shape() != samples_.shape())
    throw std::logic_error{"array size mismatch"};
  for (auto i = 0; i < samples_.size(); ++i)
  {
    auto total_samples = samples_.data()[i] + other.samples_.data()[i];
    if (total_samples > 0)
    {
      crps_.data()[i]
        = ((samples_.data()[i] * crps_.data()[i]) + (other.samples_.data()[i] * other.crps_.data()[i]))
        / total_samples;
      samples_.data()[i] = total_samples;
    }
  }
}

auto crps::update(span<array2f const> ensemble, array2f const& truth) -> void
{
  // sanity checks
  if (truth.size() != samples_.size())
    throw std::logic_error{"array size mismatch"};
  for (auto i = 0; i < ensemble.size(); ++i)
    if (ensemble[i].size() != samples_.size())
      throw std::logic_error{"array size mismatch"};

  // update each cell of the domain
  vector<float> fcst(ensemble.size());
  for (auto i = 0; i < samples_.size(); ++i)
  {
    auto obs = truth.data()[i];

    // skip missing observations
    if (std::isnan(obs))
      continue;

    // get the forecast value from each member
    for (auto j = 0; j < ensemble.size(); ++j)
      fcst[j] = ensemble[j].data()[i];

    // sort by forecast value (ascending)
    std::sort(fcst.begin(), fcst.end());

    // sum up the CRPS
    // method described in 'Ensemble Verification II' training course 2012, Martin Leutbecher
    double crps = 0.0;
    double im = 1.0 / ensemble.size();
    for (auto j = 0; j < ensemble.size() - 1; ++j)
    {
      auto x0 = fcst[j];
      auto x1 = fcst[j+1];
      auto pj = (j + 1) * im;

      if (x1 < obs)
        crps += (x1 - x0) * (pj * pj);
      else if (x0 < obs)
        crps += (obs - x0) * (pj * pj) + (x1 - obs) * ((1.0 - pj) * (1.0 - pj));
      else
        crps += (x1 - x0) * (1.0 - pj) * (1.0 - pj);
    }

    // the loop above does not account for the edge cases of the observation being lower than all members
    // so this adds the missing bits of area for these cases
    if (obs < fcst[0])
      crps += fcst[0] - obs;
    else if (fcst[ensemble.size() - 1] < obs)
      crps += obs - fcst[ensemble.size() - 1];

    // update the average CRPS for this cell
    auto s = samples_.data()[i];
    crps_.data()[i] = ((s * crps_.data()[i]) + crps) / (s + 1);
    ++samples_.data()[i];
  }
}

auto crps::domain_crps() const -> float
{
  double weighted_sum = 0.0;
  auto count = 0.0;
  for (auto i = 0; i < samples_.size(); ++i)
  {
    auto s = samples_.data()[i];
    weighted_sum += s * crps_.data()[i];
    count += s;
  }
  return weighted_sum / count;
}

spread_reliability::spread_reliability(int bins, float max_error)
  : bins_(bins)
{
  float delta = max_error / bins_.size();
  for (auto i = 0; i < bins_.size(); ++i)
  {
    auto& b = bins_[i];
    b.threshold = (i + 1) * delta;
    b.mid_point = b.threshold - (0.5f * delta);
    b.samples   = 0;
    b.sqr_spread = 0.0;
  }
}

auto spread_reliability::reset() -> void
{
  for (auto& b : bins_)
  {
    b.sqr_spread = 0;
    b.samples = 0;
  }
}

auto spread_reliability::aggregate(spread_reliability const& other) -> void
{
  if (other.bins_.size() != bins_.size())
    throw std::logic_error{"array size mismatch"};
  for (auto i = 0; i < bins_.size(); ++i)
  {
    bins_[i].samples += other.bins_[i].samples;
    bins_[i].sqr_spread += other.bins_[i].sqr_spread;
  }
}

auto spread_reliability::update(span<array2f const> ensemble, array2f const& truth) -> void
{
  // sanity checks
  for (auto i = 0; i < ensemble.size(); ++i)
    if (ensemble[i].size() != truth.size())
      throw std::logic_error{"array size mismatch"};

  // update the bins
  for (auto i = 0; i < truth.size(); ++i)
  {
    auto obs = truth.data()[i];

    // skip missing observations
    if (std::isnan(obs))
      continue;

    // determine the ensemble mean
    double ens_mean = 0.0;
    for (auto j = 0; j < ensemble.size(); ++j)
      ens_mean += ensemble[j].data()[i];
    ens_mean /= ensemble.size();;

    // determine the ensemble spread
    double ens_variance = 0.0;
    for (auto j = 0; j < ensemble.size(); ++j)
      ens_variance += (ens_mean - ensemble[j].data()[i]) * (ens_mean - ensemble[j].data()[i]);
    ens_variance /= ensemble.size();

    // determine the error of the ensemble mean
    double ens_mean_error = obs - ens_mean;

    // find the bin appropriate for this spread
    for (auto& b : bins_)
    {
      if (ens_mean_error < b.threshold)
      {
        b.samples++;
        b.sqr_spread += ens_variance;
        break;
      }
    }
  }
}

reliability_curve::reliability_curve(int bins)
  : bins_{bins}
{
  // we want the bins to be centered on the exact probabilities given by the number of members
  // the number of bins passed should be members + 1
  auto delta = 1.0f / (bins_.size() - 1);
  for (auto i = 0; i < bins_.size(); ++i)
  {
    auto& r = bins_[i];
    r.threshold = (i + 0.5f) * delta;
    r.probability = i * delta;
    r.forecasts = 0;
    r.occurrences = 0;
  }
}

auto reliability_curve::reset() -> void
{
  for (auto& r : bins_)
  {
    r.forecasts = 0;
    r.occurrences = 0;
  }
}

auto reliability_curve::aggregate(reliability_curve const& other) -> void
{
  if (other.bins_.size() != bins_.size())
    throw std::logic_error{"array size mismatch"};
  for (auto i = 0; i < bins_.size(); ++i)
  {
    bins_[i].forecasts += other.bins_[i].forecasts;
    bins_[i].occurrences += other.bins_[i].occurrences;
  }
}

auto reliability_curve::update(span<event_occurrence const> observations, span<float const> probabilities) -> void
{
  if (observations.size() != probabilities.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    auto obs = observations[i];
    if (obs == event_occurrence::unknown)
      continue;

    // determine the appropriate forecast category
    for (auto& r : bins_)
    {
      if (probabilities[i] < r.threshold)
      {
        ++r.forecasts;
        if (observations[i] == event_occurrence::yes)
          ++r.occurrences;
        break;
      }
    }
  }
}

auto reliability_curve::climatology_probability() const -> float
{
  auto forecasts = 0l, occurrences = 0l;
  for (auto& r : bins_)
  {
    forecasts += r.forecasts;
    occurrences += r.occurrences;
  }
  return double(occurrences) / forecasts;
}

roc_curve::roc_curve(int points)
  : non_occurrences_(0l)
  , occurrences_(0l)
  , points_{points}
{
  float delta = 1.0f / (points_.size() + 1);
  for (auto i = 0; i < points_.size(); ++i)
  {
    // points should be in reverse order (for graphing purposes)
    auto& r = points_[points_.size() - 1 - i];

    r.threshold = (i + 1) * delta;
    r.false_alarms = 0l;
    r.hits = 0l;
  }
}

auto roc_curve::reset() -> void
{
  non_occurrences_ = 0l;
  occurrences_ = 0l;
  for (auto& r : points_)
  {
    r.false_alarms = 0l;
    r.hits = 0l;
  }
}

auto roc_curve::aggregate(roc_curve const& other) -> void
{
  if (other.points_.size() != points_.size())
    throw std::logic_error{"array size mismatch"};

  non_occurrences_ += other.non_occurrences_;
  occurrences_ += other.occurrences_;
  for (auto i = 0; i < points_.size(); ++i)
  {
    points_[i].false_alarms += other.points_[i].false_alarms;
    points_[i].hits += other.points_[i].hits;
  }
}

auto roc_curve::update(
      span<event_occurrence const> observations
    , span<float const> probabilities
    ) -> void
{
  if (observations.size() != probabilities.size())
    throw std::logic_error{"array size mismatch"};

  for (auto i = 0; i < observations.size(); ++i)
  {
    if (observations[i] == event_occurrence::no)
    {
      // false alarm or correct rejection
      ++non_occurrences_;
      for (auto& r : points_)
        if (r.threshold < probabilities[i])
          ++r.false_alarms;
    }
    else if (observations[i] == event_occurrence::yes)
    {
      // hit or miss
      ++occurrences_;
      for (auto& r : points_)
        if (r.threshold < probabilities[i])
          ++r.hits;
    }
  }
}

auto roc_curve::area() const -> float
{
  const double rnon = non_occurrences_;
  const double rocc = occurrences_;

  auto area = 0.0;

  // setup left point as 0,0
  auto l_far = 0.0;
  auto l_pod = 0.0;

  // all points except between last and 1,1
  for (auto& r : points_)
  {
    auto r_far = double(r.false_alarms) / rnon;
    auto r_pod = double(r.hits) / rocc;
    area += (r_far - l_far) * (r_pod + l_pod) * 0.5;
    l_far = r_far;
    l_pod = r_pod;
  }

  // between last point and 1,1
  area += (1.0 - l_far) * (1.0 + l_pod) * 0.5;

  return area;
}

rmse::rmse()
  : count_{0}
  , ms_error_{0.0}
  , ms_spread_{0.0}
{ }

auto rmse::reset() -> void
{
  count_ = 0;
  ms_error_ = 0.0;
  ms_spread_ = 0.0;
}

auto rmse::aggregate(rmse const& other) -> void
{
  if (other.count_ > 0)
  {
    ms_error_ = (ms_error_ * count_ + other.ms_error_ * other.count_) / (count_ + other.count_);
    ms_spread_ = (ms_spread_ * count_ + other.ms_spread_ * other.count_) / (count_ + other.count_);
    count_ += other.count_;
  }
}

auto rmse::update(span<array2f const> ensemble, array2f const& truth) -> void
{
  // calculate the mse(ensemble mean) and ms(ensemble spread) for this ensemble
  auto count = 0l;
  auto this_ms_error = 0.0;
  auto this_ms_spread = 0.0;
  for (auto i = 0; i < truth.size(); ++i)
  {
    auto obs = truth.data()[i];

    // skip missing observations
    if (std::isnan(obs))
      continue;

    // determine the ensemble mean
    auto ens_mean = 0.0;
    for (auto j = 0; j < ensemble.size(); ++j)
      ens_mean += ensemble[j].data()[i];
    ens_mean /= ensemble.size();;

    // determine the ensemble variance
    auto ens_variance = 0.0;
    for (auto j = 0; j < ensemble.size(); ++j)
      ens_variance += (ens_mean - ensemble[j].data()[i]) * (ens_mean - ensemble[j].data()[i]);
    ens_variance /= ensemble.size();

    // aggregate results
    /* note that:
     * - RMSE term is RMS error of ensemble mean, NOT RMS error of individual members
     * - correct calculation for mean spread is square root of average ensemble variance NOT average of ensemble
     *   standard deviation
     * - See Fortin, V. et al, 2014, Why Should Ensemble Spread Match the RMSE of Ensemble Mean? */
    this_ms_error += (obs - ens_mean) * (obs - ens_mean);
    this_ms_spread += ens_variance;
    count++;
  }
  this_ms_error /= count;
  this_ms_spread /= count;

  // incorporate the result from this ensemble into our total stats
  if (count > 0)
  {
    ms_error_ = (ms_error_ * count_ + this_ms_error * count) / (count_ + count);
    ms_spread_ = (ms_spread_ * count_ + this_ms_spread * count) / (count_ + count);
    count_ += count;
  }
}

average_correlation::average_correlation()
  : count_{0}
  , mean_{0.0}
{ }

auto average_correlation::reset() -> void
{
  count_ = 0;
  mean_ = 0.0;
}

auto average_correlation::aggregate(average_correlation const& other) -> void
{
  if (other.count_ > 0)
  {
    mean_ = (mean_ * count_ + other.mean_ * other.count_) / (count_ + other.count_);
    count_ += other.count_;
  }
}

auto average_correlation::update(span<array2f const> ensemble, array2f const& truth) -> void
{
  auto count = 0l;
  auto mean = 0.0;
  for (auto& member : ensemble)
  {
    mean += correlation_ignore_nans(member, truth);
    count++;
  }
  mean /= count;

  // incorporate the result from this ensemble into our total stats
  if (count > 0)
  {
    mean_ = (mean_ * count_ + mean * count) / (count_ + count);
    count_ += count;
  }
}

// LCOV_EXCL_START
TEST_CASE("generate_occurrence_field")
{
  auto bad = array1f{5};

  auto obs = array1f{4};
  obs[0] = std::numeric_limits<float>::quiet_NaN();
  obs[1] = 10.0f;
  obs[2] = 20.0f;
  obs[3] = 30.0f;
  auto thl = array1f{4};
  thl[0] = 15.0f;
  thl[1] = 5.0f;
  thl[2] = 15.0f;
  thl[3] = 35.0f;
  auto thh = array1f{4};
  thh[0] = 25.0f;
  thh[1] = 7.0f;
  thh[2] = 35.0f;
  thh[3] = 45.0f;

  auto res = array1<event_occurrence>{4};

  SUBCASE("low_detect")
  {
    CHECK_THROWS(generate_occurrence_field_low_detect(bad, 15.0f, res));
    generate_occurrence_field_low_detect(obs, 15.0f, res);
    CHECK(res[0] == event_occurrence::unknown);
    CHECK(res[1] == event_occurrence::yes);
    CHECK(res[2] == event_occurrence::no);
    CHECK(res[3] == event_occurrence::no);
  }
  SUBCASE("high_detect")
  {
    CHECK_THROWS(generate_occurrence_field_high_detect(bad, 15.0f, res));
    generate_occurrence_field_high_detect(obs, 15.0f, res);
    CHECK(res[0] == event_occurrence::unknown);
    CHECK(res[1] == event_occurrence::no);
    CHECK(res[2] == event_occurrence::yes);
    CHECK(res[3] == event_occurrence::yes);
  }
  SUBCASE("band_detect")
  {
    CHECK_THROWS(generate_occurrence_field_band_detect(bad, 15.0f, 25.0f, res));
    generate_occurrence_field_band_detect(obs, 15.0f, 25.0f, res);
    CHECK(res[0] == event_occurrence::unknown);
    CHECK(res[1] == event_occurrence::no);
    CHECK(res[2] == event_occurrence::yes);
    CHECK(res[3] == event_occurrence::no);
  }
  SUBCASE("low_detect(array)")
  {
    CHECK_THROWS(generate_occurrence_field_low_detect(bad, thl, res));
    CHECK_THROWS(generate_occurrence_field_low_detect(obs, bad, res));
    generate_occurrence_field_low_detect(obs, thl, res);
    CHECK(res[0] == event_occurrence::unknown);
    CHECK(res[1] == event_occurrence::no);
    CHECK(res[2] == event_occurrence::no);
    CHECK(res[3] == event_occurrence::yes);
  }
  SUBCASE("high_detect(array)")
  {
    CHECK_THROWS(generate_occurrence_field_high_detect(bad, thl, res));
    CHECK_THROWS(generate_occurrence_field_high_detect(obs, bad, res));
    generate_occurrence_field_high_detect(obs, thl, res);
    CHECK(res[0] == event_occurrence::unknown);
    CHECK(res[1] == event_occurrence::yes);
    CHECK(res[2] == event_occurrence::yes);
    CHECK(res[3] == event_occurrence::no);
  }
  SUBCASE("band_detect(array)")
  {
    CHECK_THROWS(generate_occurrence_field_band_detect(bad, thl, thh, res));
    CHECK_THROWS(generate_occurrence_field_band_detect(obs, bad, thh, res));
    CHECK_THROWS(generate_occurrence_field_band_detect(obs, thl, bad, res));
    generate_occurrence_field_band_detect(obs, thl, thh, res);
    CHECK(res[0] == event_occurrence::unknown);
    CHECK(res[1] == event_occurrence::no);
    CHECK(res[2] == event_occurrence::yes);
    CHECK(res[3] == event_occurrence::no);
  }
}
TEST_CASE("rank_histogram")
{
  constexpr auto members = 10;
  auto ensemble = std::array<array2f, members>{};
  for (auto& m : ensemble)
    m.resize(1, 1);
  auto truth = array2f{1, 1};
  auto score = rank_histogram{members};

  REQUIRE(score.bin_count() == 11);
  for (auto i = 0; i < score.bin_count(); ++i)
    CHECK(score.counts()[i] == 0);

  // if we are below all members we expect all hits in the lowest bin
  score.reset();
  for (auto& m : ensemble)
    m[0][0] = 0.0f;
  truth[0][0] = -1.0f;
  for (auto i = 0; i < 10; ++i)
    score.update(ensemble, truth);
  CHECK(score.counts()[0] == 10);
  for (auto i = 1; i < score.bin_count(); ++i)
    CHECK(score.counts()[i] == 0);

  // if we are above all members we expect all hits in the highest bin
  score.reset();
  for (auto& m : ensemble)
    m[0][0] = 0.0f;
  truth[0][0] = 1.0f;
  for (auto i = 0; i < 10; ++i)
    score.update(ensemble, truth);
  for (auto i = 0; i < score.bin_count() - 1; ++i)
    CHECK(score.counts()[i] == 0);
  CHECK(score.counts()[score.bin_count() - 1] == 10);

  // if we are equal to all members we expect hits to be evenly distributed
  score.reset();
  for (auto& m : ensemble)
    m[0][0] = 0.0f;
  truth[0][0] = 0.0f;
  for (auto i = 0; i < 1000; ++i)
    score.update(ensemble, truth);
  auto sum = 0;
  for (auto i = 0; i < score.bin_count(); ++i)
  {
    CHECK(score.counts()[i] > 0.5 * 1000 / score.bin_count());
    CHECK(score.counts()[i] < 2.0 * 1000 / score.bin_count());
    sum += score.counts()[i];
  }
  CHECK(sum == 1000);

  // perfect linear ramp, with matching truth - one hit per bin
  score.reset();
  for (auto i = 0; i < members; ++i)
    ensemble[i][0][0] = members - i - 1;
  for (auto i = 0; i < 11; ++i)
  {
    truth[0][0] = i - 0.5f;
    score.update(ensemble, truth);
  }
  for (auto i = 0; i < score.bin_count(); ++i)
    CHECK(score.counts()[i] == 1);

  // exactly matches one member - expect 50/50 split on bin either side
  score.reset();
  for (auto i = 0; i < members; ++i)
    ensemble[i][0][0] = i;
  truth[0][0] = 5.0f;
  for (auto i = 0; i < 1000; ++i)
    score.update(ensemble, truth);
  CHECK(score.counts()[5] + score.counts()[6] == 1000);
  CHECK(score.counts()[5] > 350);
  CHECK(score.counts()[5] < 650);

  // exactly matches two members - expect 33/33/33 split
  score.reset();
  for (auto i = 0; i < members; ++i)
    ensemble[i][0][0] = i;
  ensemble[1][0][0] = 5.0f;
  truth[0][0] = 5.0f;
  for (auto i = 0; i < 1000; ++i)
    score.update(ensemble, truth);
  CHECK(score.counts()[4] + score.counts()[5] + score.counts()[6] == 1000);
  CHECK(score.counts()[4] > 270);
  CHECK(score.counts()[4] < 400);
  CHECK(score.counts()[5] > 270);
  CHECK(score.counts()[5] < 400);
}
// LCOV_EXCL_STOP
