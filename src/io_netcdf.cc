/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "io_netcdf.h"
#include "array1.h"
#include "cf.h"
#include "compression.h"
#include "products.h"
#include "trace.h"
#include "unit.h"
#include "unit_test.h"
#include "version.h"
#if STEPS_WITH_AWS
#include "aws.h"
#endif

using namespace steps;

// mutex used to protect NetCDF and HDF5 API calls which are not thread safe
static std::mutex mut_netcdf_;

auto steps::nc::lock_mutex() -> std::unique_lock<std::mutex>
{
  return std::unique_lock<std::mutex>{mut_netcdf_};
}

auto nc::write_metadata_as_attributes(metadata const& meta, nc::file& file) -> void
{
  for (auto& md : meta)
  {
    // we ignore attributes starting with a period.  this allows us to create 'special' attributes such as those used
    // to specify the reference file path and variable name needed to copy projection metadata
    if (md.first.c_str()[0] == '.')
      continue;

    // time_point is not directly supported by our netcdf API so convert it to a string first
    std::visit([&](auto const& val)
    {
      using T = std::decay_t<decltype(val)>;
      if constexpr (std::is_same_v<T, time_point>)
        file.att_set(md.first, format("{}", val));
      else
        file.att_set(md.first, val);
    }, md.second);
  }
}

inline static auto load_netcdf(string_view path, bool optional) -> shared_ptr<nc::file const>
{
  if (optional && !std::filesystem::exists(path))
    return nullptr;

  auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
  return std::make_shared<nc::file const>(string(path), nc::io_mode::read_only);
}

// cache of reference files that are currently opened
/* Because of this list, it is critical that we call release_reference_files() at the end of main().  This ensures that
 * the files are closed before we exit main (assuming the user hasn't copied the returned shared_ptr into a global
 * variable).  This is important because it's not possible to ensure that this list is destroyed before the NetCDF/HDF5
 * APIs are shutdown. */
static std::mutex mut_reference_;
static list<shared_ptr<nc::file const>> reference_files_;

auto steps::nc::release_reference_files() -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_reference_};
  reference_files_.clear();
}

auto steps::nc::get_reference_file(string_view path, bool cached_only) -> shared_ptr<nc::file const>
try
{
  auto lock = std::lock_guard<std::mutex>{mut_reference_};

  // see if we already have this file open
  for (auto& ptr : reference_files_)
    if (ptr->path() == path)
      return ptr;

  // bail out if the user is only interested in pre-cached files
  if (cached_only)
    return nullptr;

  // no record of this file so we need to actually open it
  auto ptr = shared_ptr<nc::file const>{};
  switch (determine_uri_scheme(path))
  {
  case uri_scheme::none:
    ptr = load_netcdf(path, false);
    break;
  case uri_scheme::file:
    ptr = load_netcdf(strip_file_schema(path), false);
    break;
  case uri_scheme::s3:
#if STEPS_WITH_AWS
    ptr = aws::load_netcdf_s3(path, false);
    break;
#else
    throw std::runtime_error{"AWS support disabled"};
#endif
  }

  // store a copy in our cache of reference files
  reference_files_.push_back(ptr);

  return ptr;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to open reference file {}", path)});
}

auto steps::nc::get_grid_reference(metadata const& meta) -> grid_reference
try
{
  if (auto i = meta.find(".reference-file"); i != meta.end())
    return {get_reference_file(std::get<string>(i->second)), std::get<string>(meta.at(".reference-variable"))};
  return {};
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{"Failed to load NetCDF projection reference file"});
}

auto steps::nc::create_spatial_dimensions(
      nc::file& file
    , vec2i grid_shape
    , grid_reference const& ref
    ) -> std::tuple<nc::dimension*, nc::dimension*, optional<string>>
{
  auto ret = std::tuple<nc::dimension*, nc::dimension*, optional<string>>{};

  if (!ref.first)
  {
    std::get<0>(ret) = &file.create_dimension("y", grid_shape.y);
    std::get<1>(ret) = &file.create_dimension("x", grid_shape.x);
  }
  else
  {
    auto& var = ref.first->lookup_variable(ref.second);
    if (var.dimensions().size() < 2)
      throw std::runtime_error{"Reference grid variable must be rank 2 or higher"};
    if (   var.dimensions()[var.dimensions().size() - 2]->size() != grid_shape.y
        || var.dimensions()[var.dimensions().size() - 1]->size() != grid_shape.x)
      throw std::runtime_error{"Reference grid variable size mismatch"};

    std::get<0>(ret) = &cf::copy_dimension_and_coordinates(*var.dimensions()[var.dimensions().size() - 2], file);
    std::get<1>(ret) = &cf::copy_dimension_and_coordinates(*var.dimensions()[var.dimensions().size() - 1], file);
    std::get<2>(ret) = var.att_get_as<optional<string>>("grid_mapping");
    if (std::get<2>(ret))
      file.copy_variable(ref.first->lookup_variable(*std::get<2>(ret)));
  }

  return ret;
}

input_netcdf::input_netcdf(configuration const& config)
  : variable_(config["variable"])
  , units_str_{config.optional("units_override", "")}
  , var_duration_{config.optional("duration_variable", "")}
  , var_from_{config.optional("accumulation_start_variable", "")}
  , var_till_{config.optional("accumulation_end_variable", "")}
  , xy0_{config.optional("x0", -1), config.optional("y0", -1)}
  , max_cur_files_{config.optional("max_open_files", 1)}
  , units_setup_{false}
{
  // sanity check
  if (var_from_.empty() != var_till_.empty())
    throw std::runtime_error{"Must specify both accumulation_start_variable and accumulation_end_variable (or neither)"};
  if ((xy0_.x == -1) != (xy0_.y == -1))
    throw std::runtime_error{"Must specify both x0 and y0 (or neither)"};
}

input_netcdf::~input_netcdf()
{
  // we must do this explicitly to ensure the netcdf mutex is locked while closing the file
  auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
  cur_files_.clear();
}

auto input_netcdf::read(time_point reference_time, int member, time_point time, array2f& data) -> bool
{
  auto path = determine_path(reference_time, member, time);
  auto lock = std::lock_guard<std::mutex>(mut_file_);
  return read_impl(path, member, time, data);
}

auto input_netcdf::read(time_point reference_time, span<int> members, time_point time, span<array2f> data) -> bool
{
  if (members.size() != data.size())
    throw std::runtime_error{"Array size mismatch"};

  /* In this variation we lock the input during the whole series of reads.  This prevents any other threads from
   * sneaking in and changing the active file which is not an error but will cause us to constantly open and index new
   * files if the requests become interleaved.  If we someday keep multiple files open at once this would become a
   * non issue. */
  auto lock = std::lock_guard<std::mutex>(mut_file_);

  for (auto i = 0; i < members.size(); ++i)
  {
    auto path = determine_path(reference_time, members[i], time);
    if (!read_impl(path, members[i], time, data[i]))
    {
      if (i == 0)
        return false;
      else
        throw std::runtime_error{format("Failed to locate member {}", members[i])};
    }
  }

  return true;
}

auto input_netcdf::read_impl(string_view path, int member, time_point time, array2f& data) -> bool
try
{
  // do we need to open a new file?
  auto ifile = std::find_if(cur_files_.begin(), cur_files_.end(), [&](auto& af){ return af.file->path() == path; });
  if (ifile == cur_files_.end())
  {
    // check if we already have the file open as a reference file, and if not try to open it ourselves
    // make sure netcdf mutex is NOT locked during these call (call itself will lock)
    auto file = nc::get_reference_file(path, true);
    if (!file)
    {
      file = open_file(path);
      if (!file)
        return false;
    }

    // lock the netcdf mutex
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};

    // enforce the limit on open files
    // make sure netcdf mutx IS locked during this call since destruction of nc::file may occur
    if (int(cur_files_.size()) >= max_cur_files_)
      cur_files_.pop_back();

    // index the file as our current file
    cur_files_.emplace_front(std::move(file), variable_);

    // ensure that our unit conversions are setup
    if (!units_setup_)
      setup_unit_xform(cur_files_.front());

    if (!cur_files_.front().read(member, time, xy0_, data))
      return false;
  }
  else
  {
    // move this file to the front of the queue (as most recently used)
    cur_files_.splice(cur_files_.begin(), cur_files_, ifile);

    // read the field
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    if (!ifile->read(member, time, xy0_, data))
      return false;
  }

  // perform any required unit transformation
  if (unit_xform_)
    (*unit_xform_)(data);

  return true;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to read file '{}'", path)});
}

// assumes that netcdf mutex is already locked!
input_netcdf::active_file::active_file(shared_ptr<nc::file const> f, string_view var_name)
  : file{std::move(f)}
  , var{&file->lookup_variable(var_name)}
{
  // for now we just assume that a 3d variable is [time][y][x] and a 4d is [member][time][y][x]
  if (var->dimensions().size() == 2)
  {
    // nothing to do
  }
  else if (var->dimensions().size() == 3)
  {
    times.resize(var->dimensions()[0]->size());
    cf::read_times(cf::lookup_coordinates(*var->dimensions()[0]), times);
  }
  else if (var->dimensions().size() == 4)
  {
    members.resize(var->dimensions()[0]->size());
    cf::packer{}.read(cf::lookup_coordinates(*var->dimensions()[0]), members);

    times.resize(var->dimensions()[1]->size());
    cf::read_times(cf::lookup_coordinates(*var->dimensions()[1]), times);
  }
  else
    throw std::runtime_error{format("Unexpected variable rank {}, should be between 2 and 4", var->dimensions().size())};
}

// assumes that netcdf mutex is already locked!
auto input_netcdf::active_file::read(int member, time_point time, vec2i xy0, array2f& data) -> bool
{
  if (var->dimensions().size() == 2)
  {
    if (xy0.x != -1)
      cf::packer{}.read(
            *var
          , data
          , std::numeric_limits<float>::quiet_NaN()
          , { xy0.y, xy0.x }
          , { data.shape().y, data.shape().x });
    else
      cf::packer{}.read(
            *var
          , data
          , std::numeric_limits<float>::quiet_NaN());
  }
  else if (var->dimensions().size() == 3)
  {
    // locate the appropriate time index
    auto itime = std::lower_bound(times.begin(), times.end(), time);
    if (itime == times.end() || *itime != time)
      return false;

    if (xy0.x != -1)
      cf::packer{}.read(
            *var
          , data
          , std::numeric_limits<float>::quiet_NaN()
          , { int(itime - times.begin()), xy0.y, xy0.x }
          , { 1, data.shape().y, data.shape().x });
    else
      cf::packer{}.read(
            *var
          , data
          , std::numeric_limits<float>::quiet_NaN()
          , { int(itime - times.begin()) });
  }
  else if (var->dimensions().size() == 4)
  {
    // locate the appropriate member index
    auto imember = std::lower_bound(members.begin(), members.end(), member);
    if (imember == members.end() || *imember != member)
      return false;

    // locate the appropriate time index
    auto itime = std::lower_bound(times.begin(), times.end(), time);
    if (itime == times.end() || *itime != time)
      return false;

    if (xy0.x != -1)
      cf::packer{}.read(
            *var
          , data
          , std::numeric_limits<float>::quiet_NaN()
          , { int(imember - members.begin()), int(itime - times.begin()), xy0.y, xy0.x }
          , { 1, 1, data.shape().y, data.shape().x });
    else
      cf::packer{}.read(
            *var
          , data
          , std::numeric_limits<float>::quiet_NaN()
          , { int(imember - members.begin()), int(itime - times.begin()) });
  }
  return true;
}

// this function assumes you have already locked the netcdf mutex
// TODO this technically breaks if units ever change between files (including between different runs using same core)
auto input_netcdf::setup_unit_xform(active_file const& file) -> void
try
{
  units_setup_ = false;

  // our target unit (CF compliant version of mm/hr)
  auto unit_mmh = unit{"kg m-2 h-1"s};

  // create udunits objects while converting any 'mm' which is non-standard to 'kg m-2' which is udunits compatible
  if (units_str_.empty())
    units_str_ = replace_all(file.var->att_get_as<string>("units"), "mm", "kg m-2");

  // are we already in units of mm/hr?
  if (unit{units_str_} == unit_mmh)
  {
    units_setup_ = true;
    return;
  }

  // check if we have an explicit duration
  if (!var_duration_.empty())
  {
    auto& vdur = file.file->lookup_variable(var_duration_);
    double dur_val;
    vdur.read(span{&dur_val, 1});
    units_str_ = format("({}) / ({} {})", units_str_, dur_val, vdur.att_get_as<string>("units"));
  }

  // check if we have start and end time variables which we can use to calculate the duration
  if (!var_from_.empty() && !var_till_.empty())
  {
    auto& vfrom = file.file->lookup_variable(var_from_);
    auto& vtill = file.file->lookup_variable(var_till_);
    auto start = 0;
    auto from = vfrom.dimensions().size() == 0 ? cf::read_time(vfrom) : cf::read_time(vfrom, span{&start, 1});
    auto till = vtill.dimensions().size() == 0 ? cf::read_time(vtill) : cf::read_time(vtill, span{&start, 1});
    units_str_ = format("({}) / ({} seconds)", units_str_, std::chrono::duration_cast<std::chrono::seconds>(till - from).count());
  }

  // build a transform to our unit
  unit_xform_.emplace(unit{units_str_}, unit_mmh);

  // trace about it for posterity
  trace(trace_level::log, "Determined input unit to mm/h scale factor of {}", (*unit_xform_)(1.0f));

  units_setup_ = true;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{"Failed to determine unit transform needed to convert data to mm/hr"});
}

input_netcdf_local::input_netcdf_local(configuration const& config, metadata const& user_parameters)
  : input_netcdf{config}
  , user_parameters_{&user_parameters}
  , path_format_(strip_file_schema(config["path"].string()))
{ }

auto input_netcdf_local::determine_path(time_point reference_time, int member, time_point time) const -> string
try
{
  auto lead = time - reference_time;
  auto lead_hr = std::chrono::duration_cast<std::chrono::hours>(lead);
  auto lead_min = std::chrono::duration_cast<std::chrono::minutes>(lead - lead_hr);
  return format(
        path_format_
      , "param"_a = *user_parameters_
      , "member"_a = member
      , "time"_a = time
      , "reference"_a = reference_time
      , "lead"_a = lead
      , "lead_hr"_a = lead_hr.count()
      , "lead_min"_a = lead_min.count()
      );
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Path format '{}' is invalid", path_format_)});
}

auto input_netcdf_local::open_file(string_view path) -> shared_ptr<nc::file const>
{
  return load_netcdf(path, true);
}

static auto compress_write_hdf5(hid_t varid, int level, span<char const> data, span<hsize_t const> offset) -> void
{
  auto filter_mask = uint32_t{0};

  if (level == nc::uncompressed)
  {
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    if (H5DOwrite_chunk(varid, H5P_DEFAULT, filter_mask, offset.data(), data.size(), data.data()) < 0)
      throw std::runtime_error{"H5DOwrite_chunk failed"};
  }
  else
  {
    // compress the chunk the same way the HDF5 library does internally
    auto cdata = zlib_compress(data, level);

    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    if (H5DOwrite_chunk(varid, H5P_DEFAULT, filter_mask, offset.data(), cdata.second, cdata.first.data()) < 0)
      throw std::runtime_error{"H5DOwrite_chunk failed"};
  }
}

// write multiple copies of the same chunk
// used when outputting null forecast products
static auto compress_write_hdf5_multi(hid_t varid, int level, span<char const> data, span<hsize_t> offset, span<int const> counts) -> void
{
  auto filter_mask = uint32_t{0};

  if (level == nc::uncompressed)
  {
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    while (true)
    {
      // write the chunk
      if (H5DOwrite_chunk(varid, H5P_DEFAULT, filter_mask, offset.data(), data.size(), data.data()) < 0)
        throw std::runtime_error{"H5DOwrite_chunk failed"};

      // increment offset to next chunk
      auto d = 0;
      while (d < counts.size())
      {
        if (hssize_t(offset[d]) < counts[d] - 1)
        {
          ++offset[d];
          break;
        }
        offset[d++] = 0;
      }
      if (d == counts.size())
        break;
    }
  }
  else
  {
    // compress the chunk the same way the HDF5 library does internally
    auto cdata = zlib_compress(data, level);

    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    while (true)
    {
      // write the chunk
      if (H5DOwrite_chunk(varid, H5P_DEFAULT, filter_mask, offset.data(), cdata.second, cdata.first.data()) < 0)
        throw std::runtime_error{"H5DOwrite_chunk failed"};

      // increment offset to next chunk
      auto d = 0;
      while (d < counts.size())
      {
        if (hssize_t(offset[d]) < counts[d] - 1)
        {
          ++offset[d];
          break;
        }
        offset[d++] = 0;
      }
      if (d == counts.size())
        break;
    }
  }
}

template <typename T>
static auto pack_cf_data(float scale_factor, float add_offset, T fill_value, span<float const> data)
{
  auto buf = array1<char>{int(data.size() * sizeof(T))};
  auto out = reinterpret_cast<T*>(buf.data());
  if constexpr (std::is_integral_v<T>)
  {
    for (auto i = 0; i < data.size(); ++i)
      out[i] = std::isnan(data[i]) ? fill_value : T(std::lround((data[i] - add_offset) / scale_factor));
  }
  else
  {
    for (auto i = 0; i < data.size(); ++i)
      out[i] = std::isnan(data[i]) ? fill_value : (data[i] - add_offset) / scale_factor;
  }
  return buf;
}

static auto pack_cf_data_as(nc::data_type type, float scale_factor, float add_offset, double fill_value, span<float const> data)
{
  switch (type)
  {
  case nc::data_type::i8:  return pack_cf_data<int8_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::u8:  return pack_cf_data<uint8_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::i16: return pack_cf_data<int16_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::u16: return pack_cf_data<uint16_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::i32: return pack_cf_data<int32_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::u32: return pack_cf_data<uint32_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::i64: return pack_cf_data<int64_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::u64: return pack_cf_data<uint64_t>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::f32: return pack_cf_data<float>(scale_factor, add_offset, fill_value, data);
  case nc::data_type::f64: return pack_cf_data<double>(scale_factor, add_offset, fill_value, data);
  default:
    throw std::logic_error{"Unsupported NetCDF type for packing"};
  }
}

static auto determine_series_name(product_series_type type, metadata const& meta) -> string
{
  switch (type)
  {
  case product_series_type::none:
    return "";
  case product_series_type::member:
    return "member";
  case product_series_type::probability:
    if (auto i = meta.find(".th-name"); i != meta.end())
      return std::get<string>(i->second);
    return "threshold";
  case product_series_type::percentile:
    return "percentile";
  }
  throw std::logic_error{"Unsupported product series type"};
}

static auto default_variable_name(product_units units) -> char const*
{
  switch (units)
  {
  case product_units::rate:
    return "rain_rate";
  case product_units::accumulation:
    return "precipitation";
  case product_units::probability:
    return "probability";
  }
  throw std::logic_error{"Unsupported product unit"};
}

output_netcdf::output_netcdf(configuration const& config)
  : name_time_ref_(config.optional("variable_reference_time", "reference_time"))
  , name_time_(config.optional("dimension_time", "time"))
  , name_time_start_(config.optional("variable_start_time", "start_time"))
  , name_variable_{config.optional("variable", "")}
  , data_type_{config.optional("data_type", nc::data_type::i16)}
  , compression_{zlib_parse_level(config.optional("compression", "5"))}
  , scale_factor_{config.optional("scale_factor", 0.01f)}
  , add_offset_{config.optional("add_offset", 0.0f)}
  , fill_value_{config.optional("fill_value", -1.0f)}
  , direct_hdf5_{config.optional("direct_hdf5", true)}
  , flatten_series_{config.optional("flatten_series", false)}
  , flatten_time_{config.optional("flatten_time", false)}
{ }

output_netcdf::~output_netcdf()
{
  // manually close here because we need the mutex to be locked while nc_close or H5Idec_ref is called
  close_file();
}

auto output_netcdf::initialize(product const& parent, time_point reference_time) -> void
{
  // open the file (make sure netcdf mutex is NOT locked during this call since the call itself uses the mutex)
  auto file_nc = std::optional<nc::file>{create_file(reference_time, [&]
  {
    /* for ensemble based products we supply the first member number to create_file.  this allows it to be used in file
     * names to prevent collisions if the user is forecasting members in batches. */
    auto labels = parent.series_labels();
    return labels && labels->size() > 0 && parent.series_type() == product_series_type::member ? int((*labels)[0]) : -1;
  }())};

  // grab the grid reference file if any (make sure netcdf mutex is NOT locked during this call)
  auto projection = nc::get_grid_reference(parent.output_metadata());

  // lock the netcdf mutex
  auto lock = std::lock_guard<std::mutex>{mut_netcdf_};

  auto& meta = parent.output_metadata();

  // setup global attributes
  file_nc->att_set("Conventions", "CF-1.7");
  file_nc->att_set("source", format("STEPS {} ({})", steps_version(), steps_source_ref()));
  file_nc->att_set("history", format("{:%a %b %d %H:%M:%S %Y}: {}", clock::now(), get_command_line_string()));
  write_metadata_as_attributes(meta, *file_nc);

  // probability products output integers and use a special metadata to indicate their scaling
  // this is an optimisation that prevents us from having to calculate floats then immediately convert back to ints
  expect_i8_ = parent.units() == product_units::probability;

  skip_d0_ = false;
  skip_d1_ = false;
  ndims_ = 0;

  // setup dimensions
  nc::dimension* dims[4];
  size_t chunks[4];
  optional<string> grid_mapping_name;
  if (auto labels = parent.series_labels())
  {
    auto series_type = parent.series_type();
    auto series_name = determine_series_name(series_type, meta);
    auto var = static_cast<nc::variable*>(nullptr);
    if (labels->size() == 1 && flatten_series_)
    {
      var = &file_nc->create_variable(
            series_name
          , series_type == product_series_type::member ? nc::data_type::i32 : nc::data_type::f32);
      skip_d0_ = true;
    }
    else
    {
      dimlens_[ndims_] = labels->size();
      dims[ndims_] = &file_nc->create_dimension(series_name, dimlens_[ndims_]);
      var = &file_nc->create_variable(
            series_name
          , series_type == product_series_type::member ? nc::data_type::i32 : nc::data_type::f32
          , span{&dims[ndims_], 1});
      chunks[ndims_] = 1;
      ++ndims_;
    }
    switch (series_type)
    {
    case product_series_type::none:
      break;
    case product_series_type::member:
      var->att_set("standard_name", "realization");
      var->att_set("units", "1");
      var->att_set("long_name", "Ensemble member");
      break;
    case product_series_type::probability:
      if (meta.find(".th-name") != meta.end())
      {
        if (auto i = meta.find(".th-standard_name"); i != meta.end())
          var->att_set("standard_name", std::get<string>(i->second));
        if (auto i = meta.find(".th-units"); i != meta.end())
          var->att_set("units", std::get<string>(i->second));
        if (auto i = meta.find(".th-long_name"); i != meta.end())
          var->att_set("long_name", std::get<string>(i->second));
      }
      else if (parent.forecast_lengths())
      {
        var->att_set("standard_name", "precipitation_amount");
        var->att_set("units", "kg m-2");
        var->att_set("long_name", "Accumulation threshold");
      }
      else
      {
        var->att_set("standard_name", "rainfall_rate");
        var->att_set("units", "kg m-2 h-1");
        var->att_set("long_name", "Rain rate threshold");
      }
      break;
    case product_series_type::percentile:
      var->att_set("units", "percent");
      var->att_set("long_name", "Percentile");
      break;
    }
    var->write(*labels);
  }
  if (auto times = parent.forecast_times())
  {
    auto lengths = parent.forecast_lengths();
    auto var_time = static_cast<nc::variable*>(nullptr);
    auto var_start = static_cast<nc::variable*>(nullptr);
    if (times->size() == 1 && flatten_time_)
    {
      var_time = &file_nc->create_variable(name_time_, nc::data_type::i64);
      if (lengths)
        var_start = &file_nc->create_variable(name_time_start_, nc::data_type::i64);
      if (ndims_ == 1 || skip_d0_)
        skip_d1_ = true;
      else
        skip_d0_ = true;
    }
    else
    {
      dimlens_[ndims_] = times->size();
      dims[ndims_] = &file_nc->create_dimension(name_time_, dimlens_[ndims_]);
      var_time = &file_nc->create_variable(name_time_, nc::data_type::i64, span{&dims[ndims_], 1});
      if (lengths)
        var_start = &file_nc->create_variable(name_time_start_, nc::data_type::i64, span{&dims[ndims_], 1});
      chunks[ndims_] = 1;
      ++ndims_;
    }
    auto buffer = array1<long>{times->size()};
    auto time_units = format("seconds since {:%F %T UTC}", reference_time);
    {
      var_time->att_set("standard_name", "time");
      var_time->att_set("units", time_units);
      var_time->att_set("long_name", lengths ? "Accumulation forecast end time" : "Forecast time");
      for (int i = 0; i < times->size(); ++i)
        buffer[i] = std::chrono::duration_cast<std::chrono::seconds>((*times)[i]).count();
      var_time->write(buffer);
    }
    if (lengths)
    {
      var_start->att_set("units", time_units);
      var_start->att_set("long_name", "Accumulation forecast start time");
      for (int i = 0; i < times->size(); ++i)
        buffer[i] = std::chrono::duration_cast<std::chrono::seconds>((*times)[i] - (*lengths)[i]).count();
      var_start->write(buffer);
    }
    {
      auto& var = file_nc->create_variable(name_time_ref_, nc::data_type::i64);
      var.att_set("standard_name", "forecast_reference_time");
      var.att_set("units", time_units);
      var.att_set("long_name", "Forecast reference time");
      auto val = int64_t{0};
      var.write(span{&val, 1});
    }
  }
  {
    auto grid_shape = parent.grid_shape();
    dimlens_[ndims_+0] = grid_shape.y;
    dimlens_[ndims_+1] = grid_shape.x;
    std::tie(dims[ndims_+0], dims[ndims_+1], grid_mapping_name) = create_spatial_dimensions(*file_nc, grid_shape, projection);
    chunks[ndims_+0] = dimlens_[ndims_+0];
    chunks[ndims_+1] = dimlens_[ndims_+1];
    ndims_ += 2;
  }

  // create main variable
  auto const& var_name = !name_variable_.empty() ? name_variable_ : default_variable_name(parent.units());
  auto var_nc = &file_nc->create_variable(
        var_name
      , expect_i8_ ? nc::data_type::i8 : data_type_
      , span{dims, ndims_}
      , span{chunks, ndims_}
      , compression_);

  // attributes for main variable
  switch (parent.units())
  {
  case product_units::rate:
    var_nc->att_set("standard_name", "rainfall_rate");
    var_nc->att_set("units", "kg m-2 h-1");
    var_nc->att_set("long_name", "Rain rate forecast");
    break;
  case product_units::accumulation:
    var_nc->att_set("standard_name", "precipitation_amount");
    var_nc->att_set("units", "kg m-2");
    var_nc->att_set("long_name", "Accumulation forecast");
    break;
  case product_units::probability:
    var_nc->att_set("units", "1");
    var_nc->att_set("long_name", "Probability of exceeding precipitation threshold");
    break;
  }
  if (grid_mapping_name)
    var_nc->att_set("grid_mapping", *grid_mapping_name);

  // setup packing and fill values
  if (expect_i8_)
  {
    var_nc->att_set("scale_factor", std::get<float>(meta.at(".prob-scale_factor")));
    var_nc->att_set("_FillValue", int8_t(-1));
  }
  else
  {
    if (data_type_ != nc::data_type::f32 && data_type_ != nc::data_type::f64)
    {
      if (scale_factor_ != 1.0f)
        var_nc->att_set("scale_factor", scale_factor_);
      if (add_offset_ != 0.0f)
        var_nc->att_set("add_offset", add_offset_);
    }

    switch (data_type_)
    {
    case nc::data_type::i8:  var_nc->att_set("_FillValue", int8_t(fill_value_)); break;
    case nc::data_type::u8:  var_nc->att_set("_FillValue", uint8_t(fill_value_)); break;
    case nc::data_type::i16: var_nc->att_set("_FillValue", int16_t(fill_value_)); break;
    case nc::data_type::u16: var_nc->att_set("_FillValue", uint16_t(fill_value_)); break;
    case nc::data_type::i32: var_nc->att_set("_FillValue", int32_t(fill_value_)); break;
    case nc::data_type::u32: var_nc->att_set("_FillValue", uint32_t(fill_value_)); break;
    case nc::data_type::i64: var_nc->att_set("_FillValue", int64_t(fill_value_)); break;
    case nc::data_type::u64: var_nc->att_set("_FillValue", uint64_t(fill_value_)); break;
    case nc::data_type::f32: var_nc->att_set("_FillValue", float(fill_value_)); break;
    case nc::data_type::f64: var_nc->att_set("_FillValue", double(fill_value_)); break;
    default:
      throw std::logic_error{"Unreachable"};
    }
  }

  // if we are intending to compress internally we must close the NetCDF API handle and repoen with HDF5
  auto file_h5 = nc::hdf_handle{}, var_h5 = nc::hdf_handle{};
  if (direct_hdf5_)
  {
    auto path = file_nc->path();

    // close the native NetCDF handles
    var_nc = nullptr;
    file_nc.reset();

    // open as an HDF5 file and retrieve variable handles
    file_h5.reset(H5Fopen(path.c_str(), H5F_ACC_RDWR, H5P_DEFAULT));
    if (file_h5.get() < 0)
      throw std::runtime_error{"Failed to reopen with HDF5 API"};
    var_h5.reset(H5Dopen(file_h5, var_name.c_str(), H5P_DEFAULT));
    if (var_h5.get() < 0)
      throw std::runtime_error{"Failed to open variable"};
  }

  // success - commit our changes now that no exceptions are possible
  file_nc_ = std::move(file_nc);
  var_nc_ = var_nc;
  file_h5_ = std::move(file_h5);
  var_h5_ = std::move(var_h5);
}

auto output_netcdf::write(array2f const& data, int d0, int d1) -> void
{
  if (expect_i8_)
    throw std::runtime_error{"Unexpected data format in write() call for product type"};

  if (skip_d1_)
    d1 = -1;
  if (skip_d0_)
  {
    d0 = d1;
    d1 = -1;
  }

  if (direct_hdf5_)
  {
    auto packed = pack_cf_data_as(data_type_, scale_factor_, add_offset_, fill_value_, data);
    if (d1 != -1)
      compress_write_hdf5(var_h5_, compression_, packed, std::array<hsize_t, 4>{hsize_t(d0), hsize_t(d1), 0, 0});
    else if (d0 != -1)
      compress_write_hdf5(var_h5_, compression_, packed, std::array<hsize_t, 3>{hsize_t(d0), 0, 0});
    else
      compress_write_hdf5(var_h5_, compression_, packed, std::array<hsize_t, 2>{0, 0});
  }
  else
  {
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    if (d1 != -1)
      cf::packer{}.write(*var_nc_, data, {d0, d1});
    else if (d0 != -1)
      cf::packer{}.write(*var_nc_, data, {d0});
    else
      cf::packer{}.write(*var_nc_, data);
  }
}

auto output_netcdf::write(array2<int8_t> const& data, int d0, int d1) -> void
{
  if (!expect_i8_)
    throw std::runtime_error{"Unexpected data format in write() call for product type"};

  if (skip_d1_)
    d1 = -1;
  if (skip_d0_)
  {
    d0 = d1;
    d1 = -1;
  }

  if (direct_hdf5_)
  {
    auto packed = span<char const>{reinterpret_cast<char const*>(data.data()), data.size_bytes()};
    if (d1 != -1)
      compress_write_hdf5(var_h5_, compression_, packed, std::array<hsize_t, 4>{hsize_t(d0), hsize_t(d1), 0, 0});
    else if (d0 != -1)
      compress_write_hdf5(var_h5_, compression_, packed, std::array<hsize_t, 3>{hsize_t(d0), 0, 0});
    else
      compress_write_hdf5(var_h5_, compression_, packed, std::array<hsize_t, 2>{0, 0});
  }
  else
  {
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    if (d1 != -1)
      var_nc_->write(data, {d0, d1});
    else if (d0 != -1)
      var_nc_->write(data, {d0});
    else
      var_nc_->write(data);
  }
}

auto output_netcdf::write_all(array2f const& data) -> void
{
  if (expect_i8_)
    throw std::runtime_error{"Unexpected data format in write() call for product type"};

  if (direct_hdf5_)
  {
    auto packed = pack_cf_data_as(data_type_, scale_factor_, add_offset_, fill_value_, data);
    hsize_t offset[] = { 0, 0, 0, 0 };
    if (ndims_ == 4)
      compress_write_hdf5_multi(var_h5_, compression_, packed, span{offset, 4}, std::array<int, 2>{dimlens_[0], dimlens_[1]});
    else if (ndims_ == 3)
      compress_write_hdf5_multi(var_h5_, compression_, packed, span{offset, 3}, std::array<int, 1>{dimlens_[0]});
    else
      compress_write_hdf5_multi(var_h5_, compression_, packed, span{offset, 2}, std::array<int, 0>{});
  }
  else
  {
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    auto packer = cf::packer{};
    if (ndims_ == 4)
    {
      for (auto d0 = 0; d0 < dimlens_[0]; ++d0)
        for (auto d1 = 0; d1 < dimlens_[1]; ++d1)
          packer.write(*var_nc_, data, {d0, d1});
    }
    else if (ndims_ == 3)
    {
      for (auto d0 = 0; d0 < dimlens_[0]; ++d0)
        packer.write(*var_nc_, data, {d0});
    }
    else
    {
      packer.write(*var_nc_, data);
    }
  }
}

auto output_netcdf::write_all(array2<int8_t> const& data) -> void
{
  if (!expect_i8_)
    throw std::runtime_error{"Unexpected data format in write() call for product type"};

  if (direct_hdf5_)
  {
    auto packed = span<char const>{reinterpret_cast<char const*>(data.data()), data.size_bytes()};
    hsize_t offset[] = { 0, 0, 0, 0 };
    if (ndims_ == 4)
      compress_write_hdf5_multi(var_h5_, compression_, packed, span{offset, 4}, std::array<int, 2>{dimlens_[0], dimlens_[1]});
    else if (ndims_ == 3)
      compress_write_hdf5_multi(var_h5_, compression_, packed, span{offset, 3}, std::array<int, 1>{dimlens_[0]});
    else
      compress_write_hdf5_multi(var_h5_, compression_, packed, span{offset, 2}, std::array<int, 0>{});
  }
  else
  {
    auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
    if (ndims_ == 4)
    {
      for (auto d0 = 0; d0 < dimlens_[0]; ++d0)
        for (auto d1 = 0; d1 < dimlens_[1]; ++d1)
          var_nc_->write(data, {d0, d1});
    }
    else if (ndims_ == 3)
    {
      for (auto d0 = 0; d0 < dimlens_[0]; ++d0)
        var_nc_->write(data, {d0});
    }
    else
    {
      var_nc_->write(data);
    }
  }
}

auto output_netcdf::close_file() -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
  if (direct_hdf5_)
  {
    var_h5_.reset();
    file_h5_.reset();
  }
  else
  {
    var_nc_ = nullptr;
    file_nc_.reset();
  }
}

output_netcdf_local::output_netcdf_local(configuration const& config, metadata const& user_parameters)
  : output_netcdf{config}
  , user_parameters_{&user_parameters}
  , path_format_(strip_file_schema(config["path"].string()))
{ }

auto output_netcdf_local::finalize() -> void
{
  close_file();
  final_path_.clear();
}

auto output_netcdf_local::abort() -> void
{
  close_file();
  /* We try to remove the output file if this is not a successful outcome.  If we fail to remove the file just trace
   * it as a non-fatal error - we just give a best-effort guarantee for cleanup during forecast failure. */
  if (auto ec = std::error_code{}; !final_path_.empty() && !std::filesystem::remove(final_path_, ec) && ec)
    trace(trace_level::error, "Failed to remove partial output file {}: {}", final_path_, ec.message());
  final_path_.clear();
}

auto output_netcdf_local::create_file(time_point reference_time, int first_member) -> nc::file
{
  try
  {
    final_path_ = first_member != -1
      ? format(path_format_, "param"_a = *user_parameters_, "reference"_a = reference_time, "member"_a = first_member)
      : format(path_format_, "param"_a = *user_parameters_, "reference"_a = reference_time);
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Path format '{}' is invalid", path_format_)});
  }

  auto lock = std::lock_guard<std::mutex>{mut_netcdf_};
  return nc::file{final_path_, nc::io_mode::create};
}
