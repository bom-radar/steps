/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "metadata.h"
#include "unit_test.h"

using namespace steps;

/* At first glance, it seems strange that this function takes two metadata object parameters when the purpose of this
 * function is to parse an object of this type from a configuration file.  This is because the metadata object serves
 * two distinct use cases:
 *   1. A holder for values to be provided as extra named arguments to later format() calls
 *   2. A holder for values to be written as global attributes in netcdf products
 * It is the second use case that requires the additional arguments.  The user_parameters argument allows the user to
 * substitute their custom parameters (via use case 1) into the 'value' of each metadata item being parsed.  For
 * example, they might want to include the site number, set via a user parameter, as part of the NetCDF/CF 'title'
 * attribute.  The parent argument allows products to inherit metadata from a parent product rather than it needing to
 * be respecified for each output. */
auto steps::parse_metadata(
      configuration const& config
    , metadata const& user_parameters
    , metadata const* parent
    ) -> metadata
{
  auto ret = parent ? *parent : metadata{};

  if (config.type() == configuration::node_type::null)
    return ret;

  for (auto& [name, body] : config.object())
  {
    try
    {
      if (body.type() == configuration::node_type::string)
          ret.insert_or_assign(name, format(body.string(), "param"_a = user_parameters));
      else if (body.type() == configuration::node_type::object)
      {
        auto& type = body["type"].string();
        auto value = format(body["value"].string(), "param"_a = user_parameters);
        if (type == "i8")
          ret.insert_or_assign(name, parse<int8_t>(value));
        else if (type == "u8")
          ret.insert_or_assign(name, parse<uint8_t>(value));
        else if (type == "i16")
          ret.insert_or_assign(name, parse<int16_t>(value));
        else if (type == "u16")
          ret.insert_or_assign(name, parse<uint16_t>(value));
        else if (type == "i32")
          ret.insert_or_assign(name, parse<int32_t>(value));
        else if (type == "u32")
          ret.insert_or_assign(name, parse<uint32_t>(value));
        else if (type == "i64")
          ret.insert_or_assign(name, parse<int64_t>(value));
        else if (type == "u64")
          ret.insert_or_assign(name, parse<uint64_t>(value));
        else if (type == "f32")
          ret.insert_or_assign(name, parse<float>(value));
        else if (type == "f64")
          ret.insert_or_assign(name, parse<double>(value));
        else if (type == "string")
          ret.insert_or_assign(name, value);
        else if (type == "time")
          ret.insert_or_assign(name, parse<time_point>(value));
        else
          throw std::runtime_error{format("Invalid metadatum type {}", type)};
      }
      else
        throw std::runtime_error{"Expected string or '{ type <type> value <value> }'"};
    }
    catch (...)
    {
      std::throw_with_nested(std::runtime_error{format("Failed to parse configuration for metadatum '{}'", name)});
    }
  }

  return ret;
}

// LCOV_EXCL_START
TEST_CASE("metadata")
{
  auto store = metadata{};
  store["int"] = 7;
  store["float"] = 25.83;
  store["string"] = "hello world";

  auto str = string{};

  CHECK_THROWS(str = fmt::format("{}", store));
  CHECK_THROWS(str = fmt::format("{:}", store));
  CHECK_THROWS(str = fmt::format("{:foo}", store));

  CHECK(fmt::format("{:int}", store) == "7");
  CHECK(fmt::format("{:int:03d}", store) == "007");
  CHECK(fmt::format("{store:string} {store:float:0.0f} {store:int}", "store"_a = store) == "hello world 26 7");
}
// LCOV_EXCL_STOP
