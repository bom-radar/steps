/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "traits.h"
#include "util.h"
#include "ext/fmt/format.h"
#include "ext/fmt/chrono.h"
#include <cerrno>
#include <charconv>

namespace steps
{
  // allow use of ""_a UDLs
  using namespace fmt::literals;

  using fmt::format;

  /// Trait used to enable implicit conversions and assignments between a type and string when serializing
  template <typename T>
  struct allow_simple_serialization
  {
    static constexpr bool value = std::is_arithmetic<std::decay_t<T>>::value || enum_traits<std::decay_t<T>>::specialized;
  };
  template <> struct allow_simple_serialization<time_point> { static constexpr bool value = true; };
  template <> struct allow_simple_serialization<duration> { static constexpr bool value = true; };

  /// Template that can be specialized to provide parsing functionality for a type
  template <typename T, typename Enable = void>
  struct parser;

  /// Parser for integer types
  template <typename T>
  struct parser<T, std::enable_if_t<std::is_integral_v<T>>>
  {
    auto parse(string_view str) -> T
    {
      auto val = T{};
      auto res = std::from_chars(str.data(), str.data() + str.size(), val);
      if (str.empty() || isspace(str[0]) || res.ptr != str.data() + str.size())
        throw std::runtime_error{format("failed to parse {} from '{}'", "int", str)};
      return val;
    };
  };

  /// Parser for floating point types
  template <typename T>
  struct parser<T, std::enable_if_t<std::is_floating_point_v<T>>>
  {
    auto parse(string_view str) -> T
    {
      // from_chars should work with floats, bug gcc doesn't have it implemented yet
      char* end;
      errno = 0;
      auto val = strtod(str.data(), &end);
      if (str.empty() || isspace(str[0]) || end != str.data() + str.size() || errno == ERANGE)
        throw std::runtime_error{format("failed to parse {} from '{}'", "float", str)};
      return val;
    };
  };

  /// Parser for enumerates that have a enum_traits defined
  template <typename T>
  struct parser<T, std::enable_if_t<steps::enum_traits<T>::specialized>>
  {
    auto parse(string_view str) -> T
    {
      return enum_value<T>(str);
    }
  };

  /// Parser for bool
  template <>
  struct parser<bool>
  {
    auto parse(string_view str) -> bool;
  };

  /// Parser for a time_point
  template <>
  struct parser<time_point>
  {
    auto parse(string_view str) -> time_point;
  };

  /// Parser for a duration
  template <>
  struct parser<duration>
  {
    auto parse(string_view str) -> duration;
  };

  /// Template function to parse a type from a string or string view
  template <typename T>
  auto parse(string_view str) -> T
  {
    return parser<T>{}.parse(str);
  }
}

namespace fmt
{
  /// Formatter for std::exception that prints nested exceptions
  template <typename T, typename Char>
  struct formatter<T, Char, std::enable_if_t<std::is_base_of<std::exception, T>::value>>
  {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      return ctx.begin();
    }

    template <typename FormatContext>
    auto format(std::exception const& err, FormatContext& ctx) -> decltype(ctx.out())
    {
      format_to(ctx.out(), "{}", err.what());
      try
      {
        std::rethrow_if_nested(err);
      }
      catch (std::exception const& nested)
      {
        format_to(ctx.out(), "\n-> ");
        format(nested, ctx);
      }
      return ctx.out();
    }
  };

  /// Formatter for std::optional that prints nested exceptions
  template <typename T, typename Char>
  struct formatter<std::optional<T>, Char> : public formatter<T, Char>
  {
    template <typename FormatContext>
    auto format(std::optional<T> const& val, FormatContext& ctx)
    {
      return val ? formatter<T, Char>::format(*val, ctx) : format_to(ctx.out(), "-null-");
    }
  };

  /// Formatter for enum types which have specialized enum_traits<T>
  /** We inherit the std::string formatter to allow use of padding and other string specific parameters. */
  template <typename T, typename Char>
  struct formatter<T, Char, std::enable_if_t<steps::enum_traits<T>::specialized>> : formatter<steps::string>
  {
    template <typename FormatContext>
    auto format(T const& val, FormatContext& ctx)
    {
      return formatter<steps::string, Char>::format(enum_label(val), ctx);
    }
  };

  /// Formatter for our time_point type
  /** Note that this overwrites the default time_point formatter because it always pints in the local time zone and
   *  defaults to a format that contains spaces.  We always print in UTC and default to ISO8601 (with the T and Z). */
  template <typename Char>
  struct formatter<steps::time_point, Char> : formatter<std::tm, Char>
  {
    #if 0
    FMT_CONSTEXPR formatter() {
      basic_string_view<Char> default_specs =
          detail::string_literal<Char, '%', 'F', 'T', '%', 'T', 'Z'>{};
      this->do_parse(default_specs.begin(), default_specs.end());
    }
    #endif
    FMT_CONSTEXPR formatter() {
      this->format_str_ = detail::string_literal<Char, '%', 'F', 'T', '%', 'T', 'Z'>{};
    }

    template <typename FormatContext>
    auto format(steps::time_point val, FormatContext& ctx) const -> decltype(ctx.out())
    {
      return formatter<std::tm, Char>::format(gmtime(val), ctx);
    }
  };

  /// Formatter for our duration type
  /** Note that this overwrites the default duration formatter because it always prints in the native resolution of the
   *  duration itself, which is nanoseconds on our current platforms.  We always print as seconds in this formatter. */
  template <typename Char>
  struct formatter<steps::duration, Char> : formatter<double, Char>
  {
    template <typename FormatContext>
    auto format(steps::duration const& v, FormatContext& ctx)
    {
      return formatter<double, Char>::format(std::chrono::duration_cast<std::chrono::duration<double>>(v).count(), ctx);
    }
  };
}
