/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "util.h"

namespace steps
{
  /// Parse a zlib compression level from a string
  /** This returns -1 for 'none' or otherwise parses as a simple integer.  There is a distinction between uncompressed
   *  data (none/-1) and data which has been compressed using a compression level of 0. */
  auto zlib_parse_level(string_view str) -> int;

  /// Compress an array using zlib compression
  /**
   * Returns a buffer containing the compressed data, and a size.  The size indicates how much of the buffer is used
   * by the acutal compressed data.  This is likely to be much smaller than the size of the buffer itself since that
   * buffer is sized assuming worst-case compression.  If you are intending to keep the buffer for a long time it may
   * be worth copying the data into a correctly sized buffer to preserve memory.
   *
   * The optional offset parameter causes this many extra bytes to be left unallocated at the start of the returned
   * compressed data buffer.  This can be used to allocated extra space needed for things like a header structure
   * without needing to either write a separate struct (extra I/O) or shift the compressed data to make room.  Any
   * 'offset' value counts towards the compressed data count returned by the function.  This ensures the returned
   * count always points to the array index one past the last compressed byte. */
  auto zlib_compress(span<char const> data, int level, int offset = 0) -> pair<array1<char>, int>;

  /// Decompress a chunk zlib compressed data
  /** Returns the number of bytes stored in the destination buffer. */
  auto zlib_decompress(span<char const> chunk, span<char> data) -> int;
}
