/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2020 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "aws_driver.h"
#include "core.h"
#include "mosaic.h"
#include "postprocessor.h"
#include "profile.h"
#include "trace.h"

#include <aws/batch/model/SubmitJobRequest.h>
#include <aws/monitoring/model/PutMetricDataRequest.h>
#include <aws/sns/model/PublishRequest.h>
#include <aws/sqs/model/ChangeMessageVisibilityRequest.h>
#include <aws/sqs/model/DeleteMessageRequest.h>
#include <aws/sqs/model/ReceiveMessageRequest.h>
#include <aws/sqs/model/ReceiveMessageResult.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <csignal>

using namespace steps;
using namespace steps::aws;

static profile prof_aws_request{"aws_driver::handle_request"};

// termination flag (set by receipt of SIGINT or SIGTERM
static volatile std::sig_atomic_t terminate;

static void signal_handler(int signal)
{
  if (signal == SIGINT || signal == SIGTERM)
    terminate = true;
}

void setup_signal_handler()
{
  terminate = false;

  struct sigaction act;
  act.sa_handler = signal_handler;
  sigemptyset(&act.sa_mask);
  act.sa_flags = SA_RESTART;

  if (sigaction(SIGINT, &act, nullptr) != 0 || sigaction(SIGTERM, &act, nullptr) != 0)
    throw std::runtime_error{"Failed to establish signal handler"};
}

driver::driver(configuration const& config)
  : cw_{client::get().client_config()}
  , sns_{client::get().client_config()}
  , metric_namespace_{config["metric_namespace"]}
  , sns_arn_{config["sns_arn"].string()}
  , cur_members_{0}
{
  if (auto conf_custom = config.find("custom_actions"))
  {
    custom_actions_.reserve(conf_custom->array().size());
    for (auto& action : conf_custom->array())
      custom_actions_.push_back(action.string());
  }
}

auto driver::handle_request(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  auto ps = profile::scope{prof_aws_request};

  // reset current members count so we only need to set it in applicable request types
  cur_members_ = 0;

  // determine the correct handler for this message and call it
  /* by checking custom actions first we ensure that we don't break existing user code if we add new 'native' steps
   * actions in the future.  incidentally this also allows uers to deliberately overwrite our native actions. */
  auto action = request.View().GetString("action");
  if (find(custom_actions_.begin(), custom_actions_.end(), action) != custom_actions_.end())
    run_custom_action(group, request);
  else if (action == "forecast")
    run_forecast(group, request);
  else if (action == "postprocess")
    run_postprocess(group, request);
  else if (action == "preprocess")
    run_preprocess(group, request);
  else if (action == "mosaic")
    run_mosaic(group, request);
  else if (action == "verify")
    run_verify(group, request);
  else if (action == "aggregate-verify")
    run_verify_aggregate(group, request);
  else
    throw std::runtime_error{format("Unknown request action {}", action)};
}

auto driver::on_progress(int member, int forecast) -> void
{ }

auto driver::notify_result(string const& group, Aws::Utils::Json::JsonValue& request, bool success) -> void
{
  request.WithString("result", success ? "success" : "failure");
  auto req_publish = Aws::SNS::Model::PublishRequest{};
  req_publish.SetTopicArn(sns_arn_);
  req_publish.SetMessage(request.View().WriteCompact());
  req_publish.SetMessageGroupId(group);
  auto res_publish = sns_.Publish(req_publish);
}

auto driver::publish_metrics(char const* metric, bool success, steady_time time_start, steady_time time_end) -> void
{
  auto req_metric = Aws::CloudWatch::Model::PutMetricDataRequest{};
  req_metric.SetNamespace(metric_namespace_);
  req_metric.AddMetricData(
      Aws::CloudWatch::Model::MetricDatum{}
      .WithMetricName(format("{}-{}-count", metric, success ? "pass" : "fail"))
      .WithUnit(Aws::CloudWatch::Model::StandardUnit::Count)
      .WithValue(1.0));
  req_metric.AddMetricData(
      Aws::CloudWatch::Model::MetricDatum{}
      .WithMetricName(format("{}-time", metric))
      .WithUnit(Aws::CloudWatch::Model::StandardUnit::Seconds)
      .WithValue(std::chrono::duration_cast<std::chrono::duration<double>>(time_end - time_start).count()));
  auto res_metric = cw_.PutMetricData(req_metric);
  if (!res_metric.IsSuccess())
    trace(trace_level::error, "Failed to put {} metric data: {}", metric, res_metric.GetError().GetMessage());
}

static auto parse_user_param_overrides(Aws::Utils::Json::JsonView& req) -> map<string, string>
{
  auto param_overrides = map<string, string>{};
  if (req.KeyExists("user_parameters"))
  {
    for (auto& req_param : req.GetObject("user_parameters").GetAllObjects())
    {
      if (req_param.second.IsString())
        param_overrides[req_param.first] = req_param.second.AsString();
      else if (req_param.second.IsBool())
        param_overrides[req_param.first] = req_param.second.AsBool() ? "true" : "false";
      else if (req_param.second.IsIntegerType())
        param_overrides[req_param.first] = format("{}", req_param.second.AsInt64());
      else if (req_param.second.IsFloatingPointType())
        param_overrides[req_param.first] = format("{}", req_param.second.AsDouble());
      else
        throw std::runtime_error{"Non-scalar value encountered for user_parameters override"};
    }
  }
  return param_overrides;
}

static auto apply_user_param_overrides(map<string, string> const& param_overrides, configuration& config)
{
  for (auto& po : param_overrides)
  {
    auto& param_conf = config["user_parameters"][po.first];
    if (param_conf.type() == configuration::node_type::string)
      param_conf = po.second;
    else if (param_conf.type() == configuration::node_type::object)
      param_conf["value"] = po.second;
    else
      throw std::runtime_error{"Expected string or '{ type <type> value <value> }'"};
  }
}

auto driver::run_forecast(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  struct stream_overrides
  {
    optional<time_point> reference_time;
    optional<time_point> backfill_reference_time;
  };

  // decode the request arguments
  auto req = request.View();
  auto config_uri = req.GetString("config");
  auto domain = req.GetString("domain");
  auto reference_time = parse<time_point>(req.GetString("reference_time"));
  auto members = req.KeyExists("members") ? req.GetInteger("members") : -1;
  auto member_offset = req.KeyExists("member_offset") ? req.GetInteger("member_offset") : -1;
  auto obs_overrides = stream_overrides{};
  if (req.KeyExists("observation"))
  {
    auto req_obs = req.GetObject("observation");
    if (req_obs.KeyExists("reference_time"))
      obs_overrides.reference_time = parse<time_point>(req_obs.GetString("reference_time"));
    if (req_obs.KeyExists("backfill_reference_time"))
      obs_overrides.backfill_reference_time = parse<time_point>(req_obs.GetString("backfill_reference_time"));
  }
  auto mod_overrides = map<string, stream_overrides>{};
  if (req.KeyExists("models"))
  {
    for (auto& req_mod : req.GetObject("models").GetAllObjects())
    {
      auto& mo = mod_overrides[req_mod.first];
      if (req_mod.second.KeyExists("reference_time"))
        mo.reference_time = parse<time_point>(req_mod.second.GetString("reference_time"));
      if (req_mod.second.KeyExists("backfill_reference_time"))
        mo.backfill_reference_time = parse<time_point>(req_mod.second.GetString("backfill_reference_time"));
    }
  }
  auto param_overrides = parse_user_param_overrides(req);

  // run the model
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Forecast {} {} commencing", domain, reference_time);

    // fetch the configuration from s3
    auto config = load_configuration_s3(config_uri, false);

    // override user parameters
    apply_user_param_overrides(param_overrides, config);

    // override settings for member count and offset
    /* we were using optional<int>'s here instead of a special value of -1 to indicate the value had not been set,
     * but a compiler bug in gcc causes erroneous "maybe uninitialized" warnings.  also in run_preprocess() below. */
    if (members != -1)
      config["members"] = members;
    if (member_offset != -1)
      config["member_offset"] = member_offset;

    // overrite settings for stream reference times
    if (obs_overrides.reference_time)
      config["observation"]["reference_time"] = format("{}", *obs_overrides.reference_time);
    if (obs_overrides.backfill_reference_time)
      config["observation"]["backfill_reference_time"] = format("{}", *obs_overrides.backfill_reference_time);
    for (auto& mo : mod_overrides)
    {
      if (!config["models"].find(mo.first))
        throw std::runtime_error{format("Unknown model {} requested", mo.first)};
      if (mo.second.reference_time)
        config["models"][mo.first]["reference_time"] = format("{}", *mo.second.reference_time);
      if (mo.second.backfill_reference_time)
        config["models"][mo.first]["backfill_reference_time"] = format("{}", *mo.second.backfill_reference_time);
    }

    // perform forecasts
    auto c = core{config};
    cur_members_ = c.members();
    c.set_progress_callback([&](int m, int f){ this->on_progress(m, f); });
    c.generate_forecasts(reference_time);

    // record outcome
    success = true;
    trace(trace_level::notice, "Forecast {} {} completed", domain, reference_time);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Forecast {} {} failed:\n-> {}", domain, reference_time, err);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  publish_metrics("forecast", success, time_start, time_end);
}

auto driver::run_postprocess(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  // decode the request arguments
  auto req = request.View();
  auto config_uri = req.GetString("config");
  auto domain = req.GetString("domain");
  auto reference_time = parse<time_point>(req.GetString("reference_time"));
  auto members = req.KeyExists("members") ? req.GetInteger("members") : -1;
  auto member_offset = req.KeyExists("member_offset") ? req.GetInteger("member_offset") : -1;
  auto param_overrides = parse_user_param_overrides(req);

  // run the model
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Postprocess {} {} commencing", domain, reference_time);

    // fetch the configuration from s3
    auto config = load_configuration_s3(config_uri, false);

    // override user parameters
    apply_user_param_overrides(param_overrides, config);

    // override settings for member count and offset
    if (members != -1)
      config["members"] = members;
    if (member_offset != -1)
      config["member_offset"] = member_offset;

    // perform forecasts
    auto c = postprocessor{config};
    cur_members_ = c.members();
    c.set_progress_callback([&](int m, int f){ this->on_progress(m, f); });
    c.generate_products(reference_time);

    // record outcome
    success = true;
    trace(trace_level::notice, "Postprocess {} {} completed", domain, reference_time);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Postprocess {} {} failed:\n-> {}", domain, reference_time, err);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  publish_metrics("postprocess", success, time_start, time_end);
}

auto driver::run_preprocess(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  // decode the request arguments
  auto req = request.View();
  auto config_uri = req.GetString("config");
  auto domain = req.GetString("domain");
  auto model = req.GetString("model");
  auto reference_time = parse<time_point>(req.GetString("reference_time"));
  auto members = req.KeyExists("members") ? req.GetInteger("members") : -1;
  auto member_offset = req.KeyExists("member_offset") ? req.GetInteger("member_offset") : -1;
  auto backfill_reference_time = req.KeyExists("backfill_reference_time")
    ? optional<time_point>(parse<time_point>(req.GetString("backfill_reference_time")))
    : nullopt;
  auto from = parse<time_point>(req.GetString("from"));
  auto forecasts = req.GetInteger("forecasts");
  auto param_overrides = parse_user_param_overrides(req);

  // run the model
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Preprocess {} {} commencing", domain, reference_time);

    // fetch the configuration from s3
    auto config = load_configuration_s3(config_uri, false);

    // sanity checks
    if (!config["models"].find(model))
      throw std::runtime_error{format("Unknown model {} requested", model)};

    // override user parameters
    apply_user_param_overrides(param_overrides, config);

    // overwrite settings for preprocessing
    config["forecasts"] = forecasts;
    config["models"][model]["reference_time"] = format("{}", reference_time);
    if (backfill_reference_time)
      config["models"][model]["backfill_reference_time"] = format("{}", backfill_reference_time);
    if (members != -1)
      config["members"] = members;
    if (member_offset != -1)
      config["member_offset"] = member_offset;

    // perform preprocessing
    auto c = core{config};
    cur_members_ = c.members();
    c.set_progress_callback([&](int m, int f){ this->on_progress(m, f); });
    c.preprocess_model(model, from);

    // record outcome
    success = true;
    trace(trace_level::notice, "Preprocess {} {} completed", domain, reference_time);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Preprocess {} {} failed:\n-> {}", domain, reference_time, err);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  publish_metrics("preprocess", success, time_start, time_end);
}

auto driver::run_mosaic(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  // decode the request arguments
  auto req = request.View();
  auto config_uri = req.GetString("config");
  auto domain = req.GetString("domain");
  auto reference_time = parse<time_point>(req.GetString("reference_time"));
  auto param_overrides = parse_user_param_overrides(req);

  // run the model
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Mosaic {} {} commencing", domain, reference_time);

    // fetch the configuration from s3
    auto config = load_configuration_s3(config_uri, false);

    // override user parameters
    apply_user_param_overrides(param_overrides, config);

    // perform mosaicing
    mosaic_generator{config}.generate_mosaics(reference_time);

    // record outcome
    success = true;
    trace(trace_level::notice, "Mosaic {} {} completed", domain, reference_time);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Mosaic {} {} failed:\n-> {}", domain, reference_time, err);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  publish_metrics("mosaic", success, time_start, time_end);
}

auto driver::run_verify(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  // decode the request arguments
  auto req = request.View();
  auto config_uri = req.GetString("config");
  auto domain = req.GetString("domain");
  auto from = parse<time_point>(req.GetString("from"));
  auto till = parse<time_point>(req.GetString("till"));
  auto period = duration{std::chrono::seconds{req.GetInteger("period")}};
  auto param_overrides = parse_user_param_overrides(req);

  // run the model
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Verify {} {} {} {} commencing", domain, from, till, period);

    // fetch the configuration from s3
    auto config = load_configuration_s3(config_uri, false);

    // override user parameters
    apply_user_param_overrides(param_overrides, config);

    // perform verification
    verification{config}.verify_period(from, till, period);

    // record outcome
    success = true;
    trace(trace_level::notice, "Verify {} {} {} {} completed", domain, from, till, period);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Verify {} {} {} {} failed:\n-> {}", domain, from, till, period, err);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  publish_metrics("verify", success, time_start, time_end);
}

auto driver::run_verify_aggregate(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  // decode the request arguments
  auto req = request.View();
  auto config_uri = req.GetString("config");
  auto domain = req.GetString("domain");
  auto paths_json = req.GetArray("paths");
  auto paths = vector<string>();
  paths.reserve(paths_json.GetLength());
  for (size_t i = 0; i < paths_json.GetLength(); ++i)
    paths.push_back(paths_json[i].AsString());
  auto param_overrides = parse_user_param_overrides(req);

  // run the model
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Aggregate verify {} commencing", domain);

    // fetch the configuration from s3
    auto config = load_configuration_s3(config_uri, false);

    // override user parameters
    apply_user_param_overrides(param_overrides, config);

    // perform verification
    verification{config}.aggregate_results(paths);

    // record outcome
    success = true;
    trace(trace_level::notice, "Aggregate verify {} completed", domain);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Aggregate verify {} failed:\n-> {}", domain);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  // treat aggregate-verify as verify in metrics for now to limit dashboard complexity
  publish_metrics("verify", success, time_start, time_end);
}

static auto fork_and_run(string const& script, string const& payload) -> void
{
  auto argv = std::array<char const*, 3>{script.c_str(), payload.c_str(), nullptr};

  // setup our pipe to capture traces
  int pipefd[2];
  if (pipe(pipefd) != 0)
    throw std::system_error{errno, std::generic_category(), "pipe"};

  // fork a child process for the custom action
  auto pid = fork();
  if (pid == -1)
  {
    // failed to fork - cleanup and throw
    auto err = std::system_error{errno, std::generic_category(), "fork"};
    close(pipefd[0]);
    close(pipefd[1]);
    throw err;
  }
  else if (pid == 0)
  {
    // child - run the job

    // close read end of pipe
    if (close(pipefd[0]) != 0 && errno != EINTR)
      _exit(EXIT_FAILURE);

    // send stdout to the pipe
    while (dup2(pipefd[1], 1) == -1)
      if (errno != EINTR)
        _exit(EXIT_FAILURE);

    // send stderr to the pipe
    while (dup2(pipefd[1], 2) == -1)
      if (errno != EINTR)
        _exit(EXIT_FAILURE);

    // close the write end of the pipe (since it's now duped to stdout and stderr)
    if (close(pipefd[1]) != 0 && errno != EINTR)
      _exit(EXIT_FAILURE);

    // execvp does not change it's args so the const_cast is okay (old C API problem)
    execvp(argv[0], const_cast<char**>(argv.data()));

    // if we get here, execvp failed - so kill our child process with a fail return code
    _exit(EXIT_FAILURE);
  }
  else
  {
    // parent - wait for child while forwarding its output to our traces

    // close the write end of the pipe
    if (close(pipefd[1]) != 0 && errno != EINTR)
      trace(trace_level::warning, "Failed to close write end of pipe: {}", std::system_error{errno, std::generic_category(), "close"});

    // read output from the child process until it terminates
    string trace_buffer;
    while (true)
    {
      char buffer[1024];
      auto ret = read(pipefd[0], buffer, sizeof(buffer));

      // return of zero indicates EOF, which means the child closed the pipe, most likely due to it terminating
      if (ret == 0)
        break;

      if (ret < 0)
      {
        // interrupted by signal, just try again
        if (errno == EINTR)
          continue;

        /* EAGAIN is the normal way to terminate the loop indicating that the child process is still
         * alive, but we have output everything it has sent us so far. */
        if (errno != EAGAIN)
          trace(trace_level::error, "Failed to read trace pipe: {}", std::system_error(errno, std::generic_category(), "read"));
        break;
      }

      // output whole lines immediately, buffer partial lines
      auto from = 0, to = 0;
      while (to < ret)
      {
        if (buffer[to] == '\n')
        {
          trace_buffer.append(&buffer[from], to - from);
          trace(trace_level::log, "{}", trace_buffer);
          trace_buffer.clear();
          from = to + 1;
        }
        ++to;
      }
      trace_buffer.append(&buffer[from], to - from);

      /* detect large amounts of output without a new line and ignore it.  this is most likely caused by
       * a dumb script, but could also be a malicious attempt to exhaust our memory.  safest respose is
       * to ignore the trace output and warn the user.  note we've already appended to the buffer at this
       * point so it may be larger than the limit - but not by more that 1024 bytes. */
      if (trace_buffer.size() > 2048)
      {
        trace_buffer.clear();
        trace(trace_level::error, "Output line too large for trace buffer");
      }
    }

    // close the read end of the pipe
    if (close(pipefd[0]) != 0 && errno != EINTR)
      trace(trace_level::warning, "Failed to close read end of pipe: {}", std::system_error{errno, std::generic_category(), "close"});

    // flush any left over trace (i.e. partial line)
    if (!trace_buffer.empty())
      trace(trace_level::log, "{}", trace_buffer);

    // process the child exit code
    auto status = 0;
    if (waitpid(pid, &status, 0) == -1)
      trace(trace_level::warning, "Failed to retrieve child exit status: {}", std::system_error{errno, std::generic_category(), "waitpid"});
    else if (WIFEXITED(status) && WEXITSTATUS(status) != 0)
      throw std::runtime_error{format("Child process exited with return code {}", WEXITSTATUS(status))};
    else if (WIFSIGNALED(status))
      throw std::runtime_error{format("Child process terminated by signal {}:{}", WTERMSIG(status), strsignal(WTERMSIG(status)))};
  }
}

auto driver::run_custom_action(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  // decode the request arguments
  auto action = request.View().GetString("action");
  auto domain = request.View().GetString("domain");

  // run the custom job
  auto success = false;
  auto time_start = std::chrono::steady_clock::now();
  try
  {
    trace(trace_level::notice, "Custom action {} {} commencing", action, domain);

    fork_and_run(action, request.View().WriteCompact());

    // record outcome
    success = true;
    trace(trace_level::notice, "Custom action {} {} completed", action, domain);
  }
  catch (std::exception& err)
  {
    trace(trace_level::notice, "Custom action {} {} failed:\n-> {}", action, domain, err);
  }
  auto time_end = std::chrono::steady_clock::now();

  // send the sns notification and update our cloud watch metrics
  notify_result(group, request, success);
  // use a single 'custom' metric category to simplify dashboard for now
  publish_metrics("custom", success, time_start, time_end);
}

driver_sqs::driver_sqs(configuration const& config)
  : driver{config}
  , sqs_{client::get().client_config()}
  , batch_{client::get().client_config()}
  , sqs_url_{config["sqs_url"].string()}
  , timeout_initial_{config["timeout_initial"]}
  , timeout_extension_{config["timeout_extension"]}
  , timeout_margin_{config["timeout_margin"]}
  , batch_job_defn_arn_{config.optional("batch_job_defn_arn", string{})}
  , batch_queue_h_arn_{config.optional("batch_queue_h_arn", string{})}
  , batch_queue_m_arn_{config.optional("batch_queue_m_arn", string{})}
  , batch_queue_l_arn_{config.optional("batch_queue_l_arn", string{})}
  , cur_init_count_{0}
{ }

auto driver_sqs::run() -> void
{
  setup_signal_handler();

  // enter the main service loop
  trace(trace_level::notice, "Commencing forecast services");
  while (!terminate)
  {
    auto req_receive = Aws::SQS::Model::ReceiveMessageRequest{};
    req_receive.SetQueueUrl(sqs_url_);
    req_receive.SetVisibilityTimeout(std::chrono::duration_cast<std::chrono::seconds>(timeout_initial_).count());
    req_receive.SetMaxNumberOfMessages(1);
    req_receive.SetWaitTimeSeconds(20);
    // TODO - the AWS C++ SDK is broken here... we only want to retrieve the MessageGroupId attribute, but the
    // function in the SDK is incorrectly based around the QueueAttributeName enum.  We can currently ask for
    // "All" since that exists in the QueueAttributeName enum and just happens to be valid for the AttributeNames
    // parameter in the main API as well.
    req_receive.AddAttributeNames(Aws::SQS::Model::QueueAttributeName::All);

    // obtain a message from the queue (long polling - blocks until a message is available)
    auto res_receive = sqs_.ReceiveMessage(req_receive);
    if (!res_receive.IsSuccess())
      throw std::runtime_error{format("Failed to receive message: {}", res_receive.GetError().GetMessage())};

    // if we managed to retrieve a request, handle it immediately
    auto const& messages = res_receive.GetResult().GetMessages();
    if (!messages.empty())
    {
      cur_receipt_handle_ = messages[0].GetReceiptHandle();
      cur_timeout_bump_ = clock::now() + timeout_initial_ - timeout_margin_;

      // update the queue latency metric
      publish_queue_latency(messages[0]);

      // handle the request
      try
      {
        // decode our request message
        auto group = messages[0].GetAttributes().at(Aws::SQS::Model::MessageSystemAttributeName::MessageGroupId);
        auto request = Aws::Utils::Json::JsonValue{messages[0].GetBody()};
        if (!request.WasParseSuccessful())
          throw std::runtime_error{format("Failed to parse JSON: {}", request.GetErrorMessage())};

        // check for user overrides to delete the sqs message prior to job completion
        cur_delete_policy_ = request.View().KeyExists("sqs_delete")
          ? parse<sqs_delete_policy>(request.View().GetString("sqs_delete"))
          : sqs_delete_policy::complete;
        cur_init_count_ = 0;

        if (cur_delete_policy_ == sqs_delete_policy::immediate)
          delete_current_request();

        if (request.View().KeyExists("batch"))
          submit_batch_job(group, request);
        else
          handle_request(group, request);

        // remove this message from the queue now that it has been processed, if not already done earlier
        if (!cur_receipt_handle_.empty())
          delete_current_request();
      }
      catch (std::exception& err)
      {
        trace(trace_level::warning, "Failed to process message '{}':\n-> {}", messages[0].GetBody(), err);

        // set the visibility timeout to 0 so that another instance can retry (or send it to the dead letter queue)
        // unless of course we've already deleted it due to the deltion policy in which case there's nothing to do
        if (!cur_receipt_handle_.empty())
        {
          auto req_visibility = Aws::SQS::Model::ChangeMessageVisibilityRequest{};
          req_visibility.SetQueueUrl(sqs_url_);
          req_visibility.SetVisibilityTimeout(0);
          req_visibility.SetReceiptHandle(cur_receipt_handle_);
          if (auto res = sqs_.ChangeMessageVisibility(req_visibility); !res.IsSuccess())
            trace(trace_level::error, "Failed to zero message visibility timeout: {}", res.GetError().GetMessage());
        }
      }

      // output profile report
      if constexpr (!opt_disable_profile)
        trace(trace_level::log, "Performance report\n{}", profile::generate_report(true));

      // flush trace after each request to ensure timely log delivery
      trace_flush_target();
    }
  }
  trace(trace_level::notice, "Forecast services terminated gracefully");
}

auto driver_sqs::on_progress(int member, int forecast) -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_cur_request_};

  /* we use this callback for knowing when to delete the message (if using the delete after init policy) and for
   * extending the message visibility timeout if we are getting uncomfortably close to it.  both of those are
   * not applicable once we have already deleted the request so we check here to save redundant checks later on. */
  if (cur_receipt_handle_.empty())
    return;

  /* a notification for forecast 0 indicates passing the initialization stage and entering the main forecast loop.
   * so we can simply count the number we receive to know how many members have initialized. */
  if (forecast == 0)
  {
    ++cur_init_count_;

    // once we receive one such notification per member we know that we have safely reached the 'after_init" state
    if (cur_delete_policy_ == sqs_delete_policy::after_init && cur_init_count_ >= cur_members())
    {
      trace(trace_level::log, "All members initialized, deleting message");
      delete_current_request();
      return;
    }
  }

  // do we need to bump the visibility timeout a bit?
  if (timeout_extension_ > duration::zero() && clock::now() >= cur_timeout_bump_)
  {
    /* note we set the new visibility timeout to extension + margin rather than just extension.  this is because we
     * trigger the extension at T-margin, and the new value is always relative to current time.  so if we want to
     * extend by extension we need to set the timeout to extension + margin. */
    auto new_timeout_sec = std::chrono::duration_cast<std::chrono::seconds>(timeout_extension_ + timeout_margin_).count();

    auto req_visibility = Aws::SQS::Model::ChangeMessageVisibilityRequest{};
    req_visibility.SetQueueUrl(sqs_url_);
    req_visibility.SetVisibilityTimeout(new_timeout_sec);
    req_visibility.SetReceiptHandle(cur_receipt_handle_);
    if (auto res = sqs_.ChangeMessageVisibility(req_visibility); !res.IsSuccess())
      trace(trace_level::error, "Failed to extend message visibility timeout: {}", res.GetError().GetMessage());

    cur_timeout_bump_ = clock::now() + timeout_extension_;

    trace(trace_level::log, "Extended visibility timeout to {} seconds", new_timeout_sec);
  }
}

auto driver_sqs::publish_queue_latency(Aws::SQS::Model::Message const& msg) -> void
{
  auto sent_time_ms = parse<long>(msg.GetAttributes().at(Aws::SQS::Model::MessageSystemAttributeName::SentTimestamp));
  auto sent_time = std::chrono::system_clock::from_time_t(sent_time_ms / 1000);
  auto latency = std::chrono::system_clock::now() - sent_time;

  auto req_metric = Aws::CloudWatch::Model::PutMetricDataRequest{};
  req_metric.SetNamespace(metric_namespace());
  req_metric.AddMetricData(
      Aws::CloudWatch::Model::MetricDatum{}
      .WithMetricName("request-wait-time")
      .WithUnit(Aws::CloudWatch::Model::StandardUnit::Seconds)
      .WithValue(std::chrono::duration_cast<std::chrono::duration<double>>(latency).count()));
  auto res_metric = cw().PutMetricData(req_metric);
  if (!res_metric.IsSuccess())
    trace(trace_level::error, "Failed to put request-wait-time metric data: {}", res_metric.GetError().GetMessage());
}

auto driver_sqs::submit_batch_job(string const& group, Aws::Utils::Json::JsonValue& request) -> void
{
  trace(trace_level::notice, "Submitting batch job for group {}", group);

  // convert message into a batch job submission
  auto view = request.View();
  auto view_batch = view.GetObject("batch");
  auto req_job = Aws::Batch::Model::SubmitJobRequest{};
  req_job.SetJobName(format("{}-{}", view.GetString("domain"), view.GetString("action")));
  req_job.SetJobDefinition(batch_job_defn_arn_);
  auto priority = view_batch.GetString("priority");
  if (priority == "high")
    req_job.SetJobQueue(batch_queue_h_arn_);
  else if (priority == "medium")
    req_job.SetJobQueue(batch_queue_m_arn_);
  else if (priority == "low")
    req_job.SetJobQueue(batch_queue_l_arn_);
  else
    throw std::runtime_error{"Invalid priority for batch job"};
  auto vcpus = view_batch.KeyExists("vcpus") ? view_batch.GetInteger("vcpus") : 0;
  auto memory = view_batch.KeyExists("memory") ? view_batch.GetInteger("memory") : 0;
  auto threads = view_batch.KeyExists("threads") ? view_batch.GetInteger("threads") : vcpus;
  if (vcpus || memory)
  {
    auto contover = Aws::Batch::Model::ContainerOverrides{};
    if (vcpus)
    {
      auto res = Aws::Batch::Model::ResourceRequirement{};
      res.SetType(Aws::Batch::Model::ResourceType::VCPU);
      res.SetValue(format("{}", vcpus));
      contover.AddResourceRequirements(std::move(res));
    }
    if (memory)
    {
      auto res = Aws::Batch::Model::ResourceRequirement{};
      res.SetType(Aws::Batch::Model::ResourceType::MEMORY);
      res.SetValue(format("{}", memory));
      contover.AddResourceRequirements(std::move(res));
    }
    req_job.SetContainerOverrides(std::move(contover));
  }
  req_job.AddParameters("group", format("group={}", group));
  req_job.AddParameters("payload", format("payload={}", view.WriteCompact()));
  if (threads)
    req_job.AddParameters("threads", format("threads={}", threads));

  if (auto res = batch_.SubmitJob(req_job); !res.IsSuccess())
    trace(trace_level::error, "Failed to submit batch job: {}", res.GetError().GetMessage());

  /* note that since we delete the message from the SQS queue after this, the user cannot rely on the SQS FIFO
   * message group mechanism to serialize tasks.  in the case of batch jobs it is up to the client to implement
   * the necessary logic.  in practice this would have always been the case anyway since you always want to
   * check the result of a task (via the SNS topic) before deciding to launch its dependencies or not. */
}

auto driver_sqs::delete_current_request() -> void
{
  auto req_delete = Aws::SQS::Model::DeleteMessageRequest{};
  req_delete.SetQueueUrl(sqs_url_);
  req_delete.SetReceiptHandle(cur_receipt_handle_);
  if (auto res = sqs_.DeleteMessage(req_delete); !res.IsSuccess())
    trace(trace_level::error, "Failed to delete message: {}", res.GetError().GetMessage());
  cur_receipt_handle_.clear();
}

driver_batch::driver_batch(configuration const& config)
  : driver{config}
{ }

auto driver_batch::run(configuration const& config) -> void
{
  // decode our request message
  auto request = Aws::Utils::Json::JsonValue{config["payload"].string()};
  if (!request.WasParseSuccessful())
    throw std::runtime_error{format("Failed to parse JSON: {}", request.GetErrorMessage())};

  // the job parameters are also passed on the command line so we receive them in the same config object
  handle_request(config["group"].string(), request);

  // output profile report
  if constexpr (!opt_disable_profile)
    trace(trace_level::log, "Performance report\n{}", profile::generate_report(false));
}
