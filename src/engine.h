/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "core.h"
#include "autoregressive_model.h"
#include "stochastic.h"

namespace steps
{
  /// Member generation engine
  class engine
  {
  public:
    engine(core& c, int index);

    auto member_forecast(time_point reference_time, int member) -> void;

  protected:
    using state_ptr = shared_ptr<stream_state const>;

    auto initialize_test_pattern() -> void;
    auto initialize_conditional() -> void;
    auto initialize_semiconditional() -> void;
    auto initialize_unconditional() -> void;

    auto perturb_initial_state() -> void;

    auto dump_diagnostics_t0() -> void;
    auto dump_diagnostics_local_stats(char const* name, local_stats const& data, std::initializer_list<int> coords) const -> void;

    auto retrieve_model_states() -> void;
    auto diagnose_initial_model_skills() -> void;
    auto lookup_initial_model_skills() -> void;

    auto update_model_skills() -> void;
    auto update_observation_skills() -> void;

    auto blend_flow() -> void;

    auto make_level_psd(int lvl, filter& psd) -> void;

    auto advect_nowcast() -> void;
    auto evolve_nowcast() -> void;
    auto post_process() -> void;
    auto output_forecast() -> void;

    auto debug_sanity_check_state() -> void;

  private:
    // Housekeeping
    core&                 c;                      // Model core
    int                   index_;                 // Engine index

    // Simulation state
    int                   member_;                // Member number being generated
    int                   forecast_;              // Index of current forecast (0 = ref time, 1 = first forecast)
    time_point            time_;                  // Time of current forecast
    duration              lead_time_;             // Lead time of current forecast
    state_ptr             obs_state_;             // Conditional simulation T0 state (i.e. radar state)
    vector<state_ptr>     mod_states_;            // Model states
    random_engine         random_engine_;         // Random number generator for this engine
    array2i               mask_;                  // Output mask

    // Distribution correction related state
    vector<distribution_correction::analysis const*> distcor_model_analysis_;  // Model stream analysis
    std::any              distcor_state_;         // State for the distribution correction

    // Flow related state
    array2f2              flow_;                  // Current flow field
    array1f               flow_skill_t0_;         // Model skills at diagnosed reference time
    array1f               flow_skill_tn_;         // Model skills at current time
    array1d               flow_skills_;           // Skills vector

    // PSD related state
    int                   psd_diagnosed_levels_;  // Number of PSD levels to diagnose (others use default from core)
    array2f               psd_skill_t0_;          // Model skills diagnosed at reference time
    array2f               psd_skill_tn_;          // Model skills at current time
    vector<array1d>       psd_skills_;            // Skills vector

    // Cascade related state
    vector<local_stats>   cascade_stats_;         // Local process parameters for AR models
    array1f               cascade_skill_t0_;      // Diagnosed model skills at reference time
    array1f               cascade_skill_tn_;      // Model skills at current time
    array1d               cascade_skills_;        // Skills vector

    cascade               lag1_;                  // Lag-1 cascade
    cascade               lag0_;                  // Lag-0 cascade
    array2f               lag0_field_;            // Recomposed lag-0 field
  };
}
