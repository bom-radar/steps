/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "engine.h"
#include "blend_weights.h"
#include "profile.h"
#include "statistics.h"
#include "test_patterns.h"
#include "thread_pool.h"
#include "trace.h"

using namespace steps;

static profile prof_advect_nowcast{"engine::advect_nowcast"};
static profile prof_evolve_nowcast{"engine::evolve_nowcast"};
static profile prof_output_forecast{"engine::output_forecast"};

engine::engine(core& c, int index)
  : c(c)
  , index_{index}
  , mod_states_(c.model_count())
  , random_engine_{init_random_engine()}
  , distcor_model_analysis_{size_t(c.model_count())}
  , flow_{c.grid_shape()}
  , flow_skill_t0_{c.model_count()}
  , flow_skill_tn_{c.model_count()}
  , psd_diagnosed_levels_{c.diagnosed_psd_levels()}
  , psd_skill_t0_{c.model_count(), c.levels()}
  , psd_skill_tn_{c.model_count(), c.levels()}
  , psd_skills_(c.levels())
  , cascade_stats_(c.levels())
  , cascade_skill_t0_{c.model_count()}
  , cascade_skill_tn_{c.model_count()}
  , lag1_{c.levels(), c.grid_shape()}
  , lag0_{c.levels(), c.grid_shape()}
  , lag0_field_{c.grid_shape()}
{
  // the skill vectors must include an extra row/column for the skill of the nowcast itself
  flow_skills_.resize(c.model_count() + 1);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    psd_skills_[lvl].resize(c.model_count() + 1);
  cascade_skills_.resize(c.model_count() + 1);

  // initialize the spatially varying parameter arrays
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    cascade_stats_[lvl].resize(c.grid_shape());

  if (c.mask_advected_in())
    mask_.resize(c.grid_shape());
}

auto engine::member_forecast(time_point reference_time, int member) -> void
{
  trace(trace_level::log, "Starting member");

  // if we are using explicit random seeds then reinit the random engine each member using the specified seed
  // offset by the member number
  if (c.random_seed() != 0)
  {
    trace(trace_level::log, "Random engine seed set to {}", c.random_seed() + member);
    random_engine_ = seed_random_engine(c.random_seed() + member);
  }

  member_ = member;
  forecast_ = 0;
  time_ = reference_time;
  lead_time_ = 0s;

  // initialize the nowcast state
  if (c.test_pattern().type() != configuration::node_type::null)
    initialize_test_pattern();
  else if (c.observation())
    initialize_conditional();
  else if (!c.models().empty())
    initialize_semiconditional();
  else
    initialize_unconditional();

  // perturb initial states to increase spread
  perturb_initial_state();

  // initialize the distribution correction
  for (auto mod = 0; mod < c.model_count(); ++mod)
    distcor_model_analysis_[mod] = mod_states_[mod]->distcor_analysis.get();
  distcor_state_ = c.distribution_correction().initialize(obs_state_->distcor_analysis.get(), distcor_model_analysis_);

  // initialize the output mask (if used)
  if (c.mask_advected_in())
  {
    if (c.mask_static() && c.mask_static_advect())
      mask_.copy(*c.mask_static());
    else
      mask_.fill(1);
  }

  // write the initial diagnostics
  dump_diagnostics_t0();

  // clean up any reference files that were loaded during initialization which we no longer need
  nc::release_reference_files();

  // inform driver code that we have reached the end of the member initialization phase
  c.engine_notify_progress(member_, forecast_);

  // generate the forecasts
  // note that the first forecast is forecast 1, not 0!  this makes indexing in the diagnostic file easier
  for (forecast_ = 1; forecast_ <= c.forecasts(); ++forecast_)
  {
    auto prefix = trace_prefix{format("t{}", forecast_)};
    lead_time_ = forecast_ * c.time_step();
    time_ = reference_time + lead_time_;

    // check for early termination and wait for slower engines to catch up if necessary
    if (!c.engine_commence_time_step(index_, reference_time, false))
      return;

    trace(trace_level::verbose, "Starting forecast {}", forecast_);

    // sanity check our state variables (debugging only)
    if constexpr (opt_debug_engine_state)
      debug_sanity_check_state();

    // retrieve model inputs
    retrieve_model_states();

    // regress observation skills based on autocorrelation
    update_observation_skills();

    // regress model skills towards climatology
    update_model_skills();

    // blend the flow field
    blend_flow();

    // use the flow field to acconut for apparent motion of the rain field
    advect_nowcast();

    // use the AR(1) model to account for dynamic evolution of the rain field
    evolve_nowcast();

    // perform post processing such as distribution correction
    post_process();

    // output the forecast
    output_forecast();

    // inform driver code that we have completed this time step
    c.engine_notify_progress(member_, forecast_);
  }
}

auto engine::initialize_test_pattern() -> void
try
{
  throw std::runtime_error{"Test patterns not implemented"};

  #if 0 // TODO
  trace(trace_level::warning, "Using test pattern to initialize nowcast state");

  // initialize the field pattern
  make_test_pattern(c.test_pattern()["lag0"], c.transformer(), lag0_field_);

  // initialize the flow pattern
  if (auto config = c.test_pattern().find("flow"))
  {
    if (c.test_pattern().find("lag1"))
      trace(trace_level::error, "Both lag1 and flow specified in test pattern.  Ignoring lag1 configuration.");
    make_test_pattern(*config, c.transformer(), flow_);
  }
  else if (auto config = c.test_pattern().find("lag1"))
  {
    auto lag1_field = array2f{c.grid_shape()};
    make_test_pattern(*config, c.transformer(), lag1_field);
    c.tracker().determine_flow(lag0_field_, lag1_field, false, flow_);
  }
  else
    throw std::runtime_error{"Test pattern config must specify either lag1 or explicit flow"};

  // okay - decompose our field and set the rest of our state
  decompose(c.fft(), c.bandpass_filters(), lag0_field_, c.log_zero(), lag0_, nullptr);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
  {
    cascade_mean_[lvl] = 0.0f;
    auto radius = c.bandpass_filters().central_wavelength(lvl) * c.parameter_radius();
    if constexpr (opt_disable_local_stddev)
      cascade_stddev_[lvl].fill(lag0_[lvl].mean_stddev().second);
    else
      local_standard_deviation(lag0_[lvl], cascade_mean_[lvl], std::ceil(radius), cascade_stddev_[lvl]);
  }

  psd_diagnosed_levels_ = 0;
  #endif
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{"Failed to initialize test pattern"});
}

auto engine::initialize_conditional() -> void
{
  // get the observation state at the reference time
  // TODO - observation_ won't be getting notified about when it should release cache at the moment...
  if (!(obs_state_ = c.observation()->determine_state(member_, time_)))
    throw std::runtime_error{format("Failed to ingest observation for time {}", time_)};

  // get the model states at the reference time
  retrieve_model_states();

  // condition the initial nowcast state using the observation
  flow_.copy(obs_state_->flow);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    cascade_stats_[lvl].copy(obs_state_->cascade_stats[lvl]);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    lag0_[lvl].copy(obs_state_->cascade[lvl]);
  lag0_field_.copy(obs_state_->field);

  // TODO - decide if the wet area ratio is sufficient to use diagnosed psd and autocorrelation
  auto allow_diagnosis = true;

  // setup diagnosis limits for PSD
  psd_diagnosed_levels_ = allow_diagnosis ? c.diagnosed_psd_levels() : 0;

  // diagnose model skills at reference time by comparing to current observations
  diagnose_initial_model_skills();
}

auto engine::initialize_semiconditional() -> void
{
  throw std::runtime_error{"Semi-conditional simulation not implemented"};

  #if 0 // TODO
  // no observations available
  obs_state_.reset();

  // get the model states at the reference time
  retrieve_model_states();
  if (!mod_states_[0])
    throw std::runtime_error{format("Failed to ingest model {} for time {}", c.models()[0].name(), time_)};

  // condition the initial nowcast state using the observation
  /* TODO we might want to apply extra stochastic influence here since we are dealing with a forecast rather than an
   * observation.  for now we just use the first NWP model as if it was obs. */
  flow_.copy(mod_states_[0]->flow);
  cascade_mean_.copy(mod_states_[0]->mean_cascade);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    cascade_stddev_[lvl].copy(mod_states_[0]->std_cascade[lvl]);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    lag0_[lvl].copy(mod_states_[0]->cascade[lvl]);
  lag0_field_.copy(mod_states_[0]->field);

  // TODO - decide if the wet area ratio is sufficient to use diagnosed psd and autocorrelation
  auto allow_diagnosis = true;

  // setup diagnosis limits for PSD
  psd_diagnosed_levels_ = allow_diagnosis ? c.diagnosed_psd_levels() : 0;

  // use climatological model skills since we can't diagnose against an observation
  lookup_initial_model_skills();
  #endif
}

auto engine::initialize_unconditional() -> void
{
  throw std::runtime_error{"Unconditional simulation not implemented"};

  #if 0 // TODO
  obs_state_.reset();

  // don't bother retrieving initial model states since we won't be diagnosing skills
  for (auto& s : mod_states_)
    s.reset();

  psd_diagnosed_levels_ = 0;

  // no truth to allow diagnosis of model skills so set initial skills to NaN
  flow_skill_t0_.fill(std::numeric_limits<float>::quiet_NaN());
  psd_skill_t0_.fill(std::numeric_limits<float>::quiet_NaN());
  cascade_skill_t0_.fill(std::numeric_limits<float>::quiet_NaN());
  #endif
}

auto engine::perturb_initial_state() -> void
{
  // disable perturbations for the control member
  if (member_ == c.control_member())
  {
    trace(trace_level::log, "Initial perturbations disabled for control member");
    return;
  }

  // perturb the flow field for this member
  if (c.perturb_flow() > 0.0f)
  {
    auto dist = std::lognormal_distribution<float>(log(1.0) + (c.perturb_flow() * c.perturb_flow()), c.perturb_flow());
    auto perturb_factor = dist(random_engine_);
    flow_.multiply(perturb_factor);
    trace(trace_level::log, "Flow vector perturbed by {}", perturb_factor);
  }
}

auto engine::dump_diagnostics_t0() -> void
{
  lag0_.dump_diagnostics("lag0", {0});
  diagnostics::write("lag0-f", lag0_field_, {0});
  diagnostics::write("flow", flow_, {0});
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
    dump_diagnostics_local_stats("obs", cascade_stats_[lvl], {0, lvl});
}

auto engine::dump_diagnostics_local_stats(char const* name, local_stats const& data, std::initializer_list<int> coords) const -> void
{
  auto buf = array2f{c.grid_shape()};
  for (auto i = 0; i < data.size(); ++i)
    buf.data()[i] = data.data()[i].mean;
  diagnostics::write(format("{}-mean", name).c_str(), buf, coords);
  for (auto i = 0; i < data.size(); ++i)
    buf.data()[i] = data.data()[i].stddev;
  diagnostics::write(format("{}-std", name).c_str(), buf, coords);
  for (auto i = 0; i < data.size(); ++i)
    buf.data()[i] = data.data()[i].autocor;
  diagnostics::write(format("{}-ac", name).c_str(), buf, coords);
}

auto engine::retrieve_model_states() -> void
{
  for (auto mod = 0; mod < c.model_count(); ++mod)
  {
    // get state for this time
    mod_states_[mod] = c.models()[mod].determine_state(member_, time_);

    // TODO if a model drops out, burn it's contribution into the main nowcast cascade using it's current weight
    //      this will prevent shocking the system by suddenly not including a model
    //      after this eliminate it from covar matrix/skills vector and don't use it in blend, never allow it to
    //      contribute again (stop trying determine_state)
    if (!mod_states_[mod])
      throw std::runtime_error{format("Failed to ingest model {} for time {}", c.models()[mod].name(), time_)};
  }

  if (diagnostics::enabled())
  {
    for (auto mod = 0; mod < c.model_count(); ++mod)
    {
      diagnostics::write("mod-fld", mod_states_[mod]->field, {forecast_, mod});
      mod_states_[mod]->cascade.dump_diagnostics("models", {forecast_, mod});
      for (auto lvl = 0; lvl < c.levels(); ++lvl)
        dump_diagnostics_local_stats("mod", mod_states_[mod]->cascade_stats[lvl], {forecast_, mod, lvl});
    }
  }
}

auto engine::diagnose_initial_model_skills() -> void
{
  // setup t0 skill vectors and update climatologies
  for (auto mod = 0; mod < c.model_count(); ++mod)
  {
    auto &mstate = *mod_states_[mod];
    auto &ostate = *obs_state_;

    // initial skills are the same as the obs/model cells in the covariance matrix
    /* low skills are clamped to 0.0 to prevent negative values.  it is of course possible to measure a negative covariance
     * or correlation between a model and the observation, but we don't want to interpret this as 'real' skill just having
     * an inverted magnitude.  if we did, it would often lead to negative blend weights which are problematic. */
    if constexpr (opt_flow_correlation)
      flow_skill_t0_[mod] = std::max(0.0, correlation(mstate.flow, ostate.flow).magnitude());
    else
      flow_skill_t0_[mod] = std::max(0.0, covariance(mstate.flow, ostate.flow).magnitude());
    thread_pool_distribute_loop(c.levels(), [&](int lvl)
    {
      if constexpr (opt_psd_correlation)
        psd_skill_t0_[mod][lvl] = std::max(0.0, correlation_weighted(c.bandpass_filters()[lvl], mstate.psd, ostate.psd));
      else
        psd_skill_t0_[mod][lvl] = std::max(0.0, covariance_weighted(c.bandpass_filters()[lvl], mstate.psd, ostate.psd));
    });
    if (c.spatial_skill_levels() > 1)
    {
      // this partial recompose could be avoided if we stored the partial recomposition in stream state
      // not sure if it would be worth the memory tradeoff though
      auto mpartial = array2f{c.grid_shape()};
      recompose_partial(mstate.cascade, c.spatial_skill_levels(), mpartial);
      auto opartial = array2f{c.grid_shape()};
      recompose_partial(ostate.cascade, c.spatial_skill_levels(), opartial);
      if constexpr (opt_cascade_correlation)
        cascade_skill_t0_[mod] = std::max(0.0, correlation(mpartial, opartial));
      else
        cascade_skill_t0_[mod] = std::max(0.0, covariance(mpartial, opartial));
    }
    else
    {
      if constexpr (opt_cascade_correlation)
        cascade_skill_t0_[mod] = std::max(0.0, correlation(mstate.cascade[0], ostate.cascade[0]));
      else
        cascade_skill_t0_[mod] = std::max(0.0, covariance(mstate.cascade[0], ostate.cascade[0]));
    }

    // update the skill climatology
    c.models()[mod].update_skills(
          member_
        , time_
        , cascade_skill_t0_[mod]
        , span{psd_skill_t0_[mod], c.levels()}
        , flow_skill_t0_[mod]);
  }
}

auto engine::lookup_initial_model_skills() -> void
{
  for (auto mod = 0; mod < c.model_count(); ++mod)
    c.models()[mod].lookup_skills(
          member_
        , time_
        , cascade_skill_t0_[mod]
        , span{psd_skill_t0_[mod], c.levels()}
        , flow_skill_t0_[mod]);
}

auto engine::update_model_skills() -> void
{
  // update skill vectors
  for (auto mod = 0; mod < c.model_count(); ++mod)
  {
    // get climatological skills for this time
    c.models()[mod].lookup_skills(
          member_
        , time_
        , cascade_skill_tn_[mod]
        , span{psd_skill_tn_[mod], c.levels()}
        , flow_skill_tn_[mod]);

    // determine interpolation factor
    auto frac = std::clamp(lead_time_.count() / (c.models()[mod].skills_halflife().count() * 2.0f), 0.0f, 1.0f);
    auto lerp_skill = [](float t0, float tn, float frac)
    {
      if (std::isnan(tn))
        return !std::isnan(t0) ? t0 : throw std::runtime_error{"No diagnosed or climatological skill available"};
      else
        return !std::isnan(t0) ? lerp(t0, tn, frac) : tn;
    };

    // regress model skills towards climatology if both diagnosed and climatological skills are available
    flow_skill_tn_[mod] = lerp_skill(flow_skill_t0_[mod], flow_skill_tn_[mod], frac);
    for (auto lvl = 0; lvl < c.levels(); ++lvl)
      psd_skill_tn_[mod][lvl] = lerp_skill(psd_skill_t0_[mod][lvl], psd_skill_tn_[mod][lvl], frac);
    cascade_skill_tn_[mod] = lerp_skill(cascade_skill_t0_[mod], cascade_skill_tn_[mod], frac);

    // transpose skills from [mod][lvl] to [lvl][mod]
    flow_skills_[mod] = flow_skill_tn_[mod];
    for (auto lvl = 0; lvl < c.levels(); ++lvl)
      psd_skills_[lvl][mod] = psd_skill_tn_[mod][lvl];
    cascade_skills_[mod] = cascade_skill_tn_[mod];
  }
}

auto engine::update_observation_skills() -> void
{
  if (!c.observation())
    return;

  auto scale_correlation = [](double scale_factor, double input)
  {
    return 1.0 - std::pow(1.0 - input, scale_factor);
  };
  auto scale_covariance = [](double scale_factor, double input)
  {
    return scale_factor * input;
  };

  // regress skill of the observation input based on lag-n autocorrelation determined by AR model
  auto obs = c.model_count();
  if constexpr (opt_flow_correlation)
    flow_skills_[obs] = scale_correlation(c.obs_skill_scale(), ar1_autocorrelation(obs_state_->ac_flow, forecast_));
  else
    flow_skills_[obs] = scale_covariance(c.obs_skill_scale(), ar1_autocovariance(std::sqrt(obs_state_->var_flow), obs_state_->ac_flow, forecast_));
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
  {
    if constexpr (opt_psd_correlation)
      psd_skills_[lvl][obs] = scale_correlation(c.obs_skill_scale(), ar1_autocorrelation(obs_state_->ac_psd[lvl], forecast_));
    else
      psd_skills_[lvl][obs] = scale_covariance(c.obs_skill_scale(), ar1_autocovariance(std::sqrt(obs_state_->var_psd[lvl]), obs_state_->ac_psd[lvl], forecast_));
  }
  if constexpr (opt_cascade_correlation)
    // HACK - the obs skills are dropping too slowly, so I'm trying out multiplying the spatial AC and flow AC
    // here before calculating skill.  that way uncertainty in the tracking lowers our spatial skill as well
    #if 1
    cascade_skills_[obs] = scale_correlation(c.obs_skill_scale(), ar1_autocorrelation(obs_state_->ac_flow * obs_state_->ac_cascade, forecast_));
    #else
    cascade_skills_[obs] = c.obs_skill_scale() * ar1_autocorrelation(obs_state_->ac_cascade, forecast_);
    #endif
  else
    cascade_skills_[obs] = scale_covariance(c.obs_skill_scale(), ar1_autocovariance(std::sqrt(obs_state_->var_cascade), obs_state_->ac_cascade, forecast_));
}

static auto trace_weights(char const* tag, int level, array2d const& covars, array1d const& skills, array1f const& weights) -> void
{
  auto str = level == -1 ? format("{} covars ", tag) : format("{}-{} covars ", tag, level);
  for (auto y = 0; y < covars.shape().y; ++y)
  {
    str.append("[ ");
    for (auto x = 0; x < covars.shape().x; ++x)
      str.append(format(" {:6.3f}", covars[y][x]));
    str.append(" ] ");
  }
  str.append("skills [");
  for (auto i = 0; i < skills.size(); ++i)
    str.append(format(" {:6.3f}", skills[i]));
  str.append(" ] weights [");
  auto sum_weight = 0.0f;
  for (auto i = 0; i < skills.size(); ++i)
  {
    str.append(format(" {:6.3f}", weights[i]));
    sum_weight += weights[i];
  }
  str.append(format(" | {:6.3f} ]", sum_weight));
  trace(trace_level::log, "{}", str);
}

auto engine::advect_nowcast() -> void
{
  auto ps = profile::scope{prof_advect_nowcast};

  // if advection is manually disabled then the standard deviations don't change
  // the field itself must be re-decomposed because distribution correction might have modified it
  if (c.disable_advection())
  {
    // decompose the lag1 field (which is just the unadvected lag0) into the lag1 cascade
    decompose(c.fft(), c.bandpass_filters(), lag0_field_, c.log_zero(), c.anti_ring_border(), lag1_, nullptr);
    return;
  }

  // use the first NWP model to fill in off edge pixels if available
  if (!mod_states_.empty())
  {
    // advect the flow field itself if desired
    if (c.advect_flow())
    {
      auto buf_flow = flow_;
      advect(buf_flow, mod_states_[0]->flow, buf_flow, flow_);
    }

    // advect the previous local statistics
    /* we do NOT use the model as background for locally varying statistics.  instead we store an invalid value in the the
     * advected in regions.  this acts as a mask during the evolution phase.  grid cells without valid local statistics
     * in the nowcast will simply use the model values directly. */
    thread_pool_distribute_loop(c.levels(), [&](int lvl)
    {
      auto buf = local_stats{c.grid_shape()};
      advect(flow_, cell_stats::invalid(), cascade_stats_[lvl], buf);
      std::swap(buf, cascade_stats_[lvl]);
    });

    // advect the previous lag0 field to make a lag1 field
    // using lag0_[0] as a buffer here
    advect(flow_, mod_states_[0]->field, lag0_field_, lag0_[0]);

    // decompose the lag1 field to make up the lag1 cascade
    decompose(c.fft(), c.bandpass_filters(), lag0_[0], c.log_zero(), c.anti_ring_border(), lag1_, nullptr);
  }
  else
  {
    // advect the flow field itself if desired
    if (c.advect_flow())
    {
      auto buf_flow = flow_;
      advect(buf_flow, buf_flow, buf_flow, flow_);
    }

    // advect the previous local standard deviations
    thread_pool_distribute_loop(c.levels(), [&](int lvl)
    {
      auto buf = local_stats{c.grid_shape()};
      advect(flow_, cell_stats::invalid(), cascade_stats_[lvl], buf);
      std::swap(cascade_stats_[lvl], buf);
    });

    // advect the previous lag0 field to make a lag1 field
    // using lag0_[0] as a buffer here
    advect(flow_, 0.0f, lag0_field_, lag0_[0]);

    // decompose the lag1 field to make up the lag1 cascade
    decompose(c.fft(), c.bandpass_filters(), lag0_[0], c.log_zero(), c.anti_ring_border(), lag1_, nullptr);
  }

  // update the mask based on the flow this frame
  if (c.mask_advected_in())
  {
    array2i mask_cpy{mask_};
    advect(flow_, 0, mask_cpy, mask_);
  }

  if (diagnostics::enabled())
  {
    diagnostics::write("flow", flow_, {forecast_});
    for (auto lvl = 0; lvl < c.levels(); ++lvl)
      dump_diagnostics_local_stats("obs", cascade_stats_[lvl], {forecast_, lvl});
  }
}

/* for a simple advection forecast, this function just runs the AR(1) update of the field using the local statistics
 * that were originally diagnosed and advected.  for a model blended forecast, we don't blend in the cascade values
 * directly, but rather we blend the local parameters that drive the AR(1) model - the mean, stddev and autocor.  this
 * ensures that the nowcast itself keeps evolving according to the AR(1) model, but that the parameters of the process
 * drift to match the NWP over time.  since the local mean is non-zero in the low frequency cascade levels (where the
 * models tend to have some skil at placing rain) this makes the nowcast evolve rain in the same general areas as the
 * models.  also the blending of the stddev and autocor ensure that the rain which is evolved has similar structural
 * and temporal characteristics.
 *
 * an important thing to note is that we do NOT update the local statictics themselves.  we always blend the current
 * local stats from the model with the local stats that were originally diagnosed from the observation (just advected
 * to the current time).  this is important!  if we blended the nwp into the local stats of the nowcast and retained
 * it then we would have a positive feedback loop with the model being blending back on to the model each step and it
 * would quickly take over.  the weight is the total contribution we want from the model for this time step so we need
 * to blend with the original observed paramters.
 *
 * in areas where the observation is not available due to field advection we just use the nwp parameters directly.  at
 * this point it is the only information we have so there's not really anything to blend. */
auto engine::evolve_nowcast() -> void
{
  auto ps = profile::scope{prof_evolve_nowcast};

  // if evolution is manually disabled then our (advected) lag1 field becomes lag0 exactly
  if (c.disable_evolution())
  {
    for (auto lvl = 0; lvl < c.levels(); ++lvl)
      lag0_[lvl].copy(lag1_[lvl]);
    lag0_.dump_diagnostics("lag0", {forecast_});
    return;
  }

  // determine model blend weights if needed
  auto weights = array1f{};
  if (!c.models().empty())
  {
    auto obs = c.model_count();

    // partially recompose cascades using levels the user nominated for skill evaluation (if needed)
    auto partial = vector<array2f>();
    if (c.spatial_skill_levels() > 1)
    {
      // the model partial recompositions could be avoided if we stored this as part of the stream state
      // note sure if this is worth the tradeoff for memory though
      partial.reserve(c.model_count() + 1);
      for (auto mod = 0; mod < c.model_count(); ++mod)
        recompose_partial(mod_states_[mod]->cascade, c.spatial_skill_levels(), partial.emplace_back(c.grid_shape()));
      recompose_partial(lag1_, c.spatial_skill_levels(), partial.emplace_back(c.grid_shape()));
    }

    // collect arrays into single list
    auto variables = vector<span<float const>>(c.model_count() + 1);
    if (c.spatial_skill_levels() > 1)
    {
      for (auto mod = 0; mod < c.model_count(); ++mod)
        variables[mod] = partial[mod];
      variables[c.model_count()] = partial[obs];
    }
    else
    {
      for (auto mod = 0; mod < c.model_count(); ++mod)
        variables[mod] = mod_states_[mod]->cascade[0];
      variables[c.model_count()] = lag1_[0];
    }

    // determine current covariance matrix
    auto covars = covariance_matrix(variables);
    assert_covariance_matrix(covars);
    if constexpr (opt_cascade_correlation)
      covariance_matrix_to_correlation_matrix(covars);

    // constrain covariances based on skills
    constrain_covariances(covars, cascade_skills_, index_ == 0);

    // update blend weights
    weights = calculate_blend_weights(covars, cascade_skills_, blend_weight_normalization::standard);

    // trace for debugging
    // we only do this for engine 0 as a way of limiting trace size and increasing readability
    if (index_ == 0 && trace_active(trace_level::log))
      trace_weights("cascade", -1, covars, cascade_skills_, weights);

    // dump to diagnostics
    if (diagnostics::enabled())
    {
      diagnostics::write("skills_c", cascade_skills_, {forecast_});
      diagnostics::write("weight_c", weights, {forecast_});
    }
  }

  // generate our new noise spectrum
  auto noise_spectrum = generate_noise_spectrum(c.fft(), random_engine_);

  // evolve each cascade level independently
  thread_pool_distribute_loop(c.levels(), [&](int lvl)
  {
    auto psd = filter{c.fft().shape_c()};
    auto noise = array2f{c.grid_shape()};

    #if opt_debug_ar1_parameters
    auto ar1_stats = local_stats{c.grid_shape()};
    auto ar1_blend_factor = array2f{c.grid_shape()};
    auto ar1_blend_delta = array2f{c.grid_shape()};
    #endif

    // build the PSD for the noise at this level
    make_level_psd(lvl, psd);

    // generate the noise field for this level
    generate_n01_spatial_noise(c.fft(), psd, noise_spectrum, noise);

    // use AR(1) model to create our next lag0
    if (c.models().empty())
    {
      for (auto i = 0; i < lag0_[lvl].size(); ++i)
      {
        if (!cascade_stats_[lvl].data()[i].valid())
          lag0_[lvl].data()[i] = lag1_[lvl].data()[i];
        else
        {
          lag0_[lvl].data()[i] = ar1_forecast(
                cascade_stats_[lvl].data()[i].mean
              , cascade_stats_[lvl].data()[i].stddev
              , cascade_stats_[lvl].data()[i].autocor
              , noise.data()[i]
              , lag1_[lvl].data()[i]);
        }

        #if opt_debug_ar1_parameters
        ar1_stats.data()[i] = cascade_stats_[lvl].data()[i];
        #endif
      }
    }
    else
    {
      auto obs = c.model_count();

      if (c.model_count() > 1)
      {
        for (auto i = 0; i < lag0_[lvl].size(); ++i)
        {
          float mean, stddev, autocor;
          if (!cascade_stats_[lvl].data()[i].valid())
          {
            mean = mod_states_[0]->cascade_stats[lvl].data()[i].mean;
            stddev = mod_states_[0]->cascade_stats[lvl].data()[i].stddev;
            autocor = mod_states_[0]->cascade_stats[lvl].data()[i].autocor;
          }
          else
          {
            /* blend process mean using weighted arithmetic mean.  only really has an impact at large scales where the
             * local mean is non-zero - which is the scales where the models have some absolute skill in cascade values.
             * as a result, the absolute values of the cascade at in large scales will drift towards the models. */
            mean = weights[obs] * cascade_stats_[lvl].data()[i].mean;
            for (auto mod = 0; mod < c.model_count(); ++mod)
              mean += weights[mod] * mod_states_[mod]->cascade_stats[lvl].data()[i].mean;

            /* blend process stddev using weighted quadratic mean (i.e. blend in variance, then convert back to stddev).
             * this gives preference to the higher stddev which is much better when the nowcast and model(s) disagree
             * over whether there is rain in the area.  it allows existing rain to continue to have structure. */
            stddev = weights[obs] * cascade_stats_[lvl].data()[i].stddev * cascade_stats_[lvl].data()[i].stddev;
            for (auto mod = 0; mod < c.model_count(); ++mod)
              stddev += weights[mod] * mod_states_[mod]->cascade_stats[lvl].data()[i].stddev * mod_states_[mod]->cascade_stats[lvl].data()[i].stddev;
            stddev = std::sqrt(stddev);

            /* blend autocor using weighted harmonic mean.  this gives preference to the smaller autocor when the
             * nowcast and model(s) disagree.  this ensures that existing rain continues to evolve even if the model
             * has no rain in that area and is highly weighted.  without this observed rain stops evolving over time
             * and freezes until it is finally advected out of the domain. */
            #if 0
            // weighted geometric mean
            autocor = weights[obs] * std::log(std::max(0.01f, cascade_stats_[lvl].data()[i].autocor));
            for (auto mod = 0; mod < c.model_count(); ++mod)
              autocor += weights[mod] * std::log(std::max(0.01f, mod_states_[mod]->cascade_stats[lvl].data()[i].autocor));
            autocor = std::exp(autocor);
            #else
            // weighted harmonic mean
            autocor = weights[obs] / std::max(0.01f, cascade_stats_[lvl].data()[i].autocor);
            for (auto mod = 0; mod < c.model_count(); ++mod)
              autocor += weights[mod] / std::max(0.01f, mod_states_[mod]->cascade_stats[lvl].data()[i].autocor);
            autocor = 1.0f / autocor;
            #endif
          }

          /* calculate a direct blend between the cascade values based on weights.  then interpolate between the
           * real lag1 and this blended value to create a new modified 'lag1' that we use as input to the AR(1).
           * the amount of the blended value used is conditioned on the autocorrelation so that only very slowly
           * evolving areas (high autocor) will use the direct blend.  it is also limited by as 'max blend per
           * step' parameter which is configured by the user as a 'maximum blending halflife'. */
          // TODO - this is only using a single model!
          auto blend_factor = float(c.blend_limit() * std::pow(autocor, c.blend_ac_exponent()));
          auto lag1
              = weights[0] * mod_states_[0]->cascade[lvl].data()[i]
              + weights[1] * lag1_[lvl].data()[i];
          lag1 = lerp(lag1_[lvl].data()[i], lag1, blend_factor);

          // use AR(1) model to create our next lag0 value
          lag0_[lvl].data()[i] = ar1_forecast(mean, stddev, autocor, noise.data()[i], lag1);

          #if opt_debug_ar1_parameters
          ar1_stats.data()[i].mean = mean;
          ar1_stats.data()[i].stddev = stddev;
          ar1_stats.data()[i].autocor = autocor;
          ar1_blend_factor.data()[i] = blend_factor;
          ar1_blend_delta.data()[i] = lag1 - lag1_[lvl].data()[i];
          #endif
        }
      }
      else
      {
        // model loop unrolled for better performance in the one model case
        for (auto i = 0; i < lag0_[lvl].size(); ++i)
        {
          float mean, stddev, autocor;
          if (!cascade_stats_[lvl].data()[i].valid())
          {
            mean = mod_states_[0]->cascade_stats[lvl].data()[i].mean;
            stddev = mod_states_[0]->cascade_stats[lvl].data()[i].stddev;
            autocor = mod_states_[0]->cascade_stats[lvl].data()[i].autocor;
          }
          else
          {
            mean
              = weights[0] * mod_states_[0]->cascade_stats[lvl].data()[i].mean
              + weights[1] * cascade_stats_[lvl].data()[i].mean;

            stddev = std::sqrt(
                weights[0] * mod_states_[0]->cascade_stats[lvl].data()[i].stddev * mod_states_[0]->cascade_stats[lvl].data()[i].stddev
              + weights[1] * cascade_stats_[lvl].data()[i].stddev * cascade_stats_[lvl].data()[i].stddev);

            #if 0
            autocor = std::exp(
                weights[0] * std::log(std::max(0.01f, mod_states_[0]->cascade_stats[lvl].data()[i].autocor))
              + weights[1] * std::log(std::max(0.01f, cascade_stats_[lvl].data()[i].autocor)));
            #else
            autocor = 1.0f /
              ( weights[0] / std::max(0.01f, mod_states_[0]->cascade_stats[lvl].data()[i].autocor)
              + weights[1] / std::max(0.01f, cascade_stats_[lvl].data()[i].autocor));
            #endif
          }

          auto blend_factor = float(c.blend_limit() * std::pow(autocor, c.blend_ac_exponent()));
          auto lag1
              = weights[0] * mod_states_[0]->cascade[lvl].data()[i]
              + weights[1] * lag1_[lvl].data()[i];
          lag1 = lerp(lag1_[lvl].data()[i], lag1, blend_factor);

          lag0_[lvl].data()[i] = ar1_forecast(mean, stddev, autocor, noise.data()[i], lag1);

          #if opt_debug_ar1_parameters
          ar1_stats.data()[i].mean = mean;
          ar1_stats.data()[i].stddev = stddev;
          ar1_stats.data()[i].autocor = autocor;
          ar1_blend_factor.data()[i] = blend_factor;
          ar1_blend_delta.data()[i] = lag1 - lag1_[lvl].data()[i];
          #endif
        }
      }
    }

    // dump some diagnostics
    if (diagnostics::enabled())
    {
      diagnostics::write("noise", noise, {forecast_, lvl});
      diagnostics::write("lag0", lag0_[lvl], {forecast_, lvl});
      diagnostics::write("lag1", lag1_[lvl], {forecast_, lvl});
      #if opt_debug_ar1_parameters
      dump_diagnostics_local_stats("ar1", ar1_stats, {forecast_, lvl});
      diagnostics::write("blend-factor", ar1_blend_factor, {forecast_, lvl});
      diagnostics::write("blend-delta", ar1_blend_delta, {forecast_, lvl});
      #endif
    }
  });

  if (diagnostics::enabled())
  {
    auto buf = array2f{c.grid_shape()};
    recompose(lag0_, buf);
    diagnostics::write("lag0-f", buf, {forecast_});
    recompose(lag1_, buf);
    diagnostics::write("lag1-f", buf, {forecast_});
  }
}

auto engine::blend_flow() -> void
{
  // if no blending required then the flow field is static so nothing to do
  if (c.models().empty())
    return;

  // collect arrays into single list
  auto variables = vector<span<vec2f const>>(c.model_count() + 1);
  for (auto mod = 0; mod < c.model_count(); ++mod)
    variables[mod] = mod_states_[mod]->flow;
  variables[c.model_count()] = flow_;

  // determine covariance matrix
  auto covars = covariance_matrix(variables);
  assert_covariance_matrix(covars);
  if constexpr (opt_flow_correlation)
    covariance_matrix_to_correlation_matrix(covars);

  // constrain covariances based on skills
  constrain_covariances(covars, flow_skills_, index_ == 0);

  // determine blend weights
  /* since we have no way to stochastically evolve the flow field in a way that makes sense we just assume that the
   * models inputs are the best information we have.  this means we want to assign the 'residual' weight to the models.
   * this is the 'except_last' normalization strategy (weight distributed among models to bring total weight to 1.0). */
  auto weights = calculate_blend_weights(covars, flow_skills_, blend_weight_normalization::except_last);

  // trace for debugging
  // we only do this for engine 0 as a way of limiting trace size and increasing readability
  if (index_ == 0 && trace_active(trace_level::log))
    trace_weights("   flow", -1, covars, flow_skills_, weights);

  // dump to diagnostics
  if (diagnostics::enabled())
  {
    diagnostics::write("skills_f", flow_skills_, {forecast_});
    diagnostics::write("weight_f", weights, {forecast_});
  }

  // blend the diagnosed and model flow fields
  /* use of the obs_state flow here is the reason that we can't enable blending and advection of flow vectors at the
   * same time.  to use them both we will need to keep a separate 'obs only' flow which we advect each time step using
   * the blended flow so that we have a clean (no nwp) flow field with which to blend next time step.  we can do this,
   * but it costs an extra flow field per engine.  for now we just make the options mutually exclusive. */
  if (c.observation())
    flow_.copy_scaled(obs_state_->flow, weights[c.model_count()]);
  else
    flow_.fill(vec2f{0.0f, 0.0f});
  for (auto mod = 0; mod < c.model_count(); ++mod)
    flow_.add_scaled(mod_states_[mod]->flow, weights[mod]);
}

auto engine::make_level_psd(int lvl, filter& psd) -> void
{
  auto& bandpass = c.bandpass_filters()[lvl];

  // if we are below the allowed scale for psd diagnosis, use the default psd
  if (lvl >= psd_diagnosed_levels_)
  {
    for (auto i = 0; i < psd.size(); ++i)
      psd.data()[i] = bandpass.data()[i] * c.default_psd().data()[i];
    return;
  }

  // if there are no models just use the observed psd
  if (c.models().empty())
  {
    for (auto i = 0; i < psd.size(); ++i)
      psd.data()[i] = bandpass.data()[i] * obs_state_->psd.data()[i];
    return;
  }

  // collect arrays into single list
  auto variables = vector<span<float const>>(c.model_count() + 1);
  for (auto mod = 0; mod < c.model_count(); ++mod)
    variables[mod] = mod_states_[mod]->psd;
  variables[c.model_count()] = obs_state_->psd;

  // determine current covariance matrix
  auto covars = covariance_matrix_weighted(bandpass, variables);
  assert_covariance_matrix(covars);
  if constexpr (opt_psd_correlation)
    covariance_matrix_to_correlation_matrix(covars);

  // constrain covariances based on skills
  constrain_covariances(covars, psd_skills_[lvl], index_ == 0);

  // update blend weights
  /* since we have no way to stochastically evolve the PSD in a way that makes sense we just assume that the models
   * inputs are the best information we have.  this means we want to assign the 'residual' weight to the models.
   * this is the 'except_last' normalization strategy (weight distributed among models to bring total weight to 1.0).
   * note that this arrangement really implies that you should use the psd_diagnosis_limit to prevent use of nwp
   * PSD at high frequencies where the grid resolution (and general lack of structure) will do harm. */
  auto weights = calculate_blend_weights(covars, psd_skills_[lvl], blend_weight_normalization::except_last);

  // trace for debugging
  // we only do this for engine 0 as a way of limiting trace size and increasing readability
  if (index_ == 0 && trace_active(trace_level::log))
    trace_weights("  psd", lvl, covars, psd_skills_[lvl], weights);

  // dump to diagnostics
  if (diagnostics::enabled())
  {
    diagnostics::write("skills_p", psd_skills_[lvl], {forecast_, lvl});
    diagnostics::write("weight_p", weights, {forecast_, lvl});
  }

  // perform the blend
  for (auto i = 0; i < psd.size(); ++i)
  {
    auto f = bandpass.data()[i];

    // short circuit if this level does not contribute to the wave in question
    if (f <= 0.0f)
    {
      psd.data()[i] = 0.0f;
      continue;
    }

    auto lvlval = c.observation() ? obs_state_->psd.data()[i] * weights[c.model_count()] : 0.0f;
    for (auto mod = 0; mod < c.model_count(); ++mod)
      lvlval += mod_states_[mod]->psd.data()[i] * weights[mod];
    psd.data()[i] = lvlval * f;
  }
}

auto engine::post_process() -> void
{
  // recompose into the output field
  recompose(lag0_, lag0_field_);

  // correct the distribution of rain rates
  for (auto mod = 0; mod < c.model_count(); ++mod)
    distcor_model_analysis_[mod] = mod_states_[mod]->distcor_analysis.get();
  c.distribution_correction().correct_field(distcor_state_, distcor_model_analysis_, flow_, lag0_field_);
  diagnostics::write("dcor-f", lag0_field_, {forecast_});
}

auto engine::output_forecast() -> void
{
  auto ps = profile::scope{prof_output_forecast};

  // we use lag1_[0] as a buffer here...

  // transform back to mm/hr units
  lag1_[0].copy(lag0_field_);
  c.transformer().inverse(lag1_[0]);

  // threshold low rain rates to 0.0
  if (c.threshold_output())
    lag1_[0].threshold_min(c.minimum_rain_rate(), 0.0f);

  // mask out areas that were advected from outside the domain
  if (c.mask_advected_in())
  {
    for (auto i = 0; i < lag1_[0].size(); ++i)
      if (!mask_.data()[i])
        lag1_[0].data()[i] = std::numeric_limits<float>::quiet_NaN();
  }

  // mask out areas from the user's static mask (unless advecting it, in which case it is already part of mask_)
  if (c.mask_static() && !c.mask_static_advect())
  {
    for (auto i = 0; i < lag1_[0].size(); ++i)
      if (!c.mask_static()->data()[i])
        lag1_[0].data()[i] = std::numeric_limits<float>::quiet_NaN();
  }

  // write the output product
  c.products().write(member_ - c.member_offset(), forecast_ - 1, lag1_[0], &flow_);
  diagnostics::write("output", lag1_[0], {forecast_});
}

struct check_exception
{
  string  name;
  int     index = -1;
  int     mod = -1;
  int     lvl = -1;
};

template <typename T>
inline auto check_impl(T const& value, char const* name)
{
  using std::isnan;
  if (isnan(value))
    throw check_exception{name};
}

template <typename T>
inline auto check_impl(array1<T> const& value, char const* name)
{
  using std::isnan;
  for (auto i = 0; i < value.size(); ++i)
    if (isnan(value.data()[i]))
      throw check_exception{name, i};
}

template <typename T>
inline auto check_impl(array2<T> const& value, char const* name)
{
  using std::isnan;
  for (auto i = 0; i < value.size(); ++i)
    if (isnan(value.data()[i]))
      throw check_exception{name, i};
}

#define check(expr) check_impl(expr, #expr)

auto engine::debug_sanity_check_state() -> void
try
{
  // obs state
  if (obs_state_)
  {
    check(obs_state_->field);
    check(obs_state_->flow);
    check(obs_state_->psd);
    check(obs_state_->var_flow);
    check(obs_state_->ac_flow);
    check(obs_state_->var_psd);
    check(obs_state_->ac_psd);
    check(obs_state_->var_cascade);
    check(obs_state_->ac_cascade);
    for (auto lvl = 0; lvl < c.levels(); ++lvl)
    {
      try
      {
        check(obs_state_->cascade[lvl]);
        check(obs_state_->cascade_stats[lvl]);
      }
      catch (check_exception& err)
      {
        err.lvl = lvl;
        throw;
      }
    }
    // TODO check(obs_state_->distcor_analysis);
  }

  // model states
  for (auto mod = 0; mod < c.model_count(); ++mod)
  {
    if (!mod_states_[mod])
      continue;

    try
    {
      check(mod_states_[mod]->field);
      check(mod_states_[mod]->flow);
      check(mod_states_[mod]->psd);
      check(mod_states_[mod]->var_flow);
      check(mod_states_[mod]->ac_flow);
      check(mod_states_[mod]->var_psd);
      check(mod_states_[mod]->ac_psd);
      check(mod_states_[mod]->var_cascade);
      check(mod_states_[mod]->ac_cascade);
      for (auto lvl = 0; lvl < c.levels(); ++lvl)
      {
        try
        {
          check(mod_states_[mod]->cascade[lvl]);
          check(mod_states_[mod]->cascade_stats[lvl]);
        }
        catch (check_exception& err)
        {
          err.lvl = lvl;
          throw;
        }
      }
      // TODO check(mod_states_[mod]->distcor_analysis);
    }
    catch (check_exception& err)
    {
      err.mod = mod;
      throw;
    }
  }

  // main engine state
  check(flow_);
  check(flow_skill_t0_);
  check(flow_skill_tn_);
  check(flow_skills_);
  check(psd_skill_t0_);
  check(psd_skill_tn_);
  check(cascade_skill_t0_);
  check(cascade_skill_tn_);
  check(cascade_skills_);
  check(lag0_field_);
  for (auto lvl = 0; lvl < c.levels(); ++lvl)
  {
    try
    {
      check(psd_skills_[lvl]);
      check(cascade_stats_[lvl]);
      check(lag1_[lvl]);
      check(lag0_[lvl]);
    }
    catch (check_exception& err)
    {
      err.lvl = lvl;
      throw;
    }
  }

  trace(trace_level::notice, "Debug sanity check passed");
}
catch (check_exception& err)
{
  auto msg = format("NaN detected in '{}'", err.name);
  if (err.index != -1)
    msg.append(format(" at index {}", err.index));
  if (err.mod != -1)
    msg.append(format(", mod = {}", err.mod));
  if (err.lvl != -1)
    msg.append(format(", lvl = {}", err.lvl));
  throw std::runtime_error{std::move(msg)};
}
