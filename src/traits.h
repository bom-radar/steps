/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "macros.h"
#include <algorithm>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>

namespace steps
{
  /// Constexpr function to determine whether the arguments represent an ascending sequence of integers
  constexpr auto is_sequential() { return true; }
  template <typename L>
  constexpr auto is_sequential(L l) { return true; }
  template <typename L, typename R, typename... Args>
  constexpr auto is_sequential(L l, R r, Args... args) { return l + 1 == r && is_sequential(r, args...); }

  /// Constexpr function to determine whether the arguments represent an ascending sequence of integers starting at 0
  template <typename L, typename... Args>
  constexpr auto is_index_sequence(L l, Args... args) { return l == 0 && is_sequential(l, args...); }

  /// Trait to determine if a type is an instance of a particular template
  template <class, template <class, class...> class>
  struct is_instance : public std::false_type {};

  template <class...Ts, template <class, class...> class U>
  struct is_instance<U<Ts...>, U> : public std::true_type {};

  template <class T, template <class, class...> class U>
  inline constexpr bool is_instance_v = is_instance<T, U>::value;

  /// Get type of first type in parameter pack
  /// Traits template used to provide reflection of enum values and labels
  /** Traits may be defined for new enum types using the STEPS_ENUM_TRAITS() macro.  After running this macro a
   *  specialization of the enum_traits template will exist for the enum which contains the following members:
   *    type       - the enum type itself (T)
   *    value_type - the underlying type for the enum (std::underlying_type<T>::type)
   *    name       - name of the enum type (char const*)
   *    count      - the number of enumerates defined by the enum type (size_t)
   *    labels     - an array of strings specifying the name of each enumerate (char const* [])
   *    values     - an array of values specifying the underlying value of each enumerate (value_type[])
   *    is_simple  - a boolean which is true if values are exactly [0..N)
   */
  template <typename T>
  struct enum_traits
  {
    static constexpr bool specialized = false;
  };

  /// Macro used to define traits for an enumerate type
  /** The arguments to this macro should be the name of the enumerate (relative to the bom namespace) followed
   *  by each of the enumerate values in the same order as they are delcared in. */
  #define STEPS_ENUM_TRAITS(x, ...) \
    template <> \
    struct enum_traits<x> \
    { \
      using type = x; \
      using value_type = std::underlying_type_t<x>; \
      static inline constexpr bool specialized = true; \
      static inline constexpr char const* name = #x; \
      static inline constexpr int count = STEPS_NUM_ARGS(__VA_ARGS__); \
      static inline constexpr char const* labels[] = { STEPS_STRINGIFY_ARGS(__VA_ARGS__) }; \
      static inline constexpr value_type values[] = { STEPS_PREPEND_TYPE_ARGS(__VA_ARGS__) }; \
      static inline constexpr value_type max_value = std::max({STEPS_PREPEND_TYPE_ARGS(__VA_ARGS__)}); \
      static inline constexpr bool is_simple = is_index_sequence( STEPS_PREPEND_TYPE_ARGS(__VA_ARGS__) ); \
    }

  /// Get the string associated with an enum value
  /** In the case of a complex enum where multiple enumerates map to the same value, the first one listed in
   *  the enum_traits for the type will be returned. */
  template <typename T>
  constexpr auto enum_label(T val) -> typename std::enable_if_t<enum_traits<T>::is_simple, char const*>
  {
    return enum_traits<T>::labels[static_cast<std::underlying_type_t<T>>(val)];
  }
  template <typename T>
  auto enum_label(T val) -> typename std::enable_if_t<!enum_traits<T>::is_simple, char const*>
  {
    for (int i = 0; i < enum_traits<T>::count; ++i)
      if (enum_traits<T>::values[i] == static_cast<typename std::underlying_type_t<T>>(val))
        return enum_traits<T>::labels[i];
    throw std::logic_error{std::string("invalid enum_traits detected for type ") + enum_traits<T>::name};
  }

  /// Get the enum associated with a string
  template <typename T>
  auto enum_value(std::string_view str) -> T
  {
    for (int i = 0; i < enum_traits<T>::count; ++i)
      if (str == enum_traits<T>::labels[i])
        return static_cast<T>(enum_traits<T>::values[i]);
    std::string msg = "failed to parse ";
    msg.append(enum_traits<T>::name).append(" from '").append(str).append("'");
    throw std::runtime_error{msg};
  }
}
