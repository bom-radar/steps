/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

/* This file defines various compilation time options which may be set by the STEPS developer.  They are NOT intended
 * to be modified by users of the model.  The settings here mostly enable special debug outputs, or change certain
 * behaviours for the purpose of experimentation and research. */

/// Disable the profiling capability
#define opt_disable_profile false

/// Fundamental floating point type for fftw
#define opt_fftw_single_precision true

/// Enable extra diagnostic fields for spatially varying AR(1) parameters
#define opt_debug_ar1_parameters false

namespace steps
{
  /// Enable extra trace to helps analyize engine spread
  inline constexpr bool opt_debug_engine_spread = false;

  /// Enable sanity checks on engine state variables each time step
  inline constexpr bool opt_debug_engine_state = false;

  /// Remove the DC component of the PSD when generating stochastic cascade
  inline constexpr bool opt_remove_dc_from_stochastic_psd = false;

  /// Generate noise in spatial domain and FFT instead of generating it directly in the spectrum
  inline constexpr bool opt_spatial_noise = false;

  /// Use correlation for flow blend weights instead of covariance
  inline constexpr bool opt_flow_correlation = true;

  /// Use correlations for spectral blend weight calculations instead of covariance
  inline constexpr bool opt_psd_correlation = true;

  /// Use correlations for spatial blend weight calculations instead of covariance
  inline constexpr bool opt_cascade_correlation = true;
}
