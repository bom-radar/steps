/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "io.h"
#include "configuration.h"
#include "nc.h"
#include "unit.h"
#include <hdf5.h>
#include <hdf5_hl.h>

#include <mutex>

// STEPS specific helpers and wrappers for using the NetCDF API
namespace steps::nc
{
  /// Acquire the mutex used to serialize NetCDF API calls
  auto lock_mutex() -> std::unique_lock<std::mutex>;

  /// Write a metadata store as global attributes in a NetCDF file
  /** This function expects you to hold the netcdf mutex externally before calling it.  If you call this function
   *  without already holding the netcdf mutex you may crash.  You have been warned! */
  auto write_metadata_as_attributes(metadata const& meta, nc::file& file) -> void;

  /// Release all reference files that are currently loaded
  /** This function clears the internal cache of previously loaded reference files so that they will be released
   *  immediately (or as soon as possible if the user is still holding a shared_ptr to them).  Currently this function
   *  is called by the forecast engine after the initial model state has been initialized.
   *
   *  This function locks the netcdf mutex internally.  If you call it while already holding the netcdf mutex the
   *  thread will immediately deadlock.  You have been warned! */
  auto release_reference_files() -> void;

  /// Open a reference file using the currently installed callback
  /** Note that the same shared_ptr may be returned to duplicate calls of this function.  This ensures that multiple
   *  users of the same reference file do not redundantly open it multiple times.  Reference files returned by this
   *  function are held open until the release_reference_files() function is called.  This allows reference files to
   *  be efficiently reused in disparate parts of the code where passing the returned shared pointer would be
   *  difficult.
   *
   *  If cached_only is true, then this function will only return a file if it was already availble in the cache.
   *  It not, it will return nullptr rather than attempting to directly load the file.  This is used by input_netcdf
   *  to check if the file to be used as input happens to have already been loaded as a reference file.
   *
   *  This function locks the netcdf mutex internally.  If you call it while already holding the netcdf mutex the
   *  thread will immediately deadlock.  You have been warned! */
  auto get_reference_file(string_view path, bool cached_only = false) -> shared_ptr<nc::file const>;

  /// Type used to store reference file and variable used as source of projection metadata
  using grid_reference = pair<shared_ptr<nc::file const>, string>;

  /// Get a projection reference file and variable based on special metadata fields
  /** This function locks the netcdf mutex internally through a call to get_reference_file().  If you call it while
   *  already holding the netcdf mutex the thread will immediately deadlock.  You have been warned! */
  auto get_grid_reference(metadata const& meta) -> grid_reference;

  /// Create spatial dimensions, coordinate variables, bounds and grid mapping based on universal reference file
  /** This function expects you to hold the netcdf mutex externally before calling it.  If you call this function
   *  without already holding the netcdf mutex you may crash.  You have been warned! */
  auto create_spatial_dimensions(
        nc::file& file
      , vec2i grid_shape
      , grid_reference const& ref
      ) -> std::tuple<nc::dimension*, nc::dimension*, optional<string>>;

  /// Wrapper around H5Idec_ref that only decrements valid ids
  inline auto hdf_decref_safe(hid_t id)  { if (id > 0) H5Idec_ref(id); }

  /// RAII wrapper for HDF5 API object handle
  using hdf_handle = unique_handle<hid_t, functor<hdf_decref_safe>, -1>;
}

namespace steps
{
  /// NetCDF based input
  class input_netcdf : public input
  {
  public:
    input_netcdf(configuration const& config);
    ~input_netcdf();

    auto read(time_point reference_time, int member, time_point time, array2f& data) -> bool override;
    auto read(time_point reference_time, span<int> members, time_point time, span<array2f> data) -> bool override;

  protected:
    /// Determine the path of the file which should contain the desired input (if available)
    virtual auto determine_path(time_point reference_time, int member, time_point time) const -> string = 0;

    /// Check if a file is available and open it if so
    /** This function returns an optional pair consisting of a unique_lock and a nc::file object.  If the file was
     *  not available then nullopt must be returned.  If the file was available but an error occurred during retrieval
     *  or opening, then an exception must be thrown.  If the file was available and successfully opened then the pair
     *  must be initialized with the locked netcdf API mutex as returned by steps::nc::lock_mutex, and the opened
     *  nc::file object for the file.
     *
     * The default implementation assumes data is already available locally and just looks for it on disk.  Derived
     * classes are expected to perform more complicated operations such as fetching files from remote storage. */
    virtual auto open_file(string_view path) -> shared_ptr<nc::file const> = 0;

  private:
    // state related to a currently open file
    /* We curently only have one open file at a time (cur_file_), but we could keep a cache of these open later on if
     * needed.  This would be important if the input is a 3D file per NWP member since the naive implementation of
     * only keeping the most recent file open would cause us to close and open a file for every request.  In that
     * situation we would prefer to open the file for each member and keep it open while we are looping through time
     * steps. */
    struct active_file
    {
      active_file(shared_ptr<nc::file const> f, string_view var_name);
      auto read(int member, time_point time, vec2i xy0, array2f& data) -> bool;

      shared_ptr<nc::file const>  file;
      nc::variable const*         var;
      array1i                     members;
      array1<time_point>          times;
    };

  private:
    auto read_impl(string_view path, int member, time_point time, array2f& data) -> bool;
    auto setup_unit_xform(active_file const& file) -> void;

  private:
    string                    variable_;        // name of variable containing rate or accum data
    string                    units_str_;       // input units string (detected from input if not set)
    string                    var_duration_;    // name of variable containing accum duration (optional)
    string                    var_from_;        // name of variable containing accum start time (optional)
    string                    var_till_;        // name of variable containing accum end time (optional)
    vec2i                     xy0_;             // x/y offset into input grid of slice to read (optional)
    int                       max_cur_files_;   // maximum number of files to keep open

    std::mutex                mut_file_;        // mutex to protect all the below state
    bool                      units_setup_;     // flag set after units have been setup on first read
    optional<unit::transform> unit_xform_;      // transform input units to mm/hr
    list<active_file>         cur_files_;       // list currently opened files (in LRU order)
  };

  class input_netcdf_local : public input_netcdf
  {
  public:
    input_netcdf_local(configuration const& config, metadata const& user_parameters);

  protected:
    auto determine_path(time_point reference_time, int member, time_point time) const -> string override;
    auto open_file(string_view path) -> shared_ptr<nc::file const> override;

  private:
    metadata const* user_parameters_;
    string          path_format_;     // format spec for input netcdf path
  };

  class output_netcdf : public output
  {
  public:
    output_netcdf(configuration const& config);
    ~output_netcdf();

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(array2f const& data, int d0, int d1) -> void override;
    auto write(array2<int8_t> const& data, int d0, int d1) -> void override;
    auto write_all(array2f const& data) -> void override;
    auto write_all(array2<int8_t> const& data) -> void override;

  protected:
    auto close_file() -> void;

    virtual auto create_file(time_point reference_time, int first_member) -> nc::file = 0;

  private:
    string              name_time_ref_;   // name of reference time variable
    string              name_time_;       // name of time dimension / variable
    string              name_time_start_; // name of start time variable (for accumulations)
    string              name_variable_;   // name of main output variable
    nc::data_type       data_type_;       // output data type
    int                 compression_;     // compression level
    float               scale_factor_;    // CF scale_factor for integer encodings
    float               add_offset_;      // CF add_offset for integer encodings
    float               fill_value_;      // CF _FillValue for masking
    bool                direct_hdf5_;     // compress internally and use HDF5 API to write chunks directly
    bool                flatten_series_;  // drop the series dimension if it has length 1?
    bool                flatten_time_;    // drop the time dimension if it has length 1?

    bool                expect_i8_;       // are we expecting to receive i8 data from the product?
    bool                skip_d0_;         // are we suppressing the d0 dimension due to flattening?
    bool                skip_d1_;         // are we suppressing the d1 dimension due to flattening?
    int                 ndims_;           // number of dimensions in primary variable
    int                 dimlens_[4];      // size of each dimension in primary variable

    optional<nc::file>  file_nc_;         // netcdf file handle
    nc::variable*       var_nc_;          // cached pointer to main output variable
    nc::hdf_handle      file_h5_;         // hdf5 file handle
    nc::hdf_handle      var_h5_;          // hdf5 variable handle
  };

  class output_netcdf_local : public output_netcdf
  {
  public:
    output_netcdf_local(configuration const& config, metadata const& user_parameters);

    auto finalize() -> void override;
    auto abort() -> void override;

  protected:
    auto create_file(time_point reference_time, int first_member) -> nc::file override;

  private:
    metadata const* user_parameters_;
    string          path_format_;       // format string spec to generate the output file name

    string          final_path_;  // final output path for the current file
  };
}
