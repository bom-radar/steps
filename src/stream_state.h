/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include "cascade.h"
#include "distribution_correction.h"
#include "local_statistics.h"
#include "optical_flow.h"
#include "options.h"

#include <istream>
#include <ostream>

namespace steps
{
  /// State of a stream at a particular point in time for a particular input member
  struct stream_state
  {
    using dcor_analysis_ptr = unique_ptr<distribution_correction::analysis>;

    array2f             field;            // Spatial field in log space
    array2f2            flow;             // Flow field, instantaneous for this time step
    filter              psd;              // Power spectral density
    steps::cascade      cascade;          // Cascade levels
    double              var_flow;         // Variance of flow field
    float               ac_flow;          // Lag 1 autocorrelation of the flow field
    array1d             var_psd;          // Variance of PSD at each cascade level
    array1f             ac_psd;           // Lag 1 autocorrelation of the PSD at each cascade level
    double              var_cascade;      // Variance of the cascade (partially recomposed to skill evaluation level)
    float               ac_cascade;       // Lag 1 autocorrelation of the cascade (partially recomposed to skill evaluation level)
    vector<local_stats> cascade_stats;    // Local statistics for each cascade level
    dcor_analysis_ptr   distcor_analysis; // Field analysis used by distribution correction
    bool                cold_start;       // Flags to indicate a cut-down cold start state

    stream_state(int levels, vec2i grid_shape, vec2i psd_shape)
      : field{grid_shape}
      , flow{grid_shape}
      , psd{psd_shape}
      , cascade{levels, grid_shape}
      , var_flow{std::numeric_limits<double>::quiet_NaN()}
      , ac_flow{std::numeric_limits<float>::quiet_NaN()}
      , var_psd{levels}
      , ac_psd{levels}
      , var_cascade{std::numeric_limits<float>::quiet_NaN()}
      , ac_cascade{std::numeric_limits<float>::quiet_NaN()}
      , cascade_stats(levels)
      , cold_start{false}
    {
      for (auto lvl = 0; lvl < levels; ++lvl)
        cascade_stats[lvl].resize(grid_shape);
    }

    auto levels() const { return cascade.size(); }
    auto grid_shape() const { return field.shape(); }
    auto psd_shape() const { return psd.shape(); }

    /// Write stream state to an output stream
    auto write(std::ostream& os, int compression, int precision_reduction) const -> std::ostream&;

    /// Read stream state from an input stream
    auto read(std::istream& is) -> std::istream&;
  };
}
