/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "core.h"
#include "autoregressive_model.h"
#include "blend_weights.h"
#include "cf.h"
#include "engine.h"
#include "options.h"
#include "profile.h"
#include "thread_pool.h"
#include "trace.h"
#include "unit_test.h"
#include <atomic>
#include <thread>

#include <fstream>

using namespace steps;

static profile prof_exec{"core::generate_forecasts"};
static profile prof_spread_wait{"core::spread limit wait"};

static auto make_default_autocorrelations(filter_bank const& filters, configuration const& config)
try
{
  // is it an explicit array of correlations? (one per level)
  if (config.type() == configuration::node_type::array)
  {
    auto acs = config_array_to_container<array1f>(config);
    if (acs.size() != filters.size())
      throw std::runtime_error{"Number of explicit values does not match number of levels"};
    return acs;
  }

  // okay, must be a parametric setup
  auto parametric = autocorrelations{
      double(config["scale_1"])
    , double(config["value_1"])
    , double(config["scale_2"])
    , double(config["value_2"])};
  array1f acs{filters.size()};
  for (auto lvl = 0; lvl < acs.size(); ++lvl)
    acs[lvl] = parametric.determine_autocorrelation(filters.central_wavelength(lvl));
  return acs;
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{"Failed to initialize default autocorrelations"});
}

/* the value calculated here is the exponent 'n' required to ensure that raising some autocorrelation to a power of 'n'
 * results in a value of 0.5.  we use this to scale our autocorrelations such that large scales (high autocorrelations)
 * are allowed to use absolute cascade blending, while small scales (low autocorrelations) are forced to rely on AR(1)
 * parameter blending and natural evolution only. */
static auto calculate_blend_ac_exponent(double blend_ac_halfrate) -> float
{
  return std::log(0.5) / std::log(blend_ac_halfrate);
}

auto steps::determine_fft_shape(configuration const& config, vec2i grid_shape, int anti_ring_border) -> vec2i
{
  auto shape = vec2i{config.optional("fft_cols", grid_shape.x * 2), config.optional("fft_rows", grid_shape.y * 2)};
  auto min_shape = vec2i{grid_shape.x + anti_ring_border * 2, grid_shape.y + anti_ring_border * 2};
  if (shape.x < min_shape.x || shape.y < min_shape.y)
    throw std::runtime_error{format("Invalid shape for fft ({}), must be at least ({})", shape, min_shape)};
  return shape;
}

core::core(configuration const& config)
  : user_parameters_{parse_metadata(config.optional("user_parameters", configuration{}), metadata{})}
  , members_{config["members"]}
  , member_offset_{config["member_offset"]}
  , control_member_{config.optional("control_member", std::numeric_limits<int>::lowest())}
  , forecasts_{config["forecasts"]}
  , time_step_{config["time_step"]}
  , levels_{config["levels"]}
  , grid_shape_{int(config["grid_cols"]), int(config["grid_rows"])}
  , grid_resolution_{config["grid_resolution"]}
  , minimum_rain_rate_{config["minimum_rain_rate"]}
  , minimum_rain_area_{config.optional("minimum_rain_area", std::numeric_limits<float>::quiet_NaN())}
  , null_forecast_mode_{config.optional("null_forecast_mode", null_forecast_mode::none)}
  , parameter_radius_{config["parameter_radius"]}
  , spatial_skill_levels_{config.optional("spatial_skill_levels", 1)}
  , obs_skill_scale_{config.optional("obs_skill_scale", 1.0f)}
  , perturb_flow_{config["perturb_flow"]}
  , advect_flow_{config["advect_flow"]}
  , blend_halflife_{config.optional("blend_halflife", duration{})}
  , blend_ac_halfrate_{config.optional("blend_ac_halfrate", std::numeric_limits<float>::quiet_NaN())}
  , blend_limit_{calculate_blend_factor_for_halflife(time_step_, blend_halflife_)}
  , blend_ac_exponent_{calculate_blend_ac_exponent(blend_ac_halfrate_)}
  , threshold_output_{config["threshold_output"]}
  , mask_advected_in_{config["mask_advected_in"]}
  , random_seed_{config.optional("random_seed", 0)}
  , disable_advection_{config.optional("disable_advection", false)}
  , disable_evolution_{config.optional("disable_evolution", false)}
  , test_pattern_{config.optional("test_pattern", configuration{})}
  , skills_path_{config.optional("skills_path", "")}
  , diagnostics_path_{config.optional("diagnostics", "")}
  , engines_{config.optional("engines", members_)}
  , max_engine_spread_{config.optional("max_engine_spread", -1)}
  , next_member_{member_offset_}
  , anti_ring_border_{config["anti_ring_border_width"]}
  , fft_{determine_fft_shape(config, grid_shape_, anti_ring_border_), config.optional("fft_threads", 1)}
  , transform_{transform::instantiate(config["transform"])}
  , log_zero_{transform_->forward(0.0f)}
  , tracker_{optical_flow::instantiate(grid_shape_, grid_resolution_, log_zero_, config["optical_flow"])}
  , filters_{
      fft_
    , grid_resolution_
    , levels_
    , float(config["rolloff"])
    , config_array_to_container<array1f>(config["crossovers"])}
  , default_acs_{make_default_autocorrelations(filters_, config["autocorrelations_default"])}
  , default_psd_{make_parametric_psd(
            fft_
          , grid_resolution_
          , config.optional("psd_scale_break", 32.0)
          , config.optional("psd_beta_1", 1.5)
          , config.optional("psd_beta_2", 2.5))}
  , diagnosed_levels_{[&]()
    {
      auto limit = config.optional("psd_diagnosis_limit", 0.0f);
      auto lvl = 0;
      while (lvl < filters_.size() && filters_.central_wavelength(lvl) > limit)
        ++lvl;
      return lvl;
    }()}
  , distcor_{distribution_correction::instantiate(
            time_step_
          , grid_shape_
          , grid_resolution_
          , minimum_rain_rate_
          , *transform_
          , config["distribution_correction"])}
  , products_{
      members_
    , member_offset_
    , forecasts_
    , time_step_
    , grid_shape_
    , grid_resolution_
    , config["product"]
    , user_parameters_}
{
  // load the static mask from file
  if (auto conf_msk = config.find("mask_static"))
  {
    auto buf = array2f{grid_shape_};
    // get reference file must occur outside the netcdf lock
    auto file = nc::get_reference_file(format((*conf_msk)["path"].string(), "param"_a = user_parameters_));
    {
      auto lock = nc::lock_mutex();
      auto& var = file->lookup_variable((*conf_msk)["variable"].string());
      if (var.dimensions().size() < 2)
        throw std::runtime_error{"Mask variable has unexpected rank"};
      if (   var.dimensions()[var.dimensions().size() - 2]->size() != grid_shape_.y
          || var.dimensions()[var.dimensions().size() - 1]->size() != grid_shape_.x)
        throw std::runtime_error{"Mask variable grid size mismatch"};
      auto packer = cf::packer{};
      auto start = array1i{var.dimensions().size() - 2};
      start.fill(0);
      packer.read(var, buf, std::numeric_limits<float>::quiet_NaN(), start);
    }
    mask_static_.emplace(grid_shape_);
    for (auto i = 0; i < buf.size(); ++i)
      mask_static_->data()[i] = std::isnan(buf.data()[i]) ? 0 : 1;
    mask_static_advect_ = bool((*conf_msk)["advect"]);
    if (mask_static_advect_ && !mask_advected_in_)
    {
      trace(trace_level::warning, "Enabling advection of static mask implies masking of advected in areas");
      mask_advected_in_ = true;
    }
  }

  if (auto conf_obs = config.find("observation"))
  {
    try
    {
      observation_.emplace(*this, "observation", *conf_obs, true);
    }
    catch (...)
    {
      std::throw_with_nested(std::runtime_error{format("Failed to configure stream {}", "observation"sv)});
    }
  }

  if (auto conf_mod = config.find("models"))
  {
    models_.reserve(conf_mod->object().size());
    for (auto& config_stream : conf_mod->object())
    {
      try
      {
        models_.emplace_back(*this, config_stream.first, config_stream.second, false);
      }
      catch (...)
      {
        std::throw_with_nested(std::runtime_error{format("Failed to configure stream {}", config_stream.first)});
      }
    }
  }

  // sanity checks
  if (!models_.empty())
  {
    // prevent advection of flow vectors when also blending flow.  see comments in engine::blend_flow()
    if (advect_flow_)
      throw std::runtime_error{"Cannot use advect_flow option when model inputs are configured"};
    if (blend_halflife_ == duration{})
      throw std::runtime_error{"Model blend halflife required when model inputs are configured"};
    if (std::isnan(blend_ac_halfrate_))
      throw std::runtime_error{"Model blend autocorrelation half-rate required when model inputs are configured"};
  }
}

auto core::preprocess_model(string_view model_name, time_point reference_time) -> void
{
  // find the model
  auto model = 0;
  while (models_[model].name() != model_name)
    ++model;
  if (model == int(models_.size()))
    throw std::runtime_error{format("Unknown model '{}'", model_name)};

  // limit engine count to the number of members
  auto engines = std::min(engines_, members_);

  // reset engine state
  fatal_error_ = nullptr;
  next_member_ = member_offset_;
  step_index_.resize(engines);
  std::fill(step_index_.begin(), step_index_.end(), -1);
  step_index_min_ = members_ * (forecasts_ + 1);
  step_index_max_ = -1;

  // launch the forecast engines
  /* the naive assumption is that processing a single member would be enough to generate the
   * cache files for a NWP model, however this is not always the case.  if the NWP input is
   * itself an ensemble then different STEPS members may be mapping to different NWP members
   * and therefore different cache files.  for this reason it is necessary to preprocess all
   * members.  in the common case of a deterministic NWP input only one thread will generate
   * each state and the others will just sit idle (not a problem). */
  thread_pool_distribute_loop(engines, [&](int i){ thread_preprocess(i, model, reference_time); });

  // rethrow the first fatal exception that occured in the engine threads (if any)
  if (fatal_error_)
    std::rethrow_exception(fatal_error_);
}

auto core::generate_forecasts(time_point reference_time) -> forecast_info
{
  auto ps = profile::scope{prof_exec};

  // abort run and output null product set if observations are empty
  if (handle_null_forecast(reference_time))
    return forecast_info{ true };

  // limit engine count to the number of members
  auto engines = std::min(engines_, members_);

  // reset engine state
  fatal_error_ = nullptr;
  next_member_ = member_offset_;
  step_index_.resize(engines);
  std::fill(step_index_.begin(), step_index_.end(), -1);
  step_index_min_ = members_ * (forecasts_ + 1);
  step_index_max_ = -1;

  initialize_diagnostics(reference_time);
  load_skills();

  try
  {
    // initialize output products
    products_.initialize(reference_time);

    // launch the forecast engines using our thread as engine 0, then wait for them to complete
    thread_pool_distribute_loop(engines, [&](int i){ thread_forecast(i, reference_time); });

    // rethrow the first fatal exception that occured in the engine threads (if any)
    if (fatal_error_)
      std::rethrow_exception(fatal_error_);

    // finalize output products
    products_.finalize();
  }
  catch (...)
  {
    // try to clean up any half written products
    products_.abort();
    throw;
  }

  store_skills();
  shutdown_diagnostics();

  return forecast_info{ false };
}

auto core::handle_null_forecast(time_point reference_time) -> bool
{
  // did user disable minimum raining area condition?
  if (std::isnan(minimum_rain_area_))
    return false;

  // are we running an unconditional nowcast (i.e. nothing to check WAR against)?
  if (!observation_)
    return false;

  // grab the input grid for the reference time
  /* This _seems_ like double handling of the input grid since we are about to read it in the input stream.  Technically
   * it is, however it's not as bad as you might think.  The NetCDF I/O class keeps the last read file open, which means
   * that as long as the first thing the stream class does is read our file, then we are at least not closing/reopening
   * (or even worse when on cloud - downloading twice).  The only double handling is the actual reading of the variable
   * which is not so bad considering the very substantial amount of time saved when there is no rain, which is most of
   * the time for most radars! */
  auto data = array2f{grid_shape_};
  if (!observation_->read_and_backfill(member_offset_, reference_time, data))
    throw std::runtime_error{format("Failed to read observation for time {}", reference_time)};

  // check the raining area threshold
  auto area = data.count_greater_equal(minimum_rain_rate_) * grid_resolution_ * grid_resolution_;
  if (area >= minimum_rain_area_)
    return false;

  trace(trace_level::log, "Low observation raining area of {}km^2 triggered null forecast mode", area);

  if (null_forecast_mode_ == null_forecast_mode::none)
    return true;

  // create a blank grid with either zeros or nans according to user preference
  data.fill(null_forecast_mode_ == null_forecast_mode::missing ? std::numeric_limits<float>::quiet_NaN() : 0.0f);
  if (null_forecast_mode_ == null_forecast_mode::zero && mask_static_)
  {
    for (auto i = 0; i < data.size(); ++i)
      if (!mask_static_->data()[i])
        data.data()[i] = std::numeric_limits<float>::quiet_NaN();
  }

  // output the null product set
  try
  {
    products_.initialize(reference_time);
    products_.write_all(data);
    products_.finalize();
  }
  catch (...)
  {
    products_.abort();
    throw;
  }

  return true;
}

auto core::load_skills() -> void
try
{
  if (models_.empty() || skills_path_.empty())
    return;

  auto data = load_configuration(skills_path_, true);
  for (auto& model : models_)
  {
    if (auto c = data.find(model.name()))
      model.load_skills(*c);
    else
      trace(trace_level::warning, "No climatological skills available for model {}", model.name());
  }
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to load skills path {}", skills_path_)});
}

auto core::store_skills() -> void
try
{
  if (models_.empty() || skills_path_.empty())
    return;

  auto data = configuration{};
  for (auto& model : models_)
    model.store_skills(data[model.name()]);
  save_configuration(skills_path_, data);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Failed to store skills path {}", skills_path_)});
}

auto core::initialize_diagnostics(time_point reference_time) -> void
{
  if (diagnostics_path_.empty())
    return;

  if (step_index_.size() != 1)
    throw std::runtime_error{"Diagnostic file enabled while engine count > 1"};

  diagnostics::initialize(format(diagnostics_path_, "udp"_a = user_parameters_, "reference"_a = reference_time));

  // setup dimensions
  diagnostics::create_dimension("forecast", forecasts_ + 1);
  diagnostics::create_dimension("model", models_.size());
  diagnostics::create_dimension("stream", observation_ ? models_.size() + 1 : models_.size());
  diagnostics::create_dimension("level", levels_);
  diagnostics::create_dimension("y", grid_shape_.y);
  diagnostics::create_dimension("x", grid_shape_.x);
  diagnostics::create_dimension("sy", fft_.shape_c().y);
  diagnostics::create_dimension("sx", fft_.shape_c().x);
  diagnostics::create_dimension("uv", 2);

  // setup variables
  diagnostics::create_variable("model", nc::data_type::string, {"model"}, "Model name");
  diagnostics::create_variable("stream", nc::data_type::string, {"stream"}, "Stream name");
  diagnostics::create_variable("time", nc::data_type::f64, {"forecast"}, "Forecast time", "seconds since 1970-01-01 00:00:00 UTC");

  diagnostics::create_variable("lead_time", nc::data_type::i32, {"forecast"}, "Forecast lead time", "seconds");
  diagnostics::create_variable("filters", nc::data_type::f32, {"level", "sy", "sx"}, "Cascade filter bank");
  diagnostics::create_variable("wavelength", nc::data_type::f32, {"level"}, "Central wavelength of cascade level", "km");

  diagnostics::create_variable("def_psd", nc::data_type::f32, {"sy", "sx"}, "Default PSD");

  diagnostics::create_variable("mean_c", nc::data_type::f32, {"forecast", "level"}, "Process mean for cascade");
  diagnostics::create_variable("std_c", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Process standard deviation for cascade");
  diagnostics::create_variable("ac_c", nc::data_type::f32, {"forecast", "level"}, "Cascade autocorrelation");

  diagnostics::create_variable("skills_f", nc::data_type::f32, {"forecast", "stream"}, "Flow skills");
  diagnostics::create_variable("skills_p", nc::data_type::f32, {"forecast", "level", "stream"}, "PSD skills");
  diagnostics::create_variable("skills_c", nc::data_type::f32, {"forecast", "stream"}, "Cascade skills");

  diagnostics::create_variable("weight_f", nc::data_type::f32, {"forecast", "stream"}, "Flow blend weights");
  diagnostics::create_variable("weight_p", nc::data_type::f32, {"forecast", "level", "stream"}, "PSD blend weights");
  diagnostics::create_variable("weight_c", nc::data_type::f32, {"forecast", "stream"}, "Cascade blend weights");

  diagnostics::create_variable("flow", nc::data_type::f32, {"forecast", "y", "x", "uv"}, "Flow field");
  diagnostics::create_variable("flow_mag", nc::data_type::f32, {"forecast", "y", "x"}, "Flow field magnitude");
  diagnostics::create_variable("flow_ang", nc::data_type::f32, {"forecast", "y", "x"}, "Flow field angle");

  diagnostics::create_variable("psd", nc::data_type::f32, {"forecast", "sy", "sx"}, "PSD");

  diagnostics::create_variable("noise", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Random input for AR(n) model");
  diagnostics::create_variable("noise-f", nc::data_type::f32, {"forecast", "y", "x"}, "Unconditional simulation recompsed noise cascade");

  diagnostics::create_variable("lag1", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Lag-1 input of AR(n) model");
  diagnostics::create_variable("lag1-f", nc::data_type::f32, {"forecast", "y", "x"}, "Recomposed lag-1 field");
  diagnostics::create_variable("lag0", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Lag-0 output of AR(n) model");
  diagnostics::create_variable("lag0-f", nc::data_type::f32, {"forecast", "y", "x"}, "Recomposed lag-0 field");

  diagnostics::create_variable("mod-fld", nc::data_type::f32, {"forecast", "model", "y", "x"}, "Model input fields");
  diagnostics::create_variable("models", nc::data_type::f32, {"forecast", "model", "level", "y", "x"}, "Model cascades");

  diagnostics::create_variable("obs-mean", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Observation local means");
  diagnostics::create_variable("obs-std", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Observation local standard deviations");
  diagnostics::create_variable("obs-ac", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Observation local autocorrelations");

  diagnostics::create_variable("mod-mean", nc::data_type::f32, {"forecast", "model", "level", "y", "x"}, "Model local means");
  diagnostics::create_variable("mod-std", nc::data_type::f32, {"forecast", "model", "level", "y", "x"}, "Model local standard deviations");
  diagnostics::create_variable("mod-ac", nc::data_type::f32, {"forecast", "model", "level", "y", "x"}, "Model local autocorrelations");

  #if opt_debug_ar1_parameters
  diagnostics::create_variable("ar1-mean", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Process mean for AR(1) model");
  diagnostics::create_variable("ar1-std", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Process standard deviation for AR(1) model");
  diagnostics::create_variable("ar1-ac", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Process standard deviation for AR(1) model");
  diagnostics::create_variable("blend-factor", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Blend amount used in direct blending of high autocor values");
  diagnostics::create_variable("blend-delta", nc::data_type::f32, {"forecast", "level", "y", "x"}, "Delta between real lag1 and direct blended lag1");
  #endif

  diagnostics::create_variable("dcor-f", nc::data_type::f32, {"forecast", "y", "x"}, "Distribution corrected field");
  diagnostics::create_variable("output", nc::data_type::f32, {"forecast", "y", "x"}, "Output field in mm/hr");

  // write static data
  auto names = vector<string>(models_.size());
  for (size_t i = 0; i < models_.size(); ++i)
    names[i] = models_[i].name();
  diagnostics::write("model", names);

  if (observation_)
    names.emplace_back("Obs");
  diagnostics::write("stream", names);

  auto times = array1d{forecasts_ + 1};
  for (auto i = 0; i < times.size(); ++i)
    times[i] = clock::to_time_t(reference_time + i * time_step_);
  diagnostics::write("time", times);

  auto leads = array1i{forecasts_ + 1};
  for (auto i = 0; i < leads.size(); ++i)
    leads[i] = std::chrono::duration_cast<std::chrono::seconds>(time_step_ * i).count();
  diagnostics::write("lead_time", leads);

  for (auto lvl = 0; lvl < levels_; ++lvl)
    diagnostics::write("filters", filters_[lvl], {lvl});

  auto wavelengths = array1f{levels_};
  for (auto lvl = 0; lvl < levels_; ++lvl)
    wavelengths[lvl] = filters_.central_wavelength(lvl);
  diagnostics::write("wavelength", wavelengths);

  diagnostics::write("def_psd", default_psd_);
}

auto core::shutdown_diagnostics() -> void
{
  // TODO - write any closing metadata and/or stats
  diagnostics::shutdown();
}

auto core::thread_preprocess(int engine, int model, time_point reference_time) -> void
{
  auto prefix_e = trace_prefix{format("e{}", engine)};

  // run the engine
  while (true)
  {
    int member;
    {
      auto lock = std::lock_guard<std::mutex>{mut_engines_};
      if (fatal_error_)
        break;
      if (next_member_ == member_offset_ + members_)
        break;
      member = next_member_++;
    }
    auto prefix_m = trace_prefix{format("m{}", member)};

    try
    {
      for (auto fc = 0; fc <= forecasts_; ++fc)
      {
        auto time = reference_time + fc * time_step_;
        auto prefix = trace_prefix{format("t{}", fc)};

        // check for early termination and wait for slower engines to catch up if necessary
        if (!engine_commence_time_step(engine, reference_time, false))
          break;

        trace(trace_level::verbose, "Preprocessing time step {}", fc);
        if (!models_[model].determine_state(member, time))
          throw std::runtime_error{format("Failed to preprocess time step {} for time {}", fc, time)};

        // inform driver code that we have completed this time step
        engine_notify_progress(member, fc);
      }
    }
    catch (...)
    {
      trace(trace_level::log, "Terminating preprocessing due to exception");
      auto lock = std::lock_guard<std::mutex>{mut_engines_};
      if (!fatal_error_)
        fatal_error_ = std::current_exception();
      cv_spread_.notify_all();
    }
  }

  // remove ourselves from the spread calculations
  engine_commence_time_step(engine, reference_time, true);
}

auto core::thread_forecast(int engine, time_point reference_time) -> void
{
  auto prefix_e = trace_prefix{format("e{}", engine)};

  auto eng = steps::engine{*this, engine};

  // run the engine
  while (true)
  {
    int member;
    {
      auto lock = std::lock_guard<std::mutex>{mut_engines_};
      if (fatal_error_)
        break;
      if (next_member_ == member_offset_ + members_)
        break;
      member = next_member_++;
    }
    auto prefix_m = trace_prefix{format("m{}", member)};

    try
    {
      eng.member_forecast(reference_time, member);
    }
    catch (...)
    {
      trace(trace_level::log, "Terminating forecast due to exception");
      auto lock = std::lock_guard<std::mutex>{mut_engines_};
      if (!fatal_error_)
        fatal_error_ = std::current_exception();
      cv_spread_.notify_all();
    }
  }

  // remove ourselves from the spread calculations
  engine_commence_time_step(engine, reference_time, true);
}

auto core::engine_commence_time_step(int engine, time_point reference_time, bool reset) -> bool
{
  auto lock = std::unique_lock<std::mutex>{mut_engines_};

  // check for termination request
  if (fatal_error_)
    return false;

  // adjust the step index for our engine
  auto old_index = step_index_[engine];
  step_index_[engine] = reset ? -1 : step_index_[engine] + 1;

  // if we are completely inside the active range then we can early out
  if (old_index > step_index_min_ && old_index < step_index_max_)
    return true;

  // determine the current range of steps being worked on by the engines
  // note the +1 to forecasts_. this is due to our 'extra' step at the start (fc 0) for initialization
  int min = members_ * (forecasts_ + 1), max = -1;
  for (auto c : step_index_)
  {
    if (c == -1)
      continue;
    if (c < min)
      min = c;
    if (c > max)
      max = c;
  }

  // if there are no engines running then early out (occurs on reset of last engine to terminate)
  if (max < 0)
    return true;

  // enable this to help analyize engine spread
  if constexpr (opt_debug_engine_spread)
  {
    string str;
    for (auto i : step_index_)
      str += format("{:4}", i % (forecasts_ + 1));
    trace(trace_level::verbose, "spread: {} ({} - {}) = {}", str, min % (forecasts_ + 1), max % (forecasts_ + 1), max - min);
  }

  // notify the streams if the active time range has changed
  if (min != step_index_min_ || max != step_index_max_)
  {
    // store the updated min and max indexes
    step_index_min_ = min;
    step_index_max_ = max;

    // notify all streams of the adjusted active time range
    auto min_time = reference_time + (step_index_min_ % (forecasts_ + 1)) * time_step_;
    auto max_time = reference_time + (step_index_max_ % (forecasts_ + 1)) * time_step_;
    for (auto& m : models_)
      m.notify_active_time_range(min_time, max_time);
  }

  // enforce spread restriction if enabled
  /* If this value is set to 0 then all engines must finish time T before any engine will start work on time T+1.  If
   * the value is set to -1 then there are no restrictions and engines will run at maximum speed regardless of what
   * time the other engines are up to.  Setting the value to a non-zero value can be useful to ensure that no one
   * engine gets too far ahead.  When streams are exploiting caches this can help restrict cache size. */
  if (max_engine_spread_ >= 0)
  {
    // if we are the lagging engine check if we can unblock other engine threads
    if (step_index_[engine] == step_index_min_)
    {
      if (step_index_max_ - step_index_min_ <= max_engine_spread_)
        cv_spread_.notify_all();
    }
    // if we are a leading engine check if we have hit the spread limit
    else if (step_index_[engine] == step_index_max_)
    {
      while (step_index_max_ - step_index_min_ > max_engine_spread_)
      {
        auto ps = profile::scope{prof_spread_wait};
        cv_spread_.wait(lock);
        if (fatal_error_)
          return false;
      }
    }
  }

  return true;
}

auto core::engine_notify_progress(int member, int forecast) -> void
{
  if (cb_progress_)
    cb_progress_(member, forecast);
}
