/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "cascade.h"
#include "fourier_transform.h"
#include "options.h"
#include <random>

namespace steps
{
  /// Adopt Mersenne Twister 19937 generator as our standard random number engine
  using random_engine = std::mt19937;

  /// Initialize a Mersenne Twister random number engine using a true source of randomness
  /** This method of creating a random engine uses std::random_device to initialize the engine with a real source
   *  of randomness (or as close as possible) from the system. */
  auto init_random_engine() -> random_engine;

  /// Seed a Mersenne Twister random number engine using a specific known value
  /** This method of creating a random engine allows for reliable reproduction of the forecast by using a known
   *  fixed seed value.  This functionality is intended to assist with assessing the impact of engine parameters by
   *  allowing the same forecast to be rerun using the same noise values but different settings.  During operational
   *  use it is strongly recommended to use the init_random_engine() method instead. */
  auto seed_random_engine(int seed) -> random_engine;

  /// Generate a spectrum corresponding to N(0,1) white noise
  auto generate_noise_spectrum(fourier_transform const& fft, random_engine& rng) -> fourier_transform::array2c;

  /// Generate an N(0,1) spatial field which is contrained to a given PSD using an input N(0,1) white noise spectrum
  auto generate_n01_spatial_noise(
        fourier_transform const& fft
      , filter const& psd
      , fourier_transform::array2c const& noise_spectrum
      , array2f& noise_spatial
      ) -> void;

  #if 0
  /// Stochastic simulation manager
  class stochastic
  {
  public:
    stochastic(fourier_transform const& fft, filter_bank const& filters, int seed, bool spatial = opt_spatial_noise);

    /// Access internal the random number engine
    auto& random_engine() { return random_engine_; }

    /// Generate a white noise spectrum
    auto generate_noise_spectrum() -> fourier_transform::array2c;

    /// Generate a realization of the stochastic noise cascade using the provided PSDs
    auto generate_noise(
          fourier_transform::array2c const& noise
        , filter const& psd_l   ///< PSD to use for low frequency levels
        , filter const& psd_h   ///< PSD to use for high frequency levels
        , int crossover_level   ///< Index of first level for which to use high frequency PSD
        , cascade& cascade      ///< Output cascade
        ) -> void;

  private:
    fourier_transform const*        fft_;           // Fourier transform manager
    filter_bank const*              filters_;       // Bandpass filters
    bool                            spatial_;       // Whether to use the spatial or spectral generation technique
    std::mt19937                    random_engine_; // Psuedo-random number generator
    std::normal_distribution<float> dist_N_0_1_;    // N(0,1) distribution
    std::normal_distribution<float> dist_N_0_05_;   // N(0,1/2) distribution
  };
  #endif
}
