/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "advection.h"
#include "array2.h"
#include "configuration.h"
#include "transform.h"

namespace steps
{
  auto make_test_pattern(configuration const& config, transform const& xform, array2f& out) -> void;
  auto make_test_pattern(configuration const& config, transform const& xform, array2f2& out) -> void;

  auto test_pattern_grid(configuration const& config, transform const& xform, array2f& out) -> void;
  auto test_pattern_random(configuration const& config, transform const& xform, array2f& out) -> void;
  auto test_pattern_squares(configuration const& config, transform const& xform, array2f& out) -> void;

  auto test_pattern_uniform(configuration const& config, transform const& xform, array2f2& out) -> void;
  auto test_pattern_square(configuration const& config, transform const& xform, array2f2& out) -> void;
  auto test_pattern_rotation(configuration const& config, transform const& xform, array2f2& out) -> void;
}
