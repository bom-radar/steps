/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "distribution_correction.h"
#include "advection.h"
#include "profile.h"
#include "statistics.h"
#include "trace.h"
#include "unit_test.h"

using namespace steps;

static profile prof_distcor_init{"distribution correction setup"};
static profile prof_distcor_run{"distribution correction"};

enum class distribution_correction::analysis::type_id
{
    none
  , probability_matching
  , probability_matching_adv
  , shift_scale
  , local_war
};

auto distribution_correction::instantiate(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config
    ) -> unique_ptr<distribution_correction>
{
  auto const& type = config["type"].string();
  if (type == "local-war")
    return std::make_unique<distcor_local_war>(time_step, grid_shape, grid_resolution, min_rain_rate, xform, config);
  if (type == "clamp")
    return std::make_unique<distcor_clamp>(time_step, grid_shape, grid_resolution, min_rain_rate, xform, config);
  if (type == "probability-matching-adv")
    return std::make_unique<distcor_probability_matching_adv>(time_step, grid_shape, grid_resolution, min_rain_rate, xform, config);
  if (type == "probability-matching")
    return std::make_unique<distcor_probability_matching>(time_step, grid_shape, grid_resolution, min_rain_rate, xform, config);
  if (type == "shift-scale")
    return std::make_unique<distcor_shift_scale>(time_step, grid_shape, grid_resolution, min_rain_rate, xform, config);
  if (type == "none")
    return std::make_unique<distcor_none>(time_step, grid_shape, grid_resolution, min_rain_rate, xform, config);
  throw std::runtime_error{format("Invalid distribution correction manager type {}", type)};
}

auto distribution_correction::write_analysis(unique_ptr<analysis> const& a, std::ostream& os) -> void
{
  auto id = a ? a->type() : analysis::type_id::none;
  os.write(reinterpret_cast<char const*>(&id), sizeof(id));
  if (a)
    a->write(os);
}

auto distribution_correction::read_analysis(std::istream& is) -> unique_ptr<analysis>
{
  auto id = analysis::type_id::none;
  is.read(reinterpret_cast<char*>(&id), sizeof(id));
  switch (id)
  {
  case analysis::type_id::none:
    return nullptr;
  case analysis::type_id::probability_matching:
    return std::make_unique<distcor_probability_matching::dc_analysis>(is);
  case analysis::type_id::probability_matching_adv:
    return std::make_unique<distcor_probability_matching_adv::dc_analysis>(is);
  case analysis::type_id::shift_scale:
    return std::make_unique<distcor_shift_scale::dc_analysis>(is);
  case analysis::type_id::local_war:
    return std::make_unique<distcor_local_war::dc_analysis>(is);
  }
  throw std::runtime_error{format("Invalid distribution correction analysis type {}", static_cast<int>(id))};
}

auto distribution_correction::analyse(array2f const& field) const -> unique_ptr<analysis>
{
  return nullptr;
}

auto distribution_correction::initialize(analysis const* obs, span<analysis const*> mod) const -> std::any
{
  return {};
}

distcor_none::distcor_none(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config)
{ }

auto distcor_none::correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void
{ }

distcor_clamp::distcor_clamp(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config)
  : max_rate_{config["max_rate"]}
  , xform_max_rate_{xform.forward(max_rate_)}
{ }

auto distcor_clamp::correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void
{
  for (auto i = 0; i < field.size(); ++i)
    if (field.data()[i] > xform_max_rate_)
      field.data()[i] = xform_max_rate_;
}

distcor_probability_matching::distcor_probability_matching(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config)
{ }

auto distcor_probability_matching::analyse(array2f const& field) const -> unique_ptr<analysis>
{
  auto ps = profile::scope(prof_distcor_init);

  // sort the field values to produce our target distribution
  auto dca = std::make_unique<dc_analysis>(field.size());
  std::partial_sort_copy(field.begin(), field.end(), dca->sorted.begin(), dca->sorted.end());
  return dca;
}

auto distcor_probability_matching::initialize(analysis const* obs, span<analysis const*> mod) const -> std::any
{
  auto ps = profile::scope(prof_distcor_init);

  // copy the observation analysis as our main state (just reuse our analysis type)
  auto dca = dynamic_cast<dc_analysis const*>(obs);
  if (!dca)
    throw std::runtime_error{"Unexpected dc_analysis type"};
  return std::make_any<dc_analysis>(*dca);
}

auto distcor_probability_matching::correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void
{
  auto ps = profile::scope(prof_distcor_run);

  auto& dcs = std::any_cast<dc_analysis&>(state);
  if (dcs.sorted.size() != field.size())
    throw std::logic_error{"Array size mismatch"};

  // create a sorted index of the new field
  auto index = array1i{field.size()};
  for (auto i = 0; i < field.size(); ++i)
    index[i] = i;
  std::sort(index.begin(), index.end(), [&](auto lhs, auto rhs) { return field.data()[lhs] < field.data()[rhs]; });

  // remap the ranked input values to the target distribution
  for (auto i = 0; i < field.size(); ++i)
    field.data()[index[i]] = dcs.sorted[i];
}

distcor_probability_matching::dc_analysis::dc_analysis(std::istream& is)
{
  auto size = 0;
  is.read(reinterpret_cast<char*>(&size), sizeof(size));
  sorted.resize(size);
  is.read(reinterpret_cast<char*>(sorted.data()), sorted.size_bytes());
}

auto distcor_probability_matching::dc_analysis::type() const -> type_id
{
  return type_id::probability_matching;
}

auto distcor_probability_matching::dc_analysis::write(std::ostream& os) const -> void
{
  auto size = sorted.size();
  os.write(reinterpret_cast<char const*>(&size), sizeof(size));
  os.write(reinterpret_cast<char const*>(sorted.data()), sorted.size_bytes());
}

distcor_shift_scale::distcor_shift_scale(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config)
  : xform_{&xform}
  , xform_min_rain_{xform_->forward(min_rain_rate)}
  , xform_zero_{xform_->forward(0.0f)}
{ }

auto distcor_shift_scale::analyse(array2f const& field) const -> unique_ptr<analysis>
{
  auto ps = profile::scope(prof_distcor_init);

  // determine the wet area ratio and conditional mean rain (in mm/hr)
  // TODO - not 100% sure this should be the conditional mean...
  auto count = 0;
  auto mean = 0.0;
  for (auto i = 0; i < field.size(); ++i)
  {
    if (field.data()[i] >= xform_min_rain_)
    {
      mean += xform_->inverse(field.data()[i]);
      ++count;
    }
  }
  auto dca = std::make_unique<dc_analysis>();
  dca->wet_area_ratio = float(count) / field.size();
  dca->mean_rainfall = mean / count;
  return dca;
}

auto distcor_shift_scale::initialize(analysis const* obs, span<analysis const*> mod) const -> std::any
{
  auto ps = profile::scope(prof_distcor_init);

  // copy the observation analysis as our main state (just reuse our analysis type)
  auto dca = dynamic_cast<dc_analysis const*>(obs);
  if (!dca)
    throw std::runtime_error{"Unexpected dc_analysis type"};
  return std::make_any<dc_analysis>(*dca);
}

auto distcor_shift_scale::correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void
{
  auto ps = profile::scope(prof_distcor_run);

  auto& dcs = std::any_cast<dc_analysis&>(state);

  constexpr auto initial_scale = 1.0f;
  constexpr auto tol_ratio = 0.01f;

  // create a sorted array of our input data
  auto z = field;
  std::sort(z.data(), z.data() + z.size());

  // determine initial conditions
  auto shift_idx = int((1 - dcs.wet_area_ratio) * field.size()) - 1;
  auto shift = z.data()[std::clamp(shift_idx, 0, field.size() - 1)];

  // lambda used as error function to minimize in nelder_mead
  auto get_error = [&](float scale)
  {
    auto mean = 0.0f;
    auto count = 0;
    for (auto i = 0; i < field.size(); ++i)
    {
      if (z.data()[i] > shift)
      {
        mean += xform_->inverse(z.data()[i] * scale);
        ++count;
      }
    }
    mean /= count;

    return std::abs(mean - dcs.mean_rainfall);
  };

  // calculate the scale parameter based on the required second moment
  auto scale = nelder_mead(initial_scale - 0.1f, initial_scale + 0.1f, tol_ratio, get_error);
  trace(trace_level::verbose, "Shift scale decided on scale of {}", scale);

  // apply the scale to our data so that the inverse transform will restore our wet area ratio and conditional mean
  for (auto i = 0; i < field.size(); ++i)
    field.data()[i] = field.data()[i] > shift ? field.data()[i] * scale : xform_zero_;
}

distcor_shift_scale::dc_analysis::dc_analysis(std::istream& is)
{
  is.read(reinterpret_cast<char*>(&wet_area_ratio), sizeof(wet_area_ratio));
  is.read(reinterpret_cast<char*>(&mean_rainfall), sizeof(mean_rainfall));
}

auto distcor_shift_scale::dc_analysis::type() const -> type_id
{
  return type_id::shift_scale;
}

auto distcor_shift_scale::dc_analysis::write(std::ostream& os) const -> void
{
  os.write(reinterpret_cast<char const*>(&wet_area_ratio), sizeof(wet_area_ratio));
  os.write(reinterpret_cast<char const*>(&mean_rainfall), sizeof(mean_rainfall));
}

distcor_probability_matching_adv::distcor_probability_matching_adv(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config)
  : restart_ratio_{config["restart_ratio"]}
{ }

auto distcor_probability_matching_adv::analyse(array2f const& field) const -> unique_ptr<analysis>
{
  auto ps = profile::scope(prof_distcor_init);

  /* TODO
   * when we refactored distribution corrections to add the analyse function, this class was a bit of pain to handle.
   * we don't really want to store the full field and index as part of the stream cache, so the easiest way to make it
   * work was to store a redundant copy of the field here instead.  that lets us pass it straight to 'reinitialize()'
   * like we used to before the refactor.  if we really want to use this distribution correction algorithm operationally
   * then it should be properly optimised. */

  auto dca = std::make_unique<dc_analysis>(field);
  return dca;
}

auto distcor_probability_matching_adv::initialize(analysis const* obs, span<analysis const*> mod) const -> std::any
{
  auto ps = profile::scope(prof_distcor_init);

  auto dca = dynamic_cast<dc_analysis const*>(obs);
  if (!dca)
    throw std::runtime_error{"Unexpected dc_analysis type"};

  auto state = std::make_any<dc_state>(dca->field.shape());
  auto& dcs = std::any_cast<dc_state&>(state);

  reinitialize(dca->field, dcs);

  return state;
}

auto distcor_probability_matching_adv::correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void
{
  auto ps = profile::scope(prof_distcor_run);

  auto& dcs = std::any_cast<dc_state&>(state);
  if (dcs.index.shape() != field.shape())
    throw std::logic_error{"Array size mismatch"};

  // advect the index map to find out which pixels are still part of the domain
  auto buf_idx = array2i{dcs.index.shape()};
  advect(flow, -1, dcs.index, buf_idx);
  std::swap(buf_idx, dcs.index);

  // generate our filtered distribution
  auto filt_dist = array1f{field.size()};
  auto filt_size = 0;
  for (auto i = 0; i < field.size(); ++i)
  {
    if (dcs.index.data()[i] < 0)
      continue;
    filt_dist[filt_size] = dcs.target[dcs.index.data()[i]];
    ++filt_size;
  }
  std::sort(filt_dist.begin(), filt_dist.begin() + filt_size);

  // build an index into the field sorted by rain amount
  auto idx_fld = array1i{field.size()};
  for (auto i = 0; i < idx_fld.size(); ++i)
    idx_fld[i] = i;
  std::sort(idx_fld.begin(), idx_fld.end(), [&](auto lhs, auto rhs){ return field.data()[lhs] < field.data()[rhs]; });

  // remap the over the edge cells based on their position in the overall distribution
  // at the same time filter these cells out of our index list for the next pass
  auto iout = 0;
  for (auto i = 0; i < field.size(); ++i)
  {
    if (dcs.index.data()[idx_fld[i]] < 0)
      field.data()[idx_fld[i]] = dcs.target[i];
    else
      idx_fld[iout++] = idx_fld[i];
  }

  // now remap the 'normal' cells based on their position in the filtered distribution
  for (auto i = 0; i < filt_size; ++i)
    field.data()[idx_fld[i]] = filt_dist[i];

  // if we've lost too much of our original distribution reinitialize from the current state
  if (filt_size / float(field.size()) < restart_ratio_)
  {
    trace(trace_level::log, "Restarting probability matching advection correction");
    reinitialize(field, dcs);
  }
}

auto distcor_probability_matching_adv::reinitialize(array2f const& field, dc_state& dcs) const -> void
{
  // create an index sorted by rainfall amount
  auto idx_tmp = array1i{field.size()};
  for (auto i = 0; i < idx_tmp.size(); ++i)
    idx_tmp[i] = i;
  std::sort(idx_tmp.begin(), idx_tmp.end(), [&](auto l, auto r){ return field.data()[l] < field.data()[r]; });

  // store the sorted rainfall distribution, and create our index into it
  for (auto i = 0; i < idx_tmp.size(); ++i)
  {
    dcs.target[i] = field.data()[idx_tmp[i]];
    dcs.index.data()[idx_tmp[i]] = i;
  }
}

distcor_probability_matching_adv::dc_analysis::dc_analysis(std::istream& is)
{
  auto shape = vec2i{0, 0};
  is.read(reinterpret_cast<char*>(&shape), sizeof(shape));
  field.resize(shape);
  is.read(reinterpret_cast<char*>(field.data()), field.size_bytes());
}

auto distcor_probability_matching_adv::dc_analysis::type() const -> type_id
{
  return type_id::probability_matching;
}

auto distcor_probability_matching_adv::dc_analysis::write(std::ostream& os) const -> void
{
  auto shape = field.shape();
  os.write(reinterpret_cast<char const*>(&shape), sizeof(shape));
  os.write(reinterpret_cast<char const*>(field.data()), field.size_bytes());
}

distcor_local_war::distcor_local_war(
      duration time_step
    , vec2i grid_shape
    , float grid_resolution
    , float min_rain_rate
    , transform const& xform
    , configuration const& config)
  : box_shape_{static_cast<int>(std::ceil(float(config["local_size"]) / grid_resolution))}
  , blend_factor_{calculate_blend_factor_for_halflife(time_step, duration{config["blend_halflife"]})}
  , xform_min_rain_{xform.forward(min_rain_rate)}
  , xform_zero_{xform.forward(0.0f)}
  , analysis_shape_{grid_shape / box_shape_}
  , lerp_y_{grid_shape.y}
  , lerp_x_{grid_shape.x}
{
  // sanity check
  if (grid_shape.y < box_shape_ || grid_shape.x < box_shape_)
    throw std::runtime_error{"Box size too large for input grid"};

  // precalculate indexes and fractions needed for bilinear interpolation of threshold
  auto half = box_shape_ / 2;
  for (auto y = 0; y < grid_shape.y; ++y)
  {
    lerp_y_[y].d0 = (y - half) / box_shape_;
    lerp_y_[y].frac = ((y - half) % box_shape_) / float(box_shape_);
    if (lerp_y_[y].frac < 0.0f) // cope with first half of first box
      lerp_y_[y].frac = 0.0f;
    else if (lerp_y_[y].d0 >= analysis_shape_.y - 1) // copy with last half of last box
    {
      lerp_y_[y].d0 = analysis_shape_.y - 2;
      lerp_y_[y].frac = 1.0f;
    }
  }
  for (auto x = 0; x < grid_shape.x; ++x)
  {
    lerp_x_[x].d0 = (x - half) / box_shape_;
    lerp_x_[x].frac = ((x - half) % box_shape_) / float(box_shape_);
    if (lerp_x_[x].frac < 0.0f) // cope with first half of first box
      lerp_x_[x].frac = 0.0f;
    else if (lerp_x_[x].d0 >= analysis_shape_.x - 1) // copy with last half of last box
    {
      lerp_x_[x].d0 = analysis_shape_.x - 2;
      lerp_x_[x].frac = 1.0f;
    }
  }

  #if 0
  trace(trace_level::log, "Local WAR model blend factor {}", blend_factor_);
  #endif
}

auto distcor_local_war::analyse(array2f const& field) const -> unique_ptr<analysis>
{
  auto a = std::make_unique<dc_analysis>(analysis_shape_);

  // determine the wet area ratio in each box of our analysis grid
  for (auto ya = 0; ya < analysis_shape_.y; ++ya)
  {
    for (auto xa = 0; xa < analysis_shape_.x; ++xa)
    {
      // calculate bounds of analysis region, we expand the last box to edge of the grid rather than trying
      // to deal sensibly with a partial box (which might be very small and behave strangely)
      auto ybeg = ya * box_shape_;
      auto yend = ya < analysis_shape_.y - 1 ? ybeg + box_shape_ : field.shape().y;
      auto xbeg = xa * box_shape_ ;
      auto xend = xa < analysis_shape_.x - 1 ? xbeg + box_shape_ : field.shape().x;

      // perform the war calculation
      auto count = 0;
      for (auto y = ybeg; y < yend; ++y)
      {
        for (auto x = xbeg; x < xend; ++x)
        {
          if (field[y][x] >= xform_min_rain_)
            ++count;
        }
      }
      a->war[ya][xa] = float(count) / ((yend - ybeg) * (xend - xbeg));
    }
  }

  return a;
}

auto distcor_local_war::initialize(analysis const* obs, span<analysis const*> mod) const -> std::any
{
  auto state = std::make_any<dc_state>();
  auto& dcs = std::any_cast<dc_state&>(state);

  // copy in the initial war analysis from the observation
  dcs.war = static_cast<dc_analysis const*>(obs)->war;

  return state;
}

auto distcor_local_war::correct_field(std::any& state, span<analysis const*> mod, array2f2 const& flow, array2f& field) const -> void
{
  auto ps = profile::scope(prof_distcor_run);

  auto& dcs = std::any_cast<dc_state&>(state);

  // for now we just use the first model rather than trying to blend the WAR analysis of different models
  // based on their skill
  if (!mod[0])
    throw std::runtime_error{"Local WAR distribution correction requires model input"};
  auto modan = static_cast<dc_analysis const*>(mod[0]);

  // transition from obs to model war values over time
  for (auto i = 0; i < dcs.war.size(); ++i)
    dcs.war.data()[i] = lerp(dcs.war.data()[i], modan->war.data()[i], blend_factor_);

  // determine the wet area ratio in each box of our analysis grid
  array2f buf;
  array2f thresholds{analysis_shape_};
  for (auto ya = 0; ya < analysis_shape_.y; ++ya)
  {
    for (auto xa = 0; xa < analysis_shape_.x; ++xa)
    {
      // calculate bounds of analysis region, we expand the last box to edge of the grid rather than trying
      // to deal sensibly with a partial box (which might be very small and behave strangely)
      auto ybeg = ya * box_shape_;
      auto yend = ya < analysis_shape_.y - 1 ? ybeg + box_shape_ : field.shape().y;
      auto xbeg = xa * box_shape_ ;
      auto xend = xa < analysis_shape_.x - 1 ? xbeg + box_shape_ : field.shape().x;

      // collect our box of data into a buffer
      buf.resize(vec2i{xend - xbeg, yend - ybeg});
      for (auto y = ybeg; y < yend; ++y)
        for (auto x = xbeg; x < xend; ++x)
          buf[y - ybeg][x - xbeg] = field[y][x];

      // sort by rain rate
      std::sort(buf.data(), buf.data() + buf.size());

      // now find the rain rate below which we should zero to enforce force our WAR
      auto war_index = int((1.0f - dcs.war[ya][xa]) * buf.size());
      thresholds[ya][xa] = buf.data()[std::clamp(war_index, 0, buf.size() - 1)];
    }
  }

  // loop through our field and apply the threshold
#if 0
  // this version uses the analysis boxes strictly, which can result in hard boundaries
  for (auto y = 0; y < field.shape().y; ++y)
  {
    for (auto x = 0; x < field.shape().x; ++x)
    {
      // simple model - use exact threshold of containing box
      auto ya = std::min(y / box_shape_, analysis_shape_.y - 1);
      auto xa = std::min(x / box_shape_, analysis_shape_.x - 1);
      if (field[y][x] < thresholds[ya][xa])
        field[y][x] = xform_zero_;

      field[y][x] = xform_->forward(ya * 10 + xa);
    }
  }
#else
  // this version bilinearly interpolates the WAR enforcement threshold between analysis boxes
  for (auto y = 0; y < field.shape().y; ++y)
  {
    auto& yl = lerp_y_[y];
    for (auto x = 0; x < field.shape().x; ++x)
    {
      auto& xl = lerp_x_[x];
      auto threshold = lerp(
            lerp(thresholds[yl.d0][xl.d0], thresholds[yl.d0][xl.d0+1], xl.frac)
          , lerp(thresholds[yl.d0+1][xl.d0], thresholds[yl.d0+1][xl.d0+1], xl.frac)
          , yl.frac);
      if (field[y][x] < threshold)
        field[y][x] = xform_zero_;
    }
  }
#endif
}

distcor_local_war::dc_analysis::dc_analysis(std::istream& is)
{
  auto shape = vec2i{};
  is.read(reinterpret_cast<char*>(&shape), sizeof(shape));
  war.resize(shape);
  is.read(reinterpret_cast<char*>(war.data()), war.size_bytes());
}

auto distcor_local_war::dc_analysis::type() const -> type_id
{
  return type_id::local_war;
}

auto distcor_local_war::dc_analysis::write(std::ostream& os) const -> void
{
  auto shape = war.shape();
  os.write(reinterpret_cast<char const*>(&shape), sizeof(shape));
  os.write(reinterpret_cast<char const*>(war.data()), war.size_bytes());
}

#if 0
// LCOV_EXCL_START
TEST_CASE("distcor_probability_matching")
{
  // flow field is unused by this distribution
  auto flow = array2f2{3, 3};

  // target distribution is number 0..9
  auto target = array2f{3, 3};
  target[0][0] = 0.0f; target[0][1] = 5.0f; target[0][2] = 7.0f;
  target[1][0] = 6.0f; target[1][1] = 1.0f; target[1][2] = 4.0f;
  target[2][0] = 8.0f; target[2][1] = 3.0f; target[2][2] = 2.0f;

  // input distribution we want to remap
  auto input = array2f{3, 3};
  input[0][0] = 10.0f; input[0][1] = 60.0f; input[0][2] = 40.0f;
  input[1][0] = 80.0f; input[1][1] = 20.0f; input[1][2] = 70.0f;
  input[2][0] = 30.0f; input[2][1] = 50.0f; input[2][2] = 00.0f;

  // expected output distribution for comparison
  auto expected = array2f{3, 3};
  expected[0][0] = 1.0f; expected[0][1] = 6.0f; expected[0][2] = 4.0f;
  expected[1][0] = 8.0f; expected[1][1] = 2.0f; expected[1][2] = 7.0f;
  expected[2][0] = 3.0f; expected[2][1] = 5.0f; expected[2][2] = 0.0f;

  // transform the input to the target distribution
  auto xform = transform::instantiate("type inverse-hyperbolic-sine prescale 1.0"_config);
  auto dc = distribution_correction::instantiate(vec2i{3, 3}, 0.0f, *xform, "type probability-matching"_config);
  auto state = dc->initialize(target);
  dc->correct_field(state, flow, input);

  // check the distribution is what we expect
  CHECK_ALL(input, expected, lhs == approx(rhs));
}
// LCOV_EXCL_STOP
#endif
