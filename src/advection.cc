/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "advection.h"
#include "options.h"
#include "unit_test.h"

using namespace steps;

// modified liang barsky method to clip line against box
// we assume that xmin and ymax are 0.0 and that start point is always inside box
static auto clip_line(float xmax, float ymax, float x0, float y0, float& x1, float& y1) -> bool
{
  constexpr auto xmin = 0.0f;
  constexpr auto ymin = 0.0f;

  auto t0 = 0.0f;
  auto t1 = 1.0f;
  auto xd = x1 - x0;
  auto yd = y1 - y0;

  for (auto edge = 0; edge < 4; ++edge)
  {
    float p, q;
    if (edge == 0) { p = -xd; q = -(xmin - x0); }
    if (edge == 1) { p =  xd; q =  (xmax - x0); }
    if (edge == 2) { p = -yd; q = -(ymin - y0); }
    if (edge == 3) { p =  yd; q =  (ymax - y0); }

    auto r = q / p;

    // parallel line outside box (impossible)
    if (p == 0 && q < 0)
      return false;

    if (p < 0)
    {
      // fully clipped? (impossible)
      if (r > t1)
        return false;
      // partially clipped?
      else if (r > t0)
        t0 = r;
    }
    else if (p > 0)
    {
      // fully clipped? (impossible)
      if (r < t0)
        return false;
      // partially clipped?
      else if (r < t1)
        t1 = r;
    }
  }

  x1 = x0 + t1 * xd;
  y1 = y0 + t1 * yd;
  return true;
}

/* The flow vectors in 'flow' are accumulated into the existing vectors at lag1 which are assumed to contain the total
 * advection vectors up until the lag1 time.  The resulting total flow is written to lag0. */
auto steps::accumulate_flow(array2f2 const& flow, array2f2 const& lag1, array2f2& lag0) -> void
{
  // sanity checks
  if (&flow == &lag0 || &lag1 == &lag0)
    throw std::invalid_argument("Inplace operation not supported");
  if (lag0.shape() != flow.shape() || lag1.shape() != flow.shape())
    throw std::invalid_argument("Aray size mismatch");

  // cache floating point limits used when clipping vectors
  auto clip_limit_x = flow.shape().x - 2.0f;
  auto clip_limit_y = flow.shape().y - 2.0f;

  for (int y = 0; y < flow.shape().y; ++y)
  {
    for (int x = 0; x < flow.shape().x; ++x)
    {
      // convert x and y to floats
      auto xf = float(x);
      auto yf = float(y);

      // determine the location we want to take our accumulation so far vector from
      auto src_x = xf + flow[y][x].x;
      auto src_y = yf + flow[y][x].y;

      // integer coordinates of the nw corner grid cell
      auto src_xi = int(std::floor(src_x));
      auto src_yi = int(std::floor(src_y));

      // determine linear interpolation fraction
      auto frac_x = src_x - src_xi;
      auto frac_y = src_y - src_yi;

      // if our vector extends outside the box then intersect with the edges
      // this has the effect of making the advection field persist the border values out infinitely
      if (src_xi < 0 || src_yi < 0 || src_xi >= flow.shape().x - 1 || src_yi >= flow.shape().y - 1)
      {
        // clip to border
        clip_line(clip_limit_x, clip_limit_y, xf, yf, src_x, src_y);

        // new integer coordinates for nw
        src_xi = std::floor(src_x);
        src_yi = std::floor(src_y);

        // fix bad coordinates (should only happen due to floating point error)
        if (src_xi < 0)
        {
          src_xi = 0;
          frac_x = 0.0f;
        }
        else if (src_xi >= flow.shape().x - 1)
        {
          src_xi = flow.shape().x - 2;
          frac_x = 1.0f;
        }
        if (src_yi < 0)
        {
          src_yi = 0;
          frac_y = 0.0f;
        }
        else if (src_yi >= flow.shape().x - 2)
        {
          src_yi = flow.shape().y - 2;
          frac_y = 1.0f;
        }
      }

      // perform linear interpolation to determine output vector
      lag0[y][x] = flow[y][x]
        + (1.0f - frac_y) * (1.0f - frac_x) * lag1[src_yi][src_xi]
        +     frac_y      * (1.0f - frac_x) * lag1[src_yi + 1][src_xi]
        + (1.0f - frac_y) *     frac_x      * lag1[src_yi][src_xi + 1]
        +     frac_y      *     frac_x      * lag1[src_yi + 1][src_xi + 1];
    }
  }
}

// LCOV_EXCL_START
TEST_CASE("advect(array2f)")
{
  auto flow = array2f2{vec2i{32, 32}};
  auto lag1 = array2f{vec2i{32, 32}};
  auto lag0 = array2f{vec2i{32, 32}};
  auto ref = array2f{vec2i{32, 32}};

  // square of 10.0f in the center
  lag1.fill(0.0f);
  for (auto y = 13; y < 19; ++y)
    for (auto x = 13; x < 19; ++x)
      lag1[y][x] = 10.0f;

  // fill lag0 with huge values to ensure it is a pure 'out' parameter
  lag0.fill(1000.0f);

  SUBCASE("sanity")
  {
    auto bad = array2f{flow.shape() * 2};
    CHECK_THROWS(advect(flow, 0.0f, lag0, lag0));
    CHECK_THROWS(advect(flow, 0.0f, bad, lag0));
    CHECK_THROWS(advect(flow, 0.0f, lag1, bad));
  }

  SUBCASE("zero")
  {
    flow.fill(vec2f{0.0f, 0.0f});
    ref.copy(lag1);
    advect(flow, 0.0f, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  SUBCASE("h5")
  {
    flow.fill(vec2f{-5.0f, 0.0f});
    ref.fill(0.0f);
    for (auto y = 13; y < 19; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] = 10.0f;
    advect(flow, 0.0f, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  SUBCASE("v-6")
  {
    flow.fill(vec2f{0.0f, 6.0f});
    ref.fill(0.0f);
    for (auto y = 13 - 6; y < 19 - 6; ++y)
      for (auto x = 13; x < 19; ++x)
        ref[y][x] = 10.0f;
    advect(flow, 0.0f, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  SUBCASE("diag5")
  {
    flow.fill(vec2f{-5.0f, -5.0f});
    ref.fill(0.0f);
    for (auto y = 13 + 5; y < 19 + 5; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] = 10.0f;
    advect(flow, 0.0f, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  // tests that vectors are at lag0 locations and point to lag1 locations (expected orientation)
  SUBCASE("non-uniform")
  {
    // only advect a box that is 5 to the right (pulling values 5 from the left - our center 10.0 box)
    flow.fill(vec2f{0.0f, 0.0f});
    for (auto y = 13; y < 19; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        flow[y][x] = vec2f{-5.0f, 0.0f};

    // ref image will have advected square to the right, but also left behind 10.0s in center where the
    // flow vectors were 0.0
    ref.fill(0.0f);
    for (auto y = 13; y < 19; ++y)
    {
      // 'left behind' bit
      for (auto x = 13; x < 13 + 5; ++x)
        ref[y][x] = 10.0f;

      // advected bit
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] = 10.0f;
    }
    advect(flow, 0.0f, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  // check that things still work when we use a background field instead of a scalar
  // no need to repeat _all_ the tests since we ultimately still call sample_grid.  just
  // check a simple case to confirm out background values are being used
  SUBCASE("back")
  {
    auto back = array2f{vec2i{32, 32}};
    back.fill(1.0f);

    auto bad = array2f{flow.shape() * 2};
    CHECK_THROWS(advect(flow, back, lag0, lag0));
    CHECK_THROWS(advect(flow, bad, lag1, lag0));
    CHECK_THROWS(advect(flow, back, bad, lag0));
    CHECK_THROWS(advect(flow, back, lag1, bad));

    flow.fill(vec2f{-5.0f, 0.0f});
    ref.fill(0.0f);
    for (auto y = 13; y < 19; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] = 10.0f;
    for (auto y = 0; y < 32; ++y)
      for (auto x = 0; x < 5; ++x)
        ref[y][x] = 1.0f;
    advect(flow, back, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }
}

TEST_CASE("advect(array2i)")
{
  auto flow = array2f2{vec2i{32, 32}};
  auto lag1 = array2i{vec2i{32, 32}};
  auto lag0 = array2i{vec2i{32, 32}};
  auto ref = array2i{vec2i{32, 32}};

  // square of 10.0f in the center
  lag1.fill(0);
  for (auto y = 13; y < 19; ++y)
    for (auto x = 13; x < 19; ++x)
      lag1[y][x] = 10;

  // fill lag0 with huge values to ensure it is a pure 'out' parameter
  lag0.fill(1000);

  SUBCASE("sanity")
  {
    auto bad = array2i{flow.shape() * 2};
    CHECK_THROWS(advect(flow, 0, lag0, lag0));
    CHECK_THROWS(advect(flow, 0, bad, lag0));
    CHECK_THROWS(advect(flow, 0, lag1, bad));
  }

  SUBCASE("zero")
  {
    flow.fill(vec2f{0.0f, 0.0f});
    ref.copy(lag1);
    advect(flow, 0, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  SUBCASE("h5")
  {
    flow.fill(vec2f{-5.0f, 0.0f});
    ref.fill(0);
    for (auto y = 13; y < 19; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] = 10;
    advect(flow, 0, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  SUBCASE("v-6")
  {
    flow.fill(vec2f{0.0f, 6.0f});
    ref.fill(0);
    for (auto y = 13 - 6; y < 19 - 6; ++y)
      for (auto x = 13; x < 19; ++x)
        ref[y][x] = 10;
    advect(flow, 0, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }

  SUBCASE("diag5")
  {
    flow.fill(vec2f{-5.0f, -5.0f});
    ref.fill(0);
    for (auto y = 13 + 5; y < 19 + 5; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] = 10;
    advect(flow, 0, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs == approx(rhs));
  }
}

TEST_CASE("accumulate_flow")
{
  auto flow = array2f2{vec2i{32, 32}};
  auto lag1 = array2f2{vec2i{32, 32}};
  auto lag0 = array2f2{vec2i{32, 32}};
  auto ref = array2f2{vec2i{32, 32}};

  // square of 10.0f in the center
  lag1.fill(vec2f{0.0f, 0.0f});
  for (auto y = 13; y < 19; ++y)
    for (auto x = 13; x < 19; ++x)
      lag1[y][x] = vec2f{10.0f, 10.0f};

  // fill lag0 with huge values to ensure it is a pure 'out' parameter
  lag0.fill(vec2f{1000.0f, 1000.0f});

  SUBCASE("sanity")
  {
    auto bad = array2f2{flow.shape() * 2};
    CHECK_THROWS(accumulate_flow(lag0, lag1, lag0));
    CHECK_THROWS(accumulate_flow(flow, lag0, lag0));
    CHECK_THROWS(accumulate_flow(flow, bad, lag0));
    CHECK_THROWS(accumulate_flow(flow, lag1, bad));
  }

  SUBCASE("zero")
  {
    flow.fill(vec2f{0.0f, 0.0f});
    ref.copy(lag1);
    accumulate_flow(flow, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs.x == approx(rhs.x) && lhs.y == approx(rhs.y));
  }

  SUBCASE("h5")
  {
    flow.fill(vec2f{-5.0f, 0.0f});
    ref.fill(vec2f{-5.0f, 0.0f});
    for (auto y = 13; y < 19; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] += vec2f{10.0f, 10.0f};
    accumulate_flow(flow, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs.x == approx(rhs.x) && lhs.y == approx(rhs.y));
  }

  SUBCASE("v-6")
  {
    flow.fill(vec2f{0.0f, 6.0f});
    ref.fill(vec2f{0.0f, 6.0f});
    for (auto y = 13 - 6; y < 19 - 6; ++y)
      for (auto x = 13; x < 19; ++x)
        ref[y][x] += vec2f{10.0f, 10.0f};
    accumulate_flow(flow, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs.x == approx(rhs.x) && lhs.y == approx(rhs.y));
  }

  SUBCASE("diag5")
  {
    flow.fill(vec2f{-5.0f, -5.0f});
    ref.fill(vec2f{-5.0f, -5.0f});
    for (auto y = 13 + 5; y < 19 + 5; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] += vec2f{10.0f, 10.0f};
    accumulate_flow(flow, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs.x == approx(rhs.x) && lhs.y == approx(rhs.y));
  }

  // tests that vectors are at lag0 locations and point to lag1 locations (expected orientation)
  SUBCASE("non-uniform")
  {
    // only advect a box that is 5 to the right (pulling values 5 from the left - our center 10.0 box)
    flow.fill(vec2f{0.0f, 0.0f});
    for (auto y = 13; y < 19; ++y)
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        flow[y][x] = vec2f{-5.0f, 0.0f};

    // ref image will have advected square to the right, but also left behind 10.0s in center where the
    // flow vectors were 0.0
    ref.copy(flow);
    for (auto y = 13; y < 19; ++y)
    {
      // 'left behind' bit
      for (auto x = 13; x < 13 + 5; ++x)
        ref[y][x] += vec2f{10.0f, 10.0f};

      // advected bit
      for (auto x = 13 + 5; x < 19 + 5; ++x)
        ref[y][x] += vec2f{10.0f, 10.0f};
    }
    accumulate_flow(flow, lag1, lag0);
    CHECK_ALL(lag0, ref, lhs.x == approx(rhs.x) && lhs.y == approx(rhs.y));
  }
}
// LCOV_EXCL_STOP
