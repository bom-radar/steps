/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "configuration.h"
#include "io.h"
#include "metadata.h"
#include "nc.h"
#include "io_netcdf.h"
#include "scores.h"
#include "version.h"

#include <condition_variable>
#include <mutex>

namespace steps
{
  class verification_io;

  /// Verification engine
  class verification
  {
  public:
    /// Initialize a new verification from config file
    verification(configuration const& config);

    /// Run the verification
    auto verify_period(time_point from, time_point till, duration period) -> void;

    /// Aggregate a list of verification files
    auto aggregate_results(vector<string> const& paths) -> void;

    auto& user_parameters() const   { return user_parameters_; }

    /// Get the model name
    auto& model_name() const        { return model_name_; }
    /// Minimum rain rate for a grid cell to be included in verification
    auto minimum_rain_rate() const  { return minimum_rain_rate_; }
    auto border_width() const       { return border_width_; }
    auto grid_shape() const         { return grid_shape_; }
    auto& members() const           { return members_; }
    auto& lead_times() const        { return lead_times_; }
    auto& thresholds() const        { return thresholds_; }
    auto spread_bins() const        { return spread_bins_; }
    auto max_error() const          { return max_error_; }
    auto roc_points() const         { return roc_points_; }
    auto reliability_bins() const   { return reliability_bins_; }

  public:
    /// Verification scores which are tracked per lead time and threshold
    struct threshold_scores
    {
      std::mutex                mut_roc;
      roc_curve                 roc;

      std::mutex                mut_reliability;
      reliability_curve         reliability;

      threshold_scores(int roc_points, int reliability_bins);
    };

    /// Verification scores which are tracked per lead time
    struct lead_time_scores
    {
      int                 sample_count; // actually protected by mut_rmse to save a separate lock/unlock

      std::mutex          mut_rmse;
      ::steps::rmse       rmse;

      std::mutex          mut_correlation;
      average_correlation correlation;

      std::mutex          mut_rank;
      rank_histogram      rank;

      std::mutex          mut_crps;
      ::steps::crps       crps;

      std::mutex          mut_spread;
      spread_reliability  spread;

      vector<unique_ptr<threshold_scores>>  thresholds;

      lead_time_scores(
            int members
          , vec2i grid_shape
          , int spread_bins
          , float max_error
          , int num_thresholds
          , int roc_points
          , int reliability_bins);
    };

    using score_store = vector<unique_ptr<lead_time_scores>>;

  private:
    using member_store = vector<array2f>;

    struct job_observation
    {
      job_observation(time_point time)
        : time{time}
      { }

      time_point          time;     // observation time
    };

    struct job_lead_time
    {
      job_lead_time(time_point time, shared_ptr<array2f> obs, int ilead)
        : time{time}, obs{std::move(obs)}, ilead{ilead}
      { }

      time_point          time;     // observation time
      shared_ptr<array2f> obs;      // observation data for time
      int                 ilead;    // index of lead time
    };

    struct job_threshold
    {
      job_threshold(int ilead, shared_ptr<member_store> ens, shared_ptr<array2f> cond_obs, int iscore)
        : ilead{ilead}, ens{std::move(ens)}, cond_obs{std::move(cond_obs)}, iscore{iscore}
      { }

      int                       ilead;    // index of lead time
      shared_ptr<member_store>  ens;      // ensemble members for time & lead time
      shared_ptr<array2f>       cond_obs; // observations conditioned on non-zero obs or forecast
      int                       iscore;   // index of score
    };

    struct job_score
    {
      job_score(
            int ilead
          , int ithreshold
          , shared_ptr<array2<event_occurrence>> events
          , shared_ptr<ensemble_probability> pops
          , int iscore)
        : ilead{ilead}, ithreshold{ithreshold}, events{std::move(events)}, pops{std::move(pops)}, iscore{iscore}
      { }

      int                                   ilead;      // index of lead time
      int                                   ithreshold; // index of threshold
      shared_ptr<array2<event_occurrence>>  events;     // actual event occurrence based on observations
      shared_ptr<ensemble_probability>      pops;       // forecast probabilites of event occurrence
      int                                   iscore;     // index of score
    };

  private:
    auto thread_verify(int thread_count) -> void;
    auto process_job(job_observation const& req) -> list<job_lead_time>;
    auto process_job(job_lead_time const& req) -> list<job_threshold>;
    auto process_job(job_threshold const& req) -> list<job_score>;
    auto process_job(job_score const& req) -> void;

    auto construct_scores() const -> score_store;
    auto mask_border(array2f& obs) const -> void;
    auto condition_active_cells(array2f const& obs, member_store const& ens, array2f& cond_obs) const -> void;
    auto write_output_file(nc::file& fstats) -> void;
    auto read_scores(nc::file const& file, score_store& scores) -> void;

  private:
    metadata                user_parameters_;
    string                  model_name_;
    float                   minimum_rain_rate_;
    int                     border_width_;
    vec2i                   grid_shape_;
    array1i                 members_;
    array1<duration>        lead_times_;
    array1f                 thresholds_;
    int                     spread_bins_;
    float                   max_error_;
    int                     roc_points_;
    int                     reliability_bins_;
    unique_ptr<input>       input_obs_;
    unique_ptr<input>       input_ens_;
    unique_ptr<verification_io> output_;

    // state for current run
    score_store             scores_;

    // work queues for engine threads
    std::mutex              mut_queues_;
    list<job_observation>   queue_observations_;
    list<job_lead_time>     queue_lead_times_;
    list<job_threshold>     queue_thresholds_;
    list<job_score>         queue_scores_;
    std::condition_variable cv_queues_;
    int                     threads_idle_;
    std::exception_ptr      fatal_error_;
  };

  /// Verification output manager interface
  /** This interface is a simple wrapper around a nc::file object.  The reason for abstracting in this way instead
   *  of using a nc::file directly is that it allows the user to customize how the file is opened and stored.  For
   *  example, writing to a temporary file and automatically uploading it to a cloud platform. */
  class verification_io
  {
  public:
    /// Output factory function pointer type
    using factory = auto (*)(verification const&, configuration const&, metadata const&) -> unique_ptr<verification_io>;

    /// Set the output factory function
    static auto set_factory(factory function) -> void;

    /// Instanciate an output using the currently installed factory function
    static auto instantiate(verification const& c, configuration const& config, metadata const& user_parameters) -> unique_ptr<verification_io>;

  public:
    /// Virtual destructor
    virtual ~verification_io() = default;

    /// Output a verification result file
    virtual auto write(
          time_point from
        , time_point till
        , verification::score_store const& scores
        ) -> void = 0;

    /// Read a verification result file
    virtual auto read(
          string_view path
        , time_point& from
        , time_point& till
        , verification::score_store& scores
        ) -> bool = 0;
  };

  class verification_io_netcdf : public verification_io
  {
  public:
    verification_io_netcdf(verification const& c, configuration const& config);

  protected:
    /* These functions require that the caller MUST be holding a lock on the netcdf io mutex using a call to
     * nc::lock_mutex().  Without holding the netcdf io mutex these function will not be thread safe. */
    auto write_impl(
          time_point from
        , time_point till
        , verification::score_store const& scores
        , nc::file& fstats
        ) -> void;

    auto read_impl(
          nc::file const& fstats
        , time_point& from
        , time_point& till
        , verification::score_store& scores
        ) -> void;

  private:
    verification const*     c_;
    metadata                metadata_;
    nc::grid_reference      projection_;
  };

  class verification_io_netcdf_local : public verification_io_netcdf
  {
  public:
    verification_io_netcdf_local(verification const& c, configuration const& config, metadata const& user_parameters);

    auto write(
          time_point from
        , time_point till
        , verification::score_store const& scores
        ) -> void override;

    auto read(
          string_view path
        , time_point& from
        , time_point& till
        , verification::score_store& scores
        ) -> bool override;

  private:
    auto determine_path(time_point from, time_point till) const -> string;

  private:
    metadata const* user_parameters_;
    string          path_format_;
  };
}
