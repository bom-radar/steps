/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "optical_flow.h"
#include "core.h"
#include "trace.h"

using namespace steps;

optical_flow_rf3::optical_flow_rf3(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config)
  : dims_{grid_shape}
  , scale_{config.optional("scale", 0.5f)}                // scale factor for tracking resolution levels
  , win_size_{config.optional("window_size", 5)}          // blur operation kernel size
  , iterations_{config.optional("iterations", 4)}         // number of blur iterations at each resolution
  , poly_n_{config.optional("polygon_neighbourhood", 5)}  // or 7
  , poly_sigma_{config.optional("polygon_sigma", 1.1)}    // 1.5 if neighbourhood is 7
  , background_{config.optional("background", 0.0f)}
  , threshold_{config.optional("threshold", 1.0f)}
  , gain_{config.optional("gain", 100.0f)}
  , fill_gaps_{config.optional("fill_gaps", true)}
  , spacing_{config.optional("spacing", 8)}
  , min_frac_bins_for_avg_{config.optional("min_frac_bins_for_avg", 0.05f)}
  , idw_low_res_pwr_{config.optional("idw_low_res_pwr", 4.0)}
  , idw_high_res_pwr_{config.optional("idw_high_res_pwr", 3.0)}
{
  constexpr int min_size = 32;

  // limit the levels to a sensible value
  auto levels = config.optional("max_levels", 100);
  int min_side = std::min(dims_.x, dims_.y);
  auto scale = 1.0f;
  for (int i = 0; i < levels; ++i, scale *= scale_)
  {
    if (min_side * scale < min_size)
    {
      levels = i;
      break;
    }
  }

  // calculate the per level constants
  info_.resize(levels);
  scale = 1.0f;
  for (int i = 0; i < levels; ++i, scale *= scale_)
  {
    auto& info = info_[i];

    // record the scale at this level
    info.scale = scale;

    // determine kernel size and sigma for gaussian blur
    info.sigma = (1.0f / scale - 1.0f) * 0.5f;
    info.ksize = std::max(std::lround(info.sigma * 5.0f) | 1L, 3L);

    // determine width and height at this level
    info.dims.x = std::lround(dims_.y * scale);
    info.dims.y = std::lround(dims_.x * scale);

    trace(trace_level::verbose, "optical flow lvl {} scale {} sigma {} ksize {} size {}", i, scale, info.sigma, info.ksize, info.dims);
  }

  // setup the constants for the polynomial expansion function
  poly_exp_setup();
}

auto optical_flow_rf3::determine_flow(
      array2f const& from
    , array2f const& to
    , bool use_initial_flow
    , array2f2& flow
    ) const -> void
{
  auto ps = profile::scope{prof_optical_flow};

  // sanity checks
  if (to.shape() != dims_ || from.shape() != dims_ || flow.shape() != dims_)
    throw std::runtime_error("determine velocities: field/flow size mismatch");

  auto from_copy = from;
  auto to_copy = to;

  // need to apply a signal threshold?
  if (threshold_ > background_)
  {
    from_copy.threshold_min(threshold_, background_);
    to_copy.threshold_min(threshold_, background_);
  }

  // neet to apply a fixed gain?
  if (gain_ != 1.0f)
  {
    from_copy.multiply(gain_);
    to_copy.multiply(gain_);
  }

  // do the actual tracking
  // track backwards from to to from, so that vectors are located according to the to
  // data.  this means we then have to negate all the vectors afterwards
  track_fields(to_copy, from_copy, flow, use_initial_flow);
  flow.multiply(-1.0f);

  // interpolate over gap areas if needed
  if (fill_gaps_)
  {
    // flag vectors where the original input was less than the threshold as bad
    auto const d0 = to.data();
    auto o0 = flow.data();
    for (int i = 0; i < to_copy.size(); ++i)
      if (d0[i] < threshold_)
        o0[i].x = bad_vel;

    // now do the gap filling interpolation
    interpolate_gaps(flow);
  }
}

// Interpolate data from one array onto another of a different size (bi-linear)
template <typename T>
auto interpolate(array2<T> const& input, array2<T>& output) -> void
{
  if (output.shape() == input.shape())
  {
    // special case 1: no interpolation required
    output.copy(input);
  }
  else if (output.shape() * 2 == input.shape())
  {
    // special case 2: perfect downscale (we are one level smaller than input)
    for (auto y = 0; y < output.shape().y; ++y)
    {
      const T* top = input[y * 2];
      const T* bot = input[y * 2 + 1];
            T* out = output[y];
      for (auto x = 0; x < output.shape().x; ++x)
      {
        out[0] = (top[0] + top[1] + bot[0] + bot[1]) * 0.25f;
        top += 2;
        bot += 2;
        ++out;
      }
    }
  }
  else
  {
    // generic case: bi-linear interpolation (much slower)
    auto scale_x = float(input.shape().x) / output.shape().x;
    auto scale_y = float(input.shape().y) / output.shape().y;
    for (auto y = 0; y < output.shape().y; ++y)
    {
      T* out = output[y];
      for (auto x = 0; x < output.shape().x; ++x)
      {
        // target x, y (the +0.5 is for rounding purposes)
        auto tx = x * scale_x + 0.5f;
        auto ty = y * scale_y + 0.5f;

        // left, right, top and bottom sample boundaries
        auto x2 = int(tx);
        auto x1 = x2 - 1;
        auto y2 = int(ty);
        auto y1 = y2 - 1;

        // convert target coordinates to interpolation fractions
        // the +0.5 previously added means that these are correct
        tx -= x2;
        ty -= y2;

        // clamp samples to borders
        if (x1 < 0)
          x1 = 0;
        if (x2 >= input.shape().x)
          x2 = input.shape().x - 1;
        if (y1 < 0)
          y1 = 0;
        if (y2 >= input.shape().y)
          y2 = input.shape().y - 1;

        // interpolate in x direction first
        auto r1 = input[y1][x1] * tx + input[y1][x2] * (1 - tx);
        auto r2 = input[y2][x1] * tx + input[y2][x2] * (1 - tx);

        // now interpolate in y direction
        out[x] = r1 * ty + r2 * (1 - ty);
      }
    }
  }
}

static void run_kernel_x(array2f& data, float const kernel[], int kernel_size)
{
  // note: to enable safe in-place operation (ie: src == dst), a buffer is used that delays
  //       output of calculated values back into the image until the kernel has fully passed
  //       over the value in question.  Output is delayed by 'side' iterations.
  // note: the use of negative array indexes in this function is intentional (not a bug)
  if (data.shape().x < kernel_size)
    throw std::runtime_error("filter field smaller than kernel - unimplemented feature");

  auto inp = data.data();
  auto out = data.data();
  auto buf = static_cast<float*>(alloca(kernel_size * sizeof(float)));

  int side = kernel_size / 2;
  for (int y = 0; y < data.shape().y; ++y)
  {
    // left border (replicate left most pixel)
    for (int x = 0; x < side; ++x)
    {
      auto val = 0.0f;
      for (int i = 0; i < side - x; ++i)
        val += kernel[i] * inp[-x];
      for (int i = side - x; i < kernel_size; ++i)
        val += kernel[i] * inp[i - side];
      ++inp;

      // just fill our buffer
      buf[x % side] = val;
    }

    // middle section of row
    for (int x = side; x < data.shape().x - side; ++x)
    {
      auto val = 0.0f;
      for (int i = 0; i < kernel_size; ++i)
        val += kernel[i] * inp[i - side];
      ++inp;

      // output buffered value, and fill more space in it
      *out = buf[x % side];
      buf[x % side] = val;
      ++out;
    }

    // right border (replicate right most pixel)
    for (int x = data.shape().x - side; x < data.shape().x; ++x)
    {
      auto val = 0.0f;
      for (int i = 0; i < kernel_size - ((side + 1) - (data.shape().x - x)); ++i)
        val += kernel[i] * inp[i - side];
      for (int i = kernel_size - ((side + 1) - (data.shape().x - x)); i < kernel_size; ++i)
        val += kernel[i] * inp[data.shape().x - x - 1];
      ++inp;

      // output buffered value and fill more space
      *out = buf[x % side];
      buf[x % side] = val;
      ++out;
    }

    // flush remaining buffered values
    for (int x = data.shape().x; x < data.shape().x + side; ++x)
    {
      *out = buf[x % side];
      ++out;
    }
  }
}

static void run_kernel_y(array2f& data, float const kernel[], int kernel_size)
{
  // note: to enable safe in-place operation, a buffer is used that delays
  //       output of calculated values back into the image until the kernel has fully passed
  //       over the value in question.  Output is delayed by 'side' iterations.
  // note: the use of negative array indexes in this function is intentional (not a bug)
  if (data.shape().y < kernel_size)
    throw std::runtime_error("filter field smaller than kernel - unimplemented feature");

  int side = kernel_size / 2;
  for (int x = 0; x < data.shape().x; ++x)
  {
    auto inp = &data.data()[x];
    auto out = &data.data()[x];
    auto buf = static_cast<float*>(alloca(kernel_size * sizeof(float)));

    // top border (replicate top most pixel)
    for (int y = 0; y < side; ++y)
    {
      auto val = 0.0f;
      for (int i = 0; i < side - y; ++i)
        val += kernel[i] * inp[-y * data.shape().x];
      for (int i = side - y; i < kernel_size; ++i)
        val += kernel[i] * inp[(i - side) * data.shape().x];
      inp += data.shape().x;

      // just fill our buffer
      buf[y % side] = val;
    }

    // middle section of column
    for (int y = side; y < data.shape().y - side; ++y)
    {
      auto val = 0.0f;
      for (int i = 0; i < kernel_size; ++i)
        val += kernel[i] * inp[(i - side) * data.shape().x];
      inp += data.shape().x;

      // output a buffered value and record the currently calculated one
      *out = buf[y % side];
      buf[y % side] = val;
      out += data.shape().x;
    }

    // bottom border (replicate bottom most pixel)
    for (int y = data.shape().y - side; y < data.shape().y; ++y)
    {
      auto val = 0.0f;
      for (int i = 0; i < kernel_size - ((side + 1) - (data.shape().y - y)); ++i)
        val += kernel[i] * inp[(i - side) * data.shape().x];
      for (int i = kernel_size - ((side + 1) - (data.shape().y - y)); i < kernel_size; ++i)
        val += kernel[i] * inp[(data.shape().y - y - 1) * data.shape().x];
      inp += data.shape().x;

      // output a buffered value and record current calculation
      *out = buf[y % side];
      buf[y % side] = val;
      out += data.shape().x;
    }

    // flush remaining buffered values
    for (int y = data.shape().y; y < data.shape().y + side; ++y)
    {
      *out = buf[y % side];
      out += data.shape().x;
    }
  }
}

static void gaussian_blur(array2f& data, int kernel_size, float sigma)
{
  // auto calculate the kernel size from sigma or vice versa (if desired)
  if (kernel_size == 0 && sigma > 0.0f)
    kernel_size = static_cast<int>(std::round(sigma * 4 * 2 + 1)) | 1;
  else if (sigma <= 0.0f && kernel_size > 0)
    sigma = ((kernel_size - 1) * 0.5f - 1.0f) * 0.3f + 0.8f;

  // sanity checks
  if (kernel_size < 0 || kernel_size % 2 != 1)
    throw std::invalid_argument("gaussian_blur: invalid kernel size (must be 0 or odd)");

  // build the kernel
  float* kernel = static_cast<float*>(alloca(kernel_size * sizeof(float)));
  {
    float scale_2x = -0.5f / (sigma * sigma);
    float sum = 0.0f;
    for (int i = 0; i < kernel_size; ++i)
    {
      float x = i - (kernel_size - 1) * 0.5f;
      kernel[i] = std::exp(scale_2x * x * x);
      sum += kernel[i];
    }
    sum = 1.0f / sum;
    for (int i = 0; i < kernel_size; ++i)
      kernel[i] *= sum;
  }

  // as our kernel is symetrical, we can do 2 passes of the 1D kernel
  run_kernel_x(data, kernel, kernel_size);
  run_kernel_y(data, kernel, kernel_size);
}

auto optical_flow_rf3::track_fields(
      array2f const& from
    , array2f const& to
    , array2f2& velocity
    , bool use_initial_flow
    ) const -> void
{
  auto& flow0 = velocity;

  // field buffers
  array2f img0(dims_);
  array2f img1(dims_);

  // matrix buffers
  array2<vec5d> r0(dims_);
  array2<vec5d> r1(dims_);
  array2<vec5d> m(dims_);

  // temporary flow buffers
  array2f2 f0(info_[1].dims);
  array2f2 f1(info_[1].dims);

  // flow pointers
  auto flow = &f0;
  auto prev_flow = &f1;

  // process from the coarsest level upwards
  auto first = true;
  for (auto lvl = info_.size(); lvl-- != 0; )
  {
    auto const& info = info_[lvl];

    // hack the size of wrapper objects needed for this level
    img1.hack_size(info.dims);
    r0.hack_size(info.dims);
    r1.hack_size(info.dims);
    m.hack_size(info.dims);

    // on the final iteration, write directly to our output vectors
    if (lvl > 0)
      flow->hack_size(info.dims);
    else
      flow = &flow0;

    // fill our flow with the appropriate initial state
    if (first)
    {
      // top level - use the passed in flow as initial state?
      if (use_initial_flow)
      {
        // yes - resize down from the input flow
        interpolate(flow0, *flow);

        // scale the vectors to match the level size
        flow->multiply(info.scale);
      }
      else
      {
        // no - use zero velocities
        flow->fill(vec2f{0.0f, 0.0f});
      }
      first = false;
    }
    else
    {
      // not the top level, resize up from previous level's data
      interpolate(*prev_flow, *flow);

      // scale the vectors to match the level size (ie: multiply by inverse scale)
      flow->multiply(1.0f / scale_);
    }

    // process from/to for this level
    // BUG BUG - OpenCV still blurs with sigma 0 (which CV sees as a special value
    //           meaning automatically calculate sigma - 0.8 for our case) at level 0!
    if (lvl > 0)
    {
      img0.copy(from);
      gaussian_blur(img0, info.ksize, info.sigma);
      interpolate(img0, img1);
      poly_exp(img1, r0);

      img0.copy(to);
      gaussian_blur(img0, info.ksize, info.sigma);
      interpolate(img0, img1);
      poly_exp(img1, r1);
    }
    else
    {
      poly_exp(from, r0);
      poly_exp(to, r1);
    }

    // update the matricies
    update_matrices(r0, r1, *flow, m, 0, flow->shape().y);

    // perform our blur/update iterations
    for (int i = 0; i < iterations_; ++i)
      blur_iteration(r0, r1, *flow, m, i < iterations_ - 1);

    // swap the current and previous flow for the next level
    std::swap(flow, prev_flow);
  }
}

auto optical_flow_rf3::poly_exp_setup() -> void
{
  // setup constant part of polyexp calls
  kbuf_.resize(poly_n_ * 6 + 3);

  auto* g = &kbuf_[poly_n_];
  auto* xg = g + poly_n_ * 2 + 1;
  auto* xxg = xg + poly_n_ * 2 + 1;

  if (poly_sigma_ < std::numeric_limits<float>::epsilon())
    poly_sigma_ = poly_n_ * 0.3;

  double s = 0.0;
  for (int x = -poly_n_; x <= poly_n_; ++x)
  {
    g[x] = std::exp(-x * x / (2.0 * poly_sigma_ * poly_sigma_));
    s += g[x];
  }

  s = 1.0 / s;
  for (int x = -poly_n_; x <= poly_n_; ++x)
  {
    g[x] = g[x] * s;
    xg[x] = x * g[x];
    xxg[x] = x * x * g[x];
  }

  double gmat[6][6];
  memset(&gmat, 0, 6 * 6 * sizeof(double));

  for(int y = -poly_n_; y <= poly_n_; y++ )
  {
    for(int x = -poly_n_; x <= poly_n_; x++ )
    {
      gmat[0][0] += g[y] * g[x];
      gmat[1][1] += g[y] * g[x] * x * x;
      gmat[3][3] += g[y] * g[x] * x * x * x * x;
      gmat[5][5] += g[y] * g[x] * x * x * y * y;
    }
  }

  //gmat[0][0] = 1.0;
  gmat[2][2] = gmat[0][3] = gmat[0][4] = gmat[3][0] = gmat[4][0] = gmat[1][1];
  gmat[4][4] = gmat[3][3];
  gmat[3][4] = gmat[4][3] = gmat[5][5];

  // invG:
  // [ x        e  e    ]
  // [    y             ]
  // [       y          ]
  // [ e        z       ]
  // [ e           z    ]
  // [                u ]
  double ginv[6][6];
  // colesky matrix inversion of gmat (destructive to gmat)
  {
    memset(&ginv, 0, 6 * 6 * sizeof(double));
    ginv[0][0] = ginv[1][1] = ginv[2][2] = ginv[3][3] = ginv[4][4] = ginv[5][5] = 1.0;

    double s;
    for (int i = 0; i < 6; ++i)
    {
      for (int j = 0; j < i; ++j)
      {
        s = gmat[i][j];
        for (int k = 0; k < j; ++k)
          s -= gmat[i][k] * gmat[j][k];
        gmat[i][j] = s * gmat[j][j];
      }
      s = gmat[i][i];
      for (int k = 0; k < i; ++k)
      {
        double t = gmat[i][k];
        s -= t * t;
      }
      if (s < std::numeric_limits<double>::epsilon())
        throw std::runtime_error("optical_flow: unable to invert 'G' matrix");
      gmat[i][i] = 1.0 / std::sqrt(s);
    }

    for (int i = 0; i < 6; ++i)
    {
      for (int j = 0; j < 6; ++j)
      {
        s = ginv[i][j];
        for (int k = 0; k < i; ++k)
          s -= gmat[i][k] * ginv[k][j];
        ginv[i][j] = s * gmat[i][i];
      }
    }

    for (int i = 6; i-- != 0; )
    {
      for (int j = 0; j < 6; ++j)
      {
        s = ginv[i][j];
        for (int k = 6; k-- > i; )
          s -= gmat[k][i] * ginv[k][j];
        ginv[i][j] = s * gmat[i][i];
      }
    }
  }

  // store the values we care about
  ig11_ = ginv[1][1];
  ig03_ = ginv[0][3];
  ig33_ = ginv[3][3];
  ig55_ = ginv[5][5];
}

auto optical_flow_rf3::poly_exp(array2f const& src, array2<vec5d>& dst) const -> void
{
  // allocate some buffers
  // TODO - can this be allocated outside here to reduce repeat allocs?
  auto rbuf = array1f{(src.shape().x + poly_n_ * 2) * 3};

  // get the pointers into our buffers
  // don't store these in the class, because it would mean writing our own copy constructor
  auto const* g = &kbuf_[poly_n_];
  auto const* xg = g + poly_n_ * 2 + 1;
  auto const* xxg = xg + poly_n_ * 2 + 1;
  auto* row = &rbuf[poly_n_ * 3];

  int rows = src.shape().y;
  int cols = src.shape().x;

  for (int y = 0; y < rows; ++y)
  {
    float g0 = g[0], g1, g2;
    float const* srow0 = src[y];
    float const* srow1 = nullptr;
    vec5d* drow = dst[y];

    // vertical part of convolution
    for (int x = 0; x < cols; ++x)
    {
      row[x*3] = srow0[x] * g0;
      row[x*3+1] = row[x*3+2] = 0.0f;
    }

    for (int k = 1; k <= poly_n_; ++k)
    {
      g0 = g[k]; g1 = xg[k]; g2 = xxg[k];
      srow0 = src[std::max(y - k, 0)];
      srow1 = src[std::min(y + k, rows - 1)];

      for (int x = 0; x < cols; ++x)
      {
        auto p = srow0[x] + srow1[x];
        auto t0 = row[x*3] + g0 * p;
        auto t1 = row[x*3+1] + g1 * (srow1[x] - srow0[x]);
        auto t2 = row[x*3+2] + g2 * p;

        row[x*3] = t0;
        row[x*3+1] = t1;
        row[x*3+2] = t2;
      }
    }

    // horizontal part of convolution
    for (int x = 0; x < poly_n_ * 3; ++x)
    {
      row[-1-x] = row[2-x];
      row[cols*3+x] = row[cols*3+x-3];
    }

    for (int x = 0; x < cols; ++x)
    {
      g0 = g[0];
      // r1 ~ 1, r2 ~ x, r3 ~ y, r4 ~ x^2, r5 ~ y^2, r6 ~ xy
      double b1 = row[x*3] * g0, b2 = 0, b3 = row[x*3+1] * g0,
             b4 = 0, b5 = row[x*3+2] * g0, b6 = 0;

      for (int k = 1; k <= poly_n_; ++k)
      {
        double tg = row[(x+k)*3] + row[(x-k)*3];
        g0 = g[k];
        b1 += tg*g0;
        b4 += tg*xxg[k];
        b2 += (row[(x+k)*3] - row[(x-k)*3]) * xg[k];
        b3 += (row[(x+k)*3+1] + row[(x-k)*3+1]) * g0;
        b6 += (row[(x+k)*3+1] - row[(x-k)*3+1]) * xg[k];
        b5 += (row[(x+k)*3+2] + row[(x-k)*3+2]) * g0;
      }

      // do not store r1
      drow[x][1] = b2 * ig11_;
      drow[x][0] = b3 * ig11_;
      drow[x][3] = b1 * ig03_ + b4 * ig33_;
      drow[x][2] = b1 * ig03_ + b5 * ig33_;
      drow[x][4] = b6 * ig55_;
    }
  }

  // TODO - why is this here?!?! from OpenCV implementation
  // all this does is realign row with rbuf, but it's never used...
  row -= poly_n_*3;
}

auto optical_flow_rf3::update_matrices(
      array2<vec5d> const& r0
    , array2<vec5d> const& r1
    , array2f2 const& flow
    , array2<vec5d>& mat
    , int row_from
    , int row_to
    ) const -> void
{
  constexpr int bsize = 5;
  static const float border[bsize] = {0.14f, 0.14f, 0.4472f, 0.4472f, 0.4472f};

  for (int y = row_from; y < row_to; ++y)
  {
    auto flow_y = flow[y];
    auto r0_y = r0[y];
    auto mat_y = mat[y];

    for (int x = 0; x < flow.shape().x; ++x)
    {
      vec5d const& r0_yx = r0_y[x];

      auto dx = flow_y[x].x, dy = flow_y[x].y;
      auto fx = x + dx, fy = y + dy;

      int x1 = std::floor(fx), y1 = std::floor(fy);
      float r2, r3, r4, r5, r6;

      fx -= x1; fy -= y1;

      // yes, these casts to unsigned are important (we exploit negatives wraping to very positives!)
      if (   (unsigned) x1 < (unsigned) (flow.shape().x - 1)
          && (unsigned) y1 < (unsigned) (flow.shape().y - 1))
      {
        auto a00 = (1.0f - fx) * (1.0f - fy), a01 = fx * (1.0f - fy),
             a10 = (1.0f - fx) * fy, a11 = fx * fy;

        vec5d const& r1_00 = r1[y1][x1];
        vec5d const& r1_01 = r1[y1][x1+1];
        vec5d const& r1_10 = r1[y1+1][x1];
        vec5d const& r1_11 = r1[y1+1][x1+1];

        r2 = a00 * r1_00[0] + a01 * r1_01[0] + a10 * r1_10[0] + a11 * r1_11[0];
        r3 = a00 * r1_00[1] + a01 * r1_01[1] + a10 * r1_10[1] + a11 * r1_11[1];
        r4 = a00 * r1_00[2] + a01 * r1_01[2] + a10 * r1_10[2] + a11 * r1_11[2];
        r5 = a00 * r1_00[3] + a01 * r1_01[3] + a10 * r1_10[3] + a11 * r1_11[3];
        r6 = a00 * r1_00[4] + a01 * r1_01[4] + a10 * r1_10[4] + a11 * r1_11[4];

        r4 = (r0_yx[2] + r4) * 0.5f;
        r5 = (r0_yx[3] + r5) * 0.5f;
        r6 = (r0_yx[4] + r6) * 0.25f;
      }
      else
      {
        r2 = r3 = 0.0f;
        r4 = r0_yx[2];
        r5 = r0_yx[3];
        r6 = r0_yx[4] * 0.5f;
      }

      r2 = (r0_yx[0] - r2) * 0.5f;
      r3 = (r0_yx[1] - r3) * 0.5f;

      r2 += r4 * dy + r6 * dx;
      r3 += r6 * dy + r5 * dx;

      // yes, these casts to unsigned are important (we exploit negatives wraping to very positives!)
      if (   (unsigned) (x - bsize) >= (unsigned) (flow.shape().x - bsize * 2)
          || (unsigned) (y - bsize) >= (unsigned) (flow.shape().y - bsize * 2))
      {
        auto scale =
            (x < bsize ? border[x] : 1.0f)
          * (x >= flow.shape().x - bsize ? border[flow.shape().x - x - 1] : 1.0f)
          * (y < bsize ? border[y] : 1.0f)
          * (y >= flow.shape().y - bsize ? border[flow.shape().y - y - 1] : 1.0f);

        r2 *= scale; r3 *= scale; r4 *= scale;
        r5 *= scale; r6 *= scale;
      }

      vec5d& mat_yx = mat_y[x];
      mat_yx[0] = r4*r4 + r6*r6; // G(1,1)
      mat_yx[1] = (r4 + r5)*r6;  // G(1,2)=G(2,1)
      mat_yx[2] = r5*r5 + r6*r6; // G(2,2)
      mat_yx[3] = r4*r2 + r6*r3; // h(1)
      mat_yx[4] = r6*r2 + r5*r3; // h(2)
    }
  }
}

auto optical_flow_rf3::blur_iteration(
      array2<vec5d> const& r0
    , array2<vec5d> const& r1
    , array2f2& flow
    , array2<vec5d>& mat
    , bool update_mats
    ) const -> void
{
  int m = win_size_ / 2;
  int min_update_stripe = std::max((1 << 10) / flow.shape().x, win_size_);
  double scale = 1.0 / (win_size_ * win_size_);

  // allocate a buffer
  // TODO - move this outside and unify with poly_exp's temp buffer since
  //        we only need one of them at a time
  std::unique_ptr<vec5d[]> vbuf(new vec5d[flow.shape().x + m * 2 + 2]);
  vec5d* vsum = &vbuf[m+1];

  // init vsum
  vec5d const* srow0 = mat[0];
  for (int x = 0; x < flow.shape().x; ++x)
  {
    vsum[x][0] = srow0[x][0] * (m + 2);
    vsum[x][1] = srow0[x][1] * (m + 2);
    vsum[x][2] = srow0[x][2] * (m + 2);
    vsum[x][3] = srow0[x][3] * (m + 2);
    vsum[x][4] = srow0[x][4] * (m + 2);
  }

  for (int y = 1; y < m; ++y)
  {
    srow0 = mat[std::min(y, flow.shape().y - 1)];
    for (int x = 0; x < flow.shape().x; ++x)
    {
      vsum[x][0] += srow0[x][0];
      vsum[x][1] += srow0[x][1];
      vsum[x][2] += srow0[x][2];
      vsum[x][3] += srow0[x][3];
      vsum[x][4] += srow0[x][4];
    }
  }

  // compute blur(G)*flow=blur(h)
  for (int y = 0, y0 = 0; y < flow.shape().y; ++y)
  {
    auto flow_y = flow[y];

                 srow0 = mat[std::max(y - m - 1, 0)];
    vec5d const* srow1 = mat[std::min(y + m, flow.shape().y - 1)];

    // vertical blur
    for (int x = 0; x < flow.shape().x; ++x)
    {
      vsum[x][0] += srow1[x][0] - srow0[x][0];
      vsum[x][1] += srow1[x][1] - srow0[x][1];
      vsum[x][2] += srow1[x][2] - srow0[x][2];
      vsum[x][3] += srow1[x][3] - srow0[x][3];
      vsum[x][4] += srow1[x][4] - srow0[x][4];
    }

    // update borders
    for (int x = 0; x < m + 1; ++x)
    {
      // left border
      vsum[-x-1][4] = vsum[-x][4];
      vsum[-x-1][3] = vsum[-x][3];
      vsum[-x-1][2] = vsum[-x][2];
      vsum[-x-1][1] = vsum[-x][1];
      vsum[-x-1][0] = vsum[-x][0];

      // right border
      vsum[flow.shape().x+x][0] = vsum[flow.shape().x+x-1][0];
      vsum[flow.shape().x+x][1] = vsum[flow.shape().x+x-1][1];
      vsum[flow.shape().x+x][2] = vsum[flow.shape().x+x-1][2];
      vsum[flow.shape().x+x][3] = vsum[flow.shape().x+x-1][3];
      vsum[flow.shape().x+x][4] = vsum[flow.shape().x+x-1][4];
    }

    // init g** and h*
    double g11 = vsum[0][0] * (m + 2);
    double g12 = vsum[0][1] * (m + 2);
    double g22 = vsum[0][2] * (m + 2);
    double h1  = vsum[0][3] * (m + 2);
    double h2  = vsum[0][4] * (m + 2);

    for (int x = 1; x < m; ++x)
    {
      g11 += vsum[x][0];
      g12 += vsum[x][1];
      g22 += vsum[x][2];
      h1  += vsum[x][3];
      h2  += vsum[x][4];
    }

    // horizontal blur
    for (int x = 0; x < flow.shape().x; ++x)
    {
      g11 += vsum[x+m][0] - vsum[x-m-1][0];
      g12 += vsum[x+m][1] - vsum[x-m-1][1];
      g22 += vsum[x+m][2] - vsum[x-m-1][2];
      h1  += vsum[x+m][3] - vsum[x-m-1][3];
      h2  += vsum[x+m][4] - vsum[x-m-1][4];

      double g11_ = g11 * scale;
      double g12_ = g12 * scale;
      double g22_ = g22 * scale;
      double h1_  = h1 * scale;
      double h2_  = h2 * scale;

      double idet = 1.0 / (g11_ * g22_ - g12_ * g12_ + 1e-3);

      flow_y[x].x = (g11_ *h2_ - g12_ * h1_) * idet;
      flow_y[x].y = (g22_ *h1_ - g12_ * h2_) * idet;
    }

    int y1 = y == flow.shape().y - 1 ? flow.shape().y : y - win_size_;
    if (update_mats && (y1 == flow.shape().y || y1 >= y0 + min_update_stripe))
    {
      update_matrices(r0, r1, flow, mat, y0, y1);
      y0 = y1;
    }
  }
}

auto optical_flow_rf3::interpolate_gaps(array2f2& velocity) const -> void
{
  int min_bins_for_avg = (spacing_ * spacing_) * min_frac_bins_for_avg_;
  auto half_spacing = spacing_ / 2;

  // stage 1 - build the low resolution grid based on cell averages
  auto low_size = (velocity.shape() + vec2i{spacing_ - 1, spacing_ - 1}) / spacing_;
  array2<vec2f> grid{low_size};
  for (auto y = 0; y < grid.shape().y; ++y)
  {
    for (auto x = 0; x < grid.shape().x; ++x)
    {
      int count = 0;
      float sum_u = 0.0f, sum_v = 0.0f;
      auto ymax = std::min((y + 1) * spacing_, velocity.shape().y);
      auto xmax = std::min((x + 1) * spacing_, velocity.shape().x);
      for (auto yy = y * spacing_; yy < ymax; ++yy)
      {
        for (auto xx = x * spacing_; xx < xmax; ++xx)
        {
          auto val_u = velocity[yy][xx].x;
          auto val_v = velocity[yy][xx].y;
          if (val_u > bad_vel_cmp)
          {
            ++count;
            sum_u += val_u;
            sum_v += val_v;
          }
        }
      }
      if (count < min_bins_for_avg)
      {
        grid[y][x].x = bad_vel;
        grid[y][x].y = bad_vel;
      }
      else
      {
        grid[y][x].x = sum_u / count;
        grid[y][x].y = sum_v / count;
      }
    }
  }

  // stage 2 - fill in missing low resolution cells using inverse distance weighting
  auto pwr = idw_low_res_pwr_ * 0.5; // to avoid std::pow(std::sqrt(...))
  vec2i max(
        ((grid.shape().x - 1) * spacing_ + half_spacing < velocity.shape().x ? grid.shape().x : grid.shape().x - 1)
      , ((grid.shape().y - 1) * spacing_ + half_spacing < velocity.shape().y ? grid.shape().y : grid.shape().y - 1));
  for (auto y = 0; y < max.y; ++y)
  {
    auto yo = y * spacing_ + half_spacing;
    for (auto x = 0; x < max.x; ++x)
    {
      auto& out_u = velocity[yo][x * spacing_ + half_spacing].x;
      auto& out_v = velocity[yo][x * spacing_ + half_spacing].y;

      if (out_u < bad_vel_cmp)
      {
        double ac_weight = 0.0;
        double ac_val_u = 0.0;
        double ac_val_v = 0.0;

        // search in rings around the point until we get at least 8 neighbours
        // TODO - is this really doing what it says!?!?!! this really IDVs the whole low res grid!
        for (auto yy = 0; yy < grid.shape().y; ++yy)
        {
          for (auto xx = 0; xx < grid.shape().x; ++xx)
          {
            auto vv_u = grid[yy][xx].x;
            auto vv_v = grid[yy][xx].y;
            if (vv_u > bad_vel_cmp)
            {
              double weight = 1.0 / std::pow((double(xx) - x) * (double(xx) - x) + (double(yy) - y) * (double(yy) - y), pwr);
              //double weight = 1.0 / std::pow(std::hypot(double(xx) - x, double(yy) - y), idw_low_res_pwr_);
              ac_val_u += vv_u * weight;
              ac_val_v += vv_v * weight;
              ac_weight += weight;
            }
          }
        }
        if (ac_weight > 0.0)
        {
          out_u = ac_val_u / ac_weight;
          out_v = ac_val_v / ac_weight;
        }
        else
        {
          out_u = 0.0;
          out_v = 0.0;
        }
      }
    }
  }

  // stage 4 - perform the high resolution interpolation
  pwr = idw_high_res_pwr_ * 0.5; // to avoid std::pow(std::sqrt(...))
  array2f2 ref(velocity);
  for (auto y = 0; y < velocity.shape().y; ++y)
  {
    for (auto x = 0; x < velocity.shape().x; ++x)
    {
      if (ref[y][x].x > bad_vel_cmp)
        continue;

      double ac_weight = 0.0;
      double ac_val_u = 0.0;
      double ac_val_v = 0.0;
      vec2i min(std::max(x, spacing_) - spacing_, std::max(y, spacing_) - spacing_);
      vec2i max(std::min(x + spacing_, velocity.shape().x), std::min(y + spacing_, velocity.shape().y));
      for (auto yy = min.y; yy < max.y; ++yy)
      {
        for (auto xx = min.x; xx < max.x; ++xx)
        {
          auto const& vv_u = ref[yy][xx].x;
          auto const& vv_v = ref[yy][xx].y;
          if (vv_u > bad_vel_cmp)
          {
            //double weight = 1.0 / std::pow(std::hypot(double(xx) - x, double(yy) - y), idw_high_res_pwr_);
            double weight = 1.0 / std::pow((double(xx) - x) * (double(xx) - x) + (double(yy) - y) * (double(yy) - y), pwr);
            ac_val_u += vv_u * weight;
            ac_val_v += vv_v * weight;
            ac_weight += weight;
          }
        }
      }
      velocity[y][x].x = ac_val_u / ac_weight;
      velocity[y][x].y = ac_val_v / ac_weight;
    }
  }
}

