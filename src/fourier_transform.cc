/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "fourier_transform.h"
#include "format.h"
#include "profile.h"
#include "trace.h"
#include "unit_test.h"
#include <fftw3.h>
#include <mutex>

using namespace steps;

#if opt_fftw_single_precision
  #define fftw_init_threads fftwf_init_threads
  #define fftw_plan_with_nthreads fftwf_plan_with_nthreads
  #define fftw_plan fftwf_plan
  #define fftw_complex fftwf_complex
  #define fftw_destroy_plan fftwf_destroy_plan
  #define fftw_malloc fftwf_malloc
  #define fftw_free fftwf_free
  #define fftw_import_wisdom_from_filename fftwf_import_wisdom_from_filename
  #define fftw_export_wisdom_to_filename fftwf_export_wisdom_to_filename
  #define fftw_plan_dft_r2c_2d fftwf_plan_dft_r2c_2d
  #define fftw_plan_dft_c2r_2d fftwf_plan_dft_c2r_2d
  #define fftw_execute_dft_r2c fftwf_execute_dft_r2c
  #define fftw_execute_dft_c2r fftwf_execute_dft_c2r
#endif

static_assert(std::is_pointer<fftw_plan>::value, "fftw_plan expected to be a pointer type");
static_assert(sizeof(fftw_plan) == sizeof(void*), "fftw_plan expected to have same size as void*");
static_assert(
      sizeof(std::complex<fourier_transform::real>) == sizeof(fftw_complex)
    , "fftw_complex incompatible with std::complex");

static profile prof_fft_plan{"fourier_transform::planning"};
static profile prof_fft_fwd{"fourier_transform::forward"};
static profile prof_fft_inv{"fourier_transform::inverse"};

static std::mutex mut_fftw_;
static bool fftw_init_ = false;
static unsigned plan_quality_ = FFTW_MEASURE;

static auto delete_plan(void* plan) -> void
{
  fftw_destroy_plan(static_cast<fftw_plan>(plan));
}

auto steps::aligned_malloc(size_t size) -> void*
{
  if (auto ptr = fftw_malloc(size))
    return ptr;
  throw std::runtime_error{format("Failed to allocate aligned memory of size {}", size)};
}

auto steps::aligned_free(void* ptr) -> void
{
  fftw_free(ptr);
}

auto fourier_transform::load_wisdom(std::filesystem::path const& path) -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_fftw_};
  if (!fftw_import_wisdom_from_filename(path.c_str()))
    throw std::runtime_error{format("Failed to load FFTW wisdom from path {}", path.native())};
}

auto fourier_transform::save_wisdom(std::filesystem::path const& path) -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_fftw_};
  if (!fftw_export_wisdom_to_filename(path.c_str()))
    throw std::runtime_error{format("Failed to save FFTW wisdom to path {}", path.native())};
}

auto fourier_transform::set_plan_exhaustive(bool enabled) -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_fftw_};
  plan_quality_ = enabled ? FFTW_EXHAUSTIVE : FFTW_MEASURE;
}

fourier_transform::fourier_transform(vec2i shape, int threads)
  : shape_r_{shape}
  , shape_c_{shape.x / 2 + 1, shape.y}
  , plan_r2c_{nullptr, &delete_plan}
  , plan_c2r_{nullptr, &delete_plan}
{
  auto ps = profile::scope{prof_fft_plan};

  auto lock = std::lock_guard<std::mutex>{mut_fftw_};

  // ensure fftw is correctly initialized
  if (!fftw_init_)
  {
    fftw_init_threads();
    fftw_init_ = true;
  }

  // set the number of threads to use
  fftw_plan_with_nthreads(threads);

  // temporary buffers needed for planning
  auto r_data = array2r{shape};
  auto c_data = array2c{vec2i{shape.x / 2 + 1, shape.y}};

  // plan the forward transformation
  // cast is safe according to c++11 standard
  plan_r2c_.reset(
      fftw_plan_dft_r2c_2d(
            shape.y
          , shape.x
          , r_data.data()
          , reinterpret_cast<fftw_complex*>(c_data.data())
          , plan_quality_));
  if (!plan_r2c_)
    throw std::runtime_error{format("Failed to plan r2c fft for shape {}", shape)};

  // plan the inverse transformation
  // cast is safe according to c++11 standard
  plan_c2r_.reset(
      fftw_plan_dft_c2r_2d(
            shape.y
          , shape.x
          , reinterpret_cast<fftw_complex*>(c_data.data())
          , r_data.data()
          , plan_quality_));
  if (!plan_c2r_)
    throw std::runtime_error{format("Failed to plan c2r fft for shape {}", shape)};
}

auto fourier_transform::execute_forward(array2r const& src, array2c& dst) const -> void
{
  auto ps = profile::scope{prof_fft_fwd};

  // sanity checks
  if (src.shape() != shape_r_)
    throw std::runtime_error{format("Array size mismatch. Passed {} expected {}", src.shape(), shape_r_)};
  if (dst.shape() != shape_c_)
    throw std::runtime_error{format("Array size mismatch. Passed {} expected {}", dst.shape(), shape_c_)};

  // perform the transform
  // r2c plans are non-destructive by default so the const_cast here is safe (see FFTW manual on planner flags)
  // reinterpret cast is safe according to c++11 standard
  fftw_execute_dft_r2c(
        static_cast<fftw_plan>(plan_r2c_.get())
      , const_cast<real*>(src.data())
      , reinterpret_cast<fftw_complex*>(dst.data()));

  // fftw doesn't give us a normalized result, so we need to normalize the output
  // by dividing by number of elements in the real array
  // don't use * (1/norm_factor) because of potential numerical stability issues
  real norm_factor = shape_r_.x * shape_r_.y;
  for (auto i = 0; i < dst.size(); ++i)
    dst.data()[i] /= norm_factor;
}

auto fourier_transform::execute_inverse(array2c& src, array2r& dst) const -> void
{
  auto ps = profile::scope{prof_fft_inv};

  // sanity checks
  if (src.shape() != shape_c_)
    throw std::runtime_error{format("Array size mismatch. Passed {} expected {}", src.shape(), shape_c_)};
  if (dst.shape() != shape_r_)
    throw std::runtime_error{format("Array size mismatch. Passed {} expected {}", dst.shape(), shape_r_)};

  // perform the transform
  // c2r plans are destructive to inputs by default so we cannot const_caast here (see FFTW manual on planner flags)
  // cast is safe according to c++11 standard
  fftw_execute_dft_c2r(
        static_cast<fftw_plan>(plan_c2r_.get())
      , reinterpret_cast<fftw_complex*>(src.data())
      , dst.data());
}

// LCOV_EXCL_START
TEST_CASE("fourier_transform")
{
  CHECK(plan_quality_ == FFTW_MEASURE);

  CHECK_THROWS(fourier_transform::load_wisdom("bad"));

  fourier_transform fft{{16, 16}, 4};
  CHECK(fft.shape_r() == vec2i{16, 16});
  CHECK(fft.shape_c() == vec2i{9, 16});

  fourier_transform::array2r real{fft.shape_r()};
  fourier_transform::array2c cplx{fft.shape_c()};
  fourier_transform::array2r test{fft.shape_r()};

  // test the array size sanity checks
  fourier_transform::array2r bad_real{vec2i{1,1}};
  fourier_transform::array2c bad_cplx{vec2i{1,1}};
  CHECK_THROWS(fft.execute_forward(bad_real, cplx));
  CHECK_THROWS(fft.execute_forward(real, bad_cplx));
  CHECK_THROWS(fft.execute_inverse(bad_cplx, real));
  CHECK_THROWS(fft.execute_inverse(cplx, bad_real));

  // fill an array with some random numbers and check that the FFT roundtrip is stable
  std::srand(250383);
  for (int i = 0; i < real.size(); ++i)
    real.data()[i] = (std::rand() % 100) - 50;

  CHECK_NOTHROW(fft.execute_forward(real, cplx));
  CHECK_NOTHROW(fft.execute_inverse(cplx, test));

  // ensure RMSE is effectively zero
  auto rmse = 0.0;
  for (int i = 0; i < real.size(); ++i)
    rmse += (test.data()[i] - real.data()[i]) * (test.data()[i] - real.data()[i]);
  rmse = std::sqrt(rmse / real.size());
  CHECK(rmse == approx(0.0));

  CHECK_NOTHROW(fourier_transform::save_wisdom("ut-tmp"));
  CHECK_NOTHROW(fourier_transform::load_wisdom("ut-tmp"));

  CHECK_NOTHROW(fourier_transform::set_plan_exhaustive(true));
  CHECK(plan_quality_ == FFTW_EXHAUSTIVE);

  // reset plan quality to exhaustive so that the rest of our unit tests don't suffer delays!
  fourier_transform::set_plan_exhaustive(false);
}
// LCOV_EXCL_STOP

#if 0
// HACK
#include "nc.h"
TEST_CASE("blah")
{
  auto shape_r = vec2i{250, 370};

  plan_quality_ = FFTW_MEASURE;
  auto fft = fourier_transform{shape_r};
  auto c = fourier_transform::array2c{fft.shape_c()};
  auto r = fourier_transform::array2r{fft.shape_r()};

  trace(trace_level::log, "shape_r {} shape_c {}", fft.shape_r(), fft.shape_c());

  c.fill({0.0,0.0});
  c[4][7] = 10.0;

  fft.execute_inverse(c, r);

  auto fout = nc::file{"tester.nc", nc::io_mode::create};
  auto& dy = fout.create_dimension("y", fft.shape_r().y);
  auto& dx = fout.create_dimension("x", fft.shape_r().x);
  auto& vr = fout.create_variable("real", nc::data_type::f32, {&dy, &dx});
  vr.write(r);
}
#endif
