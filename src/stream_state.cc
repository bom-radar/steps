/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "stream_state.h"
#include "compression.h"
#include "diagnostics.h"
#include "thread_pool.h"
#include "unit_test.h"

#include <sstream>

using namespace steps;

static constexpr uint64_t cache_file_magic = 0x636d337370657473;
static constexpr uint32_t cache_file_version = 1;

struct cache_file_header
{
  uint64_t  magic;
  uint32_t  version;
  int       levels;
  vec2i     grid_shape;
  vec2i     psd_shape;
  int       compression;
};

template <typename T>
constexpr auto zero_low_bits_mask(int bits) -> T
{
  return ~((T(1) << T(bits)) - T(1));
}

template <typename T>
auto precision_reduced(array2<T> const& data, int bits_to_mask) -> array1<uint32_t>
{
  // sanity checks to ensure our casts below are safe
  static_assert(std::is_same_v<T, float> || std::is_same_v<T, vec2f> || std::is_same_v<T, cell_stats>, "Unexpected type");
  static_assert(std::numeric_limits<float>::is_iec559, "Expected float to be IEEE754");
  static_assert(sizeof(float) == sizeof(uint32_t), "Expected float to be 32-bit");
  static_assert(sizeof(vec2f) == 2 * sizeof(float), "Expected vec2f to be 2 x 32-bit floats");
  static_assert(sizeof(cell_stats) == 3 * sizeof(float), "Expected cell_stats to be 3 x 32-bit floats");

  auto mask = zero_low_bits_mask<uint32_t>(bits_to_mask);
  auto buf = array1<uint32_t>{int(data.size_bytes() / sizeof(float))};
  auto ptr = reinterpret_cast<uint32_t const*>(&data.data()[0]);
  for (auto i = 0; i < data.size(); ++i)
    buf[i] = ptr[i] & mask;
  return buf;
}

auto stream_state::write(std::ostream& os, int compression, int precision_reduction) const -> std::ostream&
{
  // sanity checks
  if (precision_reduction < 0 || precision_reduction >= std::numeric_limits<float>::digits)
    throw std::runtime_error{"Invalid precision reduction requested"};

  // write the header
  cache_file_header header;
  header.magic = cache_file_magic;
  header.version = cache_file_version;
  header.levels = cascade.size();
  header.grid_shape = field.shape();
  header.psd_shape = psd.shape();
  header.compression = compression;
  os.write(reinterpret_cast<char const*>(&header), sizeof(header));

  // TODO - write information needed to detect relevant engine config changes and invalidate entries

  // write the actual state data
  if (compression != 0)
  {
    auto queue_compression = [&](auto& data)
    {
      return thread_pool_queue_task([&]()
      {
        if (precision_reduction > 0)
        {
          // zero out lowest n bits of mantissa, reducing precision from 23 to 23-n bits
          auto buf = precision_reduced(data, precision_reduction);
          return zlib_compress(span<char const>{reinterpret_cast<char const*>(buf.data()), buf.size_bytes()}, compression);
        }
        else
          return zlib_compress(span<char const>{reinterpret_cast<char const*>(data.data()), data.size_bytes()}, compression);
      });
    };
    auto fut_field = queue_compression(field);
    auto fut_flow = queue_compression(flow);
    auto fut_psd = queue_compression(psd);
    auto fut_cascade = vector<thread_pool_future<pair<array1<char>, int>>>();
    for (auto lvl = 0; lvl < levels(); ++lvl)
      fut_cascade.push_back(queue_compression(cascade[lvl]));
    auto fut_cascade_stats = vector<thread_pool_future<pair<array1<char>, int>>>();
    for (auto lvl = 0; lvl < levels(); ++lvl)
      fut_cascade_stats.push_back(queue_compression(cascade_stats[lvl]));

    auto write_compressed_block = [&](auto const& data)
    {
      os.write(reinterpret_cast<char const*>(&data.second), sizeof(data.second));
      os.write(data.first.data(), data.second);
    };
    write_compressed_block(fut_field.get());
    write_compressed_block(fut_flow.get());
    write_compressed_block(fut_psd.get());
    for (auto lvl = 0; lvl < levels(); ++lvl)
      write_compressed_block(fut_cascade[lvl].get());
    os.write(reinterpret_cast<char const*>(&var_flow), sizeof(var_flow));
    os.write(reinterpret_cast<char const*>(&ac_flow), sizeof(ac_flow));
    os.write(reinterpret_cast<char const*>(var_psd.data()), var_psd.size_bytes());
    os.write(reinterpret_cast<char const*>(ac_psd.data()), ac_psd.size_bytes());
    os.write(reinterpret_cast<char const*>(&var_cascade), sizeof(var_cascade));
    os.write(reinterpret_cast<char const*>(&ac_cascade), sizeof(ac_cascade));
    for (auto lvl = 0; lvl < levels(); ++lvl)
      write_compressed_block(fut_cascade_stats[lvl].get());
    distribution_correction::write_analysis(distcor_analysis, os);
  }
  else
  {
    os.write(reinterpret_cast<char const*>(field.data()), field.size_bytes());
    os.write(reinterpret_cast<char const*>(flow.data()), flow.size_bytes());
    os.write(reinterpret_cast<char const*>(psd.data()), psd.size_bytes());
    for (auto lvl = 0; lvl < levels(); ++lvl)
      os.write(reinterpret_cast<char const*>(cascade[lvl].data()), cascade[lvl].size_bytes());
    os.write(reinterpret_cast<char const*>(&var_flow), sizeof(var_flow));
    os.write(reinterpret_cast<char const*>(&ac_flow), sizeof(ac_flow));
    os.write(reinterpret_cast<char const*>(var_psd.data()), var_psd.size_bytes());
    os.write(reinterpret_cast<char const*>(ac_psd.data()), ac_psd.size_bytes());
    os.write(reinterpret_cast<char const*>(&var_cascade), sizeof(var_cascade));
    os.write(reinterpret_cast<char const*>(&ac_cascade), sizeof(ac_cascade));
    for (auto lvl = 0; lvl < levels(); ++lvl)
      os.write(reinterpret_cast<char const*>(cascade_stats[lvl].data()), cascade_stats[lvl].size_bytes());
    distribution_correction::write_analysis(distcor_analysis, os);
  }

  return os;
}

auto stream_state::read(std::istream& is) -> std::istream&
{
  // read and sanity check the header
  cache_file_header header;
  is.read(reinterpret_cast<char*>(&header), sizeof(header));
  if (header.magic != cache_file_magic)
    throw std::runtime_error{"Header magic token mismatch"};
  if (header.version != cache_file_version)
    throw std::runtime_error{"Cache file version mismatch"};
  if (   header.levels != levels()
      || header.grid_shape != grid_shape()
      || header.psd_shape != psd_shape())
    throw std::runtime_error{"Array size mismatch"};

  // TODO - check that engine configuration matches expected

  // read the state data
  if (header.compression != 0)
  {
    auto futures = vector<thread_pool_future<void>>();
    futures.reserve(3 + 2 * levels());

    auto queue_deflate = [&](auto& data)
    {
      int size;
      is.read(reinterpret_cast<char*>(&size), sizeof(size));
      auto buf = array1<char>{size};
      is.read(reinterpret_cast<char*>(buf.data()), buf.size_bytes());
      futures.push_back(thread_pool_queue_task([size, chunk = std::move(buf), &data]()
      {
        zlib_decompress(chunk, span{reinterpret_cast<char*>(data.data()), data.size_bytes()});
      }));
    };

    queue_deflate(field);
    queue_deflate(flow);
    queue_deflate(psd);
    for (auto lvl = 0; lvl < levels(); ++lvl)
      queue_deflate(cascade[lvl]);
    is.read(reinterpret_cast<char*>(&var_flow), sizeof(var_flow));
    is.read(reinterpret_cast<char*>(&ac_flow), sizeof(ac_flow));
    is.read(reinterpret_cast<char*>(var_psd.data()), var_psd.size_bytes());
    is.read(reinterpret_cast<char*>(ac_psd.data()), ac_psd.size_bytes());
    is.read(reinterpret_cast<char*>(&var_cascade), sizeof(var_cascade));
    is.read(reinterpret_cast<char*>(&ac_cascade), sizeof(ac_cascade));
    for (auto lvl = 0; lvl < levels(); ++lvl)
      queue_deflate(cascade_stats[lvl]);
    distcor_analysis = distribution_correction::read_analysis(is);

    /* we explicitly call get() even though the destructor of our thread_pool_future will ensure we wait for the tasks
     * to complete as we leave this scope.  this is to ensure that any exception thrown on the worker threads is
     * propagated to this thread (which only happens on get not wait). */
    for (auto& fut : futures)
      fut.get();
  }
  else
  {
    is.read(reinterpret_cast<char*>(field.data()), field.size_bytes());
    is.read(reinterpret_cast<char*>(flow.data()), flow.size_bytes());
    is.read(reinterpret_cast<char*>(psd.data()), psd.size_bytes());
    for (auto lvl = 0; lvl < levels(); ++lvl)
      is.read(reinterpret_cast<char*>(cascade[lvl].data()), cascade[lvl].size_bytes());
    is.read(reinterpret_cast<char*>(&var_flow), sizeof(var_flow));
    is.read(reinterpret_cast<char*>(&ac_flow), sizeof(ac_flow));
    is.read(reinterpret_cast<char*>(var_psd.data()), var_psd.size_bytes());
    is.read(reinterpret_cast<char*>(ac_psd.data()), ac_psd.size_bytes());
    is.read(reinterpret_cast<char*>(&var_cascade), sizeof(var_cascade));
    is.read(reinterpret_cast<char*>(&ac_cascade), sizeof(ac_cascade));
    for (auto lvl = 0; lvl < levels(); ++lvl)
      is.read(reinterpret_cast<char*>(cascade_stats[lvl].data()), cascade_stats[lvl].size_bytes());
    distcor_analysis = distribution_correction::read_analysis(is);
  }

  return is;
}

// LCOV_EXCL_START
TEST_CASE("disk_cache_io")
{
  auto levels = 6;
  auto shape = vec2i{512, 512};
  auto psd_shape = vec2i{512, 256};

  auto ref = stream_state{levels, shape, psd_shape};
  auto tst = stream_state{levels, shape, psd_shape};

  // initialize a state object and fill it with random data
  ref.field.fill(44.3);
  ref.flow.fill(vec2f{20.0f, -44.0f});
  ref.psd.fill(78.0);
  for (auto lvl = 0; lvl < levels; ++lvl)
    ref.cascade[lvl].fill(lvl * 12.0f);
  ref.var_flow = 0.15;
  ref.ac_flow = 0.3f;
  ref.var_psd.fill(0.35);
  ref.ac_psd.fill(0.4f);
  ref.var_cascade = 0.45;
  ref.ac_cascade = 0.46f;
  for (auto lvl = 0; lvl < levels; ++lvl)
    ref.cascade_stats[lvl].fill(cell_stats{0.5f * lvl, 0.6f * lvl, 0.7f * lvl});

  // serialize it to a stream, then unserialize it back to another object
  std::stringstream ss;
  ref.write(ss, 0, 0);
  tst.read(ss);

  // check that the content is identical
  CHECK_ALL(tst.field, ref.field, lhs == approx(rhs));
  CHECK_ALL(tst.flow, ref.flow, lhs.x == approx(rhs.x) && lhs.y == approx(rhs.y));
  CHECK_ALL(tst.psd, ref.psd, lhs == approx(rhs));
  for (auto lvl = 0; lvl < levels; ++lvl)
    CHECK_ALL(tst.cascade[lvl], ref.cascade[lvl], lhs == approx(rhs));
  CHECK(tst.var_flow == approx(ref.var_flow));
  CHECK(tst.ac_flow == approx(ref.ac_flow));
  CHECK_ALL(tst.var_psd, ref.var_psd, lhs == approx(rhs));
  CHECK_ALL(tst.ac_psd, ref.ac_psd, lhs == approx(rhs));
  CHECK(tst.var_cascade == approx(ref.var_cascade));
  CHECK(tst.ac_cascade == approx(ref.ac_cascade));
  for (auto lvl = 0; lvl < levels; ++lvl)
  {
    CHECK_ALL(tst.cascade_stats[lvl], ref.cascade_stats[lvl], lhs.mean == approx(rhs.mean));
    CHECK_ALL(tst.cascade_stats[lvl], ref.cascade_stats[lvl], lhs.stddev == approx(rhs.stddev));
    CHECK_ALL(tst.cascade_stats[lvl], ref.cascade_stats[lvl], lhs.autocor == approx(rhs.autocor));
  }
}
// LCOV_EXCL_STOP
