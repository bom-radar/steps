/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "core.h"
#include "format.h"
#include "fourier_transform.h"
#include "mosaic.h"
#include "plots.h"
#include "postprocessor.h"
#include "products.h"
#include "profile.h"
#include "thread_pool.h"
#include "trace.h"
#include "unit_test.h"
#include "verification.h"
#include "version.h"
#if STEPS_WITH_AWS
#include "aws_driver.h"
#endif
#include <getopt.h>
#include <unistd.h>
#include <iostream>
#include <thread>

using namespace steps;

static profile prof_main{"main"};

constexpr auto try_again = "Try --help for usage instructions\n";
constexpr auto usage_string =
R"(Short Term Ensemble Prediction System (STEPS)

Usage:
  steps [options] config_file [(key=value)...]
  steps --unit-test [(key=value)...]
  steps --verify-aggregate config_file
  steps --generate-plots output.html verif_1.nc[=name_1] [verif_2.nc[=name_2]...]

Available options:
  -h, --help
      Show this message and exit

  -w, --wisdom=path
      Set the path to the FFTW wisdom file which was generated using --exhaustive-plan

  --version
      Show version information and exit

  --unit-test
      Run unit tests and exit.  The unit test mode supports several extended configuration
      options that may be supplied to customize execution of the tests.  These should be
      provided as key=value pairs.

  --exhaustive-plan
      Enable exhaustive FFTW planning and write the wisdom file and exit.  Without a wisdom
      file STEPS will use the default flags to perform FFTW planning (FFTW_MEASURE).  This
      option allows the user to perform the most detailed level of planning (FFTW_EXHAUSTIVE)
      once and save the results to a file which will be used in subsequent executions.

  --preprocess=model
      Perform preprocessing of the listed model and exit.  This option causes STEPS to generate
      cache files for the given model as if a full forecast had been run.  The cache files can
      then be used for many forecast runs later on.

  --forecast
      Generate forecasts using the provided configuration.  This is the default mode, so there is
      no need to actually pass this option on the command line; it is included for completeness.

  --postprocess
      Generate products using a precalculated forecast grids that are loaded from an input source
      instead of generated on the fly.

  --mosaic
      Create mosaic products from tiles output by previously run forecasts.

  --verify
      Perform verification using the provided configuration.

  --verify-aggregate
      Aggregate multiple verification files together.  Paths to input verification files to be
      aggregated should be supplied using stdin, one file per line.

  --generate-plots
      Generate an HTML file which contains dynamic plots of one or more verification files.

config_file:
  Path to STEPS configuration file

key=value:
  Key path and value used to override an item in the configuration file.  Path components in the
  key are separated by a period, and an equals (=) symbol must separate the key from the value.
  No whitespace is allowed between the key and value.  If whitespace is needed in the value then
  ensure that the entire key=value pair is passed as a single command line argument (typically by
  surrounding it in quotes).  Examples:

    members=6 forecasts=18 time=1983-03-24T14:10:00Z
    models.access_r.reference_time=2019-01-01:00:00Z
    "outputs.ensemble.metadata.title=Forecast ensemble"
)";

constexpr auto short_options = "hw:";
constexpr struct option long_options[] =
{
    { "help",               no_argument,       0, 'h' }
  , { "wisdom",             required_argument, 0, 'w' }
  , { "version",            no_argument,       0, 'I' }
  , { "unit-test",          no_argument,       0, 'U' }
  , { "exhaustive-plan",    no_argument,       0, 'E' }
  , { "preprocess",         required_argument, 0, 'P' }
  , { "forecast",           no_argument,       0, 'F' }
  , { "postprocess",        no_argument,       0, 'R' }
  , { "mosaic",             no_argument,       0, 'M' }
  , { "verify",             no_argument,       0, 'V' }
  , { "verify-aggregate",   no_argument,       0, 'A' }
  , { "generate-plots",     no_argument,       0, 'G' }
#if STEPS_WITH_AWS
  , { "aws-driver-sqs",     no_argument,       0, 'D' } // hidden mode (not described in help)
  , { "aws-driver-batch",   no_argument,       0, 'B' } // hidden mode (not described in help)
#endif
  , { 0, 0, 0, 0 }
};

enum class main_action
{
    unit_test
  , plan
  , preprocess
  , forecast
  , postprocess
  , mosaic
  , verify
  , aggregate
  , plot
#if STEPS_WITH_AWS
  , aws_driver_sqs
  , aws_driver_batch
#endif
};

auto override_config(configuration& config, string_view str) -> void
{
  // find the end of the token
  auto i = str.find_first_of(".=");
  if (i == string_view::npos)
    throw std::runtime_error{"Invalid key/value pair"};

  if (str[i] == '.')
    override_config(config[str.substr(0, i)], str.substr(i + 1));
  else
    config[str.substr(0, i)] = configuration(string(str.substr(i + 1)));
}

/* this class causes progress bar plots to be shown in the terminal below the current trace output.  it works by
 * installing an alternative target to the trace system, and a progress callback to the model core.  whenever a
 * new trace line is output, or we are notified of updates progress the current bars are erased using ANSI escape
 * sequences and redrawn underneath any new trace lines. */
class progress_bars
{
public:
  progress_bars(core& c)
    : c{c}
    , bars_{c.members()}
  {
    bars_.fill(-1);

    // ensure any current output has been flushed before we make the change
    std::cout.flush();

    // this reserves placeholder lines that we can safely delete when outputting bars for first time
    for (auto m = 0; m < bars_.size(); ++m)
      std::cout.put('\n');

    // install an alternative trace target to keep our bars below trace output
    trace_set_target([&](string buf){ this->trace(std::move(buf)); }, [&](){ this->flush(); });

    // install the progress callback in the model core
    c.set_progress_callback([&](int m, int f){ this->update(m, f); });
  }

  progress_bars(progress_bars const& rhs) = delete;
  progress_bars(progress_bars&& rhs) noexcept;

  auto operator=(progress_bars const& rhs) -> progress_bars& = delete;
  auto operator=(progress_bars&& rhs) noexcept -> progress_bars&;

  ~progress_bars()
  {
    c.set_progress_callback(nullptr);
    trace_set_target(trace_target_stdout_write, trace_target_stdout_flush);
    clear();
  }

  auto clear() -> void
  {
    fmt::print("\033[{}A\033[J", bars_.size());
  }

  auto write() -> void
  {
    for (auto m = 0; m < bars_.size(); ++m)
      fmt::println(
            "Member {:02} [{:#>{}}{: >{}}]"
          , m + c.member_offset()
          , "", bars_[m] + 1
          , "", c.forecasts() + 1 - (bars_[m] + 1));
  }

  auto update(int member, int forecast) -> void
  {
    auto lock = std::lock_guard<std::mutex>{mut_};
    clear();
    bars_[member - c.member_offset()] = forecast;
    write();
  }

  auto trace(string buf) -> void
  {
    auto lock = std::lock_guard<std::mutex>{mut_};
    clear();
    std::cout.write(buf.data(), buf.size());
    write();
  }

  auto flush() -> void
  {
    auto lock = std::lock_guard<std::mutex>{mut_};
    std::cout.flush();
  }

private:
  core&       c;
  std::mutex  mut_;
  array1i     bars_;
};

// LCOV_EXCL_START
int main(int argc, char* argv[])
try
{
  // determine if we are running in a terminal
  auto in_tty = isatty(STDOUT_FILENO);

  // use colored trace output in terminals
  if (in_tty)
    trace_set_color_output(true);

  // store a copy of the command line for use in logs and/or output provenance attributes
  set_command_line_string(argc, argv);

  // process command line options
  auto action = main_action::forecast;
  auto wisdom_path = std::filesystem::path{"fftw_wisdom"};
  auto preprocess_model_name = string();
  while (true)
  {
    int option_index = 0;
    int c = getopt_long(argc, argv, short_options, long_options, &option_index);
    if (c == -1)
      break;
    switch (c)
    {
    case 'h':
      fmt::print(usage_string);
      return EXIT_SUCCESS;
    case 'w':
      wisdom_path = optarg;
      break;
    case 'I':
      fmt::print("STEPS {}\nSource: {}\n Build: {}\n", steps_version(), steps_source_ref(), steps_build_ref());
      return EXIT_SUCCESS;
    case 'U':
      action = main_action::unit_test;
      break;
    case 'E':
      action = main_action::plan;
      break;
    case 'P':
      action = main_action::preprocess;
      preprocess_model_name = optarg;
      break;
    case 'F':
      action = main_action::forecast;
      break;
    case 'R':
      action = main_action::postprocess;
      break;
    case 'M':
      action = main_action::mosaic;
      break;
    case 'V':
      action = main_action::verify;
      break;
    case 'A':
      action = main_action::aggregate;
      break;
    case 'G':
      action = main_action::plot;
      break;
#if STEPS_WITH_AWS
    case 'D':
      action = main_action::aws_driver_sqs;
      break;
    case 'B':
      action = main_action::aws_driver_batch;
      break;
#endif
    case '?':
      fmt::print(try_again);
      return EXIT_FAILURE;
    }
  }

  // unit test mode?
  if (action == main_action::unit_test)
  {
    auto config = configuration{};
    config["reference_dir"] = STEPS_REFERENCE_DATA_DIR "/ref";
    for (auto i = optind; i < argc; ++i)
      override_config(config, argv[i]);
    thread_pool_set_size(8);
    return run_unit_tests(config);
  }

  // plot generation mode?
  if (action == main_action::plot)
  {
    if (argc - optind < 1)
    {
      fmt::print("Output file not specified.\n{}", try_again);
      return EXIT_FAILURE;
    }
    auto files = vector<std::filesystem::path>{};
    auto names = vector<string>{};
    for (auto i = optind + 1; i < argc; ++i)
    {
      auto val = string(argv[i]);
      auto pos = val.find('=');
      if (pos != string::npos)
      {
        files.emplace_back(val.substr(0, pos));
        names.emplace_back(val.substr(pos + 1));
      }
      else
      {
        files.emplace_back(argv[i]);
        names.emplace_back("");
      }
    }
    return run_generate_plots(STEPS_REFERENCE_DATA_DIR, argv[optind], files, names);
  }

  // check for mandatory config file path argument
  if (argc - optind < 1)
  {
    fmt::print("Configuration file not specified.\n{}", try_again);
    return EXIT_FAILURE;
  }

  // load our configuration file and apply command line overrides
  auto config_path = string{argv[optind]};
  auto config = load_configuration(config_path, false);
  for (auto i = optind + 1; i < argc; ++i)
    override_config(config, argv[i]);

  // set the trace level
  trace_set_max_level(config.optional("trace_level", trace_level::log));

  // determine thread count
  auto threads = config.optional("threads", 0);
  if (threads < 1)
  {
    threads = std::thread::hardware_concurrency();
    if (threads < 1)
      throw std::runtime_error{"Unable to determine CPU core count, set 'threads' config item manually"};
  }

  // print our startup notice
  trace(trace_level::notice, "Welcome to STEPS");
  trace(trace_level::notice, "Version: {} ({})", steps_version(), steps_source_ref());
  trace(trace_level::notice, "  Build: {}", steps_build_ref());
  trace(trace_level::notice, " Config: {}", std::filesystem::absolute(config_path).string());
  trace(trace_level::notice, "Command: {}", get_command_line_string());
  trace(trace_level::notice, "   Exec: {} threads", threads);

  // make sure the user knows if memory checking is enabled
#if STEPS_ENABLE_ASAN
  trace(trace_level::warning, "AddressSanitizer is enabled!  Expect poor performance");
#endif

  // launch our thread pool
  thread_pool_set_size(threads - 1);

  // postprocess mode?
  if (action == main_action::postprocess)
  {
    auto post = postprocessor{config};
    post.generate_products(time_point{config["reference_time"]});
    return EXIT_SUCCESS;
  }

  // mosaic mode?
  if (action == main_action::mosaic)
  {
    auto mosaic = mosaic_generator{config};
    mosaic.generate_mosaics(time_point{config["reference_time"]});
    return EXIT_SUCCESS;
  }

  // verification mode?
  if (action == main_action::verify)
  {
    auto verif = verification{config};
    verif.verify_period(time_point{config["from"]}, time_point{config["till"]}, duration{config["period"]});
    return EXIT_SUCCESS;
  }

  // verification aggregation mode?
  if (action == main_action::aggregate)
  {
    auto line = string();
    auto paths = vector<string>();
    while (std::getline(std::cin, line))
      paths.push_back(std::move(line));
    auto verif = verification{config};
    verif.aggregate_results(paths);
    return EXIT_SUCCESS;
  }

  // steps on aws driver mode?
#if STEPS_WITH_AWS
  if (action == main_action::aws_driver_sqs)
  {
    auto driver = aws::driver_sqs{config};
    driver.run();
    return EXIT_SUCCESS;
  }
  if (action == main_action::aws_driver_batch)
  {
    auto driver = aws::driver_batch{config};
    driver.run(config);
    return EXIT_SUCCESS;
  }
#endif

  // setup FFTW wisdom
  if (action == main_action::plan)
    fourier_transform::set_plan_exhaustive(true);
  else if (std::filesystem::exists(wisdom_path))
    fourier_transform::load_wisdom(wisdom_path);
  else
    trace(trace_level::warning, "FFTW wisdom file {} not found", wisdom_path.string());

  // initialize and run the engine
  {
    auto ps = profile::scope{prof_main};

    auto steps = core{config};
    auto reference_time = time_point{config["reference_time"]};

    // enable progress bars when running in terminal
    auto pbs = optional<progress_bars>();
    if (in_tty)
      pbs.emplace(steps);

    // run the model
    if (action == main_action::forecast)
      steps.generate_forecasts(reference_time);
    else if (action == main_action::preprocess)
      steps.preprocess_model(preprocess_model_name, reference_time);
  }

  // save FFTW wisdom if desired
  if (action == main_action::plan)
    fourier_transform::save_wisdom(wisdom_path);

  // output profile report
  if constexpr (!opt_disable_profile)
    trace(trace_level::log, "Performance report\n{}", profile::generate_report(false));

  // this is critical to ensure that our reference netcdf files are released before the netcdf api is shutdown
  nc::release_reference_files();

  return EXIT_SUCCESS;
}
catch (std::exception& err)
{
  trace(trace_level::error, "Fatal exception:\n-> {}", err);

  // this is critical to ensure that our reference netcdf files are released before the netcdf api is shutdown
  nc::release_reference_files();

  return EXIT_FAILURE;
}

TEST_CASE("override_config")
{
  auto config = configuration::read_string("foo { bar { hello 3 } bye 5 } low high");

  CHECK_THROWS(override_config(config, "foo"));
  CHECK_THROWS(override_config(config, "foo.bar.hello"));

  override_config(config, "foo.bar.hello=6");
  override_config(config, "foo.bye=10");
  override_config(config, "low=med");

  CHECK(config["foo"]["bar"]["hello"].string() == "6");
  CHECK(config["foo"]["bye"].string() == "10");
  CHECK(config["low"].string() == "med");
}
// LCOV_EXCL_STOP
