/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "test_patterns.h"
#include <random>

using namespace steps;

auto steps::make_test_pattern(configuration const& config, transform const& xform, array2f& out) -> void
{
  auto& pattern = config["pattern"].string();
  if (pattern == "grid")
    test_pattern_grid(config, xform, out);
  else if (pattern == "random")
    test_pattern_random(config, xform, out);
  else if (pattern == "squares")
    test_pattern_squares(config, xform, out);
  else
    throw std::runtime_error{format("Unknown test pattern {}", pattern)};
}

auto steps::make_test_pattern(configuration const& config, transform const& xform, array2f2& out) -> void
{
  auto& pattern = config["pattern"].string();
  if (pattern == "uniform")
    test_pattern_uniform(config, xform, out);
  else if (pattern == "square")
    test_pattern_square(config, xform, out);
  else if (pattern == "rotation")
    test_pattern_rotation(config, xform, out);
  else
    throw std::runtime_error{format("Unknown test pattern {}", pattern)};
}

auto steps::test_pattern_grid(configuration const& config, transform const& xform, array2f& out) -> void
{
  auto value = xform.forward(float(config["value"]));
  auto spacing = int(config["spacing"]);
  auto thickness = int(config["thickness"]);
  for (auto y = 0; y < out.shape().y; ++y)
    for (auto x = 0; x < out.shape().x; ++x)
      out[y][x] = (y % spacing < thickness || x % spacing < thickness) ? value : 0.0f;
}

auto steps::test_pattern_random(configuration const& config, transform const& xform, array2f& out) -> void
{
  auto value = xform.forward(float(config["value"]));
  auto size = int(config["size"]);
  auto rnd = std::mt19937(config.optional("seed", 25031983));

  auto dist = std::uniform_real_distribution{0.0f, value};
  auto values = array2f{(out.shape().y / size) + 1, (out.shape().x / size) + 1};
  for (auto i = 0; i < values.shape().y; ++i)
    for (auto j = 0; j < values.shape().x; ++j)
      values[i][j] = dist(rnd);

  for (auto y = 0; y < out.shape().y; ++y)
    for (auto x = 0; x < out.shape().x; ++x)
      out[y][x] = values[y/size][x/size];
}

auto steps::test_pattern_squares(configuration const& config, transform const& xform, array2f& out) -> void
{
  auto value_mmh = float(config["value"]);
  auto value = xform.forward(value_mmh);
  auto size = int(config["size"]);
  auto count = int(config["count"]);
  auto offset_x = config.optional("offset_x", 0);
  auto offset_y = config.optional("offset_y", 0);

  // these settings let the user offset one square by a different amount
  auto rebel_i = config.optional("rebel_i", -1);
  auto rebel_j = config.optional("rebel_j", -1);
  auto rebel_offset_x = config.optional("rebel_offset_x", 0);
  auto rebel_offset_y = config.optional("rebel_offset_y", 0);
  auto rebel_value = xform.forward(config.optional("rebel_value", value_mmh));

  out.fill(0.0f);
  for (auto i = 0; i < count; ++i)
  {
    for (auto j = 0; j < count; ++j)
    {
      auto vv = i == rebel_i && j == rebel_j ? rebel_value : value;
      auto yo = i == rebel_i && j == rebel_j ? rebel_offset_y : offset_y;
      auto xo = i == rebel_i && j == rebel_j ? rebel_offset_x : offset_x;
      auto ys = std::max(0, int((i + 0.5f) * (out.shape().y / float(count)) - (size * 0.5f) + yo));
      auto xs = std::max(0, int((j + 0.5f) * (out.shape().x / float(count)) - (size * 0.5f) + xo));
      auto ye = std::min(out.shape().y, ys + size);
      auto xe = std::min(out.shape().x, xs + size);
      for (auto y = std::max(0, ys); y < ye; ++y)
        for (auto x = std::max(0, xs); x < xe; ++x)
          out[y][x] = vv;
    }
  }
}

auto steps::test_pattern_uniform(configuration const& config, transform const& xform, array2f2& out) -> void
{
  auto magnitude = float(config["magnitude"]);
  auto angle = -float(config["angle"]);

  auto angle_rad = degrees_to_radians(angle);
  auto value = vec2f{magnitude * std::sin(angle_rad), magnitude * std::cos(angle_rad)};
  out.fill(value);
}

auto steps::test_pattern_square(configuration const& config, transform const& xform, array2f2& out) -> void
{
  test_pattern_uniform(config, xform, out);

  auto size = int(config["size"]);
  auto magnitude2 = float(config["magnitude2"]);
  auto angle2 = -float(config["angle2"]);

  auto angle_rad2 = degrees_to_radians(angle2);
  auto value2 = vec2f{magnitude2 * std::sin(angle_rad2), magnitude2 * std::cos(angle_rad2)};
  for (auto y = (out.shape().y - size) / 2; y < (out.shape().y + size) / 2; ++y)
    for (auto x = (out.shape().x - size) / 2; x < (out.shape().x + size) / 2; ++x)
      out[y][x] = value2;
}

auto steps::test_pattern_rotation(configuration const& config, transform const& xform, array2f2& out) -> void
{
  auto magnitude = float(config["magnitude"]);
  auto angle = -float(config["angle"]);

  auto angle_rad = degrees_to_radians(angle);

  // reference circumference at grid edge where magnitude should equal user setting
  auto circ_ref = pi<float> * out.shape().x;

  for (auto y = 0; y < out.shape().y; ++y)
  {
    for (auto x = 0; x < out.shape().x; ++x)
    {
      // shift x,y so that we get an offset from center of rotation
      auto xy = vec2f{x - out.shape().x * 0.5f, y - out.shape().y * 0.5f};

      // determine circumference of rotation at this offset
      auto circ = 2.0f * pi<float> * xy.magnitude();

      // scale so that speed at reference circumference equals user setting
      auto mag = magnitude * circ / circ_ref;

      // determine the angle to this point as right angle to angle of vector itself
      // we also let the user specify an additional angular offset to form a spiral
      auto ang = float(xy.angle() + angle_rad + pi<float> * 0.5f);

      // set the flow vector
      out[y][x] = vec2f{mag * std::sin(ang), mag * std::cos(ang)};
    }
  }
}
