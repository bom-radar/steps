/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "array2.h"
#include "unit_test.h"

using namespace steps;

// LCOV_EXCL_START
TEST_CASE("array2::array2()")
{
  array2i v;
  CHECK(v.size() == 0);
  CHECK(v.shape().x == 0);
  CHECK(v.shape().y == 0);
  CHECK(v.data() == nullptr);
}

TEST_CASE("array2::array2(vec2i)")
{
  vec2i shape{5, 10};
  array2f v{shape};
  CHECK(v.shape().x == shape.x);
  CHECK(v.shape().y == shape.y);
  CHECK(v.size() == shape.x * shape.y);
  CHECK(v.data() != nullptr);
}

TEST_CASE("array2::array2(move)")
{
  vec2i shape{5, 10};
  array2i a{shape};
  a.fill(1);
  auto ptr = a.data();

  array2i b{std::move(a)};
  REQUIRE(a.data() == nullptr);
  REQUIRE(b.data() == ptr);
  CHECK(b.shape() == shape);
}

TEST_CASE("array2::operator=(copy)")
{
  vec2i shape{5, 10};
  array2i a{shape};
  a.fill(1);

  array2i b;
  SUBCASE("same") { b = array2i{shape}; }
  SUBCASE("diff") { b = array2i{2, 2}; }
  b.fill(2);

  b = a;
  CHECK(b.shape() == shape);
  CHECK_ALL(a, b, lhs == rhs);
}

TEST_CASE("array2::operator=(move)")
{
  vec2i shape{5, 10};
  array2i a{shape}, b{shape};

  a.fill(1);
  b.fill(2);

  auto bdata = b.data();
  CHECK(a.data()[0] == 1);

  a = std::move(b);

  REQUIRE(a.data() == bdata);
  REQUIRE(b.data() == nullptr);
  CHECK(a.data()[0] == 2);
}

TEST_CASE("std::swap(array2)")
{
  vec2i shape_a{5, 10}, shape_b{20, 30};
  array2i a{shape_a}, b{shape_b};
  auto ptr_a = a.data();
  auto ptr_b = b.data();
  std::swap(a, b);
  CHECK(a.shape() == shape_b);
  CHECK(a.data() == ptr_b);
  CHECK(b.shape() == shape_a);
  CHECK(b.data() == ptr_a);
}

TEST_CASE("array2::resize")
{
  array2i a{5, 10};
  auto ptr = a.data();

  // null change
  a.resize(5, 10);
  CHECK(a.shape() == vec2i{10, 5});
  CHECK(a.size() == 5 * 10);
  CHECK(a.data() == ptr);

  // rows, cols args
  a.resize(16, 8);
  CHECK(a.shape() == vec2i{8, 16});
  CHECK(a.size() == 16 * 8);

  // vec2 args
  a.resize(vec2i{2, 3});
  CHECK(a.shape() == vec2i{2, 3});
  CHECK(a.size() == 6);
}

TEST_CASE("array2::operator[]")
{
  array2i v{vec2i{100, 5}};
  CHECK(v[0] == v.data());
  CHECK(v[1] == &v.data()[100]);
  CHECK(v[2] == &v.data()[200]);

  auto const& vv = v;
  CHECK(vv[0] == v.data());
  CHECK(vv[1] == &v.data()[100]);
  CHECK(vv[2] == &v.data()[200]);
}

TEST_CASE("array2::<in place operation>")
{
  array2i v{vec2i{2, 2}};
  for (int i = 0; i < v.size(); ++i)
    v.data()[i] = i;

  array2i v2{vec2i{2, 2}};
  for (int i = 0; i < v2.size(); ++i)
    v2.data()[i] = i * 2;

  SUBCASE("fill")
  {
    v.fill(7.0);
    CHECK(v.data()[0] == 7);
    CHECK(v.data()[1] == 7);
    CHECK(v.data()[2] == 7);
    CHECK(v.data()[3] == 7);
  }

  SUBCASE("copy")
  {
    v.copy(v2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 2);
    CHECK(v.data()[2] == 4);
    CHECK(v.data()[3] == 6);
  }

  SUBCASE("add")
  {
    v.add(v2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 3);
    CHECK(v.data()[2] == 6);
    CHECK(v.data()[3] == 9);
  }

  SUBCASE("add")
  {
    v.add(1);
    CHECK(v.data()[0] == 1);
    CHECK(v.data()[1] == 2);
    CHECK(v.data()[2] == 3);
    CHECK(v.data()[3] == 4);
  }

  SUBCASE("copy_scaled")
  {
    v.copy_scaled(v2, 2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 4);
    CHECK(v.data()[2] == 8);
    CHECK(v.data()[3] == 12);
  }

  SUBCASE("add_scaled")
  {
    v.add_scaled(v2, 2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 5);
    CHECK(v.data()[2] == 10);
    CHECK(v.data()[3] == 15);
  }

  SUBCASE("subtract")
  {
    v2.fill(1);
    v.subtract(v2);
    CHECK(v.data()[0] == -1);
    CHECK(v.data()[1] == 0);
    CHECK(v.data()[2] == 1);
    CHECK(v.data()[3] == 2);
  }

  SUBCASE("multiply")
  {
    v.multiply(4);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 4);
    CHECK(v.data()[2] == 8);
    CHECK(v.data()[3] == 12);
  }

  SUBCASE("multiply")
  {
    v.multiply(v2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 2);
    CHECK(v.data()[2] == 8);
    CHECK(v.data()[3] == 18);
  }

  SUBCASE("divide")
  {
    v.divide(2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 0);
    CHECK(v.data()[2] == 1);
    CHECK(v.data()[3] == 1);
  }

  SUBCASE("divide")
  {
    v2.fill(2);
    v.divide(v2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 0);
    CHECK(v.data()[2] == 1);
    CHECK(v.data()[3] == 1);
  }

  SUBCASE("replace")
  {
    v.replace(1, 2);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 2);
    CHECK(v.data()[2] == 2);
    CHECK(v.data()[3] == 3);
  }

  SUBCASE("threshold_min")
  {
    v.threshold_min(2, 0);
    CHECK(v.data()[0] == 0);
    CHECK(v.data()[1] == 0);
    CHECK(v.data()[2] == 2);
    CHECK(v.data()[3] == 3);
  }

  SUBCASE("add_multiply")
  {
    v.add_multiply(2, 2);
    CHECK(v.data()[0] == 4);
    CHECK(v.data()[1] == 6);
    CHECK(v.data()[2] == 8);
    CHECK(v.data()[3] == 10);
  }

  SUBCASE("multiply_add")
  {
    v.multiply_add(2, 1);
    CHECK(v.data()[0] == 1);
    CHECK(v.data()[1] == 3);
    CHECK(v.data()[2] == 5);
    CHECK(v.data()[3] == 7);
  }

  SUBCASE("replace_or_multiply_add")
  {
    v.replace_or_multiply_add(2, 4, 2, 1);
    CHECK(v.data()[0] == 1);
    CHECK(v.data()[1] == 3);
    CHECK(v.data()[2] == 4);
    CHECK(v.data()[3] == 7);
  }
}

TEST_CASE("array2<float>")
{
  array2f v{vec2i{2, 2}};
  for (int i = 0; i < v.size(); ++i)
    v.data()[i] = i;

  SUBCASE("replace_nan")
  {
    v.data()[2] = std::numeric_limits<float>::quiet_NaN();
    v.replace_nan(7);
    CHECK(v.data()[0] == approx(0));
    CHECK(v.data()[1] == approx(1));
    CHECK(v.data()[2] == approx(7));
    CHECK(v.data()[3] == approx(3));
  }
}

TEST_CASE("array2::<stats>")
{
  array2f v{vec2i{2, 2}};
  for (int i = 0; i < v.size(); ++i)
    v.data()[i] = i;

  CHECK(v.mean() == approx(1.5));
  CHECK(v.mean_stddev().first == approx(1.5));
  CHECK(v.mean_stddev().second == approx(1.118033));

  auto cond = [](auto i) { return i != 0; };
  CHECK(v.conditional_mean(cond) == approx(2.0));
  CHECK(v.conditional_mean_stddev(cond).first == approx(2.0));
  CHECK(v.conditional_mean_stddev(cond).second == approx(0.816497));

  CHECK(v.count_greater_equal(0.5f) == 3);

  v.normalize(10.0, 5.0);
  CHECK(v.data()[0] == approx(-2.0));
  CHECK(v.data()[1] == approx(-1.8));
  CHECK(v.data()[2] == approx(-1.6));
  CHECK(v.data()[3] == approx(-1.4));

  v.unnormalize(10.0, 5.0);
  for (int i = 0; i < v.size(); ++i)
    CHECK(v.data()[i] == approx(i));
}
// LCOV_EXCL_STOP
