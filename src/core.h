/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "autocorrelations.h"
#include "configuration.h"
#include "diagnostics.h"
#include "distribution_correction.h"
#include "filters.h"
#include "fourier_transform.h"
#include "io.h"
#include "metadata.h"
#include "products.h"
#include "stream.h"
#include "transform.h"
#include "vec2.h"
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <set>

namespace steps
{
  /// Modes for output of a null product set when there is insufficient rainfall for a reliable forecast
  enum class null_forecast_mode
  {
      none    ///< Do not output null products
    , missing ///< Output null forecasts as missing data
    , zero    ///< Output null forecasts as zeros
  };
  STEPS_ENUM_TRAITS(null_forecast_mode, none, missing, zero);

  /// Metadata returned to the user describing a forecast run
  struct forecast_info
  {
    bool  null_forecast;  // whether forecast generation was short-circuited as a null forecast
  };

  /// Read and sanity check custom FFT size from config, or return sensible default if not set
  auto determine_fft_shape(configuration const& config, vec2i grid_shape, int anti_ring_border) -> vec2i;

  /// Core run configuration
  /** The core may launch several threads, each of which is responsible for the generation of a single member at
   *  a time.  The threads will share resources (e.g. static cascades) wherever possible.  This arrangement
   *  allows the processing of members to get slightly out of sync so that they naturally form a pipeline.  While
   *  one member is blending another member can be performing I/O.  Also it allows us to exploit parallelism
   *  more effectively when the number of CPUs exceeds the number of levels in the cascade. */
  class core
  {
  public:
    /// Instantiate the STEPS model core
    core(configuration const& config);

    /* Make the core class uncopyable & unmoveable.  Necessary because a pointer back to the core is stored in child
     * objects such as streams. */
    core(core const&) = delete;
    core(core&&) noexcept = delete;
    auto operator=(core const&) -> core& = delete;
    auto operator=(core&&) noexcept -> core& = delete;

    /// Install a forecast progress callback
    /** This function sets an optional callback that is fired when STEPS finishes processing for each forecast time
     *  of each member.  The function is passed the member and forecast number being processed. This allows driver
     *  code to implement features such as progress trackers and efficient pipelining of sequential forecast cycles.
     *
     *  Note that forecast 0 is the model reference time and its receipt indicates that the initialization phase of a
     *  member has completed.  Forecast 1 is therefore the first true forecast, and forecast 'c.forecasts()' is the
     *  final forecast for a member.  Once all members have reported completion of their final forecast there may
     *  still be some extra delay cause by finalization of products.
     *
     *  For example, any stream cache generated from the observation at forecast reference time is garanteed to have
     *  been updated once this callback has fired for forecast 0 on all members.  This could be used by a driver to
     *  indicate that it is now "safe" to launch the next forecast cycle in parallel.*/
    auto set_progress_callback(progress_callback_fn cb_progress)  { cb_progress_ = std::move(cb_progress); }

    /// User defined parameters
    auto& user_parameters() const   { return user_parameters_; }

    /// Number of members to generate
    auto members() const            { return members_; }
    /// Index of first member to generate
    auto member_offset() const      { return member_offset_; }
    /// Index of the control member
    auto control_member() const     { return control_member_; }

    /// Number of forecasts to generate
    auto forecasts() const          { return forecasts_; }
    /// Time between forecasts
    auto time_step() const          { return time_step_; }

    /// Number of levels in the cascades
    auto levels() const             { return levels_; }
    /// Shape of forecast domain
    auto grid_shape() const         { return grid_shape_; }
    /// Width and height of grid cell in kilometers
    auto grid_resolution() const    { return grid_resolution_; }

    /// Minimum detectable rain rate used to distinguish between raining and non-raining cells
    auto minimum_rain_rate() const  { return minimum_rain_rate_; }

    /// Minimum raining area (in observation stream) to generate forecast
    auto minimum_rain_area() const  { return minimum_rain_area_; }

    /// Radius of region used to calculate spatially varying parameter values
    /** For a radius of R a 2*R+1 by 2*R+1 box around each parameter sample will be used to calculate the local value. */
    auto parameter_radius() const   { return parameter_radius_; }

    /// Maximum cascade level to use for spatial skill determination
    /** When measuring cascade skill for model inputs, the skill is based on the correlation/covariance of the model
     *  and observation cascades which have had the top this many levels recomposed.  For example, a value of 3
     *  means skills will be determined by summing levels 0..2 of each cascade and then comparing the results. */
    auto spatial_skill_levels() const { return spatial_skill_levels_; }

    /// Scaling factor to apply to diagnosed observation skills
    auto obs_skill_scale() const    { return obs_skill_scale_; }

    /// Standard devation of log normal distribution used to perturb flow speeds
    auto perturb_flow() const       { return perturb_flow_; }

    /// Whether to advect the flow field by itself at each time step
    auto advect_flow() const        { return advect_flow_; }

    /// Time until model values account for 50% of cascade in high autocorrelation cascade levels
    /** The main method of incorporating model contributions into the nowcast is by blending the local process
     *  parameters for the AR(1) model.  This works very well in cascade levels / areas where there is a low
     *  autocorrelation because the nowcast evolves quickly and can respond to changes in NWP parameters rapidly.
     *  In levels / areas with a very high autocorrelation however, the nowcast is forced to evolve very slowly
     *  and therefore cannot incorporate new NWP information.
     *
     *  The impact of this is seen when when the nowcast has rain in an area, but the model inputs do not.  The rain
     *  will turn into a smooth blob and become frozen because the small scale details evolve away quickly but the
     *  large scale feature cannot respond quickly.
     *
     *  To avoid this problem we also allow allow the cascade values to be directly blended, but to a degree which
     *  is in proportion to the autocorrelation.  For low autocorrelation areas (i.e. small scale features) the
     *  blend is virtually nil.  For very high autocorrelation areas (i.e. large scale features) a small amount
     *  of the model will be directly blended in each time step.  This helps areas where there is a strong
     *  disagreement between the nowcast and model(s) to transition cleanly over to the model values once the
     *  model skill exceeds the nowcast skill.
     *
     *  This parameter controls the strength of blending per time step.  It is expressed as the length of time
     *  needed for the directly blended contribution of the model(s) to account for 50% of the output value, for
     *  a cascade level that has an autocorrelation of 1.0 (i.e. not evolving).  In reality even the top cascade
     *  level will have an autocorrelation below 1.0, so this parameter represents an lower bound of the blend
     *  time. */
    auto blend_halflife() const     { return blend_halflife_; }

    /// Autocorrelation at which model values will be blended at half the maximum blend rate
    /** This parameter sets the relationship between the local autocorrelation at some level / area in the cascade
     *  and the relative strength of the blend.  When the autocorrelation is 1 (i.e. static value), then the model
     *  value(s) will be blended at the maximum strength as determined by blend_halflife.  The strength of the
     *  blend decreases exponentially as the autocorrelation drops from 1 to 0.  This parameter is used to set the
     *  exponent of this relationship such that values with an autocorrelation matching this parameter will be
     *  blended at 50% of the maximum strength.
     *
     *  Since autocorrelation increases with scale, you can (roughly) think of this parameter setting the relative
     *  blend strength between cascade levels.  A high value will cause only large scale (slowly evolving) levels
     *  to be directly blended with the model, while forcing small (fast evolving) levels to rely in the AR(1)
     *  parameter blending to incorporate model characteristics. */
    auto blend_ac_halfrate() const  { return blend_ac_halfrate_; }

    /// Maximum amount of absolute model blending per time step
    auto blend_limit() const        { return blend_limit_; }

    /// Power to raise autocorrelation to when determining absolute model blending rate
    auto blend_ac_exponent() const  { return blend_ac_exponent_; }

    /// Whether to threshold output values below the minimum rain rate to 0.0
    auto threshold_output() const   { return threshold_output_; }

    /// Whether to mask output areas advected in from outside the domain
    auto mask_advected_in() const   { return mask_advected_in_; }

    /// User defined static mask to apply to output products
    auto& mask_static() const       { return mask_static_; }

    /// Whether to advect the static mask along with the forecast
    auto mask_static_advect() const { return mask_static_advect_; }

    /// Random seed (0 for automatic)
    auto random_seed() const        { return random_seed_; }

    /// Whether to disable advection of the nowcast (used for tuning and testing)
    auto disable_advection() const  { return disable_advection_; }

    /// Whether to disable evolution of the nowcast (used for tuning and testing)
    auto disable_evolution() const  { return disable_evolution_; }

    /// Test pattern configuration (optional)
    auto& test_pattern() const      { return test_pattern_; }

    /// Optical flow manager
    auto& tracker() const           { return *tracker_; }

    /// Dataspace transformation manager
    auto& transformer() const       { return *transform_; }

    /// Value of 0 mm/hr transformed into log space
    auto log_zero() const           { return log_zero_; }

    /// Size anti-ringing border applied during decomposition by reflecting and fading into padding
    auto anti_ring_border() const   { return anti_ring_border_; }

    /// Fourier transform for cascade decomposition
    auto& fft() const               { return fft_; }

    /// Bandpass filters for cascade decomposition
    auto& bandpass_filters() const  { return filters_; }

    /// Default autocorrelation as a function of scale relationship if diagnosed values are unavailable or unreliable
    auto default_autocorrelations() const { return default_acs_; }

    /// PSD to use for noise generation at scales where diagnosed PSD is unavailable or unreliable
    auto& default_psd() const       { return default_psd_; }

    /// Number of levels for which a diagnosed PSD is permitted to be used
    auto diagnosed_psd_levels() const { return diagnosed_levels_; }

    /// Output distribution correction manager
    auto& distribution_correction() const { return *distcor_; }

    /// Model inputs
    auto& models()                  { return models_; }

    /// Model count
    /** This function exists only to ensure we can get the model count as an int rather than depending on the size_type
     *  of the models_ container (which is size_t).  It's a convenience to avoid either lots of casts from models().size()
     *  to int, or lots of warnings about narrowing conversions and/or sign mismatched comparisons. */
    auto model_count() const        { return int(models_.size()); }

    /// Observation input
    auto& observation()             { return observation_; }

    /// Root of products tree
    auto& products()                { return products_; }

    /// Preprocess a single model input
    auto preprocess_model(string_view model_name, time_point reference_time) -> void;

    /// Run the engine
    auto generate_forecasts(time_point reference_time) -> forecast_info;

  private:
    auto handle_null_forecast(time_point reference_time) -> bool;

    auto load_skills() -> void;
    auto store_skills() -> void;

    auto initialize_diagnostics(time_point reference_time) -> void;
    auto shutdown_diagnostics() -> void;

    auto thread_preprocess(int engine, int model, time_point reference_time) -> void;
    auto thread_forecast(int engine, time_point reference_time) -> void;

  public:
    auto engine_commence_time_step(int engine, time_point reference_time, bool reset) -> bool;
    auto engine_notify_progress(int member, int forecast) -> void;

  private:
    metadata                  user_parameters_;   // User defined parameters passed to format strings in config file
    int                       members_;           // Number of members
    int                       member_offset_;     // Index of first member
    int                       control_member_;    // Index of control member
    int                       forecasts_;         // Number of forecasts
    duration                  time_step_;         // Time between forecasts
    int                       levels_;            // Number of levels in the cascade
    vec2i                     grid_shape_;        // Shape of forecast domain
    float                     grid_resolution_;   // Width of a grid cell in meters
    float                     minimum_rain_rate_; // Minimum detectable rain rate
    float                     minimum_rain_area_; // Suppress forecasts if raining area of observation is below this value
    null_forecast_mode        null_forecast_mode_;// Action to take when forecast is suppressed due to low of rain area
    float                     parameter_radius_;  // Radius of spatially varying parameter local domains in units of wavelength
    int                       spatial_skill_levels_;  // Number of cascade levels to use in determining spatial skills
    float                     obs_skill_scale_;   // Manual scale factor for diagnosed observation skills
    float                     perturb_flow_;      // Stddev of flow perturbation
    bool                      advect_flow_;       // Whether to advect the flow field each time step
    duration                  blend_halflife_;    // Time until model blend accounts for 50% of cascade values (see above)
    float                     blend_ac_halfrate_; // Autocorrelation at which absolute model blending will be at half strength
    float                     blend_limit_;       // Maximum amount of absolute model blending per time step
    float                     blend_ac_exponent_; // Autocorrelation exponent for absolute model blending
    bool                      threshold_output_;  // Whether to force output values below the min rain rate to zero
    bool                      mask_advected_in_;  // Whether to mask output areas advected in from outside the domain
    optional<array2i>         mask_static_;       // Static mask for output products
    bool                      mask_static_advect_;// Whether to advect the static mask along with the forecast
    int                       random_seed_;       // Random seed (0 = true random)
    bool                      disable_advection_; // Whether to disable advection of the nowcast
    bool                      disable_evolution_; // Whether to disable evolution of the nowcast
    configuration             test_pattern_;      // Test pattern configuration (node_type of null type if disabled)
    string                    skills_path_;       // Path or URI for skills file
    string                    diagnostics_path_;  // Path format for diagnostics file

    int                       engines_;           // Number of forecast engines to use
    int                       max_engine_spread_; // Maximum number of time steps ahead to allow any one engines

    progress_callback_fn      cb_progress_;       // Callback to report forecast progress to driver code

    std::mutex                mut_engines_;       // Mutex protecting engine management state
    std::exception_ptr        fatal_error_;       // First fatal exception thrown by an engine thread
    int                       next_member_;       // Next member to generate
    std::condition_variable   cv_spread_;         // Condition variable used to enforce max engine spread
    vector<int>               step_index_;        // Index of time step being forecast by each engine
    int                       step_index_min_;
    int                       step_index_max_;

    int                       anti_ring_border_;  // Length of tukey window reflect & fade into FFT padding in grid cells
    fourier_transform         fft_;               // Fourier transform manager
    unique_ptr<transform>     transform_;         // Dataspace transform
    float                     log_zero_;          // Transformed value of 0 mm/hr in log space
    unique_ptr<optical_flow>  tracker_;           // Optical flow manager
    filter_bank               filters_;           // Bandpass filters for cascade decomposition
    array1f                   default_acs_;       // Default autocorrelations for each cascade level
    filter                    default_psd_;       // Fallback PSD for noise when diagnosed values are unreliable
    int                       diagnosed_levels_;  // Maximum number of levels that a diagnosed PSD can be used for
    optional<stream>          observation_;       // Observation stream used at startup for conditional simulation
    vector<stream>            models_;            // Model streams used for blending

    unique_ptr<steps::distribution_correction> distcor_; // Distribution correction manager
    products::rain_rate       products_;          // Root of output products tree
  };
}
