/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array2.h"
#include "util.h"
#include "vec2.h"

namespace steps
{
  // we should try this with a u8 type to check memory vs performance tradeoff, my default assumption is that i32 will
  // be better for performance and easier to vectorize
  using mask_val = int;

  /// Determine the intersection of two masks
  auto intersection(span<mask_val const> l, span<mask_val const> r, span<mask_val> out) -> void;

  // Determine the variance of an array
  auto variance(span<float const> a) -> double;
  auto variance(span<vec2f const> a) -> double;
  auto variance_ignore_nans(span<float const> a) -> double;
  auto variance_masked(span<mask_val const> mask, span<float const> a) -> double;
  auto variance_masked(span<mask_val const> mask, span<vec2f const> a) -> double;
  auto variance_weighted(span<float const> weight, span<float const> a) -> double;

  // Determine covariance between two arrays
  auto covariance(span<float const> a, span<float const> b) -> double;
  auto covariance(span<vec2f const> a, span<vec2f const> b) -> vec2d;
  auto covariance_ignore_nans(span<float const> a, span<float const> b) -> double;
  auto covariance_masked(span<mask_val const> mask, span<float const> a, span<float const> b) -> double;
  auto covariance_masked(span<mask_val const> mask, span<vec2f const> a, span<vec2f const> b) -> vec2d;
  auto covariance_weighted(span<float const> weight, span<float const> a, span<float const> b) -> double;

  /// Determine correlation between two arrays
  auto correlation(span<float const> a, span<float const> b) -> double;
  auto correlation(span<vec2f const> a, span<vec2f const> b) -> vec2d;
  auto correlation_ignore_nans(span<float const> a, span<float const> b) -> double;
  auto correlation_masked(span<mask_val const> mask, span<float const> a, span<float const> b) -> double;
  auto correlation_masked(span<mask_val const> mask, span<vec2f const> a, span<vec2f const> b) -> vec2d;
  auto correlation_weighted(span<float const> weight, span<float const> a, span<float const> b) -> double;

  /// Calculate correlation of two variables based on their already known covariance and individual variances
  inline auto covariance_to_correlation(double covariance, double variance_a, double variance_b) -> double
  {
    return (variance_a * variance_b) > 0.0 ? covariance / std::sqrt(variance_a * variance_b) : 0.0;
  }
  inline auto covariance_to_correlation(vec2d covariance, double variance_a, double variance_b) -> vec2d
  {
    return (variance_a * variance_b) > 0.0 ? covariance / std::sqrt(variance_a * variance_b) : vec2d{0.0, 0.0};
  }

  /// Calculate a covariance matrix
  auto covariance_matrix(span<span<float const> const> inputs) -> array2d;
  auto covariance_matrix(span<span<vec2f const> const> inputs) -> array2d;
  auto covariance_matrix_masked(span<mask_val const> mask, span<span<float const> const> inputs) -> array2d;
  auto covariance_matrix_masked(span<mask_val const> mask, span<span<vec2f const> const> inputs) -> array2d;
  auto covariance_matrix_weighted(span<float const> weight, span<span<float const> const> inputs) -> array2d;

  /// Check that a covariance matrix is valid
  auto assert_covariance_matrix(array2d const& matrix) -> void;

  /// Convert a covariance matrix to a correlation matrix
  auto covariance_matrix_to_correlation_matrix(array2d& matrix) -> void;

  /// Determine local standard deviations of an array with a known mean based on a given radius
  auto local_standard_deviation(array2f const& data, float mean, int radius, array2f& out) -> void;

  // based on https://en.wikipedia.org/wiki/Nelder%E2%80%93Mead_method
  template <typename ErrorFunc>
  auto nelder_mead(float xa, float xb, float tol_error, ErrorFunc get_error)
  {
    // use Nelder-Mead on a line to minimise the error function
    auto tol_scale = 0.00001f; // terminate the search when the two points have converged
    auto alpha = 1.0f;
    auto gamma = 2.0f;
    auto rho = 0.5f;
    int max_iterations = 100;

    auto error_a = get_error(xa);
    auto error_b = get_error(xb);

    for (auto i = 0; i < max_iterations; ++i)
    {
      //trace(trace_level::verbose, "xa {} err {}, xb {} err {}", xa, error_a, xb, error_b);

      if (std::abs(xa - xb) < tol_scale)
        return error_a < error_b ? xa : xb;

      // order the two points
      if (error_b < error_a)
      {
        std::swap(xa, xb);
        std::swap(error_a, error_b);
      }

      if (error_a < tol_error)
        return xa;

      // reflection
      auto xr = xa + alpha * (xa - xb);
      auto error_r = get_error(xr);
      if (error_r > error_a && error_r < error_b)
      {
        xb = xr;
        error_b = error_r;
      }
      // expansion
      else if (error_r < error_a)
      {
        auto xe = xa + gamma*(xr-xa);
        auto error_e = get_error(xe);
        if ( error_e < error_r)
        {
          xb = xe;
          error_b = error_e;
        }
        else
        {
          xb = xr;
          error_b = error_r;
        }
      }
      // contraction
      else
      {
        auto xc = xa + rho * (xb - xa);
        auto error_c = get_error(xc);
        xb = xc;
        error_b = error_c;
      }
    }

    throw std::runtime_error{"Optimization with nelder_mead failed to converge"};
  }
}
