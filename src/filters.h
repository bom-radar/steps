/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "fourier_transform.h"

namespace steps
{
  using filter = array2<fourier_transform::real>;

  /// Make a parametric PSD based on two exponents and a scale break
  /** The beta terms define how quickly the power falls off as a function of wavelength (inversely).  A beta of 2
   *  will produce inverse square falloff which is probably a reasonable starting place.  Setting to a beta of 1
   *  would produce linear falloff (as a function of wave _length_ not wave _number_!). */
  auto make_parametric_psd(
        fourier_transform const& fft  ///< Fourier transform filter will be used with
      , float grid_resolution         ///< Side length of each grid cell
      , float scale_break             ///< Wavelength of crossover between two power regimes
      , float beta_1                  ///< Scaling exponent for low frequency regime
      , float beta_2                  ///< Scaling exponent for high frequency regime
      ) -> filter;

  /// Make a low-pass filter
  auto make_lowpass_filter(
        fourier_transform const& fft  ///< Fourier transform filter will be used with
      , float grid_resolution         ///< Side length of each grid cell
      , float rolloff                 ///< Width of each crossover in octaves
      , float crossover               ///< Wavelength of crossover point
      ) -> filter;

  /// Make a band-pass filter
  /** The rolloff parameter defines the width of the crossover zone at either side of the filter in octaves.  This
   *  means that a value of 1 will cause the fade between bands to occur over a magnitude of 2^1 (so it might start
   *  at 100km and end at 50km).  A value of 2 fades over two octaves (2^2 - so 100km to 25km) etc.  The crossover
   *  wavelengths are given in the same units as the grid resolution. */
  auto make_bandpass_filter(
        fourier_transform const& fft  ///< Fourier transform filter will be used with
      , float grid_resolution         ///< Side length of each grid cell
      , float rolloff                 ///< Width of each crossover in octaves
      , float crossover_l             ///< Wavelength of crossover point on low frequency side
      , float crossover_h             ///< Wavelength of crossover point on high frequency side
      ) -> filter;

  /// Make a high-pass filter
  auto make_highpass_filter(
        fourier_transform const& fft  ///< Fourier transform filter will be used with
      , float grid_resolution         ///< Side length of each grid cell
      , float rolloff                 ///< Width of each crossover in octaves
      , float crossover               ///< Wavelength of crossover point
      ) -> filter;

  /// Bandpass filter bank with perfect reconstruction
  class filter_bank
  {
  public:
    /// Construct the filter bank
    /** The filter bank will be a set of low-pass/band-pass/high-pass filters which together add up to unity so that
     *  the original total input power is retained at every frequency.  The rolloff parameter defines the width of the
     *  crossover zone at either side of the bandpass filters in octaves.  This means that a value of 1 will cause the
     *  fade between bands to occur over a magnitude of 2^1 (so it might start at 100km and end at 50km).  A value of
     *  2 fades over two octaves (2^2 - so 100km to 25km) etc.
     *  If explicit crossover wavelengths are not given (the crossovers array has size 0) then the crossovers will be
     *  spread evenly throughout the spectrum in log space.  The wavelengths specified in crossovers are given in the
     *  same units as the grid resolution. */
    filter_bank(fourier_transform const& fft, float grid_resolution, int bands, float rolloff, array1f crossovers);

    /// Number of filters in the bank
    auto size() const { return info_.size(); }

    /// Wavelength of low frequency crossover for a filter (same units as grid resolution)
    auto crossover_low(int i) const { return info_[i].xo_low; };

    /// Wavelength at center of a filter (same units as grid resolution)
    auto central_wavelength(int i) const { return info_[i].center; }

    /// Wavelength of high frequency crossover for a filter (same units as grid resolution)
    auto crossover_high(int i) const { return info_[i].xo_low; };

    /// Access a filter
    auto& operator[](int i) const { return filters_[i]; }

  private:
    struct info
    {
      float xo_low;
      float center;
      float xo_high;
    };

  private:
    array1<info>    info_;
    vector<filter>  filters_;
  };
}
