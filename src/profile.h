/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once
#include "format.h"
#include "options.h"
#include <atomic>
#include <chrono>

namespace steps
{
#if opt_disable_profile
  class [[maybe_unused]] profile
  {
  public:
    class [[maybe_unused]] scope
    {
    public:
      scope(profile& prof) { }
      scope(profile const&) = delete;
      scope(profile&&) = delete;
      auto reset(profile& prof) -> void { }
    };

  public:
    profile(string name) { }

  public:
    static auto generate_report(bool reset) -> string { return {}; }
  };
#else
  class profile
  {
  public:
    using clock = std::chrono::high_resolution_clock;

    /// RAII object that measures its lifetime and adds it to a profile
    class scope
    {
    public:
      scope(profile& prof)
      {
        prof_ = &prof;
        start_ = clock::now();
      }

      scope(profile const&) = delete;
      scope(profile&&) = delete;

      ~scope() noexcept
      {
        prof_->apply(clock::now() - start_);
      }

      /// Apply the time measured so far and restart measuring against a new profile
      auto reset(profile& prof) -> void
      {
        auto now = clock::now();
        prof_->apply(now - start_);
        prof_ = &prof;
        start_ = now;
      }

    private:
      profile*          prof_;
      clock::time_point start_;
    };

  public:
    profile(string name);

    ~profile();

    auto& name() const { return name_; }
    auto count() const -> long { return count_; }
    auto total() const -> long { return total_; }

    auto apply(clock::duration time) -> void
    {
      count_++;
      total_ += std::chrono::duration_cast<std::chrono::microseconds>(time).count();
    }

    auto reset() -> void
    {
      count_ = 0;
      total_ = 0;
    }

  public:
    static auto generate_report(bool reset) -> string;

  private:
    string            name_;    // name of this profile category
    std::atomic_long  count_;   // number of hits
    std::atomic_long  total_;   // total time spent running (microseconds)
  };
#endif
}
