/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "trace.h"
#include "unit_test.h"
#include <iostream>
#include <mutex>

using namespace steps;

using prefix_store = std::array<string, enum_traits<trace_level>::count>;

static auto const level_codes = prefix_store{"  ", "EE", "WW", "**", "--", "DD"};
static auto const color_init = prefix_store{"", "\033[31;1m", "\033[33;1m", "\033[36;1m", "", "\033[32m"};
static auto const color_term = "\033[0m"s;

static trace_level max_level_ = trace_level::log;
static bool color_ = false;
static thread_local string thread_prefix_;

static std::mutex mut_stdout_;
static trace_write_fn target_write_ = trace_target_stdout_write;
static trace_flush_fn target_flush_ = trace_target_stdout_flush;

auto steps::trace_max_level() -> trace_level
{
  return max_level_;
}

auto steps::trace_set_max_level(trace_level max_level) -> void
{
  max_level_ = max_level;
}

auto steps::trace_color_output() -> bool
{
  return color_;
}

auto steps::trace_set_color_output(bool enabled) -> void
{
  color_ = enabled;
}

auto steps::trace_target_stdout_write(string buffer) -> void
{
  /* this mutex is not required for thread safety - the standard already guarantees that for cout.  however the
   * standard allows for characters from concurrent calls to be interleaved.  so we just use this lock to ensure that
   * each whole "trace" payload is output atomically. */
  auto lock = std::lock_guard<std::mutex>{mut_stdout_};
  std::cout.write(buffer.data(), buffer.size());
}

auto steps::trace_target_stdout_flush() -> void
{
  auto lock = std::lock_guard<std::mutex>{mut_stdout_};
  std::cout.flush();
}

auto steps::trace_set_target(trace_write_fn write, trace_flush_fn flush) -> void
{
  target_write_ = std::move(write);
  target_flush_ = std::move(flush);
}

auto steps::trace_flush_target() -> void
{
  if (target_flush_)
    target_flush_();
}

auto steps::vtrace(trace_level level, char const* format_str, fmt::format_args const& args) -> void
{
  if (level > max_level_ || !target_write_)
    return;

  auto ilevel = static_cast<int>(level);
  auto prefix = format("{:%Y-%m-%dT%H:%M:%SZ} {} {}", clock::now(), level_codes[ilevel], thread_prefix_);
  auto payload = fmt::vformat(format_str, args);

  auto buffer = string();
  if (color_)
    buffer.append(color_init[ilevel]);
  string::size_type from = 0, till;
  while ((till = payload.find('\n', from)) != string::npos)
  {
    buffer.append(prefix);
    buffer.append(&payload[from], ++till - from);
    from = till;
  }
  buffer.append(prefix);
  buffer.append(&payload[from], payload.size() - from);
  if (color_)
    buffer.append(color_term);
  buffer.push_back('\n');

  target_write_(std::move(buffer));
}

trace_prefix::trace_prefix(char const* prefix)
  : restore_length_{thread_prefix_.size()}
{
  thread_prefix_.append(prefix).push_back(' ');
}

trace_prefix::trace_prefix(string const& prefix)
  : restore_length_{thread_prefix_.size()}
{
  thread_prefix_.append(prefix).push_back(' ');
}

trace_prefix::~trace_prefix()
{
  thread_prefix_.resize(restore_length_);
}

// LCOV_EXCL_START
#include <regex>
TEST_CASE("trace")
{
  std::ostringstream oss;
  auto oss_target = [&](string buf) { oss.write(buf.data(), buf.size()); };

  CHECK(parse<trace_level>("verbose") == trace_level::verbose);
  CHECK_THROWS(parse<trace_level>("bad"));

  // do this to ensure our global state is know.  caller or other tests may have messed with it before now
  trace_set_max_level(trace_level::log);
  CHECK(trace_max_level() == trace_level::log);
  trace_set_color_output(false);
  CHECK(trace_color_output() == false);

  trace_set_target(oss_target, nullptr);

  CHECK_NOTHROW(trace(trace_level::log, "foo"));
  CHECK(std::regex_match(oss.str(), std::regex{".{20} -- foo\n"}));

  oss.str("");
  CHECK_NOTHROW(trace(trace_level::error, "foo\nbar"));
  CHECK(std::regex_match(oss.str(), std::regex{".{20} EE foo\n.{20} EE bar\n"}));

  trace_set_max_level(trace_level::error);

  oss.str("");
  CHECK_NOTHROW(trace(trace_level::warning, "foo"));
  CHECK(oss.str() == "");

  oss.str("");
  CHECK_NOTHROW(vtrace(trace_level::verbose, "foo", fmt::make_format_args()));
  CHECK(oss.str() == "");

  trace_set_max_level(trace_level::log);
  {
    auto pfx1 = trace_prefix{"foo"};
    oss.str("");
    CHECK_NOTHROW(trace(trace_level::log, "baz"));
    CHECK(std::regex_match(oss.str(), std::regex{".{20} -- foo baz\n"}));

    {
      auto pfx2 = trace_prefix{"bar"s};
      oss.str("");
      CHECK_NOTHROW(trace(trace_level::log, "baz"));
      CHECK(std::regex_match(oss.str(), std::regex{".{20} -- foo bar baz\n"}));
    }

    oss.str("");
    CHECK_NOTHROW(trace(trace_level::log, "baz"));
    CHECK(std::regex_match(oss.str(), std::regex{".{20} -- foo baz\n"}));
  }
  oss.str("");
  CHECK_NOTHROW(trace(trace_level::log, "baz"));
  CHECK(std::regex_match(oss.str(), std::regex{".{20} -- baz\n"}));

  oss.str("");
  trace_set_color_output(true);
  CHECK(trace_color_output() == true);
  CHECK_NOTHROW(trace(trace_level::error, "foo"));
  CHECK(std::regex_match(oss.str(), std::regex{"\033\\[31;1m.{20} EE foo\033\\[0m\n"}));

  trace_set_target(nullptr, nullptr);
  CHECK_NOTHROW(trace(trace_level::notice, "foo"));
}
// LCOV_EXCL_STOP
