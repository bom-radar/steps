/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "cascade.h"
#include "diagnostics.h"
#include "options.h"
#include "profile.h"
#include "thread_pool.h"
#include "unit_test.h"

using namespace steps;

static profile prof_recompose{"cascade::recompose"};
static profile prof_decompose_space{"cascade::decompose_space"};
static profile prof_decompose_spect{"cascade::decompose_spect"};

cascade::cascade(int levels, vec2i shape)
{
  if (levels < 1)
    throw std::invalid_argument{"Invalid number of cascade levels specified"};

  levels_.reserve(levels);
  for (auto lvl = 0; lvl < levels; ++lvl)
    levels_.emplace_back(shape);
}

auto cascade::dump_diagnostics(char const* name, std::initializer_list<int> coords) const -> void
{
  auto lvl_coords = array1i(coords.size() + 1);
  for (auto i = 0; i < lvl_coords.size() - 1; ++i)
    lvl_coords[i] = *(coords.begin() + i);

  for (auto lvl = 0; lvl < size(); ++lvl)
  {
    lvl_coords[lvl_coords.size() - 1] = lvl;
    diagnostics::write(name, levels_[lvl], lvl_coords);
  }
}

inline static auto decompose_impl(
      fourier_transform const& fft
    , filter_bank const& filters
    , fourier_transform::array2c const& spectrum
    , cascade& cascade
    ) -> void
{
  // apply each bandpass and inverse FFT to generate the levels
  thread_pool_distribute_loop(cascade.size(), [&](int lvl)
  {
    auto& level = cascade[lvl];
    auto band_c = fourier_transform::array2c{fft.shape_c()};
    auto band_r = fourier_transform::array2r{fft.shape_r()};

    // apply bandpass filter to the spectrum
    for (int i = 0; i < band_c.size(); ++i)
      band_c.data()[i] = spectrum.data()[i] * filters[lvl].data()[i];

    // transform from complex to real
    fft.execute_inverse(band_c, band_r);

    // transfer top right corner of spatial domain to cascade
    for (int y = 0; y < level.shape().y; ++y)
      for (int x = 0; x < level.shape().x; ++x)
        level[y][x] = band_r[y][x];
  });
}

static auto pad_reflect_and_fade(array2f const& field, vec2i shape, float padding, int border)
{
  // sanity checks
  if (   field.shape().x + border * 2 > shape.x
      || field.shape().y + border * 2 > shape.y)
    throw std::runtime_error{"Border width too large"};

  // precalculate the tukey window weight in the border pixels
  auto weights = array1f{border};
  for (auto i = 0; i < weights.size(); ++i)
    weights[i] = 0.5f * (1 + std::cos(pi<double> * i / border));

  // copy in while reflecting borders into the padding area
  auto out = fourier_transform::array2r{shape};
  for (auto y = 0; y < field.shape().y; ++y)
  {
    for (auto x = 0; x < field.shape().x; ++x)
      out[y][x] = field[y][x];
    for (auto x = field.shape().x; x < field.shape().x + border; ++x)
      out[y][x] = lerp(padding, field[y][field.shape().x - (x - field.shape().x) - 2], weights[x - field.shape().x]);
    for (auto x = field.shape().x + border; x < out.shape().x - border; ++x)
      out[y][x] = padding;
    for(auto x = out.shape().x - border; x < out.shape().x; ++x)
      out[y][x] = lerp(padding, field[y][out.shape().x - x], weights[out.shape().x - x - 1]);
  }
  for (auto y = field.shape().y; y < field.shape().y + border; ++y)
  {
    auto yi = field.shape().y - (y - field.shape().y) - 2;
    auto yf = weights[y - field.shape().y];
    for (auto x = 0; x < field.shape().x; ++x)
      out[y][x] = field[yi][x] * yf;
    for (auto x = field.shape().x; x < field.shape().x + border; ++x)
      out[y][x] = lerp(padding, field[yi][field.shape().x - (x - field.shape().x) - 2], yf * weights[x - field.shape().x]);
    for (auto x = field.shape().x + border; x < out.shape().x - border; ++x)
      out[y][x] = padding;
    for(auto x = out.shape().x - border; x < out.shape().x; ++x)
      out[y][x] = lerp(padding, field[yi][out.shape().x - x], yf * weights[out.shape().x - x - 1]);
  }
  for (auto y = field.shape().y + border; y < out.shape().y - border; ++y)
    for (auto x = 0; x < out.shape().x; ++x)
      out[y][x] = padding;
  for (auto y = out.shape().y - border; y < out.shape().y; ++y)
  {
    auto yi = out.shape().y - y;
    auto yf = weights[out.shape().y - y - 1];
    for (auto x = 0; x < field.shape().x; ++x)
      out[y][x] = field[yi][x] * yf;
    for (auto x = field.shape().x; x < field.shape().x + border; ++x)
      out[y][x] = lerp(padding, field[yi][field.shape().x - (x - field.shape().x) - 2], yf * weights[x - field.shape().x]);
    for (auto x = field.shape().x + border; x < out.shape().x - border; ++x)
      out[y][x] = padding;
    for(auto x = out.shape().x - border; x < out.shape().x; ++x)
      out[y][x] = lerp(padding, field[yi][out.shape().x - x], yf * weights[out.shape().x - x - 1]);
  }

  return out;
}

auto steps::decompose(
      fourier_transform const& fft
    , filter_bank const& filters
    , array2f const& field
    , float padding
    , int border_width
    , cascade& cascade
    , filter* psd
    ) -> void
{
  auto ps = profile::scope{prof_decompose_space};

  // copy the input to the top right corner of spatial domain while reflecting the borders out into
  // the zero padding area.  we fade out the reflection using at tukey window with the specified width
  auto real = pad_reflect_and_fade(field, fft.shape_r(), padding, border_width);

  // transform field from real to complex
  auto spectrum = fourier_transform::array2c{fft.shape_c()};
  fft.execute_forward(real, spectrum);

  // calculate the PSD
  // note this is not actually the PSD which would use the square of the abs (i.e. std::norm).  it's just the spectrum magnitude
  if (psd)
  {
    if (psd->shape() != spectrum.shape())
      throw std::invalid_argument{"PSD size mismatch"};
    for (auto i = 0; i < spectrum.size(); ++i)
      psd->data()[i] = std::abs(spectrum.data()[i]);
  }

  decompose_impl(fft, filters, spectrum, cascade);
}

auto steps::decompose(
      fourier_transform const& fft
    , filter_bank const& filters
    , fourier_transform::array2c const& spectrum
    , cascade& cascade
    ) -> void
{
  auto ps = profile::scope{prof_decompose_spect};
  decompose_impl(fft, filters, spectrum, cascade);
}

auto steps::recompose(cascade const& cascade, array2f& field) -> void
{
  auto ps = profile::scope{prof_recompose};

  // sanity checks
  if (field.size() != cascade[0].size())
    throw std::invalid_argument{"Recompose field size mismatch"};

  // accumulate cascade levels into the result field
  for (int i = 0; i < cascade[0].size(); ++i)
  {
    auto val = 0.0f;
    for (auto lvl = 0; lvl < cascade.size(); ++lvl)
      val += cascade[lvl].data()[i];
    field.data()[i] = val;
  }
}

auto steps::recompose_partial(cascade const& cascade, int levels, array2f& field) -> void
{
  auto ps = profile::scope{prof_recompose};

  // sanity checks
  if (field.size() != cascade[0].size())
    throw std::invalid_argument{"Recompose field size mismatch"};

  // accumulate cascade levels into the result field
  for (int i = 0; i < cascade[0].size(); ++i)
  {
    auto val = 0.0f;
    for (auto lvl = 0; lvl < levels; ++lvl)
      val += cascade[lvl].data()[i];
    field.data()[i] = val;
  }
}

auto steps::recompose_unnormalize(cascade const& cascade, array1f const& means, array1f const& stddevs, array2f& field) -> void
{
  auto ps = profile::scope{prof_recompose};

  // sanity checks
  if (field.size() != cascade[0].size())
    throw std::invalid_argument{"Recompose field size mismatch"};

  // unnormalize the values at each level and accumulate into the result field
  for (int i = 0; i < cascade[0].size(); ++i)
  {
    auto val = 0.0f;
    for (auto lvl = 0; lvl < cascade.size(); ++lvl)
      val += means[lvl] + stddevs[lvl] * cascade[lvl].data()[i];
    field.data()[i] = val;
  }
}

// LCOV_EXCL_START
TEST_CASE("cascade")
{
  CHECK_THROWS(cascade(0, {1,1}));

  // TODO - decompose and recompose a reference image and check recomposition matches reference
}
TEST_CASE("pad_reflect_and_fade")
{
  auto field = array2f{10, 10};
  for (auto y = 0; y < field.shape().y; ++y)
    for (auto x = 0; x < field.shape().x; ++x)
      field[y][x] = y * 100 + x;

  auto res = pad_reflect_and_fade(field, vec2i{20, 20}, 0.0f, 4);
  CHECK(res.shape() == vec2i{20, 20});
  #if 0 // TODO
  for (auto y = 0; y < res.shape().y; ++y)
  {
    string str;
    for (auto x = 0; x < res.shape().x; ++x)
      str.append(format("{:1.1f} ", res[y][x]));
    trace(trace_level::log, "{}", str);
  }
  #endif
}
// LCOV_EXCL_STOP
