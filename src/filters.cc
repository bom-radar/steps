/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "filters.h"
#include "profile.h"
#include "thread_pool.h"
#include "trace.h"
#include "unit_test.h"
#include <fstream>
#include <thread>

using namespace steps;

static_assert(std::numeric_limits<float>::has_infinity, "Floating point representation does not support infinity");

static profile prof_filter_bank_init{"filter_bank::setup"};

auto steps::make_parametric_psd(
      fourier_transform const& fft
    , float grid_resolution
    , float scale_break
    , float beta_1
    , float beta_2
    ) -> filter
{
  // useful constants
  auto aspect = float(fft.shape_r().x / fft.shape_r().y);
  auto scale = std::pow(scale_break, beta_1 - beta_2);

  // generate 2D psd
  auto psd = filter{fft.shape_c()};

  // calculate the top half of the psd (it is vertically symetrical)
  auto half = fft.shape_c().y / 2 + 1;
  for (auto y = 0; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      // determine the frequency at this point
      auto ya = y * aspect;
      auto wavelength = (fft.shape_r().x * grid_resolution) / std::sqrt(x * x + ya * ya);

      // calculate the desired PSD value for this wavelength
      psd[y][x] = wavelength > scale_break ? std::pow(wavelength, beta_1) : scale * std::pow(wavelength, beta_2);

      /* our PSD arrays are really spectrum magnitude NOT spectrum power since they store the absolute value of the wave
       * in the spectrum not the square of the absolute value.  see the decompose function.  consequently we need to
       * square root our value here since our beta parameters are specified as for the real PSD (a squared quantity) */
      psd[y][x] = std::sqrt(psd[y][x]);
    }
  }

  // fix degenerate infinite wavelength pixel
  psd[0][0] = 0.0f;

  // mirror the top half into the bottom
  for (auto y = 1; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      psd[fft.shape_c().y - y][x] = psd[y][x];
    }
  }

  return psd;
}

auto steps::make_lowpass_filter(
      fourier_transform const& fft
    , float grid_resolution
    , float rolloff
    , float crossover
    ) -> filter
{
  auto aspect = float(fft.shape_r().x / fft.shape_r().y);
  auto width = rolloff * 2.0f;
  auto qtr_width = 0.25f * width;
  auto twopi_on_width = (2.0f * pi<float>) / width;

  auto r_lnxo = std::log2(crossover);
  auto r_peak = r_lnxo + qtr_width;
  auto r_zero = r_lnxo - qtr_width;

  // lambda to calculate the filter value at a given frequency
  auto calc_filter = [&](float wavelength)
  {
    auto lnwv = std::log2(wavelength);
    if (lnwv > r_peak)
      return 1.0f;
    if (lnwv > r_zero)
      return 0.5f * (1.0f + std::cos(twopi_on_width * (lnwv - r_peak)));
    return 0.0f;
  };

  // generate 2D filter
  auto filter = steps::filter{fft.shape_c()};

  // calculate the top half of the filter (it is vertically symetrical)
  auto half = fft.shape_c().y / 2 + 1;
  for (auto y = 0; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      // determine the frequency at this point
      auto ya = y * aspect;
      auto wavelength = (fft.shape_r().x * grid_resolution) / std::sqrt(x * x + ya * ya);
      filter[y][x] = calc_filter(wavelength);
    }
  }

  // fix degenerate infinite wavelength pixel
  filter[0][0] = 1.0f;

  // mirror the top half into the bottom
  for (auto y = 1; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      filter[fft.shape_c().y - y][x] = filter[y][x];
    }
  }

  return filter;
}

auto steps::make_bandpass_filter(
      fourier_transform const& fft
    , float grid_resolution
    , float rolloff
    , float crossover_l
    , float crossover_h
    ) -> filter
{
  // sanity checks
  if (crossover_l <= crossover_h)
    throw std::runtime_error{"Bandpass crossover points invalid"};

  // useful constants
  auto aspect = float(fft.shape_r().x / fft.shape_r().y);
  auto width = rolloff * 2.0f;
  auto qtr_width = 0.25f * width;
  auto twopi_on_width = (2.0f * pi<float>) / width;

  // precalculate filter characteristics
  auto l_lnxo = std::log2(crossover_l);
  auto l_peak = l_lnxo - qtr_width;
  auto l_zero = l_lnxo + qtr_width;

  auto r_lnxo = std::log2(crossover_h);
  auto r_peak = r_lnxo + qtr_width;
  auto r_zero = r_lnxo - qtr_width;

  // sanity check for filter too narrow to support desired rollofff
  if (l_peak < r_peak)
    throw std::runtime_error{format("{} - {} km bandpass filter too narrow for {} octave rolloff", crossover_l, crossover_h, rolloff)};

  // lambda to calculate the filter value at a given frequency
  auto calc_filter = [&](float wavelength)
  {
    auto lnwv = std::log2(wavelength);
    if (lnwv > l_zero)
      return 0.0f;
    if (lnwv > l_peak)
      return 0.5f * (1.0f + std::cos(twopi_on_width * (lnwv - l_peak)));
    if (lnwv > r_peak)
      return 1.0f;
    if (lnwv > r_zero)
      return 0.5f * (1.0f + std::cos(twopi_on_width * (lnwv - r_peak)));
    return 0.0f;
  };

  // allocate 2D filter fields
  auto filter = steps::filter{fft.shape_c()};

  // calculate the top half of the filter (it is vertically symetrical)
  auto half = fft.shape_c().y / 2 + 1;
  for (auto y = 0; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      // determine the frequency at this point
      auto ya = y * aspect;
      auto wavelength = (fft.shape_r().x * grid_resolution) / std::sqrt(x * x + ya * ya);
      filter[y][x] = calc_filter(wavelength);
    }
  }

  // fix degenerate infinite wavelength pixel
  filter[0][0] = 0.0f;

  // mirror the top half into the bottom
  for (auto y = 1; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      filter[fft.shape_c().y - y][x] = filter[y][x];
    }
  }

  return filter;
}

auto steps::make_highpass_filter(
      fourier_transform const& fft
    , float grid_resolution
    , float rolloff
    , float crossover
    ) -> filter
{
  // useful constants
  auto aspect = float(fft.shape_r().x / fft.shape_r().y);
  auto width = rolloff * 2.0f;
  auto qtr_width = 0.25f * width;
  auto twopi_on_width = (2.0f * pi<float>) / width;

  // precalculate filter characteristics
  auto l_lnxo = std::log2(crossover);
  auto l_peak = l_lnxo - qtr_width;
  auto l_zero = l_lnxo + qtr_width;

  // lambda to calculate the filter value at a given frequency
  auto calc_filter = [&](float wavelength)
  {
    auto lnwv = std::log2(wavelength);
    if (lnwv > l_zero)
      return 0.0f;
    if (lnwv > l_peak)
      return 0.5f * (1.0f + std::cos(twopi_on_width * (lnwv - l_peak)));
    return 1.0f;
  };

  // allocate 2D filter fields
  auto filter = steps::filter{fft.shape_c()};

  // calculate the top half of the filter (it is vertically symetrical)
  auto half = fft.shape_c().y / 2 + 1;
  for (auto y = 0; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      // determine the frequency at this point
      auto ya = y * aspect;
      auto wavelength = (fft.shape_r().x * grid_resolution) / std::sqrt(x * x + ya * ya);
      filter[y][x] = calc_filter(wavelength);
    }
  }

  // fix degenerate infinite wavelength pixel
  filter[0][0] = 0.0f;

  // mirror the top half into the bottom
  for (auto y = 1; y < half; ++y)
  {
    for (auto x = 0; x < fft.shape_c().x; ++x)
    {
      filter[fft.shape_c().y - y][x] = filter[y][x];
    }
  }

  return filter;
}

filter_bank::filter_bank(fourier_transform const& fft, float grid_resolution, int bands, float rolloff, array1f crossovers)
  : info_{bands}
  , filters_(size_t(bands))
{
  auto ps = profile::scope{prof_filter_bank_init};

  // sanity checks
  if (bands < 1)
    throw std::runtime_error{"Invalid number of levels"};
  if (crossovers.size() != 0 && crossovers.size() != bands - 1)
    throw std::runtime_error{"Incorrect number of crossover wavelengths supplied"};

  // useful constants
  // log2 of our maximum wavelength (domain size)
  auto ln_max = std::log2(grid_resolution * float(std::max(fft.shape_r().x, fft.shape_r().y)));
  // log2 of our minimum wavelenth (2 cells)
  auto ln_min = std::log2(grid_resolution * 2);

  // provide default crossovers if not supplied by user
  if (crossovers.size() == 0)
  {
    crossovers = array1f{bands - 1};
    for (auto band = 0; band < bands - 1; ++band)
      crossovers[band] = std::exp2(ln_max + (float(band + 1) / bands) * (ln_min - ln_max));
  }

  // determine central frequencies
  info_[0].xo_low = std::numeric_limits<float>::infinity();
  info_[0].center = std::exp2(ln_max + 0.5 * (std::log2(crossovers[0]) - ln_max));
  info_[0].xo_high = crossovers[0];
  for (auto band = 1; band < bands - 1; ++band)
  {
    info_[band].xo_low = crossovers[band - 1];
    info_[band].center = std::exp2(std::log2(crossovers[band-1]) + 0.5 * (std::log2(crossovers[band]) - std::log2(crossovers[band-1])));
    info_[band].xo_high = crossovers[band];
  }
  info_[bands - 1].xo_low = crossovers[bands - 2];
  info_[bands - 1].center = std::exp2(std::log2(crossovers[bands-2]) + 0.5 * (ln_min - std::log2(crossovers[bands-2])));
  info_[bands - 1].xo_high = 0.0f;

  // generate the filter for each band
  thread_pool_distribute_loop(bands, [&](int band)
  {
    if (band == 0)
      filters_[band] = make_lowpass_filter(fft, grid_resolution, rolloff, crossovers[band]);
    else if (band < bands - 1)
      filters_[band] = make_bandpass_filter(fft, grid_resolution, rolloff, crossovers[band-1], crossovers[band]);
    else
      filters_[band] = make_highpass_filter(fft, grid_resolution, rolloff, crossovers[band-1]);
  });

  // trace filter shapes
  for (auto band = 0; band < bands; ++band)
    trace(trace_level::log, "Level {} xo-lo {:<6.2f} center {:<6.2f} xo-hi {:<6.2f}", band, info_[band].xo_low, info_[band].center, info_[band].xo_high);

#if 0
  // sanity check that the filter bank has unity output at all frequencies
  for (auto i = 0; i < filters_[0].size(); ++i)
  {
    auto sum = 0.0f;
    for (auto band = 0; band < bands; ++band)
      sum += filters_[band].data()[i];
    if (sum != approx(1.0f))
      throw std::runtime_error{format("Filter bank not unity for frequency {}", i)};
  }
#endif

#if 0
  // output 1d filter between our longest wavelength (domain size) and shortest (2 grid cells)
  std::ofstream ofs{"filters.csv"};
  for (auto i = 0; i < 200; ++i)
  {
    auto wl = std::exp2(ln_max + (i / 200.0f) * (ln_min - ln_max));
    auto lnwv = std::log2(wl);

    ofs << wl;
    for (auto band = 0; band < bands; ++band)
    {
      auto width = rolloff * 2.0f;
      auto qtr_width = 0.25f * width;
      auto twopi_on_width = (2.0f * pi<float>) / width;

      auto l_lnxo = std::log2(info_[band].xo_low);
      auto l_peak = l_lnxo - qtr_width;
      auto l_zero = l_lnxo + qtr_width;

      auto r_lnxo = std::log2(info_[band].xo_high);
      auto r_peak = r_lnxo + qtr_width;
      auto r_zero = r_lnxo - qtr_width;

      float val;
      if (lnwv > l_zero)
        val = 0.0f;
      else if (lnwv > l_peak)
        val = 0.5f * (1.0f + std::cos(twopi_on_width * (lnwv - l_peak)));
      else if (lnwv > r_peak)
        val = 1.0f;
      else if (lnwv > r_zero)
        val = 0.5f * (1.0f + std::cos(twopi_on_width * (lnwv - r_peak)));
      else
        val = 0.0f;
      ofs << ',' << val;
    }
    ofs << '\n';
  }
#endif
}

#if 0
/* this is a very rough function to create a 1D PSD based on a psd filter as used by STEPS. note that we
 * square the values of the psd filter so that the output from this is a "real" PSD as opposed to our
 * internal PSD arrays which actually store spectrum magnitude not spectrum power.  this is far from a
 * correct implementation, but it gives a rough idea.  remember that the power must be _averaged_ over
 * all waves of a given length. */
auto dump_1d_psd(fourier_transform const& fft, float grid_resolution, filter const& psd) -> void
{
  auto aspect = float(fft.shape_r().x / fft.shape_r().y);

  auto nth = 20;
  struct histbin { double wavelength, total; long count; };
  auto histogram = vector<histbin>(fft.shape_c().x / nth);
  for (auto bin = 0; bin < histogram.size(); ++bin)
  {
    // histogram.wavelength is HIGH side of bin (being lazy)
    histogram[bin].wavelength = (fft.shape_r().x * grid_resolution) / ((bin + 1) * nth);
    histogram[bin].total = 0.0;
    histogram[bin].count = 0;
  }

  for (auto y = 0; y < psd.shape().y / 2; ++y)
  {
    for (auto x = 0; x < psd.shape().x; ++x)
    {
      auto ya = y * aspect;
      auto wavelength = (fft.shape_r().x * grid_resolution) / std::sqrt(x * x + ya * ya);

      auto bin = 0;
      while (bin < histogram.size())
      {
        if (wavelength > histogram[bin].wavelength)
        {
          histogram[bin].total += psd[y][x] * psd[y][x];
          histogram[bin].count++;
          break;
        }
        ++bin;
      }
    }
  }

  // this is normalized so so the first bin reports a power of 1
  auto ofs = std::ofstream{"psd.csv"};
  ofs << "wavelength,power\n";
  auto norm = histogram[0].total / histogram[0].count;
  for (auto bin = 0; bin < histogram.size(); ++bin)
  {
    auto val = (histogram[bin].total / histogram[bin].count) * norm;
    ofs << histogram[bin].wavelength << "," << val << "\n";
  }
}
#endif

// LCOV_EXCL_START
TEST_CASE("filter_bank")
{
  auto fft = fourier_transform{{512, 512}, 4};

  CHECK_THROWS(filter_bank(fft, 1.0f, 0, 1.0f, array1f(0)));
  CHECK_THROWS(filter_bank(fft, 1.0f, 5, 1.0f, array1f(3)));
  CHECK_THROWS(filter_bank(fft, 1.0f, 5, 10.0f, array1f(0)));

  // initialize our filter bank
  auto bank = filter_bank{fft, 1.0f, 5, 1.0f, {}};

  /* the first row of the spectrum is DC and is NOT replicated on the bottom so, ensure that the filters have been
   * correctly mirrored.  this means row 1 should match the last row, row 2 match the second last and so on */
  for (int y = 1; y < fft.shape_c().y/2; ++y)
    for (int x = 0; x < fft.shape_c().x; ++x)
      CHECK(bank[0][y][x] == approx(bank[0][fft.shape_c().y-y][x]));

  // check that the overall output at each frequency is 1
  for (auto i = 0; i < bank[0].size(); ++i)
  {
    auto sum = 0.0f;
    for (auto band = 0; band < bank.size(); ++band)
      sum += bank[band].data()[i];
    if (sum != approx(1.0f))
      throw std::runtime_error{format("Filter bank not unity for frequency {}", i)};
  }
}
// LCOV_EXCL_STOP
