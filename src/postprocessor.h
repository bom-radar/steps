/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2023 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "products.h"

namespace steps
{
  class postprocessor
  {
  public:
    postprocessor(configuration const& config);

    auto set_progress_callback(progress_callback_fn cb_progress)  { cb_progress_ = std::move(cb_progress); }

    auto members() const { return members_; }

    auto generate_products(time_point reference_time) -> void;

  private:
    metadata              user_parameters_;
    int                   members_;
    int                   member_offset_;
    int                   forecasts_;
    duration              time_step_;
    vec2i                 grid_shape_;
    float                 grid_resolution_;
    unique_ptr<input>     input_;
    products::rain_rate   products_;
    int                   max_engine_spread_;
    progress_callback_fn  cb_progress_;
  };
}
