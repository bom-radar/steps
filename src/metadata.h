/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "configuration.h"
#include "format.h"
#include "util.h"

namespace steps
{
  /// Variant used to contain a generic metadatum value
  using metadatum = variant<
      int8_t
    , uint8_t
    , int16_t
    , uint16_t
    , int32_t
    , uint32_t
    , int64_t
    , uint64_t
    , float
    , double
    , string
    , time_point
    >;

  /// Store of user defined metadata which can be passed to outputs
  using metadata = map<string, metadatum, std::less<>>;

  /// Parse a metadata store from configuration
  auto parse_metadata(
        configuration const& config
      , metadata const& user_parameters
      , metadata const* parent = nullptr
      ) -> metadata;
}

namespace fmt
{
  /// Formatter for a metadata store
  /** This formatter allows the user to select a member of the metadata store by name, and then delegate formatting to
   *  the stored type. */
  template <typename Char>
  struct formatter<steps::metadata, Char>
  {
    template <typename ParseContext>
    FMT_CONSTEXPR auto parse(ParseContext& ctx) -> decltype(ctx.begin())
    {
      // extract the key name
      auto i = ctx.begin();
      auto begin = i;
      while (i != ctx.end() && *i != ':' && *i != '}')
        ++i;
      if (i == begin)
        throw std::runtime_error{"No key specified in metadata store format args"};
      key_ = string_view{&(*begin), size_t(i - begin)};

      // parse any optional args for the value itself
      if (i != ctx.end() && *i == ':')
      {
        ++i;
        begin = i;
        while (i != ctx.end() && *i != '}')
          ++i;
        fmt_ = string_view{&(*begin), size_t(i - begin)};
      }

      return i;
    }

    template <typename FormatContext>
    auto format(steps::metadata const& val, FormatContext& ctx) -> decltype(ctx.out())
    {
      auto ival = val.find(key_);
      if (ival == val.end())
        throw std::runtime_error{fmt::format("Key {} not found in metadata store", key_)};

      auto pctx = basic_format_parse_context<Char>{fmt_};
      return std::visit([&](auto const& v)
      {
        auto f = formatter<std::decay_t<decltype(v)>, Char>{};
        f.parse(pctx);
        return f.format(v, ctx);
      }, ival->second);
    }

  private:
    string_view key_;
    string_view fmt_;
  };
}
