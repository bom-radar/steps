/*---------------------------------------------------------------------------------------------------------------------
 * STEPS AWS Service
 *
 * Copyright 2020 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "mosaic.h"
#include "compression.h"
#include "thread_pool.h"
#include "trace.h"

using namespace steps;

static constexpr uint64_t mosaic_tile_file_magic = 0xb1a2202885407b03;
static constexpr uint32_t mosaic_tile_file_version = 1;

struct mosaic_tile_header
{
  uint64_t  magic;
  uint32_t  version;
  int       grid_count;
  vec2i     grid_shape;
  bool      int8_data;    // true if encoded as int8_t, false if encoded as float
  int       compression;  // zlib compression level (-1 for no compression)
};

struct mosaic_tile_chunk_header
{
  int       grid_index;   // index of grid if chunks were stored in an ordered and flattened array
  uint32_t  chunk_size;   // size of compressed data chunk
};

output_mosaic_tile::output_mosaic_tile(configuration const& config)
  : compression_{config.optional("compression", 1)}
  , grid_count_{0}
  , grid_stride_{0}
{ }

auto output_mosaic_tile::initialize(product const& parent, time_point reference_time) -> void
{
  auto series = parent.series_labels();
  auto times = parent.forecast_times();
  if (series && times)
  {
    grid_count_ = series->size() * times->size();
    grid_stride_ = times->size();
  }
  else if (series)
  {
    grid_count_ = series->size();
    grid_stride_ = 1;
  }
  else if (times)
  {
    grid_count_ = times->size();
    grid_stride_ = 1;
  }
  else
  {
    grid_count_ = 1;
    grid_stride_ = 1;
  }

  auto hdr = mosaic_tile_header{};
  hdr.magic = mosaic_tile_file_magic;
  hdr.version = mosaic_tile_file_version;
  hdr.grid_count = grid_count_;
  hdr.grid_shape = parent.grid_shape();
  hdr.int8_data = parent.units() == product_units::probability;
  hdr.compression = compression_;

  output_chunk(span{reinterpret_cast<char const*>(&hdr), sizeof(mosaic_tile_header)});
}

auto output_mosaic_tile::write(array2f const& data, int d0, int d1) -> void
{
  auto cdata = zlib_compress(
        span{reinterpret_cast<char const*>(data.data()), data.size_bytes()}
      , compression_
      , sizeof(mosaic_tile_chunk_header));
  auto hdr = reinterpret_cast<mosaic_tile_chunk_header*>(cdata.first.data());
  hdr->grid_index = (d0 == -1 ? 0 : d0) * grid_stride_ + (d1 == -1 ? 0 : d1);
  hdr->chunk_size = cdata.second - sizeof(mosaic_tile_chunk_header);
  output_chunk(span{cdata.first.data(), cdata.second});
}

auto output_mosaic_tile::write(array2<int8_t> const& data, int d0, int d1) -> void
{
  auto cdata = zlib_compress(
        span{reinterpret_cast<char const*>(data.data()), data.size_bytes()}
      , compression_
      , sizeof(mosaic_tile_chunk_header));
  auto hdr = reinterpret_cast<mosaic_tile_chunk_header*>(cdata.first.data());
  hdr->grid_index = (d0 == -1 ? 0 : d0) * grid_stride_ + (d1 == -1 ? 0 : d1);
  hdr->chunk_size = cdata.second - sizeof(mosaic_tile_chunk_header);
  output_chunk(span{cdata.first.data(), cdata.second});
}

auto output_mosaic_tile::write_all(array2f const& data) -> void
{
  auto cdata = zlib_compress(
        span{reinterpret_cast<char const*>(data.data()), data.size_bytes()}
      , compression_
      , sizeof(mosaic_tile_chunk_header));
  auto hdr = reinterpret_cast<mosaic_tile_chunk_header*>(cdata.first.data());
  hdr->chunk_size = cdata.second - sizeof(mosaic_tile_chunk_header);
  for (hdr->grid_index = 0; hdr->grid_index < grid_count_; ++hdr->grid_index)
    output_chunk(span{cdata.first.data(), cdata.second});
}

auto output_mosaic_tile::write_all(array2<int8_t> const& data) -> void
{
  auto cdata = zlib_compress(
        span{reinterpret_cast<char const*>(data.data()), data.size_bytes()}
      , compression_
      , sizeof(mosaic_tile_chunk_header));
  auto hdr = reinterpret_cast<mosaic_tile_chunk_header*>(cdata.first.data());
  hdr->chunk_size = cdata.second - sizeof(mosaic_tile_chunk_header);
  for (hdr->grid_index = 0; hdr->grid_index < grid_count_; ++hdr->grid_index)
    output_chunk(span{cdata.first.data(), cdata.second});
}

output_mosaic_tile_local::output_mosaic_tile_local(configuration const& config, metadata const& user_parameters)
  : output_mosaic_tile{config}
  , user_parameters_{&user_parameters}
  , path_format_{strip_file_schema(config["path"].string())}
{ }

auto output_mosaic_tile_local::initialize(product const& parent, time_point reference_time) -> void
{
  // store current reference time and create our output stream
  path_final_ = format(path_format_, "param"_a = *user_parameters_, "reference"_a = reference_time);
  stream_.exceptions(std::ofstream::failbit | std::ofstream::badbit);
  stream_.open(path_final_, std::ofstream::binary | std::ofstream::trunc);

  // call our parent class so that it dumps out the file header
  output_mosaic_tile::initialize(parent, reference_time);
}

auto output_mosaic_tile_local::output_chunk(span<char const> chunk) -> void
{
  // write is called from worker threads, we must use a mutex to ensure we output one chunk at at time
  auto lock = std::lock_guard<std::mutex>{mut_stream_};
  stream_.write(chunk.data(), chunk.size());
}

auto output_mosaic_tile_local::finalize() -> void
{
  stream_.close();
  path_final_.clear();
}

auto output_mosaic_tile_local::abort() -> void
{
  stream_.close();
  /* We try to remove the output file if this is not a successful outcome.  If we fail to remove the file just trace
   * it as a non-fatal error - we just give a best-effort guarantee for cleanup during forecast failure. */
  if (auto ec = std::error_code{}; !path_final_.empty() && !std::filesystem::remove(path_final_, ec) && ec)
    trace(trace_level::error, "Failed to remove partial output file {}: {}", path_final_, ec.message());
  path_final_.clear();
}

mosaic_input::mosaic_input(configuration const& config)
  : domain{config["domain"]}
  , shape{int(config["width"]), int(config["height"])}
  , offset{int(config["x"]), int(config["y"])}
{ }

mosaic_product::mosaic_product(configuration const& config, metadata const& user_parameters)
  : product{config, user_parameters, nullptr}
  , user_parameters_{&user_parameters}
  , time_step_{config["time_step"]}
  , accumulation_length_{config.optional("accumulation_length", duration{})}
  , grid_shape_{int(config["grid_cols"]), int(config["grid_rows"])}
  , grid_resolution_{config["grid_resolution"]}
  , input_path_{config["input"]}
  , method_{config["method"]}
{
  // since all products current have a time dimension...
  auto first_time = duration{config["first_forecast_offset"]};
  times_ = array1<duration>{int(config["forecasts"])};
  for (auto i = 0; i < times_->size(); ++i)
    (*times_)[i] = first_time + time_step_ * i;

  // and since everything that has a time dimension also can be based on an accum...
  if (accumulation_length_ != duration{})
  {
    lengths_ = array1<duration>{times_->size()};
    lengths_->fill(accumulation_length_);
  }

  auto& type = config["product_type"].string();
  if (type == "ensemble_mean") // first to avoid matching in the next 'if' which checks for 'starts with ensembles'
  {
    series_type_ = product_series_type::none;
    units_ = accumulation_length_ == duration{} ? product_units::rate : product_units::accumulation;
  }
  else if (starts_with(type, "ensemble"sv))
  {
    series_type_ = product_series_type::member;
    units_ = accumulation_length_ == duration{} ? product_units::rate : product_units::accumulation;
    auto offset = config.optional("member_offset", 1);
    labels_ = array1f{int(config["members"])};
    for (auto i = 0; i < labels_->size(); ++i)
      (*labels_)[i] = offset + i;
  }
  else if (type == "percentiles")
  {
    series_type_ = product_series_type::percentile;
    units_ = accumulation_length_ == duration{} ? product_units::rate : product_units::accumulation;
    labels_ = config_array_to_container<array1f>(config["percentiles"]);
  }
  else if (starts_with(type, "probabilities"sv))
  {
    series_type_ = product_series_type::probability;
    units_ = product_units::probability;
    labels_ = config_array_to_container<array1f>(config["thresholds"]);
    output_metadata()[".prob-scale_factor"] = 1.0f / int(config["members"]);
  }
  else
    throw std::runtime_error{format("Unsupported product type for mosaic: {}", type)};

  // determine metrics about our output product
  grid_count_ = (labels_ ? labels_->size() : 1) * (times_ ? times_->size() : 1);
  int8_data_ = units_ == product_units::probability;
  cell_size_ = int8_data_ ? sizeof(int8_t) : sizeof(float);
  grid_size_bytes_ = grid_count_ * grid_shape_.y * grid_shape_.x * cell_size_;
}

auto mosaic_product::generate(mosaic_input_list const& inputs, time_point reference_time) -> void
{
  /* CAREFUL - we are passing a reference to ourselves as our own parent!  this is just to avoid forcing us to
   * convert all the parent references to pointers, since almost every product is guaranteed to have a valid parent
   * it is nice to use references.  this is actually safe to do since our base class(es) actually do anything with
   * the parent.  but if you are messing around in the future take care that you don't setup an infinite loop by
   * calling into our parent (which is ourselves). */
  initialize(*this, reference_time);
  try
  {
    // initialize running state
    input_buffers_ = std::make_unique<buffer_input[]>(inputs.size());
    output_buffers_ = std::make_unique<buffer_output[]>(grid_count_);

    // queue a task to mosaic each individual grid
    thread_pool_distribute_loop(inputs.size() * grid_count_, [&](int job_index)
    {
      generate_one(inputs, reference_time, job_index);
    });

    input_buffers_.reset();
    output_buffers_.reset();
    finalize();
  }
  catch (...)
  {
    input_buffers_.reset();
    output_buffers_.reset();
    abort();
    throw;
  }
}

auto mosaic_product::generate_one(mosaic_input_list const& inputs, time_point reference_time, int job_index) -> void
{
  // determine the input and grid index for this job
  auto iinput = job_index % inputs.size();
  auto igrid = job_index / inputs.size();

  // acquire the input grid
  /* this function ensures the entry for this input in input_buffers_ has been initialized.  after this call
   * it is always safe to read members of the structure even without holding the mutex since the input buffers
   * are read only after initialization during multi-threading (only need to lock to check 'inited' flag) */
  acquire_input_data(inputs, reference_time, iinput);

  // acquire the output buffer for this job - one thread at a time per output grid please
  auto lock = std::lock_guard<std::mutex>(output_buffers_[igrid].mut);

  // allocate and initialize our output buffer if needed
  if (!output_buffers_[igrid].inited)
  {
    if (int8_data_)
    {
      output_buffers_[igrid].datai.resize(grid_shape_);
      output_buffers_[igrid].datai.fill(-1);
    }
    else
    {
      output_buffers_[igrid].dataf.resize(grid_shape_);
      output_buffers_[igrid].dataf.fill(std::numeric_limits<float>::quiet_NaN());
    }
    output_buffers_[igrid].inited = true;
  }

  // if we managed to get the input, mosaic it in
  if (input_buffers_[iinput].input)
  {
    auto& iinfo = *input_buffers_[iinput].input;

    // setup limits assuming that entire input is embedded in output
    auto icol_beg = 0, irow_beg = 0;
    auto icol_end = iinfo.shape.x, irow_end = iinfo.shape.y;
    auto ocol_beg = iinfo.offset.x, orow_beg = iinfo.offset.y;

    // trim limits if input not fully embedded
    if (ocol_beg + icol_end > grid_shape_.x)
      icol_end -= (ocol_beg + icol_end - grid_shape_.x); // extended over right edge
    if (orow_beg + irow_end > grid_shape_.y)
      irow_end -= (orow_beg + irow_end - grid_shape_.y); // extended over bottom edge
    if (ocol_beg < 0)
      icol_beg = -ocol_beg, ocol_beg = 0; // extended over left edge
    if (orow_beg < 0)
      irow_beg = -orow_beg, orow_beg = 0; // extended over top edge

    // find the chunk of raw input data for this input grid
    auto cptr = &input_buffers_[iinput].data[input_buffers_[iinput].chunks[igrid]];
    auto csize = reinterpret_cast<mosaic_tile_chunk_header const*>(cptr)->chunk_size;
    auto cdata = span<char const>{cptr + sizeof(mosaic_tile_chunk_header), csize};

    // integrate the input into our mosaic
    if (int8_data_)
    {
      auto indata = array2<int8_t>{iinfo.shape};
      if (zlib_decompress(cdata, span{reinterpret_cast<char*>(indata.data()), indata.size_bytes()}) != indata.size_bytes())
        throw std::runtime_error{"Mosaic tile decompressed to incorrect size"};

      auto& outdata = output_buffers_[igrid].datai;
      for (auto iy = irow_beg, oy = orow_beg; iy < irow_end; ++iy, ++oy)
      {
        for (auto ix = icol_beg, ox = ocol_beg; ix < icol_end; ++ix, ++ox)
        {
          auto inval = indata[iy][ix];
          if (outdata[oy][ox] < 0)
            outdata[oy][ox] = inval;
          else if (inval >= 0)
            outdata[oy][ox] = method_ == mosaic_method::maximum
              ? std::max(inval, outdata[oy][ox])
              : std::min(inval, outdata[oy][ox]);
        }
      }
    }
    else
    {
      auto indata = array2<float>{iinfo.shape};
      if (zlib_decompress(cdata, span{reinterpret_cast<char*>(indata.data()), indata.size_bytes()}) != indata.size_bytes())
        throw std::runtime_error{"Mosaic tile decompressed to incorrect size"};

      auto& outdata = output_buffers_[igrid].dataf;
      for (auto iy = irow_beg, oy = orow_beg; iy < irow_end; ++iy, ++oy)
      {
        for (auto ix = icol_beg, ox = ocol_beg; ix < icol_end; ++ix, ++ox)
        {
          auto inval = indata[iy][ix];
          if (std::isnan(outdata[oy][ox]))
            outdata[oy][ox] = inval;
          else if (!std::isnan(inval))
            outdata[oy][ox] = method_ == mosaic_method::maximum
              ? std::max(inval, outdata[oy][ox])
              : std::min(inval, outdata[oy][ox]);
        }
      }
    }
  }

  // increment the number of inputs we've procecessed for this grid
  output_buffers_[igrid].count++;

  // if we have processed all inputs for this grid, finalize and output it
  if (output_buffers_[igrid].count >= int(inputs.size()))
  {
    // determine the output d0 and d1 indexes
    int d0, d1;
    if (labels_ && times_)
    {
      d0 = igrid / times_->size();
      d1 = igrid % times_->size();
    }
    else if (labels_ || times_)
    {
      d0 = igrid;
      d1 = -1;
    }
    else
    {
      d0 = -1;
      d1 = -1;
    }

    // output the product and free up our buffer
    if (int8_data_)
    {
      write(d0, d1, output_buffers_[igrid].datai, nullptr);
      output_buffers_[igrid].datai.resize(vec2i{0, 0});
    }
    else
    {
      write(d0, d1, output_buffers_[igrid].dataf, nullptr);
      output_buffers_[igrid].dataf.resize(vec2i{0, 0});
    }
  }
}

auto mosaic_product::acquire_input_data(mosaic_input_list const& inputs, time_point reference_time, int iinput) -> void
{
  auto lock = std::lock_guard<std::mutex>{input_buffers_[iinput].mut};

  // is this entry already initialized?
  if (input_buffers_[iinput].inited)
    return;

  // loop through the list of sites in order of priority until we obtain one
  for (auto& info : inputs[iinput])
  {
    auto path = format(input_path_, "param"_a = *user_parameters_, "domain"_a = info.domain, "reference"_a = reference_time);
    try
    {
      // read or download the tile
      input_buffers_[iinput].data = load_binary_blob(path, true);
      if (input_buffers_[iinput].data.size() == 0)
      {
        trace(trace_level::log, "Failed to acquire mosaic input {}: missing tile", path);
        continue;
      }

      // sanity check the header
      if (input_buffers_[iinput].data.size() < int(sizeof(mosaic_tile_header)))
        throw std::runtime_error{"Truncated file header"};
      auto hdr = reinterpret_cast<mosaic_tile_header const*>(input_buffers_[iinput].data.data());
      if (hdr->magic != mosaic_tile_file_magic)
        throw std::runtime_error{"Magic token mismatch"};
      if (hdr->version != mosaic_tile_file_version)
        throw std::runtime_error{"File version mismatch"};
      if (hdr->grid_count != grid_count_)
        throw std::runtime_error{"Grid count mismatch"};
      if (hdr->grid_shape != info.shape)
        throw std::runtime_error{"Grid shape mismatch"};
      if (hdr->int8_data != int8_data_)
        throw std::runtime_error{"Grid data type mismatch"};

      // allocate our chunk index table
      input_buffers_[iinput].chunks.resize(grid_count_);
      input_buffers_[iinput].chunks.fill(0);

      // scan the file and record the offset to each grid
      auto pos = int(sizeof(mosaic_tile_header));
      while (pos < input_buffers_[iinput].data.size())
      {
        if (pos + sizeof(mosaic_tile_chunk_header) > size_t(input_buffers_[iinput].data.size()))
          throw std::runtime_error{"Truncated chunk header"};
        auto chdr = reinterpret_cast<mosaic_tile_chunk_header const*>(&input_buffers_[iinput].data[pos]);
        if (chdr->grid_index < 0 || chdr->grid_index >= grid_count_)
          throw std::runtime_error{"Invalid chunk grid index"};
        if (input_buffers_[iinput].chunks[chdr->grid_index] != 0)
          throw std::runtime_error{"Duplicated chunk grid index"};
        if (pos + sizeof(mosaic_tile_chunk_header) + chdr->chunk_size > size_t(input_buffers_[iinput].data.size()))
          throw std::runtime_error{"Truncated chunk data"};

        input_buffers_[iinput].chunks[chdr->grid_index] = pos;
        pos += sizeof(mosaic_tile_chunk_header) + chdr->chunk_size;
      }
      if (pos != input_buffers_[iinput].data.size())
        throw std::runtime_error{"Unexpected file size (too long)"};

      // check that all chunks are accounted for
      for (auto pos : input_buffers_[iinput].chunks)
        if (pos == 0)
          throw std::runtime_error{"Missing chunk"};

      // all good - the presence of this pointer indicates valid data to the user
      input_buffers_[iinput].input = &info;

      // stop searching now that we've found a valid site to use for this input
      break;
    }
    catch (std::exception& err)
    {
      trace(trace_level::error, "Exception while acquiring mosaic input {}: {}", path, err);
    }
  }

  input_buffers_[iinput].inited = true;
}

mosaic_generator::mosaic_generator(configuration const& config)
  : user_parameters_{parse_metadata(config.optional("user_parameters", configuration{}), metadata{})}
{
  // load input list
  auto& conf_inputs = config["inputs"].array();
  inputs_.reserve(conf_inputs.size());
  for (auto& ci : conf_inputs)
  {
    vector<mosaic_input> list;
    if (ci.type() == configuration::node_type::object)
    {
      // top level site
      list.emplace_back(ci);
    }
    else if (ci.type() == configuration::node_type::array)
    {
      // priority list of sites (only first found is used)
      auto& conf_priority_list = ci.array();
      list.reserve(conf_priority_list.size());
      for (auto& csi : conf_priority_list)
        list.emplace_back(csi);
    }
    inputs_.emplace_back(std::move(list));
  }

  // load product list
  auto& conf_products = config["products"].array();
  products_.reserve(conf_products.size());
  for (auto& cp : conf_products)
    products_.push_back(std::make_unique<mosaic_product>(cp, user_parameters_));
}

auto mosaic_generator::generate_mosaics(time_point reference_time) -> void
{
  for (auto& prod : products_)
    prod->generate(inputs_, reference_time);
}
