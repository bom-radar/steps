/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2020 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "configuration.h"
#include "io_netcdf.h"
#include "mosaic.h"
#include "persistent_cache.h"
#include "traits.h"
#include "verification.h"

#include <aws/core/Aws.h>
#include <aws/core/utils/json/JsonSerializer.h>
#include <aws/s3/S3Client.h>
#include <aws/s3-crt/S3CrtClient.h>

namespace steps::aws
{
  /// RAII wrapper around Aws::InitAPI and Aws::ShutdownAPI calls
  struct aws_api
  {
    aws_api();
    aws_api(aws_api const&) = delete;
    aws_api(aws_api&&) = delete;
    auto operator=(aws_api const&) -> aws_api& = delete;
    auto operator=(aws_api&&) -> aws_api& = delete;
    ~aws_api();

    Aws::SDKOptions options_;
  };

  /// Singleton to hold AWS service clients
  class client
  {
  public:
    /// Access the AWS client singleton
    /** The setup of the AWS API and client is only performed on the first call of this function.  This ensures
     *  that we avoid the overhead entirely if no AWS functionality is needed by the user configuration. */
    static auto get() -> client&;

    auto& client_config() const { return client_config_; }
    auto& s3()                  { return s3_; }
    auto& s3_crt()              { return s3_crt_; }

  private:
    client();

  private:
    // some of these aws sdk identifiers are just too long...
    using ClientConfig = Aws::Client::ClientConfiguration;
    using S3Client = Aws::S3::S3Client;
    using S3CrtClient = Aws::S3Crt::S3CrtClient;

  private:
    aws_api           aws_api_;             // raii handle around api init and shutdown calls
    ClientConfig      client_config_;       // common configuration for all our AWS client objects (except S3CrtClient)
    S3Client          s3_;                  // original S3 client (better for small files)
    S3CrtClient       s3_crt_;              // crt based S3 client (better for large files)
  };

  /// Load a configuration file from an S3 bucket
  auto load_configuration_s3(string_view uri, bool optional) -> configuration;

  /// Save a configuration file to an S3 bucket
  auto save_configuration_s3(string_view uri, configuration const& data) -> void;

  /// Load a generic binary blob from an S3 bucket
  auto load_binary_blob_s3(string_view uri, bool optional) -> array1<char>;

  /// Load a netcdf file from an S3 bucket
  /** The netcdf I/O mutex must NOT be held when calling this function since it is locked internally while
   *  loading the file.  File is loaded into an in-memory NetCDF file. */
  auto load_netcdf_s3(string_view uri, bool optional) -> shared_ptr<nc::file const>;

  /// S3 object implementation of output_sream
  class output_stream_s3 : public output_stream
  {
  public:
    output_stream_s3(string uri);
    ~output_stream_s3();
    auto write(span<char const> data) -> void override;
    auto commit() -> void override;
    auto abort() -> void override;

  private:
    string uri_;
    std::shared_ptr<std::stringstream> body_;
  };

  /// Wrapper for netcdf input that retrieves content from the S3 bucket for the steps-aws platform
  class input_netcdf_s3 : public input_netcdf
  {
  public:
    input_netcdf_s3(configuration const& config, metadata const& user_parameters);

  protected:
    auto determine_path(time_point reference_time, int member, time_point time) const -> string override;
    auto open_file(string_view path) -> shared_ptr<nc::file const> override;

  private:
    metadata const* user_parameters_;
    string          path_format_;
  };

  /// Wrapper for netcdf output that stores completed files in S3 bucket
  class output_netcdf_s3 : public output_netcdf
  {
  public:
    output_netcdf_s3(configuration const& config, metadata const& user_parameters);

    auto finalize() -> void override;
    auto abort() -> void override;

  protected:
    auto create_file(time_point reference_time, int first_member) -> nc::file override;

  private:
    auto cleanup_temp_file() -> void;

  private:
    metadata const* user_parameters_;
    string          path_format_;

    string          tmp_path_;
    string          final_path_;
  };

  /// Output that writes mosaic tiles directly to S3
  class output_mosaic_tile_s3 : public output_mosaic_tile
  {
  public:
    output_mosaic_tile_s3(configuration const& config, metadata const& user_parameters);
    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto finalize() -> void override;
    auto abort() -> void override;

  protected:
    auto output_chunk(span<char const> chunk) -> void override;

  private:
    metadata const* user_parameters_;
    string          path_format_;

    std::mutex      mut_stream_;
    time_point      reference_time_;
    shared_ptr<std::stringstream> stream_;  // stream to buffer outptu for S3
  };

  /// Persistent cache manager that uses an S3 bucket as a backing store
  class persistent_cache_s3 : public persistent_cache
  {
  public:
    persistent_cache_s3(
          string_view stream_name
        , time_point reference_time
        , time_point bf_reference_time
        , string_view path
        , int compression
        , int precision_reduction);

    auto write(cache_key const& key, stream_state const& state) -> void override;
    auto read(cache_key const& key, stream_state& state) -> bool override;

  private:
    auto make_s3_path(cache_key const& key) const -> string;

  private:
    string  base_dir_;
    int     compression_;
    int     precision_reduction_;
  };

  /// Output manager for verification files
  class verification_io_netcdf_s3 : public verification_io_netcdf
  {
  public:
    verification_io_netcdf_s3(verification const& c, configuration const& config, metadata const& user_parameters);

    auto write(
          time_point from
        , time_point till
        , verification::score_store const& scores
        ) -> void override;

    auto read(
          string_view path
        , time_point& from
        , time_point& till
        , verification::score_store& scores
        ) -> bool override;

  private:
    auto determine_path(time_point from, time_point till) const -> string;

  private:
    metadata const* user_parameters_;
    string          path_format_;
  };
}
