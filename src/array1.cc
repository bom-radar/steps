/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "array1.h"
#include "unit_test.h"

using namespace steps;

// LCOV_EXCL_START
TEST_CASE("array1::<basics>")
{
  SUBCASE("array1()")
  {
    array1i v;
    CHECK(v.size() == 0);
    CHECK(v.data() == nullptr);
  }

  SUBCASE("array1(int)")
  {
    array1f v{10};
    CHECK(v.size() == 10);
    CHECK(v.data() != nullptr);
  }

  SUBCASE("array1(copy)")
  {
    array1i a{10};
    a.fill(1);
    array1i b{a};
    CHECK(a.size() == b.size());
    CHECK_ALL(a, b, lhs == rhs);
    CHECK(a.data() != b.data());
  }

  SUBCASE("array1(move)")
  {
    array1i a{10};
    a.fill(1);
    auto ptr = a.data();

    array1i b{std::move(a)};
    REQUIRE(a.data() == nullptr);
    REQUIRE(b.data() == ptr);
  }

  SUBCASE("operator=(copy)")
  {
    array1i a{10};
    a.fill(1);

    array1i b;
    SUBCASE("same") { b = array1i{10}; }
    SUBCASE("diff") { b = array1i{15}; }
    b.fill(2);

    b = a;
    CHECK(b.size() == 10);
    CHECK_ALL(a, b, lhs == rhs);
  }

  SUBCASE("operator=(move)")
  {
    array1i a{10}, b{10};

    a.fill(1);
    b.fill(2);

    auto bdata = b.data();
    CHECK(a[0] == 1);

    a = std::move(b);

    REQUIRE(a.data() == bdata);
    REQUIRE(b.data() == nullptr);
    CHECK(a[0] == 2);
  }

  SUBCASE("std::swap")
  {
    array1i a{5}, b{10};
    auto ptr_a = a.data();
    auto ptr_b = b.data();
    std::swap(a, b);
    CHECK(a.size() == 10);
    CHECK(a.data() == ptr_b);
    CHECK(b.size() == 5);
    CHECK(b.data() == ptr_a);
  }

  SUBCASE("resize")
  {
    array1i a{10};
    auto ptr = a.data();

    // no change
    a.resize(10);
    CHECK(a.size() == 10);
    CHECK(a.data() == ptr);

    // change
    a.resize(16);
    CHECK(a.size() == 16);
  }

  SUBCASE("operator[]")
  {
    array1i v{10};
    CHECK(&v[0] == v.data());
    CHECK(&v[1] == &v.data()[1]);

    auto const& vv = v;
    CHECK(&vv[0] == v.data());
    CHECK(&vv[1] == &v.data()[1]);
  }
}

TEST_CASE("array1::<in place operation>")
{
  array1i v{4};
  for (int i = 0; i < v.size(); ++i)
    v[i] = i;

  array1i v2{4};
  for (int i = 0; i < v2.size(); ++i)
    v2[i] = i * 2;

  SUBCASE("fill")
  {
    v.fill(7);
    CHECK(v[0] == 7);
    CHECK(v[1] == 7);
    CHECK(v[2] == 7);
    CHECK(v[3] == 7);
  }

  SUBCASE("copy")
  {
    v.copy(v2);
    CHECK(v[0] == 0);
    CHECK(v[1] == 2);
    CHECK(v[2] == 4);
    CHECK(v[3] == 6);
  }

  SUBCASE("replace")
  {
    v.replace(1, 2);
    CHECK(v[0] == 0);
    CHECK(v[1] == 2);
    CHECK(v[2] == 2);
    CHECK(v[3] == 3);
  }

  SUBCASE("multiply_add")
  {
    v.multiply_add(2, 1);
    CHECK(v[0] == 1);
    CHECK(v[1] == 3);
    CHECK(v[2] == 5);
    CHECK(v[3] == 7);
  }

  SUBCASE("replace_or_multiply_add")
  {
    v.replace_or_multiply_add(2, 4, 2, 1);
    CHECK(v[0] == 1);
    CHECK(v[1] == 3);
    CHECK(v[2] == 4);
    CHECK(v[3] == 7);
  }
}
// LCOV_EXCL_STOP

