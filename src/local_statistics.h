/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array2.h"
#include "format.h"

namespace steps
{
  /// Statistics which are collected on a per grid cell basis
  struct cell_stats
  {
    float mean;
    float stddev;
    float autocor;

    auto valid() const { return !std::isnan(mean); }

    static constexpr auto invalid()
    {
      return cell_stats{std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN()};
    }
  };
  inline auto operator+(cell_stats const& lhs, cell_stats const& rhs)
  {
    return cell_stats{lhs.mean + rhs.mean, lhs.stddev + rhs.stddev, lhs.autocor + rhs.autocor};
  }
  inline auto operator*(float lhs, cell_stats const& rhs)
  {
    return cell_stats{lhs * rhs.mean, lhs * rhs.stddev, lhs * rhs.autocor};
  }
  inline auto isnan(cell_stats val)
  {
    return std::isnan(val.mean) || std::isnan(val.stddev) || std::isnan(val.autocor);
  }
  template <>
  struct allow_simple_serialization<cell_stats>
  {
    static constexpr bool value = true;
  };

  /// Field of local statistics
  using local_stats = array2<cell_stats>;

  /// Determine local mean, standard deviations, and autocorrelations based on a given radius
  /** Also returns the scalar statistics for the whole field. */
  auto local_statistics(array2f const& lag1, array2f const& lag0, int radius, local_stats& out) -> cell_stats;

  /// Apply tweaks to diagnosed local statistics to account for inherent biases
  auto local_statistics_post(float stddev_scale, cell_stats scalar_stats, local_stats& out) -> void;
}

namespace fmt
{
  // By inheriting from float's formatter we don't have to implement the parse function ourselves
  template <>
  struct formatter<steps::cell_stats> : formatter<float>
  {
    template <typename FormatContext>
    auto format(steps::cell_stats const& v, FormatContext& ctx)
    {
      format_to(ctx.out(), "mean ");
      formatter<float>::format(v.mean, ctx);
      format_to(ctx.out(), " stddev ");
      formatter<float>::format(v.stddev, ctx);
      format_to(ctx.out(), " autocor ");
      formatter<float>::format(v.autocor, ctx);
      return ctx.out();
    }
  };
}
