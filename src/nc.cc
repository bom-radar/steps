/*------------------------------------------------------------------------------
 * Bureau of Meteorology Core Support Library
 *
 * Copyright 2014 Mark Curtis
 * Copyright 2016 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *----------------------------------------------------------------------------*/
#include "nc.h"
#include "array1.h"
#include "trace.h"
#include "unit_test.h"
#include <alloca.h>
#include <netcdf.h>
#if STEPS_NETCDF_IN_MEMORY_FILES
#include <netcdf_mem.h>
#endif
#include <cmath>
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <limits>
#include <sstream>

using namespace steps;
using namespace steps::nc;

// we rely on NC_NOERR being 0 to allow us to use the convenient "if (auto status = nc_xxx())" syntax
static_assert(NC_NOERR == 0, "expected NC_NOERR to be defined as 0!");

static auto nc_type_to_data_type(nc_type in) -> data_type
{
  switch (in)
  {
  case NC_BYTE:
    return data_type::i8;
  case NC_UBYTE:
    return data_type::u8;
  case NC_CHAR:
    return data_type::string; // data_type::c8;
  case NC_SHORT:
    return data_type::i16;
  case NC_USHORT:
    return data_type::u16;
  case NC_INT:
    return data_type::i32;
  case NC_UINT:
    return data_type::u32;
  case NC_INT64:
    return data_type::i64;
  case NC_UINT64:
    return data_type::u64;
  case NC_FLOAT:
    return data_type::f32;
  case NC_DOUBLE:
    return data_type::f64;
  case NC_STRING:
    return data_type::string;
  }
  throw std::logic_error{"unknown nc_type"};
}

static auto data_type_to_nc_type(data_type in) -> nc_type
{
  switch (in)
  {
  case data_type::i8:
    return NC_BYTE;
  case data_type::u8:
    return NC_UBYTE;
#if 0 // depreciated
  case data_type::c8:
    return NC_CHAR;
#endif
  case data_type::i16:
    return NC_SHORT;
  case data_type::u16:
    return NC_USHORT;
  case data_type::i32:
    return NC_INT;
  case data_type::u32:
    return NC_UINT;
  case data_type::i64:
    return NC_INT64;
  case data_type::u64:
    return NC_UINT64;
  case data_type::f32:
    return NC_FLOAT;
  case data_type::f64:
    return NC_DOUBLE;
  case data_type::string:
    return NC_STRING;
  }
  throw std::logic_error{"unknown data_type"};
}

static auto get_path(int ncid) -> string
{
  // determine string length
  size_t len;
  if (nc_inq_path(ncid, &len, nullptr))
    return "unknown";

  // direct modification of string contents like this is safe since C++11
  string ret(len, '\0');
  if (nc_inq_path(ncid, nullptr, &ret[0]))
    return "unknown";

  return ret;
}

static auto get_location(object const* at) -> string
{
  return at->parent() ? get_location(at->parent()).append("/").append(at->name()) : at->name();
}

error::error(dimension const* at, string const& desc, char const* api, int status)
{
  api = api ? api : "";
  description_ = format(
        "netcdf error:\n  path: {}\n  location: {}/{}\n  operation: {}\n  api: {}\n  ncerr: {}"
      , get_path(at->parent()->ncid())
      , get_location(at->parent()), at->name()
      , desc
      , api
      , nc_strerror(status));
}

error::error(object const* at, string const& desc, char const* api, int status)
{
  api = api ? api : "";
  description_ = format(
        "netcdf error:\n  path: {}\n  location: {}\n  operation: {}\n  api: {}\n  ncerr: {}"
      , get_path(at->ncid())
      , get_location(at)
      , desc
      , api
      , nc_strerror(status));
}

error::error(string const& path, string const& desc, char const* api, int status)
{
  api = api ? api : "";
  description_ = format(
        "netcdf error:\n  path: {}\n  operation: {}\n  api: {}\n  ncerr: {}"
      , path
      , desc
      , api
      , nc_strerror(status));
}

auto error::what() const noexcept -> char const*
{
  return description_.c_str();
}

dimension::dimension(group* parent, string name, int size)
  : parent_{parent}
  , name_{std::move(name)}
  , unlimited_{size == ::steps::nc::unlimited}
  , size_{unlimited_ ? 0 : size}
{
  if (auto status = nc_def_dim(parent_->ncid(), name_.c_str(), unlimited_ ? NC_UNLIMITED : size_, &dimid_))
    throw error{this, format("add dimension {}", name_), "nc_def_dim", status};
}

dimension::dimension(group* parent, int dimid)
  : parent_{parent}
  , dimid_{dimid}
  , unlimited_{false}
{
  // read name and size
  size_t size;
  char buf[NC_MAX_NAME + 1];
  if (auto status = nc_inq_dim(parent_->ncid(), dimid_, buf, &size))
    throw error{this, "read dimension", "nc_inq_dim", status};

  name_.assign(buf);
  size_ = size;

  // determine whether it is unlimited
  int count;
  if (auto status = nc_inq_unlimdims(parent_->ncid(), &count, nullptr))
    throw error{this, "read dimension", "nc_inq_unlimdim", status};
  if (count != 0)
  {
    array1i ids(count);
    if (auto status = nc_inq_unlimdims(parent_->ncid(), nullptr, ids.data()))
      throw error{this, "read dimension", "nc_inq_unlimdim", status};

    for (auto id : ids)
      if (id == dimid_)
        unlimited_ = true;
  }
}

auto dimension::rename(string const& name) -> void
{
  if (auto status = nc_rename_dim(parent_->ncid(), dimid_, name.c_str()))
    throw error{this, format("rename to '{}'", name), "nc_rename_dim", status};
  name_.assign(name);
}

object::object(group* parent, string name, int ncid, int varid)
  : parent_{parent}
  , name_{std::move(name)}
  , ncid_{ncid}
  , varid_{varid}
{
  /* NOTE: do not do anything in here!  Depending on which group/variable constructor we are in the name, ncid
   * and varid may not actually be set to their final values yet. */
}

auto object::att_size() const -> int
{
  int count;
  if (varid_ == NC_GLOBAL)
  {
    if (auto status = nc_inq_natts(ncid_, &count))
      throw error{this, "get attribute count", "nc_inq_natts", status};
  }
  else
  {
    if (auto status = nc_inq_varnatts(ncid_, varid_, &count))
      throw error{this, "get attribute count", "nc_inq_varnatts", status};
  }
  return count;
}

auto object::att_name(int i) const -> string
{
  char buf[NC_MAX_NAME + 1];
  if (auto status = nc_inq_attname(ncid_, varid_, i, buf))
    throw error{this, "get attribute name", "nc_inq_attname", status};
  return buf;
}

auto object::att_exists(char const* name) const -> bool
{
  int val;
  auto status = nc_inq_attid(ncid_, varid_, name, &val);
  if (status == NC_NOERR)
    return true;
  if (status == NC_ENOTATT)
    return false;
  throw error{this, format("attribute '{}' exists", name), "nc_inq_attid", status};
}

auto object::att_type(char const* name) const -> data_type
{
  nc_type type;
  if (auto status = nc_inq_atttype(ncid_, varid_, name, &type))
    throw error{this, format("get attribute '{}' type", name), "nc_inq_atttype", status};
  return nc_type_to_data_type(type);
}

auto object::att_size(char const* name) const -> int
{
  size_t len;
  if (auto status = nc_inq_attlen(ncid_, varid_, name, &len))
    throw error{this, format("get attribute '{}' size", name), "nc_inq_attlen", status};
  return len;
}

auto object::att_rename(char const* name, char const* new_name) -> void
{
  if (auto status = nc_rename_att(ncid_, varid_, name, new_name))
    throw error{this, format("rename attribute '{}' to '{}'", name, new_name), "nc_rename_att", status};
}

auto object::att_erase(char const* name) -> void
{
  if (auto status = nc_del_att(ncid_, varid_, name))
    throw error{this, format("erase attribute '{}'", name), "nc_del_att", status};
}

auto object::att_copy(object const& from, char const* name) -> void
{
  if (auto status = nc_copy_att(from.ncid_, from.varid_, name, ncid_, varid_))
    throw error{this, format("copy attribute '{}'", name), "nc_copy_att", status};
}

auto object::att_copy(object const& from, bool overwrite) -> void
{
  for (auto i = 0; i < from.att_size(); ++i)
  {
    auto name = from.att_name(i);
    if (att_exists(name))
    {
      if (overwrite)
        att_erase(name);
      else
        continue;
    }
    att_copy(from, name);
  }
}

auto object::att_set(char const* name, signed char val) -> void
{
  if (auto status = nc_put_att_schar(ncid_, varid_, name, NC_BYTE, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_schar", status};
}

auto object::att_set(char const* name, unsigned char val) -> void
{
  if (auto status = nc_put_att_uchar(ncid_, varid_, name, NC_UBYTE, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_uchar", status};
}

auto object::att_set(char const* name, short val) -> void
{
  if (auto status = nc_put_att_short(ncid_, varid_, name, NC_SHORT, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_short", status};
}

auto object::att_set(char const* name, unsigned short val) -> void
{
  if (auto status = nc_put_att_ushort(ncid_, varid_, name, NC_USHORT, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_ushort", status};
}

auto object::att_set(char const* name, int val) -> void
{
  if (auto status = nc_put_att_int(ncid_, varid_, name, NC_INT, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_int", status};
}

auto object::att_set(char const* name, unsigned int val) -> void
{
  if (auto status = nc_put_att_uint(ncid_, varid_, name, NC_UINT, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_uint", status};
}

auto object::att_set(char const* name, long val) -> void
{
  if (auto status = nc_put_att_long(ncid_, varid_, name, NC_LONG, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_long", status};
}

auto object::att_set(char const* name, unsigned long val) -> void
{
  // see variable::read() of unsigned long for rational behind this
  if (sizeof(unsigned long) == sizeof(unsigned int))
  {
    if (auto status = nc_put_att_uint(ncid_, varid_, name, NC_UINT, 1, reinterpret_cast<unsigned int const*>(&val)))
      throw error{this, format("set attribute '{}'", name), "nc_put_att_ulong", status};
  }
  else
  {
    if (auto status = nc_put_att_ulonglong(ncid_, varid_, name, NC_UINT64, 1, reinterpret_cast<unsigned long long const*>(&val)))
      throw error{this, format("set attribute '{}'", name), "nc_put_att_ulong", status};
  }
}

auto object::att_set(char const* name, long long val) -> void
{
  if (auto status = nc_put_att_longlong(ncid_, varid_, name, NC_INT64, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_longlong", status};
}

auto object::att_set(char const* name, unsigned long long val) -> void
{
  if (auto status = nc_put_att_ulonglong(ncid_, varid_, name, NC_UINT64, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_ulonglong", status};
}

auto object::att_set(char const* name, float val) -> void
{
  if (auto status = nc_put_att_float(ncid_, varid_, name, NC_FLOAT, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_float", status};
}

auto object::att_set(char const* name, double val) -> void
{
  if (auto status = nc_put_att_double(ncid_, varid_, name, NC_DOUBLE, 1, &val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_double", status};
}

auto object::att_set(char const* name, char const* val) -> void
{
  if (auto status = nc_put_att_text(ncid_, varid_, name, strlen(val), val))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_text", status};
}

auto object::att_set(char const* name, string const& val) -> void
{
  if (auto status = nc_put_att_text(ncid_, varid_, name, val.size(), val.c_str()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_text", status};
}

auto object::att_set(char const* name, span<signed char const> val) -> void
{
  if (auto status = nc_put_att_schar(ncid_, varid_, name, NC_BYTE, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_schar", status};
}

auto object::att_set(char const* name, span<unsigned char const> val) -> void
{
  if (auto status = nc_put_att_uchar(ncid_, varid_, name, NC_UBYTE, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_uchar", status};
}

auto object::att_set(char const* name, span<short const> val) -> void
{
  if (auto status = nc_put_att_short(ncid_, varid_, name, NC_SHORT, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_short", status};
}

auto object::att_set(char const* name, span<unsigned short const> val) -> void
{
  if (auto status = nc_put_att_ushort(ncid_, varid_, name, NC_USHORT, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_ushort", status};
}

auto object::att_set(char const* name, span<int const> val) -> void
{
  if (auto status = nc_put_att_int(ncid_, varid_, name, NC_INT, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_int", status};
}

auto object::att_set(char const* name, span<unsigned int const> val) -> void
{
  if (auto status = nc_put_att_uint(ncid_, varid_, name, NC_UINT, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_uint", status};
}

auto object::att_set(char const* name, span<long const> val) -> void
{
  if (auto status = nc_put_att_long(ncid_, varid_, name, NC_LONG, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_long", status};
}

auto object::att_set(char const* name, span<unsigned long const> val) -> void
{
  // see variable::read() of unsigned long for rational behind this
  if (sizeof(unsigned long) == sizeof(unsigned int))
  {
    if (auto status = nc_put_att_uint(ncid_, varid_, name, NC_UINT, val.size(), reinterpret_cast<unsigned int const*>(val.data())))
      throw error{this, format("set attribute '{}'", name), "nc_put_att_ulong", status};
  }
  else
  {
    if (auto status = nc_put_att_ulonglong(ncid_, varid_, name, NC_UINT64, val.size(), reinterpret_cast<unsigned long long const*>(val.data())))
      throw error{this, format("set attribute '{}'", name), "nc_put_att_ulong", status};
  }
}

auto object::att_set(char const* name, span<long long const> val) -> void
{
  if (auto status = nc_put_att_longlong(ncid_, varid_, name, NC_INT64, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_longlong", status};
}

auto object::att_set(char const* name, span<unsigned long long const> val) -> void
{
  if (auto status = nc_put_att_ulonglong(ncid_, varid_, name, NC_UINT64, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_ulonglong", status};
}

auto object::att_set(char const* name, span<float const> val) -> void
{
  if (auto status = nc_put_att_float(ncid_, varid_, name, NC_FLOAT, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_float", status};
}

auto object::att_set(char const* name, span<double const> val) -> void
{
  if (auto status = nc_put_att_double(ncid_, varid_, name, NC_DOUBLE, val.size(), val.data()))
    throw error{this, format("set attribute '{}'", name), "nc_put_att_double", status};
}

auto object::att_get(char const* name, signed char& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_schar(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_schar", status};
}

auto object::att_get(char const* name, unsigned char& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_uchar(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_uchar", status};
}

auto object::att_get(char const* name, short& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_short(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_short", status};
}

auto object::att_get(char const* name, unsigned short& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_ushort(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_ushort", status};
}

auto object::att_get(char const* name, int& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_int(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_int", status};
}

auto object::att_get(char const* name, unsigned int& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_uint(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_uint", status};
}

auto object::att_get(char const* name, long& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_long(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_long", status};
}

auto object::att_get(char const* name, unsigned long& val) const -> void
{
  // see variable::read() of unsigned long for rational behind this
  if (sizeof(unsigned long) == sizeof(unsigned int))
  {
    if (att_size(name) > 1)
      throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
    if (auto status = nc_get_att_uint(ncid_, varid_, name, reinterpret_cast<unsigned int*>(&val)))
      throw error{this, format("get attribute '{}'", name), "nc_get_att_ulong", status};
  }
  else
  {
    if (att_size(name) > 1)
      throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
    if (auto status = nc_get_att_ulonglong(ncid_, varid_, name, reinterpret_cast<unsigned long long*>(&val)))
      throw error{this, format("get attribute '{}'", name), "nc_get_att_ulong", status};
  }
}

auto object::att_get(char const* name, long long& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_longlong(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_longlong", status};
}

auto object::att_get(char const* name, unsigned long long& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_ulonglong(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_ulonglong", status};
}

auto object::att_get(char const* name, float& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_float(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_float", status};
}

auto object::att_get(char const* name, double& val) const -> void
{
  if (att_size(name) > 1)
    throw error{this, format("get attribute '{}'", name), "attribute is not a scalar"};
  if (auto status = nc_get_att_double(ncid_, varid_, name, &val))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_double", status};
}

auto object::att_get(char const* name, string& val) const -> void
{
  // direct modification of string contents like this is safe since C++11
  val.resize(att_size(name));
  if (auto status = nc_get_att_text(ncid_, varid_, name, &val[0]))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_schar", status};
}

auto object::att_get(char const* name, span<signed char> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_schar(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_schar", status};
}

auto object::att_get(char const* name, span<unsigned char> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_uchar(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_uchar", status};
}

auto object::att_get(char const* name, span<short> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_short(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_short", status};
}

auto object::att_get(char const* name, span<unsigned short> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_ushort(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_ushort", status};
}

auto object::att_get(char const* name, span<int> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_int(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_int", status};
}

auto object::att_get(char const* name, span<unsigned int> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_uint(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_uint", status};
}

auto object::att_get(char const* name, span<long> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_long(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_long", status};
}

auto object::att_get(char const* name, span<unsigned long> val) const -> void
{
  // see variable::read() of unsigned long for rational behind this
  if (sizeof(unsigned long) == sizeof(unsigned int))
  {
    if (att_size(name) != val.size())
      throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
    if (auto status = nc_get_att_uint(ncid_, varid_, name, reinterpret_cast<unsigned int*>(val.data())))
      throw error{this, format("get attribute '{}'", name), "nc_get_att_ulong", status};
  }
  else
  {
    if (att_size(name) != val.size())
      throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
    if (auto status = nc_get_att_ulonglong(ncid_, varid_, name, reinterpret_cast<unsigned long long*>(val.data())))
      throw error{this, format("get attribute '{}'", name), "nc_get_att_ulong", status};
  }
}

auto object::att_get(char const* name, span<long long> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_longlong(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_longlong", status};
}

auto object::att_get(char const* name, span<unsigned long long> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_ulonglong(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_ulonglong", status};
}

auto object::att_get(char const* name, span<float> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_float(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_float", status};
}

auto object::att_get(char const* name, span<double> val) const -> void
{
  if (att_size(name) != val.size())
    throw error{this, format("get attribute '{}'", name), "attribute size mismatch"};
  if (auto status = nc_get_att_double(ncid_, varid_, name, val.data()))
    throw error{this, format("get attribute '{}'", name), "nc_get_att_double", status};
}

variable::variable(
      group* parent
    , string name
    , data_type type
    , span<dimension* const> dims
    , span<size_t const> chunk_size
    , int compression_level)
  : object{parent, std::move(name), parent->ncid(), 0} // varid is temporary
  , type_{type}
  , dimensions_(dims.size())
{
  // sanity check requested rank
  if (dims.size() > max_var_dims)
    throw error{this, "create variable", format("rank {} exceeds API limit of {}", dims.size(), max_var_dims).c_str()};

  // lookup dimension ids
  std::array<int, max_var_dims> dim_ids;
  for (auto i = 0; i < dims.size(); ++i)
  {
    // check the dimension is visible to this group
    auto p = parent_;
    while (p && p != dims[i]->parent())
      p = p->parent();
    if (!p)
      throw error{this, "create variable", format("dimension {} not visible from here", dims[i]->name()).c_str()};

    dimensions_[i] = dims[i];
    dim_ids[i] = dims[i]->dimid();
  }

  // create the actual variable
  if (auto status = nc_def_var(ncid_, name_.c_str(), data_type_to_nc_type(type_), dimensions_.size(), dim_ids.data(), &varid_))
    throw error{this, "create variable", "nc_def_var", status};

  // setup chunking and compression
  if (chunk_size.size() > 0)
  {
    if (chunk_size.size() != dims.size())
      throw error{this, "create variable", format("chunk rank {} mismatch with variable rank {}", chunk_size.size(), dims.size()).c_str()};

    if (auto status = nc_def_var_chunking(ncid_, varid_, NC_CHUNKED, chunk_size.data()))
      throw error{this, "create variable", "nc_def_var_chunking", status};

    if (compression_level != uncompressed)
    {
      if (auto status = nc_def_var_deflate(ncid_, varid_, 0, 1, compression_level))
        throw error{this, "create variable", "nc_def_var_deflate", status};
    }
  }
}

variable::variable(group* parent, variable const& rhs)
  : object{parent, rhs.name_, parent->ncid(), 0} // varid is temporary
  , type_{rhs.type_}
  , dimensions_{rhs.dimensions_.size()}
{
  // sanity check that the variable does not already exist
  for (auto& v : parent_->variables())
    if (v.name() == rhs.name())
      throw error{this, "copy variable", "variable with this name already exists"};

  // collect dimension pointers - copying as necessary
  for (auto i = 0; i < dimensions_.size(); ++i)
  {
    if (auto d = parent_->find_dimension(rhs.dimensions_[i]->name()))
    {
      if (d->size() != rhs.dimensions_[i]->size())
        throw error{this, "copy variable", format("size mismatch on existing dimension '{}'", d->name()).c_str()};
      dimensions_[i] = d;
    }
    else
    {
      dimensions_[i] = &parent_->create_dimension(rhs.dimensions_[i]->name(), rhs.dimensions_[i]->size());
    }
  }

  // copy the variable itself (only works properly from NetCDF 4.3.0 onwards)
  if (auto status = nc_copy_var(rhs.ncid(), rhs.varid(), ncid_))
    throw error{this, "copy variable", "nc_copy_var", status};

  // lookup our variable id - why is it not returned by nc_copy_var?!?
  if (auto status = nc_inq_varid(ncid_, name_.c_str(), &varid_))
    throw error{this, "copy variable", "nc_inq_varid", status};
}

variable::variable(group* parent, int varid)
  : object{parent, {}, parent->ncid(), varid} // name is temporary
{
  // read the name first to assist with any error messaging
  char buf[NC_MAX_NAME + 1];
  if (auto status = nc_inq_varname(ncid_, varid_, buf))
    throw error{this, "load variable", "nc_inq_varname", status};
  name_.assign(buf);

  // sanity check the rank of the variable
  int ndims;
  if (auto status = nc_inq_varndims(ncid_, varid_, &ndims))
    throw error{this, "load variable", "nc_inq_varndims", status};
  if (ndims > max_var_dims)
    throw error{this, "load variable", format("rank {} exceeds API limit of {}", ndims, max_var_dims).c_str()};

  // read the dimensions and data type
  nc_type type;
  std::array<int, max_var_dims> dim_ids;
  if (auto status = nc_inq_var(ncid_, varid_, nullptr, &type, nullptr, dim_ids.data(), nullptr))
    throw error{this, "load variable", "nc_inq_var", status};

  // translate the data type
  type_ = nc_type_to_data_type(type);

  // translate the dimension ids
  dimensions_ = array1<dimension*>{ndims};
  for (int i = 0; i < ndims; ++i)
  {
    dimension* dd = nullptr;
    for (auto p = parent_; p != nullptr && dd == nullptr; p = p->parent())
    {
      for (auto d = p->dimensions().begin(); d != p->dimensions().end(); ++d)
      {
        if (d->dimid() == dim_ids[i])
        {
          dd = &(*d);
          break;
        }
      }
    }

    if (dd == nullptr)
      throw error{this, "load variable", "failed to map dimension objects for variable"};
    dimensions_[i] = dd;
  }
}

auto variable::rename(string const& name) -> void
{
  if (auto status = nc_rename_var(ncid_, varid_, name.c_str()))
    throw error{this, "rename variable", "nc_rename_var", status};
  name_.assign(name);
}

auto variable::setup_hyperslab(
      int elements
    , span<int const> start
    , span<int const> count
    , hs_indexes& hs_start
    , hs_indexes& hs_count
    ) const -> void
{
  // sanity check rank of user inputs
  if (start.size() > dimensions_.size())
    throw error{this, "start values exceed variable rank"};
  if (count.size() > start.size())
    throw error{this, "more count values provided than start values"};

  // convert input indexes to hyperslab start and count values
  int total_count = 1;
  for (auto i = 0; i < start.size(); ++i)
  {
    // allow special 'all' start index as shorthand for start=0, count=dimension size
    if (start[i] == all)
    {
      // enforce consistency of count value if provided
      if (i < count.size() && count[i] != 0 && count[i] != dimensions_[i]->size())
        throw error{this, format(
                  "start[{}] is 'all' but count[{}] is not dimension '{}' size ({} != {})"
                , i, i
                , dimensions_[i]->name()
                , count[i]
                , dimensions_[i]->size()).c_str()};

      // set the hyperslab start/count for this dimension
      hs_start[i] = 0;
      hs_count[i] = dimensions_[i]->size();
    }
    else
    {
      // check for start index overflow
      if (start[i] >= dimensions_[i]->size())
        throw error{this, format(
                  "start[{}] overflows dimension {} ({} > {})"
                , i
                , dimensions_[i]->name()
                , start[i]
                , dimensions_[i]->size()).c_str()};

      // user supplied count?
      if (i < count.size())
      {
        // check for end index overflow
        if (start[i] + count[i] > dimensions_[i]->size())
          throw error{this, format(
                    "start[{}] + count[{}] overflows dimension '{}' ({} + {} > {})"
                  , i, i
                  , dimensions_[i]->name()
                  , start[i]
                  , count[i]
                  , dimensions_[i]->size()).c_str()};

        hs_start[i] = start[i];
        hs_count[i] = count[i];
      }
      else
      {
        hs_start[i] = start[i];
        hs_count[i] = 1;
      }
    }

    // keep a running tally of the total number of elements to read/write
    total_count *= hs_count[i];
  }

  // remaining starts are treated as 'all'
  for (auto i = start.size(); i < dimensions_.size(); ++i)
  {
    hs_start[i] = 0;
    hs_count[i] = dimensions_[i]->size();
    total_count *= hs_count[i];
  }

  // sanity check the size of the user's data array
  if (total_count != elements)
    throw error{this, format(
              "mismatch between provided data size {} and requested read/write size {}"
            , elements
            , total_count).c_str()};
}

auto variable::read(span<signed char> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_schar(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_schar", status};
}

auto variable::read(span<unsigned char> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_uchar(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_uchar", status};
}

auto variable::read(span<short> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_short(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_short", status};
}

auto variable::read(span<unsigned short> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_ushort(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_ushort", status};
}

auto variable::read(span<int> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_int(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_int", status};
}

auto variable::read(span<unsigned int> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_uint(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_uint", status};
}

auto variable::read(span<long> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_long(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_long", status};
}

auto variable::read(span<unsigned long> data, span<int const> start, span<int const> count) const -> void
{
  /* NetCDF does not provide a native nc_get_vara_ulong function, so we must emulate it here by discovering
   * which of the interger types (unsigned int or unsigned long long) has the same size as long.
   *
   * I believe the lack of this function is a flaw in the NetCDF API, however it is claimed to be a 'feature'
   * by NetCDF developers.  NetCDF defines 16, 32 and 64 bit integers as NC_SHORT, NC_INT and NC_INT64
   * respectively.  The API also defines NC_LONG as an alias for NC_INT.  That is fine, although in hindsight
   * they probably should have just had NC_INT16, NC_INT32 and NC_INT64.
   *
   * The problem is that the API developers are interpreting the fact that NC_INT and NC_LONG are both 32 bit
   * as meaning that you don't need functions to access the data as the native long type.  This is a bad
   * assumption.  These functions convert the data anyway, and if the programmer is free to read the data as
   * all the other types there is no justifiable reason to exclude only this one.
   *
   * If we ever convice the NetCDF folks to add this functionality back in then you can implement this
   * function the natural way as for all the other types. */
  static_assert(
        sizeof(unsigned long) == sizeof(unsigned int) || sizeof(unsigned long) == sizeof(unsigned long long)
      , "long must be same size as int or long long to work around NetCDF API deficiency!");
  if (sizeof(unsigned long) == sizeof(unsigned int))
  {
    hs_indexes hs_start, hs_count;
    setup_hyperslab(data.size(), start, count, hs_start, hs_count);
    if (auto status = nc_get_vara_uint(ncid(), varid(), hs_start.data(), hs_count.data(), reinterpret_cast<unsigned int*>(data.data())))
      throw error{this, "read variable", "nc_get_vara_ulong", status};
  }
  else
  {
    hs_indexes hs_start, hs_count;
    setup_hyperslab(data.size(), start, count, hs_start, hs_count);
    if (auto status = nc_get_vara_ulonglong(ncid(), varid(), hs_start.data(), hs_count.data(), reinterpret_cast<unsigned long long*>(data.data())))
      throw error{this, "read variable", "nc_get_vara_ulong", status};
  }
}

auto variable::read(span<long long> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_longlong(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_longlong", status};
}

auto variable::read(span<unsigned long long> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_ulonglong(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_ulonglong", status};
}

auto variable::read(span<float> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_float(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_float", status};
}

auto variable::read(span<double> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_get_vara_double(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "read variable", "nc_get_vara_double", status};
}

auto variable::read(span<string> data, span<int const> start, span<int const> count) const -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);

  // create a buffer of c-string pointers to receive the strings from netcdf
  array1<char*> ptrs(data.size());

  if (auto status = nc_get_vara_string(ncid(), varid(), hs_start.data(), hs_count.data(), ptrs.data()))
    throw error{this, "read variable", "nc_get_vara_string", status};

  // copy c-strings into real strings
  try
  {
    for (auto i = 0; i < data.size(); ++i)
      data[i].assign(ptrs[i]);
    nc_free_string(data.size(), ptrs.data());
  }
  catch (...)
  {
    nc_free_string(data.size(), ptrs.data());
    throw;
  }
}

auto variable::write(span<signed char const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_schar(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_schar", status};
}

auto variable::write(span<unsigned char const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_uchar(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_uchar", status};
}

auto variable::write(span<short const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_short(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_short", status};
}

auto variable::write(span<unsigned short const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_ushort(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_ushort", status};
}

auto variable::write(span<int const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_int(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_int", status};
}

auto variable::write(span<unsigned int const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_uint(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_uint", status};
}

auto variable::write(span<long const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_long(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_long", status};
}

auto variable::write(span<unsigned long const> data, span<int const> start, span<int const> count) -> void
{
  // see variable::read() of unsigned long for rational behind this
  if (sizeof(unsigned long) == sizeof(unsigned int))
  {
    hs_indexes hs_start, hs_count;
    setup_hyperslab(data.size(), start, count, hs_start, hs_count);
    if (auto status = nc_put_vara_uint(ncid(), varid(), hs_start.data(), hs_count.data(), reinterpret_cast<unsigned int const*>(data.data())))
      throw error{this, "write variable", "nc_put_vara_ulong", status};
  }
  else
  {
    hs_indexes hs_start, hs_count;
    setup_hyperslab(data.size(), start, count, hs_start, hs_count);
    if (auto status = nc_put_vara_ulonglong(ncid(), varid(), hs_start.data(), hs_count.data(), reinterpret_cast<unsigned long long const*>(data.data())))
      throw error{this, "write variable", "nc_put_vara_ulong", status};
  }
}

auto variable::write(span<long long const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_longlong(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_longlong", status};
}

auto variable::write(span<unsigned long long const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_ulonglong(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_ulonglong", status};
}

auto variable::write(span<float const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_float(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_float", status};
}

auto variable::write(span<double const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);
  if (auto status = nc_put_vara_double(ncid(), varid(), hs_start.data(), hs_count.data(), data.data()))
    throw error{this, "write variable", "nc_put_vara_double", status};
}

auto variable::write(span<string const> data, span<int const> start, span<int const> count) -> void
{
  hs_indexes hs_start, hs_count;
  setup_hyperslab(data.size(), start, count, hs_start, hs_count);

  // create a buffer of c-string pointers to pass to netcdf
  array1<char const*> ptrs(data.size());
  for (auto i = 0; i < data.size(); ++i)
    ptrs[i] = data[i].c_str();

  if (auto status = nc_put_vara_string(ncid(), varid(), hs_start.data(), hs_count.data(), ptrs.data()))
    throw error{this, "write variable", "nc_put_vara_string", status};
}

group::group(group* parent, string name)
  : object{parent, std::move(name), 0, NC_GLOBAL}
{
  if (auto status = nc_def_grp(parent_->ncid(), name_.c_str(), &ncid_))
    throw error{this, "create group", "nc_def_grp", status};
}

/* as much as I hate the idiom, we use a delayed constructor and init() function for the group class.
 * this allows us to safely make file a subclass of group since file needs to open and store the top
 * level handle before initializing the group. */
group::group(group* parent)
  : object{parent, {}, 0, NC_GLOBAL} // name and ncid are temporary
{ }

group::group(group&& rhs)
  : object{std::move(rhs)}
  , groups_{std::move(rhs.groups_)}
  , dimensions_{std::move(rhs.dimensions_)}
  , variables_{std::move(rhs.variables_)}
{
  // fix parent_ in all our children
  for (auto& g : groups_)
    g.parent_ = this;
  for (auto& d : dimensions_)
    d.parent_ = this;
  for (auto& v : variables_)
    v.parent_ = this;
}

auto group::operator=(group&& rhs) ->group&
{
  object::operator=(std::move(rhs));
  groups_ = std::move(rhs.groups_);
  dimensions_ = std::move(rhs.dimensions_);
  variables_ = std::move(rhs.variables_);
  // fix parent_ in all our children
  for (auto& g : groups_)
    g.parent_ = this;
  for (auto& d : dimensions_)
    d.parent_ = this;
  for (auto& v : variables_)
    v.parent_ = this;
  return *this;
}

auto group::init(int ncid) -> void
{
  // store our actual ncid
  ncid_ = ncid;

  // get the group name
  // important to do first so that errors thrown while constructing children can find our name
  char buf[NC_MAX_NAME + 1];
  if (auto status = nc_inq_grpname(ncid_, buf))
    throw error{this, "load group", "nc_inq_grpname", status};
  name_.assign(buf);

  // get the total number of dimensions visible from this group
  int ndims;
  if (auto status = nc_inq_ndims(ncid_, &ndims))
    throw error{this, "load group", "nc_inq_ndims", status};

  // get the ids of each dimension that is defined in this group
  // this may reduce the value of ndims retrieved above
  array1i dim_ids{ndims};
  if (auto status = nc_inq_dimids(ncid_, &ndims, dim_ids.data(), 0))
    throw error{this, "load group", "nc_inq_dimids", status};

  // initialize the dimension store
  for (int i = 0; i < ndims; ++i)
    dimensions_.push_back({this, dim_ids[i]});

  // get the number of variables in this group
  int nvars;
  if (auto status = nc_inq_nvars(ncid_, &nvars))
    throw error{this, "load group", "nc_inq_nvars", status};

  // load each variable
  for (int i = 0; i < nvars; ++i)
    variables_.push_back({this, i});

  // get the number of subgroups in this group
  int ngrps;
  if (auto status = nc_inq_grps(ncid_, &ngrps, nullptr))
    throw error{this, "load group", "nc_inq_grps", status};

  // get the ids of each subgroup
  array1i grp_ids{ngrps};
  if (auto status = nc_inq_grps(ncid_, &ngrps, grp_ids.data()))
    throw error{this, "load group", "nc_inq_grps", status};

  // initialize the group store
  for (int i = 0; i < ngrps; ++i)
  {
    groups_.push_back(this);
    groups_.back().init(grp_ids[i]);
  }
}

auto group::rename(string const& name) -> void
{
  if (auto status = nc_rename_grp(ncid_, name.c_str()))
    throw error{this, "rename group", "nc_rename_grp", status};
  name_.assign(name);
}

auto group::create_group(string name) -> group&
{
  return *groups_.insert(groups_.end(), {this, std::move(name)});
}

auto group::create_dimension(string name, int size) -> dimension&
{
  return *dimensions_.insert(dimensions_.end(), {this, std::move(name), size});
}

auto group::find_dimension(string_view name) const -> dimension const*
{
  for (auto& d : dimensions_)
    if (d.name() == name)
      return &d;
  return parent_ ? parent_->find_dimension(name) : nullptr;
}

auto group::lookup_dimension(string_view path) const -> dimension const&
{
  // skip leading slashes
  path.remove_prefix(std::min(path.find_first_not_of("/"), path.size()));

  // find end of first token
  auto pos = path.find('/');

  // is this an intermediate node?
  if (pos != string_view::npos)
  {
    // find subgroup and descend
    auto grpname = path.substr(0, pos);
    for (auto& grp : groups_)
      if (grp.name() == grpname)
        return grp.lookup_dimension(path.substr(pos, string_view::npos));
    throw error{this, "lookup dimension", format("sub-group '{}' not found", grpname).c_str()};
  }
  else
  {
    // find dimension
    for (auto& dim : dimensions_)
      if (dim.name() == path)
        return dim;
    throw error{this, "lookup dimension", format("dimension '{}' not found", path).c_str()};
  }
}

auto group::create_variable(
      string name
    , data_type type
    , span<dimension* const> dims
    , span<size_t const> chunk_size
    , int compression_level
    ) -> variable&
{
  return *variables_.insert(variables_.end(), {this, std::move(name), type, dims, chunk_size, compression_level});
}

auto group::copy_variable(variable const& rhs) -> variable&
{
  return *variables_.insert(variables_.end(), {this, rhs});
}

auto group::find_variable(string_view name) const -> variable const*
{
  for (auto& v : variables_)
    if (v.name() == name)
      return &v;
  return parent_ ? parent_->find_variable(name) : nullptr;
}

auto group::lookup_variable(string_view path) const -> variable const&
{
  // skip leading slashes
  path.remove_prefix(std::min(path.find_first_not_of("/"), path.size()));

  // find end of first token
  auto pos = path.find('/');

  // is this an intermediate node?
  if (pos != string_view::npos)
  {
    // find subgroup and descend
    auto grpname = path.substr(0, pos);
    for (auto& grp : groups_)
      if (grp.name() == grpname)
        return grp.lookup_variable(path.substr(pos, string_view::npos));
    throw error{this, "lookup variable", format("sub-group '{}' not found", grpname).c_str()};
  }
  else
  {
    // find variable
    for (auto& var : variables_)
      if (var.name() == path)
        return var;
    throw error{this, "lookup variable", format("variable '{}' not found", path).c_str()};
  }
}

auto group::lookup_group(string_view path) const -> group const&
{
  // skip leading slashes
  path.remove_prefix(std::min(path.find_first_not_of("/"), path.size()));

  // a null path means this group
  if (path.empty())
    return *this;

  // find end of first token
  auto pos = path.find('/');

  // is this an intermediate node?
  if (pos != string_view::npos)
  {
    // find subgroup and descend
    auto grpname = path.substr(0, pos);
    for (auto& grp : groups_)
      if (grp.name() == grpname)
        return grp.lookup_group(path.substr(pos, string_view::npos));
    throw error{this, "lookup group", format("group '{}' not found", grpname).c_str()};
  }
  else
  {
    // find group
    for (auto& grp : groups_)
      if (grp.name() == path)
        return grp;
    throw error{this, "lookup group", format("group '{}' not found", path).c_str()};
  }
}

/* NOTE: the fact that the raii handle is stored in the file class means that it will be destroyed _before_
 * the group base class.  this means it is IMPERATIVE that the dimension/object/group/variable destructors
 * do not cause ANY NetCDF API functions to be invoked. */
file::file(string path, io_mode mode)
  : group{nullptr}
  , path_{std::move(path)}
  , handle_{nc_file_handle::nullhnd, nc_close}
{
  // open the file
  int ncid;
  if (mode == io_mode::create)
  {
    if (auto status = nc_create(path_.c_str(), NC_NETCDF4, &ncid))
      throw error{path_, "open file", "nc_create", status};
  }
  else
  {
    if (auto status = nc_open(path_.c_str(), mode == io_mode::read_only ? NC_NOWRITE : NC_WRITE, &ncid))
      throw error{path_, "open file", "nc_open", status};
  }
  handle_.reset(ncid);

  /* initialize our actual group.  this allows us to delay the main body of the group constructor until after
   * we have opened the file safely and stored our file path.  the handle is needed (duh) to scan through the
   * file, but the path is also important since it is accessed by the error class when exceptions are thrown. */
  init(handle_);

  // for writeable files increase speed by not filling new variables with the _FillValue
  if (mode != io_mode::read_only)
  {
    int prior;
    if (auto status = nc_set_fill(handle_, NC_NOFILL, &prior))
      throw error{path_, "open file", "nc_set_fill", status};
  }
}

#if STEPS_NETCDF_IN_MEMORY_FILES
file::file(string path, io_mode mode, buffer_ptr data, size_t size)
  : group{nullptr}
  , path_{std::move(path)}
  , handle_{nc_file_handle::nullhnd, nc_close}
{
  /* The NetCDF API really doesn't like it if we leave a URI schema on the 'path' when opening a file in-memory
   * even though it won't be attempting to use the path.  However, it's important for us to keep this as part of
   * the 'path' from the user perspective when returned by the path() function since the value is often used as
   * the key to a list of cached files.
   *
   * To work around this issue, we detect when the passed path is in the form of a URI and skip the schema token
   * when we pass the path to the API.  The user will still see the original path they passed in though which is
   * exactly what we want.
   *
   * This isn't that important really since we could just pass a fixed token like "in-memory" to the NetCDF API.
   * The documents are a bit vague and seem to indicate that using the same token for multiple calls could return
   * a reference to the first file instead of opening a new one - but testing shows that this is not the case for
   * in memory files.  Still, partially for safety, and partially to help with tracing during errors, we prefer
   * to pass _something_ that is close to correct as the path to the NetCDF API. */
  auto skip = path_.find_first_of(":/");
  if (skip != string::npos && path_.size() > skip + 2 && path_[skip] == ':' && path_[skip + 1] == '/' && path_[skip + 2] == '/')
    skip += 3;
  else
    skip = 0;

  // open the file
  int ncid;
  if (mode == io_mode::create)
  {
    if (auto status = nc_create_mem(path_.c_str() + skip, NC_NETCDF4, size, &ncid))
      throw error{path_, "open file", "nc_create_mem", status};
  }
  else
  {
    NC_memio params;
    params.size = size;
    params.memory = data.release();
    params.flags = 0;
    if (auto status = nc_open_memio(path_.c_str() + skip, mode == io_mode::read_only ? NC_NOWRITE : NC_WRITE, &params, &ncid))
      throw error{path_, "open file", "nc_open_memio", status};
  }
  handle_.reset(ncid);

  /* initialize our actual group.  this allows us to delay the main body of the group constructor until after
   * we have opened the file safely and stored our file path.  the handle is needed (duh) to scan through the
   * file, but the path is also important since it is accessed by the error class when exceptions are thrown. */
  init(handle_);

  // for writeable files increase speed by not filling new variables with the _FillValue
  if (mode != io_mode::read_only)
  {
    int prior;
    if (auto status = nc_set_fill(handle_, NC_NOFILL, &prior))
      throw error{path_, "open file", "nc_set_fill", status};
  }
}

auto file::close_and_acquire_memory() && -> pair<buffer_ptr, size_t>
{
  NC_memio params;
  if (auto status = nc_close_memio(handle_, &params))
    throw error{this, "flush", "nc_close_memio", status};
  handle_.release();
  return { buffer_ptr{reinterpret_cast<char*>(params.memory)}, params.size };
}
#endif

auto file::flush() -> void
{
  if (auto status = nc_sync(handle_))
    throw error{this, "flush", "nc_sync", status};
}

// LCOV_EXCL_START
#include "array2.h"
TEST_CASE("netcdf")
{
  auto path = std::filesystem::temp_directory_path() / "steps.nc";
  REQUIRE(!std::filesystem::exists(path));

  // writing
  {
    auto f1 = file{path, io_mode::create};

    f1.att_set("a1"s, (signed char) 1);
    f1.att_set("a2"s, (unsigned char) 2);
    f1.att_set("a3"s, (short) 3);
    f1.att_set("a4"s, (unsigned short) 4);
    f1.att_set("a5"s, (int) 5);
    f1.att_set("a6"s, (unsigned int) 6);
    f1.att_set("a7"s, (long) 7);
    f1.att_set("a8"s, (unsigned long) 8);
    f1.att_set("a9"s, (long long) 9);
    f1.att_set("a10"s, (unsigned long long) 10);
    f1.att_set("a11"s, 11.0f);
    f1.att_set("a12"s, 12.0);
    f1.att_set("a13"s, "13");
    f1.att_set("a14"s, "14"s);
    auto set_att_array = [&](string const& name, auto val)
    {
      CAPTURE(name);
      array1<decltype(val)> v(2);
      v.fill(val);
      f1.att_set(name, v);
    };
    set_att_array("aa1"s, (signed char) 1);
    set_att_array("aa2"s, (unsigned char) 2);
    set_att_array("aa3"s, (short) 3);
    set_att_array("aa4"s, (unsigned short) 4);
    set_att_array("aa5"s, (int) 5);
    set_att_array("aa6"s, (unsigned int) 6);
    set_att_array("aa7"s, (long) 7);
    set_att_array("aa8"s, (unsigned long) 8);
    set_att_array("aa9"s, (long long) 9);
    set_att_array("aa10"s, (unsigned long long) 10);
    set_att_array("aa11"s, 11.0f);
    set_att_array("aa12"s, 12.0);

    f1.att_rename("a14"s, "a14-r"s);

    auto& dy = f1.create_dimension("y", 5);
    auto& dx = f1.create_dimension("x", 10);

    auto write_var = [&](string const& name, data_type type, auto val)
    {
      CAPTURE(name);
      auto& var = f1.create_variable(name, type, {&dy, &dx}, {5, 10});
      vector<decltype(val)> v(50);
      std::fill(v.begin(), v.end(), val);
      var.write(v);
    };
    write_var("v1", data_type::i8, int8_t(1));
    write_var("v2", data_type::u8, int8_t(2));
    write_var("v3", data_type::i16, int16_t(3));
    write_var("v4", data_type::u16, uint16_t(4));
    write_var("v5", data_type::i32, int32_t(5));
    write_var("v6", data_type::u32, uint32_t(6));
    write_var("v7", data_type::i64, int64_t(7));
    write_var("v8", data_type::u64, uint64_t(8));
    write_var("v9", data_type::f32, 9.0f);
    write_var("v10", data_type::f64, 10.0);
    write_var("v11", data_type::string, "11"s);

    auto& g1 = f1.create_group("foo");
    auto& dt = g1.create_dimension("time", 12);
    CHECK_NOTHROW(dt.rename("t"));

    g1.att_set("asdf", 3);
    auto& g11 = g1.create_group("bar");
    g11.att_set("funny", 2);
    auto& g2 = f1.create_group("hello");
    g2.att_set("att1", 33);

    auto& vens = g11.create_variable(
          "precipitation"
        , data_type::f32
        , {&dt, &dy, &dx}
        , {1, 5, 10});
    CHECK_NOTHROW(vens.rename("precip")); // rename seems to only work between create and first write
    array2i data{{dx.size(), dy.size()}};
    for (auto i = 0; i < data.size(); ++i)
      data.data()[i] = i % 4;
    for (auto i = 0; i < dt.size(); ++i)
      vens.write(data, {i, all, all});

    g11.att_copy(f1, "a14-r");
    g11.att_erase("funny");

    CHECK_NOTHROW(g1.rename("foo2"));
    CHECK_NOTHROW(g11.rename("bar2"));
    CHECK_NOTHROW(f1.flush());
  }
  // reading
  {
    CHECK_THROWS(file("/bad/path/", io_mode::create));
    CHECK_THROWS(file("/bad/path/", io_mode::read_only));

    auto f1 = file{path, io_mode::read_only};

    CHECK(f1.path() == path);

    {
      auto i = 0;
      while (i < f1.att_size() && f1.att_name(i) != "a1")
        ++i;
      if (i == f1.att_size())
        ADD_FAIL_CHECK_AT(__FILE__, __LINE__, "Failed to locate attribute by iteration");
    }

    auto check_att = [&](char const* name, auto val)
    {
      CAPTURE(name);
      if constexpr (std::is_arithmetic_v<decltype(val)>)
      {
        array1<decltype(val)> vbad{2};
        CHECK_THROWS(f1.att_get(name, vbad));
      }
      decltype(val) v;
      f1.att_get(name, v);
      if constexpr (std::is_floating_point_v<decltype(val)>)
        CHECK(v == approx(val));
      else
        CHECK(v == val);
    };
    check_att("a1", (signed char) 1);
    check_att("a2", (unsigned char) 2);
    check_att("a3", (short) 3);
    check_att("a4", (unsigned short) 4);
    check_att("a5", (int) 5);
    check_att("a6", (unsigned int) 6);
    check_att("a7", (long) 7);
    check_att("a8", (unsigned long) 8);
    check_att("a9", (long long) 9);
    check_att("a10", (unsigned long long) 10);
    check_att("a11", 11.0f);
    check_att("a12", 12.0);
    check_att("a13", "13"s);
    check_att("a14-r", "14"s);
    CHECK(f1.att_type("a14-r") == data_type::string);

    auto check_att_array = [&](char const* name, auto val)
    {
      CAPTURE(name);
      REQUIRE(f1.att_size(name) == 2);
      decltype(val) vbad;
      CHECK_THROWS(f1.att_get(name, vbad));
      array1<decltype(val)> v{2};
      f1.att_get(name, v);
      if constexpr (std::is_floating_point_v<decltype(val)>)
      {
        CHECK(v[0] == approx(val));
        CHECK(v[1] == approx(val));
      }
      else
      {
        CHECK(v[0] == val);
        CHECK(v[1] == val);
      }
    };
    check_att_array("aa1", (signed char) 1);
    check_att_array("aa2", (unsigned char) 2);
    check_att_array("aa3", (short) 3);
    check_att_array("aa4", (unsigned short) 4);
    check_att_array("aa5", (int) 5);
    check_att_array("aa6", (unsigned int) 6);
    check_att_array("aa7", (long) 7);
    check_att_array("aa8", (unsigned long) 8);
    check_att_array("aa9", (long long) 9);
    check_att_array("aa10", (unsigned long long) 10);
    check_att_array("aa11", 11.0f);
    check_att_array("aa12", 12.0);

    auto check_var = [&](char const* name, auto val)
    {
      CAPTURE(name);
      auto& var = f1.lookup_variable(name);
      REQUIRE(var.dimensions().size() == 2);
      REQUIRE(var.dimensions()[0]->size() == 5);
      REQUIRE(var.dimensions()[1]->size() == 10);
      vector<decltype(val)> v(50), ref(50);
      std::fill(ref.begin(), ref.end(), val);
      var.read(v);
      if constexpr (std::is_floating_point_v<decltype(val)>)
        CHECK_ALL(v, ref, lhs == approx(rhs));
      else
        CHECK_ALL(v, ref, lhs == rhs);
    };
    check_var("v1", int8_t(1));
    check_var("v2", int8_t(2));
    check_var("v3", int16_t(3));
    check_var("v4", uint16_t(4));
    check_var("v5", int32_t(5));
    check_var("v6", uint32_t(6));
    check_var("v7", int64_t(7));
    check_var("v8", uint64_t(8));
    check_var("v9", 9.0f);
    check_var("v10", 10.0);
    check_var("v11", "11"s);

    CHECK_THROWS(f1.lookup_dimension("/foo2/time"));
    auto& dt = f1.lookup_dimension("/foo2/t");
    CHECK(dt.parent() != nullptr);
    CHECK(dt.parent()->name() == "foo2");
    CHECK(dt.name() == "t");

    auto& g1 = f1.lookup_group("/foo2");
    CHECK(g1.find_dimension("t") == &dt);
    CHECK(g1.find_dimension("x") != nullptr);
    CHECK(g1.find_dimension("bad") == nullptr);

    CHECK(&f1.lookup_group("") == &f1);
    CHECK_THROWS(f1.lookup_group("bad"));
    auto& g11 = f1.lookup_group("/foo2/bar2");

    CHECK(g11.att_exists("a14-r"));
    CHECK(g11.att_type("a14-r") == data_type::string);
    auto str = string{};
    g11.att_get("a14-r", str);
    CHECK(str == "14");
    CHECK(g11.att_exists("funny") == false);

    CHECK_THROWS(f1.lookup_variable("/bad/precipitation"));
    CHECK_THROWS(f1.lookup_variable("/foo2/bar2/bad"));
    auto& vens = f1.lookup_variable("/foo2/bar2/precip");

    CHECK(g11.find_variable("bad") == nullptr);
    CHECK(g11.find_variable("precipitation") == nullptr);
    CHECK(g11.find_variable("precip") == &vens);
  }
  std::filesystem::remove(path);
}

TEST_CASE("netcdf-str")
{
  string desc = "hello world";
  vector<string> months =
  {
      "january", "february", "march"
    , "april", "may", "june"
    , "july", "august", "september"
    , "october", "november", "december"
  };

  {
    file f1{"test3.nc", io_mode::create};
    auto& d1 = f1.create_dimension("month", 12);

    auto& v1 = f1.create_variable("desc", data_type::string);
    v1.write(span{&desc, 1});

    auto& v2 = f1.create_variable("months", data_type::string, {&d1});
    v2.write(months);
  }

  {
    file f{"test3.nc", io_mode::read_only};

    string desc_in;
    f.lookup_variable("desc").read(span{&desc_in, 1});
    CHECK(desc_in == desc);

    vector<string> months_in(months.size());
    f.lookup_variable("months").read(months_in);
    CHECK(std::equal(months_in.begin(), months_in.end(), months.begin()));
  }
}

#if STEPS_NETCDF_IN_MEMORY_FILES
TEST_CASE("nc - in memory")
{
  auto inmem = pair<file::buffer_ptr, size_t>{};

  {
    auto f = file{"file:///foo/bar", io_mode::create, nullptr, 1024 * 1024 * 16};
    f.att_set("hello", "world");
    auto& d = f.create_dimension("time", 10);
    auto& var = f.create_variable("data", data_type::i32, {&d});
    auto data = array1i{10};
    data.fill(5);
    var.write(data);
    inmem = std::move(f).close_and_acquire_memory();
  }
  {
    auto f = file{"file:///foo/bar", io_mode::read_only, std::move(inmem.first), inmem.second};
    string str;
    CHECK_NOTHROW(f.att_get("hello", str));
    CHECK(str == "world");
    auto& d = f.lookup_dimension("time");
    CHECK(d.size() == 10);
    auto& var = f.lookup_variable("data");
    CHECK(var.dimensions().size() == 1);
    CHECK(var.dimensions()[0] == &d);
    auto data = array1i{10};
    CHECK_NOTHROW(var.read(data));
    CHECK_ALL(data, data, lhs == 5);
  }
}
#endif
// LCOV_EXCL_STOP
