/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array2.h"

namespace steps
{
  template <typename T>
  inline auto get_pixel(array2<T> const& img, int i, int j, T background) -> T
  {
    return (i < 0 || i >= img.shape().y || j < 0 || j >= img.shape().x) ? background : img[i][j];
  }

  /// Sample a real coordinate value from a grid using bilinear filtering
  template <typename T>
  inline auto sample_grid(array2<T> const& img, float src_x, float src_y, T background) -> T
  {
  #if 0 // HACK HACK - pick nearest
    int src_i = std::round(src_y);
    int src_j = std::round(src_x);
    if (   src_i < 0 || src_i >= img.shape().y
        || src_j < 0 || src_j >= img.shape().x)
      return background;
    return img[src_i][src_j];
  #endif

    // integer coordinates of the nw source bin
    // note: must use floor (not cast) to cope with negative indexes
    int src_i = std::floor(src_y);
    int src_j = std::floor(src_x);

    // determine fractional contributions of each pixel under the source mask
    float frac_i = 1.0f + src_i - src_y;
    float frac_j = 1.0f + src_j - src_x;

    // if any of the source pixel mask is outside domain do the slow case
    if (   src_i < 0 || src_i >= img.shape().y - 1
        || src_j < 0 || src_j >= img.shape().x - 1)
    {
      // if _all_ of the source pixels are outside, just set to border value
      if (   src_i < -1 || src_i >= img.shape().y
          || src_j < -1 || src_j >= img.shape().x)
      {
        return background;
      }
      else
      {
        // okay, at least one good pixel.  use it
        return
                frac_i      *     frac_j      * get_pixel(img, src_i, src_j, background)
          +     frac_i      * (1.0f - frac_j) * get_pixel(img, src_i, src_j + 1, background)
          + (1.0f - frac_i) *     frac_j      * get_pixel(img, src_i + 1, src_j, background)
          + (1.0f - frac_i) * (1.0f - frac_j) * get_pixel(img, src_i + 1, src_j + 1, background);
      }
    }
    else
    {
      // normal case - use all 4 pixels
      return
              frac_i      *     frac_j      * img[src_i][src_j]
        +     frac_i      * (1.0f - frac_j) * img[src_i][src_j + 1]
        + (1.0f - frac_i) *     frac_j      * img[src_i + 1][src_j]
        + (1.0f - frac_i) * (1.0f - frac_j) * img[src_i + 1][src_j + 1];
    }
  }

  /// Accumulate new flow vectors into an existing flow field
  /** The flow vectors are 'at' the lag0 location and point 'backward' to locations in lag1. */
  auto accumulate_flow(array2f2 const& flow, array2f2 const& lag1, array2f2& lag0) -> void;

  /// Advect a field using prediagnosed flow vectors using a scalar value to fill in off edge advections
  template <typename T>
  auto advect(array2f2 const& flow, T background, array2<T> const& lag1, array2<T>& lag0) -> void
  {
    // sanity checks
    if (&lag1 == &lag0)
      throw std::invalid_argument("Inplace operation not supported");
    if (lag0.shape() != flow.shape() || lag1.shape() != flow.shape())
      throw std::invalid_argument("Aray size mismatch");

    // advect every pixel in the output image
    for (auto y = 0; y < flow.shape().y; ++y)
      for (auto x = 0; x < flow.shape().x; ++x)
        lag0[y][x] = sample_grid(lag1, x + flow[y][x].x, y + flow[y][x].y, background);
  }

  /// Advect a field using prediagnosed flow vectors using a background array to fill in off edge advections
  template <typename T>
  auto advect(array2f2 const& flow, array2<T> const& background, array2<T> const& lag1, array2<T>& lag0) -> void
  {
    // sanity checks
    if (&lag1 == &lag0)
      throw std::invalid_argument("Inplace operation not supported");
    if (background.shape() != flow.shape() || lag0.shape() != flow.shape() || lag1.shape() != flow.shape())
      throw std::invalid_argument("Aray size mismatch");

    // advect every pixel in the output image
    for (auto y = 0; y < flow.shape().y; ++y)
      for (auto x = 0; x < flow.shape().x; ++x)
        lag0[y][x] = sample_grid(lag1, x + flow[y][x].x, y + flow[y][x].y, background[y][x]);
  }

  /// Advect an integer field
  /** This function is the integer equivalent of advect(), with the exception that the output value
   *  is copied exactly from the nearest input rather than using bilinear interpolation. */
  template <>
  inline auto advect(array2f2 const& flow, int background, array2i const& lag1, array2i& lag0) -> void
  {
    // sanity checks
    if (&lag1 == &lag0)
      throw std::invalid_argument("Inplace operation not supported");
    if (lag0.shape() != flow.shape() || lag1.shape() != flow.shape())
      throw std::invalid_argument("Aray size mismatch");

    // advect every pixel in the output image - pick the nearest
    for (auto y = 0; y < flow.shape().y; ++y)
    {
      for (auto x = 0; x < flow.shape().x; ++x)
      {
        int src_i = std::round(y + flow[y][x].y);
        int src_j = std::round(x + flow[y][x].x);
        if (   src_i < 0 || src_i >= lag1.shape().y
            || src_j < 0 || src_j >= lag1.shape().x)
          lag0[y][x] = background;
        else
          lag0[y][x] = lag1[src_i][src_j];
      }
    }
  }
}
