/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "optical_flow.h"
#include "core.h"
#include "trace.h"
#include "thread_pool.h"
#include "ext/brox_mt/brox_optic_flow.h"

using namespace steps;

optical_flow_brox_mt::prefilter::prefilter(vec2i fft_shape, int fft_threads, float grid_resolution, float rolloff, float crossover)
  : fft{fft_shape, fft_threads}
  , low_pass{make_lowpass_filter(fft, grid_resolution, rolloff, crossover)}
{ }

optical_flow_brox_mt::optical_flow_brox_mt(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config)
  : log_zero_{log_zero}
  , alpha_{config.optional("alpha", 18.0f)}
  , gamma_{config.optional("gamma", 7.0f)}
  , scales_{config.optional("scales", 100)}
  , zfactor_{config.optional("zfactor", 0.75f)}
  , tol_{config.optional("tol", 0.0001f)}
  , initer_{config.optional("initer", 1)}
  , outiter_{config.optional("outiter", 15)}
{
  if (   alpha_ <= 0.0f
      || gamma_ < 0.0f
      || scales_ <= 0
      || zfactor_ <= 0.0f
      || zfactor_ >= 1.0f
      || tol_ <= 0.0f
      || initer_ <= 0
      || outiter_ <= 0)
    throw std::runtime_error{"Invalid parameter for optical_flow_brox_mt"};

  if (auto lp = config.find("low_pass"))
  {
    prefilter_.emplace(
          determine_fft_shape(config, grid_shape, 0)
        , config.optional("fft_threads", 1)
        , grid_resolution
        , config.optional("low_pass_rolloff", 1.0f)
        , float(*lp));
  }

  // sanity check based on original brox driver code (main.cpp)
  // set the number of scales according to the size of the images.  The value N is computed to assure
  // that the smaller images of the pyramid don't have a size smaller than 16x16
  const float N = 1 + log(std::min(grid_shape.x, grid_shape.y) / 16.) / log(1./zfactor_);
  if ((int) N < scales_)
    scales_ = (int) N;
}

auto optical_flow_brox_mt::determine_flow(
      array2f const& from
    , array2f const& to
    , bool use_initial_flow
    , array2f2& flow
    ) const -> void
{
  auto ps = profile::scope{prof_optical_flow};

  auto run_lowpass = [&](auto const& in) -> array2f const
  {
    auto real = fourier_transform::array2r{prefilter_->fft.shape_r()};
    real.fill(log_zero_);
    for (auto y = 0; y < in.shape().y; ++y)
      for (auto x = 0; x < in.shape().x; ++x)
        real[y][x] = in[y][x];
    auto spectrum = fourier_transform::array2c{prefilter_->fft.shape_c()};
    prefilter_->fft.execute_forward(real, spectrum);
    for (auto i = 0; i < spectrum.size(); ++i)
      spectrum.data()[i] *= prefilter_->low_pass.data()[i];
    prefilter_->fft.execute_inverse(spectrum, real);
    auto out = array2f{in.shape()};
    for (auto y = 0; y < in.shape().y; ++y)
      for (auto x = 0; x < in.shape().x; ++x)
        out[y][x] = real[y][x];
    return out;
  };

  array2f flow_u{to.shape()}, flow_v{to.shape()};

  // run the algorithm
  brox_mt::brox_optic_flow(
        (prefilter_ ? run_lowpass(from) : from).data()
      , (prefilter_ ? run_lowpass(to) : to).data()
      , flow_u.data()
      , flow_v.data()
      , from.shape().x
      , from.shape().y
      , alpha_
      , gamma_
      , scales_
      , zfactor_
      , tol_
      , initer_
      , outiter_
      , 0 // verbose?
      );

  // extract output vectors back into our flow
  for (int i = 0; i < flow.size(); ++i)
  {
    flow.data()[i].x = flow_u.data()[i];
    flow.data()[i].y = flow_v.data()[i];
  }
}
