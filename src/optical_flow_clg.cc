/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "optical_flow.h"
#include "core.h"
#include "trace.h"

extern "C"
{
#include "ext/clg/clg_of.h"
}

using namespace steps;

optical_flow_clg::optical_flow_clg(vec2i grid_shape, float grid_resolution, float log_zero, configuration const& config)
  // these four parameters had to be specified on command line - defaults are from readme example
  : alpha_{config.optional("alpha", 200.0)}
  , rho_{config.optional("rho", 5.0)}
  , sigma_{config.optional("sigma", 1.0)}
  , iterations_{config.optional("iterations", 200)}
  // these four parameters were optional - defaults are from driver code
  , w_factor_{config.optional("w_factor", 1.9)}
  , scales_{config.optional("scales", 1)}
  , scale_factor_{config.optional("scale_factor", 0.65)}
  , coupled_mode_{config.optional("coupled_mode", true)}
{
  // sanity check adapted from clg_of_main.c driver code
  // At the highest pyramid level the image must be have
  // enough pixels to apply the smoothing filter.
  double max_siz = DEFAULT_GAUSSIAN_WINDOW_SIZE*rho_ + 1.0;
  int nScalesMax = (int) floor(1.0 + log(max_siz / std::min(grid_shape.x, grid_shape.y))
                         / log(scale_factor_));
  if (scales_ > nScalesMax)
  {
    scales_ = nScalesMax;
    trace(trace_level::warning, "CLG optical flow corrected 'scales' to max value allowed ({})", scales_);
  }
}

auto optical_flow_clg::determine_flow(
      array2f const& from
    , array2f const& to
    , bool use_initial_flow
    , array2f2& flow
    ) const -> void
{
  auto ps = profile::scope{prof_optical_flow};

  // double based buffers needed by CLG implementation
  array2d from_d{from.shape()}, to_d{to.shape()}, flow_u{to.shape()}, flow_v{to.shape()};

  // copy in our lags and zero the flow field
  for (auto i = 0; i < from_d.size(); ++i)
    from_d.data()[i] = from.data()[i] * 100.0; // TEMP - should make a 'gain' parameter for this
  for (auto i = 0; i < to_d.size(); ++i)
    to_d.data()[i] = to.data()[i] * 100.0;
  flow_u.fill(0.0);
  flow_v.fill(0.0);

#if 0 // TEMP - for testing
  // HACK - ignore low rain rates!
  for (auto i = 0; i < from_d.size(); ++i)
    if (from.data()[i] < 1.0)
      from_d.data()[i] = 0.0;
  for (auto i = 0; i < to_d.size(); ++i)
    if (to.data()[i] < 1.0)
      to_d.data()[i] = 0.0;
#endif

  // run the algorithm
  calcMSCLG_OF(
        from_d.data()
      , to_d.data()
      , flow_u.data()
      , flow_v.data()
      , from.shape().y
      , from.shape().x
      , iterations_
      , alpha_
      , rho_
      , sigma_
      , w_factor_
      , scales_
      , scale_factor_
      , coupled_mode_
      , 1);

  // extract output vectors back into our flow
  for (int i = 0; i < flow.size(); ++i)
  {
    flow.data()[i].x = flow_u.data()[i];
    flow.data()[i].y = flow_v.data()[i];
  }
}
