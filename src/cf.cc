/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "cf.h"
#include "array2.h"
#include "unit_test.h"

using namespace steps;
using namespace steps::cf;

// multipliers used to convert time units into seconds
// TODO - could calculate these ratio automatically at compile time and use clock::duration instead
//        it would be a marginal speed improvement possibly
static constexpr double time_unit_factors[] =
{
  1.0, 60.0, 3600.0, 86400.0
};

auto steps::cf::duration_from_time_units(double val, time_unit units) -> duration
{
  return std::chrono::seconds{static_cast<std::chrono::seconds::rep>(val * time_unit_factors[static_cast<int>(units)])};
}

auto steps::cf::time_units_from_duration(duration val, time_unit units) -> double
{
  return std::chrono::duration_cast<std::chrono::seconds>(val).count() / time_unit_factors[static_cast<int>(units)];
}

// TODO - use UDUNITS to perform this function to ensure support for full set of representations
auto steps::cf::parse_time_units(string const& str) -> pair<time_unit, time_point>
{
  pair<time_unit, time_point> ret;

  // find the first space (end of unit)
  auto uend = str.find(' ');

  // determine the units
  if (str.compare(0, uend, "seconds") == 0)
    ret.first = time_unit::seconds;
  else if (str.compare(0, uend, "minutes") == 0)
    ret.first = time_unit::minutes;
  else if (str.compare(0, uend, "hours") == 0)
    ret.first = time_unit::hours;
  else if (str.compare(0, uend, "days") == 0)
    ret.first = time_unit::days;
  else
    throw std::runtime_error{format("unsupported CF time unit: {}", str)};

  // find the second space (end of 'since')
  auto send = str.find(' ', uend + 4); // + 4 since shortest units is days and we can safely skip these characters

  // ensure 'since' was present
  if (str.compare(uend, send - uend, " since") != 0)
    throw std::runtime_error{format("unsupported CF time unit: {}", str)};

  // strip an optional ' UTC' from the end of the timestamp
  auto tend = str.compare(str.size() - 4, 4, " UTC") == 0 ? str.size() - 4 : str.size();

  // parse the timestamp
  ret.second = parse<time_point>(string_view{str.c_str() + send + 1, tend - send - 1});

  return ret;
}

auto steps::cf::read_time(nc::variable const& var, span<int const> start) -> time_point
{
  double val;
  packer{}.read(var, span{&val, 1}, std::numeric_limits<double>::quiet_NaN(), start);
  auto units_epoch = parse_time_units(var.att_get_as<string>("units"));
  return units_epoch.second + duration_from_time_units(val, units_epoch.first);
}

auto steps::cf::read_times(
      nc::variable const& var
    , span<time_point> data
    , span<int const> start
    , span<int const> count
    ) -> void
{
  array1d val(data.size());
  packer{}.read(var, val, std::numeric_limits<double>::quiet_NaN(), start, count);
  auto units_epoch = parse_time_units(var.att_get_as<string>("units"));
  for (decltype(data.size()) i = 0; i < data.size(); ++i)
    data[i] = units_epoch.second + duration_from_time_units(val[i], units_epoch.first);
}

auto steps::cf::find_coordinates(nc::dimension const& dim) -> nc::variable const*
{
  // don't use find variable because it ascends to parents.  we only want to check if the variable exists
  // at the same level as the dimension
  for (auto& var : dim.parent()->variables())
  {
    if (var.name() == dim.name())
    {
      if (var.dimensions().size() != 1)
        throw std::runtime_error{format(
                  "coordinate variable for dimension {} is rank {} (expected 1)"
                , dim.name()
                , var.dimensions().size())};
      if (var.dimensions()[0] != &dim)
        throw std::runtime_error{format(
                  "coordinate variable for dimension {} is indexed on incorrect dimension {}"
                , dim.name()
                , var.dimensions()[0]->name())};
      return &var;
    }
  }
  return nullptr;
}

auto steps::cf::lookup_coordinates(nc::dimension const& dim) -> nc::variable const&
{
  if (auto ret = find_coordinates(dim))
    return *ret;
  throw std::runtime_error{format("coordinate variable for dimension {} not found", dim.name())};
}

auto steps::cf::copy_dimension_and_coordinates(nc::dimension const& dim, nc::group& target) -> nc::dimension&
{
  // copy the dimension first
  auto& ret = target.create_dimension(dim.name(), dim.size());

  // copy coordinates if they exist
  if (auto var = find_coordinates(dim))
  {
    target.copy_variable(*var);

    // copy bounds if they exist
    if (auto bname = var->att_get_as<optional<string>>("bounds"))
      target.copy_variable(var->parent()->lookup_variable(*bname));
  }

  return ret;
}

// LCOV_EXCL_START
TEST_CASE("cf::duration_from_time_units")
{
  CHECK(cf::duration_from_time_units(10, cf::time_unit::seconds) == std::chrono::seconds{10});
  CHECK(cf::duration_from_time_units(10, cf::time_unit::minutes) == std::chrono::seconds{600});
  CHECK(cf::duration_from_time_units(10, cf::time_unit::hours) == std::chrono::seconds{36000});
  CHECK(cf::duration_from_time_units(10, cf::time_unit::days) == std::chrono::seconds{864000});
}
TEST_CASE("cf::time_units_from_duration")
{
  CHECK(cf::time_units_from_duration(std::chrono::seconds{864000}, cf::time_unit::seconds) == approx(864000.0));
  CHECK(cf::time_units_from_duration(std::chrono::seconds{864000}, cf::time_unit::minutes) == approx(14400.0));
  CHECK(cf::time_units_from_duration(std::chrono::seconds{864000}, cf::time_unit::hours) == approx(240.0));
  CHECK(cf::time_units_from_duration(std::chrono::seconds{864000}, cf::time_unit::days) == approx(10.0));
}
TEST_CASE("cf::parse_time_units")
{
  CHECK_THROWS(parse_time_units("bad since 1970-01-01T00:00:00Z"));
  CHECK(parse_time_units("seconds since 2021-01-01T00:00:00Z").second == parse<time_point>("2021-01-01T00:00:00Z"));
  CHECK(parse_time_units("seconds since 2021-01-01T00:00:00Z").first == cf::time_unit::seconds);
  CHECK(parse_time_units("minutes since 2021-01-01T00:00:00Z").first == cf::time_unit::minutes);
  CHECK(parse_time_units("hours since 2021-01-01T00:00:00Z").first == cf::time_unit::hours);
  CHECK(parse_time_units("days since 2021-01-01T00:00:00Z").first == cf::time_unit::days);
}
TEST_CASE("cf::packer (no fill value")
{
  auto path = std::filesystem::temp_directory_path() / "steps.tmp";
  REQUIRE(!std::filesystem::exists(path));

  auto pk = packer{};
  auto ddata = array2d{2, 2};

  auto file = nc::file{path, nc::io_mode::create};
  auto& dy = file.create_dimension("y", ddata.shape().y);
  auto& dx = file.create_dimension("x", ddata.shape().x);
  auto& var = file.create_variable("var", nc::data_type::i32, {&dy, &dx});
  var.att_set("scale_factor", 5.0);
  var.att_set("add_offset", 10.0);

  // pack and write the data
  ddata[0][0] = 10.0;
  ddata[0][1] = 110.0;
  ddata[1][0] = 210.0;
  ddata[1][1] = 310.0;
  pk.write(var, ddata);

  // check the packed values
  auto idata = array2i{ddata.shape()};
  idata.fill(-1);
  var.read(idata);
  CHECK(idata[0][0] == 0);
  CHECK(idata[0][1] == 20);
  CHECK(idata[1][0] == 40);
  CHECK(idata[1][1] == 60);

  // check the unpacked result
  array2d rdata{ddata.shape()};
  rdata.fill(-1.0);
  pk.read(var, rdata);
  CHECK(rdata[0][0] == approx(10.0));
  CHECK(rdata[0][1] == approx(110.0));
  CHECK(rdata[1][0] == approx(210.0));
  CHECK(rdata[1][1] == approx(310.0));

  std::filesystem::remove(path);
}
TEST_CASE("cf::packer (with fill value")
{
  auto path = std::filesystem::temp_directory_path() / "steps.tmp";
  REQUIRE(!std::filesystem::exists(path));

  auto pk = packer{};
  auto ddata = array2d{2, 2};

  auto file = nc::file{path, nc::io_mode::create};
  auto& dy = file.create_dimension("y", ddata.shape().y);
  auto& dx = file.create_dimension("x", ddata.shape().x);
  auto& var = file.create_variable("var", nc::data_type::i32, {&dy, &dx});
  var.att_set("scale_factor", 5.0);
  var.att_set("add_offset", 10.0);
  var.att_set("_FillValue", -999);

  // pack and write the data
  ddata[0][0] = std::numeric_limits<double>::quiet_NaN();
  ddata[0][1] = 110.0;
  ddata[1][0] = 210.0;
  ddata[1][1] = 310.0;
  pk.write(var, ddata);

  // check the packed values
  auto idata = array2i{ddata.shape()};
  idata.fill(-1);
  var.read(idata);
  CHECK(idata[0][0] == -999);
  CHECK(idata[0][1] == 20);
  CHECK(idata[1][0] == 40);
  CHECK(idata[1][1] == 60);

  // check the unpacked result
  auto rdata = array2d{ddata.shape()};
  rdata.fill(-1.0);
  pk.read(var, rdata);
  CHECK(std::isnan(rdata[0][0]));
  CHECK(rdata[0][1] == approx(110.0));
  CHECK(rdata[1][0] == approx(210.0));
  CHECK(rdata[1][1] == approx(310.0));

  std::filesystem::remove(path);
}
// LCOV_EXCL_STOP
