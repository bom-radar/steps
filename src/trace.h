/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "format.h"
#include "traits.h"
#include <functional>

namespace steps
{
  /// Trace severity levels
  enum class trace_level
  {
      none
    , error
    , warning
    , notice
    , log
    , verbose
  };
  STEPS_ENUM_TRAITS(trace_level, none, error, warning, notice, log, verbose);

  /// Type of trace target output function
  using trace_write_fn = std::function<void(string)>;

  /// Type of trace target flush function
  using trace_flush_fn = std::function<void()>;

  /// Default target function which outputs data to stdout
  /** This function is the default trace target at application startup, and can be provided to trace_set_target() to
   *  restore the default target if desired.  Although a user can call this function directly, this does not make
   *  much sense since it will output the buffer to stdout instead of the currently configured trace target. */
  auto trace_target_stdout_write(string buffer) -> void;

  /// Default target flush function which flushes stdout
  auto trace_target_stdout_flush() -> void;

  /// Get the current maximum trace level
  auto trace_max_level() -> trace_level;

  /// Set the maximum trace level
  auto trace_set_max_level(trace_level max) -> void;

  /// Set the trace output function
  /** The default implementation of this function simply writes trace output to stdout in a thread safe manner.
   *  You can restore the default target by passing trace_starget_stdout as the target. */
  auto trace_set_target(trace_write_fn write, trace_flush_fn flush) -> void;

  /// Flush the underlying output target
  auto trace_flush_target() -> void;

  /// Get whether color output is enabled
  auto trace_color_output() -> bool;

  /// Set whether color output is enabled
  auto trace_set_color_output(bool enabled) -> void;

  /// Trace output function with precompiled format arguments
  auto vtrace(trace_level level, char const* format_str, fmt::format_args const& args) -> void;

  /// Trace output function
  template <typename... Args>
  auto trace(trace_level level, char const* format_str, Args const&... args) -> void
  {
    if (level <= trace_max_level())
      vtrace(level, format_str, fmt::make_format_args(args...));
  }

  /// Check whether the given trace level is active
  /** Useful for conditioning blocks of code that are only needed to prepare detailed trace output. */
  inline auto trace_active(trace_level level)
  {
    return level <= trace_max_level();
  }

  /// Prefix before the contents of each trace line printed during the life of this object
  /** The supplied prefix string is pushed onto a stack of prefixes upon construction and popped from the
   *  stack upon destruction.  The stack of prefixes is thread local so using these objects in multi-threaded
   *  code is safe and will behave naturally.
   *
   *  When multiple prefixes are available in the stack they will be output separated by a period character.
   *
   *  This class retains a pointer to the string provided and it is the responsibility of the user to
   *  ensure that this pointer remains valid during the lifetime of the object.  Destroying the prefix string
   *  while the object is alive results in undefined behaviour.  This behaviour is analogous to that of
   *  std::lock_guard.
   *
   *  It is also the responsibility of the user to ensure that objects are destroyed by the same thread that
   *  created them.  This is easily ensured by only creating these objects directly on the stack.
   */
  class trace_prefix
  {
  public:
    explicit trace_prefix(char const* prefix);
    explicit trace_prefix(string const& prefix);

    trace_prefix(trace_prefix const&) = delete;
    trace_prefix(trace_prefix&&) = delete;
    auto operator=(trace_prefix const&) -> trace_prefix& = delete;
    auto operator=(trace_prefix&&) -> trace_prefix& = delete;

    ~trace_prefix();

  private:
    string::size_type restore_length_;
  };
}
