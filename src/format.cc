/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "format.h"
#include "unit_test.h"

using namespace steps;

auto parser<bool>::parse(string_view str) -> bool
{
  if (str == "true")
    return true;
  if (str == "false")
    return false;
  throw std::runtime_error{format("Failed to parse {} from '{}'", "bool", str)};
}

auto parser<time_point>::parse(string_view str) -> time_point
{
  if (str.empty())
    throw std::runtime_error{format("Failed to parse {} from '{}'", "time_point", str)};

  // is it a time_t directly?
  if (str.front() == '@')
  {
    str.remove_prefix(1);
    return clock::from_time_t(steps::parse<time_t>(str));
  }

  // okay, try to parse ISO8601
  std::tm tms;
  char t, tz_sign;
  int tz_hh = 0, tz_mm = 0;

  // zero out our broken down time
  std::memset(&tms, 0, sizeof(std::tm));
  tms.tm_mon = 1;
  tms.tm_mday = 1;

  // build the expected format string according to string length, and note expected return value of sscanf
  char const* fmt;
  int eret;
  switch (str.size())
  {
  case 10: eret = 3;  fmt = "%04d-%02d-%02d"; break;
  case 13: eret = 5;  fmt = "%04d-%02d-%02d%c%02d"; break;
  case 16: eret = 6;  fmt = "%04d-%02d-%02d%c%02d:%02d"; break;
  case 19: eret = 7;  fmt = "%04d-%02d-%02d%c%02d:%02d:%02d"; break;
  case 20: eret = 8;  fmt = "%04d-%02d-%02d%c%02d:%02d:%02d%c"; break;
  case 22: eret = 9;  fmt = "%04d-%02d-%02d%c%02d:%02d:%02d%c%02d"; break;
  case 24: eret = 10; fmt = "%04d-%02d-%02d%c%02d:%02d:%02d%c%02d%02d"; break;
  case 25: eret = 10; fmt = "%04d-%02d-%02d%c%02d:%02d:%02d%c%02d:%02d"; break;
  default:
    throw std::runtime_error{format("Failed to parse {} from '{}'", "time_point", str)};
  }

  // copy our string_view into a null terminated buffer
  char buf[26];
  strncpy(buf, str.data(), 25);
  buf[str.size()] = '\0';

  // try to parse as ISO 8601 with delimeters
  // NOTE: if no timezone is specified we assume UTC _not_ local time
  int ret = sscanf(
        buf
      , fmt
      , &tms.tm_year
      , &tms.tm_mon
      , &tms.tm_mday
      , &t
      , &tms.tm_hour
      , &tms.tm_min
      , &tms.tm_sec
      , &tz_sign
      , &tz_hh
      , &tz_mm);

  // verify that we at least got the date components and that the entire string is consumed
  if (   ret != eret
      || ret < 3
      || (ret >= 4 && t != 'T' && t != ' ')
      || (ret == 8 && tz_sign != 'Z')
      || (ret >= 9 && tz_sign != '-' && tz_sign != '+'))
    throw std::runtime_error{format("Failed to parse {} from '{}'", "time_point", str)};

  // adjust tm structure offsets
  tms.tm_year -= 1900;
  tms.tm_mon -= 1;

  // determine the timezone adjustment
  auto tz_delta = std::chrono::seconds{(tz_sign == '-' ? -1 : 1) * (tz_hh * 3600 + tz_mm * 60)};

  // convert struct tm and offset to unix time
  // note: portability issue here - see man timegm
  return clock::from_time_t(timegm(&tms)) - tz_delta;
}

auto parser<duration>::parse(string_view str) -> duration
{
  return std::chrono::seconds{::steps::parse<long>(str)};
}

// LCOV_EXCL_START
TEST_CASE("parse")
{
  CHECK(parse<int>("123") == 123);
  CHECK_THROWS(parse<int>(" 123"));
  CHECK_THROWS(parse<int>("123 "));
  CHECK_THROWS(parse<int>("123f"));

  CHECK(parse<float>("123.45") == approx(123.45));
  CHECK_THROWS(parse<float>(" 123.45"));
  CHECK_THROWS(parse<float>("123.45 "));
  CHECK_THROWS(parse<float>("123.45f"));

  CHECK(parse<bool>("true") == true);
  CHECK(parse<bool>("false") == false);
  CHECK_THROWS(parse<bool>("bad"));
}

TEST_CASE("parse<time_point>")
{
  CHECK(clock::to_time_t(parse<time_point>("@417399043")) == 417399043);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10:43+0005")) == 417398743);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10:43-1015")) == 417435943);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10:43+08")) == 417370243);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10:43-08")) == 417427843);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10:43Z")) == 417399043);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10:43")) == 417399043);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00:10")) == 417399000);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25T00")) == 417398400);
  CHECK(clock::to_time_t(parse<time_point>("1983-03-25")) == 417398400);
  CHECK_THROWS(parse<time_point>(""));
  CHECK_THROWS(parse<time_point>("@bad"));
  CHECK_THROWS(parse<time_point>("bad"));
  CHECK_THROWS(parse<time_point>("1983-03-xx"));
  CHECK_THROWS(parse<time_point>("1983-03-25X"));
  CHECK_THROWS(parse<time_point>("1983-03-25T00:10:43X"));
  CHECK_THROWS(parse<time_point>("1983-03-25T00:10:43X0000"));
}

TEST_CASE("parse<duration>")
{
  CHECK(parse<duration>("80") == 80s);
  CHECK(parse<duration>("-3") == -3s);
  CHECK_THROWS(parse<duration>("80.3"));
}

TEST_CASE("format")
{
  CHECK(format("foo") == "foo");
  CHECK(format("foo{}", "bar") == "foobar");
  CHECK(format("{1}{0}", "bar", "foo") == "foobar");
  CHECK(format("{f}{b}", "b"_a="bar", "f"_a="foo") == "foobar");
}

TEST_CASE("format<exception>")
{
  // base, derived and double derived exceptions
  string str;
  CHECK_NOTHROW(str = format("{}", std::exception{}));
  CHECK(format("{}", std::runtime_error{"foo"}) == "foo");
  struct bar : std::runtime_error { bar() : std::runtime_error{"bar"} { } };
  CHECK(format("{}", bar{}) == "bar");

  // nested exceptions
  auto a = [](){ throw std::runtime_error{"a"}; };
  auto b = [&](){ try { a(); } catch (...) { std::throw_with_nested(std::runtime_error{"b"}); } };
  auto c = [&](){ try { b(); } catch (...) { std::throw_with_nested(std::runtime_error{"c"}); } };
  try
  {
    c();
  }
  catch (std::exception& err)
  {
    CHECK(format("{}", err) == "c\n-> b\n-> a");
  }
}

TEST_CASE("format<optional>")
{
  CHECK(format("{}", optional<int>{80}) == "80");
  CHECK(format("{}", optional<int>{}) == "-null-");
}

TEST_CASE("format<time_point>")
{
  auto time = clock::from_time_t(1536713215); // 2018-09-12T00:46:55Z

  CHECK(format("{}", time) == "2018-09-12T00:46:55Z");
  CHECK(format("{:}", time) == "2018-09-12T00:46:55Z");
  CHECK(format("{:%Y %m %d %H %M %S}", time) == "2018 09 12 00 46 55");
}

TEST_CASE("format<duration>")
{
  CHECK(format("{}", duration{99s}) == "99");
  CHECK(format("{:g}", duration{99s}) == "99");
}

namespace steps
{
  enum class ut_enum { foo, bar };
  STEPS_ENUM_TRAITS(ut_enum, foo, bar);
}
TEST_CASE("format<enum>")
{
  CHECK(format("{}", ut_enum::foo) == "foo");
  CHECK(format("{}", ut_enum::bar) == "bar");
}
// LCOV_EXCL_STOP
