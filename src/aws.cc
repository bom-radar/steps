/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2020 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "aws.h"

#include "nc.h"
#include "core.h"
#include "profile.h"
#include "trace.h"
#include "version.h"

#include <aws/core/utils/logging/LogSystemInterface.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3-crt/model/GetObjectRequest.h>
#include <aws/s3-crt/model/PutObjectRequest.h>

#include <cstdio>
#include <cstdarg>
#include <fstream>

using namespace steps;
using namespace steps::aws;

static profile prof_aws_get_object_s3{"aws::get_object (s3)"};
static profile prof_aws_get_object_s3crt{"aws::get_object (s3crt)"};
static profile prof_aws_put_object_s3{"aws::put_object (s3)"};
static profile prof_aws_put_object_s3crt{"aws::put_object (s3crt)"};

static profile prof_aws_load_binary{"aws::load_binary_blob_s3"};
static profile prof_aws_load_netcdf{"aws::load_netcdf_s3"};
static profile prof_aws_output_netcdf{"aws::output_netcdf"};
static profile prof_aws_output_mosaic_tile{"aws::output_mosaic_tile"};
static profile prof_aws_output_cache{"aws::output_cache"};

/* note: the aws sdk takes into account several environment variables (e.g. AWS_REGION) when it is initializing.
 * this means we currently don't need to bother with supplying much configuration explicitly.  if we do one day
 * want to allow the user much more fine grained control over the options for the API and/or service clients,
 * then we should probably create a global variable (aws_config_path_) which is set in main() based on a
 * command line argument.  then, when we initialize the client() class from the singleton accessor, we can check
 * this variable and load the user selected config file on demand.  this seems like the least intrusive way to
 * get user config into the class while preserving our 'init on demand' behaviour. */

/* note: there are two different implementations of the S3Client interface available in the AWS SDK.  these are the
 * original implementation "S3Client" and a newer implementation based on the CRT libraries called "S3CrtClient".
 * the key difference is that the CRT library will split PutObject calls into a multi-part upload, and GetObjects
 * into multiple ranged requests, running each in parallel.  this allows the Crt client to achieve far greater
 * throughput for large files.
 *
 * unfortunately as of sdk 1.10.46 there is no clear winner in terms of performance.  S3Client is faster when
 * working with small files, while S3CrtClient is faster when working with large files.  for example, when tested
 * on an advection only forecast for the national australian domain with NO caching, the S3Client version saved
 * around 5 seconds (on a total runtime of around 180 seconds).  however when testing on a nwp blending mode for
 * the same domain where caching is enabled the S3CrtClient saved a whopping 370 seconds (360 down from 730)!
 *
 * it is tempting to use the crt client everywhere for simplicity and just put up with the time penalty incurred
 * by using it on small files.  however in the above tests that represents around 2% of total model runtime.  this
 * is a massive penalty considering it is pure API overhead.  instead for now we initialize both clients and then
 * allow the user to pick which ones should use the CRT client.  this is done by introducing a special URI form
 * for s3 that indicates the CRT client should be used:
 *   - s3://bucket/key/foo/bar = use S3Client
 *   - s3crt://bucket/key/foo/bar = use S3CrtClient
 *
 * most likely the cache and large product outputs will benefit from CRT and everything else should stick to the
 * legacy client.
 *
 * TODO periodically test the use of S3CrtClient everywhere with the latest SDK version to see if it has been
 * optimized for use with small files.  if we find S3CrtClient no longer has a significant penalty on small files
 * then transition to using it exclusively. */

namespace steps::aws
{
  class log_system : public Aws::Utils::Logging::LogSystemInterface
  {
  public:
    using LogLevel = Aws::Utils::Logging::LogLevel;

  public:
    log_system(LogLevel level);
    auto GetLogLevel(void) const -> LogLevel override;
    auto Log(LogLevel logLevel, char const* tag, char const* formatStr, ...) -> void override;
    auto LogStream(LogLevel logLevel, char const* tag, Aws::OStringStream const& messageStream) -> void override;
    auto Flush() -> void override;

  private:
    LogLevel  log_level_;
  };

  /* we derive from the DefaultCRTLogSystem instead of the CRTLogSystemInterface here because the "default"
   * class is the one that actually hooks into the CRT libraries.  if we overwrite the interface then we would
   * also need to provide our own hookups into CRT.  this effectively makes the interface class useless since
   * it won't be used anywhere. */
  class crt_log_system : public Aws::Utils::Logging::DefaultCRTLogSystem
  {
  public:
    using LogLevel = Aws::Utils::Logging::LogLevel;

  public:
    crt_log_system(LogLevel level);
    auto Log(LogLevel logLevel, const char* subjectName, const char* formatStr, va_list args) -> void override;
  };
}

static auto log_level_for_aws(Aws::Utils::Logging::LogLevel level)
{
  switch (level)
  {
  case Aws::Utils::Logging::LogLevel::Off:
    return trace_level::none;
  case Aws::Utils::Logging::LogLevel::Fatal:
    return trace_level::error;
  case Aws::Utils::Logging::LogLevel::Error:
  case Aws::Utils::Logging::LogLevel::Warn:
    return trace_level::warning;
  case Aws::Utils::Logging::LogLevel::Info:
  case Aws::Utils::Logging::LogLevel::Debug:
    return trace_level::log;
  case Aws::Utils::Logging::LogLevel::Trace:
    return trace_level::verbose;
  }
  throw std::logic_error{"Unreachable"};
}

log_system::log_system(LogLevel level)
  : log_level_{level}
{ }

auto log_system::GetLogLevel(void) const -> LogLevel
{
  return log_level_;
}

auto log_system::Log(LogLevel logLevel, char const* tag, char const* formatStr, ...) -> void
{
  char buf[1024];
  std::va_list args;
  va_start(args, formatStr);
  auto count = std::vsnprintf(buf, 1024, formatStr, args);
  va_end(args);
  buf[1023] = '\0';
  trace(log_level_for_aws(logLevel), "aws-sdk: {}{}", buf, count >= 1023 ? "... (truncated by STEPS)" : "");
}

auto log_system::LogStream(LogLevel logLevel, char const* tag, Aws::OStringStream const& messageStream) -> void
{
  trace(log_level_for_aws(logLevel), "aws-sdk: {}", messageStream.str());
}

auto log_system::Flush() -> void
{
  trace_flush_target();
}

crt_log_system::crt_log_system(LogLevel level)
  : DefaultCRTLogSystem(level)
{ }

auto crt_log_system::Log(LogLevel logLevel, const char* subjectName, const char* formatStr, va_list args) -> void
{
  // va_start / va_end already done by caller (see CRTLogSystem.cpp)
  char buf[1024];
  auto count = std::vsnprintf(buf, 1024, formatStr, args);
  buf[1023] = '\0';
  trace(log_level_for_aws(logLevel), "aws-sdk: {}{}", buf, count >= 1023 ? "... (truncated by STEPS)" : "");
}

aws_api::aws_api()
{
  // this sneaky environment variable helps when debugging connectivity issues with AWS in-place
  auto debug_aws = false;
  if (auto ptr = std::getenv("STEPS_DEBUG_AWS"))
    if (auto str = string_view{ptr}; str == "true" || str == "TRUE" || str == "1")
      debug_aws = true;

  // install a signal handler for SIGPIPE.  this is necessary when using the cURL backend since despite best efforts
  // the signal cannot be avoided in all circumstances (see https://curl.haxx.se/libcurl/c/CURLOPT_NOSIGNAL.html)
  options_.httpOptions.installSigPipeHandler = true;

  // setup aws log level, and install our custom interfaces so that aws sdk logs are redirected to our trace function
  auto level = debug_aws
    ? Aws::Utils::Logging::LogLevel::Trace // most verbose level ofered by sdk
    : Aws::Utils::Logging::LogLevel::Warn;
  options_.loggingOptions.logLevel = level;
  options_.loggingOptions.logger_create_fn = [level](){ return std::make_shared<log_system>(level); };
  options_.loggingOptions.crt_logger_create_fn = [level](){ return std::make_shared<crt_log_system>(level); };

  // initialize the API
  Aws::InitAPI(options_);
}

aws_api::~aws_api()
{
  Aws::ShutdownAPI(options_);
}

static inline auto create_client_configuration() -> Aws::Client::ClientConfiguration
{
  auto client_config = Aws::Client::ClientConfiguration{};
  client_config.requestTimeoutMs = 30000;

  /* the client config _should_ set smart defaults for configuration items based on the EC2 instance metadata.
   * most important of these is probably the region, which ensures our traffic is sent to local endpoints.
   * misconfigurations or other issues can prevent the API from reaching the metadata service however so we
   * log the detected region here to aid with diagnosis of metadata store issues later on. */
  trace(trace_level::notice, "AWS clients configured to use {} region", client_config.region);

  return client_config;
}

static inline auto create_s3crt_client_configuration() -> Aws::S3Crt::ClientConfiguration
{
  auto client_config = Aws::S3Crt::ClientConfiguration{};
  client_config.requestTimeoutMs = 30000;
  trace(
        trace_level::notice
      , "AWS S3Crt clients configured to use {} region with part size {} bytes, target throughput {} Gbps"
      , client_config.region
      , client_config.partSize
      , client_config.throughputTargetGbps);
  return client_config;
}

client::client()
  : client_config_{create_client_configuration()}
  , s3_{client_config_}
  , s3_crt_{create_s3crt_client_configuration()}
{ }

auto client::get() -> client&
{
  static client instance;
  return instance;
}

template <typename Request>
static inline auto set_bucket_and_key(Request& req, string_view uri, int prefix_length) -> void
{
  auto split = uri.find('/', prefix_length);
  if (split == string::npos)
    throw std::runtime_error{format("Invalid S3 URI {}", uri)};
  req.SetBucket(string(uri.substr(prefix_length, split - prefix_length)));
  req.SetKey(string(uri.substr(split + 1)));
}

// wrapper to a GetObject result that hides whether we used S3Client or S3CrtClient
struct get_object_result
{
  variant<std::monostate, Aws::S3::Model::GetObjectResult, Aws::S3Crt::Model::GetObjectResult> result;
  long long       content_length;
  Aws::IOStream*  body;

  get_object_result()
    : content_length{0}
    , body{nullptr}
  { }

  get_object_result(Aws::S3::Model::GetObjectResult&& res)
    : result{std::move(res)}
    , content_length{std::get<Aws::S3::Model::GetObjectResult>(result).GetContentLength()}
    , body{&std::get<Aws::S3::Model::GetObjectResult>(result).GetBody()}
  { }

  get_object_result(Aws::S3Crt::Model::GetObjectResult&& res)
    : result{std::move(res)}
    , content_length{std::get<Aws::S3Crt::Model::GetObjectResult>(result).GetContentLength()}
    , body{&std::get<Aws::S3Crt::Model::GetObjectResult>(result).GetBody()}
  { }
};

static auto s3_get_object(string_view uri, bool optional) -> get_object_result
{
  if (starts_with(uri, "s3crt://"sv))
  {
    auto ps = profile::scope{prof_aws_get_object_s3crt};
    auto req = Aws::S3Crt::Model::GetObjectRequest{};
    set_bucket_and_key(req, uri, "s3crt://"sv.size());
    auto res = client::get().s3_crt().GetObject(req);
    if (!res.IsSuccess())
    {
      // if the user flagged it as optional and file doesn't exist just return an empty config
      if (optional && res.GetError().GetErrorType() == Aws::S3Crt::S3CrtErrors::NO_SUCH_KEY)
        return {};

      throw std::runtime_error{format(
                "Failed to get S3 bucket {} key {}: {}: {}"
              , req.GetBucket()
              , req.GetKey()
              , res.GetError().GetExceptionName()
              , res.GetError().GetMessage())};
    }

    return res.GetResultWithOwnership();
  }
  else
  {
    auto ps = profile::scope{prof_aws_get_object_s3};
    auto req = Aws::S3::Model::GetObjectRequest{};
    set_bucket_and_key(req, uri, "s3://"sv.size());
    auto res = client::get().s3().GetObject(req);
    if (!res.IsSuccess())
    {
      // if the user flagged it as optional and file doesn't exist just return an empty config
      if (optional && res.GetError().GetErrorType() == Aws::S3::S3Errors::NO_SUCH_KEY)
        return {};

      throw std::runtime_error{format(
                "Failed to get S3 bucket {} key {}: {}: {}"
              , req.GetBucket()
              , req.GetKey()
              , res.GetError().GetExceptionName()
              , res.GetError().GetMessage())};
    }

    return res.GetResultWithOwnership();
  }
}

static auto s3_put_object(string_view uri, shared_ptr<Aws::IOStream> body) -> void
{
  if (starts_with(uri, "s3crt://"sv))
  {
    auto ps = profile::scope{prof_aws_put_object_s3crt};
    auto req = Aws::S3Crt::Model::PutObjectRequest{};
    set_bucket_and_key(req, uri, "s3crt://"sv.size());
    req.SetBody(std::move(body));
    if (auto res = client::get().s3_crt().PutObject(req); !res.IsSuccess())
      throw std::runtime_error{format(
                "Failed to put S3 bucket {} key {}: {}: {}"
              , req.GetBucket()
              , req.GetKey()
              , res.GetError().GetExceptionName()
              , res.GetError().GetMessage())};
  }
  else
  {
    auto ps = profile::scope{prof_aws_put_object_s3};
    auto req = Aws::S3::Model::PutObjectRequest{};
    set_bucket_and_key(req, uri, "s3://"sv.size());
    req.SetBody(std::move(body));
    if (auto res = client::get().s3().PutObject(req); !res.IsSuccess())
      throw std::runtime_error{format(
                "Failed to put S3 bucket {} key {}: {}: {}"
              , req.GetBucket()
              , req.GetKey()
              , res.GetError().GetExceptionName()
              , res.GetError().GetMessage())};
  }
}

auto steps::aws::load_configuration_s3(string_view uri, bool optional) -> configuration
{
  auto res = s3_get_object(uri, optional);
  return res.body ? configuration{*res.body} : configuration{};
}

auto steps::aws::save_configuration_s3(string_view uri, configuration const& data) -> void
{
  // disable exceptions after this since the AWS SDK always tries to read past end of stream
  // which will throw if we leave the exception flags set to fail and bad
  auto body = std::make_shared<std::stringstream>();
  body->exceptions(Aws::StringStream::failbit | Aws::StringStream::badbit);
  data.write(*body);
  body->exceptions(Aws::StringStream::goodbit);

  s3_put_object(uri, std::move(body));
}

auto steps::aws::load_binary_blob_s3(string_view uri, bool optional) -> array1<char>
{
  auto ps = profile::scope{prof_aws_load_binary};

  auto res = s3_get_object(uri, optional);
  if (!res.body)
    return {};

  // read the file contents out into a buffer
  // TODO THIS IS CRAZY - we really have to read from a stream like this?!?!?!?! redundant copy!
  if (res.content_length >= std::numeric_limits<array1<char>::size_type>::max())
    throw std::runtime_error{"Binary blob is too large for array1<char> container"};
  auto buffer = array1<char>{static_cast<array1<char>::size_type>(res.content_length)};
  res.body->read(buffer.data(), res.content_length);

  return buffer;
}

auto steps::aws::load_netcdf_s3(string_view uri, bool optional) -> shared_ptr<nc::file const>
{
  auto ps = profile::scope{prof_aws_load_netcdf};

  // get the file from S3
  auto res = s3_get_object(uri, optional);
  if (!res.body)
    return nullptr;

  // download the data into a buffer
  // TODO THIS IS CRAZY - we really have to read from a stream like this?!?!?!?! redundant copy!
  auto buffer = nc::file::buffer_ptr{reinterpret_cast<char*>(malloc(res.content_length))};
  res.body->read(buffer.get(), res.content_length);

  // open the netcdf file in-memory
  auto lock = nc::lock_mutex();
  return std::make_shared<nc::file>(string(uri), nc::io_mode::read_only, std::move(buffer), res.content_length);
}

output_stream_s3::output_stream_s3(string uri)
  : uri_{std::move(uri)}
  , body_{std::make_shared<std::stringstream>()}
{
  body_->exceptions(Aws::StringStream::failbit | Aws::StringStream::badbit);
}

output_stream_s3::~output_stream_s3()
{
  abort();
}

auto output_stream_s3::write(span<char const> data) -> void
{
  body_->write(data.data(), data.size());
}

auto output_stream_s3::commit() -> void
{
  // disable exceptions after this since the AWS SDK always tries to read past end of stream
  // which will throw if we leave the exception flags set to fail and bad
  body_->exceptions(Aws::StringStream::goodbit);

  s3_put_object(uri_, std::move(body_));

  uri_.clear();
}

auto output_stream_s3::abort() -> void
{
  uri_.clear();
}

input_netcdf_s3::input_netcdf_s3(configuration const& config, metadata const& user_parameters)
  : input_netcdf{config}
  , user_parameters_{&user_parameters}
  , path_format_{config["path"]}
{ }

auto input_netcdf_s3::determine_path(time_point reference_time, int member, time_point time) const -> string
try
{
  auto lead = time - reference_time;
  auto lead_hr = std::chrono::duration_cast<std::chrono::hours>(lead);
  auto lead_min = std::chrono::duration_cast<std::chrono::minutes>(lead - lead_hr);
  return format(
        path_format_
      , "param"_a = *user_parameters_
      , "member"_a = member
      , "time"_a = time
      , "reference"_a = reference_time
      , "lead"_a = lead
      , "lead_hr"_a = lead_hr.count()
      , "lead_min"_a = lead_min.count()
      );
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Path format '{}' is invalid", path_format_)});
}

auto input_netcdf_s3::open_file(string_view path) -> shared_ptr<nc::file const>
{
  return load_netcdf_s3(path, true);
}

output_netcdf_s3::output_netcdf_s3(configuration const& config, metadata const& user_parameters)
  : output_netcdf{config}
  , user_parameters_{&user_parameters}
  , path_format_{config["path"]}
{ }

auto output_netcdf_s3::finalize() -> void
{
  auto ps = profile::scope{prof_aws_output_netcdf};

  close_file();

  try
  {
    auto body = std::make_shared<std::fstream>(tmp_path_, std::fstream::in | std::fstream::binary);
    s3_put_object(final_path_, std::move(body));
  }
  catch (...)
  {
    cleanup_temp_file();
    final_path_.clear();
    throw;
  }

  cleanup_temp_file();
  final_path_.clear();
}

auto output_netcdf_s3::abort() -> void
{
  close_file();
  cleanup_temp_file();
  final_path_.clear();
}

auto output_netcdf_s3::create_file(time_point reference_time, int first_member) -> nc::file
{
  // determine the S3 key that we will upload the completed product to
  try
  {
    final_path_ = first_member != -1
      ? format(path_format_, "param"_a = *user_parameters_, "reference"_a = reference_time, "member"_a = first_member)
      : format(path_format_, "param"_a = *user_parameters_, "reference"_a = reference_time);
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Path format '{}' is invalid", path_format_)});
  }

  // determine a temporary path for our file on disk during write
  tmp_path_ = "tmp.XXXXXX";
  mktemp(tmp_path_.data());
  if (tmp_path_[0] == '\0')
    throw std::system_error{errno, std::generic_category(), "mktemp"};

  // create the file
  auto lock = nc::lock_mutex();
  return nc::file{tmp_path_, nc::io_mode::create};
}

auto output_netcdf_s3::cleanup_temp_file() -> void
{
  if (auto ec = std::error_code{}; !tmp_path_.empty() && !std::filesystem::remove(tmp_path_, ec) && ec)
    trace(trace_level::error, "Failed to remove temporary output file {}: {}", tmp_path_, ec.message());
  tmp_path_.clear();
}

output_mosaic_tile_s3::output_mosaic_tile_s3(configuration const& config, metadata const& user_parameters)
  : output_mosaic_tile{config}
  , user_parameters_{&user_parameters}
  , path_format_{config["path"]}
{ }

auto output_mosaic_tile_s3::initialize(product const& parent, time_point reference_time) -> void
{
  // store current reference time and create our output stream
  reference_time_ = reference_time;
  stream_ = std::make_shared<std::stringstream>(std::stringstream::in | std::stringstream::out | std::stringstream::binary);
  stream_->exceptions(std::stringstream::failbit | std::stringstream::badbit);

  // call our parent class so that it dumps out the file header
  output_mosaic_tile::initialize(parent, reference_time);
}

auto output_mosaic_tile_s3::output_chunk(span<char const> chunk) -> void
{
  // write is called from engine threads, we must use a mutex to ensure we output one chunk at at time
  auto lock = std::lock_guard<std::mutex>{mut_stream_};
  stream_->write(chunk.data(), chunk.size());
}

auto output_mosaic_tile_s3::finalize() -> void
{
  auto ps = profile::scope{prof_aws_output_mosaic_tile};

  try
  {
    // disable stream exceptions this since the AWS SDK always tries to read past end of stream
    // which will throw if we leave the exception flags set to fail and bad
    stream_->exceptions(std::stringstream::goodbit);

    auto uri = format(path_format_, "param"_a = *user_parameters_, "reference"_a = reference_time_);
    s3_put_object(uri, std::move(stream_));
  }
  catch (...)
  {
    reference_time_ = time_point{};
    stream_.reset();
    throw;
  }

  reference_time_ = time_point{};
  stream_.reset();
}

auto output_mosaic_tile_s3::abort() -> void
{
  reference_time_ = time_point{};
  stream_.reset();
}

persistent_cache_s3::persistent_cache_s3(
      string_view stream_name
    , time_point reference_time
    , time_point bf_reference_time
    , string_view path
    , int compression
    , int precision_reduction)
  : base_dir_{path}
  , compression_{compression}
  , precision_reduction_{precision_reduction}
{
  base_dir_.append("/"sv).append(stream_name);
  if (reference_time != time_point{})
    base_dir_.append(format(".{:%Y%m%dT%H%M%S}", reference_time));
  if (bf_reference_time != time_point{})
    base_dir_.append(format(".{:%Y%m%dT%H%M%S}", bf_reference_time));
}

auto persistent_cache_s3::write(cache_key const& key, stream_state const& state) -> void
{
  auto ps = profile::scope{prof_aws_output_cache};

  auto path = format("{}/{:%Y%m%dT%H%M%S}.{}.{}", base_dir_, key.time, key.nwp_member, key.bf_nwp_member);
  try
  {
    // disable exceptions after this since the AWS SDK always tries to read past end of stream
    // which will throw if we leave the exception flags set to fail and bad
    auto body = std::make_shared<std::stringstream>();
    body->exceptions(Aws::StringStream::failbit | Aws::StringStream::badbit);
    state.write(*body, compression_, precision_reduction_);
    body->exceptions(Aws::StringStream::goodbit);

    s3_put_object(path, std::move(body));
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to write cache entry {}", path)});
  }
}

auto persistent_cache_s3::read(cache_key const& key, stream_state& state) -> bool
{
  auto path = format("{}/{:%Y%m%dT%H%M%S}.{}.{}", base_dir_, key.time, key.nwp_member, key.bf_nwp_member);
  try
  {
    // get the file from S3
    auto res = s3_get_object(path, true);
    if (!res.body)
      return false;

    // read the content into our stream object
    res.body->exceptions(Aws::IOStream::eofbit | Aws::IOStream::failbit | Aws::IOStream::badbit);
    state.read(*res.body);
  }
  catch (...)
  {
    std::throw_with_nested(std::runtime_error{format("Failed to read cache entry {}", path)});
  }

  return true;
}

verification_io_netcdf_s3::verification_io_netcdf_s3(
      verification const& c
    , configuration const& config
    , metadata const& user_parameters)
  : verification_io_netcdf{c, config}
  , user_parameters_{&user_parameters}
  , path_format_{config["path"]}
{ }

auto verification_io_netcdf_s3::write(
      time_point from
    , time_point till
    , verification::score_store const& scores
    ) -> void
{
  // determine the S3 key that we will upload the completed product to
  auto path = determine_path(from, till);

  // determine a temporary path for our file on disk during write
  auto tmp_path = "tmp.XXXXXX"s;
  mktemp(tmp_path.data());
  if (tmp_path[0] == '\0')
    throw std::system_error{errno, std::generic_category(), "mktemp"};

  try
  {
    // write the file
    {
      auto lock = nc::lock_mutex();
      auto file = nc::file{tmp_path, nc::io_mode::create};
      write_impl(from, till, scores, file);
    }

    // upload to s3
    auto body = std::make_shared<std::fstream>(tmp_path, std::fstream::in | std::fstream::binary);
    s3_put_object(path, std::move(body));
  }
  catch (...)
  {
    /* We try to remove the output file if this is not a successful outcome.  If we fail to remove the file just trace
     * it as a non-fatal error - we just give a best-effort guarantee for cleanup during forecast failure. */
    if (auto ec = std::error_code{}; std::filesystem::exists(path, ec) && !std::filesystem::remove(path, ec) && ec)
      trace(trace_level::error, "Failed to remove partial output file {}: {}", path, ec.message());
    throw;
  }
}

auto verification_io_netcdf_s3::read(
      string_view path
    , time_point& from
    , time_point& till
    , verification::score_store& scores
    ) -> bool
{
  // get the file from S3
  auto res = s3_get_object(path, true);
  if (!res.body)
    return false;

  // download the data into a buffer
  // TODO THIS IS CRAZY - we really have to read from a stream like this?!?!?!?! redundant copy!
  auto buffer = nc::file::buffer_ptr{reinterpret_cast<char*>(malloc(res.content_length))};
  res.body->read(buffer.get(), res.content_length);

  // open the netcdf file in-memory
  auto lock = nc::lock_mutex();
  auto file = nc::file{string(path), nc::io_mode::read_only, std::move(buffer), size_t(res.content_length)};

  // read the scores
  read_impl(file, from, till, scores);
  return true;
}

auto verification_io_netcdf_s3::determine_path(time_point from, time_point till) const -> string
try
{
  return format(path_format_, "param"_a = *user_parameters_, "from"_a = from, "till"_a = till);
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{format("Path format '{}' is invalid", path_format_)});
}
