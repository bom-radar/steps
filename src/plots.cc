/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "plots.h"
#include "nc.h"
#include "trace.h"
#include <fstream>

using namespace steps;

using file_store = vector<nc::file>;

constexpr auto rel_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x',
  yaxis: 'y',
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: [{xx}],
  y: [{yy}],
}}
)STR";

constexpr auto rel_hist_trace =
R"STR(
{{
  type: 'bar',
  xaxis: 'x3',
  yaxis: 'y3',
  showlegend: false,
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: [{xx}],
  y: [{yy}],
}}
)STR";

constexpr auto roc_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x2',
  yaxis: 'y2',
  showlegend: false,
  hoverinfo: 'text',
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: [0, {fars} 1],
  y: [0, {pods} 1],
  text: [1, {prbs} 0],
}}
)STR";

constexpr auto crps_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x4',
  yaxis: 'y4',
  showlegend: false,
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: lead_times,
  y: [{yy}],
}}
)STR";

constexpr auto spread_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x5',
  yaxis: 'y5',
  showlegend: false,
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: [{xx}],
  y: [{yy}],
}}
)STR";

constexpr auto rank_trace =
R"STR(
{{
  type: 'bar',
  xaxis: 'x6',
  yaxis: 'y6',
  showlegend: false,
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  y: [{yy}],
}}
)STR";

constexpr auto rmse_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x7',
  yaxis: 'y7',
  showlegend: false,
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: lead_times,
  y: [{yy}],
}}
)STR";

constexpr auto rmse_spread_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x7',
  yaxis: 'y7',
  showlegend: false,
  line: {{ color: 'red', dash: 'dash' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: lead_times,
  y: [{yy}],
}}
)STR";

constexpr auto correl_trace =
R"STR(
{{
  type: 'scatter',
  xaxis: 'x8',
  yaxis: 'y8',
  showlegend: false,
  line: {{ color: 'red' }},
  marker: {{ color: 'red' }},
  mode: 'lines',
  x: lead_times,
  y: [{yy}],
}}
)STR";

template <typename T>
static auto make_js_array(array1<T> const& data, char const* spec = "{}") -> string
{
  auto str = string{};
  for (auto const& v : data)
    if constexpr (std::is_floating_point_v<T>)
      str.append(std::isnan(v) ? "NaN" : format(spec, v)).append(",");
    else
      str.append(format(spec, v)).append(",");
  return str;
}

static auto write_model_names(vector<string> const& names, file_store const& files, std::ofstream& fout)
{
  fout << "var model_names = [";
  string name;
  for (size_t i = 0; i < files.size(); ++i)
  {
    if (names[i].empty())
      files[i].lookup_variable("model").read(span(&name, 1));
    else
      name = names[i];
    fout << "'" << name << "',";
  }
  fout << "];\n";
}

static auto write_lead_times(file_store const& files, std::ofstream& fout)
{
  auto& var = files[0].lookup_variable("lead_time");
  if (var.dimensions().size() != 1)
    throw std::runtime_error{"Invalid lead_time variable"};
  auto data = array1i{var.dimensions()[0]->size()};
  var.read(data);

  // sanity check all inputs use same lead times
  auto data2 = array1i{data.size()};
  for (auto& f : files)
  {
    try
    {
      f.lookup_variable("lead_time").read(data2);
    }
    catch (std::exception& err)
    {
      std::throw_with_nested(std::runtime_error{"Mismatch in input lead times"});
    }
    for (auto i = 0; i < data.size(); ++i)
      if (data[i] != data2[i])
        throw std::runtime_error{"Mismatch in input lead times"};
  }

  // convert to minutes for user convenience
  auto data_min = array1f{data.size()};
  for (auto i = 0; i < data.size(); ++i)
    data_min[i] = data[i] / 60.0f;

  auto strdata = make_js_array(data_min);
  fout << "var lead_times = [" << strdata << "];\n";
}

static auto write_thresholds(file_store const& files, std::ofstream& fout)
{
  auto& var = files[0].lookup_variable("threshold");
  if (var.dimensions().size() != 1)
    throw std::runtime_error{"Invalid threshold variable"};
  auto data = array1f{var.dimensions()[0]->size()};
  var.read(data);
  auto strdata = make_js_array(data, "{:g}");
  fout << "var thresholds = [" << strdata << "];\n";

  // sanity check all inputs use same thresholds
  for (auto& f : files)
  {
    try
    {
      f.lookup_variable("threshold").read(data);
    }
    catch (std::exception& err)
    {
      std::throw_with_nested(std::runtime_error{"Mismatch in input thresholds"});
    }
    if (strdata != make_js_array(data, "{:g}"))
      throw std::runtime_error{"Mismatch in input thresholds"};
  }
}

static auto write_roc_curves(file_store const& files, std::ofstream& fout)
{
  fout << "var roc_traces = [";
  for (auto& f : files)
  {
    auto& vprb = f.lookup_variable("roc_probability");
    if (vprb.dimensions().size() != 1)
      throw std::runtime_error{"Invalid roc data"};
    auto probs = array1f{vprb.dimensions()[0]->size()};
    vprb.read(probs);
    auto prbstr = make_js_array(probs, "'T={:0.2f}'");

    auto& vpod = f.lookup_variable("roc_pod");
    if (vpod.dimensions().size() != 3)
      throw std::runtime_error{"Invalid roc data"};

    auto& vfar = f.lookup_variable("roc_far");
    if (vfar.dimensions().size() != 3)
      throw std::runtime_error{"Invalid roc data"};

    auto pods = array1f(probs.size());
    auto fars = array1f(probs.size());

    fout << "[";
    for (auto ilt = 0; ilt < vpod.dimensions()[0]->size(); ++ilt)
    {
      fout << "[";
      for (auto ith = 0; ith < vpod.dimensions()[1]->size(); ++ith)
      {
        vpod.read(pods, {ilt, ith});
        vfar.read(fars, {ilt, ith});
        fout << format(roc_trace, "fars"_a=make_js_array(fars), "pods"_a=make_js_array(pods), "prbs"_a=prbstr) << ",";
      }
      fout << "],";
    }
    fout << "],";
  }
  fout << "];\n";
}

static auto write_rel_curves(file_store const& files, std::ofstream& fout)
{
  fout << "var rel_traces = [";
  for (auto& f : files)
  {
    auto& vx = f.lookup_variable("reliability_probability");
    if (vx.dimensions().size() != 1)
      throw std::runtime_error{"Invalid rel data"};
    auto xx = array1f{vx.dimensions()[0]->size()};
    vx.read(xx);
    auto xxstr = make_js_array(xx);

    auto& vy = f.lookup_variable("reliability_observed_frequency");
    if (vy.dimensions().size() != 3)
      throw std::runtime_error{"Invalid rel data"};
    auto yy = array1f{vy.dimensions()[2]->size()};

    fout << "[";
    for (auto ilt = 0; ilt < vy.dimensions()[0]->size(); ++ilt)
    {
      fout << "[";
      for (auto ith = 0; ith < vy.dimensions()[1]->size(); ++ith)
      {
        vy.read(yy, {ilt, ith});
        fout << format(rel_trace, "xx"_a=xxstr, "yy"_a=make_js_array(yy)) << ",";
      }
      fout << "],";
    }
    fout << "],";
  }
  fout << "];\n";

  auto& vc = files.back().lookup_variable("reliability_climatology");
  if (vc.dimensions().size() != 2)
    throw std::runtime_error{"Invalid rel data"};
  auto cc = array1f{vc.dimensions()[1]->size()};
  fout << "var rel_climatologies = [";
  for (auto ilt = 0; ilt < vc.dimensions()[0]->size(); ++ilt)
  {
    vc.read(cc, {ilt});
    fout << "[" << make_js_array(cc) << "],";
  }
  fout << "];\n";
}

static auto write_rel_histograms(file_store const& files, std::ofstream& fout)
{
  fout << "var rel_hist_traces = [";
  for (auto& f : files)
  {
    auto& vx = f.lookup_variable("reliability_probability");
    if (vx.dimensions().size() != 1)
      throw std::runtime_error{"Invalid rel data"};
    auto xx = array1f{vx.dimensions()[0]->size()};
    vx.read(xx);
    auto xxstr = make_js_array(xx);

    auto& vy = f.lookup_variable("reliability_forecasts");
    if (vy.dimensions().size() != 3)
      throw std::runtime_error{"Invalid rel data"};
    auto yy = array1d{vy.dimensions()[2]->size()};

    fout << "[";
    for (auto ilt = 0; ilt < vy.dimensions()[0]->size(); ++ilt)
    {
      fout << "[";
      for (auto ith = 0; ith < vy.dimensions()[1]->size(); ++ith)
      {
        vy.read(yy, {ilt, ith});
        fout << format(rel_hist_trace, "xx"_a=xxstr, "yy"_a=make_js_array(yy)) << ",";
      }
      fout << "],";
    }
    fout << "],";
  }
  fout << "];\n";
}

static auto write_spread_reliabilities(file_store const& files, std::ofstream& fout)
{
  float spread_max = 0.0f;
  fout << "var spread_traces = [";
  for (auto& f : files)
  {
    auto& vx = f.lookup_variable("spread_rmse");
    if (vx.dimensions().size() != 1)
      throw std::runtime_error{"Invalid spread rel data"};
    auto xx = array1f{vx.dimensions()[0]->size()};
    vx.read(xx);
    auto xxstr = make_js_array(xx);

    if (xx[xx.size()-1] > spread_max)
      spread_max = xx[xx.size()-1];

    auto& vy = f.lookup_variable("spread_spread");
    if (vy.dimensions().size() != 2)
      throw std::runtime_error{"Invalid spread rel data"};
    auto yy = array1f{vy.dimensions()[1]->size()};

    fout << "[";
    for (auto ilt = 0; ilt < vy.dimensions()[0]->size(); ++ilt)
    {
      vy.read(yy, {ilt});
      fout << format(spread_trace, "xx"_a=xxstr, "yy"_a=make_js_array(yy)) << ",";
    }
    fout << "],";
  }
  fout << "];\n";
  fout << "var spread_max = " << spread_max << ";\n";
}

static auto write_crps_domain_curves(file_store const& files, std::ofstream& fout)
{
  fout << "var crps_traces = [";
  for (auto& f : files)
  {
    auto& vcrps = f.lookup_variable("crps_domain");
    auto data = array1f(vcrps.dimensions()[0]->size());
    vcrps.read(data);
    fout << format(crps_trace, "yy"_a=make_js_array(data)) << ",";
  }
  fout << "];\n";
}

static auto write_rank_histograms(file_store const& files, std::ofstream& fout)
{
  fout << "var rank_traces = [";
  for (auto& f : files)
  {
    auto& vy = f.lookup_variable("rank_frequency");
    if (vy.dimensions().size() != 2)
      throw std::runtime_error{"Invalid rel data"};
    auto yy = array1f{vy.dimensions()[1]->size()};

    fout << "[";
    for (auto ilt = 0; ilt < vy.dimensions()[0]->size(); ++ilt)
    {
      vy.read(yy, {ilt});
      fout << format(rank_trace, "yy"_a=make_js_array(yy)) << ",";
    }
    fout << "],";
  }
  fout << "];\n";
}

static auto write_rmse(file_store const& files, std::ofstream& fout)
{
  fout << "var rmse_traces = [";
  for (auto& f : files)
  {
    auto& vrmse = f.lookup_variable("rmse_rmse_ens_mean");
    auto data = array1f(vrmse.dimensions()[0]->size());
    vrmse.read(data);
    fout << format(rmse_trace, "yy"_a=make_js_array(data)) << ",";
  }
  fout << "];\n";

  fout << "var rmse_spread_traces = [";
  for (auto& f : files)
  {
    auto& vrmse = f.lookup_variable("rmse_rms_spread");
    auto data = array1f(vrmse.dimensions()[0]->size());
    vrmse.read(data);
    fout << format(rmse_spread_trace, "yy"_a=make_js_array(data)) << ",";
  }
  fout << "];\n";
}

static auto write_correlation(file_store const& files, std::ofstream& fout)
{
  fout << "var correl_traces = [";
  for (auto& f : files)
  {
    auto& vcorrel = f.lookup_variable("correl_mean");
    auto data = array1f(vcorrel.dimensions()[0]->size());
    vcorrel.read(data);
    fout << format(correl_trace, "yy"_a=make_js_array(data)) << ",";
  }
  fout << "];\n";
}

static auto copy_file_to_stream(std::filesystem::path const& path, std::ostream& out)
{
  auto file = std::ifstream(path, std::ios::binary);
  if (!file)
    throw std::runtime_error{format("Failed to read {}", path.native())};
  out << file.rdbuf();
}

auto steps::run_generate_plots(
      std::filesystem::path const& reference_data_dir
    , std::filesystem::path const& path_out
    , vector<std::filesystem::path> const& paths_in
    , vector<string> const& names_in
    ) -> int
{
  // open our input files
  auto files = file_store{};
  for (auto& p : paths_in)
    files.emplace_back(p, nc::io_mode::read_only);

  // write our html file
  auto fout = std::ofstream{path_out};
  fout << "<html>\n";
  copy_file_to_stream(reference_data_dir / "plots.html", fout);
  fout << "<script type=\"text/javascript\">\n";
  write_model_names(names_in, files, fout);
  write_lead_times(files, fout);
  write_thresholds(files, fout);
  write_roc_curves(files, fout);
  write_rel_curves(files, fout);
  write_rel_histograms(files, fout);
  write_spread_reliabilities(files, fout);
  write_crps_domain_curves(files, fout);
  write_rank_histograms(files, fout);
  write_rmse(files, fout);
  write_correlation(files, fout);
  copy_file_to_stream(reference_data_dir / "plots.js", fout);
  fout << "</script>\n";
  fout << "</html>\n";

  return EXIT_SUCCESS;
}
