/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"

namespace steps
{
  /// Methods for normalization of blend weights so they sum to 1.0
  enum class blend_weight_normalization
  {
      none          ///< Do not normalize blend weights
    , standard      ///< Normalize by scaling all weights evenly
    , only_first    ///< Scale only the first weight
    , only_last     ///< Scale only the last weight
    , except_first  ///< Scale all weights except the first
    , except_last   ///< Scale all weights except the last
  };

  /// Calculate blend weights based on a covariance matrix and skills vector
  /* The blend weights are calculated as:
   *   w = covmm^-1.covmo
   * Where:
   *   - covmm is the matrix of the covariance/correlations between the models
   *   - covmo is the vector of covariances/correlations between each model and the observation (i.e. skill)
   *   - ^-1 denotes the matrix inverse
   *
   * These weights may then be normalized so that they sum to 1.0 depending on the mode provided by the user.
   */
  auto calculate_blend_weights(array2d const& covars, array1d const& skills, blend_weight_normalization mode) -> array1f;

  /// Constrain a covariance matrix based on skills
  /** Given two input variables with known skills, there is a maximum covariance that should be observed between
   *  them.  For inputs with similar skills it is okay for them to be very well correlated, however for inputs with
   *  significantly different skills it does not make sense that they could also be highly correlated with each other.
   *  When this occurs our blend weights calculation breaks down and we end up with very high positive and negative
   *  blend weights.
   *
   *  This function eliminates problematic inconsistency between the skills of inputs, and their covariance by
   *  artifically limiting the covariance.  An alternative approach would be to reduce the spread of the skills of the
   *  inputs.
   *
   *  At the same time, this function also clamps negative covariances to 0.  Although we may have calculated a negative
   *  covariance based on the 'sample' that is the current inputs, it doesn't make sense from a logical point of view.
   *  Having a negative covariance can lead to the odd situation where we try to exploit the negatively correlated
   *  input using negative weights.  Negative correlations between inputs can happen (for example) when one has rain
   *  on the opposite side of the domain to the other.  This will lead to the negative side of the large scale wave
   *  from one input aligning well with the positive side of the same wave in the other.  In this case the reality
   *  is that the inputs have no 'real' information in common since they have completely different answers about where
   *  the rain is.  The apparent negative covariance is really just an artifact of the domain size, and scale
   *  decomposition.
   */
  auto constrain_covariances(array2d& covars, array1d const& skills, bool verbose) -> void;
}
