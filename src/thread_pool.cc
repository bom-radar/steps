/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2022 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "thread_pool.h"
#include "trace.h"

using namespace steps;

static vector<std::thread> threads_;

static std::mutex mut_tasks_;
static std::condition_variable cv_tasks_;
static int size_ = 0;
static list<std::packaged_task<void()>> tasks_;
static uint64_t start_count_ = 0;
static std::once_flag flag_atexit_setup_;

static auto thread_worker(int id) -> void;

auto steps::thread_pool_size() -> int
{
  auto lock = std::lock_guard<std::mutex>{mut_tasks_};
  return size_;
}

auto steps::thread_pool_set_size(int threads) -> void
{
  {
    auto lock = std::lock_guard<std::mutex>{mut_tasks_};
    size_ = threads;
  }

  // launch any new threads we need
  if (threads > int(threads_.size()))
  {
    for (auto i = int(threads_.size()); i < threads; ++i)
      threads_.emplace_back(thread_worker, i);
  }
  // join any excess threads we no longer need
  else if (threads < int(threads_.size()))
  {
    cv_tasks_.notify_all();
    for (auto i = threads; i < int(threads_.size()); ++i)
      threads_[i].join();
    threads_.resize(threads);
  }

  // ensure that the thread pool is shutdown at exit
  std::call_once(flag_atexit_setup_, []() { atexit([]() { thread_pool_set_size(0); }); });
}

auto steps::thread_pool_process_task() -> bool
{
  auto lock = std::unique_lock<std::mutex>{mut_tasks_};

  // is there any work to do?
  if (tasks_.empty())
    return false;

  // grab the first task for ourselves
  list<std::packaged_task<void()>> task;
  task.splice(task.end(), tasks_, tasks_.begin());
  ++start_count_;

  // unlock while we run the task
  // packaged_task captures exceptions so no need for the normal try/catch here
  lock.unlock();
  task.front()();

  return true;
}

auto steps::detail::thread_pool_queue_task(std::packaged_task<void()> task) -> uint64_t
{
  int end_count;
  {
    auto lock = std::lock_guard<std::mutex>{mut_tasks_};
    tasks_.emplace_back(std::move(task));
    end_count = start_count_ + 1;
  }
  cv_tasks_.notify_one();
  return end_count;
}

auto steps::detail::thread_pool_distribute_loop(list<std::packaged_task<void()>> tasks) -> void
{
  if (tasks.empty())
    return;

  // collect up our futures
  auto futures = vector<std::future<void>>();
  futures.reserve(tasks.size());
  for (auto& t : tasks)
    futures.emplace_back(t.get_future());

  // queue the tasks and process jobs while we wait
  {
    auto lock = std::unique_lock<std::mutex>{mut_tasks_};

    // splice the task list into our queue
    tasks_.splice(tasks_.end(), tasks);

    // once start_count_ is equal to this value we know all iterations have been picked up by a thread
    auto end_count = start_count_ + tasks_.size();

    // wake up to one less thread than the number of iterations since we will also be processing
    /* it is theoretically pessimistic to do this while the mutex is still locked, but apparently pthreads handles
     * it nicely and doesn't actually wake the thread and immediately block it again, but instead just transfers the
     * notified thread from the blocked queue on the condition variable to the blocked queue on the mutex. */
    for (size_t i = 1; i < futures.size(); ++i)
      cv_tasks_.notify_one();

    // process tasks as if we are a worker thread until all of our iterations have been dequeued
    while (start_count_ < end_count)
    {
      // grab the first task for ourselves
      list<std::packaged_task<void()>> task;
      task.splice(task.end(), tasks_, tasks_.begin());
      ++start_count_;

      // unlock while we run the task
      // packaged_task captures exceptions so no need for the normal try/catch here
      lock.unlock();
      task.front()();
      lock.lock();
    }
  }

  // get the result from each iteration so that any exceptions are propagated
  try
  {
    for (auto& f : futures)
      f.get();
  }
  catch (...)
  {
    // ensure that all other iterations finished before we propagate the exception
    for (auto& f : futures)
      if (f.valid())
        f.wait();
    throw;
  }
}

auto steps::detail::thread_pool_work_until(uint64_t end_count) -> void
{
  auto lock = std::unique_lock<std::mutex>{mut_tasks_};

  // process tasks as if we are a worker thread until the desired end count is reached
  while (start_count_ < end_count)
  {
    // grab the first task for ourselves
    list<std::packaged_task<void()>> task;
    task.splice(task.end(), tasks_, tasks_.begin());
    ++start_count_;

    // unlock while we run the task
    // packaged_task captures exceptions so no need for the normal try/catch here
    lock.unlock();
    task.front()();
    lock.lock();
  }
}

static auto thread_worker(int id) -> void
{
  auto prefix = trace_prefix{format("w{}", id)};

  auto lock = std::unique_lock<std::mutex>{mut_tasks_};
  while (id < size_)
  {
    // idle until we have new work
    if (tasks_.empty())
    {
      cv_tasks_.wait(lock);
      continue;
    }

    // pop the first task from the queue and increment our next id
    list<std::packaged_task<void()>> task;
    task.splice(task.end(), tasks_, tasks_.begin());
    ++start_count_;

    // unlock while we run the tasks
    // packaged_task captures exceptions so no need for the normal try/catch here
    lock.unlock();
    task.front()();
    lock.lock();
  }
}
