#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
import sys
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np

# constants
figsize=(8, 6)
figsize_sq=(6, 6)

# open the diagnostics file and setup all global data
def open_diagnostics(path):
    global cdf
    global times, levels, ny, nx

    # open the file
    cdf = nc.Dataset(sys.argv[1])

    # get dimension sizes
    times = cdf.dimensions["forecast"].size
    levels = cdf.dimensions["level"].size
    ny = cdf.dimensions["y"].size
    nx = cdf.dimensions["x"].size

# plot cascade level stats over time
def plot_level_stats(ilvl):
    data = cdf.variables["cascade"][:,ilvl]
    means = np.mean(data, (1,2))
    stddevs = np.std(data, (1,2))

    plt.clf()

    cmap = plt.get_cmap("tab10")

    ax1 = plt.gca()
    ax1.set_xlabel("Forecast")
    ax1.set_ylabel("Mean", color=cmap(0))
    ax1.tick_params(axis='y', labelcolor=cmap(0))
    ax1.set_ylim(-1.5, 1.5)
    ax1.plot(range(times), means, color=cmap(0))

    ax2 = ax1.twinx()
    ax2.set_ylabel("StdDev", color=cmap(1))
    ax2.tick_params(axis='y', labelcolor=cmap(1))
    ax2.plot(range(times), stddevs, color=cmap(1))

    ax1.plot((0, times), (0, 0), color=cmap(0), ls='--')
    ax2.plot((0, times), (1, 1), color=cmap(1), ls='--')

    plt.title("Cascade Stats Level {}".format(ilvl))
    plt.savefig("images/stats_lvl{}.png".format(ilvl))
    plt.show()

# entry point
if __name__ == '__main__':

    if (len(sys.argv) != 2):
        print("Usage: python diagnostics.py (input.nc)")
        exit(0)

    open_diagnostics(sys.argv[1])

    for ilvl in range(levels):
        plot_level_stats(ilvl)
