/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array1.h"
#include "array2.h"
#include "configuration.h"
#include "io.h"
#include "util.h"

#include <mutex>

namespace steps
{
  /// Series types that can be output by a product
  enum class product_series_type
  {
      none        ///< Product does not output multiple series
    , member      ///< Each series is a different ensemble member
    , probability ///< Each series is a different probability threshold
    , percentile  ///< Each series is a different percentile threshold
  };
  STEPS_ENUM_TRAITS(product_series_type, none, member, probability, percentile);

  /// Units that can be output by a product
  enum class product_units
  {
      rate          ///< Instantaneous rain rate
    , accumulation  ///< Rainfall accumulation
    , probability   ///< Probability
  };
  STEPS_ENUM_TRAITS(product_units, rate, accumulation, probability);

  /// Base class for all product types
  class product
  {
  public:
    /// Constructor
    product(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    /// Virtual destructor
    virtual ~product() = default;

    /// Setup for model run
    /** This function is called once at the start of a model run.  The parent is provided here instead of during the
     *  constructor, because this implies that the parent, or in reality, important characteristics of the parent, may
     *  change between forecasts.  The most obvious example of this is a clock synchronized accumulation that will
     *  output different lead times depending on the reference time (so that they line up nicely on whole hours etc).
     *  Implementing products should therefore make no assumptions about the values returned by the parent object for
     *  subsequent calls to initialize(). */
    virtual auto initialize(product const& parent, time_point reference_time) -> void;

    /// Write an individual forecast
    /** This function may be called from multiple threads, and possibly from multiple threads at the same time.  It is
     *  therefore the responsibility of the implementing class to ensure that this function is thread safe.  The
     *  following guarantees are given by the engine:
     *  - All calls to write() will occur after initialize() and before finalize()
     *  - Each call to write() will be for a different member and time
     *  Both the member and forecast variables are 0 based.  The first member to be output will always be passed as
     *  member index 0 here, and similarly the first time step to output will always be index 0.
     *  The flow parameter, if provided, represents the flow vectors between subsequent forecasts for the given
     *  member as passed to this product.  If a product performs an operation that would mean the flow field is
     *  no longer related to data passed to children it should either provide a modified field (unlikely) or simply
     *  pass a null pointer to indicate flow data is not available.  If a child product requires flow data but
     *  none has been provided it should throw an exception.  This would represent an invalid nesting of products
     *  in the user configuration. */
    virtual auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void;
    virtual auto write(int member, int forecast, array2<int8_t> const& data, array2f2 const* flow) -> void;

    /// Write all forecasts using the same input
    /** This function causes a product to generate output as if an idendical set of inputs had been passed for
     *  all combinations of member and forecast.  In practice this only occurs during a null forecast, and the array
     *  of passed values will contain either all zeros or all nans.  The intention of this function is that products
     *  optimise away some processing for these cases, but otherwise the generated outputs should be the same as if
     *  write() had been caleld in a loop. */
    virtual auto write_all(array2f const& data) -> void;
    virtual auto write_all(array2<int8_t> const& data) -> void;

    /// Finalize products after a successful model run
    virtual auto finalize() -> void;

    /// Abort any in progress products after a failed model run
    /** It is possible for this function to be called even if there is no product in progress.  For example it may be
     *  called before initialize() or after finalize().  The implementation should simply ignore such invocations. */
    virtual auto abort() -> void;

    /// Get the metadata associated with this product
    auto& output_metadata() const { return metadata_; }

    /// Get type of series dimension if any
    virtual auto series_type() const -> product_series_type;
    /// Get labels for the series dimension (member, percentile, probability etc)
    virtual auto series_labels() const -> array1f const*;
    /// Get times for time dimension (as offsets from reference time)
    virtual auto forecast_times() const -> array1<duration> const*;
    /// Get accumulation lengths for the time dimension
    virtual auto forecast_lengths() const -> array1<duration> const*;
    /// Get the regular time step between forecasts (or 0 if not regular)
    virtual auto time_step() const -> duration;
    /// Get the regular accumulation length (or 0 if not accums or not regular)
    virtual auto accumulation_length() const -> duration;
    /// Get the grid dimensions
    virtual auto grid_shape() const -> vec2i;
    /// Get the grid resolution
    virtual auto grid_resolution() const -> float;
    /// Get the fundamental output units
    virtual auto units() const -> product_units;

  protected:
    auto& output_metadata() { return metadata_; }

  private:
    metadata            metadata_;
    unique_ptr<output>  output_;
    product const*      parent_;
  };
}

namespace steps::products
{
  /// Base class for product types that output an ensemble
  class ensemble : public product
  {
  public:
    ensemble(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;
    auto finalize() -> void override;
    auto abort() -> void override;

  private:
    vector<unique_ptr<product>> children_;
  };

  // this is the top level product produced by the model core
  class rain_rate : public ensemble
  {
  public:
    rain_rate(
          int members
        , int member_offset
        , int forecasts
        , duration time_step
        , vec2i grid_shape
        , float grid_resolution
        , configuration const& config
        , metadata const& user_parameters);

    auto initialize(time_point reference_time) -> void;

    auto series_type() const -> product_series_type override { return product_series_type::member; }
    auto series_labels() const -> array1f const* override { return &members_; }
    auto forecast_times() const -> array1<duration> const* override { return &times_; }
    auto forecast_lengths() const -> array1<duration> const* override { return nullptr; }
    auto time_step() const -> duration override { return time_step_; }
    auto accumulation_length() const -> duration override { return duration::zero(); }
    auto grid_shape() const -> vec2i override { return grid_shape_; }
    auto grid_resolution() const -> float override { return grid_resolution_; }
    auto units() const -> product_units override { return product_units::rate; }

  private:
    array1f           members_;
    array1<duration>  times_;
    duration          time_step_;
    vec2i             grid_shape_;
    float             grid_resolution_;
  };

  class accumulation : public ensemble
  {
  public:
    accumulation(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto accumulation_length() const -> duration override { return accumulation_length_; }
    auto units() const -> product_units override { return product_units::accumulation; }

  protected:
    duration accumulation_length_;
  };

  class accumulation_scaled : public accumulation
  {
  public:
    accumulation_scaled(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;

    auto forecast_lengths() const -> array1<duration> const* override { return &lengths_; }

  private:
    array1<duration>  lengths_;
  };

  class accumulation_sliding : public accumulation
  {
  public:
    accumulation_sliding(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;

    auto forecast_times() const -> array1<duration> const* override { return &times_; }
    auto forecast_lengths() const -> array1<duration> const* override { return &lengths_; }

  private:
    using array_forc = variant<array2f, array1<char>>;
    struct buffer
    {
      shared_ptr<array_forc>  data;     // the actual data array
      bool                    shared;   // whether data is actually shared (unique() & use_count() are not reliable!)

      buffer() : shared{false} { }
    };
    using buffer_store = vector<vector<buffer>>;

  private:
    unique_ptr<input> input_;         // input used to retrieve qpe data for T<=0
    int               compression_;   // compression level for internal buffers

    int               forecasts_in_;
    int               seq_length_;    // number of adjacent time slices to sum
    array1<duration>  times_;
    array1<duration>  lengths_;
    buffer_store      buffers_;       // [member][time][y][x]
  };

  class accumulation_sequential : public accumulation
  {
  public:
    accumulation_sequential(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;

    auto forecast_times() const -> array1<duration> const* override { return &times_; }
    auto forecast_lengths() const -> array1<duration> const* override { return &lengths_; }
    auto time_step() const -> duration override { return accumulation_length(); }

  private:
    using buffer_store = vector<array2f>;

  private:
    bool              clock_sync_;    // whether windows are to be synchronized against the clock
    duration          clock_offset_;  // offset of clock synchronization (e.g. sync 1hr but from xx:30 - xx:30)
    unique_ptr<input> input_;         // input used to retrieve qpe data for T<=0

    array1i           forecast_map_;  // maps input forecast number to output (or -1 if not an accumulation window end)
    array1<duration>  times_;
    array1<duration>  lengths_;
    buffer_store      buffers_;       // [member][time][y][x]
  };

  class filter_members : public ensemble
  {
  public:
    filter_members(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;
    auto finalize() -> void override;
    auto abort() -> void override;

    auto series_labels() const -> array1f const* override { return &members_; }

  private:
    array1i           members_all_;   // list of all mebmers the user said they want to output
    bool              require_all_;   // whether all nominated members must be supplied by parent product

    array1f           members_;       // actual members we will actually output may be subset of members_all_
    array1i           member_map_;    // map input member indexes to output member indexes (-1 for filtered members)
  };

  class filter_time : public ensemble
  {
  public:
    filter_time(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;
    auto finalize() -> void override;
    auto abort() -> void override;

    auto forecast_times() const -> array1<duration> const* override { return &times_; }
    auto forecast_lengths() const -> array1<duration> const* override { return lengths_.size() == 0 ? nullptr : &lengths_; }

  private:
    duration          min_lead_;
    duration          max_lead_;

    int               fc_from_;
    int               fc_till_;
    array1<duration>  times_;
    array1<duration>  lengths_;
  };

  /* Note: For now we don't overwrite output_null on the time lagged ensemble.  This means that if you have a null forecast
   * then you'll get a null output (via write_all) and the output_null will propagate directly to child products.  In
   * theory this isn't perfect because lagged inputs may have been non-zero and we should technically blend the lagged
   * non-zero members with our null members from this cycle.  In practice though this hasn't been necessary yet so we can
   * implement that in the future if desired. */
  class time_lagged : public ensemble
  {
  public:
    time_lagged(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;

    auto series_labels() const -> array1f const* override { return &members_; }
    auto forecast_times() const -> array1<duration> const* override { return &times_; }
    auto forecast_lengths() const -> array1<duration> const* override { return lengths_.size() == 0 ? nullptr : &lengths_; }

  private:
    struct offset_size
    {
      int offset;
      int size;
    };

    struct lag_raw_data
    {
      array1<offset_size> chunks; // cached offsets (x) and size (y) of each compressed grid indexed by member
      array1<char>        data;   // raw compressed data (all members)
    };

    // lagged data representing all members at a single time step
    struct lag_data
    {
      std::mutex                mutex;
      unique_ptr<lag_raw_data>  raw;
      int                       counter = 0;
    };

    struct lag_stream
    {
      std::mutex                mutex;
      unique_ptr<output_stream> stream;
      int                       counter = 0;
    };

  private:
    auto format_lag_path(int forecast, int lag) const -> string;
    auto write_lag(int member, int forecast, array2f const& data) -> void;
    auto read_lag(int member, int forecast, int lag) -> array2f;

  private:
    int                 lags_;
    duration            lag_offset_user_; // user set time between lags (0 == use parent time step)
    duration            lead_time_limit_; // max lead for determining unlaggable steps at end of sequence (0 == use parent)
    string              cache_path_;
    float               resolution_;
    int                 compression_;

    array1<duration> const* parent_times_;
    duration            lag_offset_;      // actual reference time delta between lags (non-zero)
    int                 members_per_lag_;
    array1f             members_;
    array1<duration>    times_;
    array1<duration>    lengths_;

    time_point          reference_time_;  // current reference time
    vector<lag_data>    lag_data_;        // cached input lags, index as flattened array with dimensions [forecast][lag]
    vector<lag_stream>  streams_;         // streams for output of new lag data, indexed by forecast
  };

  class median_filter : public ensemble
  {
  public:
    median_filter(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;

  private:
    int kernel_size_;
  };

  class ensemble_mean : public product
  {
  public:
    ensemble_mean(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;

    auto series_type() const -> product_series_type override { return product_series_type::none; }
    auto series_labels() const -> array1f const* override { return nullptr; }

  private:
    struct time_slot
    {
      std::mutex  mutex;
      int         member_count = 0;
      array2f     sum;
    };

  private:
    int                 member_count_;
    vector<time_slot>   slots_;
  };

  class probabilities : public product
  {
  public:
    probabilities(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;

    auto series_type() const -> product_series_type override { return product_series_type::probability; }
    auto units() const -> product_units override { return product_units::probability; }

  protected:
    struct time_slot
    {
      std::mutex  mutex;
      int         member_count = 0;
      vector<array2<int8_t>> buffers;
    };

  protected:
    int                 member_count_;
    vector<time_slot>   slots_;
  };

  class probabilities_scalar : public probabilities
  {
  public:
    probabilities_scalar(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;

    auto series_labels() const -> array1f const* override { return &thresholds_; }

  protected:
    array1f             thresholds_;
  };

  class probabilities_spatial : public probabilities
  {
  public:
    probabilities_spatial(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;
    auto write_all(array2f const& data) -> void override;

    auto series_labels() const -> array1f const* override { return &thresholds_; }

  protected:
    array1f             thresholds_;
    vector<array2f>     thresholds_spatial_;
  };

  class percentiles : public product
  {
  public:
    percentiles(configuration const& config, metadata const& user_parameters, metadata const* parent_metadata);

    auto initialize(product const& parent, time_point reference_time) -> void override;
    auto write(int member, int forecast, array2f const& data, array2f2 const* flow) -> void override;

    auto series_type() const -> product_series_type override { return product_series_type::percentile; }
    auto series_labels() const -> array1f const* override { return &percentiles_; }

  private:
    struct time_slot
    {
      std::mutex  mutex;
      int         member_count = 0;
      array2f     buffer;           // [i][member] where i is the index (y + x * width) into the grid
    };

  private:
    array1f               percentiles_;

    int                   member_count_;
    vector<time_slot>     slots_;
  };
}
