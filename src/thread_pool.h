/*---------------------------------------------------------------------------------------------------------------------
 * Short Term Ensemble Prediction System (STEPS)
 *
 * Copyright 2022 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "util.h"

#include <condition_variable>
#include <future>
#include <mutex>
#include <thread>

namespace steps
{
  namespace detail
  {
    auto thread_pool_queue_task(std::packaged_task<void()> task) -> uint64_t;
    auto thread_pool_distribute_loop(list<std::packaged_task<void()>> tasks) -> void;
    auto thread_pool_work_until(uint64_t end_count) -> void;
  }

  /// Get the number of worker threads in the pool
  auto thread_pool_size() -> int;

  /// Set the number of worker threads in the pool
  auto thread_pool_set_size(int threads) -> void;

  /// Wrapper for a std::future<T> for use with the thread_pool
  /** This wrapper introduced two key behaviours on top of the standard std::future<T> functionality:
   *  1. The destructor of this class will always block until the wrapped task has been completed.  This facilitates
   *     writing asynchronous tasks while retaining exception safety, since the future will go out of scope before
   *     the resources that were used to create the task (assuming they are created on the stack).
   *  2. All wait operations (including get() and the destructor) will first check if the wrapped task has been
   *     started (but not necessarily finisehd).  If the task has not started, then this thread will lend a hand
   *     processing tasks from the thread pool queue until it has.  This arrangement prevents the system from stalling
   *     since the engine threads come from the main thread pool.  Without this we could have all engine threads
   *     waiting for thread pool tasks to complete, and none of them actually doing anything. */
  template <typename T>
  class thread_pool_future : private std::future<T>
  {
  public:
    thread_pool_future(std::future<T>&& base, uint64_t end_count)
      : std::future<T>{std::move(base)}
      , end_count_{end_count}
    { }

    thread_pool_future(thread_pool_future&& rhs) noexcept = default;
    auto operator=(thread_pool_future&& rhs) noexcept -> thread_pool_future& = default;

    ~thread_pool_future()
    {
      if (std::future<T>::valid())
        wait();
    }

    auto wait() -> void
    {
      detail::thread_pool_work_until(end_count_);
      std::future<T>::wait();
    }

    auto get() -> decltype(std::future<T>::get())
    {
      detail::thread_pool_work_until(end_count_);
      return std::future<T>::get();
    }

  private:
    uint64_t  end_count_;
  };

  /// Queue a single function for asynchronouse execution
  /** This function returns a thread_pool_future, which is a wrapped std::future<T> that blocks during its destructor
   *  to wait for the task to complete if it hasn't already.  This behaviour makes it much much easier to avoid
   *  accidental memory corruption.  For example, if an exception is thrown while a task is running, we will often
   *  destroy objects that the task is relying on as part of stack unwinding.  By forcing the destructor of the
   *  future to block, we can easily ensure that concurrent access to objects used by the task is complete before
   *  we destroy them.  Just make sure that the lifetime of your future is shorter than the lifetime of the objects
   *  being operated on by the task.  This is the natural order of things if your future is stored on the stack
   *  anyway.
   *
   *  It is important to still call get() on the returned future even if your task returns void.  Do not rely
   *  solely on the destructor to wait for your task.  This is because the get() function will propagate any
   *  exception thrown during the task to the caller, while the destructor will simply ignore them.  The blocking
   *  destructor behaviour is really only intended to be used as part of stack unwinding where you already have
   *  an active exception and just need to ensure thread safety.
   *
   *  The thread_pool_future also ensures that the thread calling wait() or get() will start processing queued
   *  tasks until the task being waited on has at least started.  This prevents the thread pool from being
   *  stalled entirely since worker threads are themselves free to wait on other jobs in the queue.  The main
   *  example of this is forecast engine threads which are the main users of the thread pool.  If the number of
   *  engines is equal to the number of the threads in total, then of course we require that the engine threads
   *  themselves be doing the work! */
  template <typename Func>
  auto thread_pool_queue_task(Func&& func) -> thread_pool_future<decltype(func())>
  {
    auto task = std::packaged_task<decltype(func())()>{std::forward<Func>(func)};
    auto fut = task.get_future();
    auto end_count = detail::thread_pool_queue_task(std::packaged_task<void()>(std::move(task)));
    return {std::move(fut), end_count};
  }

  /// Distribute iterations of a loop over multiple worker threads
  /** The calling thread will also participate in performing the loop iterations.  This behaviour addresses a
   *  potential weakness in our thread model due to the interaction between engine and worker threads.  Since
   *  the engine threads also perform a lot of heavy lifting, we would normally want the total number of threads
   *  (engine + worker) to approximate the available processor count.  However if this function simply blocked
   *  while waiting for iterations to complete exclusively on worker threads, then we would be significantly
   *  under-utilizing available compute resources.
   *
   *  Allowing the caller thread to process iterations also allows the system to behave sensibly when the worker
   *  thread count is very low, or even zero.  In the case of no available worker threads the engine threads
   *  will effectively process all worker thread tasks themselves which should have the equivalent performance of
   *  the simple one-thread-per-engine model we previously used.
   *
   *  If an exception is thrown during an iteration it will be caught and rethrown by this function on the calling
   *  thread.  If multiple iterations throw an exception then the exception corresponding to the lowest iteration
   *  is the one to be propagated, while the others will be silently dropped.  It is guaranteed that all iterations
   *  will have completed (either normally or by exception) before this function returns (either normally or by
   *  propagating an exception). */
  template <typename Func>
  auto thread_pool_distribute_loop(int iterations, Func&& func) -> void
  {
    auto tasks = list<std::packaged_task<void()>>();
    for (auto i = 0; i < iterations; ++i)
      tasks.emplace_back([&func, i](){ func(i); });
    detail::thread_pool_distribute_loop(std::move(tasks));
  }

  /// Process one job from the task queue if available
  /** The calling thread will process the first task that is waiting to be processed by the thread pool.  If there
   *  are no tasks waiting then it will return immediately.  The return value is true if a task was processed.
   *
   *  This function is mainly useful when thre is a needsto block and wait for some external condition from within
   *  code that is already running on one of the worker threads.  In this situation if a real blocking mechanism
   *  was used (such as a wait on a condition variable), then the host worker thread is effectively removed from
   *  the pool until the wait ends.  This may lead to under-utilization of our host because the user generally limits
   *  the size of the thread pool based on available processor cores.
   *
   *  Instead, the code can wait in a pseudo busy loop.  While the waited on condition is not met, this function
   *  should be called to allow other background processing using the worker thread.  Then if this function returns
   *  false std::this_thread::yield() should be called to allow other OS threads to takeover.
   *
   *    while (!condition && !thread_pool_process_task())
   *      std::this_thread::yield();
   *
   *  Any exceptions thrown while processing the task will be captured by the relevant future or distribute_loop
   *  call as normal, so there is no need to wrap this function in a try... catch block. */
  auto thread_pool_process_task() -> bool;
}
