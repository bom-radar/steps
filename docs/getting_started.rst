========================================================================================================================
Getting started
========================================================================================================================

.. role:: bash(code)
    :language: bash

Installation
============

Installing a pre-packaged binaries
----------------------------------
STEPS is available as a binary package for common linux distributions.  If you are using one of the supported
distributions simply download the package file to a local directory and install using the command as listed below
substituting the package file name as appropriate:

Fedora / RHEL / CentOS
    .. code-block:: bash

        sudo rpm --install steps.x.y.z-dist.x86_64.rpm

Ubuntu
    .. code-block:: bash

        sudo TODO

Building from source
--------------------
To build STEPS from a source distribution, first ensure that the necessary dependencies are installed.  The following
packages are required to compile and run STEPS:

- FFTW3
- HDF5
- NetCDF
- Udunits2

In addition, the following packages are compile time only dependencies for STEPS.  These packages are only used while
peforming the build and are not required at runtime:

- CMake
- Sphinx (python)

For most Linux distributions the dependency packages can be easily installed using the standard package manager that
is provided by the operating system.  Installation commands for common operating systems are shown below:

Fedora
    .. code-block:: bash

        sudo dnf install cmake gcc-c++
        sudo dnf install fftw3-devel hdf5-devel netcdf-devel udunit2-devel

RHEL / CentOS
    .. code-block:: bash

        sudo yum install epel-release
        sudo yum install cmake3 gcc-c++
        sudo yum install fftw3-devel hdf5-devel netcdf-devel udunit2-devel

Ubuntu
    .. code-block:: bash

        sudo TODO

The STEPS build is configured using CMake, which can be used to change compile time conifugraiont options such
as the location of dependencies and the final install location.  CMake will generate standard Makefiles which are
then used to compile and install.  As is standard practice for CMake based projects, the compilation should be
performed in a dedicated build directory rather than in the main source repository.

Running the following commands from the root of the source repository will build and install STEPS to the system
default location, typically ``/usr/local``.

.. code-block:: bash

    # current directory is root of steps repository
    mkdir build
    cd build
    cmake ..
    make
    sudo make install

Options can be supplied to CMake using the ``-D`` directive.  The most common option is to change the installation
location which is achieved by setting the ``CMAKE_INSTALL_PREFIX`` option as shown below.  This configuration would
install STEPS under the ``local`` sub-directory of the user's home directory.

.. code-block:: bash

    # current directory is root of steps repository
    mkdir build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=${HOME}/local
    make
    sudo make install

After running CMake once, the ``CMakeCache.txt`` file inside the ``build`` directory may be inspected and edited to
set other compilation options.

Configuring the model
=====================
Before running forecast, STEPS must be configured as appropirate for the intended application.  This is done by
creating a configuration file which is read by STEPS at startup.  Settings specified by the file include the
basic model parameters (domain size, resolution, time step, etc.), tuning parameters (decomposition filters,
optical flow, distribution correction, etc.), and runtime settings (file paths, number of threads, random seeed,
etc.).

A demo configuration file for a basic advection only nowcast can be found in the STEPS ``demo`` directory within
the source distribution.  This demo configuration assumes input is available as a series of NetCDF files each
containing a 2D variable of the rain rate at a given time.  A good way to get started is to make a copy of this file,
and then modify it as needed to match your input data.

To use the demo configuration with your own data, modify the following configuration items as appropriate:

grid_rows
    Height of your input domain in grid cells.
grid_cols
    Width of your input domain in grid cells.
grid_resolution
    Width and height of a grid cell (cells must be square).
members
    Number of members to generate in the output ensemble.
forecasts
    Number of forecasts (time steps) per member in the output ensemble.
time_step
    Time between observation inputs in seconds.
reference_time
    UTC time of the current observation to use as the basis for the forecast.
observation.input.path
    Path of input data.  This is a format string which should include reference to a ``time`` parameter that will be
    substituted with the UTC time of observation which STEPS wishes to read.
observation.input.variable
    Name of the 2D variable containing rain rates.  If the variable lacks CF compliant metadata it may also be
    necessary to supply additional settings such as ``units_override``, ``duration_variable``, or
    ``accumulation_start_variable`` and ``accumulation_end_variable``.
engines
    Maximum number of CPU cores you wish to utilize for forecasting.  For optimal performance this value should be set
    to the number of CPU cores available on the host machine, but not higher.

A complete reference to the parameters of the configuration file may be found in the :ref:`Configuration file reference`
chapter.

Running a forecast
==================
Nowcasts are produced by running STEPS from the command line while providing the path to the configuration file as
a command line argument.  For example:

.. code-block:: bash

    steps /path/to/my/configuration.conf

Executing STEPS in this manner will use exactly the settings as specified by the configuration file.  Typically a
nowcast ensemble should be produced every time a new observation input is available.  Using the above command line
this would require editing the ``reference_time`` parameter of the configuration file before each run.

To avoid the need for frequently editing the model configuration file it is possible to overwrite any configuration
setting directly from the command line.  This is achieved by providing additional command line arguments of the
form ``key=value``, where ``key`` is the name of the setting in the configuration file to overwrite.  For nested
configuration items separate levels in the key using a period.  For example:

.. code-block:: bash

    # overwrite the forecast reference time
    steps configuration.conf reference_time=2020-04-09T10:53:00Z

    # overwrite the forecast reference time, and set the number of compute engines based on CPU count
    steps configuration.conf reference_time=2020-04-09T10:53:00Z engines=$(nproc)

    # overwrite the forecast reference time, and some custom user metadata specifying the radar id
    steps configuration.conf reference_time=2020-04-09T10:53:00Z metadata.radar.value=71

If a setting is always intended to be supplied via the command line (such as ``reference_time``), then it may be
omitted from the configuration file entirely.  This is recommended, as it ensure that STEPS will produce an error
if the user forgets to supply the required setting on the command line.
