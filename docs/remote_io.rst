.. _Remote Input and Output:

========================================================================================================================
Remote Input and Output
========================================================================================================================
In addition to reading and writing files on the local disk, STEPS is desigend to support directly accessing remote files
stores such as cloud based storage solutions.  This allows STEPS to be deployed natively in a cloud environment without
needing additional external scripts to download inputs, run the model, then upload outputs.

Use of remote file systems is acheived by supplying a URI instead of a local path when specifying paths in the model
configuration file.  All configuration items that specify a path support use of remote I/O unless otherwise noted.

Currently the folowing remote file URI schemas are supported:

============= =============================================================================================
Schema        Description
============= =============================================================================================
``file://``   Local file.
``s3://``     AWS S3 object.  Default implementation optimized for small files.
``s3crt://``  AWS S3 object.  Alternative implementation optimized for large files.
============= =============================================================================================

Local File Schema ``file://``
=============================
Paths in this schema are specified as ``file://<path to local file>``.  It is recommended to avoid using this schema
explicitly and intead just supply the path directly.  With the exception of more complex parsing, this schema is
exactly equivalent to supplying a local path.

AWS S3 Schemas ``s3://`` and ``s3crt://``
=========================================
This schema allows STEPS to read/write files as objects in an AWS S3 bucket.  The format of the URI is
``s3://<bucket_name>/<object_key>`` or ``s3crt://<bucket name>/<object key>`` where ``object_key`` may itself contain
directory separators.  For example: ``s3://my-bucket/path/to/my/file``.

To enable these URIs you must compile STEPS with AWS support enabled (set ``WITH_AWS`` to ``ON`` in ``CMake`` build).

The ``s3://...`` version of this schema is the official one as used by AWS.  When STEPS is passed a URI of this form
it uses the default S3 client from the AWS SDK (``S3Client``).  The alternative ``s3crt://...`` form is a non-standard
form that is STEPS specific.  This form causes STEPS to use an alternative S3 client provided by the AWS SDK which is
optimized for the transfer of large files (``S3CrtClient``).

Generally, the ``s3://`` form should be preferred by default, however for large files the ``s3crt://`` form should be
tested as it can yield very significant performance improvements.  Large in this context is context dependent, but as
a rule of thumb files that are larger than 10MB may benefit from use of the ``S3CrtClient``.

The ``s3crt://`` form should only be used when a performance benefit is clearly demonstrated.  The underlying
``S3CrtClient`` works by splitting uploads and downloads into multiple parallel API requests to S3.  Since S3 charges
are incurred per request, using this client may be associated with higher costs.  For small files the ``S3CrtClient``
client is also slower than the default ``S3Client`` and may in fact harm performance.

A final consideration when using the ``s3crt://...`` form for outputs is that S3 uploads are transformed into
multi-part uploads.  This means that it is possible for incomplete uploads to linger on the target S3 bucket if a
serious failure (such as a crash or power outage) occurs path way through the forecast.  It is recommended that the
target S3 bucket includes a lifecycle policy to remove orphaned partial uploads if using ``s3crt://...``.  See also
https://aws.amazon.com/blogs/aws-cloud-financial-management/discovering-and-deleting-incomplete-multipart-uploads-to-lower-amazon-s3-costs/.

The user must ensure that appropriate credentials have been exposed to the AWS SDK.  See
https://docs.aws.amazon.com/sdk-for-cpp/v1/developer-guide/credentials.html for details.
