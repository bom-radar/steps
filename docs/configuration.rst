.. _Configuration file reference:

========================================================================================================================
Configuration file reference
========================================================================================================================
The STEPS model is configured by means of a human readable text file which uses a heirarchically structured syntax
similar to JSON.  At the top level, the file may be considered as a list of name value pairs, where the value may take
the form of a scalar, an array, or an object.

- Scalar values are a single string.  The string must be contained in double quotes if it contains any whitespace
  characters.
- Array values are a list of further values enclosed by square brackets (``[ value1 value2 ... ]``).  The values of the
  list may themselves be scalars, arrays or objects.
- Objects are a list of name/value pairs enclosed by braces (``{ name1 value1 name2 value2 ... }``).  The values
  associated with each pair can be a scalar, array or object.

Comments may be added to the file using a hash (``#``) character which causes the remainder of the line to be ignored.

The below example shows a configuration file containing two scalars, an array and an object (which itself contains a
scalar and an array):

.. code-block:: none

   # a comment
   my_scalar      foo
   my_quoted      "A long quoted string"
   my_array       [ a b c d ]
   my_object
   {
     sub_scalar   bar         # another comment
     sub_array    [ 1 2 3 4 ]
     sub_object
     {
       name       value
     }
   }

Within this document the following basic types are used for scalar values:

- **Boolean** - Must be ``true`` or ``false``.
- **Integer** - An integer value in base 10, for example ``8374``.
- **Real** - A floating point value in base 10, for example ``38.44``.
- **String** - A character string in UTF-8 encoding.  Must be quoted if it contains whitespace.

Configuration items noted as **optional** may simply be ommitted from the configuration file if desired.

This chapter describes configuration items that are used when STEPS is running in forecast mode.  The configuration
file required when running in verification mode is described in the chapter on :ref:`Verification`.

Top level settings
============================================================
This section describes configuration settings that appear at the top level of the model configuration file.

advect_flow
    **Boolean** -
    Whether to advect the nowcast flow vectors at each time step or not.  Enabling this option will cause the flow field
    to move along with the rain field.  This causes raining areas to preserve their original direction and speed, rather
    than being 'pushed' through a static field.  This is an effect similar to using a forward advection scheme, except
    that we still use backward advection for each individual time step.  The use of the backward scheme at the individual
    times means that this is only an approximation of a forward scheme.

anti_ring_border_width
    **Integer** -
    Width of border region used to reduce ringing during cascade decomposition in grid cells.  The field to be decomposed
    is reflected into the zero padding area and faded out over this distance using a tapered cosine (Tukey) window.  When
    there is precipitation spanning the domain boundary this reduces the ringing caused by a sharp transition from a positive
    rain value to the hard zero used for FFT padding.

autocorrelations_default
    **Array of reals or Object** -
    Defines the default relationship between spatial scale (wavelength) and autocorrelation.  This relationship will be
    used whenever autocorrelations cannot be diagnosed from inputs.  The setting may be provided as either an array of
    reals, which explicitly sets the autocorrelation of each cascade level, or as an object which configures a parametric
    relationship.  If using a parametric relationship the configuration item must contain settings for ``scale_1``,
    ``value_1``, ``scale_2``, and ``value_2``, which specify the wavelength and corresponding autocorrelations for two
    different scales respectively.  An example of the parametric configuration is shown below.

    .. code-block:: none

       autocorrelations_default
       {
          scale_1 180.0
          value_1 0.994

          scale_2 18.0
          value_2 0.893
       }

    The function used to implement the parametric autocorrelation is:

    .. math::

       c(s) = 1 - \frac{1}{as^b + 1}

    Where :math:`a` and :math:`b` are selected such that the function passes through the two scale/value pairs defined
    in the configuration.

.. _blend_halflife:

blend_halflife
    **Real, seconds, optional** -
    Direct blending of model values into the cascade is limited to (areas within) cascade levels that have a very high
    autocorrelation.  This parameter limits the amount of model input that can be directly blended into the nowcast each
    time step.  The value is specified as the length of time it would take for a region with an autocorrelation of 1.0 (i.e.
    not evolving at all) to be made up of 50% direct model contributions and 50% nowcast (since the start of the series).
    Lower values will shorten the transition / fade time between nowcast and model, but may also cause the nowcast to be
    more strongly constrained to the model inputs (since a shorter blend half life results in more blend per time step).

blend_ac_halfrate
    **Real, optional** -
    This parameter works in concert with :ref:`blend_halflife<blend_halflife>`.  While ``blend_halflife`` limits the maximum
    total blending that can occur in any one time step, this paramter scales that blend amount according to the current
    autocorreation.  A local autocorrelation value of 1.0 will always result in the maximum direct blend (according to
    ``blend_halflife``) being employed.  The amount of blending then falls off exponentially as the autocorrelation reduces
    to 0.  The value here is used to calculate the exponent for the falloff, such that locations with this autocorrelation
    will experience exactly half of the maximum direct blending.  It is likely this value should be very close to, but not
    quite 1.  A good place to start is using the field mean autocorrelation of the top level of your cascade (typically
    around 0.995)

control_member
    **Integer, optional** -
    Index of the member to treat as a control member.  All perturbations of the initial model state, such as the flow
    perturbations introduced by :ref:`perturb_flow<perturb_flow>`, will be disabled for this member.  This is useful
    when a single member will also be used as a deterministic forecast.  If omitted or set to a member index that is
    not generated, all members will have initial state perturbations applied.

crossovers
    **Array of reals, km** -
    Decomposition bandpass filter bank crossover points.  These points the spatial scales at the boundaries between
    cascade levels (not the middle frequency) so there must be one less than the total number of levels.  The values
    correspond to wavelengths in km.  This setting may be left as empty square brackets (i.e. an array of size 0), in
    which case the crossovers will be spread evenly (in log space) throughout the spectrum.

diagnostics
    **String, file path, optional** -
    If provided, intermediate grids and other internal diagnostic values will be output to a netcdf file at this path.
    Use of this will **severely** reduce the performance of the model, and the output diagnostic file can be **huge**.
    The number of forecast engines (see :ref:`engines<engines>`) must be set to 1 when diagnostic output is
    enabled.

disable_advection
    **Boolean, optional** -
    Set this value to ``true`` to disable advection of the nowcast between time steps.  This setting is intended to be used
    for diagnostic purposes only.

disable_evolution
    **Boolean, optional** -
    Set this value to ``true`` to disable stochastic evoluation of the nowcast.  This setting is intended to be used for
    diagnostic purposes only.

distribution_correction
    **Object** -
    Configures the distribution correction method to be used at the end of each time step in the main forecast loop.
    The available methods and their parameters are described by the sub-sections below.  The scheme to be used is selected
    by setting the ``type`` attribute of this object.  Additional scheme dependent paramters may also be required.

.. _engines:

engines
    **Integer, optional** -
    Sets the maximum number of forecast engine threads to run in parallel.  This value limits the number of members
    which are genertated simultaneously.  It is suggested to set this value equal to the number of CPU cores which are
    available for use by STEPS.  Some consideration should also be given to memory usage since doubling the number of
    engines will approximately double the amount of memory used by STEPS.  If this option is not provided or set to 0,
    then STEPS will default the number of forecast engine threads to the total number of threads being used.

    The number of engine threads actually used will be limited by the number of members.  It is therefore safe to set
    this value higher than the member count, and excess engine capacity will be ignored.  The threads that would have
    been used for the extra engines will remain free for use in the worker thread pool.

fft_cols
    **Integer, optional** -
    Sets the width of the domain in grid cells that is used to zero-pad the data before a Fourier transform.  This value
    should be set to approximately double the :ref:`grid_cols<grid_cols>` value to minimize ringing artifacts in the
    spectral decomposition.  If omitted will default to exactly ``2 * grid_cols``.

    The FFT algorithm is significantly faster when operating on data with exact power of 2 sized dimensions.  For large
    domains a significant performance increase may be achieved by manually setting this value to an exact power of 2
    that approximates double the :ref:`grid_cols<grid_cols>` default.

fft_rows
    **Integer, optional** -
    Sets the height of the domain in grid cells that is used to zero-pad the data before a Fourier transform.  This value
    should be set to approximately double the :ref:`grid_rows<grid_rows>` value to minimize ringing artifacts in the
    spectral decomposition.  If omitted it will default to exactly ``2 * grid_rows``.

    The FFT algorithm is significantly faster when operating on data with exact power of 2 sized dimensions.  For large
    domains a significant performance increase may be achieved by manually setting this value to an exact power of 2
    that approximates double the :ref:`grid_rows<grid_rows>` default.

fft_threads
    **Integer, optional** -
    Number of threads to use for Fourier transform calculations.  Fourier transform threads (beyond the first) are
    currently launched in addition to the main thread pool, so care must be taken not to generate too many threads.
    It is strongly recommended to experiment with this setting to find the setting that yields the best performance
    for a given forecast configuration and hardware combination.

forecasts
    **Integer** -
    Number of forecasts (time steps) to generate.

.. _grid_cols:

grid_cols
    **Integer** -
    Width of the domain in grid cells.  Geographical width of domain is calculated by multiplying this value by
    :ref:`grid_resolution<grid_resolution>`.

.. _grid_resolution:

grid_resolution
    **Real, kilometers** -
    Edge length (height and width) of a single grid cell in kilometers.

.. _grid_rows:

grid_rows
    **Integer** -
    Height of the domain in grid cells.  Geographical height of domain is calculated by multiplying this value by
    :ref:`grid_resolution<grid_resolution>`.

levels
    **Integer** -
    Number of levels in the cascade decomposition.

mask_advected_in
    **Boolean** -
    Whether to mask areas of the forecast that have been advected from outside the domain.  These grid cells will be
    flagged as missing data in output file formats such as NetCDF.

mask_static
    **Object, optional** -
    Configures a static mask which is applied to output products.  If provided, this configuration item should contain
    settings for ``path``, ``variable`` and ``advect``, which specify the path to a NetCDF file containing the mask, the
    variable within the file containing the mask, and a Boolean flag respectively.  The Boolean ``advect`` flag indicates
    whether the mask itself should be advected along with the forecast (``true``) or entirely static and frozen in place
    (``false``).

    The ``path`` setting supports the use of user defined format parameters, see :ref:`user_parameters<user_parameters>`.

    An example of this configuration item is shown below.

    .. code-block:: none

       mask_static
       {
          path      "{param:data_path}/masks/steps_mask.nc"
          variable  domain_mask
          advect    false
       }

max_engine_spread
    **Integer, optional** -
    To minimize reading and preprocessing (decomposition, tracking) of input fields from models and observations, the
    processed input data is shared between engines and reused for each member.  Since the forecast engines operate
    essentially independently it is possible, although unlikely, for a single engine to surge ahead or lag behind.
    This increases the 'spread' of time steps which are being worked on at once, which in turn increases the amount of
    memory required to keep the preprocessed input data cached.

    This option can be used to limit the total spread of time steps that are cached in memory by preventing any one
    engine from getting too far ahead of the slowest engine.  If set to 0 then all engines will finish processing time
    T before commencing time T+1.  Setting this to a small value such as 1 or 2 helps to limit the amount of cached
    data in memory at any one time.  Any engines that get too far ahead will pause and wait for the others to catch up.

    Some product generators need to cache large amounts of memory for each forecast time step that is "in progress".
    The spread limit is therefore also relevant when running in the ``postprocess`` mode.

    If this options is not provided or set to -1 the spread limit is disabled allowing engines to run at full speed.

member_offset
    **Integer** -
    Index of the first member to generate.  Ensemble members numbered `member_offset` to `member_offset + members`
    (inclusive) will be generated.  This is useful when distributing the generation of members for a single ensemble over
    multiple hosts.

members
    **Integer** -
    Total number of ensemble members to generate.

minimum_rain_rate
    **Real, mm/hr** -
    Minimum detectable rain rate in mm/hr.  This is the minimum rain rate used to indicate a 'raining' grid cell.

.. _minimum_rain_area:

minimum_rain_area
    **Real, square kilometers, optional** -
    Minimum area in km\ :sup:`2` that must be 'raining' (above the minimum rain rate) in the observation stream when
    performing a conditional simulation.  If the observed raining area at the reference time is below this value then
    output products will be either suppressed or filled with a default value.  See :ref:`null_forecast_mode<null_forecast_mode>` for
    details.  Defaults to 0.0, which will cause forecasts to be generated regardless of raining area.  Ignored
    during unconditional simulations (i.e. when no observation stream is configured).

models
    **Object, optional** -
    If supplied, contains a list of NWP model inputs.  Each entry in this object is a name value pair where the name
    provides a user friendly name for the model input, and the value is a sub-object containing the configuration items
    for the model which are described below (**in a future release**).

.. _null_forecast_mode:

null_forecast_mode
    **Enumerate, "none" | "missing" | "zero", optional** -
    Determines how product outputs should be handled if forecasting is suppressed due to a failed raining area check
    (see :ref:`minimum_rain_area<minimum_rain_area>`).  If set to ``none``, then no products will be output.  If set to
    ``missing`` then the products will be output with every grid cell set to the missing data flag.  If set to ``zero``
    then the products will be output as if a forecast had been run and generated zero rainfall at every grid cell for
    every member and time step.  Defaults to ``none``.

observation
    **Object, optional** -
    If supplied, configures STEPS as a *conditional simulation*, conditioned on the observation at (and before) the
    forecast reference time.  See :ref:`Observation stream` for a description of this configuration item.

obs_skill_scale
    **Real, optional** -
    If supplied, causes all diagnosed skills for the observation stream to be multiplied by this value.  This parameter
    is a 'fudge factor' that can be used to manually adjust the point at which the observation and model streams
    crossover in terms of blending priority.  A value greater than 1 will cause the observation skill to be increased
    and therefore the impact of the observation on the nowcast to be lengthened.

optical_flow
    **Object** -
    Configures the optical flow method to be used when diagnosing advection vectors from input rain rate fields.  The
    available methods and their parameters are described by the sub-sections below.  The scheme to be used is selected by
    setting the ``type`` attribute of this object.  Additional scheme dependent paramters may also be required.

.. _parameter_radius:

parameter_radius
    **Integer** -
    Radius of the region used to calculate spatially varying parameter values in grid cells.  For a radius of R a 2*R+1
    by 2*R+1 window centered on each cell will be used to calculate the local value.

.. _perturb_flow:

perturb_flow
    **Real** -
    Standard devation of log normal distribution used to diagnosed flow vector speeds per member.  A value of 0.0 will
    disable perturbation of flow field speeds.

product
    **Object** -
    This object specifies configuration for the top level rain rate ensemble which is the primary product output by the
    model core.  See the :ref:`Output products` section for a description of this configuration item.

psd_scale_break
    **Real, optional** -
    Sets the wavelength in kilometers of the scale break point in the parametric default PSD.  Defaults to 32.0.

psd_beta_1
    **Real, optional** -
    Sets the exponent that determines the slope of the default parametric PSD for wavelengths longer than the scale break.

psd_beta_2
    **Real, optional** -
    Sets the exponent that determines the slope of the default parametric PSD for wavelengths shorter than the scale break.

psd_diagnosis_limit
    **Real, optional** -
    Sets the wavelength limit below which the default PSD will be used instead of any diagnosed PSD based on observations
    or model inputs.  This value applies discretely to whole cascade levels based on the central wavelength of the level.
    This means that the noise generated for a cascade level either will or will not be based on the diagnosed PSD.  There
    is no partial use of diagnosed and default PSDs within a single level.

random_seed
    **Integer, optional** -
    Sets the random seed used to initialize the random number generator.  Setting this value to 0 will set the seed to a
    random value which is obtained from a high quality source of randomness provided by the operating system.  This should
    be the default in an operational context.  If set to a non-zero integer, the specified seed will be used which allows
    results to be exactly reproduced.  In this case the seed is offset by one for each member to prevent them from
    duplicating each other.

.. _reference_time:

reference_time
    **Timestamp, ``yyyy-mm-ddThh:mm:ssZ``** -
    The base / last observation / analysis time for the model run.  The first output forecast will be exactly one time
    step after this time.  In an operational context this value is typically overwritten via a command line argument.

rolloff
    **Real** -
    Width of bandpass filter crossover used during cascade decomposition, in octaves.  This value defines the number of
    octaves (powers of 2) over which bandpass crossover between cascade levels occurs.  A value of 1 indicates that the
    wavelength at the start of the crossover will be double the wavelength at the end.  Similarly, a value of 2 indicates
    that the wavelength where the crossover starts is 4 times the crossover end.

    For example, for a crossover at 100km:

    - Rolloff of 0.5 will transition between ~119km and ~84km (:math:`119/84 \approx 2^{0.5}`)
    - Rolloff of 1.0 will transition between ~140km and ~70km (:math:`140/70 \approx 2^{1.0}`)
    - Rolloff of 2.0 will transition between ~200km and ~50km (:math:`200/50 \approx 2^{2.0}`)

spatial_skill_levels
    **Integer, optional** -
    The number of cascade levels to use when evaluating the spatial skill of a model.  Skill is determined by the
    correlation between the partially recomposed model and observation cascades at the forecast reference time.  This
    setting determines the number of cascade levels which are included in the partial recomposition.  For example,
    a value of 3 will cause skills to be extimated based on a comparison between partial recompositions of cascade
    levels 0, 1 and 2.  Defaults to 1.

test_pattern
    **Object, optional** -
    If provided, configures test patterns that can be used to overwrite the initial nowcast state.  Test patterns may be
    set for the lag-0 and lag-1 rain fields, as well as the flow field.  If a test pattern for the flow field is not
    defined then the lag-0 and lag-1 test patterns will be tracked using optical flow to generate the flow field as
    normal.

time_step
    **Integer, seconds** -
    The time step between consecutive forecasts.

threads
    **Integer, optional** -
    Set the total number of threads to be used.  One thread will be allocated to each configured forecast engine (see
    :ref:`engines<engines>`), and any remaining threads will form a general thread pool used to accelerate expensive
    operations.  Defaults to the number of processor cores exposed by the host system.

    This setting applies to the STEPS application rather than the model configuration.  This distinction is only
    important in contexts where multiple top level actions will be performed by a single instance of the executable.
    For example, within the STEPS on AWS application the ``threads`` is set once for the entire system, and
    cannot be modified for individual requests.  Any ``threads`` setting in the per-domain configuration files
    used by STEPS on AWS will be ignored.

    Note that the :ref:`engines<engines>` setting **does** apply on a per-request basis.  In the context of STEPS on
    AWS this allows users to tune memory utilisation and performance on a per-domain basis by changing the ratio of
    engine to general worker threads.

threshold_output
    **Boolean** -
    Whether to force values of the forecast which are below the minimum rain rate to exactly 0.  This option can
    significantly improve performance and files sizes when using output types which support internal compression such
    as NetCDF.

trace_level
    **Enumerate, "none|error|warning|notice|log|verbose", optional** -
    Set the logging level.  Defaults to ``log``.

    This setting applies to the STEPS application rather than the model configuration.  This distinction is only
    important in contexts where multiple top level actions will be performed by a single instance of the executable.
    For example, within the STEPS on AWS application the ``trace_level`` is set once for the entire system, and
    cannot be modified for individual requests.  Any ``trace_level`` setting in the per-domain configuration files
    used by STEPS on AWS will be ignored.

transform
    **Object** -
    Configures the method to be used when converting between linear (mm/hr) and log units.  The available methods and
    their parameters are described by the sub-sections below.  The scheme to be used is selected by setting the ``type``
    attribute of this object.  Additional scheme dependent paramters may also be required.

.. _user_parameters:

user_parameters
    **Object, optional** -
    This optional configuration item establishes a list of parameters which will be made available for use in various
    format strings throughout the configuration.  Typically this includes settings relating to file paths, and custom
    metadata for use in product output files.

    The object consists of a set of name-value pairs where the value can be either a simple string or a sub-object
    specifying an explicit ``type`` and ``value``.  In the latter case, the ``type`` must be set to one of ``i8``,
    ``u8``, ``i16``, ``u16``, ``i32``, ``u32``, ``i64``, ``u64``, ``f32``, ``f64``, ``time``, or ``string``.

    The example configuration below establishes 3 parameters; one will be a string, one a 32-bit
    integer (with the value 2), and the other is a timestamp.

    .. code-block:: none

       user_parameters
       {
          data_path    "/srv/data/steps"
          station_id   { type i32 value 2 }
          clim_date    { type time value 1983-03-25T00:10:00Z }
       }

    In subsequent format strings these parameters may be referenced as named parameters with the prefix ``param:``.
    Parameters respect the normal format string syntax relevant to the type of the paramer.  For example, a format
    string combining the above parameters might look like:

    ``{param:data_path}/climatology/{param:station_id:03d}/clim_{param:clim_date:%Y%m%d}.nc``

    Would result in the following value being used by the model:

    ``/srv/data/steps/climatology/002/clim_19830325.nc``

    The syntax for referencing a variable is described by the `Format String Syntax`_ documentation of the
    `{fmt} library`_ which is used for string formatting.

    .. _Format String Syntax: https://fmt.dev/latest/syntax.html
    .. _{fmt} library: https://fmt.dev

Log transformations
============================================================
The core STEPS algorithm relies on the representation of rain rates in log units.  The conversion from linear rain
rates in mm/hr to a log scale presents a significant challenge due to the fact that a rain rate of 0 is undefined
in log units when using a simple conversion of the form :math:`R = \log_10{r}`.  Because of this STEPS provides several
more complex schemes for implementing linear/log conversions.

All of the parameters described in this section should be nested within the top level ``transform`` object.

Inverse Hyperbolic Sine
------------------------------------------------------------
Transforms from linear (mm/hr) to log units using the the inverse hyperbolic sine function.  The forward transform
is defined as:

.. math::

   R = arcsinh(sr)

The inverse transform is defined as:

.. math::

   r =
   \begin{cases}
     \frac{sinh(R)}{s}, & \text{if } R > 0\\
     0,                 & \text{otherwise}
   \end{cases}

type
    **Enumerate, "inverse-hyperbolic-sine"** -
    Constant value which selects the Inverse Hyperbolic Sine transform for use.

prescale
    **Real,** :math:`s` -
    The rain rate values are multiplied by this factor before being converted to log space using the :math:`arcsinh`
    function.

Truncated Log
------------------------------------------------------------
Transforms from linear to log units by truncating the rain rate at a low rain rate.  The forware transform is defined
as:

.. math::

   R =
   \begin{cases}
     \log_e{r} - \log_e{t}, & \text{if } R > t\\
     0,                     & \text{otherwise}
   \end{cases}

The inverse transform is defined as:

.. math::

   r =
   \begin{cases}
     e^{R + \log_e{t}}, & \text{if } R > 0\\
     0,                 & \text{otherwise}
   \end{cases}

type
    **Enumerate, "log-truncate"** -
    Constant value which selects the Truncated Log transform for use.

threshold
    **Real,** :math:`t` -
    Rain rate in mm/hr where the truncation will occur.

Biased Log
------------------------------------------------------------
Transforms from linear to log units by adding a small constant factor to the rain rate before it is log transformed.
The forware transform is defined as:

.. math::

   R = s\log_b{r + t}

The inverse transform is defined as:

.. math::

   r = b^{\frac{R}{s}} - t

type
    **Enumerate, "log-bias"** -
    Constant value which selects the Biased Log transform for use.

scale
    **Real,** :math:`s` -
    Scale factor applied to values in log space.

base
    **Real,** :math:`b` -
    Base of logarithm used in transform.

bias
    **Real,** :math:`t` -
    Bias value applied to rain rates in linear space before conversion to log space.

Optical flow methods
============================================================
STEPS provides the user with a choice of several optical flow methods to use when diagnosing the initial flow fields
for the nowcast.  This section describes each of the available optical flow algorithms.

It is strongly recommended to use the ``brox``/``brox_mt`` method as this has been shown experimentally to produce the
best flow vectors under a variety of conditions.

All of the parameters described in this section should be nested within the top level ``optical_flow`` object.

Robust Optical Flow
------------------------------------------------------------
The optical flow algorithm described by [Brox04]_.

type
    **Enumerate, "brox" or "brox_mt"** -
    Constant value which selects the Robust Optical Flow algorithm for use.  The ``brox_mt`` variant of this algorithm
    utilizes multi-threading to perform the same operation.  It is highly recommended to test forecast configurations
    against both the single threaded (``brox``) and multi-threaded (``brox_mt``) to assess which one exibits better
    performance within the final execution context.

alpha
    **Real, optional** -
    Weight of the smoothing term.

gamma
    **Real, optional** -
    Weight of the gradient constancy term.

scales
    **Integer, optional** -
    Desired number of scales.

zfactor
    **Real, optional** -
    Downsampling factor.

tol
    **Real, optional** -
    Stopping criterion threshold for the numerical scheme.

initer
    **Integer, optional** -
    Number of inner iterations in the numerical scheme.

outiter
    **Integer, optional** -
    Number of outer iterations in the numerical scheme.

low_pass
    **Real, optional** -
    If set, causes a low pass filter with cuttoff at this wavelength to be applied to the images before tracking.

low_pass_rolloff
    **Real, optional** -
    Sets the rolloff in octaves of the pre-tracking low pass filter (if enabled).

fft_cols
    **Integer, optional** -
    Sets the width in grid cells of the zero-padding domain used for the Fourier transform operation of the pre-tracking
    low pass filter (if enabled).  Defaults to ``2 * grid_cols``.

fft_rows
    **Integer, optional** -
    Sets the height in grid cells of the zero-padding domain used for the Fourier transform operation of the pre-tracking
    low pass filter (if enabled).  Defaults to ``2 * grid_rows``.

Combined Local-Global Optical Flow
------------------------------------------------------------
The optical flow algorithm described by [Jara-Wilde15]_.

type
    **Enumerate, "clg"** -
    Constant value which selects the Combined Local-Global Optical Flow algorithm for use.

alpha
    **Real, optional** -
    Value of the global regularization coefficient.

rho
    **Real, optional** -
    Value of the local spatio-temporal derivatives regularization coefficient in the form of a gaussian smoothing with
    standard devationi rho.  Use 0 to avoid this smoothing.

sigma
    **Real, optional** -
    Standard deviation value for a gaussian smoothing applied to the input grids.  Use 0 to avoid smoothing.

iterations
    **Integer, optional** -
    Maximum number of terations for the optical flow computation.

w_factor
    **Real, optional** -
    SOR relaxation factor, between 0 and 2.

scales
    **Integer, optional** -
    Number of scales used for the pyramidal approach.

scale_factor
    **Real, optional** -
    Scale factor, between 0 and 1.

coupled_mode
    **Boolean, optional** -
    Iteratoin type.  PCGS f true, SOR if false.

Rainfields 3 Optical Flow
------------------------------------------------------------
The optical flow algorithm implemented as part of Rainfields 3.  This algorithm is a variation of [Farneback03]_ which
is used to create initial flow vectors.  This algorithm is a local method which means that vectors in non-raining
areas have a length of zero.  The vector field is post processed to interpolate vectors diagnosed from raining grid
cells into the non-raining areas.

type
    **Enumerate, "rf3"** -
    Constant value which selects the Rainfields 3 Optical Flow algorithm for use.

scale
    **Real, optional** -
    Scale factor for tracking resolution levels.

window_size
    **Integer, optional** -
    Blur operation kernel size.

iterations
    **Integer, optional** -
    Number of blur operations at each resolution.

polygon_neighbourhood
    **Integer, optional** -

polygon_sigma
    **Real, optional** -

background
    **Real, optional** -
    Value used to fill in areas of invalid data.

threshold
    **Real, optional** -

gain
    **Real, optional** -
    Scale applied to input data before tracking.

fill_gaps
    **Boolean, optional** -
    Whether to interpolate to replace diagnosed vectors between non-rainng areas (which normally tend towards zero).

spacing
    **Integer, optional** -
    Number of pixels between interpolation points in the initial low resolution interpolation pass used for gap filling.

min_frac_bins_for_avg
    **Real, optional** -
    Fraction of bins in neighbourhood of low resolution interpolation points that must be raining to consider the location
    a valid point for use in interpolation scheme.

idw_low_res_pwr
    **Real, optional** -
    Power term used in inverse distance weighting for low resolution interpolation pass during gap filling.

idw_high_res_pwr
    **Real, optional** -
    Power term used in inverse distance weighting for high resolution interpolation pass during gap filling.

Distribution correction
============================================================
After the nowcast cascade is recomposed into a single rain rate field, the final step before the generation of
output products is to perform an optional distribution correction.  This distribution correction algorithm attempts
to account for statistical biases in the output forecasts.  The resulting corrected field is used as the basis for
output products, and is also fed back into the forecast loop to initialize the next time step.

There are several different distribution correction schemes to choose from which are detailed in this section.  The
choice of scheme should be considered carefully within the context of the eventual application.  A scheme which
imposes statistical characteristics strongly (such as probability matching) may appear to have superiror
performance in bulk verification statistics, but this is likely due to the fact that most cases align well with
climatology.  The same correction might also harm forecast performance for unusual events which are likely to be
of greatest interest / importance.

All of the parameters described in this section should be nested within the top level ``distribution_correction``
object.

None
------------------------------------------------------------
This distribution correction type is a null correction which may be used to disable distribution correction entirely.
The raw rain rate field is not modified.

type
    **Enumerate, "none"** -
    Constant value which disables distribution correction.

Clamp
------------------------------------------------------------
This distribution correction simply clamps rain rate to a user defined maximum value.  Although simple, this
correction has shown to be surprisingly effective.  The stochastic evolution of a nowcast occurs (largely)
independently at each level of the cascade (i.e. each spatial scale).  This means it is possible for multiple levels
of the cascade to experience a statistically strong positive noise impulse at the same time step; effectively
constructive interference of random waves.  When this ocurs very large rain rates may be generated.

Without some form of distribution correction such a rain rate will be allowed to persist in the nowcast and impact
multiple forecast time step.  However by clamping the rate to a physically realistic value the impact of such random
events is mitigated.  In most cases the rain rate will peak at the clamped value for at most a few time steps before
being reduced by the same stochastic process that led to it in the first place.

type
    **Enumerate, "clamp"** -
    Constant value which selects the clamp based distribution correction algorithm.

max_rate
    **Real, mm/hr** -
    Maximum rain rate at which to clamp forecast values.

Probability Matching
------------------------------------------------------------
This distribution correction algorithm measures the exact distribution of rain rate values throughout the domain
at the analysis time (see :ref:`reference_time<reference_time>`) and then imposes the same distribution on all forecasts
This is performed by sorting all rain rates within the analysis and forecast grids, then overwriting the forecasting
grid rates with the equivalently ranked rates from the analysis field.

For example, consider the following 3x3 grid at analysis time which results in the sorted target distribution shown
below:

.. math::

  \begin{bmatrix}
    0.0 & 7.3 & 1.0 \\
    3.0 & 0.0 & 0.0 \\
    2.2 & 1.0 & 0.2
  \end{bmatrix}^{Analysis}
  \Rightarrow
  \begin{bmatrix} 0.0 & 0.0 & 0.0 & 0.2 & 1.0 & 1.0 & 2.2 & 3.0 & 7.3 \end{bmatrix}^{Target}

Now consider the following forecast generated by the engine.  The left matrix shows the raw forecast rates, and the
right matrix shows the ordinal rankings of the raw rain rates:

.. math::

  \begin{bmatrix}
    6.5 & 0.0 & 4.5 \\
    5.5 & 0.0 & 1.5 \\
    0.0 & 1.5 & 2.5
  \end{bmatrix}^{Forecast}
  \Rightarrow
  \begin{bmatrix}
    9 & 1 & 7 \\
    8 & 2 & 4 \\
    3 & 5 & 6
  \end{bmatrix}^{Rankings}

To peform probabiliyt matching, the rain rates in the forecast are replaced based on their rankings with the value
found at the correponding index of the target distribution of rain rates from the analysis:

.. math::

  \begin{bmatrix}
    9 & 1 & 7 \\
    8 & 2 & 4 \\
    3 & 5 & 6
  \end{bmatrix}^{Rankings}
  \Rightarrow
  \begin{bmatrix}
    7.3 & 0.0 & 2.2 \\
    3.0 & 0.0 & 0.2 \\
    0.0 & 1.0 & 1.0
  \end{bmatrix}^{Corrected}


Notice that all of the same rain rate values from the original analysis grid (at the forecast reference time) are
present in the forecast.  This characteristic is independent of the forecast lead time.  An implication of this is
that as a strong raining cell is advected out of the domain it will be forced to appear at another location.  This
is often observed as a 'bloom' of rainfall that occurs as observed rain leaves the domain.  Remaining rain is forced
to take on the higher rain rates from the advected-out cell, this effect cascades down eventually causing non-raining
areas to take on the low rates.

type
    **Enumerate, "probability-matching"** -
    Constant value which selects the probability matching distribution correction algorithm.

Probability Matching with Advecton Compensation
------------------------------------------------------------
This distribution correction algorithm extends the basic probability matching concept to account for the artifacts
that occur when rainfall from the original analysis distribution is advected out of the forecast domain.  It does
this by periodically reinitializing the analysis distribution based on the current nowcast.

During the forecast process a record is kept of which pixels have been 'advected in' to the forecast domain.  This
allows the distribution correction to know the proportion of pixels which correspond to grid cells in the oiginal
analysis.  When the ratio of 'original' to total grid cells drops below a user defined threshold, the target rain
rate distribution is reinitialized.

type
    **Enumerate, "probability-matching-adv"** -
    Constant value which selects the advection compensating probability matching distribution correction algorithm.

restart_ratio
    **Real** -
    When the fraction of grid pixels in the forecast that can be traced to the original analysis grid falls below this
    threshold the target distribution will be reinitialized using the current nowcast grid.

Shift and Scale
------------------------------------------------------------
This distribution correction algorithm attempts to preserve the wet area ratio and mean areal rain rate of the
original rainfall analysis.  It does this using the following method:

1. The distribution of rain rates in both the original analysis and forecast grids is determined.
2. The rain rate in the forecast which corresponds to the rain/no-rain boundary in the original analysis is
   found.  For example, if 25% of the analysis image has no rain, then the rain rate at the 25% percentile
   of the forecast distribution is found.
3. All values in the forecast below the determined rain rate are set to zero.  This enforces the required wet
   area ratio.
4. All values in the forecast above the determined rain rate are scaled (multiplied) by a constant value.  The
   value of the constant is chosen to minimize the difference between the mean areal rain rate of the forecast
   and the original analysis.

type
    **Enumerate, "shift-scale"** -
    Constant value which selects the shift and scale distribution correction algorithm.

Match Local Wet Area Ratio
------------------------------------------------------------
This distribution correction algorithm attempts to match the Wet Area Ratio of local regions in the nowcast to
those observed in a model input.  This is done by dividing the domain into a coarse grid of local areas defined
by the user.  At each time step, the WAR of each coarse grid cell in the first model input and the current
nowcast is analysed.  If the WAR of the model is higher than the nowcast, no action is taken since the distribution
correction is unable to create new rain.  However if the WAR of the model is lower than the nowcast, then raining
pixels are suppressed in the nowcast.  This is achieved by selecting the 'N' raining pixels in the nowcast area
that have the lowest intensity and setting them to a zero rain rate, where 'N' is the number of raining pixels that
the nowcast has in excess of the model.

To ensure a smooth transition between observations and model inputs, the Wet Area Ratio of the model is not actually
used directly as described above.  Instead, a 'target' WAR is maintained which is initially set to the WAR values
analysed from the observation stream.  Each time step, the target WAR is moved closer to the model input values by
some tunable increment.  This allows wet areas present in the observation to persist initially and move towards the
model values over time.

The threshold used to enforce the 'N' raining pixels is bilinearly interpolated rather than being applied uniformly
within each coarse grid cell.  This helps to reduce artifacts at the border between coarse grid cells caused by a
sharp change in target WAR (e.g. between zero and non-zero WAR cells).

type
    **Enumerate, "local-war"** -
    Constant value which selects the local Wet Area Ratio matching algorithm.

local_size
    **Real, km** -
    Size of a cell in the coarse analysis grid used to determine local Wet Area Ratios.  This value is used for
    both X and Y side lengths of the coarse cell.  It is not possible to use non-square cells in the analysis
    grid.

blend_halflife
    **Integer, seconds** -
    This parameter is used to tune the length of time used to transition the target WAR values from those of the
    initial observation to those of the model.  At the lead time given by this parameter the model input will
    account for approximately half of the target WAR (i.e. this is the lead time where the target WAR will be
    roughly half way between the original observation and model WAR values).  Note that the model values contribute
    a fixed increment each time step, so the transition between observation and WAR values is not a linear
    interpolation.

.. _Observation stream:

Observation stream
============================================================
In the most common mode of operation STEPS runs a *conditional simulation* to generate an ensemble of nowcasts.
That is, the initial state of the nowcast is *conditioned* on the observations at the forecast reference time.
When a conditional simulation is desired, the model must be informed about the source of the observation data.

All of the parameters described in this section should be nested within the top level ``observation`` object.

.. _input:

input
    **Object** -
    This object configures the method used to acquire raw observation grids for a given time.  Multiple different
    input schemes may be supported by the engine (for example NetCDF, GRIB etc).  The exact scheme to be used is
    selected by setting the ``type`` attribute of this object.  Additional scheme dependent attributes will be
    required, check their documentation for details. (see :ref:`NetCDF input`)

.. _backfill:

backfill
    **Object, optional** -
    If the raw input field may contain cells which are flagged as missing or invalid, a fallback input source may
    be configured.  This secondary input will be used to 'backfill' grid cells which are flagged as missing from the
    primary source.  For example a satellite based precipitation product could be used to backfill the corners of
    a square radar based product where the cells are outside the maximum range of the radar.

    The contents of this object take the same format as the main :ref:`input<input>`.

    If this object is omitted then any invalid grid cells in the primary input source will simply be replaced with
    a rain rate of 0.0.

backfill_reference_time
    **Timestamp, ``yyyy-mm-ddThh:mm:ssZ``, optional** -
    Allows the user to set the reference time to be used for the :ref:`backfill<backfill>` input.  This is particularly
    useful when the backfill data source is an NWP product.  If omitted, the reference time provided to the backfill
    input manager is undefined and should not be used.

cache
    **String, directory path, optional** -
    Path to a directory which will be used to cache observation data.  Use of this feature can significantly improve model
    runtimes in an operational context because the current (lag-0) observation for a given run is also required as the
    previous (lag-1) observation for the next run.  Using the cache means that the linear to log transformation, cascade
    decompoition and other proceses do not have to be peformed on the lag-1 data each run.  It also enabled temporal
    smoothing of various tracked parameters such as the PSD and autocorrelations.

    If this setting is omitted the caching will be disabled for the observation stream.  This will cause lag-1 paramters
    to be recalculated as needed for each forecast run, and prevent time smoothing operations.

    This setting supports the use of user defined format parameters, see :ref:`user_parameters<user_parameters>`.

.. _cache_compression:

cache_compression
    **Integer, optional** -
    Specify the internal compression level (between 0 and 9) used to compress the stream cache.  Defaults to ``0`` which
    disables compression entirely.

cache_precision_reduction
    **Integer, optional** -
    When non-zero, causes the specified number of lower order bits in the floating point arrays of a cache entry to be
    masked out.  This effectively reduces the significant digits stored in each floating point value and therefore allows
    for more efficient compression of the cache entry.  Defaults to 0.  This setting is only valid when used when cache
    compression is enabled via :ref:`cache_compression<cache_compression>`.

flow_scale
    **Real** -
    A scalar factor which is applied to diagnosed flow fields for this observation stream.  This value can be used to
    correct systematic under or over estimation of advection speeds in the observation by the optical flow algorithm.  If
    no systematic error has been diagnosed then the value should be set to 1.0.

.. _stddev_scale:

stddev_scale
    **Array of reals, optional** -
    A set of scale factors which are applied to the diagnosed spatially varying standard deviation for each cascade level.
    The number of values in the array must match the number of cascade levels.  This value is typically used to correct for
    systematic over-estimation of the standard deviation in the last (small scale) level of the cascade caused by the
    inclusion of very high frequencies.

    For example, consider cascade levels with evenly spaced (in log scale) crossovers at 256, 128, 64, 32 and 16km using
    a 1km grid.  The middle cascade levels will contain a single "scale" (power of two) of wavelengths while the last level
    contains four scales (1-2, 2-4, 4-8, 8-16).  In this setup the estimation of the standard deviation in the last level
    that should be representative of the "middle" wavelength around 6km will be dominated by the short wavelengths. It
    may be necessary to manually scale down the standard deviations diagnosed in this cascade level to prevent excessive
    structure at very short wavelengths.  A factor of 0.25 would be a good "first guess" in this case.

    Run the model with verbose traces enabled to output the whole field standard deviation for each cascade level.  These
    values can be plotted as a function of the central wavelength of the matching cascade levels.  In general this will
    reveal a log-linear relationship, with the first and last cascade levels showing deviation from the linear relationship.
    The appropriate scaling factor for the final cascade level needed to preserve the log-linear behaviour can thus be
    determined.  It is recommended that the top (large scale) cascade level should NOT be scaled since the apparent
    misdiagnosis of standard deviations for this level is a result of the size of the :ref:`parameter_radius<parameter_radius>`
    setting and is already accounted for by the presence of a non-zero spatially varying mean.

smooth_psd
    **Real** -
    Time smoothing factor applied to the diagnosed PSD.  A value of 0.0 disables time smoothing entirely so that the
    diagnosed values are used without modification.  A value of 1.0 persists the initial values from the earliest avaialble
    diagnosis without ever updating.

smooth_autocorrelation
    **Real** -
    Time smoothing factor applied to the diagnosed autocorrelations.  A value of 0.0 disables time smoothing entirely so
    that the diagnosed values are used without modification.  A value of 1.0 persists the initial values from the earliest
    avaialble diagnosis without ever updating.

Model streams
============================================================
This section will describe the configuration of NWP model inputs.  This functionality will be provided in a future
release.

.. _Output products:

Output products
============================================================
The primary output from the model core is an ensemble of rain rate forecats.  This product may be output directly,
however the engine also supports generation of several other derived products.  These include ensembles of
rainfall accumulations, ensemble means, and probabilistic products.  Any number of output products may be genrated
at once.

Output products are defined using a tree structure where the output of one product forms the input of one or more
child products.  At the top of the heirarchy is a single product which is the raw ensemble of rain rates which is
output by the model core.  This product is always created.  All other output products are configured by the user
as children of another product.  The avaialble child product types are listed in the table below.

======================= ========================================================================================================
Name                    Description
======================= ========================================================================================================
accumulation_scaled     Converts input to a desired accumulation length through simple scaling
accumulation_sliding    Converts input to a desired accumulation length by aggregating consecutive time steps with a sliding window
accumulation_sequential Converts input to a desired accumulation length by aggregating consecutive time steps to a sequence of non-overlapping windows
ensemble_mean           Determines the ensemble mean
filter_members          Filters the ensemble members which are output
filter_time             Filters the range of forecast lead times which are output
median_filter           Performs a ``n`` x ``n`` median filter on the input images
percentiles             Calculates a set of user requested percentiles
probabilities           Calculates probabilistic forecasts using user defined thresholds
probabilities_spatial   Calculates probabilistic forecasts using user supplied grids of spatially varying thresholds
time_lagged             Integrates time lagged members from previous forecast cycles
======================= ========================================================================================================

The following parameters are general to products and apply to all types except where noted.  The subsections below
describe additional parameters that are specific to the product type.

children
    **Object, optional, ensemble type products only** -
    This object contains a list of child products which will be generated based on the output of this product.  Each
    entry in this list is a name-value pair, where the name is one of the product types listed in the table above and
    the value is an object containing the configuration for the child product.  In this way the tree based structure of
    outputs is established.

    An example of a child configuration is shown below

    .. code-block:: none

       children
       {
         ensemble_mean
         {
           ...
         }
         accumulation_sliding
         {
           ...
           children
           {
             percentiles
             {
               ..
             }
           }
         }
         probabilities
         {
           ...
         }
       }

    .. note::
        Child products can only be specified for products that produce an ensemble.  This includes all accumulation
        products, filter products and time lagged ensembles.  Products such as the ensemble mean, percentiles and
        probabilities cannot have children.

output
    **Enumerate, "none" | "netcdf"** -
    Constant value which selects the type of file writer to use when writing this product.  If set to ``none`` the
    product will not be output at all.  This is useful when the product represents an intermediate step in the generation
    of a final product.  If writer other than ``none`` is configured additional parameters may be required as part of
    the output object.  These will be described in the documentation for the writer type itself.  See
    :ref:`NetCDF output`.

.. _product-metadata:

metadata
    **Object, optional** -
    This optional configuration item establishes a list of metadata which will be made available to the product writer.
    How the values are actually used is dependent on the specific type of product writer being used.  For example, the
    NetCDF based writers will set these metadata as global attributes in the output file.  See :ref:`NetCDF output`
    documentation on :ref:`metadata<netcdf-metadata>` for more details.

    The object consists of a set of name-value pairs where the value can be either a simple string or a sub-object
    specifying an explicit ``type`` and ``value``.  In the latter case, the ``type`` must be set to one of ``i8``,
    ``u8``, ``i16``, ``u16``, ``i32``, ``u32``, ``i64``, ``u64``, ``f32``, ``f64``, ``time``, or ``string``.

    The value of each metadatum (in either simple string or sub-object format) supports the use of user defined format
    parameters, see :ref:`user_parameters<user_parameters>`.

    The example configuration below establishes 5 global metadata attributes.  Of these, 2 will be strings, one a 32-bit
    integer (with the value 2), and two 32-bit floats.  Note that the ``title`` and ``station_id`` metadata are using
    user parameters to set their values indirectly.

    .. code-block:: none

       metadata
       {
          institution  "Commonwealth of Australia, Bureau of Meteorology (ABN 92 637 533 532)"
          title        "Nowcast ensemble for {param:site_name}"
          station_id   { type i32 value "{param:site_id}" }
          station_lat  { type f32 value -37.852 }
          station_lon  { type f32 value 144.752 }
       }

    Metadata for a given product is always automatically inherited by any child products.  Child products are free to
    overwrite metadata from their parents using their own metadata settings.

accumulation_scaled
------------------------------------------------------------
This product multiplies the input grids (as rain rate or accumulations) by a scalar value to convert them into an accumulation
of the desired length.  This implies that a constant rain rate is assumed at each location between time steps.  This can be
useful to cheaply convert a rain rate output into a single step accumulation, provided that the time step is short enough that
the constant rain rate assumption is not problematic.

accumulation_length
    **Integer, seconds** -
    Sets the target accumulation length in seconds

accumulation_sliding
------------------------------------------------------------
This product converts an input sequence of non-overlapping accumulations into a series of longer accumulations using a sliding
window.  For example, it can convert a sequence of 5-min accumulations representing 0-5min, 5-10min, 10-15min, etc. into a
sequence of 1-hour accumulations representing 0-60min, 5-65min, 10-70min etc.

Use of an input is also supported to allow initialization of the early time steps where the accumulation window would overlap
the reference time.  This allows the "in the past" part of the window to be initialized using observation data while the
remaining part is filled using forecast time steps.

If an input is supplied then the valid times will match the input sequence.  Continuing the above example, the first output
forecast would represent T-55min to T+5min, consisting of 55min of observed accumulation and 5min of forecast.  If no input
is supplied then the first output forecast will be for the accumulation window that entirely overlaps the forecast times.
In the example above this would be the 0-60min window.

accumulation_length
    **Integer, seconds** -
    Sets the target accumulation length in seconds

input
    **Object, optional** -
    Configuration for an input (see Observation stream :ref:`input<input>` setting) that can be used to acquire the partial
    accumulations leading up to and including the forecast reference time.  The input source configured must supply
    accumulations at the same time step and accumulation length as the input to the product (i.e. if aggregating up from
    5-min forecast time steps, the length AND period of the configured input must also be 5-min).

cache_compression
    **Integer or 'none', optional** -
    Sets the compression level used to compress the in-memory cache of the previous 'N' grids per member that are necessary
    to implement the sliding window.  This setting can significantly reduce memory consumption for configurations involving
    a long sliding window, very large grids, or both.  Use of compression comes at a runtime cost, so this value should be
    tuned carefully to balance memory consumption with impact to runtime.  Defaults to ``none``.

accumulation_sequential
------------------------------------------------------------
This product converts an input sequence of non-overlapping accumulations into a sequence of longer non-overlapping
accumulations.  For example, it can convert a sequence of 5-min accumulations representing 0-5min, 5-10min, 10-15min, etc.
into a sequence of 1-hour accumulations representing 0-60min, 60-120min, 120-180min etc.

Clock synchronization of the accumulation windows is supported.  If enabled, this will cause the sequence of windows to
be offset so that end time of each one aligns "neatly" with the UTC clock boundaries.  For example, 1-hour accumulations
could be generated to match 01:00-02:00, 02:00-03:00 etc regardless of whether the forecast reference time itself aligns
with a whole hour bondary.

Use of an input is also supported when using clock synchronization to allow initialization of the early time steps where
the desired accumulation window would overlap the reference time.  This allows the "in the past" part of the window to be
initialized using observation data while the remaining part is filled using forecast time steps.

accumulation_length
    **Integer, seconds** -
    Sets the target accumulation length in seconds

clock_offset
    **Integer, seconds, optional** -
    If :ref:`clock_sync<product-clock_sync>` is ``true`` then this value may be used to offset the start of the accumulation
    windows from 00:00 by the given number of seconds.  For example, a 1-hour clock synchronized accumulation will output
    accumulations for 00:00-01:00, 01:00-02:00 and so on by default.  If a clock offset of 1800 seconds is provided, then the
    sequence will align on the half hour boundaries instead: 00:30-01:30, 01:30-02:30 and so on.  Defautls to 0.

.. _product-clock_sync:

clock_sync
    **Boolean, optional** -
    If true, then the sequential accumulation windows will be configured to align "neatly" with UTC clock boundaries.  Consider a
    forecast reference time of 12:30Z, with an output accumulation length of 1 hour.  If this value is false then accumulations
    will be generated for 12:30-13:30, 13:30-14:30, and so on.  If this value is true, then the generated accumulations will be
    for 12:00-13:00, 13:00-14:00, and so on.  If no :ref:`input<product-input>` is supplied to provide historical QPE for the
    first accumulation that overlaps the referece time, then this first accumulation will be suppressed (making the first output
    for 13:00-14:00).  Defaults to ``false``.

.. _product-input:

input
    **Object, optional** -
    Configuration for an input (see Observation stream :ref:`input<input>` setting) that can be used to acquire the partial
    accumulations leading up to and including the forecast reference time.  The input source configured must supply
    accumulations at the same time step and accumulation length as the input to the product (i.e. if aggregating up from
    5-min forecast time steps, the length AND period of the configured input must also be 5-min).

ensemble_mean
------------------------------------------------------------
This product determines the mean of the ensemble and may be used to generate either a mean rain rate or mean accumulation
depending on the parent product.  No additional configuration is required.

filter_members
------------------------------------------------------------
This product allows for the number of members passed to the output and child products to be reduced by specifying a subset of
members to be forwarded.  For example, a user may wish to generate a large ensemble for use in probabilistic products, but
only output a smaller number of raw members to save on disk space.  If no members are selected by the filter then any output
and all downstream products will be suppressed entirely.

members
    **Array of integers** -
    List of members to output.  Members that do not match one of the member numbers specified in this list will not be output
    or passed to child products.

require_all
    **Boolean, optional** -
    Configures whether the members listed for filtering must be supplied by the parent product.  Defaults to ``true``.  If this
    setting is ``true`` then the forecast will be teriminated with an error if any of the selected members are not available.
    If this setting is ``false`` then any selected members which are not available are silently dropped from the output and
    not supplied to downstream products.

filter_time
------------------------------------------------------------
This product allows for the output to be filtered to a reduced set of lead times.  If no lead times are selected by the filter
then any output and all downstream products will be suppressed entirely.

min_lead_time
    **Integer, seconds, optional** -
    Set the minimum forecast lead time to output.  Forecasts for lead times less than this value will not be output or passed to
    child products.

max_lead_time
    **Integer, seconds, optional** -
    Set the maximum forecast lead time to output.  Forecasts for lead times greater than this value will not be output or passed
    to child products.

median_filter
------------------------------------------------------------
This product performs a ``n`` x ``n`` median filter on the input grids before forwarding to output and/or downstream products.
This can be used to smooth output forecasts that contain too much high frequency power (although attempts should be made to
resolve such artifacts in the forecast itself using mechanisms such as :ref:`stddev_scale<stddev_scale` before resoring to
a median filter on the output).

kernel_size
    **Integer** -
    Sets the side length of the kernel used to perform median filtering on the input grids.  Must be an odd number.

percentiles
------------------------------------------------------------
This product generates a set of rain rate or accumulation values corresponding to the Nth percentile of the input ensemble.
The user is able to select any number of percentile grids to generate.  Percentiles that do not exactly match a rank in
the input ensemble are estimated using a linear interpolation based on the nearest rank either side.

percentiles
    **Array of reals** -
    List of percentiles to calculate.

probabilities
------------------------------------------------------------
This product generates probability of exceedence forecasts based on a set of user defined rain rate or accumulation thresholds.

thresholds
    **Array of reals** -
    List of rain rate or accumulation thresholds for which probabilitiy of exceedence will be calculated.

probabilities_spatial
------------------------------------------------------------
This product generates probability of exceedence forecasts based on a set of user defined, spatially varying rain rate or
accumulation thresholds.  Typically this will be used to create "probability of exceeding average recurrence interval" style
products.  For example, the spatially varying thresholds may contain the 1 in 1, 10 and 100 year accumulation values, which are
different at each grid cell.  The output product would then provide the probability of exceeding a 1, 10, and 100 year event
at each cell - taking into account the localized climatology.

thresholds_path
    **String, file path, (probabilities_spatial product only)** -
    Path to a NetCDF file containing the spatially varying thresholds for which probability of exceedence will be
    calculated.  The file must contain a 3D variable where the first dimension is the exceedence event (for example ARI or
    AEP represented by the spatially varying thresholds), and the next two dimensions represent the spatial dimensions
    matching the forecast domain.

    This setting supports the use of user defined format parameters, see :ref:`user_parameters<user_parameters>`.

thresholds_variable
    **String, (probabilities_spatial product only)** -
    Name of the variable containing a set of spatially varying thresholds.  The variable must be three dimensional.  The
    first dimension represents the exceedance event being calcualted.  For example the ARI or AEPs for which probabilities
    will be generated.  The remaining two dimensions are the spatial dimensions which much exactly match the size of the
    forecast domain.

time_lagged
------------------------------------------------------------
This product allows integration of additional ensemble members which are extracted from previous forecast cycles (so-called
"lagged" ensembles).  The lagged members are not read directly from previously generated output products, but rather this
product maintains a local cache of lagged data.  Each time a new members is passed to this product from the parent product
it will be stored in the local cache.  Then when the member is passed to the downstream output and/or products additional
lagged members will be read from disk and passed as well.

cache
    **String, (time_lagged product only)** -
    Set the path prefix (local directory or URL) for the cache of data used for time lagging of ensemble members.  During each
    forecast run lagged members will be read from this location, and new members will be output for use in subsequent runs.

    This setting supports the use of user defined format parameters, see :ref:`user_parameters<user_parameters>`.

cache_compression
    **Integer, optional** -
    Specify the internal compression level used to compress the time lagged member cache.  Defaults to ``1``.

cache_resolution
    **Real, optional, (time_lagged product only)** -
    If set, causes the the values in each forecast to be rounded to this resolution before being stored in the time lag cache.
    When enabled, the forcast values are then stored as 16-bit integers rather than 32-bit floats.  This can result in significant
    savings in terms of both time and space for the time lagging functionality.  For example, when time lagging a rain rate
    ensemble setting this value to 0.1 will cause all rain rates to be rounded to the nearest 0.1 mm/hr in the lag cache.

lags
    **Integer, (time_lagged product only)** -
    Sets the number of previous forecast cycles from which to integrate lagged members.  The time lagged ensemble feature requires
    that the forecasty cycle (i.e. how often new ensembles are created), matches the forecast time step.  A side effect of using
    time lagged ensembles is that the number of forecasts available to the output and downstream products will be reduced by the
    number of lags.  This is to ensure that the same number members is available at all lead times.

.. _NetCDF input:

NetCDF input
============================================================
This section describes the configuration needed for ``input`` object (see :ref:`input<input>`) when using a NetCDF reader
as the input type.  The ``netcdf`` input type is able to detect and handle several common input situations:

    - Each input grid stored as a 2D variable in a separate file.  Typically in this scenario the path of each file will
      encode the input time.  In this situation the outer most (first) dimension of the file is assumed to be the Y dimension
      as is requried by the CF convnetions (i.e. ``variable[y][x]``).
    - Input file contains a 3D variable.  In this situation the outer most dimension of the variable is expected to
      provide the time coordinates for the input (i.e. ``variable[time][y][x]``).  The use of 3D inputs is common for
      deterministic NWP inputs.
    - Input files contains a 4D variable.  In this situation the outer most dimension of the variable must provide the
      the input member number, while the next dimension provides the time coordiantes (i.e. ``[member][time][y][x]``).
      The use of 4D inputs is common for probabilistic NWP inputs, as well as STEPS ensembles for verification.

type
    **Enumerate, "netcdf"** -
    Constant value which selects the ``netcdf`` input type.

path
    **String, file path** -
    A format string which can be used to build the path of an input file.  The string may contain references to variables
    which are provided by the model, which allows the path to encode member numbers and timestamps.  The syntax for
    referencing a variable is described by the `Format String Syntax`_ documentation of the `{fmt} library`_ which is
    used to format the path string.

    The following variables are exported by the model:

    ========== =============================================== ========= ============================ ====================
    Name       Description                                     Type      Example Format               Example Output
    ========== =============================================== ========= ============================ ====================
    param:name :ref:`User defined parameters<user_parameters>` Various   ``{param:site:02d}``         002
    time       Data time of input grid to read                 Timestamp ``{time:%Y%m%d_%H%M%S}``     20200331_112253
    reference  Input model reference time                      Timestamp ``{reference:%Y%-%m-%d_%H}`` 2020-03-31_11
    lead       Input model lead time in seconds                Integer   ``{lead}``                   5400
    lead_hr    Input model lead time (hours component)         Integer   ``{lead_hr:02d}``            01
    lead_min   Input model lead time (minutes component)       Integer   ``{lead_min}``               30
    member     Input model member number                       Integer   ``{member:03d}``             004
    ========== =============================================== ========= ============================ ====================

variable
    **String** -
    Specifies the name of the variable within the NetCDF file(s) which contains the rainfall data to read.  The varialbe
    is expected to supply `CF Conventions`_ compliant metadata.  Integer packed variables are supported using the standard
    ``scale_factor`` and ``add_offset`` metadata.  If the input may contain missing values then the standard ``_FillValue``
    attribute should be used to indicate how they are represented.  Finally, the variable should specify its native units
    using the ``units`` attribute.

    STEPS expects input values to be supplied as rain rates in mm/hr.  This corresponds to a ``units`` value of
    ``kg m-2 h-1``.  If the input units are not exactly equal to ``kg m-2 h-1`` then the model will attempt to find a
    simple multiplicative conversion.  For example, conversion from mm/s (``kg m-2 s-1``) will be automatically performed
    by multiplying the input value by 60.  Similarly, the input units could specify the rate as mm per 10 minutes
    (``kg m-2 / (600 s)``) which would cause inputs to be multiplied by 6.

    If the input product supplies rainfall accumulations rather than rain rates, then STEPS will attempt to determine the
    length of the accumulation and treat the input as a rate over the determined accumulation length.  This is achieved
    using additional variables in the file which specify either the accumulation length directly, or the accumulation
    start and end times from which the length may be dtermined.  See :ref:`duration_variable<duration_variable>`,
    :ref:`accumulation_start_variable<accumulation_start_variable>` and
    :ref:`accumulation_end_variable<accumulation_end_variable>`.

    .. _CF Conventions: https://cfconventions.org

units_override
    **String, optional** -
    If supplied, will cause the ``units`` metadata attribute of the input variable (if present) to be ignored and instead
    use the value specified.  This is useful to correct the units for inputs where a CF compliant ``units`` attribute is
    missing, or incorrect.

.. _duration_variable:

duration_variable
    **String, optional** -
    Specifies the name of a scalar variable which specifies the length of accumulations provided by the file.  The variable
    must have a valid ``units`` attribute which specifies a time length unit (e.g. ``s``).

.. _accumulation_start_variable:

accumulation_start_variable
    **String, optional** -
    Specifies the name of a scalar or 1D variable which specifies the start time of accumulation(s) provided by the file.
    In the case of a 1D variable, only the first element of this and the ``accumulation_end_variable`` will be compared
    to determine the nominal accumulation length.  The variable must supply a CF compatible ``units`` attribute specifying
    a time point (e.g. ``seconds since 2020-03-31 13:29:00 UTC``).

.. _accumulation_end_variable:

accumulation_end_variable
    **String, optional** -
    Specifies the name of a scalar or 1D variable which specifies the end time of accumulation(s) provided by the file.
    In the case of a 1D variable, only the first element of this and the ``accumulation_start_variable`` will be compared
    to determine the nominal accumulation length.  The variable must supply a CF compatible ``units`` attribute specifying
    a time point (e.g. ``seconds since 2020-03-31 13:29:00 UTC``).

.. _x0:

x0
   **Integer, optional** -
   If supplied, indicates that the input data is to be read from a slice of the input variable grid that starts at this
   index of the X dimension.  The size of the slice will always match the :ref:`grid_cols<grid_cols>` setting.  If this is
   omitted then the size of the X dimension must match :ref:`grid_cols<grid_cols>` exactly.  If this value is supplied then
   :ref:`y0<y0>` must also be supplied.

.. _y0:

y0
   **Integer, optional** -
   If supplied, indicates that the input data is to be read from a slice of the input variable grid that starts at this
   index of the Y dimension.  The size of the slice will always match the :ref:`grid_rows<grid_rows>` setting.  If this is
   omitted then the size of the Y dimension must match :ref:`grid_rows<grid_rows>` exactly.  If this value is supplied then
   :ref:`x0<x0>` must also be supplied.

max_open_files
   **Integer, optional** -
   Sets the maximum number of NetCDF files that may be held open concurrently for this input.  Defaults to 1.  This value is
   very important for scenarios where 1) multiple input grids will be required from this input, 2) multiple input grids shall
   be read from each input file, and 3) multiple input files will be required during the forecast.  If any of these three
   conditions are not met, then the default value of 1 is sensible.

   By far the most common scenario where this value should be set is for ensemble based model inputs, where each input member
   is contained in a separate file, with each file containing multiple time steps (i.e. 3D input files, one per member).  In
   this case, this value should probably be set to match the number of input members that will be used.  This will allow the
   model core to keep each input member file open.  Otherwise, each time a grid from a different member is required, the
   previously used member file will be closed and the new one opened - effectively opening and closing files for every read.

.. _NetCDF output:

NetCDF output
============================================================

path
    **String, file path** -
    A format string which can be used to build the path of the output file.  The string may contain references to variables
    which are provided by the model, which allows the path to encode information such as timestamps.  The syntax for
    referencing a variable is described by the `Format String Syntax`_ documentation of the `{fmt} library`_ which is
    used to format the path string.

    The following variables are exported by the model:

    ========== ===================================================== ========= ============================ ==============
    Name       Description                                           Type      Example Format               Example Output
    ========== ===================================================== ========= ============================ ==============
    param:name :ref:`User defined parameters<user_parameters>`       Various   ``{param:site:03d}``         002
    reference  Model run reference time                              Timestamp ``{reference:%Y%-%m-%d_%H}`` 2020-03-31_11
    member     Index of first member output (ensemble products only) Integer   ``{member:02d}``             03
    ========== ===================================================== ========= ============================ ==============

.. _netcdf-metadata:

metadata
    **Object, optional** -
    This configuration item is actually part of the :ref:`product<Output products>` configuration.  It is documented again
    here to describe how metadata impacts the output of NetCDF product files.

    All attributes other than those starting with a period (``.``) will be written as global attributes to the output
    NetCDF file.  The type of the attribute will match the type of the metadata attribute (see
    :ref:`metadata<product-metadata>`).

    Two special attributes are also available for use with NetCDF writers.  The names of these attributes start with a
    period (``.``) so they will not be written to the output file directly:

    - ``.reference-file`` - This string attribute specifies the path to a NetCDF file which may be used as a reference
      for grid coordinates, cell bounds and projection related metadata.  If set, the file must exist and contain a
      variable as specified by the ``.reference-variable`` metadata setting.
    - ``.reference-variable`` - This string attribute specifies the name of a gridded variable present in the NetCDF file
      specified by ``.reference-file``.  The last two dimensions of this variable will be assumed to be the Y and X
      coordinates of the grid.

    If ``.reference-file`` is set, the following aspects of the output file are derived from the reference:

    - X and Y dimension names
    - Coordinate variables for X and Y dimensions
    - Bounds variables for X and Y dimensions (if present in reference file)
    - Grid mapping variable (if present in the reference file)

    The NetCDF writer will also automatically add global attributes for ``Conventions``, ``source`` and ``history``.

variable_reference_time
    **String, optional** -
    Name of the variable in which the model reference time will be written.  Defaults to ``reference_time``.

dimension_time
    **String, optional** -
    Name of the time dimension in the output file.  Defaults to ``time``.

variable_start_time
    **String, optional** -
    Name of the variable in which accumulation start times (if any) will be written.  Defaults to ``start_time``.

flatten_series
    **Boolean, optional** -
    Suppress the creation of the outermost product dimension if it has a size of exactly 1.  This dimension typically
    represents the member number (ensembles) or threshold (percentiles and probabilities).  As a result, the rank of the
    main variable is reduced by 1, and the coordinate variable for the supressed dimension is output as a scalar.

    For example, if an ensemble has a single member it would normally be output as ``var[m][t][y][x]`` where the length
    of the ``m`` dimension is 1.  If this option is enabled, such an ensemble would be output as ``var[t][y][x]`` instead.

flatten_time
    **Boolean, optional** -
    Suppress the creation of the time dimension if it has a size of exactly 1.  As a result, the rank of the
    main variable is reduced by 1, and the time is output as a scalar instead of coordinate variable.

    For example, if an ensemble has a single forecast it would normally be output as ``var[m][t][y][x]`` where the length
    of the ``t`` dimension is 1.  If this option is enabled, such an ensemble would be output as ``var[m][y][x]`` instead.

variable
    **String, optional** -
    Name of the variable in which main product output will be written.  Defaults to ``rain_rate`` for rain rate based
    products, ``precipitation`` for rainfall accumulation based products, and ``probability`` for probablistic products.

data_type
    **Enumeration,  'i8'|'u8'|'i16'|'u16'|'i32'|'u32'|'i64'|'u64'|'f32'|'f64', optional** -
    Specify the data type used to encode the main output variable.  If using an integer based data type it is important
    to also specify the :ref:`scale_factor<scale_factor>` and :ref:`add_offset<add_offset>`.  Defaults to ``i16``.  Not used
    for probabilistic products.

compression
    **Integer or 'none', optional** -
    Specify the internal compression level used to compress the main output variable.  Defaults to ``5``.

.. _scale_factor:

scale_factor
    **Real, optional** -
    Specify the standard ``scale_factor`` value used in CF compliant integer packing.  Defaults to ``0.01``.  Not used for
    probabilistic products.

.. _add_offset:

add_ofset
    **Real, optional** -
    Specify the standard ``add_offset`` value used in CF compliant integer packing.  Defaults to ``0.0``.  Not used for
    probabilistic products.

fill_value
    **Real, optional** -
    Specify the value of the missing data flag used for CF compliant encoding.  Defaults to ``-1``.  Not used for
    probabilistic products.

direct_hdf5
    **Boolean, optional** -
    The official NetCDF API used to write output products is not designed for use in a multi-threading environment.  The
    only way to safely use the NetCDF API from multiple threads is to serialize each API call so that only one thread can
    call at NetCDF function at a time.  The writing of NetCDF products can represent a significant bottleneck to the
    performance of STEPS since no two engines (i.e. members) can be writing output to disk at once.  This problem is
    exacerbated when using internal data compression since the compression is performed while in a NetCDF function call
    and also subject to serialization.

    STEPS offers a way to ease the bottleneck imposed by the lack of multi-threading support in the NetCDF API by
    bypassing it and using the underlying HDF5 API to directly write pre-compressed data to the file.  If enabled, this
    option causes data compression to occur inside each engine thread (which can occur in parallel), with only the actual
    write to file via a HDF5 call being serialized.  Since the time used to compress data tends to exceed the disk write
    time of the compressed blocks, using this option can result in a significant boost to runtime performance.

    This option is enabled by default.  If disabled, the official NetCDF API will be used to compress and write all data.

Unit test mode settings
============================================================
There are several configuration items that may be used to customize the execution of unit tests when the STEPS executable
is run using the ``--unit-test`` command line parameter.  These options are described below.  Since the ``--unit-test``
mode does not support reading options from a configuration file, all options must be passed as extra options to the
command line using ``name=value`` pairs.

out_path
    **File path, optional** -
    Redirect unit test results to a local file.  If this option is not supplied, unit test results will be directed to
    ``stdout`` (i.e. the user console).

format
    **Enumeration, 'console'|'junit', optional** -
    Set the output format for unit test results.  Currently two formats are supported.  The ``console`` format is the
    default setting and presents results in a human readable format.  The ``junit`` format writes results in an XML
    format compatible with the JUnit testing framework.

fast_mode
    **Boolean, optional** -
    If set to true, several unit tests will run in a so called 'fast mode'.  In this mode these unit tests change the
    number of iterations of expensive tests to reduce runtime.  It is expected that such tests will then fail.  The
    purpose of this mode is to reduce unit test runtime during coverage testing, where branch coverage rather than
    code correctness is being measured.

reference_dir
    **Directory path, optional** -
    Overwrite the path where reference data needed for the unit tests is located.  This option should not be required
    if STEPS has been installed normally.

