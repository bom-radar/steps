.. _STEPS on AWS:

========================================================================================================================
STEPS on AWS
========================================================================================================================

.. role:: bash(code)
    :language: bash

Introduction
============

STEPS-AWS is a native cloud application for the Amazon Web Services platform that provides a Software as a Service
(SaaS) solution for operational nowcasting.  This approach allows clients to rapidly gain high quality nowcasts without
the overhead of self-managed compute infrastructure.  Clients simply make input data available, issue a nowcasting
request, wait for result notification, then access their output products.

The cloud application is built so that it scales dynamically with demand based on the maximum allowable time that a
client request can be queued before it is serviced.  The system supports any number of simultaneous radars/domains, and
also includes advanced features such as mosaicing of probabilistic products.

Architecture
============

    .. image:: steps_aws_arch.svg

Initial Cloud Deployment
========================
Deployment of STEPS-AWS requires an AWS account on which you have appropriate permissions to create and manage all of
the resources used by the CloudFormation stacks.  In the steps below actions for the cloud platform are described using
the AWS command line client.  These actions may also be performed using the console web interface GUI if preferred.

The following table lists variables that are used in the subsequent example commands.  The use of shell variables
is not required, and is only performed here for clarity.

================= ======================================================================= ========================
Name              Description                                                             Example
================= ======================================================================= ========================
STEPS_PLATFORM    Name of the STEPS platform to be deployed                               steps-dev
STEPS_RPM_VERSION Version of steps-aws, must match version in name of RPM file            0.0.0
STEPS_RPM_RELEASE Release of steps-aws, must match release in name of RPM file            1
STEPS_BUCKET      Name of an S3 bucket to store software artefacts (RPM, CFN templates)   steps-software
================= ======================================================================= ========================

To enable simple copy and paste of the commands in the following sections, make sure to export each of the above
variables into your current shell.  An example exporting the default value for each variable is shown below:

.. code-block:: bash

    export STEPS_PLATFORM=steps-dev
    export STEPS_RPM_VERSION=0.0.0
    export STEPS_RPM_RELEASE=1
    export STEPS_BUCKET=steps-software

Step 1: Upload RPM and cloudformation stacks to S3
--------------------------------------------------
The ``steps-aws`` RPM must be available from an S3 bucket so that it can be accessed later during creation of the
EC2 machine image (AMI).  You may reuse any appropriate bucket that already exists in the AWS account, or create a
new one specifically for this purpose.  The contents of the bucket should not be publically accessible.

.. code-block:: bash

    aws s3 mb s3://${STEPS_BUCKET}
    aws s3 cp steps-aws-${STEPS_RPM_VERSION}-${STEPS_RPM_RELEASE}.el8.aarch64.rpm s3://${STEPS_BUCKET}/
    aws s3 cp aws/steps-aws-images.template s3://${STEPS_BUCKET}/
    aws s3 cp aws/steps-aws-core.template s3://${STEPS_BUCKET}/

Step 2: Build machine image (AMI) for EC2 instances
---------------------------------------------------
A CloudFormation stack is used to automatically build an Amazon Machine Image (AMI) containing the ``steps-aws``
software that runs on each EC2 instance in the autoscaling group.  Deploy the ``steps-aws-images`` stack, while
supplying the appropriate parameters for the S3 bucket containing the RPM, and software version.

.. code-block:: bash

    aws cloudformation create-stack \
      --stack-name ${STEPS_PLATFORM}-images \
      --template-url https://${STEPS_BUCKET}.s3.amazonaws.com/steps-aws-images.template \
      --parameters \
        ParameterKey=BucketSoftware,ParameterValue=${STEPS_BUCKET} \
        ParameterKey=StepsVersion,ParameterValue=${STEPS_RPM_VERSION} \
        ParameterKey=StepsRelease,ParameterValue=${STEPS_RPM_RELEASE} \
      --capabilities CAPABILITY_IAM

The image will take a long time to build, often over an hour.  Run the below command to check on the stack creation
status.  After the AMI has been succesfully created, the status will change from ``CREATE_IN_PROGRESS`` to ``CREATE_COMPLETE``.

.. code-block:: bash

    aws cloudformation describe-stacks --stack-name ${STEPS_PLATFORM}-images --query "Stacks[0].StackStatus" --output=text

After the image building stack has been created note the stack output which contains the id of the created AMI.  This
value can be extracted directly into a variable using the following command.

.. code-block:: bash

    STEPS_AMI_ID=$(aws cloudformation describe-stacks --stack-name ${STEPS_PLATFORM}-images --query "Stacks[0].Outputs[?OutputKey=='AMI'].OutputValue" --output=text)

Step 3: Deploy the core application stack
-----------------------------------------
With the AMI now created, the core application stack can be deployed.  The most important parameters which control
node types and autoscaling behaviour are specified now.  Additional parameters beyond those shown below are available.
To inspect these, load the core stack template in the AWS console.

.. code-block:: bash

    # Add additional parameters to customize platform behaviour!
    aws cloudformation create-stack \
      --stack-name ${STEPS_PLATFORM} \
      --template-url https://${STEPS_BUCKET}.s3.amazonaws.com/steps-aws-core.template \
      --parameters \
        ParameterKey=InstanceAMI,ParameterValue=${STEPS_AMI_ID} \
      --capabilities CAPABILITY_IAM

Once the core stack has finished deploying STEPS will be ready to being processing forecast requests.  See the next
section on client integration for how to issue forecast requests and retrieve results.

Client Integration
==================
Clients interact with STEPS-AWS by sending commands to the application through an SQS FIFO queue.  These commands
request STEPS-AWS to perform actions such as generating a nowcast, or creating a mosaic.  STEPS-AWS notifies the
client about the outcome of each command through an SNS topic.

Core stack outputs
------------------
This section lists outputs of the core STEPS-AWS application stack.  These outputs are typically required to enable
correct integration with a client application.

RequestQueueArn
    ARN of SQS queue used to send commands to the application.

RequestQueueUrl
    URL of SQS queue used to send commands to the application.  This value is required by clients when issuing a
    forecast request via the AWS API or command line interface.

ResultTopicArn
    ARN of SNS topic use to send command results to the client.  This value is required by clients to subscribe to
    the results topic via the AWS API or command line interface.  Typically a client application will subscribe
    to the topic using a dedicated client SQS queue which can be used to process result notifications.

InstanceRoleArn
    ARN of an IAM Role that is assigned to the EC2 nodes used to perform forecasting.  Client applications should
    use resource-based IAM policies to ensure that this role has access to any S3 buckets needed for the forecast.
    This includes any S3 bucket used to store forecast configuration, input data, cache and skill files, and
    output data.

ClientRequesterPolicyArn
    ARN of an IAM Policy that grants access to issue requests to the SQS queue used to issue commands to the
    application.  Clients should attach this policy to the IAM Role or User that is used by the client application
    to request nowcasts.

ClientSubscriberPolicyArn
    ARN of an IAM Policy that grants access to subscribe to the SNS topic used to notify clients of command results.
    Clients should attach this policy to any IAM Role or User that wishes to be notified of the availability of new
    nowcasts.

Command protocol
----------------
Messages sent between STEPS-AWS and client applications take the form of a small JSON string.  These messages are
used only to trigger actions, and notify the client of the success or failure of those actions.  Actual data
input and output is performed separately via S3.

The forecast request message queue is implemented as an SQS FIFO queue.  This means that requests will by default be
processed strictly in the order that they are received, one at a time.  To enable parallel processing, the message
group ID must be set for each command sent.  Any commands received that use the same group ID will be processed
sequentially and in order, however commands with different group IDs may be processed out of order and in parallel.
In essence, the message group ID nominates an internal 'queue', where the messages in each queue are processed in
order, but the queues themselves are processed in parallel.

In a typical deployment, a separate message group ID will be used for each doamin (radar site / mosaic) that is to
be nowcast.  This will allow forecasts to be generated for multiple sites at once, but ensure that the requests
associated with a single site are processed in order.  All commands support inclusion of an optional ``sqs_delete``
parameter.  This option controls when the SQS request is deleted from the queue, allowing subsequent requests for the
same message group ID to continue.

The possible values for the ``sqs_delete`` parameter are:

- ``immediate`` - delete the message as soon as it is received and before handling of the request is started.
- ``after_init`` - delete the message after the forecast initialisation phase is completed for all members, but
  before processing of the first forecast time step commences.  This option is only applicable to the ``forecast``
  command and is used to ensure any cache of the observation stream has been updated before message deletion.
  If this value is specified for a command other than ``forecast`` the effect is the same as ``complete``.
- ``complete`` - delete the message after the request has been handled.  This is the default behaviour when no
  ``sqs_delete`` parameter is set.

The remainder of this section lists the supported commands.  For each command the result message returned to the client
is identical with the exception that a ``result`` attribute is added indicating whether the command succeeded.

Generate Forecast
+++++++++++++++++
Requests a STEPS forecast to be executed.  The request will be queued and strictly ordered with other requests (of
any type) that share the same SQS message group id.  In a typical deployment, the message group id should be set
to the same value as the ``domain`` parameter.

It is the user's responsibility to ensure that data required by the specified forecast configuration file is
available in S3 before the forecast request is issued.  The user must also take care to ensure that parallel
forecasts (by using different SQS message group ids) do not interfere with each other.  For example, by ensuring
that skill and cache file paths are separated by domain (group).

=============== ==============================================================================
Parameter       Description
=============== ==============================================================================
action          Literal value ``forecast``
domain          Client identifier for nowcast domain used for logging
config          S3 URL of forecast configuration file to execute
reference_time  Reference time for forecast to run in ISO8601 format
observation     Object containing setting related to the observation stream (optional)
models          Object containing setting related to the model stream(s) (optional)
user_parameters List of overrides for the values of user defined format parameters (optional)
sqs_delete      When to delete SQS request, one of ``immediate``, ``after_init``, or ``complete`` (optional)
result          Present in result messages with value set to either ``success`` or ``failure``
=============== ==============================================================================

The optional ``observation`` object supports the following parameters, which correspond to the similarly named
parameters in the ``observation`` section of the model configuration file.

======================= =====================================================================
Parameter               Description
======================= =====================================================================
reference_time          Reference time for input in ISO8601 format (optional)
backfill_reference_time Reference time for backfill input in ISO8601 format (optional)
======================= =====================================================================

The optional ``models`` object contains a set of parameters whos names must match the names of models listed
in the ``models`` section of the model configuration file.  The contents of each parameter is an object exposing
the same parameters as the ``observation`` object described above.

The optional ``user_parameters`` object contains a set of name value pairs which specify overrides for the values
associated with various :ref:`user_parameters<user_parameters>` settings in the model configuration file.  The
value of each entry may be any JSON scalar type (string, bool, integer or real), and will automatically overwrite
the value of the user paramter regardlessof whether it is specified using the simple string or explicitly typed
forms in the configuration file.

Request message example for a simple advection forecast:

.. code-block:: json

    {
      "action": "forecast",
      "domain": "radar-002",
      "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.adv.conf",
      "reference_time": "2021-03-25T00:10:00Z"
    }

Request message example for an NWP blended forecast, which also uses the NWP field to backfill the observation
stream:

.. code-block:: json

    {
      "action": "forecast",
      "domain": "radar-002",
      "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.nwp.conf",
      "reference_time": "2021-03-25T00:10:00Z",
      "observation": {
        "backfill_reference_time": "2021-03-24T18:00:00Z"
      },
      "models": {
        "ecmwf": {
          "reference_time": "2021-03-24T18:00:00Z"
        }
      }
    }

Request message example for a forecast which blends multiple NWP models:

.. code-block:: json

    {
      "action": "forecast",
      "domain": "radar-002",
      "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.nwp.conf",
      "reference_time": "2021-03-25T00:10:00Z",
      "models": {
        "ecmwf": {
          "reference_time": "2021-03-24T18:00:00Z"
        },
        "um": {
          "reference_time": "2021-03-24T12:00:00Z"
        },
        "city": {
          "reference_time": "2021-03-25T00:00:00Z",
          "backfill_reference_time": "2021-03-24T12:00:00Z"
        }
      }
    }

Result message example:

.. code-block:: json

    {
      "action": "forecast",
      "domain": "radar-002",
      "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.adv.conf",
      "reference_time": "2021-03-25T00:10:00Z",
      "result": "success"
    }

Preprocess Model Input
++++++++++++++++++++++
Requests preprocessing of a single model input to be executed.  The request will be queued and strictly ordered
with other requests (of any type) that share the same SQS message group id.  In a typical deployment, the message
group id should be set to a value based on (but not equal to) the ``domain`` parameter.  For example, prefixed
with the string ``preprocess`` as in ``preprocess-<domain>``.  This ensures that preprocessing for a domain does
not block forecasting on the same domain, while still ensuring that preprocessing over multiple domains is
parallelized.  In such arrangements care should be taken to continue issuing forecasts using the 'old' reference
times for model inputs until preprocessing for the 'new' reference time has been completed.

For example:

- Forecasts are requested every 10 minutes using the latest model reference time of 00Z and message group id
  of ``mydomain``
- At 07Z a new model run is available with a reference time of 06Z
- Preprocessing requests are issued for the 06Z model reference time using a message group id of
  ``preprocess-mydomain``
- Forecasts continue to be requested every 10 minutes using the 'old' reference time of 00Z and run in parallel
  to the model preprocesing due to the use of a different message group id
- Some time later, preprocessing requests for 06Z are completed, returning 'success' result messages
- Forecasts requests issued from this moment on now start using the 06Z model reference time

It is the user's responsibility to ensure that data required by the specified forecast configuration file is
available in S3 before the preprocessing request is issued.  The user must also take care to ensure that parallel
forecasts or preprocessing requests (by using different SQS message group ids) do not interfere with each other.
For example, by ensuring that cache file paths are separated by domain (group).

======================= ==============================================================================
Parameter               Description
======================= ==============================================================================
action                  Literal value ``preprocess``
domain                  Client identifier for nowcast domain used for logging
config                  S3 URL of forecast configuration file to preprocess
model                   Name of model from forecast configuration file to preprocess
reference_time          Reference time for model input in ISO8601 format
backfill_reference_time Reference time for model backfill input in ISO8601 format (optional)
from                    First forecast time to preprocess in ISO8601 format
forecasts               Number of forecast time steps to preprocess.  May be larger than the forecast
                        count contained in the model configuration file.
user_parameters         List of overrides for the values of user defined format parameters (optional)
sqs_delete              When to delete SQS request, one of ``immediate`` or ``complete`` (optional)
result                  Present in result messages with value set to either ``success`` or ``failure``
======================= ==============================================================================

Request message example for a simple advection forecast:

.. code-block:: json

    {
      "action": "preprocess",
      "domain": "radar-002",
      "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.nwp.conf",
      "model": "ecmwf",
      "reference_time": "2021-03-24T18:00:00Z",
      "from": "2021-03-25T00:00:00Z",
      "forecasts": 72
    }

Result message example:

.. code-block:: json

    {
      "action": "preprocess",
      "domain": "radar-002",
      "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.nwp.conf",
      "model": "ecmwf",
      "reference_time": "2021-03-24T18:00:00Z",
      "from": "2021-03-25T00:00:00Z",
      "forecasts": 72,
      "result": "success"
    }

Generate Mosaic
+++++++++++++++
Requests a mosaic product be generated.  The request will be queued and strictly ordered with other requests
requests that share the same SQS message group id.  In a typical deployment, the message group id should be set
to the same value as the ``domain`` parameter.

It is the user's responsibility to ensure that data required by the mosaic is available in S3 before the forecast
request is issued.  This may require logic at the client end to ensure all desired input nowcasts have been completed
before issuing the mosaic request.

=============== ==============================================================================
Parameter       Description
=============== ==============================================================================
action          Literal value ``mosaic``
domain          Client identifier for nowcast domain used for logging
config          S3 URL of mosaic configuration file to execute
reference_time  Reference time for forecast to run in ISO8601 format
user_parameters List of overrides for the values of user defined format parameters (optional)
sqs_delete      When to delete SQS request, one of ``immediate`` or ``complete`` (optional)
result          Present in result messages with value set to either ``success`` or ``failure``
=============== ==============================================================================

Request message example:

.. code-block:: json

   {
     "action": "mosaic",
     "domain": "australia",
     "config": "s3://mysteps-bucket.s3.amazonaws.com/config/australia.mosaic.conf",
     "reference_time": "2021-03-25T00:10:00Z"
   }

Result message example:

.. code-block:: json

   {
     "action": "mosaic",
     "domain": "australia",
     "config": "s3://mysteps-bucket.s3.amazonaws.com/config/australia.adv.conf",
     "reference_time": "2021-03-25T00:10:00Z",
     "result": "success"
   }

Perform Verification
++++++++++++++++++++
Requests verification for a period of observations be performed. The request will be queued and strictly ordered with other
requests requests that share the same SQS message group id.  In a typical deployment, the message group id should be set
to a dedicated ``verification`` group.  This serialises verification for all domains, and ensures that a long running
verification task does not stall forecast requests for the same site.

It is the user's responsibility to ensure that data required for the verification is available in S3 before the verify
request is issued.  This may require logic at the client end to ensure all desired input nowcasts have been completed
before issuing the verify request.

=============== ==============================================================================================
Parameter       Description
=============== ==============================================================================================
action          Literal value ``verify``
domain          Client identifier for nowcast domain used for logging
config          S3 URL of verification configuration file to execute
from            Start of verification period in ISO8601 format (inclusive - time of first observation to use)
till            End of verification period in ISO8601 format (exclusive - time past last observation to use)
period          Time between observations in seconds (integer)
user_parameters List of overrides for the values of user defined format parameters (optional)
sqs_delete      When to delete SQS request, one of ``immediate`` or ``complete`` (optional)
result          Present in result messages with value set to either ``success`` or ``failure``
=============== ==============================================================================================

Request message example:

.. code-block:: json

   {
     "action": "verify",
     "domain": "radar-002",
     "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.verify.conf",
     "from": "2021-03-25T00:00:00Z",
     "till": "2021-03-26T00:00:00Z",
     "period": 300
   }

Result message example:

.. code-block:: json

   {
     "action": "verify",
     "domain": "radar-002",
     "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.verify.conf",
     "from": "2021-03-25T00:00:00Z",
     "till": "2021-03-26T00:00:00Z",
     "period": 300,
     "result": "success"
   }

Aggregate Verification Results
++++++++++++++++++++++++++++++
Requests aggregation of a set of individual verification results.  The request will be queued and strictly ordered with
other requests requests that share the same SQS message group id.  In a typical deployment, the message group id should
be set to a dedicated ``verification`` group.  This serialises verification for all domains, and ensures that a long
running verification task does not stall forecast requests for the same site.

It is the user's responsibility to ensure that data required for the aggregation is available in S3 before the request
is issued.  This may require logic at the client end to ensure all desired input verifications have been completed
before issuing the aggregate request.

=============== ==============================================================================================
Parameter       Description
=============== ==============================================================================================
action          Literal value ``aggregate-verify``
domain          Client identifier for nowcast domain used for logging
config          S3 URL of verification configuration file
paths           A JSON array of strings, where each entry is the S3 URL of a verification result to aggregate
user_parameters List of overrides for the values of user defined format parameters (optional)
sqs_delete      When to delete SQS request, one of ``immediate`` or ``complete`` (optional)
result          Present in result messages with value set to either ``success`` or ``failure``
=============== ==============================================================================================

Request message example:

.. code-block:: json

   {
     "action": "aggregate-verify",
     "domain": "radar-002",
     "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.verify.conf",
     "paths":
     [
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-25.verif.nc",
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-26.verif.nc",
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-27.verif.nc",
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-28.verif.nc"
     ]
   }

Result message example:

.. code-block:: json

   {
     "action": "aggregate-verify",
     "domain": "radar-002",
     "config": "s3://mysteps-bucket.s3.amazonaws.com/config/melbourne.verify.conf",
     "paths":
     [
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-25.verif.nc",
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-26.verif.nc",
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-27.verif.nc",
       "s3://mysteps-bucket.s3.amazonaws.com/data/002/verif/2020-03-28.verif.nc"
     ],
     "result": "success"
   }

Example Client Usage
====================

Forecast Configuration
======================

Mosaic Generation
=================

System Monitoring
=================

