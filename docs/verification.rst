.. _Verification:

========================================================================================================================
Verification
========================================================================================================================
STEPS has a built in capability to generate probabilistic verification metrics for the nowcast ensembles that it
produces.  By nominating a particular observation period, set of lead times, and set of precipitation thresholds STEPS
will automatically verify each observation in the period against any matching forecasts it can locate.

The following verification scores are generated:
- Relative Operating Characteristic (ROC Curve)
- Continuous Ranked Probability Score (CRPS)
- Reliability
- Histogram of forecast probabilities (for sharpness)
- Spread reliability
- Rank histogram
- Root Mean Square Error (RMSE)

Performing a verification run
============================================================
Verification is performed by running the main STEPS executable with the ``--verify`` option.  In this mode a different
configuration file is expected, which is described :ref:`below<verify-config>`.

.. code-block:: bash

    steps --verify /path/to/my/verification.conf

As with the forecasting mode, it is possible to overwrite individual configuration items by providing them as extra
command line arguments:

.. code-block:: bash

    steps --verify /path/to/my/verification.conf from=2020-03-25T00:10:00Z till=2020-04-25T00:10:00Z

Once the verification has completed, a NetCDF file will be created at the configured output location.  This file
contains the results of the verification but must still be plotted.

Aggregating verification results
============================================================
Multiple verification result files may be aggreated into a single file to allow for the plotting of bulk statistics.
This is performed by running the main STEPS executable with the ``--verify-aggregate`` option.  The same configuration
file used by the ``verify`` option must be supplied as the first command line parameter.  The list of verification
files to be aggregated is read from the standard input stream (one per line).  Typically the user will pipe a list
of files into the process as demonstrated below:

.. code-block:: bash

    ls *.verif.nc | steps --verify-aggregate /path/to/my/verification.conf

The aggregated verification file will be written to the path as dictated by the ``output`` parameter of the
configuration file.  Care should be taken to ensure this parameter has been set appropriately when reusing the
same configuration file as was used to generate the original inputs with ``--verify``.  It may be overwritten on the
command line as in other modes:

.. code-block:: bash

    ls *.verif.nc | steps --verify-aggregate /path/to/my/verification.conf output=my_aggregate.nc

.. warning::
    The input verification files are NOT checked for consistency with the supplied configuration file and each other.
    The user must ensure that it makes sense to combine the supplied input files, and that these files were calculated
    with settings that are consistent with the supplied configuration file.

    For example, if different lead time values are used between inputs and/or configuration the aggregation process
    will either crash (if a different number of lead times was used) or silently combine values resulting in incorrect
    statistics (same number of lead times, but different values).

It is possible to use this function to combine verification results collected from forecasts for different but similar
domains.  This may be sensible as long as the grid size and resolution of each domain are identical.  In this situation
it is advisable to ensure any ``.reference-file`` and ``.reference-variable`` metadata values from the configuration
file have been removed so that projection meatdata is not included in the resulting file.

Plotting verification results
============================================================
STEPS provides two different methods to visualize the verification results as either an interactive web page, or as
a series of static PNG plots.  The web page visualisation is convenient for investigating results and comparing
configurations or models, while the PNG based plots are suitable for inclusion in reports and other publications.

To generate an HTML page containing interactive versions of the plots run the STEPS executable with the
``--generate-plots`` command line option.  When this option is provided the command line arguments should be the
name of the HTML file to be output, followed by the path to one or more verification NetCDF files.  If multiple
NetCDF file paths are given then the HTML page will allow the user to directly compare the results of the
different verifications.

.. code-block:: bash

    # generate HTML plots for a verification result
    steps --generate-plots output.html verification.nc

    # generate HTML plots to compare multiple verification results
    steps --generate-plots output.html trial1.verif.nc trial2.verif.nc

To generate PNG plots of the verification two Python plots are distributed along with the main STEPS executable.  These
scripts are called ``steps-plot-verification`` and ``steps-plot-comparison``, and they allow the plotting of a single
verification or comparitive verification results respectively.  Usage of these scripts is demonstrated below:

.. code-block:: bash

    # create plots for a single verification
    steps-plot-verification verification.nc output_dir

    # create plots to compare multiple verification results
    steps-plot-comparison output_dir trial1.verif.nc trial2.verif.nc

.. note::

    For the PNG plotting scripts to succeed you must have a Python 3 envionment enabled which has matplotlib, numpy,
    and netCDF4 libraries installed.

.. _verify-config:

Configuration file reference
============================================================
This section describes the configuration settings which apply to the verification mode configuration file.

user_parameters
    **Object, optional** -
    User defined parameters to use in various format strings throughout the configuration.  See
    :ref:`user_parameters<user_parameters>`.

model_name
    **String** -
    User defined name for the model which will be stored in the verification NetCDF.  This value is used as a label for
    this verification dataset when creating comparitive verification plots.  A typical use is to distinguish between
    multiple configurations when performing trials for the purpose of tuning.

from
    **Timestamp, ``yyyy-mm-ddThh:mm:ssZ``** -
    Sets the time of the first **observation** (not forecast) that will be used as part of the verification.  Typically
    this means that ensembles should be available for times earlier than this time so that forecasts of this time
    at every lead time to be verified can be accessed.

from
    **Timestamp, ``yyyy-mm-ddThh:mm:ssZ``** -
    Sets the time of the last **observation** (not forecast) that will be used as part of the verification.

period
    **Integer, seconds** -
    Length of time in seconds between consecutive observations to verify.

members
    **Integer** -
    Number of members expected in each ensemble.

member_offset
    **Integer** -
    Index of first member in ensemble to verify.

output
    **String, file path** -
    A format string used to build the path of the verification output file that will be written.  If this verification
    file already exists then the results of the verification will be merged with the existing data in the file.  This
    facilitates online verification where observations are verified incrementally as they become available.

    The string may contain references to variables which are provided by the model, which allows the path to encode user
    parameters and timestamps associated with the verification.  The syntax for referencing a variable is described by the
    `Format String Syntax`_ documentation of the `{fmt} library`_ which is used to format the path string.

    .. _Format String Syntax: https://fmt.dev/latest/syntax.html
    .. _{fmt} library: https://fmt.dev

    The following variables are exported by the model:

    ========== =============================================== ========= ============================ ====================
    Name       Description                                     Type      Example Format               Example Output
    ========== =============================================== ========= ============================ ====================
    param:name :ref:`User defined parameters<user_parameters>` Various   ``{param:site:02d}``         002
    from       Begining time of verification period            Timestamp ``{time:%Y%m%d_%H%M%S}``     20200331_112253
    till       End time of verification period                 Timestamp ``{reference:%Y%-%m-%d_%H}`` 2020-03-31_11
    ========== =============================================== ========= ============================ ====================

metadata
    **Object,optional** -
    User defined metadata that may be used by the NetCDF writer.  See :ref:`metadata<product-metadata>` and
    :ref:`NetCDF output - metadata<netcdf-metadata>`.

minimum_rain_rate
    **Real, mm/hr** -
    Minimum detectable rain rate in mm/hr.  This is the minimum rain rate used to indicate a 'raining' grid cell.

grid_cols
    **Integer** -
    Width of the domain in grid cells.

grid_rows
    **Integer** -
    Height of the domain in grid cells.

lead_times
    **Array of integers, seconds** -
    List of lead times for which to perform verification.

thresholds
    **Array of reals** -
    List of precipitation thresholds for which to perform verification.

spread_reliability_bins
    **Integer** -
    Number of RMS spread bins to use while determining the spread reliability score.

spread_reliability_max_error
    **Real** -
    Maximum RMSE expected while determining spread reliability score.  Sets upper limit of highest RMSE bin.

observations
    **Object** -
    This object contains configuration for an input reader which tells STEPS how to locate and read observations.  The
    configuration for this object is identical to the :ref:`observation input<input>` object.  See also
    :ref:`NetCDF input`.

ensembles
    **Object** -
    This object contains configuration for an input reader which tells STEPS how to locate and read ensemble members.
    The configuration for this object is identical to the :ref:`observation input<input>` object.  See also
    :ref:`NetCDF input`.
