========================================================================================================================
References
========================================================================================================================

.. [Jara-Wilde15] Jara-Wilde, Jorge & Cerda, Mauricio & Delpiano, Jose & Hartel, Steffen. (2015). An Implementation of Combined Local-Global Optical Flow. Image Processing On Line. 5. 139-158. 10.5201/ipol.2015.44.
.. [Brox04] Thomas Brox, Andrés Bruhn, Nils Papenberg, Joachim Weickert, "High Accuracy Optical Flow Estimation Based on a Theory for Warping", In Proc. 8th European Conference on Computer Vision, Springer LNCS 3024, T. Pajdla and J.Matas (Eds.), vol. 4, pp. 25-36, Prague, May 2004.
.. [Farneback03] Farneback, G. (2003) 'Two-Frame Motion Estimation Based on Polynomial Expansion', Proceeings of the 13th Scandaniavian Converence on Image Analysis
