# Short Term Ensemble Prediction System

# Third Party Code
STEPS utilizes code extracted from several open source libraries.  For convenience, where these libraries are not
commonly available as part of a standard distribution, their code has been included in the STEPS repository under
the `src/ext` directory.  The original licenses for included third party code is retained under the relevant
subdirectory for each library.

The table below details the third party code that is distributed with the STEPS codebase.

Name     | Purpose                               | URL                                  | Release
-------  | ------------------------------------- | ------------------------------------ | -------
doctest  | Unit testing framework                | https://github.com/onqtam/doctest    | 2.4.6
fmt      | String formatting                     | https://github.com/fmtlib/fmt        | 10.2.1
span     | Implementation of C++20's `std::span` | https://github.com/tcbrindle/span    | master (cd0c6d0)
clg      | Optical flow algorithm                | http://www.ipol.im/pub/art/2015/44   | -
brox     | Optical flow algorithm                | http://www.ipol.im/pub/art/2013/21   | -
