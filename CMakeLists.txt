#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.9)

# default to a release build (must be before project())
if (DEFINED CMAKE_BUILD_TYPE)
  set(DEF_BUILD_TYPE ${CMAKE_BUILD_TYPE})
else()
  set(DEF_BUILD_TYPE Release)
endif()
set(CMAKE_BUILD_TYPE ${DEF_BUILD_TYPE} CACHE STRING "Build type: <none>|Debug|Release|RelWithDebInfo|MinSizeRel")

# default to a 'developer clone' version of 0.0.0
set(RELEASE_VERSION "0.0.0" CACHE STRING "Release number X.Y.Z")

# user options
option(BUILD_STEPS          "Enable building of the steps application" ON)
option(WITH_AWS             "Enable support for AWS S3 bucket I/O" OFF)
option(BUILD_STEPS_AWS_CFN  "Enable building of the steps-aws cloudformation templates" OFF)
option(BUILD_DOCS           "Enable building of the user guide and other documentation" OFF)
option(ENABLE_UNIT_TESTS    "Enable unit test generation (make test)" ON)
option(ENABLE_COVERAGE      "Enable coverage report generation (make coverage)" OFF)
option(ENABLE_ASAN          "Enable memory checking with AddressSanitizer" OFF)

# set implied options
if (BUILD_STEPS)
  set(BUILD_CORE ON)
else()
  set(BUILD_CORE OFF)
  set(ENABLE_UNIT_TESTS OFF)
  set(ENABLE_COVERAGE OFF)
  set(ENABLE_ASAN OFF)
endif()

# initialize our project
project(steps VERSION ${RELEASE_VERSION} LANGUAGES NONE)

# custom modules
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

if (BUILD_CORE)
  enable_language(C)
  enable_language(CXX)

  # standard cmake modules
  include(GNUInstallDirs)

  # external dependencies
  find_package(Threads REQUIRED)
  find_package(FFTW3f REQUIRED)
  find_package(ZLIB REQUIRED)
  find_package(HDF5 REQUIRED COMPONENTS C HL)
  find_package(netCDF 4.3.3.1 REQUIRED)
  find_package(UDUNITS2 REQUIRED)

  # require C++17 or better, disable compiler extensions by default
  set(CMAKE_CXX_STANDARD 17)
  set(CMAKE_CXX_STANDARD_REQUIRED ON)
  set(CMAKE_CXX_EXTENSIONS OFF)

  # set a high warning level
  add_compile_options(-Wall -pedantic -Wextra -Wno-unused-parameter)

  # model core source files
  set(SRC_FILES
    src/advection.cc
    src/autocorrelations.cc
    src/autoregressive_model.cc
    src/array1.cc
    src/array2.cc
    src/blend_weights.cc
    src/cascade.cc
    src/compression.cc
    src/configuration.cc
    src/core.cc
    src/cf.cc
    src/diagnostics.cc
    src/distribution_correction.cc
    src/engine.cc
    src/filters.cc
    src/format.cc
    src/fourier_transform.cc
    src/io.cc
    src/io_netcdf.cc
    src/local_statistics.cc
    src/metadata.cc
    src/mosaic.cc
    src/nc.cc
    src/optical_flow.cc
    src/optical_flow_brox.cc
    src/optical_flow_brox_mt.cc
    src/optical_flow_clg.cc
    src/optical_flow_rf3.cc
    src/persistent_cache.cc
    src/plots.cc
    src/postprocessor.cc
    src/products.cc
    src/profile.cc
    src/scores.cc
    src/skills_climatology.cc
    src/statistics.cc
    src/stochastic.cc
    src/stream.cc
    src/stream_state.cc
    src/test_patterns.cc
    src/thread_pool.cc
    src/trace.cc
    src/transform.cc
    src/unit.cc
    src/unit_test.cc
    src/util.cc
    src/vec2.cc
    src/verification.cc

    src/ext/clg/bicubic_interpolation.c
    src/ext/clg/clg_of.c
    src/ext/clg/mask.c
    src/ext/clg/util.c
    src/ext/clg/zoom.c
    src/ext/fmt/format.cc
  )
  set(HDR_FILES
    src/advection.h
    src/autocorrelations.h
    src/autoregressive_model.h
    src/array1.h
    src/array2.h
    src/blend_weights.h
    src/cascade.h
    src/compression.h
    src/configuration.h
    src/core.h
    src/cf.h
    src/diagnostics.h
    src/distribution_correction.h
    src/engine.h
    src/filters.h
    src/format.h
    src/fourier_transform.h
    src/io.h
    src/io_netcdf.h
    src/local_statistics.h
    src/macros.h
    src/metadata.h
    src/mosaic.h
    src/nc.h
    src/optical_flow.h
    src/options.h
    src/persistent_cache.h
    src/plots.h
    src/postprocessor.h
    src/products.h
    src/profile.h
    src/scores.h
    src/skills_climatology.h
    src/statistics.h
    src/stochastic.h
    src/stream.h
    src/stream_state.h
    src/test_patterns.h
    src/thread_pool.h
    src/trace.h
    src/traits.h
    src/transform.h
    src/unit.h
    src/unit_test.h
    src/util.h
    src/vec2.h
    src/verification.h
    src/version.h
  )
  set(HDR_EXT_CLG_FILES
    src/ext/clg/bicubic_interpolation.h
    src/ext/clg/clg_of.h
    src/ext/clg/mask.h
    src/ext/clg/util.h
    src/ext/clg/zoom.h
  )
  set(HDR_EXT_DOCTEST_FILES
    src/ext/doctest/doctest.h
  )
  set(HDR_EXT_FMT_FILES
    src/ext/fmt/core.h
    src/ext/fmt/chrono.h
    src/ext/fmt/format-inl.h
    src/ext/fmt/format.h
  )
  set(HDR_EXT_SPAN_FILES
    src/ext/span/span.h
  )

  # custom command to generate version.cc
  # depends on the same files as steps library and executable to ensure it is updated when any source file changes
  add_custom_command(
    OUTPUT ${PROJECT_BINARY_DIR}/version.cc
    COMMAND ${CMAKE_COMMAND}
      -DDIR_REPO=${PROJECT_SOURCE_DIR}
      -DPRJ_VERS=${PROJECT_VERSION}
      -DCOMPILER=${CMAKE_CXX_COMPILER_ID}-${CMAKE_CXX_COMPILER_VERSION}
      -P ${PROJECT_SOURCE_DIR}/version.cmake
    DEPENDS
      ${PROJECT_SOURCE_DIR}/version.cmake
      ${SRC_FILES}
      ${HDR_FILES}
      ${HDR_EXT_CLG_FILES}
      ${HDR_EXT_DOCTEST_FILES}
      ${HDR_EXT_FMT_FILES}
      ${HDR_EXT_SPAN_FILES}
      src/main.cc
    COMMENT "Generating build information (version.cc)"
  )

  # build an object library for all the core model code
  add_library(steps-core OBJECT
    ${SRC_FILES}
    ${HDR_FILES}
    ${HDR_EXT_CLG_FILES}
    ${HDR_EXT_DOCTEST_FILES}
    ${HDR_EXT_FMT_FILES}
    ${HDR_EXT_SPAN_FILES}
    ${PROJECT_BINARY_DIR}/version.cc
  )
  target_include_directories(steps-core PRIVATE src/ext ${UDUNITS2_INCLUDE_DIRS})
  target_link_libraries(steps-core
    stdc++fs
    Threads::Threads
    FFTW3f::FFTW3f
    FFTW3f::FFTW3f_threads
    ZLIB::ZLIB
    ${HDF5_LIBRARIES}
    ${HDF5_HL_LIBRARIES}
    netCDF::netCDF
    UDUNITS2::UDUNITS2
  )

  # install our license and readme
  install(FILES LICENSE README.md DESTINATION "${CMAKE_INSTALL_DOCDIR}" COMPONENT runtime)

  # install the third party licenses
  # note that only some of our third party licenses must be included in binary distributions
  install(FILES src/ext/brox/license.txt DESTINATION "${CMAKE_INSTALL_DOCDIR}/licenses" COMPONENT runtime RENAME brox.txt)
  install(FILES src/ext/clg/license.txt DESTINATION "${CMAKE_INSTALL_DOCDIR}/licenses" COMPONENT runtime RENAME clg.txt)
endif()

# build the command line executable
if (BUILD_STEPS)
  add_executable(steps src/main.cc)
  target_compile_definitions(steps PRIVATE STEPS_REFERENCE_DATA_DIR="${CMAKE_INSTALL_FULL_DATADIR}/steps")
  target_link_libraries(steps steps-core)
  install(TARGETS steps DESTINATION "${CMAKE_INSTALL_BINDIR}" COMPONENT runtime)
  install(
    PROGRAMS
      src/steps-plot-ensemble
      src/steps-plot-verification
      src/steps-plot-comparison
    DESTINATION "${CMAKE_INSTALL_BINDIR}"
    COMPONENT runtime
  )
  install(
    FILES
      src/plots.html
      src/plots.js
    DESTINATION "${CMAKE_INSTALL_DATADIR}/steps"
    COMPONENT runtime
  )
endif()

# enable the AddressSanitizer memory checker if desired
if (ENABLE_ASAN)
  if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug" AND NOT CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
    message(FATAL_ERROR "AddressSanitizer requires debug symbols (set CMAKE_BUILD_TYPE=Debug|RelWithDebInfo)")
  endif()
  target_compile_definitions(steps-core PUBLIC STEPS_ENABLE_ASAN)
  target_compile_options(steps-core PUBLIC -fsanitize=address -fno-omit-frame-pointer)
  target_link_options(steps-core PUBLIC -fsanitize=address -fno-omit-frame-pointer)
endif()

# insert AWS support if desired
if (WITH_AWS)
  find_package(AWSSDK REQUIRED COMPONENTS monitoring sqs s3 s3-crt sns batch)
  target_sources(steps-core PRIVATE src/aws.cc src/aws.h src/aws_driver.cc src/aws_driver.h)
  target_compile_definitions(steps-core PUBLIC STEPS_WITH_AWS)
  target_link_libraries(steps-core ${AWSSDK_LINK_LIBRARIES})
endif()

# substitute our version number into the cloudformation templates
if (BUILD_STEPS_AWS_CFN)
  # these are picked up as artifacts directly by our CI pipeline from the build area, so no need to install them
  configure_file("${PROJECT_SOURCE_DIR}/aws/steps-aws-images.template.in" "${PROJECT_BINARY_DIR}/steps-aws-images.template" @ONLY)
  configure_file("${PROJECT_SOURCE_DIR}/aws/steps-aws-network-private.template.in" "${PROJECT_BINARY_DIR}/steps-aws-network-private.template" @ONLY)
  configure_file("${PROJECT_SOURCE_DIR}/aws/steps-aws-network-private-nat.template.in" "${PROJECT_BINARY_DIR}/steps-aws-network-private-nat.template" @ONLY)
  configure_file("${PROJECT_SOURCE_DIR}/aws/steps-aws-network-public.template.in" "${PROJECT_BINARY_DIR}/steps-aws-network-public.template" @ONLY)
  configure_file("${PROJECT_SOURCE_DIR}/aws/steps-aws-core.template.in" "${PROJECT_BINARY_DIR}/steps-aws-core.template" @ONLY)
endif()

# build the user guide
if (BUILD_DOCS)
  find_package(Sphinx REQUIRED)
  configure_file("${PROJECT_SOURCE_DIR}/docs/conf.py.in" "${PROJECT_BINARY_DIR}/docs_build/conf.py")
  add_custom_target(docs-html ALL
    ${SPHINX_EXECUTABLE}
      -q
      -b html
      -c "${PROJECT_BINARY_DIR}/docs_build"
      -d "${PROJECT_BINARY_DIR}/docs_cache"
      "${PROJECT_SOURCE_DIR}/docs"
      "${PROJECT_BINARY_DIR}/docs/html"
  )
  add_custom_target(docs-pdf ALL
    COMMAND ${SPHINX_EXECUTABLE}
      -q
      -b latex
      -c "${PROJECT_BINARY_DIR}/docs_build"
      -d "${PROJECT_BINARY_DIR}/docs_cache"
      "${PROJECT_SOURCE_DIR}/docs"
      "${PROJECT_BINARY_DIR}/docs/latex"
    COMMAND make -C "${PROJECT_BINARY_DIR}/docs/latex"
  )
  # we only install the docs if we are also building binaries, this decision is partially due to the GNUInstallDirs module
  # requiring that a language be enabled to resolve paths.
  if (BUILD_CORE)
    install(
      DIRECTORY "${PROJECT_BINARY_DIR}/docs/html"
      DESTINATION "${CMAKE_INSTALL_DOCDIR}"
      COMPONENT runtime
    )
    install(
      FILES "${PROJECT_BINARY_DIR}/docs/latex/steps_user_guide.pdf"
      DESTINATION "${CMAKE_INSTALL_DOCDIR}"
      COMPONENT runtime
    )
  endif()
endif()

# setup testing
if (ENABLE_UNIT_TESTS)
  target_compile_definitions(steps-core PUBLIC STEPS_UNIT_TEST)
  enable_testing()
  add_test(NAME "Units" COMMAND steps --unit-test reference_dir=${PROJECT_SOURCE_DIR}/ref)
  install(
    FILES
      ref/ut-lag0-full.nc
      ref/ut-lag1-full.nc
      ref/ut-lag2-full.nc
      ref/ut-lag0-mini.nc
      ref/ut-lag1-mini.nc
      ref/ut-lag2-mini.nc
    DESTINATION "${CMAKE_INSTALL_DATADIR}/steps/ref"
    COMPONENT runtime)
endif()
if (ENABLE_COVERAGE)
  if (NOT ENABLE_UNIT_TESTS)
    message(FATAL_ERROR "Code coverage requires unit test generation (set ENABLE_UNIT_TESTS=ON)")
  endif()
  if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
    message(FATAL_ERROR "Code coverage requires a debug build (set CMAKE_BUILD_TYPE=Debug)")
  endif()
  if (NOT CMAKE_COMPILER_IS_GNUCXX)
    message(FATAL_ERROR "Code coverage requires GCC compiler")
  endif()
  target_compile_options(steps-core PUBLIC --coverage)
  target_link_libraries(steps-core gcov)
  add_custom_target(coverage
    COMMAND steps --unit-test reference_dir=${PROJECT_SOURCE_DIR}/ref fast_mode=true
    COMMAND mkdir -p ${PROJECT_BINARY_DIR}/coverage
    COMMAND gcovr
        --root=${PROJECT_SOURCE_DIR}
        --object-directory=${PROJECT_BINARY_DIR}
        --exclude='.*/ext/.*'
        --exclude-unreachable-branches
        --html
        --html-details
        --print-summary
        --output=${PROJECT_BINARY_DIR}/coverage/coverage.html
    COMMAND gcovr
        --root=${PROJECT_SOURCE_DIR}
        --object-directory=${PROJECT_BINARY_DIR}
        --exclude='.*/ext/.*'
        --exclude-unreachable-branches
        --xml-pretty
        --print-summary
        --output=${PROJECT_BINARY_DIR}/coverage.xml
  )
endif()
