#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2018 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------

# the user MUST supply --define="src_path mypath" to tell us where the main source directory is
%{!?src_path: %define src_path %{nil}}

# the user may supply --with aws to build the steps-aws subpackage
%bcond_with aws

# the user may supply an installation prefix for the aws-cpp-sdk package if needed
%{!?aws_prefix: %define aws_prefix %{nil}}

# the user may supply --with asan to enable address santizer (for a big performance hit)
%bcond_with asan

# default to version 0.0.0 (indicates a local developer build)
# our CI job supplies an official version number for tagged releases
%{!?version: %define version 0.0.0}

# disable stripping of binaries if we are using AddressSanitizer
%if %{with asan}
  %global __os_install_post /usr/lib/rpm/brp-compress %{nil}
%endif

Name:           steps
Version:        %{version}
Release:        1%{?dist}
Summary:        Short Term Ensemble Prediction System (STEPS) Model Core
License:        Commonwealth of Australia, Bureau of Meteorology (ABN 92 637 533 532)
URL:            https://gitlab.bom.gov.au/radar/steps
Source0:        https://gitlab.bom.gov.au/radar/steps.git
BuildRequires:  cmake zlib-devel fftw-devel hdf5-devel netcdf-devel udunits2-devel
Requires:       zlib fftw hdf5 netcdf udunits2

%description
The Short Term Ensemble Prediction System (STEPS) is a stochastic probabilistic precipitation nowcasting model.  It
can be used to generate radar based advection nowcasts, radar/NWP blended nowcasts, downscaled NWP forecats and
synthetic storm simulations.

%if %{with aws}
%package aws
Summary: EC2 Instance Manager Service for STEPS on AWS
Requires: steps
%description aws
Service which executes STEPS based on requests from an SQS FIFO queue.
%endif

%prep
%setup -c -T

%build
%cmake %{src_path} \
    -DRELEASE_VERSION=%{version} \
    %{?with_aws:-DWITH_AWS=ON} %{?aws_prefix:-DCMAKE_PREFIX_PATH="%{aws_prefix}"} \
    %{?with_asan:-DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_ASAN=ON}
%cmake_build

%install
%cmake_install
%check
%if %{with aws}
mkdir -p %{buildroot}/%{_bindir}
install -m 755 %{src_path}/aws/steps-aws-crash-report %{buildroot}/%{_bindir}/
install -m 755 %{src_path}/aws/steps-aws-lifecycle-hook %{buildroot}/%{_bindir}/
mkdir -p %{buildroot}/%{_unitdir}
install -m 644 %{src_path}/aws/steps-aws.service %{buildroot}/%{_unitdir}/
install -m 644 %{src_path}/aws/steps-aws-lifecycle-hook.service %{buildroot}/%{_unitdir}/
mkdir -p %{buildroot}/%{_localstatedir}/steps-aws
mkdir -p %{buildroot}/%{_sysconfdir}
touch %{buildroot}/%{_sysconfdir}/steps-aws.conf
touch %{buildroot}/%{_sysconfdir}/steps-aws-glue.conf
%endif

%files
%{_bindir}/steps
%{_bindir}/steps-plot-comparison
%{_bindir}/steps-plot-ensemble
%{_bindir}/steps-plot-verification
%{_datadir}/steps
%{_docdir}/steps
%docdir %{_docdir}/steps

%if %{with aws}
%files aws
%{_bindir}/steps-aws-crash-report
%{_bindir}/steps-aws-lifecycle-hook
%{_unitdir}/steps-aws.service
%{_unitdir}/steps-aws-lifecycle-hook.service
%dir %attr(2775, steps-aws, steps-aws) %{_localstatedir}/steps-aws
%ghost %{_sysconfdir}/steps-aws.conf
%ghost %{_sysconfdir}/steps-aws-glue.conf

%pre aws
if ! getent group steps-aws >/dev/null ; then
  groupadd -f -r steps-aws
fi
if ! getent passwd steps-aws >/dev/null ; then
  useradd -r -g steps-aws -d %{_localstatedir}/steps-aws -s /sbin/nologin -c "STEPS AWS system user" steps-aws
fi

%post aws
%systemd_post steps-aws.service

%preun aws
%systemd_preun steps-aws.service

%postun aws
%systemd_postun steps-aws.service

%endif

%changelog

