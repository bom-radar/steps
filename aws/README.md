# STEPS on AWS
This repository provides a cloud based implementation of the STEPS nowcasting model which may be used to deliver STEPS
under a "Software as a Service" (SaaS) model.  The application stack deploys the resources needed to request, process,
and receive forecasts for any number of similar domains (e.g. multiple radars).  Processing resources are automatically
scaled based on a user specified maximum forecast queue latency.  This allows users to minimse costs while ensuring
that forecast latency meets business needs.

## Overview
The core stack consists of the following main functional components:
- An SQS queue to hold forecast requests issued by the user
- An EC2 auto scaling group to generate forecasts using the STEPS model
- An SNS topic to distribute forecast completion notifications to users
- A CloudWatch dashboard to allow monitoring of the application
- Various IAM roles and policies that allow a user to interact with the system

Storage needed by the application is implemented as S3 buckets which must be provided by the user.  The paths to all
files used by STEPS are implemented as S3 URLs provided by the user in the configuration file for each forecast
domain.  This allows input and output files to be accessed at their original and final paths without the need for
copying into an internal storage area.

## Installation
The following steps are required to spin up the application stack in AWS, starting from scratch:
1. Build the steps-aws RPM.
2. Copy the steps-aws RPM to an S3 bucket.
3. Create the steps-aws-image-builder stack.  This stack will create an EC2 machine image (AMI) that has the steps-aws
   service and any other prerequisite software installed.  It must be configured to locate the RPM uploaded in the
   previous step.
4. Create the core steps-aws stack.  The AMI created in the previous step must be provided.
5. Assign IAM policies to client application user so that it can assume the STEPS client role.
6. Modify S3 bucket policies to grant the STEPS EC2 instance role access to all required storage.
6. Subscribe the client application to the forecast result SNS topic.

From this point, the user may issue forecast requests by sending the appropriate JSON message to the forecast request
queue.  Once a forecast is completed, the JSON message will be sent back to the client via their subscription to the
forecast result SNS topic along with a 'result' value indicating whether the forecast succeeded.
