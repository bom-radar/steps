#!/bin/bash

source /etc/steps-aws-glue.conf

time_token=0
token=""

# Wait for the lifecycle hook to indicate we should terminate
echo "Entering lifecycle hook monitoring loop"
while true; do
  # Perform the check once every 10 seconds
  sleep 10

  # Generate a new metadata session token every 3 hours (although they are valid for 6 hours)
  time_now=$(date -u +%s)
  if (( time_now - time_token > 10800 )); then
    echo "Generating new ec2 metadata session token"
    time_token=${time_now}
    token=$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")
  fi

  # Check if we should terminate
  target_state=$(curl -s http://169.254.169.254/latest/meta-data/autoscaling/target-lifecycle-state -H "X-aws-ec2-metadata-token: ${token}")
  if [[ "${target_state}" == "Terminated" ]]; then
    echo "Termination lifecycle event detected"
    break
  fi
done

# Stop the steps-aws service
echo "Stopping steps-aws service"
systemctl disable --now steps-aws

# Signal successful shutdown
echo "Signalling termination lifecycle hook to continue"
instance_id=$(curl -s http://169.254.169.254/latest/meta-data/instance-id -H "X-aws-ec2-metadata-token: ${token}")
aws autoscaling complete-lifecycle-action \
  --lifecycle-hook-name Terminate \
  --auto-scaling-group-name ${STEPS_AUTOSCALING_GROUP} \
  --lifecycle-action-result CONTINUE \
  --instance-id ${instance_id} \
  --region ${STEPS_REGION}
