#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2022 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
Name: netcdf
# if you update the version here, also update it in steps-aws-images.template.in
Version: 4.9.0
Release: 1%{?dist}
Summary: Libraries for the Unidata network Common Data Form
License: NetCDF
URL: http://www.unidata.ucar.edu/software/netcdf/
Source0: https://github.com/Unidata/netcdf-c/archive/v%{version}/%{name}-%{version}.tar.gz

%description
Quick and dirty netcdf package for Amazon Linux

%package devel
Summary: Headers and libraries for netcdf
Requires: %{name} = %{version}-%{release}

%description devel
Quick and dirty netcdf package for Amazon Linux

%prep
%autosetup -n netcdf-c-%{version}

%build
%cmake3 -DENABLE_TESTS=OFF
%cmake3_build

%install
%cmake3_install

%files
%{_bindir}/*
%{_libdir}/libnetcdf*
%{_mandir}/man1/*

%files devel
%{_includedir}/*
%{_libdir}/cmake/netCDF
%{_libdir}/pkgconfig/*
