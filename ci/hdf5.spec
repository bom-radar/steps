#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2022 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
Name: hdf5
# if you update the version here, also update it in steps-aws-images.template.in
Version: 1.12.2
Release: 1%{?dist}
Summary: A general purpose library and file format for storing scientific data
License: BSD
URL: https://portal.hdfgroup.org/display/HDF5/HDF5

%global version_main %(echo %version | cut -d. -f-2)
Source0: https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-%{version_main}/hdf5-%{version}/src/hdf5-%{version}.tar.gz

%description
Quick and dirty hdf5 package for Amazon Linux

%package devel
Summary: Headers and libraries for hdf5
Requires: %{name} = %{version}-%{release}

%description devel
Quick and dirty hdf5 package for Amazon Linux

%prep
%autosetup

%build
%cmake3 -DHDF5_ENABLE_Z_LIB_SUPPORT=ON -DBUILD_STATIC_LIBS=OFF -DBUILD_TESTING=OFF -DHDF5_BUILD_UTILS=OFF -DHDF5_BUILD_TOOLS=OFF -DHDF5_BUILD_EXAMPLES=OFF -DHDF5_BUILD_CPP_LIB=OFF
%cmake3_build

%install
%cmake3_install
mkdir -p %{buildroot}%{_docdir}/%{name}
mv %{buildroot}/usr/share/COPYING %{buildroot}%{_docdir}/%{name}/
mv %{buildroot}/usr/share/*.txt %{buildroot}%{_docdir}/%{name}/

%files
/usr/lib/libhdf5*.so
/usr/lib/libhdf5*.so.*
%doc %{_docdir}/%{name}

%files devel
%{_bindir}/h5*
%{_includedir}/*
/usr/lib/libhdf5*
/usr/lib/pkgconfig/*
/usr/cmake/*
