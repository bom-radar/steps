#----------------------------------------------------------------------------------------------------------------------
# Short Term Ensemble Prediction System (STEPS)
#
# Copyright 2022 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------
Name: udunits2
# if you update the version here, also update it in steps-aws-images.template.in
Version: 2.2.28
Release: 1%{?dist}
Summary: A library for manipulating units of physical quantities
License: UCAR
URL: http://www.unidata.ucar.edu/software/udunits/
Source0: https://artifacts.unidata.ucar.edu/repository/downloads-udunits/%{version}/udunits-%{version}.zip

%description
Quick and dirty udunits2 package for Amazon Linux

%package devel
Summary: Headers and libraries for udunits2
Requires: %{name} = %{version}-%{release}

%description devel
Quick and dirty udunits2-devel package for Amazon Linux

%prep
%autosetup -n udunits-%{version}
sed -i 's/path = default_udunits2_xml_path();/path = DEFAULT_UDUNITS2_XML_PATH;/g' lib/xml.c

%build
%cmake3
%cmake3_build

%install
%cmake3_install
mkdir -p %{buildroot}%{_includedir}/%{name}/
mv %{buildroot}%{_includedir}/*.h %{buildroot}/%{_includedir}/%{name}/
mkdir -p %{buildroot}%{_libdir}
mv %{buildroot}/usr/lib/* %{buildroot}/%{_libdir}/
mv %{buildroot}%{_docdir}/udunits %{buildroot}%{_docdir}/%{name}

%files
%{_bindir}/%{name}
%{_datadir}/udunits/
%{_infodir}/%{name}*.info.*
%{_libdir}/lib%{name}.so
%doc %{_docdir}/%{name}

%files devel
%{_includedir}/%{name}/
